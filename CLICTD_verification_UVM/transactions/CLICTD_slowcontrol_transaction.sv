// Filename           : CLICTD_slowcontrol_transaction.sv
// Author             : Núria Egidos 
// Created on         : 25/7/18
// Last modification  : 25/7/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Transaction to interact with the slow control

// https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1c/html/files/macros/uvm_object_defines-svh.html

// http://blog.verificationgentleman.com/2015/09/of-copies-and-clones.html
// http://www.sunburst-design.com/papers/CummingsSNUG2014SV_UVM_Transactions.pdf
// https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1c/html/files/macros/uvm_object_defines-svh.html#Field_Macros

// Compare method: // https://www.vmmcentral.org/uvm_vmm_ik/files3/base/uvm_object-svh.html#uvm_object.compare, http://cluelogic.com/2013/01/uvm-tutorial-for-candy-lovers-do-hooks/

`ifndef __CLICTD_slowcontrol_transaction_sv__
`define __CLICTD_slowcontrol_transaction_sv__


// Transaction to hold one "snapshot" of slowcontrol_interface, i.e.
// the value of all of its signals at a certain instant of time
class CLICTD_slowcontrol_transaction extends uvm_sequence_item;
  //`uvm_object_utils(CLICTD_slowcontrol_transaction) // Register the transaction and its fields to the factory

  // Address and content (read from or written to)
  // one of the slow control registers
	logic [7:0] register_address;
  logic [7:0] register_content;

  // Indicate if a read or write I2C operation is to be performed
  string read_or_write; 

  // Associative array to store errors resulting from comparing transactions
  field_slowcontroltrans_array_type fields_with_error;


	//--------- Constructor
	function new(string name = "");
		super.new(name);
	endfunction: new

  //--------- Implementation of the do_compare function, called by the compare() method
  // The do_compare() method is called by the compare() method. The do_compare() is used to 
  // compare each property of the transaction object; it returns 1 if the comparison succeeds, 
  // and returns 0 if the comparison fails. Note that we have to cast a uvm_object (rhs) to a 
  // transaction of the present kind in order to access the properties to compare. 
  // We must call the super.do_compare() to compare the properties of the super class. 
  // The uvm_comparer argument provides a policy object for doing comparisons, but we do not use it.
  // http://cluelogic.com/2013/01/uvm-tutorial-for-candy-lovers-do-hooks/


  // Note: why to explicitly call compare() instead of just using if(transaction1 == transaction2)?
  // Using == instead of compare, we may be comparing the references of the transactions, but not 
  // all of the comparable fields (those that haven't been disabled with UVM_NOCOMPARE flag), and 
  // thus we may not be comparing all the fields or we may not get the expected result. It's better to 
  // implement do_compare in order to make sure that all the fields to be checked are compared and that
  // the results is calculated as we want.
  virtual function bit do_compare( uvm_object rhs, uvm_comparer comparer );
          CLICTD_slowcontrol_transaction that;
          bit issues_address = 0; bit issues_content = 0;

          if ( ! $cast( that, rhs ) ) return 0;

          for (int i = 0; i < 8; i++) begin
              if ( this.register_address[i] != that.register_address[i] ) issues_address = 1;
              if ( this.register_content[i] != that.register_content[i] ) issues_content = 1;
          end
     
          if ( issues_address ) fields_with_error[REGISTER_ADDRESS] = REGISTER_ADDRESS;
          if ( issues_content ) fields_with_error[REGISTER_CONTENT] = REGISTER_CONTENT;
 
          return ( super.do_compare( rhs, comparer ) &&
                   !issues_address                   &&
                   !issues_content   
                  );

  endfunction: do_compare
		
		
		// This function is implicitly called when copy() or clone() are used
		// https://verificationacademy.com/cookbook/transaction/methods
		function void do_copy(uvm_object rhs);
            CLICTD_slowcontrol_transaction that;
 
            //if(!$cast(that, rhs)) begin return;
												assert($cast(that, rhs));
 
            super.do_copy(rhs); // Chain the copy with parent classes
												this.register_address = that.register_address;
            this.register_content = that.register_content;
            this.read_or_write = that.read_or_write; 
            this.fields_with_error = that.fields_with_error;
           
  endfunction: do_copy
 


  `uvm_object_utils_begin(CLICTD_slowcontrol_transaction)
    `uvm_field_int(register_address, UVM_ALL_ON)
    `uvm_field_int(register_content, UVM_ALL_ON)
    `uvm_field_string(read_or_write, UVM_ALL_ON)
  `uvm_object_utils_end
  // we use the UVM_ALL_ON argument so that all the fields of the transaction
  // can be affected (copied, compared, etc.) by the default methods


endclass: CLICTD_slowcontrol_transaction





`endif // __CLICTD_slowcontrol_transaction_sv__

