// Filename           : CLICTD_dataout_transaction.sv
// Author             : Núria Egidos 
// Created on         : 20/6/18
// Last modification  : 20/6/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Transaction

// https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1c/html/files/macros/uvm_object_defines-svh.html

// http://blog.verificationgentleman.com/2015/09/of-copies-and-clones.html
// http://www.sunburst-design.com/papers/CummingsSNUG2014SV_UVM_Transactions.pdf
// https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1c/html/files/macros/uvm_object_defines-svh.html#Field_Macros

// Compare method: // https://www.vmmcentral.org/uvm_vmm_ik/files3/base/uvm_object-svh.html#uvm_object.compare, http://cluelogic.com/2013/01/uvm-tutorial-for-candy-lovers-do-hooks/

`ifndef __CLICTD_dataout_transaction_sv__
`define __CLICTD_dataout_transaction_sv__

//import uvm_pkg::*;
//`include <uvm_macros.svh>

// Include the parameter definition
//`include "CLICTD_parameters.sv" 


// Transaction to hold one "snapshot" of dataout_interface, i.e.
// the value of all of its signals at a certain instant of time
class CLICTD_dataout_transaction extends uvm_sequence_item;
  //`uvm_object_utils(CLICTD_dataout_transaction) // Register the transaction and its fields to the factory

  //bit timeout_alarm; // this bit is set high during readout if the rising edge of ENABLE_OUT_pad takes longer to arrive than expected, i.e. no start readout condition is detected
  
  int bit_readout_counter; // counter to store the position of the output stream that is being read out
  int config_bit_counter;
  int superpixel_segment_counter;
  int superpixel_in_segment_counter;
  int column_counter; // counter to store the position of column being readout

  // Fields used when chip_status = STATUS_RESET
  logic dataout_bit;       // bit to store the value of DATA_OUT for chip_status = STATUS_RESET

  /*
  // Fields used when chip_status = STATUS_SECOND_READOUT_ALL_ZEROS
  logic [`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED-1:0] all_zeros;

  // Fields used when chip_status = STATUS_CONFIG_READOUT_FIRST_HALF
  logic tpEnableDigital;
  logic mask0;                  
	logic mask1;                   
	logic mask2;                  
	logic mask3;                   
	logic mask4;                   
	logic mask5;                  
	logic mask6;                  
	logic mask7;                  
	logic tpEnableAnalog0;        
	logic tpEnableAnalog1;        
	logic tpEnableAnalog2;        
	logic tpEnableAnalog3;        
	logic tpEnableAnalog4;        
	logic tpEnableAnalog5;        
	logic tpEnableAnalog6;        
	logic tpEnableAnalog7;        
	logic tuningDAC00;            
	logic tuningDAC01;            
	logic tuningDAC02;            
	logic tuningDAC10;             
	logic dummy_bit_21_firsthalf; 


  // Fields used when chip_status = STATUS_CONFIG_READOUT_SECOND_HALF
  logic tuningDAC11;             
	logic tuningDAC12;             
	logic tuningDAC20;             
	logic tuningDAC21;              
	logic tuningDAC22;             
	logic tuningDAC30;             
	logic tuningDAC31;             
	logic tuningDAC32;             
	logic tuningDAC40;             
	logic tuningDAC41;             
	logic tuningDAC42;             
	logic tuningDAC50;             
	logic tuningDAC51;             
	logic tuningDAC52;             
	logic tuningDAC60;             
	logic tuningDAC61;              
	logic tuningDAC62;             
	logic tuningDAC70;             
	logic tuningDAC71;             
	logic tuningDAC72;             
	logic dummy_bit_20_secondhalf; 
	logic dummy_bit_21_secondhalf; 
  */
  // Fields used when chip_status = STATUS_MATRIX_READOUT_ACQUIRED
  //logic hit_flag;
  //logic ToT [int]; // associative array: the number of bits of ToT will depend on the readout mode
  //logic ToA [int]; // associative array: the number of bits of ToA will depend on the readout mode
  //logic hit_map [int]; // associative array: the number of bits of hit_map will depend on the readout mode
  logic column_content [int]; // associative array: the column content will depend on the readout mode

  // Fields used for any readout operation
  logic [`NUMBER_BITS_START_HEADER-1:0] start_header;
  logic [`NUMBER_BITS_STOP_HEADER-1:0] stop_header;
  logic [`NUMBER_BITS_COLUMN_HEADER-1:0] column_header;
		
		// Packed struct in which the different fields read out (ToT, ToA...) for each superpixel are stored
		Acquired_word_struct column_content_breakdown;
		
		// Packed struct in which the different fields from configuration readout (tpEnableDigital, mask...) for each superpixel are stored
		Configured_word_struct column_config_breakdown;
  
  // Associative array to store errors resulting from comparing transactions
  field_dataouttrans_array_type fields_with_error;

	//--------- Constructor
	function new(string name = "");
		super.new(name);
	endfunction: new


  //--------- Compare the transaction fields that are used in the pins_comparator
  // trans_to_compare is the transaction to which the field of the calling transaction are compared
  // fields_with_error is an associative array indexed by fields_dataout_transaction_type in which positions exist
  // if the associated field of the transaction to compare doesn't match the field of the calling transaction
  // (i.e. there are as many positions in the array as fields in which mismatch is detected between the compared transactions)
  function void compare_transactions (CLICTD_dataout_transaction trans_to_compare, field_dataouttrans_array_type fields_with_error); 
                    int issues;

                    //- - - - Checked in STATUS_RESET

                    // DATA_OUT
                    if (dataout_bit != trans_to_compare.dataout_bit) fields_with_error[DATAOUT] = DATAOUT;                    

                    // - - - - Checked in STATUS_CONFIG_READOUT_SECOND_HALF

                    // column_counter
                    if (column_counter != trans_to_compare.column_counter) fields_with_error[COLUMN_COUNTER] = COLUMN_COUNTER;

                    // size of column content
                    if (column_content.size() != trans_to_compare.column_content.size()) fields_with_error[SIZE_COLUMNCONTENT] = SIZE_COLUMNCONTENT;
                    else begin
                         // column_content
                         issues = 0; for (int i = 0; i < column_content.size(); i++) begin if (column_content[i] != trans_to_compare.column_content[i]) issues = 1; end
                         if (issues > 0) fields_with_error[COLUMN_CONTENT] = SIZE_COLUMNCONTENT;
                    end
                
   endfunction: compare_transactions

  //--------- Implementation of the do_compare function, called by the compare() method
  // The do_compare() method is called by the compare() method. The do_compare() is used to 
  // compare each property of the transaction object; it returns 1 if the comparison succeeds, 
  // and returns 0 if the comparison fails. Note that we have to cast a uvm_object (rhs) to a 
  // transaction of the present kind in order to access the properties to compare. 
  // We must call the super.do_compare() to compare the properties of the super class. 
  // The uvm_comparer argument provides a policy object for doing comparisons, but we do not use it.
  // http://cluelogic.com/2013/01/uvm-tutorial-for-candy-lovers-do-hooks/


  // Note: why to explicitly call compare() instead of just using if(transaction1 == transaction2)?
  // Using == instead of compare, we may be comparing the references of the transactions, but not 
  // all of the comparable fields (those that haven't been disabled with UVM_NOCOMPARE flag), and 
  // thus we may not be comparing all the fields or we may not get the expected result. It's better to 
  // implement do_compare in order to make sure that all the fields to be checked are compared and that
  // the results is calculated as we want.
  virtual function bit do_compare( uvm_object rhs, uvm_comparer comparer );
          
          CLICTD_dataout_transaction that;
          bit issues = 0;
          //$display("%t Started do_compare", $time);
          
          if ( ! $cast( that, rhs ) ) return 0;
          //$display("%t compare dataout_transaction, counter %d vs counter %d", $time, this.column_counter, that.column_counter);

          //- - - - Checked in STATUS_RESET

          // DATA_OUT
          if (this.dataout_bit != that.dataout_bit) begin issues = 1; fields_with_error[DATAOUT] = DATAOUT; end
   

          // - - - - Checked in STATUS_CONFIG_READOUT_SECOND_HALF

          // column_counter
          if (this.column_counter != that.column_counter) begin issues = 1; fields_with_error[COLUMN_COUNTER] = COLUMN_COUNTER; end

          // size of column content
          if (this.column_content.size() != that.column_content.size()) begin issues = 1; fields_with_error[SIZE_COLUMNCONTENT] = SIZE_COLUMNCONTENT; end
          else begin
               // column_content
               for (int i = 0; i < this.column_content.size(); i++) begin 
                    if (this.column_content[i] != that.column_content[i]) begin
                       issues = 1; 
                       fields_with_error[COLUMN_CONTENT] = COLUMN_CONTENT;
                       break;
                    end
               end
          end
          //$display("%t Finished do_compare, return %h", $time, super.do_compare( rhs, comparer ) && !issues);
          return ( super.do_compare( rhs, comparer ) && !issues );
										
          
  endfunction: do_compare
		
		
		// This function is implicitly called when copy() or clone() are used
		// https://verificationacademy.com/cookbook/transaction/methods
		function void do_copy(uvm_object rhs);
            CLICTD_dataout_transaction that;
 
            //if(!$cast(that, rhs)) begin return;
												assert($cast(that, rhs));
 
            super.do_copy(rhs); // Chain the copy with parent classes 
												
												this.bit_readout_counter = that.bit_readout_counter; 
            this.config_bit_counter = that.config_bit_counter;
            this.superpixel_segment_counter = that.superpixel_segment_counter;
            this.superpixel_in_segment_counter = that.superpixel_in_segment_counter;
            this.column_counter = that.column_counter; 
            this.dataout_bit = that.dataout_bit; 
	          	this.column_content = that.column_content; 
            this.start_header = that.start_header;
            this.stop_header = that.stop_header;
            this.column_header = that.column_header;
												copy_two_columndata_broken_into_superpixeldata (.superpixel_data_original(that.column_content_breakdown),	.superpixel_data_copied(this.column_content_breakdown));
												copy_two_columnconfig_broken_into_superpixelconfig (.superpixel_config_original(that.column_config_breakdown),	.superpixel_config_copied(this.column_config_breakdown));
            this.fields_with_error = that.fields_with_error;
		
           
  endfunction: do_copy


  `uvm_object_utils_begin(CLICTD_dataout_transaction)
    //`uvm_field_int(timeout_alarm, UVM_ALL_ON)
    `uvm_field_int(bit_readout_counter, UVM_NOCOMPARE) // this field will not be compared when the compare() method is called to compare transactions
    `uvm_field_int(config_bit_counter, UVM_ALL_ON)     // https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1c/html/files/macros/uvm_object_defines-svh.html#Field_Macros
    `uvm_field_int(superpixel_segment_counter, UVM_ALL_ON)
    `uvm_field_int(superpixel_in_segment_counter, UVM_ALL_ON)
    `uvm_field_int(column_counter, UVM_ALL_ON)
    `uvm_field_int(dataout_bit, UVM_ALL_ON)
    /*
    `uvm_field_int(all_zeros, UVM_ALL_ON)
    `uvm_field_int(tpEnableDigital, UVM_ALL_ON)
    `uvm_field_int(mask0, UVM_ALL_ON)
    `uvm_field_int(mask1, UVM_ALL_ON)
    `uvm_field_int(mask2, UVM_ALL_ON)
    `uvm_field_int(mask3, UVM_ALL_ON)
    `uvm_field_int(mask4, UVM_ALL_ON)
    `uvm_field_int(mask5, UVM_ALL_ON)
    `uvm_field_int(mask6, UVM_ALL_ON)
    `uvm_field_int(mask7, UVM_ALL_ON)
    `uvm_field_int(tpEnableAnalog0, UVM_ALL_ON)
    `uvm_field_int(tpEnableAnalog1, UVM_ALL_ON)
    `uvm_field_int(tpEnableAnalog2, UVM_ALL_ON)
    `uvm_field_int(tpEnableAnalog3, UVM_ALL_ON)
    `uvm_field_int(tpEnableAnalog4, UVM_ALL_ON)
    `uvm_field_int(tpEnableAnalog5, UVM_ALL_ON)
    `uvm_field_int(tpEnableAnalog6, UVM_ALL_ON)
    `uvm_field_int(tpEnableAnalog7, UVM_ALL_ON)
    `uvm_field_int(tuningDAC00, UVM_ALL_ON)
    `uvm_field_int(tuningDAC01, UVM_ALL_ON)
    `uvm_field_int(tuningDAC02, UVM_ALL_ON)
    `uvm_field_int(dummy_bit_21_firsthalf, UVM_ALL_ON)
    `uvm_field_int(tuningDAC11, UVM_ALL_ON)
    `uvm_field_int(tuningDAC12, UVM_ALL_ON)
    `uvm_field_int(tuningDAC20, UVM_ALL_ON)
    `uvm_field_int(tuningDAC21, UVM_ALL_ON)
    `uvm_field_int(tuningDAC22, UVM_ALL_ON)
    `uvm_field_int(tuningDAC30, UVM_ALL_ON)
    `uvm_field_int(tuningDAC31, UVM_ALL_ON)
    `uvm_field_int(tuningDAC32, UVM_ALL_ON)
    `uvm_field_int(tuningDAC40, UVM_ALL_ON)
    `uvm_field_int(tuningDAC41, UVM_ALL_ON)
    `uvm_field_int(tuningDAC42, UVM_ALL_ON)
    `uvm_field_int(tuningDAC50, UVM_ALL_ON)
    `uvm_field_int(tuningDAC51, UVM_ALL_ON)
    `uvm_field_int(tuningDAC52, UVM_ALL_ON)
    `uvm_field_int(tuningDAC60, UVM_ALL_ON)
    `uvm_field_int(tuningDAC61, UVM_ALL_ON)
    `uvm_field_int(tuningDAC62, UVM_ALL_ON)
    `uvm_field_int(tuningDAC70, UVM_ALL_ON)
    `uvm_field_int(tuningDAC71, UVM_ALL_ON)
    `uvm_field_int(tuningDAC72, UVM_ALL_ON)
    `uvm_field_int(dummy_bit_20_secondhalf, UVM_ALL_ON)
    `uvm_field_int(dummy_bit_21_secondhalf, UVM_ALL_ON) 
    */
    //`uvm_field_int(hit_flag, UVM_ALL_ON) 
    //`uvm_field_aa_int_int(ToT, UVM_ALL_ON) // Implements the data operations for an associative array of integral types indexed by the int data type
    //`uvm_field_aa_int_int(ToA, UVM_ALL_ON)
    //`uvm_field_aa_int_int(hit_map, UVM_ALL_ON)
    `uvm_field_aa_int_int(column_content, UVM_ALL_ON)
    `uvm_field_int(start_header, UVM_NOCOMPARE)
    `uvm_field_int(stop_header, UVM_NOCOMPARE)
    `uvm_field_int(column_header, UVM_ALL_ON)
				`uvm_field_int(column_content_breakdown, UVM_NOCOMPARE) // the comparison of this field will be performed with a devoted compare function called at dataout_comparator
				`uvm_field_int(column_config_breakdown, UVM_NOCOMPARE) // the comparison of this field will be performed with a devoted compare function called at dataout_comparator
  `uvm_object_utils_end
  // we use the UVM_ALL_ON argument so that all the fields of the transaction
  // can be affected (copied, compared, etc.) by the default methods
              
 
endclass: CLICTD_dataout_transaction





`endif // __CLICTD_dataout_transaction_sv__

