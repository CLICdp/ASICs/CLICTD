// Filename           : CLICTD_pins_transaction.sv
// Author             : Núria Egidos 
// Created on         : 4/6/18
// Last modification  : 4/6/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Transaction

// https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1c/html/files/macros/uvm_object_defines-svh.html

// http://blog.verificationgentleman.com/2015/09/of-copies-and-clones.html
// http://www.sunburst-design.com/papers/CummingsSNUG2014SV_UVM_Transactions.pdf
// https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1c/html/files/macros/uvm_object_defines-svh.html#Field_Macros

// Compare method: // https://www.vmmcentral.org/uvm_vmm_ik/files3/base/uvm_object-svh.html#uvm_object.compare, http://cluelogic.com/2013/01/uvm-tutorial-for-candy-lovers-do-hooks/

`ifndef __CLICTD_pins_transaction_sv__
`define __CLICTD_pins_transaction_sv__

//import uvm_pkg::*;
//`include <uvm_macros.svh>

// Include the parameter definition
//`include "CLICTD_parameters.sv" 


// Transaction to hold one "snapshot" of pins_interface, i.e.
// the value of all of its signals at a certain instant of time
class CLICTD_pins_transaction extends uvm_sequence_item;
  //`uvm_object_utils(CLICTD_pins_transaction) // Register the transaction and its fields to the factory


	//--------- Inputs to the DUT -> randomize their time of arrival/end in the drivers

  logic PWREN_pad;     // Pad to provide the power pulse
  logic TPULSE_pad;   // Pad to provide the external test pulse 
  logic READOUT_pad;	// Pad to activate readout with an external pulse
  logic SHUTTER_pad;  // Pad to provide the shutter signal
  logic RSTN_pad; // This is used by the golden models to know when the reset arrives or is released

  //--------- Outputs and signals probed from the DUT

  logic ENABLE_OUT_pad; // Signal that is high while the serial readout is in process

  // Reset test (check that they're low)
  logic [`COLUMNS-1:0] columnDone;
  logic [`COLUMNS-1:0] dataIn;
  logic [`COLUMNS-1:0] commonToken;

  // Readout control test 
  logic readoutStartExtEn; // Check if the external readout control is enabled or disabled
  logic readoutStart;

  // Test pulse test
  logic testPulseExtEnable; // Signal used to check if the external test pulse is enabled or disabled
  logic testPulse;          // Output of the multiplexer that selects internal/external test pulse
		logic testPulseIntStrobe; // Signal to monitor tpInt bit from internalStrobes register

  // Power pulse test 
  logic powerExtEnable; // Signal used to enable the external power pulse -> check if it's enabled or disabled
  logic powerEnable; // Output of the multiplexer that selects internal/external power pulse. Internal signal (digital periphery) that should change when power is enabled or disabled, either from the slow control or from PWREN_pad
  logic CLK_100_GATED; // Signal that propagates the 100 MHz clock into the matrix only when powerEnable is low (active, clock gating)
  int counter_clk100gated_while_powerEnable_is_low; 

  // Configuration test (check that these match the values configured with the slow control)
  logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] tpEnableDigital;
	// pins_if.mask is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] mask;
	// pins_if.tpEnableAnalog is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] tpEnableAnalog;
	// pins_if.tuningDACx are COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed arrays and each of their positions contains 3 bits
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC0;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC1;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC2;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC3;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC4;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC5;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC6;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC7;


	// Readout, masking, checking the effect of the moment when the discriminator rises (this signal is used to force a signal at the
  // output of the discriminator and thus emulate the arrival of a hit)
	// pins_if.disc is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] disc;
	
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] discTp; // output of the multiplexer that selects between digital test pulse
  // and OR of the disc outputs of each front-end in the superpixel (which are already and-end with the corresponding mask signal, and which come as a result
  // of incoming hits or if analog test pulse is applied)

  // Associative array to store errors resulting from comparing transactions
  field_pinstrans_array_type fields_with_error;


	//--------- Constructor
	function new(string name = "");
		super.new(name);
	endfunction: new


  //--------- Compare the transaction fields that are used in the pins_comparator
  // trans_to_compare is the transaction to which the field of the calling transaction are compared
  // fields_with_error is an associative array indexed by fields_pins_transaction_type in which positions exist
  // if the associated field of the transaction to compare doesn't match the field of the calling transaction
  // (i.e. there are as many positions in the array as fields in which mismatch is detected between the compared transactions)
  function void compare_transactions (CLICTD_pins_transaction trans_to_compare, field_pinstrans_array_type fields_with_error); 
                    int issues;

                    //- - - - Checked in STATUS_RESET

                    // columnDone
                    issues = 0; for (int i = 0; i < `COLUMNS; i++) begin if (columnDone[i] != trans_to_compare.columnDone[i]) issues = 1; end 
                    if (issues > 0) fields_with_error[COLUMNDONE] = COLUMNDONE;

                    // dataIn
                    issues = 0; for (int i = 0; i < `COLUMNS; i++) begin if (dataIn[i] != trans_to_compare.dataIn[i]) issues = 1; end
                    if (issues > 0) fields_with_error[DATAIN] = DATAIN;

                    // commonToken
                    issues = 0; for (int i = 0; i < `COLUMNS; i++) begin if (commonToken[i] != trans_to_compare.commonToken[i]) issues = 1; end
                    if (issues > 0) fields_with_error[COMMONTOKEN] = COMMONTOKEN;

                    // - - - - Checked in STATUS_CONFIG_READOUT_SECOND_HALF

                    // tpEnableDigital
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                if (tpEnableDigital[c][ss][sis] != trans_to_compare.tpEnableDigital[c][ss][sis]) issues = 1;
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TPENABLEDIGITAL] = TPENABLEDIGITAL;

                    // mask
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int p = 0; p < `PIXELS_IN_ONE_SUPERPIXEL; p++) begin
                                    if (mask[c][ss][sis][p] != trans_to_compare.mask[c][ss][sis][p]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[MASK] = MASK;

                    // tpEnableAnalog
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int p = 0; p < `PIXELS_IN_ONE_SUPERPIXEL; p++) begin
                                    if (tpEnableAnalog[c][ss][sis][p] != trans_to_compare.tpEnableAnalog[c][ss][sis][p]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TPENABLEANALOG] = TPENABLEANALOG;

                    // tuningDAC0
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int i = 0; i < 3; i++) begin
                                    if (tuningDAC0[c][ss][sis][i] != trans_to_compare.tuningDAC0[c][ss][sis][i]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TUNINGDAC0] = TUNINGDAC0;
  
                    // tuningDAC1
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int i = 0; i < 3; i++) begin
                                    if (tuningDAC1[c][ss][sis][i] != trans_to_compare.tuningDAC1[c][ss][sis][i]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TUNINGDAC1] = TUNINGDAC1;

                    // tuningDAC2
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int i = 0; i < 3; i++) begin
                                    if (tuningDAC2[c][ss][sis][i] != trans_to_compare.tuningDAC2[c][ss][sis][i]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TUNINGDAC2] = TUNINGDAC2;

                    // tuningDAC3
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int i = 0; i < 3; i++) begin
                                    if (tuningDAC3[c][ss][sis][i] != trans_to_compare.tuningDAC3[c][ss][sis][i]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TUNINGDAC3] = TUNINGDAC3;

                    // tuningDAC4
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int i = 0; i < 3; i++) begin
                                    if (tuningDAC4[c][ss][sis][i] != trans_to_compare.tuningDAC4[c][ss][sis][i]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TUNINGDAC4] = TUNINGDAC4;
 
                    // tuningDAC5
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int i = 0; i < 3; i++) begin
                                    if (tuningDAC5[c][ss][sis][i] != trans_to_compare.tuningDAC5[c][ss][sis][i]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TUNINGDAC5] = TUNINGDAC5;

                    // tuningDAC6
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int i = 0; i < 3; i++) begin
                                    if (tuningDAC6[c][ss][sis][i] != trans_to_compare.tuningDAC6[c][ss][sis][i]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TUNINGDAC6] = TUNINGDAC6;

                    // tuningDAC7
                    issues = 0; 
                    for (int c = 0; c < `COLUMNS; c++) begin
                        for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                            for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                                for (int i = 0; i < 3; i++) begin
                                    if (tuningDAC7[c][ss][sis][i] != trans_to_compare.tuningDAC7[c][ss][sis][i]) issues = 1;
                                end
                            end
                        end
                     end
                     if (issues > 0) fields_with_error[TUNINGDAC7] = TUNINGDAC7;
                    

   endfunction: compare_transactions


  //--------- Implementation of the do_compare function, called by the compare() method
  // The do_compare() method is called by the compare() method. The do_compare() is used to 
  // compare each property of the transaction object; it returns 1 if the comparison succeeds, 
  // and returns 0 if the comparison fails. Note that we have to cast a uvm_object (rhs) to a 
  // transaction of the present kind in order to access the properties to compare. 
  // We must call the super.do_compare() to compare the properties of the super class. 
  // The uvm_comparer argument provides a policy object for doing comparisons, but we do not use it.
  // http://cluelogic.com/2013/01/uvm-tutorial-for-candy-lovers-do-hooks/


  // Note: why to explicitly call compare() instead of just using if(transaction1 == transaction2)?
  // Using == instead of compare, we may be comparing the references of the transactions, but not 
  // all of the comparable fields (those that haven't been disabled with UVM_NOCOMPARE flag), and 
  // thus we may not be comparing all the fields or we may not get the expected result. It's better to 
  // implement do_compare in order to make sure that all the fields to be checked are compared and that
  // the results is calculated as we want.
  virtual function bit do_compare( uvm_object rhs, uvm_comparer comparer );
          CLICTD_pins_transaction that;
          bit issues = 0;

          if ( ! $cast( that, rhs ) ) return 0;



          //- - - - Checked in STATUS_RESET

          // columnDone
          for (int i = 0; i < `COLUMNS; i++) begin 
              if (this.columnDone[i] != that.columnDone[i]) begin
                 issues = 1; 
                 fields_with_error[COLUMNDONE] = COLUMNDONE;
                 break;
              end
          end 

          // dataIn
          for (int i = 0; i < `COLUMNS; i++) begin 
              if (this.dataIn[i] != that.dataIn[i]) begin
                 issues = 1; 
                 fields_with_error[DATAIN] = DATAIN;
                 break;
              end
          end 

          // commonToken
          for (int i = 0; i < `COLUMNS; i++) begin 
              if (this.commonToken[i] != that.commonToken[i]) begin
                 issues = 1; 
                 fields_with_error[COMMONTOKEN] = COMMONTOKEN;
                 break;
              end
          end 

          
          // - - - - Checked in STATUS_CONFIG_READOUT_SECOND_HALF

          // tpEnableDigital
          for (int c = 0; c < `COLUMNS; c++) begin
              for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                  for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                       if (this.tpEnableDigital[c][ss][sis] != that.tpEnableDigital[c][ss][sis]) begin
                           issues = 1;
                           fields_with_error[TPENABLEDIGITAL] = TPENABLEDIGITAL;
                           break;
                       end
                   end
              end
           end


           // mask
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                   for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                       for (int p = 0; p < `PIXELS_IN_ONE_SUPERPIXEL; p++) begin
                           if (this.mask[c][ss][sis][p] != that.mask[c][ss][sis][p]) begin
                              issues = 1;
                              fields_with_error[MASK] = MASK;
                              break;
                           end
                       end
                   end
               end
           end


           // tpEnableAnalog
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                   for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                       for (int p = 0; p < `PIXELS_IN_ONE_SUPERPIXEL; p++) begin
                            if (this.tpEnableAnalog[c][ss][sis][p] != that.tpEnableAnalog[c][ss][sis][p]) begin
                               issues = 1;
                               fields_with_error[TPENABLEANALOG] = TPENABLEANALOG;
                               break;
                            end
                       end
                   end
               end
           end

           // tuningDAC0
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                   for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                        for (int i = 0; i < 3; i++) begin
                            if (this.tuningDAC0[c][ss][sis][i] != that.tuningDAC0[c][ss][sis][i]) begin
                               issues = 1;
                               fields_with_error[TUNINGDAC0] = TUNINGDAC0;
                               break;
                            end
                        end
                   end
               end
           end

  
           // tuningDAC1
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                        for (int i = 0; i < 3; i++) begin
                             if (this.tuningDAC1[c][ss][sis][i] != that.tuningDAC1[c][ss][sis][i]) begin
                                issues = 1;
                                fields_with_error[TUNINGDAC1] = TUNINGDAC1;
                                break;
                             end
                        end
                    end
               end
           end


           // tuningDAC2
           for (int c = 0; c < `COLUMNS; c++) begin
                for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                        for (int i = 0; i < 3; i++) begin
                             if (this.tuningDAC2[c][ss][sis][i] != that.tuningDAC2[c][ss][sis][i]) begin
                                 issues = 1;
                                 fields_with_error[TUNINGDAC2] = TUNINGDAC2; 
                                 break;
                             end
                        end
                    end
                end
           end


           // tuningDAC3
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                   for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                        for (int i = 0; i < 3; i++) begin
                             if (this.tuningDAC3[c][ss][sis][i] != that.tuningDAC3[c][ss][sis][i]) begin
                                 issues = 1;
                                 fields_with_error[TUNINGDAC3] = TUNINGDAC3;
                                 break;
                             end
                        end
                   end
               end
           end


           // tuningDAC4
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                         for (int i = 0; i < 3; i++) begin
                              if (this.tuningDAC4[c][ss][sis][i] != that.tuningDAC4[c][ss][sis][i]) begin 
                                  issues = 1;
                                  fields_with_error[TUNINGDAC4] = TUNINGDAC4;
                                  break;
                              end
                         end
                    end
               end
           end

 
           // tuningDAC5
           for (int c = 0; c < `COLUMNS; c++) begin
                for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                         for (int i = 0; i < 3; i++) begin
                              if (this.tuningDAC5[c][ss][sis][i] != that.tuningDAC5[c][ss][sis][i]) begin 
                                 issues = 1; 
													       fields_with_error[TUNINGDAC5] = TUNINGDAC5;
                                 break; 
                              end
                         end
                     end
                end
           end


           // tuningDAC6
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                   for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                        for (int i = 0; i < 3; i++) begin
                            if (this.tuningDAC6[c][ss][sis][i] != that.tuningDAC6[c][ss][sis][i]) begin
                                issues = 1;
                                fields_with_error[TUNINGDAC6] = TUNINGDAC6;
                                break;
                            end
                        end
                   end
               end
           end


           // tuningDAC7
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                        for (int i = 0; i < 3; i++) begin
                             if (this.tuningDAC7[c][ss][sis][i] != that.tuningDAC7[c][ss][sis][i]) begin
                                issues = 1;
                                fields_with_error[TUNINGDAC7] = TUNINGDAC7;
                                break;
                             end
                        end
                    end
               end
           end

           // disc
											for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                        for (int i = 0; i < `PIXELS_IN_ONE_SUPERPIXEL; i++) begin
                             if (this.disc[c][ss][sis][i] != that.disc[c][ss][sis][i]) begin
                                issues = 1;
                                fields_with_error[DISC] = DISC;
                                break;
                             end
                        end
                    end
               end
           end
											
											


         // - - - - Checked in STATUS_POWERPULSE_EXTERNAL_WORKING, STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS, 
         // STATUS_POWERPULSE_INTERNAL_WORKING, STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS

         if (this.powerEnable != that.powerEnable) begin 
            issues = 1;
            fields_with_error[POWERENABLE] = POWERENABLE;
         end
          
         if (this.counter_clk100gated_while_powerEnable_is_low == 0 && this.powerEnable == 1'b1) begin // the counter is evaluated when powerEnable is disabled
            // Both transactions that are compared in this case will have the same value of the counter; we are only interested in knowing if 
            // it's larger than 0, i.e. if CLK_100_GATED has been propagated while powerEnable was low
            issues = 1;
            fields_with_error[COUNTER_CLK100GATED] = COUNTER_CLK100GATED;
         end
									
									//  - - - - Checked in STATUS_TESTPULSE_EXTERNAL_WORKING, STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS, 
         // STATUS_TESTPULSE_INTERNAL_WORKING, STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS
									if (this.testPulseIntStrobe != that.testPulseIntStrobe) begin 
            issues = 1;
            fields_with_error[TESTPULSEINTSTROBE] = TESTPULSEINTSTROBE;
         end
									
									if (this.TPULSE_pad != that.TPULSE_pad) begin 
            issues = 1;
            fields_with_error[TPULSE_PAD] = TPULSE_PAD;
         end
									
									// discTp
											for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                             if (this.discTp[c][ss][sis] != that.discTp[c][ss][sis]) begin
                                issues = 1;
                                fields_with_error[DISCTP] = DISCTP;
                                break;
                             end
                    end
               end
           end
 
          return ( super.do_compare( rhs, comparer ) && !issues );

  endfunction: do_compare
		
		
		// This function is implicitly called when copy() or clone() are used
		// https://verificationacademy.com/cookbook/transaction/methods
		function void do_copy(uvm_object rhs);
            CLICTD_pins_transaction that;
 
            //if(!$cast(that, rhs)) begin return;
												assert($cast(that, rhs));
 
            super.do_copy(rhs); // Chain the copy with parent classes
												
												this.PWREN_pad                                    = that.PWREN_pad;     
            this.TPULSE_pad                                   = that.TPULSE_pad;   
            this.READOUT_pad                                  = that.READOUT_pad;	
            this.SHUTTER_pad                                  = that.SHUTTER_pad;  
            this.RSTN_pad                                     = that.RSTN_pad; 
            this.ENABLE_OUT_pad                               = that.ENABLE_OUT_pad; 
            this.columnDone                                   = that.columnDone;
            this.dataIn                                       = that.dataIn;
            this.commonToken                                  = that.commonToken;
            this.readoutStartExtEn                            = that.readoutStartExtEn; 
            this.readoutStart                                 = that.readoutStart;
            this.testPulseExtEnable                           = that.testPulseExtEnable; 
            this.testPulse                                    = that.testPulse; 
												this.testPulseIntStrobe                           = that.testPulseIntStrobe;         
            this.powerExtEnable                               = that.powerExtEnable; 
            this.powerEnable                                  = that.powerEnable; 
            this.CLK_100_GATED                                = that.CLK_100_GATED; 
            this.counter_clk100gated_while_powerEnable_is_low = that.counter_clk100gated_while_powerEnable_is_low; 
            this.tpEnableDigital                              = that.tpEnableDigital;
	           this.mask                                         = that.mask;
	           this.tpEnableAnalog                               = that.tpEnableAnalog;
	           this.tuningDAC0                                   = that.tuningDAC0;
           	this.tuningDAC1                                   = that.tuningDAC1;
	           this.tuningDAC2                                   = that.tuningDAC2;
	           this.tuningDAC3                                   = that.tuningDAC3;
	           this.tuningDAC4                                   = that.tuningDAC4;
	           this.tuningDAC5                                   = that.tuningDAC5;
	           this.tuningDAC6                                   = that.tuningDAC6;
	           this.tuningDAC7                                   = that.tuningDAC7;
            this.disc                                         = that.disc;
												this.discTp                                       = that.discTp;
            this.fields_with_error                            = that.fields_with_error;
												
           
  endfunction: do_copy


  `uvm_object_utils_begin(CLICTD_pins_transaction)
    `uvm_field_int(PWREN_pad, UVM_ALL_ON)
    `uvm_field_int(TPULSE_pad, UVM_ALL_ON)
    `uvm_field_int(READOUT_pad, UVM_ALL_ON)
    `uvm_field_int(SHUTTER_pad, UVM_ALL_ON)
    `uvm_field_int(RSTN_pad, UVM_ALL_ON)
    `uvm_field_int(ENABLE_OUT_pad, UVM_ALL_ON)
    `uvm_field_int(columnDone, UVM_ALL_ON)
    `uvm_field_int(dataIn, UVM_ALL_ON)
    `uvm_field_int(commonToken, UVM_ALL_ON)
    `uvm_field_int(readoutStartExtEn, UVM_ALL_ON)
    `uvm_field_int(readoutStart, UVM_ALL_ON)
    `uvm_field_int(testPulseExtEnable, UVM_ALL_ON)
    `uvm_field_int(testPulse, UVM_ALL_ON)
				`uvm_field_int(testPulseIntStrobe, UVM_ALL_ON)
    `uvm_field_int(powerExtEnable, UVM_ALL_ON)
    `uvm_field_int(powerEnable, UVM_ALL_ON)
    `uvm_field_int(counter_clk100gated_while_powerEnable_is_low, UVM_ALL_ON)
    `uvm_field_int(CLK_100_GATED,UVM_ALL_ON)
    `uvm_field_int(mask, UVM_ALL_ON)
    `uvm_field_int(tpEnableAnalog, UVM_ALL_ON)
    `uvm_field_int(tuningDAC0, UVM_ALL_ON)
    `uvm_field_int(tuningDAC1, UVM_ALL_ON)
    `uvm_field_int(tuningDAC2, UVM_ALL_ON)
    `uvm_field_int(tuningDAC3, UVM_ALL_ON)
    `uvm_field_int(tuningDAC4, UVM_ALL_ON)
    `uvm_field_int(tuningDAC5, UVM_ALL_ON)
    `uvm_field_int(tuningDAC6, UVM_ALL_ON)
    `uvm_field_int(tuningDAC7, UVM_ALL_ON)
    `uvm_field_int(disc, UVM_ALL_ON)
	
  `uvm_object_utils_end
  // we use the UVM_ALL_ON argument so that all the fields of the transaction
  // can be affected (copied, compared, etc.) by the default methods

endclass: CLICTD_pins_transaction





`endif // __CLICTD_pins_transaction_sv__

