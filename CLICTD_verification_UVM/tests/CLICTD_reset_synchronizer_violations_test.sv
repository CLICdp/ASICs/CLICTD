// Filename           : CLICTD_reset_synchronizer_violations_test.sv
// Author             : N�ria Egidos 
// Created on         : 7/12/18
// Last modification  : 7/12/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Force timing violations at the reset synchronizer by sending several reset pulses in a row, releasing them close to a 
//                      rising edge of the clock
// Derived from       : top_test_external_internal_hits_4lanes.sv by Elia Conti for VEPIX53
//                      and  analysis_master_agent.sv
//                      http://cluelogic.com/2012/01/uvm-tutorial-for-candy-lovers-virtual-sequence/


`ifndef __CLICTD_reset_synchronizer_violations_test_sv__
`define __CLICTD_reset_synchronizer_violations_test_sv__


class CLICTD_reset_synchronizer_violations_test extends CLICTD_test;

  // Register the component into the UVM factory
	`uvm_component_utils(CLICTD_reset_synchronizer_violations_test);


    // Constructor
		function new(string name, uvm_component parent);
			super.new(name, parent);
      testname = `RESET_SYNCHRONIZER_VIOLATIONS_TEST;
		endfunction: new


    // Build phase: build the hierarchy
		virtual function void build_phase(uvm_phase phase);
				super.build_phase(phase);

        //reporting_enable_obj.enable_reporting_matrixconfigonehalfseq = `YES; // Reporting in CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence
        uvm_config_db#(ReportingEnable)::set( .cntxt(this), .inst_name("*"), .field_name("reporting_enable_obj"), .value(reporting_enable_obj) );


				// Store the configuration objects into the configuration database, where they can be accessed 
		    // from all the components that the test contains
        // inst_name is the name stored in the database corresponding to the instance that will be able to retrieve this parameter
		    uvm_config_db#( CLICTD_environment_config )::set( .cntxt( this ), .inst_name( "env" ), .field_name( "env_config" ), .value( env_config ) );
        // Create the enviroment 
				env = CLICTD_environment::type_id::create(.name("env"), .parent(this));
       
        // how do the uvm_config_db::set cntxt and inst_name arguments work?
        // in this case, cntxt = this indicates the top of the uvm hierarchy, i.e. uvm_test_top
        // if then inst_name = env, it means that this field will be accessible by all instances immediately under uvm_test_top/env;
        // to be accessible also for lowel levels in the hierarchy, we need to specify it like env.pins_agent, for instance.   
        // Note that the names we use to specify the hierarchy are the instance names, not the name of the classes (ie. env and not
        // CLICTD_environment). If we use "*" as inst_name, it means that all instances in the hierarchy can access the field.
       

		endfunction: build_phase

    

endclass: CLICTD_reset_synchronizer_violations_test


`endif // __CLICTD_reset_synchronizer_violations_test_sv__
