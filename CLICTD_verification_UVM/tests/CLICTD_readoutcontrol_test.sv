// Filename           : CLICTD_readoutcontrol_test.sv
// Author             : Núria Egidos 
// Created on         : 24/9/18
// Last modification  : 24/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Readout control test
//                      Apply reset
//                      The chip is by default configured to readout from the slow control.
//                      Readout a second time (all zeros) by applying a slow control command.
//                      Try to readout a third time by applying a pulse at READOUT_pad -> nothing should happen
//                      Configure the chip to readout by applying a pulse at READOUT_pad. 
//                      Try to readout a third time activating from the slow control -> nothing should happen
//                      Readout a third time (all zeros) activating from the external test pulse.
// Derived from       : top_test_external_internal_hits_4lanes.sv by Elia Conti for VEPIX53
//                      and  analysis_master_agent.sv
//                      http://cluelogic.com/2012/01/uvm-tutorial-for-candy-lovers-virtual-sequence/


`ifndef __CLICTD_readoutcontrol_test_sv__
`define __CLICTD_readoutcontrol_test_sv__


class CLICTD_readoutcontrol_test extends CLICTD_test;

  // Register the component into the UVM factory
	`uvm_component_utils(CLICTD_readoutcontrol_test);


    // Constructor
		function new(string name, uvm_component parent);
			super.new(name, parent);
      testname = `READOUTCONTROL_TEST;
		endfunction: new


    // Build phase: build the hierarchy
		virtual function void build_phase(uvm_phase phase);
				super.build_phase(phase);

        //reporting_enable_obj.enable_reporting_matrixconfigonehalfseq = `YES; // Reporting in CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence
        uvm_config_db#(ReportingEnable)::set( .cntxt(this), .inst_name("*"), .field_name("reporting_enable_obj"), .value(reporting_enable_obj) );


				// Store the configuration objects into the configuration database, where they can be accessed 
		    // from all the components that the test contains
        // inst_name is the name stored in the database corresponding to the instance that will be able to retrieve this parameter
		    uvm_config_db#( CLICTD_environment_config )::set( .cntxt( this ), .inst_name( "env" ), .field_name( "env_config" ), .value( env_config ) );
        // Create the enviroment 
				env = CLICTD_environment::type_id::create(.name("env"), .parent(this));
       
        // how do the uvm_config_db::set cntxt and inst_name arguments work?
        // in this case, cntxt = this indicates the top of the uvm hierarchy, i.e. uvm_test_top
        // if then inst_name = env, it means that this field will be accessible by all instances immediately under uvm_test_top/env;
        // to be accessible also for lowel levels in the hierarchy, we need to specify it like env.pins_agent, for instance.   
        // Note that the names we use to specify the hierarchy are the instance names, not the name of the classes (ie. env and not
        // CLICTD_environment). If we use "*" as inst_name, it means that all instances in the hierarchy can access the field.
       

		endfunction: build_phase

    /*
    // Report phase, to summarize results
    function void report_phase( uvm_phase phase );
       

           // chip_status = STATUS_RESET
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET)           && env.pins_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0 &&
                env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET)        && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0 &&
                env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0
               ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected default value after reset", UVM_MEDIUM)
           end
           else if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && 
                     env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && 
                     env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) 
               )  begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors pins_interface, %d errors readout_interface, %d errors slowcontrol_interface after reset", 
                         env.pins_scoreboard.comparator.number_discrepancies[STATUS_RESET], env.dataout_scoreboard.comparator.number_discrepancies[STATUS_RESET],
                         env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_RESET]),  UVM_MEDIUM);
           end

           // chip_status = STATUS_SECOND_READOUT_ALL_ZEROS
           if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_SECOND_READOUT_ALL_ZEROS) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_SECOND_READOUT_ALL_ZEROS] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected default value after second readout", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_SECOND_READOUT_ALL_ZEROS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after second readout", env.dataout_scoreboard.comparator.number_discrepancies[STATUS_SECOND_READOUT_ALL_ZEROS]),  UVM_MEDIUM)
           end

           // chip_status = STATUS_THIRD_READOUT_NOTHING_HAPPENS
           if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_THIRD_READOUT_NOTHING_HAPPENS) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_THIRD_READOUT_NOTHING_HAPPENS] > 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "Timeout for start readout detected after third readout", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_THIRD_READOUT_NOTHING_HAPPENS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", "Timeout for start readout NOT detected after third readout",  UVM_MEDIUM)
           end
  
		       // chip_status = STATUS_FOURTH_READOUT_ALL_ZEROS
           if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_FOURTH_READOUT_ALL_ZEROS) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_FOURTH_READOUT_ALL_ZEROS] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected default value after fourth readout", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_FOURTH_READOUT_ALL_ZEROS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after fourth readout", env.dataout_scoreboard.comparator.number_discrepancies[STATUS_FOURTH_READOUT_ALL_ZEROS]),  UVM_MEDIUM)
           end


		       // chip_status = STATUS_FIFTH_READOUT_NOTHING_HAPPENS
           if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_FIFTH_READOUT_NOTHING_HAPPENS) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_FIFTH_READOUT_NOTHING_HAPPENS] > 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "Timeout for start readout detected after fifth readout", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_FIFTH_READOUT_NOTHING_HAPPENS) )  begin
               `uvm_info(":( UVM TEST PROGRESS :(", "Timeout for start readout NOT detected after fifth readout",  UVM_MEDIUM)
           end
         
         // PENDING: save to file
    endfunction: report_phase
   */

endclass: CLICTD_readoutcontrol_test


`endif // __CLICTD_readoutcontrol_test_sv__
