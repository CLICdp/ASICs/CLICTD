// Filename           : CLICTD_test_pkg.sv
// Author             : Núria Egidos 
// Created on         : 1/6/18
// Last modification  : 21/6/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Test package that contains all of the test and the parameters used
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/

// On scoreboards: 
// http://www.sunburst-design.com/papers/CummingsSNUG2013SV_UVM_Scoreboards.pdf
// http://www.chipverify.com/uvm/tlm-analysis-port

// On debugging:
// http://forums.accellera.org/topic/1352-sequence-is-not-running-make-sure-sequencer-name-correct/
// https://verificationacademy.com/forums/uvm/how-can-i-see-content-uvmconfigdb

`ifndef __CLICTD_test_pkg_sv__
`define __CLICTD_test_pkg_sv__

//`include "CLICTD_parameters.sv" 
//`include "CLICTD_typedefs_pkg.sv"



package CLICTD_test_pkg;

	import uvm_pkg::*;
	`include <uvm_macros.svh>



  //import CLICTD_typedefs_pkg::*;
  typedef int default_values_registers_slowcontrol_type [int];
  typedef int addresses_registers_slowcontrol_type [int];
  //typedef int [`NUMBER_SLOWCONTROL_REGISTERS-1:0][1:0] default_values_registers_slowcontrol_type;
  //typedef bit [`NUMBER_BITS_READOUT_COMPRESSION_DISABLED-1:0] array_bits_nocompression_type;
  //typedef bit  array_bits_nocompression_type [`NUMBER_BITS_READOUT_COMPRESSION_DISABLED-1:0];


  typedef int field_of_column_content_type [int];

  typedef enum {DIGITAL_TEST_PULSE, FORCE_DISC_PULSE} digitalpulse_ORdisc_type;

  
  // Readout modes
  typedef enum {READOUT_NOMINAL, READOUT_LONGTOA, READOUT_PHOTONCOUNTING} readout_modes_type;

  // Stages of readout
  typedef enum {STATUS_IDLE, STATUS_START_HEADER, STATUS_COLUMN0_HEADER, STATUS_CONTENT0_AND_OTHERCOLUMNS, STATUS_DETECT_STOP_READOUT} readout_stages_type; 


  // Depending on the chip status, dataout_model will change according to the expected content of the serial output
  typedef enum {
                STATUS_RESET, STATUS_FIRST_READOUT_UNDEFINED, STATUS_SECOND_READOUT_ALL_ZEROS, STATUS_THIRD_READOUT_NOTHING_HAPPENS,STATUS_FOURTH_READOUT_ALL_ZEROS,STATUS_FIFTH_READOUT_NOTHING_HAPPENS, 
                STATUS_MATRIX_CONFIG_FIRST_HALF, STATUS_CONFIG_READOUT_FIRST_HALF, STATUS_MATRIX_CONFIG_SECOND_HALF, STATUS_CONFIG_READOUT_SECOND_HALF, 
                STATUS_WRITE_ALL_SLOWCONTROL_REGISTERS, STATUS_READ_ALL_SLOWCONTROL_REGISTERS,
                STATUS_POWERPULSE_EXTERNAL_WORKING, STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS, STATUS_POWERPULSE_INTERNAL_WORKING, STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS,
																STATUS_TESTPULSE_EXTERNAL_WORKING, STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS, STATUS_TESTPULSE_INTERNAL_WORKING, STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS,
                STATUS_POWERPULSE_WAIT_LOGIC_RECOVER,
																STATUS_CONFIG_ACQUISITION_MODE, STATUS_FORCE_HIT_SHORTTOT, STATUS_FORCE_HIT_LONGTOT,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3,
																STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0,
																STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1,
																STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2,
																STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3,
																STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0,
																STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1,
																STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2,
																STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0,
																STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0,
																STATUS_CONFIGURE_ENABLE_COMPRESSION, STATUS_FIRST_READOUT_ENABLE_COMPRESSION, STATUS_SECOND_READOUT_ENABLE_COMPRESSION
               } chip_status_type;


  // For CLICTD_pins_transaction compare_transactions and do_compare
  typedef enum {PWRE_PAD, TPULSE_PAD, READOUT_PAD, SHUTTER_PAD, RSTN_PAD, ENABLE_OUT_PAD, COLUMNDONE, DATAIN, COMMONTOKEN, READOUTSTARTEXTEN, READOUTSTART, TESTPULSEEXTENABLE, TESTPULSE, POWEREXTENABLE, POWERENABLE, COUNTER_CLK100GATED, TPENABLEDIGITAL, MASK, TPENABLEANALOG, TUNINGDAC0, TUNINGDAC1, TUNINGDAC2, TUNINGDAC3, TUNINGDAC4, TUNINGDAC5, TUNINGDAC6, TUNINGDAC7, DISC, DISCTP, TESTPULSEINTSTROBE} fields_pins_transaction_type;
  typedef fields_pins_transaction_type field_pinstrans_array_type [fields_pins_transaction_type];

  // For CLICTD_dataout_transaction compare_transactions and do_compare
  typedef enum {DATAOUT, COLUMN_CONTENT, SIZE_COLUMNCONTENT, COLUMN_COUNTER} fields_dataout_transaction_type;
  typedef fields_dataout_transaction_type field_dataouttrans_array_type [fields_dataout_transaction_type];

  // For CLICTD_slowcontrol_transaction do_compare
  typedef enum {REGISTER_ADDRESS, REGISTER_CONTENT} fields_slowcontrol_transaction_type;
  typedef fields_slowcontrol_transaction_type field_slowcontroltrans_array_type [fields_slowcontrol_transaction_type];


  // How to configure matrix (tpEnableDigital, mask, tpEnableAnalog...) for CLICTD_matrixconfig_sequence
  typedef enum {FIXED_DEBUG, FIXED_HITPATTERN, RANDOM_HITPATTERN, RANDOM} matrix_config_fields_type;

  // This is used in generate_expected_acquisition_readout.sv
  typedef logic [7:0] hitmap_type;


  // Auxiliary classes
  `include "ChipStatusInfo.sv"; 
  `include "CLICTDevents.sv";
  `include "BitsMonitorSlowcontrolReg.sv";
  `include "ReportingEnable.sv";
  `include "FieldsConfigureMatrix.sv";
		`include "FieldsConfigureMatrix_for_coverage.sv";
  `include "DiscriminatorBits.sv";
		`include "FieldsTimeMeasurements.sv";
		`include "FieldsTimeMeasurements_for_coverage.sv";
		`include "CLICTD_acquireddata.sv";
		//`include "CLICTD_acquireddata_pkg.sv";
  //import CLICTD_acquireddata_pkg::*;
		`include "CLICTD_configureddata.sv";
		`include "convert_from_LFSR_to_binary.sv";
		`include "build_expected_measurements_from_matrixconfig_obj.sv";
		`include "build_expected_measurements_from_matrixconfig_obj_for_coverage.sv";
  
  // Components and objects of the UVM framework

  

  // Transactions
  `include "CLICTD_pins_transaction.sv";    
  `include "CLICTD_dataout_transaction.sv";
  `include "CLICTD_slowcontrol_transaction.sv";
		`include "assert_isunkown_functions.sv"; 
   
  // Component that filters the transactions coming from the regslowcontrol_agent;
  // it sends to the regslowcontrol_scoreboard only those corresponding to a read 
  // operation of the slow control, so as to monitor the content stored in the slow
  // control registers
  `include "CLICTD_readpass_transaction_filter.sv";

  // Component that emulates the values stored into the matrix via configuration and 
  // from the arrival of hits
  `include "CLICTD_matrix_model.sv";

  // Configuration objects
  `include "CLICTD_agent_config.sv";
  `include "CLICTD_environment_config.sv";
   
  // Monitors
  `include "CLICTD_pins_monitor.sv";    
  `include "CLICTD_dataout_monitor.sv";  
  //`include "CLICTD_slowcontrol_monitor.sv";        
  `include "CLICTD_regslowcontrol_monitor.sv"; 
   

  // Drivers
  `include "CLICTD_slowcontrol_driver.sv"; 
  `include "CLICTD_pins_driver.sv"; 

  // Sequencers
  typedef uvm_sequencer#(CLICTD_slowcontrol_transaction) CLICTD_slowcontrol_sequencer;
  typedef uvm_sequencer#(CLICTD_pins_transaction) CLICTD_pins_sequencer;

  // Sequences
  `include "CLICTD_slowcontroltrans_base_sequence.sv";
  `include "CLICTD_readAllSlowcontrolReg_sequence.sv";
  `include "CLICTD_configure_external_readout_sequence.sv";
  `include "CLICTD_startstop_external_readout_sequence.sv";
  `include "CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence.sv";
  `include "CLICTD_configure_internal_readout_sequence.sv";
  `include "CLICTD_startstop_internal_readout_sequence.sv";
  `include "CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence.sv";
  `include "CLICTD_apply_reset_pulse_sequence.sv";
  `include "CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence.sv";
  `include "CLICTD_matrixconfig_virtual_sequence.sv";
  `include "CLICTD_testpulse_activatefrom_external_sequence.sv";
  `include "CLICTD_configure_acquisition_mode_sequence.sv";
		`include "CLICTD_apply_hits_external_sequence.sv";
		`include "CLICTD_apply_hits_external_sequence_for_coverage.sv";
		`include "CLICTD_configure_external_testpulse_sequence.sv";
		`include "CLICTD_configure_internal_testpulse_sequence.sv";
  `include "CLICTD_readout_test_virtual_sequence.sv";
		`include "CLICTD_readout_test_virtual_sequence_for_coverage.sv";
  `include "CLICTD_readoutcontrol_test_virtual_sequence.sv";
  `include "CLICTD_wrregisters_test_sequence.sv";
  `include "CLICTD_configure_external_powerpulse_sequence.sv";
  `include "CLICTD_startstop_external_powerpulse_sequence.sv";
  `include "CLICTD_configure_internal_powerpulse_sequence.sv";
  `include "CLICTD_startstop_internal_powerpulse_sequence.sv";
  `include "CLICTD_force_initialvalue_pwrenpad_sequence.sv";
  //`include "CLICTD_wait_10us_dummy_sequence.sv";
  `include "CLICTD_powercontrol_test_virtual_sequence.sv";
		`include "CLICTD_testpulsecontrol_test_virtual_sequence.sv";
		`include "CLICTD_reset_synchronizer_violations_sequence.sv"
		`include "CLICTD_apply_readoutpad_pulses_sequence.sv"
		`include "CLICTD_readoutstart_synchronizer_violations_virtual_sequence.sv"
  `include "CLICTD_virtual_sequence.sv";
  

   


  // Functional coverage collectors
  //`include "CLICTD_funccoverage_collector.sv";   

  // Agents
  `include "CLICTD_agent.sv";                    
  `include "CLICTD_pins_agent.sv";
  `include "CLICTD_dataout_agent.sv";
  `include "CLICTD_regslowcontrol_agent.sv";
  `include "CLICTD_slowcontrol_agent.sv"; 

  // Golden models
  `uvm_analysis_imp_decl(_matrix)
  `uvm_analysis_imp_decl(_in)
  `uvm_analysis_imp_decl(_out)
  `include "CLICTD_pins_model.sv";
  `include "CLICTD_dataout_model.sv";
  `include "CLICTD_regslowcontrol_model.sv";

  // Comparators
  `include "CLICTD_comparator.sv";
  `include "CLICTD_pins_comparator.sv";
  `include "CLICTD_dataout_comparator.sv";
  `include "CLICTD_regslowcontrol_comparator.sv";

  // Scoreboards
  `include "CLICTD_pins_scoreboard.sv";
  `include "CLICTD_dataout_scoreboard.sv"; 
  `include "CLICTD_regslowcontrol_scoreboard.sv";               
  
  // Environment
  `include "CLICTD_environment.sv";               

  // Tests
  `include "CLICTD_test.sv";   
  `include "CLICTD_reset_test.sv";
  `include "CLICTD_configuration_test.sv"; 
  `include "CLICTD_readout_test.sv";  
		`include "CLICTD_readout_test_for_coverage.sv";             
  `include "CLICTD_readoutcontrol_test.sv";  
  `include "CLICTD_wrregisters_test.sv";  
  `include "CLICTD_powercontrol_test.sv"; 
		`include "CLICTD_testpulsecontrol_test.sv"; 
		`include "CLICTD_reset_synchronizer_violations_test.sv"
		`include "CLICTD_readoutstart_synchronizer_violations_test.sv"


endpackage: CLICTD_test_pkg
  
`endif // __CLICTD_test_pkg_sv__

