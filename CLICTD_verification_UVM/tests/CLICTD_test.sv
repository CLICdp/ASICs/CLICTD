// Filename           : CLICTD_test.sv
// Author             : Núria Egidos 
// Created on         : 8/8/18 - on 31/8/18 updated slow control register definitions
// Last modification  : 31/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Base test
// Derived from       : top_test_external_internal_hits_4lanes.sv by Elia Conti for VEPIX53
//                      and  analysis_master_agent.sv
//                      http://cluelogic.com/2012/01/uvm-tutorial-for-candy-lovers-virtual-sequence/


`ifndef __CLICTD_test_sv__
`define __CLICTD_test_sv__


class CLICTD_test extends uvm_test;

  // Register the component into the UVM factory
	`uvm_component_utils(CLICTD_test);

  // Declare instances in the test
  CLICTD_environment env; // Environment
  // In this case, we declare no sequence because the input stimuli is driven 
  // from the testbench top (the reset pulse is applied directly at RSTN_pad)

  // Configuration object for the environment
  // Information for the environment: agents_to_instantiate, scoreboards_to_instantiate, instantiate_crosscoveragecollector
  CLICTD_environment_config env_config;

  // Configuration object(s) for the agent(s)
  // Information for the agent: instantiate_driver, instantiate_functionalcoveragecollector 
  CLICTD_agent_config slowcontrol_agent_config;
  CLICTD_agent_config regslowcontrol_agent_config;
  CLICTD_agent_config dataout_agent_config;
  CLICTD_agent_config pins_agent_config;

  // Declare the virtual interface handles that will be used by all the agents to access the DUT.
  // We will use these instances to retrieve the interface handles from the factory
  virtual CLICTD_slowcontrol_interface slowcontrol_vif;
  virtual CLICTD_readout_interface     readout_vif;
  virtual CLICTD_pins_interface        pins_vif;

  // Name of the test
  string testname;

  ChipStatusInfo chip_status_obj;
  CLICTDevents events_obj;
  BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
  ReportingEnable reporting_enable_obj;
  FieldsConfigureMatrix config_matrix_obj;
		FieldsTimeMeasurements time_measurements_obj;
		FieldsConfigureMatrix_for_coverage config_matrix_obj_for_coverage;
		FieldsTimeMeasurements_for_coverage time_measurements_obj_for_coverage;

  // Associative array containing the default values for the slow control registers, i.e.
  // the value they will have after reset
  //int default_values_registers_slowcontrol [int];
  default_values_registers_slowcontrol_type default_values_registers_slowcontrol;
  addresses_registers_slowcontrol_type addresses_registers_slowcontrol;


  // Array containing the expected bits read out via DATA_OUT_pad when the second
  // readout (before any configuration or acquisition) is performed
  //array_bits_nocompression_type dataout_word_second_readout_all_zeros;

  string scoreboards_to_instantiate [string];

  CLICTD_virtual_sequence virtual_seq;

    // Constructor
		function new(string name, uvm_component parent);
			super.new(name, parent);
      chip_status_obj = new();
      events_obj = new();
      bits_slowcontrol_reg_obj = new();
      reporting_enable_obj = new();
      config_matrix_obj = new();
					 time_measurements_obj = new();
						config_matrix_obj_for_coverage = new();
					 time_measurements_obj_for_coverage = new();
						
						//$display("clictd test 1 - toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
						//		         time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
						//											config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
						//											config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
						//											config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
						//											config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse);
		endfunction: new


    // Build phase: build the hierarchy
		virtual function void build_phase(uvm_phase phase);
				super.build_phase(phase);

        // The testname will be available for all components. It can be retrieved from the 
        // UVM configuratoin database using the label "testname"
        uvm_config_db#(string)::set( .cntxt(this), .inst_name("*"), .field_name("testname"), .value(testname) );

        uvm_config_db#(ChipStatusInfo)::set( .cntxt(this), .inst_name("*"), .field_name("chip_status_obj"), .value(chip_status_obj) );
        //uvm_config_db#(ChipStatusInfo)::set( .cntxt(this), .inst_name(".virtual_seq"), .field_name("chip_status_obj"), .value(chip_status_obj) );

        uvm_config_db#(CLICTDevents)::set( .cntxt(this), .inst_name("*"), .field_name("events_obj"), .value(events_obj) );

        uvm_config_db#(BitsMonitorSlowcontrolReg)::set( .cntxt(this), .inst_name("*"), .field_name("bits_slowcontrol_reg_obj"), .value(bits_slowcontrol_reg_obj) );
								
								// virtual_seq has two randomizable attributes: objects FieldsConfigureMatrix config_matrix_obj and FieldsTimeMeasurements time_measurements_obj
								// Two options were considered to randomize them:
								// 1) Assign the non-randomized objects to the sequence and then randomize the sequence: 
								//    virtual_seq.config_matrix_obj = config_matrix_obj;
								//    virtual_seq.time_measurements_obj = time_measurements_obj;
								//    assert( virtual_seq.randomize() );
								// 2) Randomize the objects and then assign them to the sequence, which is not randomized:
								//    time_measurements_obj.randomize();
      		//    config_matrix_obj.randomize();
								//    virtual_seq.config_matrix_obj = config_matrix_obj;
								//    virtual_seq.time_measurements_obj = time_measurements_obj;
								// For option 1, the value of the fields in the randomizable objects was printed at several spots of the code and randomization was not observed.
								// Option 2 worked properly, so this is the implemented option.
								// Inside the virtual sequence, other sequences are randomized and started. After randomizing these sequences, the config_matrix_obj and time_measurements_obj
								// in the virtual sequence are assigned to the respective objects in these sequences to ensure that they mantain the values in the virtual sequence
      		assert(time_measurements_obj.randomize());
      		assert(config_matrix_obj.randomize());
								// The following lines don't have any effect, because when the object was created, which is when the constraints have effect, matrix_config_fields had the default value
								//if (testname == `READOUT_TEST) config_matrix_obj.matrix_config_fields = RANDOM_HITPATTERN;
								//else config_matrix_obj.matrix_config_fields = FIXED_DEBUG;

        uvm_config_db#(FieldsConfigureMatrix)::set( .cntxt(this), .inst_name("*"), .field_name("config_matrix_obj"), .value(config_matrix_obj) );
								uvm_config_db#(FieldsTimeMeasurements)::set( .cntxt(this), .inst_name("*"), .field_name("time_measurements_obj"), .value(time_measurements_obj) );
								 // The objects are randomized here so that all blocks that use it, both UVM components and sequences/sub-sequences, see the same values from the beginning
									
									
								// In READOUT_TEST_FOR_COVERAGE, there will be both time_measurements_obj_for_coverage and time_measurements_obj, and both config_matrix_obj_for_coverage and config_matrix_obj in the database, 
								// because it was not possible to use set_type_override_by_type for all time_measurements_obj and config_matrix_obj because they're objects, they're not registered to the 
								// factory with create() (https://www.chipverify.com/uvm/using-factory-to-override-items)
        assert(time_measurements_obj_for_coverage.randomize());
      		assert(config_matrix_obj_for_coverage.randomize());
        uvm_config_db#(FieldsConfigureMatrix_for_coverage)::set( .cntxt(this), .inst_name("*"), .field_name("config_matrix_obj_for_coverage"), .value(config_matrix_obj_for_coverage) );
								uvm_config_db#(FieldsTimeMeasurements_for_coverage)::set( .cntxt(this), .inst_name("*"), .field_name("time_measurements_obj_for_coverage"), .value(time_measurements_obj_for_coverage) );

      
         

									/*
        $display("CLICTD_test config_matrix_obj - dummy bit = %b", config_matrix_obj.value_for_dummy_bit);
								for (int c = 0; c < `COLUMNS; c++) begin
													for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						    							for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
															     		if (c == config_matrix_obj.columns_to_pulse[0] || c == config_matrix_obj.columns_to_pulse[1] || c == config_matrix_obj.columns_to_pulse[2] || c == config_matrix_obj.columns_to_pulse[3] || c == config_matrix_obj.columns_to_pulse[4] ||
																				    		c == config_matrix_obj.columns_to_pulse[5] || c == config_matrix_obj.columns_to_pulse[6] || c == config_matrix_obj.columns_to_pulse[7] || c == config_matrix_obj.columns_to_pulse[8] )

																			    			$display("CLICTD_test config_matrix_obj - c = %0d ss = %0d sis = %0d, tpEnableDigital = %b, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						          																c,ss, sis, config_matrix_obj.tpEnableDigital[0][ss][sis], config_matrix_obj.mask[0][ss][sis], config_matrix_obj.tpEnableAnalog[0][ss][sis], config_matrix_obj.tuningDAC0[0][ss][sis], config_matrix_obj.tuningDAC1[0][ss][sis],
																																config_matrix_obj.tuningDAC2[0][ss][sis], config_matrix_obj.tuningDAC3[0][ss][sis], config_matrix_obj.tuningDAC4[0][ss][sis], config_matrix_obj.tuningDAC5[0][ss][sis], config_matrix_obj.tuningDAC6[0][ss][sis],
																																config_matrix_obj.tuningDAC7[0][ss][sis]);
																	end
													end
      		end
        */
 


        // Set the default value for the slow control registers and make it available for 
        // all components. It can be retrieved from the UVM configuratoin database using 
        // the label "default_values_registers_slowcontrol"
								`include "../CLICTD_instantiation_assocarray_defaultNaddr_regs.sv"
								
        
        
        uvm_config_db#(default_values_registers_slowcontrol_type)::set( .cntxt(this), .inst_name("*"), .field_name("default_values_registers_slowcontrol"), .value(default_values_registers_slowcontrol) );


        


				// Create configuration object for the environment
		     
        scoreboards_to_instantiate[`DATAOUT_SCOREBOARD] = `DATAOUT_SCOREBOARD;
        scoreboards_to_instantiate[`PINS_SCOREBOARD] = `PINS_SCOREBOARD;
        scoreboards_to_instantiate[`REGSLOWCONTROL_SCOREBOARD] = `REGSLOWCONTROL_SCOREBOARD;
		  
        env_config = CLICTD_environment_config::type_id::create(.name("env_config"));
        env_config.agents_to_instantiate[`PINS_AGENT] = `PINS_AGENT;
        env_config.agents_to_instantiate[`DATAOUT_AGENT] = `DATAOUT_AGENT;
        env_config.agents_to_instantiate[`REGSLOWCONTROL_AGENT] = `REGSLOWCONTROL_AGENT;
        env_config.agents_to_instantiate[`SLOWCONTROL_AGENT] = `SLOWCONTROL_AGENT;
        env_config.scoreboards_to_instantiate = scoreboards_to_instantiate;
        env_config.instantiate_crosscoveragecollector = `YES;
        env_config.instantiate_virtualsequencer = `NO;

				// Create configuration objects for the agents
       slowcontrol_agent_config = CLICTD_agent_config::type_id::create   ( .name("slowcontrol_agent_config")); 
       slowcontrol_agent_config.instantiate_driver = `YES;
       slowcontrol_agent_config.instantiate_functionalcoveragecollector = `NO;

       regslowcontrol_agent_config = CLICTD_agent_config::type_id::create( .name("regslowcontrol_agent_config")); 
       regslowcontrol_agent_config.instantiate_driver = `NO;
       regslowcontrol_agent_config.instantiate_functionalcoveragecollector = `NO;



			 dataout_agent_config = CLICTD_agent_config::type_id::create       ( .name("dataout_agent_config")); 
       dataout_agent_config.instantiate_driver = `NO;
       dataout_agent_config.instantiate_functionalcoveragecollector = `NO;

				pins_agent_config = CLICTD_agent_config::type_id::create ( .name("pins_agent_config"));
        pins_agent_config.instantiate_driver = `YES;
       pins_agent_config.instantiate_functionalcoveragecollector = `NO;


				// Store the configuration objects into the configuration database, where they can be accessed 
		    // from all the components that the test contains
        // inst_name is the name stored in the database corresponding to the instance that will be able to retrieve this parameter
		    //uvm_config_db#( CLICTD_environment_config )::set
		    //   ( .cntxt( this ), .inst_name( "env" ), .field_name( "env_config" ), .value( env_config ) );
        uvm_config_db#( CLICTD_agent_config )::set
		       ( .cntxt( this ), .inst_name( "env.slowcontrol_agent" ), .field_name( "slowcontrol_agent_config" ), .value( slowcontrol_agent_config ) );
        uvm_config_db#( CLICTD_agent_config )::set
		       ( .cntxt( this ), .inst_name( "env.regslowcontrol_agent" ), .field_name( "regslowcontrol_agent_config" ), .value( regslowcontrol_agent_config ) );
        uvm_config_db#( CLICTD_agent_config )::set
		       ( .cntxt( this ), .inst_name( "env.dataout_agent" ), .field_name( "dataout_agent_config" ), .value( dataout_agent_config ) );
        uvm_config_db#( CLICTD_agent_config )::set
		       ( .cntxt( this ), .inst_name( "env.pins_agent" ), .field_name( "pins_agent_config" ), .value( pins_agent_config ) );

        // how do the uvm_config_db::set cntxt and inst_name arguments work?
        // in this case, cntxt = this indicates the top of the uvm hierarchy, i.e. uvm_test_top
        // if then inst_name = env, it means that this field will be accessible by all instances immediately under uvm_test_top/env;
        // to be accessible also for lowel levels in the hierarchy, we need to specify it like env.pins_agent, for instance.   
        // Note that the names we use to specify the hierarchy are the instance names, not the name of the classes (ie. env and not
        // CLICTD_environment). If we use "*" as inst_name, it means that all instances in the hierarchy can access the field.
       


        // Check the existance of virtual interfaces; if they exist, they are retrieved as virtual interface handles that will
        // be used by all the monitors and drivers
        if( !uvm_config_db#(virtual CLICTD_pins_interface)::get(this, "", "pins_if", pins_vif) )
       `uvm_error( `NOVIF_PINS, $sformatf("Pins virtual interface not found in database for %s", this.get_full_name()) )
        if( !uvm_config_db#(virtual CLICTD_readout_interface)::get(this, "", "readout_if", readout_vif) )
       `uvm_error( `NOVIF_READOUT, $sformatf("Readout virtual interface not found in database for %s", this.get_full_name()) )
        if( !uvm_config_db#(virtual CLICTD_slowcontrol_interface)::get(this, "", "slowcontrol_if", slowcontrol_vif) )
       `uvm_error( `NOVIF_SLOWCONTROL, $sformatf("Slowcontrol virtual interface not found in database for %s", this.get_full_name()) )

		    // Create the enviroment 
				//env = CLICTD_environment::type_id::create(.name("env"), .parent(this));

        

       
		endfunction: build_phase




		// Run phase: in this case, we use no sequence because the input stimuli is driven 
    // from the testbench top (the reset pulse is applied directly at RSTN_pad)
		virtual task run_phase(uvm_phase phase);
      //CLICTD_virtual_sequence virtual_seq;
      real simulation_time; 

			
      
        
								

      simulation_time = $realtime;
      phase.raise_objection(.obj(this));

      `uvm_info("TEST",$psprintf(" TOPOLOGY..............................."),UVM_HIGH);
      uvm_top.print_topology();
      //`uvm_info("TEST",$psprintf(" CONFIG_DB_DUMP..............................."),UVM_HIGH);
      //uvm_config_db#(int)::dump();

      // Display the whole testbench topology before starting.
      //uvm_top.print();
      //factory.print();
						
						
						
						
						
						
      virtual_seq = CLICTD_virtual_sequence::type_id::create( .name( "virtual_seq" ), .contxt( get_full_name() ) );
      // If we try to retrieve the virtual interface handles and testname from the database from within the virtual
      // sequence, the compiler will complain that these can't be found (the virtual sequence is not part of the 
      // hierarchy and it runs on a null sequencer, so the handles can't be retrieved from the sequencer either).
      // Instead, we can assign it directly from here
      virtual_seq.testname = testname;
      virtual_seq.chip_status_obj = chip_status_obj;
      virtual_seq.events_obj = events_obj;
      virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
      virtual_seq.reporting_enable_obj = reporting_enable_obj;
      virtual_seq.config_matrix_obj = config_matrix_obj;
						virtual_seq.time_measurements_obj = time_measurements_obj;
						virtual_seq.config_matrix_obj_for_coverage = config_matrix_obj_for_coverage;
						virtual_seq.time_measurements_obj_for_coverage = time_measurements_obj_for_coverage;
      virtual_seq.slowcontrol_sqr = env.slowcontrol_agent.sequencer;
      virtual_seq.pins_sqr = env.pins_agent.sequencer;
      virtual_seq.addresses_registers_slowcontrol = addresses_registers_slowcontrol;
      //assert( virtual_seq.randomize() ); // randomize, if there are some random fields
						
						
						// If these messages were print at the build phase, the wrong values were observed (values different as those observed later during configuration, so maybe the post randomization had not been taken place yet)
      $display("CLICTD_test config_matrix_obj - dummy bit = %b", config_matrix_obj.value_for_dummy_bit);
								//for (int c = 0; c < `COLUMNS; c++) begin
								//					for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						  //  							for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
								//							     		if (c == config_matrix_obj.columns_to_pulse[0] || c == config_matrix_obj.columns_to_pulse[1] || c == config_matrix_obj.columns_to_pulse[2] || c == config_matrix_obj.columns_to_pulse[3] || c == config_matrix_obj.columns_to_pulse[4] ||
								//												    		c == config_matrix_obj.columns_to_pulse[5] || c == config_matrix_obj.columns_to_pulse[6] || c == config_matrix_obj.columns_to_pulse[7] || c == config_matrix_obj.columns_to_pulse[8] )

 								//											    			$display("CLICTD_test config_matrix_obj - c = %0d ss = %0d sis = %0d, tpEnableDigital = %b, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						   //       																c,ss, sis, config_matrix_obj.tpEnableDigital[0][ss][sis], config_matrix_obj.mask[0][ss][sis], config_matrix_obj.tpEnableAnalog[0][ss][sis], config_matrix_obj.tuningDAC0[0][ss][sis], config_matrix_obj.tuningDAC1[0][ss][sis],
									//																							config_matrix_obj.tuningDAC2[0][ss][sis], config_matrix_obj.tuningDAC3[0][ss][sis], config_matrix_obj.tuningDAC4[0][ss][sis], config_matrix_obj.tuningDAC5[0][ss][sis], config_matrix_obj.tuningDAC6[0][ss][sis],
									//																							config_matrix_obj.tuningDAC7[0][ss][sis]);
									//								end
									//				end
      		//end
						/*		
						$display("clictd test 2 - toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
								         time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
																	config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
																	config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
																	config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
																	config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse);
						*/
      virtual_seq.start( .sequencer( null ) );
      #100ns ;


			phase.drop_objection(.obj(this));

      $timeformat(-3, 3, "ms", 8);
			//$timeformat params:
			//1) Scaling factor (-3 for milliseconds)
			//2) Number of digits to the right of the decimal point
			//3) A string to print after the time value
			//4) Minimum field width
      // https://verificationacademy.com/forums/systemverilog/usage-realtime

      simulation_time = $realtime - simulation_time;
      `uvm_info("Simulation time", $sformatf("Simulation elapsed %t", simulation_time), UVM_LOW);
      

		endtask: run_phase

    // Report phase, to summarize results
    function void report_phase( uvm_phase phase );
       

           // chip_status = STATUS_RESET
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET)           && env.pins_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0 &&
                env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET)        && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0 &&
                env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0
               ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected default value after reset", UVM_MEDIUM)
           end
           else if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && 
                     env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && 
                     env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) 
               )  begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors pins_interface, %d errors readout_interface, %d errors slowcontrol_interface after reset", 
                         env.pins_scoreboard.comparator.number_discrepancies[STATUS_RESET], env.dataout_scoreboard.comparator.number_discrepancies[STATUS_RESET],
                         env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_RESET]),  UVM_MEDIUM);
           end

           // - - - - - - - - - - - - - FROM HERE ON, OTHER TESTS THAN RESET TEST - - - - - - - - - - //

           // chip_status = STATUS_SECOND_READOUT_ALL_ZEROS
           if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_SECOND_READOUT_ALL_ZEROS) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_SECOND_READOUT_ALL_ZEROS] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected value (all zeros) after second readout", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_SECOND_READOUT_ALL_ZEROS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after second readout", env.dataout_scoreboard.comparator.number_discrepancies[STATUS_SECOND_READOUT_ALL_ZEROS]),  UVM_MEDIUM)
           end
 
          // - - - - - - - - - - - - - EXCLUSIVE TO CONFIGURATION TEST - - - - - - - - - - - - - - - //

          // chip_status = STATUS_CONFIG_READOUT_SECOND_HALF
          if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_CONFIG_READOUT_SECOND_HALF) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_CONFIG_READOUT_SECOND_HALF] == 0 &&
               env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_CONFIG_READOUT_SECOND_HALF) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_CONFIG_READOUT_SECOND_HALF] == 0
              ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected value after matrix configuration", UVM_MEDIUM)
          end
          else if( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_CONFIG_READOUT_SECOND_HALF) && env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_CONFIG_READOUT_SECOND_HALF) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors pins interface, %d errors readout_interface after matrix configuration", 
                   env.pins_scoreboard.comparator.number_discrepancies[STATUS_CONFIG_READOUT_SECOND_HALF], env.dataout_scoreboard.comparator.number_discrepancies[STATUS_CONFIG_READOUT_SECOND_HALF]),  UVM_MEDIUM)
          end


          // - - - - - - - - - - - - - EXCLUSIVE TO READOUT CONTROL TEST - - - - - - - - - - - - - - - //

          // chip_status = STATUS_THIRD_READOUT_NOTHING_HAPPENS
           if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_THIRD_READOUT_NOTHING_HAPPENS) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_THIRD_READOUT_NOTHING_HAPPENS] > 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "Timeout for start readout detected after third readout", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_THIRD_READOUT_NOTHING_HAPPENS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", "Timeout for start readout NOT detected after third readout",  UVM_MEDIUM)
           end
  
		       // chip_status = STATUS_FOURTH_READOUT_ALL_ZEROS
           if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_FOURTH_READOUT_ALL_ZEROS) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_FOURTH_READOUT_ALL_ZEROS] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected value (all zeros) after fourth readout", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_FOURTH_READOUT_ALL_ZEROS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after fourth readout", env.dataout_scoreboard.comparator.number_discrepancies[STATUS_FOURTH_READOUT_ALL_ZEROS]),  UVM_MEDIUM)
           end


		       // chip_status = STATUS_FIFTH_READOUT_NOTHING_HAPPENS
           if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_FIFTH_READOUT_NOTHING_HAPPENS) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_FIFTH_READOUT_NOTHING_HAPPENS] > 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "Timeout for start readout detected after fifth readout", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_FIFTH_READOUT_NOTHING_HAPPENS) )  begin
               `uvm_info(":( UVM TEST PROGRESS :(", "Timeout for start readout NOT detected after fifth readout",  UVM_MEDIUM)
           end
         
 
           // - - - - - - - - - - - - - EXCLUSIVE TO WRREGISTERS TEST - - - - - - - - - - - - - - - - //
           
           if ( env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_READ_ALL_SLOWCONTROL_REGISTERS) && 
                env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_READ_ALL_SLOWCONTROL_REGISTERS] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All slow control registers hold the value they were configured with", UVM_MEDIUM)
           end
           else if ( env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_READ_ALL_SLOWCONTROL_REGISTERS) )  begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors slowcontrol_interface after writing and reading slow control registers", 
                        env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_READ_ALL_SLOWCONTROL_REGISTERS]),  UVM_MEDIUM)
           end

           // - - - - - - - - - - - - - EXCLUSIVE TO POWER PULSING CONTROL TEST - - - - - - - - - - - //

           // chip_status = STATUS_POWERPULSE_EXTERNAL_WORKING
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_POWERPULSE_EXTERNAL_WORKING) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_POWERPULSE_EXTERNAL_WORKING] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected value when external power pulsing is selected", UVM_MEDIUM)
           end
           else if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_POWERPULSE_EXTERNAL_WORKING) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after applying external power pulsing", env.pins_scoreboard.comparator.number_discrepancies[STATUS_POWERPULSE_EXTERNAL_WORKING]),  UVM_MEDIUM)
           end


           // chip_status = STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS] > 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "Timeout for powerEnable update detected when external power pulsing is selected", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", "Timeout for powerEnable update NOT detected after applying external power pulsing",  UVM_MEDIUM)
           end
  
		       // chip_status = STATUS_POWERPULSE_INTERNAL_WORKING
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_POWERPULSE_INTERNAL_WORKING) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_POWERPULSE_INTERNAL_WORKING] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected value when internal power pulsing is selected", UVM_MEDIUM)
           end
           else if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_POWERPULSE_INTERNAL_WORKING) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after applying internal power pulsing", env.pins_scoreboard.comparator.number_discrepancies[STATUS_POWERPULSE_EXTERNAL_WORKING]),  UVM_MEDIUM)
           end


           // chip_status = STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS] > 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "Timeout for powerEnable update detected when internal power pulsing is selected", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", "Timeout for powerEnable update NOT detected after applying internal power pulsing",  UVM_MEDIUM)
           end


		         // - - - - - - - - - - - - - - - - - - - EXCLUSIVE TO READOUT TEST AND READOUT TEST FOR COVERAGE - - - - - - - - - - - - - //
          
										 // chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0),  UVM_MEDIUM)
           end
											
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0],STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0),  UVM_MEDIUM)
           end
											
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1],STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2],STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3],STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0],STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1],STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2],STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3],STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3),  UVM_MEDIUM)
           end
											
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0),  UVM_MEDIUM)
           end
											
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0),  UVM_MEDIUM)
           end
											
											// chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0
											if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0) && 
											     env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0] == 0 ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", $sformatf("The hit pattern read out matches the applied hit pattern, chip status %s",STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0), UVM_MEDIUM)
           end
           else if( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after acquiring hit pattern, chip status %s", 
                   env.dataout_scoreboard.comparator.number_discrepancies[STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0],STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0),  UVM_MEDIUM)
           end
											
											
											
											// - - - - - - - - - - - - - EXCLUSIVE TO TEST PULSE CONTROL TEST - - - - - - - - - - - //

           // chip_status = STATUS_TESTPULSE_EXTERNAL_WORKING
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_TESTPULSE_EXTERNAL_WORKING) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_TESTPULSE_EXTERNAL_WORKING] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected value when external test pulsing is selected", UVM_MEDIUM)
           end
           else if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_TESTPULSE_EXTERNAL_WORKING) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after applying external test pulsing", env.pins_scoreboard.comparator.number_discrepancies[STATUS_TESTPULSE_EXTERNAL_WORKING]),  UVM_MEDIUM)
           end


           // chip_status = STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS] > 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "Timeout for discTp update detected when external test pulsing is selected", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", "Timeout for discTp update NOT detected after applying external test pulsing",  UVM_MEDIUM)
           end
  
		       // chip_status = STATUS_TESTPULSE_INTERNAL_WORKING
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_TESTPULSE_INTERNAL_WORKING) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_TESTPULSE_INTERNAL_WORKING] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected value when internal test pulsing is selected", UVM_MEDIUM)
           end
           else if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_TESTPULSE_INTERNAL_WORKING) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after applying internal test pulsing", env.pins_scoreboard.comparator.number_discrepancies[STATUS_TESTPULSE_INTERNAL_WORKING]),  UVM_MEDIUM)
           end


           // chip_status = STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS] > 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "Timeout for discTp update detected when internal test pulsing is selected", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", "Timeout for discTp update NOT detected after applying internal test pulsing",  UVM_MEDIUM)
           end
											
											
         // PENDING: save to file
    endfunction: report_phase
   

endclass: CLICTD_test


`endif // __CLICTD_test_sv__
