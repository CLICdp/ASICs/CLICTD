// Filename           : CLICTD_reset_test.sv
// Author             : Núria Egidos 
// Created on         : 31/5/18
// Last modification  : 8/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Reset test 
//                      Apply a reset pulse and check that the values of the slow control registers
//											and other signals in the chip are the specified default values
// Derived from       : top_test_external_internal_hits_4lanes.sv by Elia Conti for VEPIX53
//                      and  analysis_master_agent.sv
//                      http://cluelogic.com/2012/01/uvm-tutorial-for-candy-lovers-virtual-sequence/


`ifndef __CLICTD_reset_test_sv__
`define __CLICTD_reset_test_sv__


class CLICTD_reset_test extends CLICTD_test;

  // Register the component into the UVM factory
	`uvm_component_utils(CLICTD_reset_test);


    // Constructor
		function new(string name, uvm_component parent);
			super.new(name, parent);
      testname = `RESET_TEST;
		endfunction: new


    // Build phase: build the hierarchy
		virtual function void build_phase(uvm_phase phase);
				super.build_phase(phase);


        reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES; // Reporting in dataout_monitor, breakdown_readout_bits
			  reporting_enable_obj.enable_reporting_slowcontroldriver = `YES; // Reporting in slowcontrol_driver
        uvm_config_db#(ReportingEnable)::set( .cntxt(this), .inst_name("*"), .field_name("reporting_enable_obj"), .value(reporting_enable_obj) );


				// Store the configuration objects into the configuration database, where they can be accessed 
		    // from all the components that the test contains
        // inst_name is the name stored in the database corresponding to the instance that will be able to retrieve this parameter
		    uvm_config_db#( CLICTD_environment_config )::set
		       ( .cntxt( this ), .inst_name( "env" ), .field_name( "env_config" ), .value( env_config ) );
        // Create the enviroment 
				env = CLICTD_environment::type_id::create(.name("env"), .parent(this));
       
        // how do the uvm_config_db::set cntxt and inst_name arguments work?
        // in this case, cntxt = this indicates the top of the uvm hierarchy, i.e. uvm_test_top
        // if then inst_name = env, it means that this field will be accessible by all instances immediately under uvm_test_top/env;
        // to be accessible also for lowel levels in the hierarchy, we need to specify it like env.pins_agent, for instance.   
        // Note that the names we use to specify the hierarchy are the instance names, not the name of the classes (ie. env and not
        // CLICTD_environment). If we use "*" as inst_name, it means that all instances in the hierarchy can access the field.


      
		endfunction: build_phase


    /*

    // Report phase, to summarize results
    function void report_phase( uvm_phase phase );
       
           // chip_status = STATUS_RESET
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET)           && env.pins_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0 &&
                env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET)        && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0 &&
                env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0
               ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected default value after reset", UVM_MEDIUM)
           end
           else if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && 
                     env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && 
                     env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) 
               )  begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors pins_interface, %d errors readout_interface, %d errors slowcontrol_interface after reset", 
                         env.pins_scoreboard.comparator.number_discrepancies[STATUS_RESET], env.dataout_scoreboard.comparator.number_discrepancies[STATUS_RESET],
                         env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_RESET]),  UVM_MEDIUM);
           end

           
           //if (env.pins_scoreboard.comparator.number_discrepancies[chip_status_obj.chip_status] == 0 &&
           //    env.dataout_scoreboard.comparator.number_discrepancies[chip_status_obj.chip_status] == 0 &&
           //    env.regslowcontrol_scoreboard.comparator.number_discrepancies[chip_status_obj.chip_status] == 0
           //    ) begin
           //    `uvm_info("** UVM TEST PASSED **", "All monitored signals have the expected default value after reset", UVM_MEDIUM)
           //end
           //else begin
           //    `uvm_info("** UVM TEST FAILED **", $sformatf("%d/%d errors pins_interface, %d/%d errors dataout_interface, %d/%d errors slowcontrol_interface, test: %s", 
           //        env.pins_scoreboard.comparator.number_discrepancies[chip_status_obj.chip_status], env.pins_scoreboard.comparator.number_transactions_evaluated[chip_status_obj.chip_status], 
           //        env.dataout_scoreboard.comparator.number_discrepancies[chip_status_obj.chip_status], env.dataout_scoreboard.comparator.number_transactions_evaluated[chip_status_obj.chip_status],
           //        env.regslowcontrol_scoreboard.comparator.number_discrepancies[chip_status_obj.chip_status], env.regslowcontrol_scoreboard.comparator.number_transactions_evaluated[chip_status_obj.chip_status],
           //        testname),  UVM_MEDIUM);
           //end
           
        
         
         
         // PENDING: save to file
    endfunction: report_phase
*/

endclass: CLICTD_reset_test


`endif // __CLICTD_reset_test_sv__
