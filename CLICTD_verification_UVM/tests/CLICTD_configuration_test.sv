// Filename           : CLICTD_configuration_test.sv
// Author             : Núria Egidos 
// Created on         : 8/8/18
// Last modification  : 8/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Configuration test
//                      Send the configuration word(s) via the slow control interface and then:
//                      -  Read back the value of the applied configuration via the serial readout
//                      -  Check that the value of tpEnableDigital, mask[7:0], tpEnableAnalog[7:0] and
//                         tuningDAC7-0[2:0] (where the configuration is latched) is correct
// Derived from       : top_test_external_internal_hits_4lanes.sv by Elia Conti for VEPIX53
//                      and  analysis_master_agent.sv
//                      http://cluelogic.com/2012/01/uvm-tutorial-for-candy-lovers-virtual-sequence/


`ifndef __CLICTD_configuration_test_sv__
`define __CLICTD_configuration_test_sv__


class CLICTD_configuration_test extends CLICTD_test;

  // Register the component into the UVM factory
	`uvm_component_utils(CLICTD_configuration_test);


    // Constructor
		function new(string name, uvm_component parent);
			super.new(name, parent);
      testname = `CONFIGURATION_TEST;
		endfunction: new


    // Build phase: build the hierarchy
		virtual function void build_phase(uvm_phase phase);
				super.build_phase(phase);

        //reporting_enable_obj.enable_reporting_matrixconfigonehalfseq = `YES; // Reporting in CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence
        uvm_config_db#(ReportingEnable)::set( .cntxt(this), .inst_name("*"), .field_name("reporting_enable_obj"), .value(reporting_enable_obj) );


				// Store the configuration objects into the configuration database, where they can be accessed 
		    // from all the components that the test contains
        // inst_name is the name stored in the database corresponding to the instance that will be able to retrieve this parameter
		    uvm_config_db#( CLICTD_environment_config )::set
		       ( .cntxt( this ), .inst_name( "env" ), .field_name( "env_config" ), .value( env_config ) );
        // Create the enviroment 
				env = CLICTD_environment::type_id::create(.name("env"), .parent(this));
       
        // how do the uvm_config_db::set cntxt and inst_name arguments work?
        // in this case, cntxt = this indicates the top of the uvm hierarchy, i.e. uvm_test_top
        // if then inst_name = env, it means that this field will be accessible by all instances immediately under uvm_test_top/env;
        // to be accessible also for lowel levels in the hierarchy, we need to specify it like env.pins_agent, for instance.   
        // Note that the names we use to specify the hierarchy are the instance names, not the name of the classes (ie. env and not
        // CLICTD_environment). If we use "*" as inst_name, it means that all instances in the hierarchy can access the field.
       


        //virtual_seq = CLICTD_virtual_sequence::type_id::create( .name( "virtual_seq" ), .contxt( get_full_name() ) );
      // If we try to retrieve the virtual interface handles and testname from the database from within the virtual
      // sequence, the compiler will complain that these can't be found (the virtual sequence is not part of the 
      // hierarchy and it runs on a null sequencer, so the handles can't be retrieved from the sequencer either).
      // Instead, we can assign it directly from here
      //virtual_seq.pins_vif = pins_vif;
      //virtual_seq.testname = testname;
      //virtual_seq.chip_status_obj = chip_status_obj;
      //virtual_seq.slowcontrol_sqr = env.slowcontrol_agent.sequencer;

      
		endfunction: build_phase

    /*
    // Report phase, to summarize results
    function void report_phase( uvm_phase phase );
       

           // chip_status = STATUS_RESET
           if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET)           && env.pins_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0 &&
                env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET)        && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0 &&
                env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_RESET] == 0
               ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected default value after reset", UVM_MEDIUM)
           end
           else if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && 
                     env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) && 
                     env.regslowcontrol_scoreboard.comparator.number_discrepancies.exists(STATUS_RESET) 
               )  begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors pins_interface, %d errors readout_interface, %d errors slowcontrol_interface after reset", 
                         env.pins_scoreboard.comparator.number_discrepancies[STATUS_RESET], env.dataout_scoreboard.comparator.number_discrepancies[STATUS_RESET],
                         env.regslowcontrol_scoreboard.comparator.number_discrepancies[STATUS_RESET]),  UVM_MEDIUM);
           end

           // chip_status = STATUS_SECOND_READOUT_ALL_ZEROS
           if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_SECOND_READOUT_ALL_ZEROS) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_SECOND_READOUT_ALL_ZEROS] == 0 ) begin
               `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected default value after second readout", UVM_MEDIUM)
           end
           else if ( env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_SECOND_READOUT_ALL_ZEROS) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors readout_interface after second readout", env.dataout_scoreboard.comparator.number_discrepancies[STATUS_SECOND_READOUT_ALL_ZEROS]),  UVM_MEDIUM)
           end
 
          // chip_status = STATUS_CONFIG_READOUT_SECOND_HALF
          if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_CONFIG_READOUT_SECOND_HALF) && env.pins_scoreboard.comparator.number_discrepancies[STATUS_CONFIG_READOUT_SECOND_HALF] == 0 &&
               env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_CONFIG_READOUT_SECOND_HALF) && env.dataout_scoreboard.comparator.number_discrepancies[STATUS_CONFIG_READOUT_SECOND_HALF] == 0
              ) begin
              `uvm_info(":) UVM TEST PROGRESS :)", "All monitored signals have the expected default value after matrix configuration", UVM_MEDIUM)
          end
          else if ( env.pins_scoreboard.comparator.number_discrepancies.exists(STATUS_CONFIG_READOUT_SECOND_HALF) && env.dataout_scoreboard.comparator.number_discrepancies.exists(STATUS_CONFIG_READOUT_SECOND_HALF) ) begin
               `uvm_info(":( UVM TEST PROGRESS :(", $sformatf("%d errors pins_interface, %d errors readout_interface after matrix configuration", 
                   env.pins_scoreboard.comparator.number_discrepancies[STATUS_CONFIG_READOUT_SECOND_HALF], env.dataout_scoreboard.comparator.number_discrepancies[STATUS_CONFIG_READOUT_SECOND_HALF]),  UVM_MEDIUM)
          end


         
         
         // PENDING: save to file
    endfunction: report_phase
   */

endclass: CLICTD_configuration_test


`endif // __CLICTD_configuration_test_sv__
