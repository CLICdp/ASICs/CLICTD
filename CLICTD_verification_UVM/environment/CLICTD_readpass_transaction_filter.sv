// Filename           : CLICTD_readpass_transaction_filter.sv
// Author             : Núria Egidos 
// Created on         : 26/9/18
// Last modification  : 26/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : This block receives transactions from the regslowcontrol_monitor when a read or a write operation 
//                      of some slow control register is completed, and sends only those corresponding to a read operation
//                      to the regslowcontrol_comparator to be compared with those coming from the regslowcontrol_model

`ifndef __CLICTD_readpass_transaction_filter_sv__
`define __CLICTD_readpass_transaction_filter_sv__



class CLICTD_readpass_transaction_filter extends uvm_subscriber #(CLICTD_slowcontrol_transaction); 

	`uvm_component_utils(CLICTD_readpass_transaction_filter) // Register the component to the factory

  // Declare the analysis port to provide the "ideal outputs" to the comparator
	uvm_analysis_port#(CLICTD_slowcontrol_transaction) analysis_port; 


  // This transaction is used to store the value written to a certain slow control register,
  // which will be the expected value when the same register is read afterwards.
  // When the write operation is performed, the content associated to that register address
  // is stored in auxiliary_slowcontrol_transaction; later on, when the same register is read,
  // the content in auxiliary_slowcontrol_transaction is sent to the comparator
  CLICTD_slowcontrol_transaction auxiliary_slowcontrol_transaction;

  ChipStatusInfo chip_status_obj;
 
  // - - - - - - - - - - - - Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

  // - - - - - - - - - - - - Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
    // Create the analysis port to provide the "ideal outputs" to the scoreboard
		analysis_port = new(.name("analysis_port"), .parent(this));
    // Create the analysis export to probe some signals from slowcontrol_interface
    //model_analysisexport = new(.name("model_analysisexport"), .parent(this));

 

    // Retrieve the chip status from the database; it is set (and changed as the test progresses) from the virtual sequence 
    // in its body() task, so by this point it should be available to be retrieved
    if( !uvm_config_db#(ChipStatusInfo)::get(this, "", "chip_status_obj", chip_status_obj) )
       `uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )


	endfunction: build_phase


  // - - - - - - - - - - - - Write for the analysis_export of uvm_subscriber (this is called by slowcontrol_agent, slowcontrol_monitor)
  // Implementation of the write() function, which sends transactions (ideal outputs) to the scoreboard
  // This function is called every time the slowcontrol_agent sends a transaction to the slowcontrol_scoreboard, i.e. 
  // when the reset signal falls
	function void write(CLICTD_slowcontrol_transaction t); // t is used to match the declaration of pure virtual function write in uvm_subscriber

    // Clone the transaction t into another transaction, so as not to change the transaction
   // sent to the scoreboard
   CLICTD_slowcontrol_transaction cloned_t;
   $cast(cloned_t, t.clone());
    if (cloned_t.read_or_write == `READ) begin 
        analysis_port.write(cloned_t);  
        `uvm_info("Transaction to comparator", $sformatf("register_address = %h, register_content = %h, chip_status = %s, %s", cloned_t.register_address, cloned_t.register_content, chip_status_obj.chip_status, this.get_full_name()), UVM_LOW) 
   end 
	endfunction: write


  // - - - - - - - - - - - - Other tasks/functions


endclass: CLICTD_readpass_transaction_filter






`endif // __CLICTD_readpass_transaction_filter_sv__
