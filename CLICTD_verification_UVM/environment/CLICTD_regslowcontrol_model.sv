// Filename           : CLICTD_regslowcontrol_model.sv
// Author             : Núria Egidos 
// Created on         : 25/7/18
// Last modification  : 27/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Golden model or predictor corresponding to the value of the different signals in slowcontrol_interface

`ifndef __CLICTD_regslowcontrol_model_sv__
`define __CLICTD_regslowcontrol_model_sv__

// The built-in analysis export in the subscriber "taps" on the transaction that the slowcontrolinterface_agent
// send to the comparator in the slowcontrol_interface scoreboard. When this transaction is sent, it means that 
// there is either a falling or a rising edge in RSTN_pad, so this is the moment the generate the expected 
// output. 



class CLICTD_regslowcontrol_model extends uvm_subscriber #(CLICTD_slowcontrol_transaction); 

	`uvm_component_utils(CLICTD_regslowcontrol_model) // Register the component to the factory

  // Declare the analysis port to provide the "ideal outputs" to the comparator
	uvm_analysis_port#(CLICTD_slowcontrol_transaction) analysis_port; 
  // NOTE: WE USE THE IMPLICIT ANALYSIS_EXPORT OF UVM_SUBSCRIBER

  default_values_registers_slowcontrol_type default_values_registers_slowcontrol;
  ChipStatusInfo chip_status_obj;

  // This transaction is used to store the value written to a certain slow control register,
  // which will be the expected value when the same register is read afterwards.
  // When the write operation is performed, the content associated to that register address
  // is stored in auxiliary_slowcontrol_transaction; later on, when the same register is read,
  // the content in auxiliary_slowcontrol_transaction is sent to the comparator
  CLICTD_slowcontrol_transaction auxiliary_slowcontrol_transaction, auxiliary_slowcontrol_transaction2;
  CLICTD_slowcontrol_transaction pool_slowcontrol_transaction [string];

  bit write_transaction_to_comparator = `NO;
  bit write_arrived_before = `NO;

  
  // - - - - - - - - - - - - Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

  // - - - - - - - - - - - - Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
    // Create the analysis port to provide the "ideal outputs" to the scoreboard
		analysis_port = new(.name("analysis_port"), .parent(this));
    // Create the analysis export to probe some signals from slowcontrol_interface
    //model_analysisexport = new(.name("model_analysisexport"), .parent(this));

    if( !uvm_config_db#(default_values_registers_slowcontrol_type)::get(this, "", "default_values_registers_slowcontrol", default_values_registers_slowcontrol) )
       `uvm_error( `NODEFAULTVALUEREG, $sformatf("Default value registers not found in database for %s", this.get_full_name()) )

    // Retrieve the chip status from the database; it is set (and changed as the test progresses) from the virtual sequence 
    // in its body() task, so by this point it should be available to be retrieved
    if( !uvm_config_db#(ChipStatusInfo)::get(this, "", "chip_status_obj", chip_status_obj) )
       `uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )


    

	endfunction: build_phase


  // - - - - - - - - - - - - Write for the analysis_export of uvm_subscriber (this is called by slowcontrol_agent, slowcontrol_monitor)
  // Implementation of the write() function, which sends transactions (ideal outputs) to the scoreboard
  // This function is called every time the slowcontrol_agent sends a transaction to the slowcontrol_scoreboard, i.e. 
  // when the reset signal falls
	function void write(CLICTD_slowcontrol_transaction t); // t is used to match the declaration of pure virtual function write in uvm_subscriber
		// the predicted output is obtained by adding the monitored inputs

    // Clone the transaction t into another transaction, so as not to change the transaction
   // sent to the scoreboard
   CLICTD_slowcontrol_transaction cloned_t;
   $cast(cloned_t, t.clone());
			assert_isunknown_slowcontrol_transaction ( .slowcontrol_transaction(cloned_t), .chip_status_obj(chip_status_obj), .name_of_transaction("cloned_t"), .name_of_calling_component(this.get_full_name()) );

    predict(cloned_t);
    if (write_transaction_to_comparator == `YES) begin 
       analysis_port.write(cloned_t); 
       write_transaction_to_comparator = `NO; 
       `uvm_info("Transaction to comparator", $sformatf("register_address = %h, register_content = %h, chip_status = %s", cloned_t.register_address, cloned_t.register_content, chip_status_obj.chip_status), UVM_LOW);
    end
	endfunction: write


  // - - - - - - - - - - - - Other tasks/functions



  virtual function void predict(CLICTD_slowcontrol_transaction ideal_slowcontrol_transaction);
		// When slowcontrol_monitor writes a transaction, it can come either from a read or a write operation on the slow control registers.
    // In STATUS_RESET, we expect it from a read operation. The arrival of such transaction is used to notify to the comparator
    // the expected value of the registers after a reset.
    // In any other chip_status, the expected value of a register read comes from the value sent to the chip in a former write 
    // operation corresponding to that register


    // STATUS_RESET
    if(chip_status_obj.chip_status == STATUS_RESET) begin: status_reset
		    // Transaction coming from "write" operation detected when "read" operation was expected
		    if (ideal_slowcontrol_transaction.read_or_write == `WRITE) begin
            `uvm_error( `ERROR_MODEL, $sformatf("Transaction from write operation instead of read operation found, chip_status = %s", chip_status_obj.chip_status) ) 
        end
		    // Transaction coming from "read" operation detected
		    else begin
		         ideal_slowcontrol_transaction.register_content = default_values_registers_slowcontrol[ideal_slowcontrol_transaction.register_address];
		         // the address is already cloned from the transaction coming from slowcontrol_monitor
		         write_transaction_to_comparator = `YES;
		    end   
		end: status_reset



    // STATUS_WRITE_ALL_SLOWCONTROL_REGISTERS
    // The content of the incoming transaction is stored in auxiliary_slowcontrol_transaction (a new transaction is created for each new content
    // to avoid data collision), which will be later used to build the expected content of the transaction to send to the comparator when a transaction
    // from a read operation arrives
    else if ( chip_status_obj.chip_status == STATUS_WRITE_ALL_SLOWCONTROL_REGISTERS ) begin: writeslowcontrolreg
         if (ideal_slowcontrol_transaction.read_or_write == `READ) begin
            `uvm_error( `ERROR_MODEL, $sformatf("Transaction from read operation instead of write operation found, chip_status = %s", chip_status_obj.chip_status) ) 
         end
         else begin
				     auxiliary_slowcontrol_transaction = CLICTD_slowcontrol_transaction::type_id::create(.name("auxiliary_slowcontrol_transaction"), .contxt(get_full_name()));
				     auxiliary_slowcontrol_transaction.register_address = ideal_slowcontrol_transaction.register_address;
				     auxiliary_slowcontrol_transaction.register_content = ideal_slowcontrol_transaction.register_content;
             pool_slowcontrol_transaction[$sformatf("%h",auxiliary_slowcontrol_transaction.register_address)] = auxiliary_slowcontrol_transaction;
             `uvm_info("Transaction from monitor", $sformatf("register_address = %h, register_content = %h, chip_status = %s", pool_slowcontrol_transaction[$sformatf("%h",auxiliary_slowcontrol_transaction.register_address)].register_address, pool_slowcontrol_transaction[$sformatf("%h",auxiliary_slowcontrol_transaction.register_address)].register_content, chip_status_obj.chip_status), UVM_LOW);
				     //write_transaction_to_comparator  =`NO;
         end
    end: writeslowcontrolreg


    // STATUS_READ_ALL_SLOWCONTROL_REGISTERS
    // The content stored formerly in auxiliary_slowcontrol_transaction is used to build the transaction to send to the comparator, so as to compare it with 
    // the corresponding transaction generated from a read register content request
    else if ( chip_status_obj.chip_status == STATUS_READ_ALL_SLOWCONTROL_REGISTERS ) begin: readslowcontrolreg
         if (ideal_slowcontrol_transaction.read_or_write == `WRITE) begin
            `uvm_error( `ERROR_MODEL, $sformatf("Transaction from write operation instead of read operation found, chip_status = %s", chip_status_obj.chip_status) ) 
         end
         else begin
             if ( pool_slowcontrol_transaction.exists($sformatf("%h",ideal_slowcontrol_transaction.register_address)) ) begin
                 //auxiliary_slowcontrol_transaction2 = pool_slowcontrol_transaction[$sformatf("%h",ideal_slowcontrol_transaction.register_address)];
								 ideal_slowcontrol_transaction.register_content = pool_slowcontrol_transaction[$sformatf("%h",ideal_slowcontrol_transaction.register_address)].register_content;
								 write_transaction_to_comparator = `YES;
             end
             else begin
                  `uvm_error( `ERROR_MODEL, $sformatf("Register content has not been written before requesting read operation, chip_status = %s", chip_status_obj.chip_status) )
             end
				     
         end
    end: readslowcontrolreg

    else pool_slowcontrol_transaction.delete(); // in any other state, free up transaction pool for the next operation



	endfunction: predict

endclass: CLICTD_regslowcontrol_model






`endif // __CLICTD_regslowcontrol_model_sv__
