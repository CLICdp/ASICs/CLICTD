// Filename           : build_expected_measurements_from_matrixconfig_obj_for_coverage.sv
// Author             : N�ria Egidos 
// Created on         : 17/1/19
// Last modification  : 25/1/19 
// Project            : Doctoral student, verification of CLICTD
// Description        : This code is to be added to the CLICTD_dataout_model to generate the expected column_content
//                      values when the matrix is read out after an acquisition (STATUS_MATRIX_READOUT_ACQUIRED)



function void build_expected_measurements_from_matrixconfig_obj_for_coverage (
     input int column_number,
					input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
     input FieldsConfigureMatrix_for_coverage config_matrix_obj,
					input FieldsTimeMeasurements_for_coverage time_measurements_obj,
					input string testname,
     output Acquired_word_struct column_content_breakdown
);

      int coordinates_pulsed[string];
						string current_coordinate,aux_coordinate;
						int index_in_superpixels_to_pulse_of_current_coordinate;
						
						coordinates_pulsed.delete();
						foreach (config_matrix_obj.superpixels_to_pulse[i]) begin
									aux_coordinate = $sformatf("%0d-%0d-%0d",config_matrix_obj.superpixels_to_pulse[i].column,config_matrix_obj.superpixels_to_pulse[i].superpixel_segment,config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment);
									coordinates_pulsed[aux_coordinate] = i; // the index is given by the value of column, superpixel_segment and superpixel_in_segment at this position of the superpixels_to_pulse array;
									                                        // the value stored at this index is the corresponding position in the superpixels_to_pulse array
						end
															
										
					// Initial values (to be changed later if applies)
					for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
					     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
										     column_content_breakdown.superpixel[ss][sis].hit_map               = '0;
	  												column_content_breakdown.superpixel[ss][sis].toa                   = '0;
															column_content_breakdown.superpixel[ss][sis].tot                   = '0;
															column_content_breakdown.superpixel[ss][sis].longtoa               = '0;
															column_content_breakdown.superpixel[ss][sis].photoncount           = '0;
															//column_content_breakdown.superpixel[ss][sis].hit_flag              = 1'b0;
															column_content_breakdown.superpixel[ss][sis].hit_flag              = config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[column_number][ss][sis];
															column_content_breakdown.superpixel[ss][sis].superpixel_segment    = ss;
															column_content_breakdown.superpixel[ss][sis].superpixel_in_segment = sis;
										end
				 end
					
					
					for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin: loop_superpixel_segment
					     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin: loop_superpixel_in_segment
										     
															current_coordinate = $sformatf("%0d-%0d-%0d",column_number,ss,sis);
															
															if (coordinates_pulsed.exists(current_coordinate)) begin: coordinate_is_pulsed
															
										     																// Is digital test pulse enabled in this superpixel?
																															// --------------------------------------------------------------------------------------------------
																															if (config_matrix_obj.tpEnableDigital[column_number][ss][sis]) begin: digital_test_pulse_applied

															    																// Nominal acquisition mode
																																			if      (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0) begin
														        																					// ToA
																																											// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive;
																																											// or if ToA exceeds 255*`DELAY_FACTOR_TOA (in this acquisition mode, only 8 bits are available to count ToA), the value will saturate as well
                           																if (!bits_slowcontrol_reg_obj.tpExtEn || time_measurements_obj.toa_ns >= 255*`DELAY_FACTOR_TOA) column_content_breakdown.superpixel[ss][sis].toa = '1;
																																											else column_content_breakdown.superpixel[ss][sis].toa = time_measurements_obj.toa_ns/`DELAY_FACTOR_TOA;
																																											// ToT
																																											// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive;
																																											// or if ToT exceeds 31*`DELAY_FACTOR_TOT and totDiv = 0, or TOT exceeds 31*2*`DELAY_FACTOR_TOT and totDiv = 1, or ToT exceeds 31*4*`DELAY_FACTOR_TOT and totDiv = 2,
																																											// or ToT exceeds 31*8*`DELAY_FACTOR_TOT and totDiv = 3  (5 bits are available to count ToT), the value will saturate as well
                           																if (!bits_slowcontrol_reg_obj.tpExtEn || time_measurements_obj.tot_ns >= 31*`DELAY_FACTOR_TOT && bits_slowcontrol_reg_obj.totDiv == 0 ||
																											    																time_measurements_obj.tot_ns >= 31*2*`DELAY_FACTOR_TOT && bits_slowcontrol_reg_obj.totDiv == 1 || time_measurements_obj.tot_ns >= 31*4*`DELAY_FACTOR_TOT && bits_slowcontrol_reg_obj.totDiv == 2 || 
																																															time_measurements_obj.tot_ns >= 31*8*`DELAY_FACTOR_TOT && bits_slowcontrol_reg_obj.totDiv == 3 ) column_content_breakdown.superpixel[ss][sis].tot = '1;
																																											else column_content_breakdown.superpixel[ss][sis].tot = time_measurements_obj.tot_ns/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );

																																			end
																																			// Long ToA acquisition mode
																																			else if (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
																									  																// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive;
																																											// or if ToA exceeds 8191*`DELAY_FACTOR_TOA (in this acquisition mode, 13 bits are available to count ToA), the value will saturate as well
                           																if (!bits_slowcontrol_reg_obj.tpExtEn || time_measurements_obj.toa_ns >= 8191*`DELAY_FACTOR_TOA) column_content_breakdown.superpixel[ss][sis].longtoa = '1;
																																											else column_content_breakdown.superpixel[ss][sis].longtoa = time_measurements_obj.toa_ns/`DELAY_FACTOR_TOA;
																																			end
																																			// Photon counting acquisition mode
																																			else if (bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
														       																					if (time_measurements_obj.number_of_pulses_to_apply_within_shutter > 1) 
																																														column_content_breakdown.superpixel[ss][sis].photoncount = 13'd2;
																																										else column_content_breakdown.superpixel[ss][sis].photoncount = 13'd1;
																																			end

																															end: digital_test_pulse_applied

																															// Is some hit forced at some discriminator bit (and digital test pulse is disabled in this superpixel)?
																															// ---------------------------------------------------------------------------------------------------
																															else if (config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[column_number][ss][sis] ) begin: force_disc_bit_not_skipped_for_compression

																																																				// Nominal acquisition mode
																																																				if      (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0) begin
														        																																						// ToA

																																																												// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive;
																																																												// or if ToA exceeds 255*`DELAY_FACTOR_TOA (in this acquisition mode, only 8 bits are available to count ToA), the value will saturate as well
                           																																	if (!bits_slowcontrol_reg_obj.tpExtEn || time_measurements_obj.toa_ns >= 255*`DELAY_FACTOR_TOA) column_content_breakdown.superpixel[ss][sis].toa = '1;
																																																												else begin 
																												     																																column_content_breakdown.superpixel[ss][sis].toa = time_measurements_obj.toa_ns/`DELAY_FACTOR_TOA;
																																																																	// If the expected hit flag for this superpixel is 1, it means that at least one of the two discriminator bits that are pulsed is not masked. We check if some is masked and,
																																																																	// if necessary, we reduce the computed value of ToA to subtract the contribution of the masked front-end
																																																																	if (config_matrix_obj.mask[column_number][ss][sis][config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_0]) begin
																																											    																						// this bit corresponds to the first pulse that arrives within the shutter, so if it's masked, the value of ToA will be fixed by the arrival of the second pulse, which occurs in bit_applydiscpulse_1
																																																																					if (time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1) column_content_breakdown.superpixel[ss][sis].toa -= time_measurements_obj.time_separation_between_pulses_in_two_disc_bits/`DELAY_FACTOR_TOA;
																																																																					else column_content_breakdown.superpixel[ss][sis].toa -= (time_measurements_obj.duration_first_pulse_in_disc_bits + time_measurements_obj.time_separation_between_pulses_in_two_disc_bits)/`DELAY_FACTOR_TOA;
																																																																	end
																			  																																												// If bit_applydiscpulse_1 is masked instead, it makes no difference in terms of ToA, because it's the second pulse to arrive
                            																																end

																																																												// ToT

																																																												// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive;
																																																												// or if ToT exceeds 31*`DELAY_FACTOR_TOT and totDiv = 0, or TOT exceeds 31*2*`DELAY_FACTOR_TOT and totDiv = 1, or ToT exceeds 31*4*`DELAY_FACTOR_TOT and totDiv = 2,
																																																												// or ToT exceeds 31*8*`DELAY_FACTOR_TOT and totDiv = 3  (5 bits are available to count ToT), the value will saturate as well
                           																																	if (!bits_slowcontrol_reg_obj.tpExtEn || time_measurements_obj.tot_ns >= 31*`DELAY_FACTOR_TOT && bits_slowcontrol_reg_obj.totDiv == 0 ||
																											    																																	time_measurements_obj.tot_ns >= 31*2*`DELAY_FACTOR_TOT && bits_slowcontrol_reg_obj.totDiv == 1 || time_measurements_obj.tot_ns >= 31*4*`DELAY_FACTOR_TOT && bits_slowcontrol_reg_obj.totDiv == 2 || 
																																																																time_measurements_obj.tot_ns >= 31*8*`DELAY_FACTOR_TOT && bits_slowcontrol_reg_obj.totDiv == 3 ) column_content_breakdown.superpixel[ss][sis].tot = '1;
																																																												else begin
																																																																	column_content_breakdown.superpixel[ss][sis].tot = time_measurements_obj.tot_ns/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );

																																																																	// If the expected hit flag for this superpixel is 1, it means that at least one of the two discriminator bits that are pulsed is not masked. We check if some is masked and,
																																																																	// if necessary, we reduce the computed value of ToT to subtract the contribution of the masked front-end
																																																																	if (config_matrix_obj.mask[column_number][ss][sis][config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_0]) begin
																																											    																						// this bit corresponds to the first pulse that arrives within the shutter, so if it's masked, the value of ToT will be fixed by the second pulse, which occurs in bit_applydiscpulse_1
																																																																					if (time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1) column_content_breakdown.superpixel[ss][sis].tot -= 
																																																																					time_measurements_obj.time_separation_between_pulses_in_two_disc_bits/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																																																					else column_content_breakdown.superpixel[ss][sis].tot -= (time_measurements_obj.duration_first_pulse_in_disc_bits)/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																																																	end
																																																																	else if (config_matrix_obj.mask[column_number][ss][sis][config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_1]) begin
																																											    																						// this bit corresponds to the second pulse that arrives within the shutter, so if it's masked, the value of ToT will be fixed by the first pulse, which occurs in bit_applydiscpulse_0
																																																																					if (time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1) column_content_breakdown.superpixel[ss][sis].tot -= 
																																															   																						(time_measurements_obj.duration_second_pulse_in_disc_bits + time_measurements_obj.duration_first_pulse_in_disc_bits - time_measurements_obj.time_separation_between_pulses_in_two_disc_bits)/
																																																																								( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																																																					else column_content_breakdown.superpixel[ss][sis].tot -= (time_measurements_obj.duration_second_pulse_in_disc_bits)/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																																																	end

																																																													end 

																																																				end
																																																				// Long ToA acquisition mode
																																																				else if (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin

																				      																																// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive;
																																																										// or if ToA exceeds 8191*`DELAY_FACTOR_TOA (in this acquisition mode, 13 bits are available to count ToA), the value will saturate as well
                           																															if (!bits_slowcontrol_reg_obj.tpExtEn || time_measurements_obj.toa_ns >= 8191*`DELAY_FACTOR_TOA) column_content_breakdown.superpixel[ss][sis].longtoa = '1;
																																																										else begin		
														      																																											column_content_breakdown.superpixel[ss][sis].longtoa = time_measurements_obj.toa_ns/`DELAY_FACTOR_TOA;
																																																															// If the expected hit flag for this superpixel is 1, it means that at least one of the two discriminator bits that are pulsed is not masked. We check if some is masked and,
																																																															// if necessary, we reduce the computed value of ToA to subtract the contribution of the masked front-end
																																																															if (config_matrix_obj.mask[column_number][ss][sis][config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_0]) begin
																																											 																							// this bit corresponds to the first pulse that arrives within the shutter, so if it's masked, the value of ToA will be fixed by the arrival of the second pulse, which occurs in bit_applydiscpulse_1
																																																																			if (time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1) column_content_breakdown.superpixel[ss][sis].longtoa -= time_measurements_obj.time_separation_between_pulses_in_two_disc_bits/`DELAY_FACTOR_TOA;
																																																																			else column_content_breakdown.superpixel[ss][sis].longtoa -= (time_measurements_obj.duration_first_pulse_in_disc_bits + time_measurements_obj.time_separation_between_pulses_in_two_disc_bits)/`DELAY_FACTOR_TOA;
																																																															end
																			  																																										// If bit_applydiscpulse_1 is masked instead, it makes no difference in terms of ToA, because it's the second pulse to arrive
																																																										end
																																																				end
																																																				// Photon counting acquisition mode
																																																				else if (bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin

																														       																						if (time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1 && !config_matrix_obj.mask[column_number][ss][sis][config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_0]) begin
																																					    																						// If the discriminator bit that's pulsed first is not masked, the count will be 1
																																																															column_content_breakdown.superpixel[ss][sis].photoncount = 13'd1;
																																																											end
																																																											else begin
																																					    																						// If none of the discriminator bits that are pulsed is masked, the count will be 2
																																																															if ( !config_matrix_obj.mask[column_number][ss][sis][config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_0] &&
																																									     																						!config_matrix_obj.mask[column_number][ss][sis][config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_1] ) 
																																									     																						column_content_breakdown.superpixel[ss][sis].photoncount = 13'd2;
																																																															// Otherwise, since the expected hit flag is 1, it means at least one of the discriminator bits won't be masked, so there will be one pulse to count
																																																															else column_content_breakdown.superpixel[ss][sis].photoncount = 13'd1;
																																																											end

																																																				end





																															end: force_disc_bit_not_skipped_for_compression

																															// --------------------------------------------------------------------------------------------------


																															// Hit map - this is different from zero as soon as some hit is detected at some discriminator bit (which is not masked), regardless if a digital test pulse is applied simultaneously

																															for (int i = 0; i < `PIXELS_IN_ONE_SUPERPIXEL; i++) begin
																															     
																																				if (bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
																																				     // In photon counting mode, only the first pulse to arrive in the discriminator bits contributes to the hit map (if not masked)
																																				     // If the discriminator bit we're pulsing is not masked and the duration of the pulse is longer than the minimum detectable, there will be a hit indicated at the hit map
																																									if (i == config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_0 && !config_matrix_obj.mask[column_number][ss][sis][i] && time_measurements_obj.tot_ns >= `DELAY_FACTOR_TOT)
																																									    column_content_breakdown.superpixel[ss][sis].hit_map[i] = 1'b1;
																																													// NOTE: IF TOT < `DELAY_FACTOR_TOT, WE CAN'T GUARANTEE THAT THIS WILL BE THE OBTAINED HIT MAP; it may be all 0, or perhaps there will be a '1' at some of the disc bits fired if they're not masked (perhaps even at both if not masked)
																																									else column_content_breakdown.superpixel[ss][sis].hit_map[i] = 1'b0;
																																				end
																																				else begin
																																									// In any other acquisition mode, both discriminator bits contribute to the hit map
																																									// If the discriminator bit we're pulsing is not masked and the duration of the pulse is longer than the minimum detectable, there will be a hit indicated at the hit map
																																									if (i == config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_0 && !config_matrix_obj.mask[column_number][ss][sis][i] && time_measurements_obj.tot_ns >= `DELAY_FACTOR_TOT ||
																																													i == config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_1 && !config_matrix_obj.mask[column_number][ss][sis][i] && time_measurements_obj.tot_ns >= `DELAY_FACTOR_TOT)
																																													column_content_breakdown.superpixel[ss][sis].hit_map[i] = 1'b1;
																																													// NOTE: IF TOT < `DELAY_FACTOR_TOT, WE CAN'T GUARANTEE THAT THIS WILL BE THE OBTAINED HIT MAP; it may be all 0, or perhaps there will be a '1' at some of the disc bits fired if they're not masked (perhaps even at both if not masked)
																																									else column_content_breakdown.superpixel[ss][sis].hit_map[i] = 1'b0;
																																				end
																															end


																																$display("DISPLAY - BUILD_EXPECTED_MEAS_FOR_COVERAGE - mask[%0d][%0d][%0d] = %h, bit_applydiscpulse_0 = %0d, bit_applydiscpulse_1 = %0d, hit map = %h, mask bit 0 = %b, mask bit 1 = %b",column_number,ss,sis,config_matrix_obj.mask[column_number][ss][sis],
																																config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_0, config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_1, column_content_breakdown.superpixel[ss][sis].hit_map,
																																config_matrix_obj.mask[column_number][ss][sis][config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_0],
																																config_matrix_obj.mask[column_number][ss][sis][config_matrix_obj.superpixels_to_pulse[coordinates_pulsed[current_coordinate]].bit_applydiscpulse_1]);	
																																		
													end: coordinate_is_pulsed																								
										end: loop_superpixel_in_segment
					end: loop_superpixel_segment
					
								
endfunction: build_expected_measurements_from_matrixconfig_obj_for_coverage
