// Filename           : CLICTD_generate_verilog_to_include.sv
// Author             : N�ria Egidos 
// Created on         : 25/10/18
// Last modification  : 25/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Wrapper to connect the pads and the probed internal signals to the UVM framework - for P&R netlist


// packed arrays: https://electronics.stackexchange.com/questions/101821/instantiating-multidimensional-array-in-system-verilog

`ifndef __CLICTD_generate_verilog_to_include_sv__
`define __CLICTD_generate_verilog_to_include_sv__

`timescale 1ns / 1ps


`include "search_replace.sv"


`include "CLICTD_parameters.sv"
`include "generate_assign_internalconfigsignals_to_pinsif.sv"
`include "generate_sdfAssignments.sv"
`include "generate_stringreplace_clictdoutputsdf.sv"
`include "generate_clictdtop_i2cworkaround_somecolpnr.sv"




module CLICTD_generate_verilog_to_include;

  bit [`COLUMNS-1:0] column_is_rtl;

  //rand bit [`COLUMNS-1:0] column_is_rtl;
  //constraint number_ones_in_columnisrtl { column_is_rtl.sum() > 0; column_is_rtl.sum() < `COLUMNS; }

				
	initial begin
						
						`include "which_column_is_rtl.sv"
						
						$display ("%t CLICTD_generate_verilog_to_include - Started generate_assign_internalconfigsignals_to_pinsif", $time);
      generate_assign_internalconfigsignals_to_pinsif (.column_is_rtl(column_is_rtl));
						$display ("%t CLICTD_generate_verilog_to_include - Finished generate_assign_internalconfigsignals_to_pinsif", $time);
						$display ("%t CLICTD_generate_verilog_to_include - Started generate_clictdtop_i2cworkaround_somecolpnr", $time);
						generate_clictdtop_i2cworkaround_somecolpnr (.column_is_rtl(column_is_rtl));
						$display ("%t CLICTD_generate_verilog_to_include - Finished generate_clictdtop_i2cworkaround_somecolpnr", $time);
						$display ("%t CLICTD_generate_verilog_to_include - Started generate_stringreplace_clictdoutputsdf", $time);
      generate_stringreplace_clictdoutputsdf(); // no need to generate every time, it's very slow
						$display ("%t CLICTD_generate_verilog_to_include - Finished generate_stringreplace_clictdoutputsdf", $time);
						$display ("%t CLICTD_generate_verilog_to_include - Started generate_sdfAssignments", $time);
      generate_sdfAssignments(.column_is_rtl(column_is_rtl)); // using the generated clictd_output_replaced
						$display ("%t CLICTD_generate_verilog_to_include - Finished generate_sdfAssignments", $time);
      
			   $stop;
	end
                
endmodule: CLICTD_generate_verilog_to_include



`endif // __CLICTD_generate_verilog_to_include_sv__
