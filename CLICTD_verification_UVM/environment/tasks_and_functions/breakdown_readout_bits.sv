// Filename           : breakdown_readout_bits.sv
// Author             : Núria Egidos 
// Created on         : 13/8/18
// Last modification  : 16/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : This code is to be added to the CLICTD_dataout_monitor or to the auxiliary testbench tbaux_readout, 
//                      which tests the task in charge of indentifying the fields in the readout

// https://stackoverflow.com/questions/17102701/how-to-detect-the-posedge-of-two-clocks-asynchronous-to-each-other-at-the-same

// need this? https://verificationacademy.com/forums/systemverilog/fork-within-loop-join-all
// timeout: https://verificationacademy.com/forums/systemverilog/wait-signal-value-task-timeout


// If we don't use ref in the signature (list of arguments), the task will take the value of this inputs at the moment when it was called,
// instead of updating it when the inputs change:
//task breakdown_readout_bits (input chip_status_type chip_status, input logic DATA_OUT, input logic CLK_OUT, input logic ENABLE_OUT_pad);
// Using ref, the input arguments are updated as the signals they are connected to change their value in time:
//task breakdown_readout_bits (ref chip_status_type chip_status, logic DATA_OUT, logic CLK_OUT, logic ENABLE_OUT_pad);


//task automatic breakdown_readout_bits (ref logic DATA_OUT, input string testname);
task automatic breakdown_readout_bits (ref logic DATA_OUT);

//task breakdown_readout_bits (ref logic DATA_OUT);
  
		
		logic [21:0] aux_vector; logic [21:0] aux_header; // auxiliary vector
		
		
		
  logic [`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED*`ROWS  -1:0] aux_vector_columncontent; 
  
		
		real auxiliary_time;
  real risetime_CLKOUT;
  real risetime_ENABLEOUTpad;
		
		int counter_ones_in_columncontent;
		

  //int i;
  logic auxiliary_buffer; // to avoid missing the first bit sent via DATA_OUT

  int expected_min_number_of_bits_column_content;
  int expected_max_number_of_bits_column_content;
  int expected_bits_readout;
		
		 bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED;


  if (testname == `READOUT_TEST_FOR_COVERAGE) expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED = config_matrix_obj_for_coverage.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED;
		else expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED = config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED;


  aux_vector_columncontent = '0;
  //accumulated_column_content = '0;
		counter_ones_in_columncontent = 0;

		if     ( (chip_status_obj.chip_status == STATUS_SECOND_READOUT_ALL_ZEROS) || (chip_status_obj.chip_status == STATUS_CONFIG_READOUT_FIRST_HALF) || (chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF) ) begin
						expected_min_number_of_bits_column_content =  (`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED)*(`ROWS);
						expected_max_number_of_bits_column_content = expected_min_number_of_bits_column_content;
						expected_bits_readout = `NUMBER_BITS_START_HEADER + `COLUMNS*(`NUMBER_BITS_COLUMN_HEADER + expected_min_number_of_bits_column_content) + `NUMBER_BITS_STOP_HEADER;
		end
  else begin					  
						expected_min_number_of_bits_column_content = `ROWS; // 1 bit per superpixel when compression is enabled and no pixel has been hit
						expected_max_number_of_bits_column_content = (`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED)*(`ROWS); // all bits from all superpixels
						expected_bits_readout = 0;
						for (int c = 0; c < `COLUMNS; c++) begin
											for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						    					for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
						         					if (expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis] || bits_slowcontrol_reg_obj.noCompr) expected_bits_readout = expected_bits_readout + `NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED;
																				else if (!expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis] && !bits_slowcontrol_reg_obj.noCompr) expected_bits_readout = expected_bits_readout + 1;
															end
											end
						end
						expected_bits_readout = expected_bits_readout + `COLUMNS*`NUMBER_BITS_COLUMN_HEADER + `NUMBER_BITS_START_HEADER + `NUMBER_BITS_STOP_HEADER;
		end



        // ################### START CONDITION ################## //                
        if (readout_status == STATUS_IDLE) begin: start_condition

													auxiliary_time = $realtime;

										      // Detect start condition
                fork 
                begin
						              events_obj.rise_enableoutpad_event.wait_trigger();
																				// Both signals have risen within the timeout margin
																				`uvm_info("Protocol info", $sformatf("Start readout condition detected - rise clkout = %f, rise enable = %f, aux time = %f, chip_status = %s",risetime_CLKOUT,risetime_ENABLEOUTpad,auxiliary_time, chip_status_obj.chip_status),  UVM_LOW);
                    risetime_CLKOUT = -1;
																				risetime_ENABLEOUTpad = -1;
																				auxiliary_time = -1;

						              // it was observed in simulation that DATA_OUT doesn't rise exactly at the same time as CLK_OUT and ENABLE_OUT_pad, but slightly later;
						              // we wait until the falling edge of CLK_OUT to capture the right value of DATA_OUT
						              events_obj.fall_clkout_event.wait_trigger();
						              readout_status = STATUS_START_HEADER;
                end
                begin
                     #(`TIMEOUT_START_READOUT_CONDITION);
																					readout_ongoing = `NO; // if we place it before the wait, it blocks execution all the time
																					events_obj.timeout_start_readout_event.trigger();  

				            end
                join_any
                disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
																// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
																// which could otherwise lead to unexpected behavior (messages printing twice...)
																//if (chip_status_obj.chip_status != STATUS_SECOND_READOUT_ALL_ZEROS) $display("%t readout ongoing = %0d",$realtime,readout_ongoing);
										      
								end: start_condition
                
								// ################### START HEADER ################## //  
								else if (readout_status == STATUS_START_HEADER) begin: start_header

                     events_obj.rise_clkout_event.wait_trigger();


																				 accumulated_readout_bits[aux_bit_counter] = DATA_OUT; // in this associative array we accumulate all of the bits that have been read out
																				 // from the last time we emptied it until the current bit (including the current bit in the most significant position)
                                         //`ifdef ENABLE_REPORTING_SECOND_READOUT_ALL_ZEROS 
  																			 if (reporting_enable_obj.enable_reporting_breakdownreadoutbits == `YES) begin 
																								`uvm_info("Protocol info", $sformatf("Reading start header, aux_bit_counter = %0d, bit_readout_counter = %0d, DATA_OUT = %0d, chip_status = %s", aux_bit_counter, bit_readout_counter, DATA_OUT, chip_status_obj.chip_status),  UVM_LOW);
                 												 //`endif
 																			   end

																				
																		     // aux_bit_counter is an auxiliary counter to keep track of the number of bits being read out for a particular field,
																		     // in this case the start header
																		     if (aux_bit_counter == `NUMBER_BITS_START_HEADER-1) begin: reached_startheader_bits
																		        // aux_vector is an auxiliary vector to translate accumulated_readout_bits (at this moment, we know that the amount
																		        // of bits we are going to retrieve from accumulated_readout_bits corresponds to the number of bits in the start header)
																		        // to a packed array, which will be then compared to the value of `HEADER_START. This conversion is required for the 
																		        // compiler to perform such comparison.
																		        for (int i = 0; i < `NUMBER_BITS_START_HEADER; i++) aux_vector[i] = accumulated_readout_bits[`NUMBER_BITS_START_HEADER-1-i];
																						
																		        if (aux_vector != `HEADER_START) begin		
																							 `uvm_error( `ERROR_READOUT_PROTOCOL, $sformatf("Start header not detected, chip_status = %s", chip_status_obj.chip_status) )
																						end
																		        else begin
																		            // we store the read start header in the transaction (start_header field), bit by bit from those stored in accumulated_readout_bits
																		            for (int i = 0; i < `NUMBER_BITS_START_HEADER; i++) dataout_transaction.start_header[i] = accumulated_readout_bits[`NUMBER_BITS_START_HEADER-1-i];
																		            //aux_bit_counter = 0; // we reset this auxiliary counter because we have already read all the bits corresponding to the start header; then
																		            // the counter will be available to track the readout of other fields, such as the column header
																		            //readout_status = STATUS_COLUMN0_HEADER;
																						    // Empty the auxiliary vector to stored the read bits
																						    //accumulated_readout_bits.delete();
																		            // We don't write any transaction to transfer the start header, protocol-related fields
																		            // are not evaluated at the scoreboard
																								//`ifdef ENABLE_REPORTING_SECOND_READOUT_ALL_ZEROS 
																								if (reporting_enable_obj.enable_reporting_breakdownreadoutbits == `YES) begin 
																											 `uvm_info("Protocol info", $sformatf("Start header detected, chip_status = %s", chip_status_obj.chip_status),  UVM_LOW);
                                                //`endif
																								end
																		        end 

                                            readout_status = STATUS_COLUMN0_HEADER;
																						// Empty the auxiliary vector to stored the read bits
																						accumulated_readout_bits.delete();
																						
																		     end: reached_startheader_bits
																					
																				 if      (readout_status == STATUS_START_HEADER ) aux_bit_counter = aux_bit_counter + 1;  
																				 else if (readout_status == STATUS_COLUMN0_HEADER) aux_bit_counter = 0; // we have already read all the bits corresponding to the start header;
                                         // clear to start the count of column header bits
																				 bit_readout_counter = bit_readout_counter + 1;																				 


												 //end: startheader_posedge_clkout
								end

								// ################### COLUMN 0 HEADER ################## //  
								else if (readout_status == STATUS_COLUMN0_HEADER) begin: column0_header

                         events_obj.rise_clkout_event.wait_trigger();

																				     accumulated_readout_bits[aux_bit_counter] = DATA_OUT; // in this associative array we accumulate all of the bits that have been read out
																				     // from the last time we emptied it until the current bit (including the current bit in the most significant position)
																				     if (reporting_enable_obj.enable_reporting_breakdownreadoutbits == `YES) begin 
																							      `uvm_info("Protocol info", $sformatf("Reading column 0 header, aux_bit_counter = %0d, bit_readout_counter = %0d, DATA_OUT = %0d, chip_status = %s", aux_bit_counter, bit_readout_counter, DATA_OUT, chip_status_obj.chip_status),  UVM_LOW);
																         end

																					
																		     		if (aux_bit_counter == `NUMBER_BITS_COLUMN_HEADER-1) begin: reached_columnheader_bits
																		         		aux_vector = `HEADER_COLUMN_0;
																		         		// In this slice of accumulated_readout_bits, we expect to detect the header of column 0. We compare (XOR) each bit of the expected header
																		         		// with the bits stored in accumulated_readout_bits (the result should be 0 in the equal positions, 1 in the positions that are different)
																		         		// and store the result of the comparison in the same aux_vector. 
																		         		for (int i = 0; i < `NUMBER_BITS_COLUMN_HEADER; i++) begin 
                                  aux_header[i] = accumulated_readout_bits[`NUMBER_BITS_COLUMN_HEADER-1-i];  
                                  aux_vector[i] = accumulated_readout_bits[`NUMBER_BITS_COLUMN_HEADER-1-i] ^ aux_vector[i];
                             end
																		         		// If we then OR all the positions in aux_vector (see |aux_vector below) and obtain a 1, it means that some of the stored bits don't match 
																						 	    	// the column 0 header bits. 
																		          	if ( column_counter == 0  && (|aux_vector) ) begin
																							  		       `uvm_error( `ERROR_READOUT_PROTOCOL, $sformatf("Column header not detected or incorrect, header = %h, column = %0d, chip_status = %s", aux_header, column_counter, chip_status_obj.chip_status) )
																				      		 end
																				      		 // Regardless if we have detected the right header ot not, we store the bits in accumulated_readout_bits into the transaction column_header field and move on to read the column content
																				         aux_bit_counter = 0;
																				         readout_status = STATUS_CONTENT0_AND_OTHERCOLUMNS;
		                           for (int i = 0; i <`NUMBER_BITS_COLUMN_HEADER; i++) dataout_transaction.column_header[i] = accumulated_readout_bits[`NUMBER_BITS_COLUMN_HEADER-1-i];
																								  			// Empty the auxiliary vector to stored the read bits
																								  			accumulated_readout_bits.delete();
																											 	if (reporting_enable_obj.enable_reporting_breakdownreadoutbits == `YES) begin  
																														`uvm_info("Protocol info", $sformatf("Column header detected, header = %h, column = %0d, chip_status = %s", dataout_transaction.column_header, column_counter, chip_status_obj.chip_status), UVM_LOW )
		                          	end
																		     		end: reached_columnheader_bits

																					
																				 				if      (readout_status == STATUS_COLUMN0_HEADER ) aux_bit_counter = aux_bit_counter + 1;  
																				 				else if (readout_status == STATUS_CONTENT0_AND_OTHERCOLUMNS) aux_bit_counter = 0; // we have already read all the bits corresponding to the start header;
                                         				// clear to start the count of column header bits
																				 				bit_readout_counter = bit_readout_counter + 1;		

								
								end: column0_header

								// ################### COLUMN 0 CONTENT AND OTHER COLUMNS HEADER AND CONTENT ################## //  
								else if (readout_status == STATUS_CONTENT0_AND_OTHERCOLUMNS) begin: content0_and_others

																													 
												         events_obj.rise_clkout_event.wait_trigger();
					
																				 accumulated_readout_bits[aux_bit_counter] = DATA_OUT; // in this associative array we accumulate all of the bits that have been read out
																				 // from the last time we emptied it until the current bit (including the current bit in the most significant position)
                     //accumulated_column_content = {accumulated_column_content[`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED*`ROWS-2:0], DATA_OUT}; // the next column header will stay at the LSB positions, current column content at MSB positions

																			 	accumulated_column_content = {accumulated_column_content, DATA_OUT}; // the MSB is automatically cropped
 
																				 if (reporting_enable_obj.enable_reporting_breakdownreadoutbits == `YES) begin 
																								`uvm_info("Protocol info", $sformatf("Reading column = %0d, aux_bit_counter = %0d, bit_readout_counter = %0d, DATA_OUT = %b, accumulated_column_content = %h, chip_status = %s", column_counter,aux_bit_counter, bit_readout_counter,
																								 DATA_OUT,accumulated_column_content, chip_status_obj.chip_status), UVM_LOW )
																				 end
																					
															
																					// We expect to read a minimum number of bits per column: if chip_status is STATUS_SECOND_READOUT_ALL_ZEROS,
																					// we will read `NUMBER_BITS_READOUT_PER_COLUMN_COMPRESSION_DISABLED zeros; if chip_status is STATUS_CONFIG_READOUT_FIRST_HALF 
																					// or STATUS_CONFIG_READOUT_SECOND_HALF, we will read  `NUMBER_BITS_READOUT_PER_COLUMN_COMPRESSION_DISABLED 
 																				// corresponding to the status of the pixel configuration flip-flops at that time; if chip_status is STATUS_MATRIX_READOUT_ACQUIRED,
                     // we will read a minimum of `SUPERPIXELS_IN_ONE_COLUMN bits per column (if readout compression is 
												         // enabled, we will read 1 bit per superpixel if it hasn't been hit; in any other case, we will read 22
												         // bits per superpixel). If aux_bit_counter < expected_min_number_of_bits_column_content - 1 in each case and we detect a header,
												         // we raise an error
												         if (aux_bit_counter == (expected_min_number_of_bits_column_content + `NUMBER_BITS_COLUMN_HEADER) -2 ) begin: content_within_minnumbits
																											// when window stores the header of the next column or the stop header, we either switch to reading a new column or stop the readout
																										  for (int i = 0; i < `NUMBER_BITS_START_HEADER; i++)  window[i] = accumulated_readout_bits[aux_bit_counter-i];
																												if (reporting_enable_obj.enable_reporting_breakdownreadoutbits == `YES) begin 
																																 `uvm_info("Protocol info", $sformatf("Column = %0d content is 1 bit from minimum number of bits, aux_bit_counter %0d, window = %h, chip_status = %s", column_counter,aux_bit_counter,window, chip_status_obj.chip_status), UVM_LOW )
																												end
												                // If the window matches some of the headers, we raise an error, because we cannot detect a header while we haven't detected the minimum number of content bits,
																												// but we proceed with the readout anyway 
																											 aux_vector = `HEADER_COLUMN_0; // we use aux_vector to store the value of a column header (doesn't matter which)
												                                               // in order to compare it with the first bits of the stored window below, so that the compiler accepts the syntax
																												if ( (window == `HEADER_START) || (window == `HEADER_STOP) || (window[21:6] == aux_vector[21:6]) ) begin
																													 `uvm_error( `ERROR_READOUT_PROTOCOL, $sformatf("Next header detected when column = %0d content was expected, minimum number of content bits not yet reached, window = %h, chip_status = %s", column_counter,window, chip_status_obj.chip_status) )
																											 end
																											
												          end: content_within_minnumbits

																					
						 															// Once we have surpassed the minimum number of bits to read out, any time we detect a column header, we switch to 
												          // reading the new column; or if we detect the stop header, we conclude the readout
												          else if (aux_bit_counter >= (expected_min_number_of_bits_column_content + `NUMBER_BITS_COLUMN_HEADER) -1 && aux_bit_counter < expected_max_number_of_bits_column_content + `NUMBER_BITS_COLUMN_HEADER ) begin: content_longer_than_minnumbits
																						
																						      //$display("DISPLAY - breakdown readout - window[`NUMBER_BITS_COLUMN_HEADER-2:0] = %h, window = %h, accumulated_readout_bits[%0d] = %b",window[`NUMBER_BITS_COLUMN_HEADER-2:0],window,aux_bit_counter,accumulated_readout_bits[aux_bit_counter]);
																											
												                // At this moment, in accumulated_readout_bits we have the content of one column and the header of the 
												                // following column or the stop header. We store either of the headers in window.
																										 	window = {window[`NUMBER_BITS_COLUMN_HEADER-2:0], accumulated_readout_bits[aux_bit_counter]};
       																					if (reporting_enable_obj.enable_reporting_breakdownreadoutbits == `YES && aux_bit_counter == expected_min_number_of_bits_column_content + `NUMBER_BITS_COLUMN_HEADER -1) begin 
																														 `uvm_info("Protocol info", $sformatf("Column = %0d content has reached the minimum number of bits, aux_bit_counter %0d, window = %h, chip_status = %s", column_counter,aux_bit_counter,window, chip_status_obj.chip_status), UVM_LOW )
																										 	end
																												
												                // Detect column header of the next column 
																											 aux_vector = `HEADER_COLUMN_0;
																												if (window[21:6] == aux_vector[21:6] || window == `HEADER_STOP) begin: detect_nextcolumn_header
																																						if (window[21:6] == aux_vector[21:6] && window[5:2] != (column_counter+1)) begin // it's column header and the column number detected is not the expected value
																																										`uvm_error( `ERROR_READOUT_PROTOCOL, $sformatf("Number in column header = %0d not equal to column number = %0d, column header = %h, chip_status = %s", window[5:2], column_counter, window, chip_status_obj.chip_status) )
																												    						end
																																      //Either if we detect the right column header/stop header or not, we proceed with the readout
																															       // Store the column content (which is located in the lowest part of accumulated_readout_bits) into the transaction field column_content. The column header corresponding to this content
												                          // has already been stored before, either in STATUS_COLUMN0_HEADER or in a former visit to  STATUS_CONTENT0_AND_OTHERCOLUMNS. The content in window at this moment is the column header 
																																						// for the following column (if readout hasn't finished yet)
																																						
																																						//$display("DISPLAY - breakdown readout - size accumulated %0d",accumulated_readout_bits.size());
												                                      
                                      //  How is the data stored in accumulated_readout_bits?
                                      //            21 ------- this column content ------ 0 || 21 -------- next column header or stop header ------ 0
                                      //            0 -------------------------- accumulated_readout_bits ----------------------------------------- 43
                                      for (int i = `NUMBER_BITS_COLUMN_HEADER; i < accumulated_readout_bits.size(); i++) begin
																																				       dataout_transaction.column_content[i-`NUMBER_BITS_COLUMN_HEADER] = accumulated_column_content[i]; 
                                           aux_vector_columncontent[i-`NUMBER_BITS_COLUMN_HEADER] = accumulated_column_content[i];
                                      end
																																						if (chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS ||
																																										chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS) begin
																																										//$display("DISPLAY - breakdown readout - expected hit_flag[%0d] = %h",column_counter,config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[column_counter]);
																																									breakdown_columcontent_into_superpixeldata (.column_content(dataout_transaction.column_content), .bits_slowcontrol_reg_obj(bits_slowcontrol_reg_obj),
																																									.expected_hit_flag(expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[column_counter]),  .superpixels_data(dataout_transaction.column_content_breakdown) );
																																									//$display("DISPLAY - breakdown readout BEFORE - hit_flag[%0d][0][0] = %b, hit_map[%0d][0][0] = %h",dataout_transaction.column_counter,dataout_transaction.column_content_breakdown.superpixel[0][0].hit_flag,
																																									//dataout_transaction.column_counter,dataout_transaction.column_content_breakdown.superpixel[0][0].hit_map);
																																									// At this point, the ToT and ToA values are expressed as the output of LFSR counters. To be able to compare them with the expected values (which are expressed in binary), we convert
																																									// these values into binary
																																							  convert_tottoa_LFSR_to_binary ( .column_content_breakdown_LFSR(dataout_transaction.column_content_breakdown), .column_content_breakdown_binary(dataout_transaction.column_content_breakdown) );
																																									//$display("DISPLAY - breakdown readout AFTER - hit_flag[%0d][0][0] = %b, hit_map[%0d][0][0] = %h",dataout_transaction.column_counter,dataout_transaction.column_content_breakdown.superpixel[0][0].hit_flag,
																																									//dataout_transaction.column_counter,dataout_transaction.column_content_breakdown.superpixel[0][0].hit_map);
																																									
																																									// Rebuild the column content with the values converted to binary
																																									build_expected_columncontent_from_columncontentbreakdown (	.bits_slowcontrol_reg_obj(bits_slowcontrol_reg_obj), .column_content_breakdown(dataout_transaction.column_content_breakdown), .column_content(dataout_transaction.column_content)	); 
																																						end
																																						else if (chip_status_obj.chip_status == STATUS_CONFIG_READOUT_FIRST_HALF) breakdown_columncontent_into_superpixelconfig ( .column_content(dataout_transaction.column_content), .configuration_half(`FIRST_CONFIGURATION_HALF),
																																																																							                                                                                         .superpixels_config_word(dataout_transaction.column_config_breakdown)	);
																																						else if (chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF) 	breakdown_columncontent_into_superpixelconfig ( .column_content(dataout_transaction.column_content), .configuration_half(`SECOND_CONFIGURATION_HALF),
																																																																							                                                                                           .superpixels_config_word(dataout_transaction.column_config_breakdown)	);
																																																																																								
                                      foreach (aux_vector_columncontent[i]) begin counter_ones_in_columncontent += aux_vector_columncontent[i]; end 

																																						if (reporting_enable_obj.enable_reporting_breakdownreadoutbits == `YES) begin  
                                           if (window == `HEADER_STOP) begin
																																																`uvm_info("Protocol info", $sformatf("Column = %0d content detected, #bits = %0d, ones in aux_vector_columncontent = %0d, next column header = %h, chip_status = %s, content = %h \ncontent accumulated = %h",
																																																column_counter, accumulated_readout_bits.size() - `NUMBER_BITS_COLUMN_HEADER, counter_ones_in_columncontent,  window, chip_status_obj.chip_status,aux_vector_columncontent,accumulated_column_content), UVM_LOW )
																																		          				`uvm_info("Protocol info", $sformatf("Stop header detected, chip_status = %s", chip_status_obj.chip_status), UVM_LOW )
           																													   end
																																					 					else begin
																																					    							`uvm_info("Protocol info", $sformatf("Column = %0d content detected, #bits = %0d, ones in aux_vector_columncontent = %0d, next column header = %h, chip_status = %s, content = %h \ncontent accumulated = %h",
																																																	column_counter, accumulated_readout_bits.size() - `NUMBER_BITS_COLUMN_HEADER,counter_ones_in_columncontent,  window, chip_status_obj.chip_status,aux_vector_columncontent,accumulated_column_content), UVM_LOW )
																																					 					end
																																	  			end


																																		 		// Fill the transaction fields with the incoming information
																																		 		dataout_transaction.dataout_bit = DATA_OUT;        // bit to be read out
																																		 		dataout_transaction.bit_readout_counter = bit_readout_counter; // which bit out of the total stream we are reading out
																																		 		dataout_transaction.column_counter = column_counter;           // which column we are reading out

																		                   // Store the next column header (which is located in the highest part of accumulated_readout_bits) into the transaction field column_header
																																					// or stop_header, depending on the value of window at this moment 
                                     if(window == `HEADER_STOP)  begin 
																																					      dataout_transaction.stop_header = window;
																																					      `ifndef DEBUG_MODE
																																																			analysis_port.write(dataout_transaction); 
																																																			// Create next transaction to use, so that modifying the new one doesn't affect the values associated to the 
																																							 											// reference of the former one (otherwise, we could observe comparator errors MISCMP - miscomparison - when
																																							 											// we didn't expect them) 
																																							 											dataout_transaction = CLICTD_dataout_transaction::type_id::create(.name("dataout_transaction"), .contxt(get_full_name()));
																																											`else
																																											        dataout_transaction = CLICTD_dataout_transaction::type_id::create(.name("dataout_transaction"), .contxt(""));
																																											`endif
																																											readout_status = STATUS_DETECT_STOP_READOUT; 
																																							 			
																																					end
																		                   else begin 
																																					     `ifndef DEBUG_MODE
                                                  analysis_port.write(dataout_transaction);
																																																		// Create next transaction to use, so that modifying the new one doesn't affect the values associated to the 
																																							 										// reference of the former one (otherwise, we could observe comparator errors MISCMP - miscomparison - when
																																							 										// we didn't expect them) 
																																							 										dataout_transaction = CLICTD_dataout_transaction::type_id::create(.name("dataout_transaction"), .contxt(get_full_name()));
																																										`else
																																										        dataout_transaction = CLICTD_dataout_transaction::type_id::create(.name("dataout_transaction"), .contxt(""));
																																										`endif
                                          
                                          dataout_transaction.column_header = window; // the current content of window is the header of the next column, if readout hasn't finished
																																										if (reporting_enable_obj.enable_reporting_breakdownreadoutbits == `YES) begin  
																																											`uvm_info("Protocol info", $sformatf("Column header detected, header = %h, column = %0d, chip_status = %s", dataout_transaction.column_header, column_counter, chip_status_obj.chip_status), UVM_LOW )
		                          														end
                                     end
                                     //$display("%t Transaction sent in dataout_monitor, column counter = %0d, content = %h",$time, column_counter,aux_vector_columncontent);


																																					// Clear aux_bit_counter to be able to detect the following field, 
																																					// the column content of the next column
																														  					aux_bit_counter = -1; // it will be incremented +1 below and thus cleared to 0


																																					// Empty the auxiliary vector to store the read bits
																																					accumulated_readout_bits.delete();
												                         aux_vector_columncontent = '0;
     																																accumulated_column_content = '0;
																																					counter_ones_in_columncontent = 0;
                                     column_counter = column_counter + 1;
												                                 
																											 end: detect_nextcolumn_header

												                              

												               // Detect start header, which is not allowed at this point
																											else if (window == `HEADER_START) begin: detect_start_header
																														 `uvm_error( `ERROR_READOUT_PROTOCOL, $sformatf("Start header detected when column or stop header were expected, chip_status = %s",chip_status_obj.chip_status) )
																											end: detect_start_header

																											// Detect a field at the end of the transmission that doesn't correspond to the stop header
																											else if (column_counter == `COLUMNS && window != `HEADER_STOP && ( chip_status_obj.chip_status == STATUS_SECOND_READOUT_ALL_ZEROS || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_FIRST_HALF || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF ||
																											         chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0 ) 
																											         ) begin: stop_header_not_detected
																															    `uvm_error( `ERROR_READOUT_PROTOCOL, $sformatf("Unknown field detected instead of stop header, finishing readout immediately, chip_status = %s",chip_status_obj.chip_status) )
																													       aux_bit_counter = -1;
																						              readout_status = STATUS_DETECT_STOP_READOUT;
																																    column_counter = column_counter + 1;
																											end: stop_header_not_detected

																											// Too many bits are read out as column content
                           //else begin: too_many_bits_content
																											else if (aux_bit_counter >= expected_max_number_of_bits_column_content + `NUMBER_BITS_COLUMN_HEADER) begin: too_many_bits_content
																											         /*
                     												   if (chip_status_obj.chip_status == STATUS_SECOND_READOUT_ALL_ZEROS || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_FIRST_HALF || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF || 
																																				   	chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0 )   begin
																																				
																																				*/
																															        `uvm_error( `ERROR_READOUT_PROTOCOL, $sformatf("Unknown field detected, the number of bits sent as column content may be excessive, finishing readout immediately, chip_status = %s",chip_status_obj.chip_status) )
																													           aux_bit_counter = -1;
																						                  readout_status = STATUS_DETECT_STOP_READOUT;
																															        	column_counter = column_counter + 1;
																													       //end
																											end: too_many_bits_content
																											
																											
												                              
												          end: content_longer_than_minnumbits

																				 aux_bit_counter = aux_bit_counter + 1;  
																				 bit_readout_counter = bit_readout_counter + 1;	


								end: content0_and_others

								// ################### DETECT STOP CONDITION ################## //  
								else if (readout_status == STATUS_DETECT_STOP_READOUT) begin: stop_condition

													auxiliary_time = $realtime;
             events_obj.fall_enableoutpad_event.wait_trigger();
             readout_ongoing = `NO;

													auxiliary_time = $realtime - auxiliary_time;
													if (auxiliary_time > `TIMEOUT_FALLTIME_ENABLEOUT) begin
																	`uvm_error(`ERROR_READOUT_PROTOCOL, $sformatf("Falling edge in ENABLE_OUT_pad took longer to arrive than timeout, delay in arrival = %f, chip_status = %s",auxiliary_time, chip_status_obj.chip_status));
																							
													end
													else begin: detect_protocol_errors

																		`uvm_info("Protocol info", $sformatf("Stop condition detected. Evaluating procotol errors, chip_status = %s", chip_status_obj.chip_status), UVM_LOW )

																		if ( (chip_status_obj.chip_status == STATUS_SECOND_READOUT_ALL_ZEROS || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_FIRST_HALF || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF || 
																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0 ||
																																										chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0  )   &&
                        ( bit_readout_counter != expected_bits_readout ) 	) begin
																					`uvm_error(`ERROR_READOUT_PROTOCOL, $sformatf("Expected bits readout = %0d vs detected bits readout = %0d, chip_status = %s",expected_bits_readout,bit_readout_counter, chip_status_obj.chip_status));
																		end
																
																		else if (column_counter != `COLUMNS) begin
																					//`uvm_error(`ERROR_READOUT_PROTOCOL, $sformatf("Protocol error - Expected columns readout = %0d vs detected columns readout = %0d, chip_status = %s, # CLK_OUT rising edges = %0d",`COLUMNS,column_counter, chip_status_obj.chip_status, counter_clkout_riseedge));
																					`uvm_error(`ERROR_READOUT_PROTOCOL, $sformatf("Protocol error - Expected columns readout = %0d vs detected columns readout = %0d, chip_status = %s",`COLUMNS,column_counter, chip_status_obj.chip_status));
																		end

																		else begin
																				//`uvm_info("Protocol info", $sformatf("No protocol error detected. Readout finished correctly :), chip_status = %s, # CLK_OUT rising edges = %0d", chip_status_obj.chip_status, counter_clkout_riseedge), UVM_LOW )
																				`uvm_info("Protocol info", $sformatf("No protocol error detected. Readout finished correctly :), chip_status = %s", chip_status_obj.chip_status), UVM_LOW )
																		end

																		bit_readout_counter = 0;
																		aux_bit_counter = 0;
																		column_counter = 0;
																		readout_status = STATUS_IDLE;
																		window = '0;
																		// Empty the auxiliary vector to store the read bits
																		accumulated_readout_bits.delete();
																		accumulated_column_content = '0;
													end: detect_protocol_errors
													auxiliary_time = -1; // reset for the next readout operation
										    
								end: stop_condition





               
endtask: breakdown_readout_bits   

