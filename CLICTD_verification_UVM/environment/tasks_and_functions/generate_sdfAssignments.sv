// Filename           : generate_sdfAssignments.sv
// Author             : Núria Egidos 
// Created on         : 24/10/18
// Last modification  : 18/1/19
// Project            : Doctoral student, verification of CLICTD
// Description        : Script to generate the "connection" between the different blocks in the top level postlayout netlist and the corresponding sdf files, in order to perform a back annotated simulation.




function void copy_columnoutputsdf_to_localfolder ();

  string line, line_before;
		string first_flag_to_detect,second_flag_to_detect,third_flag_to_detect,fourth_flag_to_detect;
		bit first_flag_detected,second_flag_detected;
  
  int exit_code, counter;
  int f_original,f_local;
		bit end_condition_detected;
				
		f_original = $fopen(`PATH_ORIGINAL_COLUMNOUTPUT_SDF,"r");  
  f_local = $fopen(`PATH_LOCAL_COLUMNOUTPUT_SDF,"w");  
  
		end_condition_detected = `NO;
		first_flag_to_detect = "	)\n";
		second_flag_to_detect = "      )\n";
		third_flag_to_detect = "  )\n";
		fourth_flag_to_detect = ")\n";
		first_flag_detected = `NO;
		second_flag_detected = `NO;

		counter = 0;
		
		while (end_condition_detected == `NO) begin
  //while (!$feof(f)) begin // deadlock if the file has a new line, empty line at the end of the file (it gets stuck and execution doesn't finish) :(
         //exit_code = $fscanf (f, "%s", line);
									
         $fgets(line, f_original);
         $fwrite(f_local,"%s",line);
									
									// Detect stop condition: first_flag_to_detect, second_flag_to_detect, third_flag_to_detect and fourth_flag_to_detect arrive in a row
									if (counter > 0) begin
									    if      (line == second_flag_to_detect && line_before == first_flag_to_detect)                                 first_flag_detected = `YES;
													else if (first_flag_detected == `YES && line == third_flag_to_detect && line_before == second_flag_to_detect)  second_flag_detected = `YES;
													else if (second_flag_detected == `YES && line == fourth_flag_to_detect && line_before == third_flag_to_detect) end_condition_detected = `YES;
									end
									//$display("SCANNING %d, line = %s",counter, line);
									line_before = line;
									counter++;
									
  end 
  
		$fclose(f_original);
  $fclose(f_local);

endfunction: copy_columnoutputsdf_to_localfolder




function void generate_sdfAssignments (input logic [`COLUMNS-1:0] column_is_rtl);
//function void generate_sdfAssignments (input logic [`COLUMNS-1:0] column_is_rtl, string sdf_corner);
// sdf_corner = "sdfmin", "sdftyp", "sdfmax" 


    string s;
    integer f;
				
				// We copy column_output.sdf from the repository to my local folder sim_annotated because vsim needs to have read access on the file to be able to generate from it column_output.sdf_min.cds, column_output.sdf_typ.cds and column_output.sdf_max.cds,
				// which is the binary compilation of the column_output.sdf for each corner in order to perform the back annotated simulation
				
				
  copy_columnoutputsdf_to_localfolder();

		//f = $fopen({`PATH_SDFASSIGNMENTS_FILE,"_",sdf_corner},"w");
  f = $fopen(`PATH_SDFASSIGNMENTS_FILE,"w");  
    
   
  // https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim/sdfAssignments 
		for (int column = 0; column < `COLUMNS; column = column + 1) begin
       //if (column_is_rtl[column] == `NO)  s = $sformatf("/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/\\\\column_gen[%0d].column_gen.column\\t=/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/netlistPostLayout/column_output.sdf",column);
        // On the four backslahes in front of column_gen[%0d]: two are required for system verilog to print one backslash (it's a special character); so four are used so that 2 backslashes will be printed, which will then be interpreted as a single backslash when the file is read with launcher.do
        // Note: column_gen instead of \\column_gen is used to avoid compilation issues with $sdf_annotate when column.sdf is used;
        // but the name without \ in front cannot have "." inbetween, so column_gen[].column_gen.column is changed to column 
        
        if (column_is_rtl[column] == `NO) begin
								    s = $sformatf("/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column%0d=%s",column,`PATH_LOCAL_COLUMNOUTPUT_SDF);
            $fwrite(f,"%s\n",s);
								end
		end 
  //s = "/CLICTD_testbench_top/dut_wrapper/dut=/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/netlistPostLayout/clictd_output.sdf";
  s = $sformatf("/CLICTD_testbench_top/dut_wrapper/dut=%s",`PATH_REPLACED_CLICTDOUTPUT_SDF);
  $fwrite(f,"%s",s); // Important: no new line at the end of the file!
  
  
  /*
  // https://www.microsemi.com/document-portal/doc_view/131619-modelsim-user, page 516
  // vsim -sdfmax /system/u1=asic1.sdf -sdfmax /system/u2=asic2.sdf system
  s = "";
  for (int column = 0; column < `COLUMNS; column = column + 1) begin
				   if (column_is_rtl[column] == `NO)  s = {s, " -", sdf_corner, " ",
                                               $sformatf("/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/\column_gen[%0d].column_gen.column=RTL_PATH/column_output.sdf",column)
                                               };
		end 
  //s = {s, " -", sdf_corner, " ", "/CLICTD_testbench_top/dut_wrapper/dut=RTL_PATH/clictd_output.sdf"};
  s = {s, " -", sdf_corner, " ", "/CLICTD_testbench_top/dut_wrapper/dut=RTL_PATH/clictd_output_replaced.sdf"};
  $fwrite(f,"%s",s); 
  */
  
		$fclose(f);


endfunction: generate_sdfAssignments
