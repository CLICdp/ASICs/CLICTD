// Filename           : dataoutmonitor_action_on_chipstatus.sv
// Author             : Núria Egidos 
// Created on         : 21/8/18
// Last modification  : 21/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : This code is to be added to the CLICTD_dataout_monitor; this task performs the required monitoring actions for readout_interface




task dataoutmonitor_action_on_chipstatus ();


		forever begin: forever_begin



           
        // Retrieve the chip status from the database; it is set (and changed as the test progresses) from the virtual sequence 
    // in its body() task, so by this point it should be available to be retrieved
    //if( !uvm_config_db#(chip_status_type)::get(this, "", "chip_status", chip_status) )
    //     `uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )
         
          `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);

          // ------------------------------------------------------------------------------------------ //
				  if (chip_status_obj.chip_status == STATUS_RESET) begin: if_status_reset 

							// Sample the value of pins_interface when the reset arrives
							//@(negedge pins_aux_vif.RSTN_pad) begin
              @(posedge pins_aux_vif.RSTN_pad) begin
								#(`WAIT_A_LITTLE_BIT_NUMTIMEUNITS); // wait some time for signals to stabilize 
								// (less time than the duration of the reset pulse)
								dataout_transaction.dataout_bit = readout_vif.DATA_OUT;
				        // At this point, the transaction is complete, so it's written 
								// to the analysis port
								monitor_analysisport.write(dataout_transaction);

                //`ifdef ENABLE_WORKAROUND_CHIPSTATUS_UPDATED_FROM_DATAOUTMONITOR 
								//				chip_status_obj.chip_status = STATUS_FIRST_READOUT_UNDEFINED;
								//`endif
                chip_status_obj.chip_status_update_event.wait_trigger(); // wait for the virtual sequence to 
                // update chip_status before performing any new action
							end


		 			end: if_status_reset
          // ------------------------------------------------------------------------------------------ //
          
          else if (chip_status_obj.chip_status == STATUS_FIRST_READOUT_UNDEFINED) begin: if_first_readout
               //rise_enableoutpad_event.wait_trigger();
               chip_status_obj.chip_status_update_event.wait_trigger(); // wait for the virtual sequence to 
                // update chip_status before performing any new action
          end: if_first_readout
          
          // ------------------------------------------------------------------------------------------ //
          
          else if (chip_status_obj.chip_status == STATUS_SECOND_READOUT_ALL_ZEROS) begin: if_second_readout
               // Break down the serial stream of the serial readout into headers and column contents
               // and send the column contents to the dataout_comparator (issues concerning the protocol
               // are addressed by the monitor, not by the comparator)
								//`ifdef ENABLE_WORKAROUND_CHIPSTATUS_UPDATED_FROM_DATAOUTMONITOR
								//			// Add some delay to synchronize the execution of the task with the arrival of the start condition
								//			@($realtime == 7692687)
								//`endif
                //breakdown_readout_bits( chip_status_obj.chip_status, readout_vif.DATA_OUT, readout_vif.CLK_OUT, pins_aux_vif.ENABLE_OUT_pad); 
                breakdown_readout_bits (.chip_status(chip_status_obj.chip_status), .DATA_OUT(readout_vif.DATA_OUT), .CLK_OUT(readout_vif.CLK_OUT), .ENABLE_OUT_pad(pins_aux_vif.ENABLE_OUT_pad));
                //chip_status_obj.chip_status_update_event.wait_trigger(); // wait for the virtual sequence to 
                // update chip_status before performing any new action
                
                

          end: if_second_readout

          //else if (chip_status == STATUS_CONFIG_READOUT_FIRST_HALF )
          //else if (chip_status == STATUS_CONFIG_READOUT_SECOND_HALF)


    end: forever_begin

endtask: dataoutmonitor_action_on_chipstatus
