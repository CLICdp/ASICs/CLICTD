// Filename           : generate_clictdtop_i2cworkaround_somecolpnr.sv
// Author             : N�ria Egidos 
// Created on         : 7/11/18
// Last modification  : 7/11/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Script to generate clictdtop_i2cworkaround_somecolpnr.v from clictdtp_export.v by replacing some fields 


// System verilog strings: http://www.asicguru.com/system-verilog/tutorial/regular-expression/76/
// https://www.csee.umbc.edu/portal/help/VHDL/verilog/system.html

//`include "search_replace.sv"


function void generate_clictdtop_i2cworkaround_somecolpnr (input logic [`COLUMNS-1:0] column_is_rtl);

  string line, line_before;
		bit clictdTop_detected, replacing_in_clictdTop;
  int counter;
  int f,f_replaced;
		f = $fopen(`PATH_ORIGINAL_CLICTDNETLIST_POSTLAYOUT,"r");  
  f_replaced = $fopen(`FILE_TOPLEVEL_POSTLAYOUT_NETLIST_WITH_SOMECOL_PNR_I2CWORKAROUND,"w");  
		
		
		
		//$fwrite(f_replaced,"`include \"/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout/analogPeri.v\"",line);
		//$fwrite(f_replaced,"`include \"/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout/column_export.v\"",line);
		//$fwrite(f_replaced,"`include \"/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout/lvds.v\"",line);
		//$fwrite(f_replaced,"`include \"/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout/pads.v\"",line);
		//$fwrite(f_replaced,"`include \"/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout/modules.v\"",line);
 
  counter = 0;
		clictdTop_detected = `NO;
		replacing_in_clictdTop = `NO;
  // Read all lines in f and replace some of them as indicated below
  while (!$feof(f)) begin // deadlock if the file has a new line, empty line at the end of the file (it gets stuck and execution doesn't finish) :( --> that's the reason for the "break" instruction below
		//while (line != "endmodule\n" && clictdTop_detected != `YES) begin
		//do begin
         $fgets(line, f);
									line_before = line;
									if ( line == "module clictdTop (\n" ) clictdTop_detected = `YES;
									
									// Replace path of `include files
									
									
									// Replace column names column_gen[X].column_gen.column by ColumnX (the \ in the name is not supported for back annotation) and replace column by column_RTL if it applies, according to column_is_rtl
									for (int column = 0; column < `COLUMNS; column = column + 1) begin
									     if ( column_is_rtl[column] == `YES ) begin
                   //line = search_replace( .original_line(line), .substring_to_be_replaced( $sformatf("column column_gen\\[%0d\\]\\.column_gen\\.column",column) ), .substring_replacement( $sformatf("column_RTL Column%0d",column) ) );
																			line = search_replace( .original_line(line), .substring_to_be_replaced( $sformatf("column \\column_gen[%0d].column_gen.column",column) ), .substring_replacement( $sformatf("column_RTL Column%0d",column) ) );
																			//if (line != line_before) $display("line %d col %d RTL before = %s, after = %s",counter,column, line_before, line);
														end
														else begin
														     //line = search_replace( .original_line(line), .substring_to_be_replaced( $sformatf("column_gen\\[%0d\\]\\.column_gen\\.column",column) ), .substring_replacement( $sformatf("Column%0d",column) ) );
																			line = search_replace( .original_line(line), .substring_to_be_replaced( $sformatf("\\column_gen[%0d].column_gen.column",column) ), .substring_replacement( $sformatf("Column%0d",column) ) );
																			//if (line != line_before) $display("line %d col %d postlayout before = %s, after = %s",counter,column, line_before, line);
														end
         end
																		
									// Leave SDA_pad unconnected and provide SDAin, SDAen as ports at clictdTop to perform the I2C workaround
									line = search_replace( .original_line(line), .substring_to_be_replaced( ".PAD(SDA_pad)" ), .substring_replacement( "//.PAD(SDA_pad)\n.PAD()" ) );
									line = search_replace( .original_line(line), .substring_to_be_replaced( ".SDA_pad(SDA_pad)" ), .substring_replacement( "//.SDA_pad(SDA_pad)\n.SDA_pad()" ) );
									
									// Replace these lines only in clictdTop, not in clictdPads
									if (line == "module clictdTop (\n") replacing_in_clictdTop = `YES;
									else if (line == "endmodule\n" && replacing_in_clictdTop == `YES ) replacing_in_clictdTop = `NO;
									if (replacing_in_clictdTop) begin
													line = search_replace( .original_line(line), .substring_to_be_replaced( "SDA_pad);" ), .substring_replacement( "//SDA_pad);\nSDAin,\nSDAout,\nSDAen);" ) );
													// Another variant in which we can find the above declaration:
													line = search_replace( .original_line(line), .substring_to_be_replaced( "	SDA_pad, \n" ), .substring_replacement( "//SDA_pad,\nSDAin,\nSDAout,\nSDAen, \n" ) ); 
													line = search_replace( .original_line(line), .substring_to_be_replaced( "inout SDA_pad;" ), .substring_replacement( "//inout SDA_pad;\ninput  SDAin;\noutput SDAout;\noutput SDAen;" ) );
									end
										 									
         // At this point, if line has not been replaced, it has the original value;
									// store the original or the replace value into f_replaced
         $fwrite(f_replaced,"%s",line);
									//$display("SCANNING %d, line = %s",counter, line);
									if (line == "endmodule\n" && clictdTop_detected == `YES) break;
									counter++;
  end 
		//while (line != "endmodule");
		
		//$display("reached this point");
  
		$fclose(f);
  $fclose(f_replaced);
		
		//$display("reached this point 2");

endfunction: generate_clictdtop_i2cworkaround_somecolpnr

