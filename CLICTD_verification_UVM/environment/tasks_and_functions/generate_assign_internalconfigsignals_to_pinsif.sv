// Filename           : generate_assign_internalconfigsignals_to_pinsif.sv
// Author             : Núria Egidos 
// Created on         : 24/10/18
// Last modification  : 24/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Script to generate the paths to the internal configuration signals (tpEnableDigital, mask, tpEnableAnalog, tuningDAC_X) and assign them to the pins interface signals in CLICT_dut_wrapper_pnr.sv. 
//                      This script generates all the assigments and writes them into "generated_assign_internalconfigsignals_to_pinsif.v", which is then read by CLICT_dut_wrapper_pnr.
//                      The paths are built differently for each column, depending on whether the column is instantiated from the RTL netlist or from the P&R netlist. In the first case, the path can be built using the 
//                      column, segment and superpixel_in_segment counters, which iterate the columns, superpixel segments and superpixels in the segments respectively. In the second, the counters cannot be used, but the
//                      literal paths (with the numbers associated to the counters) must be used instead because with backslashed names everything after \\ (backslash) is interpreted *exactly* as written until the next space 
//                      (i.e. \\column_gen[column] is not translated to \\column_gen[0], but \\column_gen[0] must be written to achieve the proper interconnection).



function void generate_assign_internalconfigsignals_to_pinsif (input logic [`COLUMNS-1:0] column_is_rtl);


    string s;
    integer f;


		f = $fopen(`FILE_TO_WRITE_ASSIGNED_INTERNALCONFIGSIGNALS_TO_PINSIF,"w");
    $fwrite(f,"// column_is_rtl = %b\n",column_is_rtl);

		//genvar column, segment, superpixel_in_segment;

    // tpEnableDigital
		// pins_if.tpEnableDigital is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains 1 bit
    
		  for (int column = 0; column < `COLUMNS; column = column + 1) begin
				for(int segment = 0; segment < `SUPERPIXEL_SEGMENTS; segment=segment + 1) begin
						for(int superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment = superpixel_in_segment + 1) begin
                
                //if (column_is_rtl[column] == `YES) 
                //      s = $sformatf("assign pins_if.tpEnableDigital[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[0];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.tpEnableDigital[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .configBits[0];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                // Note: column_gen instead of \\column_gen is used to avoid compilation issues with $sdf_annotate when column.sdf is used;
                // but the name without \ in front cannot have "." inbetween, so column_gen[].column_gen.column is changed to column  
                                    
                //if (column_is_rtl[column] == `YES) 
                //      s = $sformatf("assign pins_if.tpEnableDigital[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[0];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.tpEnableDigital[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .configBits[0];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																
																
																
																if (column_is_rtl[column] == `YES) 
                      s = $sformatf("assign pins_if.tpEnableDigital[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[0];",
                                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                else  s = $sformatf("assign pins_if.tpEnableDigital[%0d][%0d][%0d] = dut.MATRIX.Column%0d.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .configBits[0];",
                                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																//else  s = $sformatf("assign pins_if.tpEnableDigital[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[0];",
																//                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																																				
																		                                    
                $fwrite(f,"%s\n",s);

						end // for pixel in the superpixel
				end // for superpixels
			end // for columns
		

		// mask
		// pins_if.mask is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
    for (int column = 0; column < `COLUMNS; column = column + 1) begin
				for(int segment = 0; segment < `SUPERPIXEL_SEGMENTS; segment=segment + 1) begin
						for(int superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment = superpixel_in_segment + 1) begin
                
                //if (column_is_rtl[column] == `YES) 
                //      s = $sformatf("assign pins_if.mask[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[8:1];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.mask[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .configBits[8:1];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                // Note: column_gen instead of \\column_gen is used to avoid compilation issues with $sdf_annotate when column.sdf is used;
                // but the name without \ in front cannot have "." inbetween, so column_gen[].column_gen.column is changed to column    
                
                //if (column_is_rtl[column] == `YES) 
                //      s = $sformatf("assign pins_if.mask[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[8:1];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.mask[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .configBits[8:1];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																																				
																if (column_is_rtl[column] == `YES) 
                      s = $sformatf("assign pins_if.mask[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[8:1];",
                                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                else  s = $sformatf("assign pins_if.mask[%0d][%0d][%0d] = dut.MATRIX.Column%0d.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .configBits[8:1];",
                                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																//else  s = $sformatf("assign pins_if.mask[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[8:1];",
																//                   column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                
                
                $fwrite(f,"%s\n",s);

						end // for pixel in the superpixel
				end // for superpixels
			end // for columns

			// tpEnableAnalog				
			// pins_if.tpEnableAnalog is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
      for (int column = 0; column < `COLUMNS; column = column + 1) begin
				for(int segment = 0; segment < `SUPERPIXEL_SEGMENTS; segment=segment + 1) begin
						for(int superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment = superpixel_in_segment + 1) begin
                
                //if (column_is_rtl[column] == `YES) 
                //      s = $sformatf("assign pins_if.tpEnableAnalog[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[16:9];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.tpEnableAnalog[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .configBits[16:9];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                // Note: column_gen instead of \\column_gen is used to avoid compilation issues with $sdf_annotate when column.sdf is used;
                // but the name without \ in front cannot have "." inbetween, so column_gen[].column_gen.column is changed to column  
                
                //if (column_is_rtl[column] == `YES) 
                //      s = $sformatf("assign pins_if.tpEnableAnalog[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[16:9];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.tpEnableAnalog[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .configBits[16:9];",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																																				
																if (column_is_rtl[column] == `YES) 
                      s = $sformatf("assign pins_if.tpEnableAnalog[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[16:9];",
                                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                else  s = $sformatf("assign pins_if.tpEnableAnalog[%0d][%0d][%0d] = dut.MATRIX.Column%0d.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .configBits[16:9];",
                                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																//else  s = $sformatf("assign pins_if.tpEnableAnalog[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.configBits[16:9];",
																//                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                
                $fwrite(f,"%s\n",s);

						end // for pixel in the superpixel
				end // for superpixels
			end // for columns


      // pins_if.tuningDACx are COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed arrays and each of their positions contains 3 bits
      for (int i = 0; i < 8; i++) begin
				  for (int column = 0; column < `COLUMNS; column = column + 1) begin
						for(int segment = 0; segment < `SUPERPIXEL_SEGMENTS; segment=segment + 1) begin
								for(int superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment = superpixel_in_segment + 1) begin
				            
				            //if (column_is_rtl[column] == `YES) 
				            //      s = $sformatf("assign pins_if.tuningDAC%0d[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.tuningDAC_%0d;",
				            //                    i,column,segment,superpixel_in_segment,column,segment,superpixel_in_segment,i);
				            //else  s = $sformatf("assign pins_if.tuningDAC%0d[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .tuningDAC_%0d;",
				            //                    i,column,segment,superpixel_in_segment,column,segment,superpixel_in_segment,i);
                // Note: column_gen instead of \\column_gen is used to avoid compilation issues with $sdf_annotate when column.sdf is used;
                // but the name without \ in front cannot have "." inbetween, so column_gen[].column_gen.column is changed to column   
                
                //if (column_is_rtl[column] == `YES) 
				            //      s = $sformatf("assign pins_if.tuningDAC%0d[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.tuningDAC_%0d;",
				            //                    i,column,segment,superpixel_in_segment,column,segment,superpixel_in_segment,i);
				            //else  s = $sformatf("assign pins_if.tuningDAC%0d[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .tuningDAC_%0d;",
				            //                    i,column,segment,superpixel_in_segment,column,segment,superpixel_in_segment,i);
																																				
																if (column_is_rtl[column] == `YES) 
				                  s = $sformatf("assign pins_if.tuningDAC%0d[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.tuningDAC_%0d;",
				                                i,column,segment,superpixel_in_segment,column,segment,superpixel_in_segment,i);
				            else  s = $sformatf("assign pins_if.tuningDAC%0d[%0d][%0d][%0d] = dut.MATRIX.Column%0d.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .tuningDAC_%0d;",
				                                i,column,segment,superpixel_in_segment,column,segment,superpixel_in_segment,i);
															 //else  s = $sformatf("assign pins_if.tuningDAC%0d[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.tuningDAC_%0d;",
																//                    i,column,segment,superpixel_in_segment,column,segment,superpixel_in_segment,i);
																								
                
				            $fwrite(f,"%s\n",s);
								end // for pixel in the superpixel
						end // for superpixels
					end // for columns
			end


      // Readout, masking, checking the effect of the moment when the discriminator rises (this signal is used to force a signal at the
      // output of the discriminator and thus emulate the arrival of a hit)
			// pins_if.disc is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
      for (int column = 0; column < `COLUMNS; column = column + 1) begin
				for(int segment = 0; segment < `SUPERPIXEL_SEGMENTS; segment=segment + 1) begin
						for(int superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment = superpixel_in_segment + 1) begin
                
                //if (column_is_rtl[column] == `YES) 
                //      s = $sformatf("assign pins_if.disc[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.disc;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.disc[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .disc;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                // Note: column_gen instead of \\column_gen is used to avoid compilation issues with $sdf_annotate when column.sdf is used;
                // but the name without \ in front cannot have "." inbetween, so column_gen[].column_gen.column is changed to column   
                
                //if (column_is_rtl[column] == `YES) 
                //     s = $sformatf("assign pins_if.disc[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.disc;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.disc[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .disc;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																																				
																//if (column_is_rtl[column] == `YES) 
                //     s = $sformatf("assign pins_if.disc[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.disc;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.disc[%0d][%0d][%0d] = dut.MATRIX.Column%0d.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .disc;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																																				
																for (int front_end_bits = 0; 	front_end_bits < 8; front_end_bits++) begin	
															      //if (column_is_rtl[column] == `YES) 																		
																					//				s = $sformatf("assign pins_if.disc[%0d][%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.fe_row[%0d].fe_cluster[%0d].ANALOG_FE.IN;",
																					//				              column,segment,superpixel_in_segment,front_end_bits,column,segment,superpixel_in_segment,front_end_bits);
																				 //else s = $sformatf("assign pins_if.disc[%0d][%0d][%0d][%0d] = dut.MATRIX.Column%0d.\\Spix_gen[%0d].Spix_gen.Spix .\\fe_row[%0d].fe_cluster[%0d].ANALOG_FE .IN;",
																					//				              column,segment,superpixel_in_segment,front_end_bits,column,segment,superpixel_in_segment,front_end_bits);
																					////else  s = $sformatf("assign pins_if.disc[%0d][%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.fe_row[%0d].fe_cluster[%0d].ANALOG_FE.IN;",
																     ////                   column,segment,superpixel_in_segment,front_end_bits,column,segment,superpixel_in_segment,front_end_bits);
																					
																					
																					if (column_is_rtl[column] == `YES) 																		
																									s = $sformatf("assign dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.fe_row[%0d].fe_cluster[%0d].ANALOG_FE.IN = pins_if.disc[%0d][%0d][%0d][%0d];",
																									              column,segment,superpixel_in_segment,front_end_bits,column,segment,superpixel_in_segment,front_end_bits);
																				 else s = $sformatf("assign dut.MATRIX.Column%0d.\\Spix_gen[%0d].Spix_gen.Spix .\\fe_row[%0d].fe_cluster[%0d].ANALOG_FE .IN = pins_if.disc[%0d][%0d][%0d][%0d];",
																									              column,segment,superpixel_in_segment,front_end_bits,column,segment,superpixel_in_segment,front_end_bits);
																																							
																				$fwrite(f,"%s\n",s);
                end
																


						end // for pixel in the superpixel
				end // for superpixels
			end // for columns

      // pins_if.discTp is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains 1 bit
      for (int column = 0; column < `COLUMNS; column = column + 1) begin
				for(int segment = 0; segment < `SUPERPIXEL_SEGMENTS; segment=segment + 1) begin
						for(int superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment = superpixel_in_segment + 1) begin
                
                //if (column_is_rtl[column] == `YES) 
                //      s = $sformatf("assign pins_if.discTp[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.discTp;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.discTp[%0d][%0d][%0d] = dut.MATRIX.\\column_gen[%0d].column_gen.column	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .discTp;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                // Note: column_gen instead of \\column_gen is used to avoid compilation issues with $sdf_annotate when column.sdf is used;
                // but the name without \ in front cannot have "." inbetween, so column_gen[].column_gen.column is changed to column   
                
                //if (column_is_rtl[column] == `YES) 
                //      s = $sformatf("assign pins_if.discTp[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.discTp;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                //else  s = $sformatf("assign pins_if.discTp[%0d][%0d][%0d] = dut.MATRIX.Column%0d	.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .discTp;",
                //                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																																				
																if (column_is_rtl[column] == `YES) 
                      s = $sformatf("assign pins_if.discTp[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.discTp;",
                                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                else  s = $sformatf("assign pins_if.discTp[%0d][%0d][%0d] = dut.MATRIX.Column%0d.\\Spix_gen[%0d].Spix_gen.Spix .\\pixel_gen[%0d].pixel_gen.pixel .discTp;",
                                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
																//else  s = $sformatf("assign pins_if.discTp[%0d][%0d][%0d] = dut.MATRIX.Column%0d.Spix_gen[%0d].Spix_gen.Spix.pixel_gen[%0d].pixel_gen.pixel.discTp;",
																//                    column,segment,superpixel_in_segment,column,segment,superpixel_in_segment);
                
                $fwrite(f,"%s\n",s);

						end // for pixel in the superpixel
				end // for superpixels
			end // for columns


		$fclose(f);


endfunction: generate_assign_internalconfigsignals_to_pinsif
