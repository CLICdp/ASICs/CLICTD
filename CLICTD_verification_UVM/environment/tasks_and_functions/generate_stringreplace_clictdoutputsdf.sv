// Filename           : generate_stringreplace_clictdoutputsdf.sv
// Author             : N�ria Egidos 
// Created on         : 24/10/18
// Last modification  : 24/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Script to open the clictd_output.sdf file, replace "column_gen\[X\]\.column_gen\.column"  by "ColumnX" 
//                      (which is the syntax used by the other auxiliary scripts that generate verilog files to include, since 
//                       it causes no compilation error) and save the resulting file into clictd_output_replaced.sdf


// https://www.chipverify.com/system-verilog/file-input-output


//`include "search_replace.sv"


function void generate_stringreplace_clictdoutputsdf ();

  string line, line_before;
		string first_flag_to_detect,second_flag_to_detect,third_flag_to_detect,fourth_flag_to_detect;
		bit first_flag_detected,second_flag_detected;
  
  int exit_code, counter;
  int f,f_replaced;
		bit end_condition_detected;
		f = $fopen(`PATH_ORIGINAL_CLICTDOUTPUT_SDF,"r");  
  f_replaced = $fopen(`PATH_REPLACED_CLICTDOUTPUT_SDF,"w");  
  
		end_condition_detected = `NO;
		first_flag_to_detect = "	)\n";
		second_flag_to_detect = "      )\n";
		third_flag_to_detect = "  )\n";
		fourth_flag_to_detect = ")\n";
		first_flag_detected = `NO;
		second_flag_detected = `NO;

		counter = 0;
  // Read all lines in clictd_output.sdf and replace "column_gen\[X\]\.column_gen\.column"  by "ColumnX" 
		while (end_condition_detected == `NO) begin
  //while (!$feof(f)) begin // deadlock if the file has a new line, empty line at the end of the file (it gets stuck and execution doesn't finish) :(
         //exit_code = $fscanf (f, "%s", line);
         $fgets(line, f);
         for (int column = 0; column < `COLUMNS; column = column + 1) begin
              line = search_replace( .original_line(line), .substring_to_be_replaced( $sformatf("column_gen\\[%0d\\]\\.column_gen\\.column",column) ), .substring_replacement( $sformatf("Column%0d",column) ) );
         end
							
									// Copy line (either the new value or the original if it hasn't been replaced) to f_replaced
         $fwrite(f_replaced,"%s",line);
									
									// Detect stop condition: first_flag_to_detect, second_flag_to_detect, third_flag_to_detect and fourth_flag_to_detect arrive in a row
									if (counter > 0) begin
									    if      (line == second_flag_to_detect && line_before == first_flag_to_detect)                                 first_flag_detected = `YES;
													else if (first_flag_detected == `YES && line == third_flag_to_detect && line_before == second_flag_to_detect)  second_flag_detected = `YES;
													else if (second_flag_detected == `YES && line == fourth_flag_to_detect && line_before == third_flag_to_detect) end_condition_detected = `YES;
									end
									//$display("SCANNING %d, line = %s",counter, line);
									line_before = line;
									counter++;
									
  end 
  
		$fclose(f);
  $fclose(f_replaced);

endfunction: generate_stringreplace_clictdoutputsdf



