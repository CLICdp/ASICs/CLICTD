// Filename           : assert_isunkown_functions.sv
// Author             : N�ria Egidos 
// Created on         : 16/11/18
// Last modification  : 16/11/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Functions that concentrate the assertions of $isunknown calls to verify that none of the fields in the transactions retrieved at the 
//                      comparators, models, etc. is undefined


// http://www.project-veripage.com/sva_9.php
// https://www.doulos.com/knowhow/sysverilog/tutorial/assertions/
// System function  $isunknown  returns  1'b1  if any bit of its argument has the value x  or  z 


// Why is the argument name_of_transaction explicitly used instead of simply calling dataout_transaction.get_name()? Because in some components (the comparators for instance), the transaction provided as an argument
// is obtained from cloning another transaction, instead of being created and then copied manually. By doing so, the name of the cloned transaction may be the default ("dataout_transaction") instead of reflecting
// the name of the transaction instance, which is interesting for the report

function void assert_isunknown_dataout_transaction (input CLICTD_dataout_transaction dataout_transaction,  ChipStatusInfo chip_status_obj, string name_of_transaction, string name_of_calling_component);
																																																										
		/*																																																								
          logic [7:0] aux_hit_map;
										logic [7:0] aux_toa;
										logic [4:0] aux_tot;
										logic [12:0] aux_longtoa;
										logic [12:0] aux_photoncount;
										logic aux_hit_flag;
										int aux_superpixel_segment;
										int aux_superpixel_in_segment;
										logic aux_tpEnableDigital;
										logic [7:0] aux_mask;
										logic [7:0] aux_tpEnableAnalog;
										logic [2:0] aux_tuningDAC0;
										logic [2:0] aux_tuningDAC1;
										logic [2:0] aux_tuningDAC2;
										logic [2:0] aux_tuningDAC3;
										logic [2:0] aux_tuningDAC4;
										logic [2:0] aux_tuningDAC5;
										logic [2:0] aux_tuningDAC6;
										logic [2:0] aux_tuningDAC7; 
										logic [2815:0] aux_maxsize_columncontent;
										
										
										
										aux_hit_map = '0;
										aux_toa = '0;
										aux_tot = '0;
										aux_longtoa = '0;
										aux_photoncount = '0;
										aux_hit_flag = '0;
									 aux_superpixel_segment = '0;
										aux_superpixel_in_segment = '0;
										aux_tpEnableDigital = '0;
										aux_mask = '0;
										aux_tpEnableAnalog = '0;
										aux_tuningDAC0 = '0;
										aux_tuningDAC1 = '0;
										aux_tuningDAC2 = '0;
										aux_tuningDAC3 = '0;
										aux_tuningDAC4 = '0;
										aux_tuningDAC5 = '0;
										aux_tuningDAC6 = '0;
										aux_tuningDAC7 = '0; 
										aux_maxsize_columncontent = '0;
										 
										
										
										
										
										
										if (chip_status_obj.chip_status == STATUS_RESET)  assert ( !$isunknown(dataout_transaction.dataout_bit) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s dataout_bit, chip_status = %s", name_of_transaction, chip_status_obj.chip_status) )
										else begin
										    assert ( !$isunknown(dataout_transaction.column_counter) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_counter, chip_status = %s, from %s", name_of_transaction, chip_status_obj.chip_status,name_of_calling_component) )
										    assert ( !$isunknown(dataout_transaction.column_header) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_header, chip_status = %s, from %s", name_of_transaction, chip_status_obj.chip_status,name_of_calling_component) )
														aux_maxsize_columncontent = '0; for (int i = 0; i < dataout_transaction.column_content.num(); i++) aux_maxsize_columncontent[i] = dataout_transaction.column_content[i];
														assert ( !$isunknown(aux_maxsize_columncontent) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_content, chip_status = %s, from %s", name_of_transaction, chip_status_obj.chip_status,name_of_calling_component) )
														// # ** Error (suppressible): (vsim-12018) Dynamic types and unpacked structs are not supported in 'isunknown'.
														// # UVM_ERROR ../environment//CLICTD_dataout_comparator.sv(429) @ 1989513187900: uvm_test_top.env.dataout_scoreboard.dataout_comparator [ERROR_UNKNOWN_VALUE] X or Z in dataout_transaction_ideal column_content_breakdown, chip_status = STATUS_CONFIG_READOUT_SECOND_HALF
														//assert ( !$isunknown(dataout_transaction.column_content_breakdown) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in dataout_transaction_ideal column_content_breakdown, chip_status = %s", chip_status_obj.chip_status) )
														//assert ( !$isunknown(dataout_transaction.column_config_breakdown) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in dataout_transaction_ideal column_config_breakdown, chip_status = %s", chip_status_obj.chip_status) )
														
														if (chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1 ||
														    chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3 ||
														    chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 ||
																		chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 ||
																		chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1 ||
																		chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3 ||
																		chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1 ||
																		chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3 ||
																		chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0 ||
																		chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0
														) begin
													
														
																
																		for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
									     								for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
														     								aux_hit_map               = dataout_transaction.column_content_breakdown.superpixel[ss][sis].hit_map; 
																											aux_toa                   = dataout_transaction.column_content_breakdown.superpixel[ss][sis].toa;
																											aux_tot                   = dataout_transaction.column_content_breakdown.superpixel[ss][sis].tot;
																											aux_longtoa               = dataout_transaction.column_content_breakdown.superpixel[ss][sis].longtoa;
																											aux_photoncount           = dataout_transaction.column_content_breakdown.superpixel[ss][sis].photoncount;
																											aux_hit_flag              = dataout_transaction.column_content_breakdown.superpixel[ss][sis].hit_flag;
																											aux_superpixel_segment    = dataout_transaction.column_content_breakdown.superpixel[ss][sis].superpixel_segment;
																											aux_superpixel_in_segment = dataout_transaction.column_content_breakdown.superpixel[ss][sis].superpixel_in_segment;

										         								assert( !$isunknown(aux_hit_map)               ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_content_breakdown hit_map ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                   								name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																											if (chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1 ||
														    													chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3 ||
														    													chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 ||
																															chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 ||
																															chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1 ||
																															chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3) begin
																															assert( !$isunknown(aux_toa)               ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_content_breakdown toa ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                   												name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																															assert( !$isunknown(aux_tot)               ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_content_breakdown tot ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                   												name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )		
																											end
																											else if (chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0 )
																											    assert( !$isunknown(aux_longtoa)           ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_content_breakdown longtoa ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                   								name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																											else if (chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0 || chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0 )
																											    assert( !$isunknown(aux_photoncount)       ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_content_breakdown photoncount ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                   								name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )	
																											assert( !$isunknown(aux_hit_flag)              ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_content_breakdown hit_flag ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                   								name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )	
																											assert( !$isunknown(aux_superpixel_segment)    ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_content_breakdown superpixel_segment ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                   								name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																											assert( !$isunknown(aux_superpixel_in_segment) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_content_breakdown superpixel_in_segment ss %0d sis %0d, chip_status = %s, from %s", 
                                                                                                                                      								name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																						end
									 								end
														end
														
														if (chip_status_obj.chip_status == STATUS_CONFIG_READOUT_FIRST_HALF) begin
																			for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
									     									for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin


																												aux_tpEnableDigital       = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tpEnableDigital;
										         									aux_mask                  = dataout_transaction.column_config_breakdown.superpixel[ss][sis].mask;
										 																	aux_tpEnableAnalog        = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tpEnableAnalog;
										 																	aux_tuningDAC0            = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC0;
										 																	aux_tuningDAC1[0]         = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC1[0];
																												aux_superpixel_segment    = dataout_transaction.column_config_breakdown.superpixel[ss][sis].superpixel_segment;
																												aux_superpixel_in_segment = dataout_transaction.column_config_breakdown.superpixel[ss][sis].superpixel_in_segment;																																																																																																																	

			                									assert( !$isunknown(aux_tpEnableDigital)       ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tpEnableDigital ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )		
																												assert( !$isunknown(aux_mask)                  ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown mask ss %0d sis %0d, chip_status = %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																												assert( !$isunknown(aux_tpEnableAnalog)        ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tpEnableAnalog ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )		
																												assert( !$isunknown(aux_tuningDAC0)            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tuningDAC0 ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )							
																												assert( !$isunknown(aux_tuningDAC1)            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tuningDAC1 ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )							
																												assert( !$isunknown(aux_superpixel_segment)    ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown superpixel_segment ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																												assert( !$isunknown(aux_superpixel_in_segment) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown superpixel_in_segment ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )																																																																																																										


																							end
									 									end
														end
														
														
														if (chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF) begin
																			for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
									     									for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																							
																							                
																					    														//$display("DISPLAY - assert isunknown - tuningDAC1[%0d][%0d][%0d][2] = %b, tuningDAC1[%0d][%0d][%0d][1] = %b",
																									         					//									dataout_transaction.column_counter,ss,sis,dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC1[2],
																																							//									dataout_transaction.column_counter,ss,sis,dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC1[1],
																																								//								);
																								                
										


										 																	aux_tuningDAC1[2:1]       = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC1[2:1];
										 																	aux_tuningDAC2            = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC2;
										 																	aux_tuningDAC3            = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC3;
										 																	aux_tuningDAC4            = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC4;
										 																	aux_tuningDAC5            = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC5;
										 																	aux_tuningDAC6            = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC6;
										 																	aux_tuningDAC7            = dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC7;																																																																																																																		
																												aux_superpixel_segment    = dataout_transaction.column_config_breakdown.superpixel[ss][sis].superpixel_segment;
																												aux_superpixel_in_segment = dataout_transaction.column_config_breakdown.superpixel[ss][sis].superpixel_in_segment;																																																																																																																	

			                									
																												assert( !$isunknown(aux_tuningDAC1)            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tuningDAC1 = %h ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,aux_tuningDAC1,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )							
																												assert( !$isunknown(aux_tuningDAC2)            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tuningDAC2 ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																												assert( !$isunknown(aux_tuningDAC3)            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tuningDAC3 ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																												assert( !$isunknown(aux_tuningDAC4)            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tuningDAC4 ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )		
																												assert( !$isunknown(aux_tuningDAC5)            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tuningDAC5 ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )		
																												assert( !$isunknown(aux_tuningDAC6)            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tuningDAC6 ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )	
																												assert( !$isunknown(aux_tuningDAC7)            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown tuningDAC7 ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																												assert( !$isunknown(aux_superpixel_segment)    ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown superpixel_segment ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )
																												assert( !$isunknown(aux_superpixel_in_segment) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s column_config_breakdown superpixel_in_segment ss %0d sis %0d, chip_status = %s, from %s", 
																			                                                                                                                  									name_of_transaction,ss,sis,chip_status_obj.chip_status,name_of_calling_component) )																																																																																																										


																							end
									 									end
														end
														
														
										end
*/
endfunction: assert_isunknown_dataout_transaction



function void assert_isunknown_pins_transaction (input CLICTD_pins_transaction pins_transaction, ChipStatusInfo chip_status_obj, string name_of_transaction, string name_of_calling_component);


/*
          // In any other state, the transaction is generated by pins_monitor and thus some value has been collected for all signals; in this state,
										// the transaction has been created by matrix model and no value is provided for the signas inside this if
          if (chip_status_obj.chip_status != STATUS_MATRIX_CONFIG_SECOND_HALF && chip_status_obj.chip_status != STATUS_CONFIG_READOUT_SECOND_HALF) begin
										    assert ( !$isunknown(pins_transaction.PWREN_pad)                                    ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s PWREN_pad, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.TPULSE_pad)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s TPULSE_pad, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.READOUT_pad)                                  ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s READOUT_pad, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.SHUTTER_pad)                                  ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s SHUTTER_pad, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.RSTN_pad)                                     ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s RSTN_pad, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														
														if  (name_of_calling_component != "uvm_test_top.env.pins_agent.pins_driver" || chip_status_obj.chip_status != STATUS_POWERPULSE_EXTERNAL_WORKING && chip_status_obj.chip_status != STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS && 
										    chip_status_obj.chip_status != STATUS_POWERPULSE_INTERNAL_WORKING && chip_status_obj.chip_status != STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS && chip_status_obj.chip_status != STATUS_FORCE_HIT_SHORTTOT && chip_status_obj.chip_status != STATUS_FORCE_HIT_LONGTOT) begin
																			assert ( !$isunknown(pins_transaction.ENABLE_OUT_pad)                               ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s ENABLE_OUT_pad, chip_status = %s, from %s", 
														                                                                                           					name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )																																																																																																									
																			assert ( !$isunknown(pins_transaction.columnDone)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s columnDone, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.dataIn)                                       ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s dataIn, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.commonToken)                                  ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s commonToken, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.readoutStartExtEn)                            ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s readoutStartExtEn, chip_status = %s, from %s",
														                                                                                           					name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.readoutStart)                                 ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s readoutStart, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.testPulseExtEnable)                           ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s testPulseExtEnable, chip_status = %s, from %s",
														                                                                                           					name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.testPulse)                                    ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s testPulse, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.powerExtEnable)                               ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s powerExtEnable, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.powerEnable)                                  ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s powerEnable, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.CLK_100_GATED)                                ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s CLK_100_GATED, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.counter_clk100gated_while_powerEnable_is_low) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s counter_clk100gated_while_powerEnable_is_low, chip_status = %s, from %s",
														                                                                                           					name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																			assert ( !$isunknown(pins_transaction.disc)                                         ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s disc, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
										    					assert ( !$isunknown(pins_transaction.discTp)                                       ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s discTp, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														
														end
										end
          
										if (chip_status_obj.chip_status != STATUS_RESET || name_of_calling_component != "uvm_test_top.env.pins_agent.pins_driver" || chip_status_obj.chip_status != STATUS_POWERPULSE_EXTERNAL_WORKING && chip_status_obj.chip_status != STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS && 
										    chip_status_obj.chip_status != STATUS_POWERPULSE_INTERNAL_WORKING && chip_status_obj.chip_status != STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS && chip_status_obj.chip_status != STATUS_FORCE_HIT_SHORTTOT && chip_status_obj.chip_status != STATUS_FORCE_HIT_LONGTOT ) begin
														assert ( !$isunknown(pins_transaction.tpEnableDigital)                              ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tpEnableDigital, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.mask)                                         ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s mask, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.tpEnableAnalog)                               ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tpEnableAnalog, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.tuningDAC0)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tuningDAC0, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.tuningDAC1)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tuningDAC1, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.tuningDAC2)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tuningDAC2, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.tuningDAC3)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tuningDAC3, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.tuningDAC4)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tuningDAC4, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.tuningDAC5)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tuningDAC5, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.tuningDAC6)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tuningDAC6, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
														assert ( !$isunknown(pins_transaction.tuningDAC7)                                   ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s tuningDAC7, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
										end
   */       
endfunction: assert_isunknown_pins_transaction


function void assert_isunknown_slowcontrol_transaction (input CLICTD_slowcontrol_transaction slowcontrol_transaction, ChipStatusInfo chip_status_obj, string name_of_transaction, string name_of_calling_component);

/*
         assert ( !$isunknown(slowcontrol_transaction.register_address) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s register_address, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
																																																																																																																		
									// The slow control driver sends commands to read/write slow control registers; in the case of a read operation, the register_content field is empty, it will be filled by the slow control monitior afterwards
								 if (	name_of_calling_component != "uvm_test_top.env.slowcontrol_agent.slowcontrol_driver" || 	name_of_calling_component == "uvm_test_top.env.slowcontrol_agent.slowcontrol_driver"	 && slowcontrol_transaction.read_or_write != `READ )		begin											
								    	assert ( !$isunknown(slowcontrol_transaction.register_content) ) else `uvm_error( `ERROR_UNKNOWN_VALUE, $sformatf("X or Z in %s register_content, chip_status = %s, from %s", name_of_transaction,chip_status_obj.chip_status,name_of_calling_component) )
									end																																																																																																									
*/        
endfunction: assert_isunknown_slowcontrol_transaction
