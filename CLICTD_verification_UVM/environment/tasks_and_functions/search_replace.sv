// https://www.edaplayground.com/x/CPs
function automatic string search_replace(string original_line, string substring_to_be_replaced, string substring_replacement);
    // First find the index of the substring_to_be_replaced string
    int start_index = 0;
    int original_line_index = 0;
    int replace_index = 0;
    bit found = 0;

    while(1) begin
      if (original_line[original_line_index] == substring_to_be_replaced[replace_index]) begin
        if (replace_index == 0) begin
          start_index = original_line_index;
        end
        replace_index++;
        original_line_index++;
        if (replace_index == substring_to_be_replaced.len()) begin
          found = 1;
          break;
        end
      end else if (replace_index != 0) begin
        replace_index = 0;
        original_line_index = start_index + 1;
      end else begin
        original_line_index++;
      end
      if (original_line_index == original_line.len()) begin
        // Not found
        break;
      end
    end

    if (!found) return original_line;

    return {
      original_line.substr(0, start_index-1),
      substring_replacement,
      original_line.substr(start_index+substring_to_be_replaced.len(), original_line.len()-1)
    };

  endfunction
