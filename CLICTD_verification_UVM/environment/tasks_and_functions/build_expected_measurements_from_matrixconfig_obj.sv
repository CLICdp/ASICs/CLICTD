// Filename           : build_expected_measurements_from_matrixconfig_obj.sv
// Author             : N�ria Egidos 
// Created on         : 1/11/18
// Last modification  : 1/11/18 
// Project            : Doctoral student, verification of CLICTD
// Description        : This code is to be added to the CLICTD_dataout_model to generate the expected column_content
//                      values when the matrix is read out after an acquisition (STATUS_MATRIX_READOUT_ACQUIRED)



function void build_expected_measurements_from_matrixconfig_obj (
     input int column_number,
					input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
     input FieldsConfigureMatrix config_matrix_obj,
					input FieldsTimeMeasurements time_measurements_obj,
					input string testname,
     output Acquired_word_struct column_content_breakdown
);
										
					// Initial values (to be changed later if applies)
					for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
					     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
										     column_content_breakdown.superpixel[ss][sis].hit_map               = '0;
	  												column_content_breakdown.superpixel[ss][sis].toa                   = '0;
															column_content_breakdown.superpixel[ss][sis].tot                   = '0;
															column_content_breakdown.superpixel[ss][sis].longtoa               = '0;
															column_content_breakdown.superpixel[ss][sis].photoncount           = '0;
															//column_content_breakdown.superpixel[ss][sis].hit_flag              = 1'b0;
															column_content_breakdown.superpixel[ss][sis].hit_flag              = config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[column_number][ss][sis];
															column_content_breakdown.superpixel[ss][sis].superpixel_segment    = ss;
															column_content_breakdown.superpixel[ss][sis].superpixel_in_segment = sis;
										end
				 end
					
					
					
					// Is there the superpixel (there is only one) where the digital test pulses are applied in this column?
					if      ( column_number == config_matrix_obj.columns_to_pulse[0] ) begin
					
														       //column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].hit_flag = 1'b1;
														
					                // hit map is not changed, it's all zeros
														
																					if ( config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[column_number][config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse] ) begin: not_skipped_due_to_compression

																											// Nominal acquisition mode
																											if      (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0) begin
														        													// ToA
																																			// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive
                                   if (!bits_slowcontrol_reg_obj.tpExtEn) column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].toa = '1;
																																			else column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].toa = time_measurements_obj.toa_ns/`DELAY_FACTOR_TOA;
																																			// ToT
																																			// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive
                                   if (!bits_slowcontrol_reg_obj.tpExtEn) column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].tot = '1;
																																			else begin
																																								if (time_measurements_obj.digital_pulse_lasts_longer_than_shutter == `NO) 
																						     																		column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].tot = time_measurements_obj.tot_short_ns/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																								else column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].tot = time_measurements_obj.tot_long_ns/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																			end
																											end
																											// Long ToA acquisition mode
																											else if (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
																											      // If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive
                                 if (!bits_slowcontrol_reg_obj.tpExtEn) column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].longtoa = '1;
														      													else column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].longtoa = time_measurements_obj.toa_ns/`DELAY_FACTOR_TOA;
																											end
																											// Photon counting acquisition mode
																											else if (bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
														       													if (time_measurements_obj.digital_pulse_lasts_longer_than_shutter == `NO && time_measurements_obj.number_of_pulses_to_apply_within_shutter > 1) 
																																		    column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].photoncount = 13'd2;
																																		else column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].photoncount = 13'd1;
																											end
			                  end: not_skipped_due_to_compression
																					
																					
																						//$display("DISPLAY - build expected content from config obj - col %0d, ss %0d, sis %0d, toa = %h, tot %h, # pulses within shutter = %0d",column_number,config_matrix_obj.ss_applydigitalpulse,config_matrix_obj.sis_applydigitalpulse,
																						//							column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].toa, column_content_breakdown.superpixel[config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse].tot,
																						//							 time_measurements_obj.number_of_pulses_to_apply_within_shutter );
		
														
					end
					else if ( testname == `READOUT_TEST && (column_number == config_matrix_obj.columns_to_pulse[1] || column_number == config_matrix_obj.columns_to_pulse[2] || column_number == config_matrix_obj.columns_to_pulse[3] ||  
														 column_number == config_matrix_obj.columns_to_pulse[5] || column_number == config_matrix_obj.columns_to_pulse[6] || column_number == config_matrix_obj.columns_to_pulse[7]) ) begin
					         //For column_number == config_matrix_obj.columns_to_pulse[4] and column_number == config_matrix_obj.columns_to_pulse[8], 
					         // both bits pulsed in the superpixel are masked, so hit_flag and the rest of signals remain at 0
														
														if ( config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[column_number][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse] ) begin: not_skipped_for_compression

																				// Nominal acquisition mode
																				if      (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0) begin
														        						// ToA
																												
																												// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive
                            if (!bits_slowcontrol_reg_obj.tpExtEn) column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].toa = '1;
																												else begin 
																												     column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].toa = time_measurements_obj.toa_ns/`DELAY_FACTOR_TOA;
																																	// Reduce the portion of signal that is not transmitted because the front-end that originates it is masked
																			  												if (column_number == config_matrix_obj.columns_to_pulse[2] || column_number == config_matrix_obj.columns_to_pulse[6])  
																				    											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].toa = 
																																			column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].toa - 100/`DELAY_FACTOR_TOA;
                            end

																												// ToT
																												
																												// If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive
                            if (!bits_slowcontrol_reg_obj.tpExtEn) column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot = '1;
																												else begin
																																	if      (column_number == config_matrix_obj.columns_to_pulse[1] || column_number == config_matrix_obj.columns_to_pulse[2] || column_number == config_matrix_obj.columns_to_pulse[3] ) 
																						        											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot = time_measurements_obj.tot_short_ns/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																	else if (column_number == config_matrix_obj.columns_to_pulse[5] || column_number == config_matrix_obj.columns_to_pulse[6] || column_number == config_matrix_obj.columns_to_pulse[7] ) 
																						        											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot = time_measurements_obj.tot_long_ns/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																	// One pulse within the shutter
																																	if (time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1) begin
																						      											if     (column_number == config_matrix_obj.columns_to_pulse[2] || column_number == config_matrix_obj.columns_to_pulse[6] ) 
																												        											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot = 
																																    											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot  - 100/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																							else if (column_number == config_matrix_obj.columns_to_pulse[3] || column_number == config_matrix_obj.columns_to_pulse[7] ) 
																												        											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot = 200/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																	end  
																																	// Two pulses within the shutter
																																	else begin
																						      											if      (column_number == config_matrix_obj.columns_to_pulse[2] ) column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot =
																												                                                                  											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot>>1 - 100/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																							else if (column_number == config_matrix_obj.columns_to_pulse[3] ) column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot =
																												                                                                  											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot>>1 + 200/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																							else if (column_number == config_matrix_obj.columns_to_pulse[6] ) column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot =
																												                                                                  											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot - 100/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																							else if (column_number == config_matrix_obj.columns_to_pulse[7] ) column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot = 200/( `DELAY_FACTOR_TOT*(2**bits_slowcontrol_reg_obj.totDiv) );
																																	end 
																													end 
																													//$display("DISPLAY - build expected content from config obj - col %0d, ss %0d, sis %0d, toa = %h, tot %h, # pulses within shutter = %0d",column_number,config_matrix_obj.ss_applydiscpulse,config_matrix_obj.sis_applydiscpulse,
																													//column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].toa, column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].tot,
																													// time_measurements_obj.number_of_pulses_to_apply_within_shutter );
																				end
																				// Long ToA acquisition mode
																				else if (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
																				
																				      // If the test pulse is applied from the slow control, the value will always be saturated to the maximum, due to the time required from the "stop test pulse" command to arrive
                          if (!bits_slowcontrol_reg_obj.tpExtEn) column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].longtoa = '1;
																										else begin		
														      											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].longtoa = time_measurements_obj.toa_ns/`DELAY_FACTOR_TOA;
																															// Reduce the portion of signal that is not transmitted because the front-end that originates it is masked
																															if (column_number == config_matrix_obj.columns_to_pulse[2] || column_number == config_matrix_obj.columns_to_pulse[6])  
																				    											column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].longtoa = 
																																			column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].longtoa - 100/`DELAY_FACTOR_TOA;
																										end
																				end
																				// Photon counting acquisition mode
																				else if (bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
														       						if      ( (column_number == config_matrix_obj.columns_to_pulse[1] || column_number == config_matrix_obj.columns_to_pulse[3]) && time_measurements_obj.number_of_pulses_to_apply_within_shutter > 1  ) 
																					        						column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].photoncount = 13'd2;
																											else if ( (column_number == config_matrix_obj.columns_to_pulse[1] || column_number == config_matrix_obj.columns_to_pulse[3]) && time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1 ||
																											          column_number == config_matrix_obj.columns_to_pulse[2] || column_number == config_matrix_obj.columns_to_pulse[5] || column_number == config_matrix_obj.columns_to_pulse[6] || column_number == config_matrix_obj.columns_to_pulse[7] )
																					        						column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].photoncount = 13'd1;
																				end




												
					         						
																													
																				// In photon counting mode, only the first pulse to arrive is used to compute the hit map						
																				if (bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin	
																								if       (column_number == config_matrix_obj.columns_to_pulse[1] || column_number == config_matrix_obj.columns_to_pulse[3] || column_number == config_matrix_obj.columns_to_pulse[5] || column_number == config_matrix_obj.columns_to_pulse[7])
																				          				column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].hit_map[config_matrix_obj.bit_applydiscpulse_0] = 1'b1;
																								else if  (column_number == config_matrix_obj.columns_to_pulse[2] || column_number == config_matrix_obj.columns_to_pulse[6]) 
																								          column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].hit_map[config_matrix_obj.bit_applydiscpulse_1] = 1'b1;
																				end
																				else begin
																				     // Is there some superpixel where a disc pulse is applied in this column?
																									if ( !config_matrix_obj.mask[column_number][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] )
																			 					   						column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].hit_map[config_matrix_obj.bit_applydiscpulse_0] = 1'b1;
																																		
																									// If 2 separated pulses are applied within the shutter, only the first to arrive is used to compute the hit map. But if these pulses are superimposed, both bits contribute to the hit map
																									if ( time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1 ) begin
																																			if ( !config_matrix_obj.mask[column_number][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] )
																			 					   																column_content_breakdown.superpixel[config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse].hit_map[config_matrix_obj.bit_applydiscpulse_1] = 1'b1;
																									end
																				end
																				
													 end: not_skipped_for_compression
					end
					
			
endfunction: build_expected_measurements_from_matrixconfig_obj



/*
function void build_superpixel_content_from_columncontentbreakdown(  
       input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
             Acquired_word_struct column_content_breakdown,
						 output logic [`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0] superpixel_content		
);

																for ( int superpixel_segment = 0; superpixel_segment < `SUPERPIXEL_SEGMENTS; superpixel_segment++) begin
				     												for ( int superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment++) begin
																					
																					      superpixel_content[superpixel_segment][superpixel_in_segment][7:0] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].hit_map;
																																																						
																											// Nominal acquisition mode
																											if      (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0) begin
														        													superpixel_content[superpixel_segment][superpixel_in_segment][15:8] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].toa;
																																			superpixel_content[superpixel_segment][superpixel_in_segment][20:16] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].tot;
																											end
																											// Long ToA acquisition mode
																											else if (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
														      													  superpixel_content[superpixel_segment][superpixel_in_segment][20:8] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].longtoa;
																											end
																											// Photon counting acquisition mode
																											else if (bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
														       													 superpixel_content[superpixel_segment][superpixel_in_segment][20:8] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].photoncount;
																											end
														
                           superpixel_content[superpixel_segment][superpixel_in_segment][21] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].hit_flag;

				     												end
				 												end

endfunction:build_superpixel_content_from_columncontentbreakdown


		
		function void build_expected_columncontent_from_columncontentbreakdown ( 
							input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
             Acquired_word_struct column_content_breakdown,
					  output logic column_content[int]		
		);
  		int offset_position_in_columncontent;
  		int ss,sis;
				logic [`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0] superpixel_content;

    build_superpixel_content_from_columncontentbreakdown(  .bits_slowcontrol_reg_obj(bits_slowcontrol_reg_obj), .column_content_breakdown(column_content_breakdown), .superpixel_content(superpixel_content)		);

    offset_position_in_columncontent = 0;
    for (int _ss = 0; _ss < `SUPERPIXEL_SEGMENTS; _ss++) begin
        ss = `SUPERPIXEL_SEGMENTS - 1 - _ss;
        for (int _sis = 0; _sis < `SUPERPIXELS_IN_ONE_SEGMENT; _sis++) begin
            sis = `SUPERPIXELS_IN_ONE_SEGMENT - 1 - _sis;
            for (int i = 0; i < `NUMBER_OF_FF_PER_SUPERPIXEL; i++) column_content[offset_position_in_columncontent+i] = superpixel_content[ss][sis][i];
            offset_position_in_columncontent = offset_position_in_columncontent + `NUMBER_OF_FF_PER_SUPERPIXEL;
        end
    end
    		

		endfunction: build_expected_columncontent_from_columncontentbreakdown
		
*/	
		
		
		
		
		function void build_superpixel_content_from_columncontentbreakdown(  
       input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
             Acquired_word_struct column_content_breakdown,
						 output logic superpixel_content	[int][int][int]	
  );
		
		 logic [21:0] aux_superpixel_content;
   superpixel_content.delete();
																for ( int superpixel_segment = 0; superpixel_segment < `SUPERPIXEL_SEGMENTS; superpixel_segment++) begin
				     												for ( int superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment++) begin
																					
																					      if ( column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].hit_flag ) begin: all_data_out																											
																											    for (int i = 0; i < 8; i++) superpixel_content[superpixel_segment][superpixel_in_segment][i] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].hit_map[i];
																															// Nominal acquisition mode
																															if      (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0) begin
														        																	for (int i = 8; i < 16; i++) superpixel_content[superpixel_segment][superpixel_in_segment][i] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].toa[i-8];
																																							for (int i = 16; i < 21; i++) superpixel_content[superpixel_segment][superpixel_in_segment][i] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].tot[i-16];
																															end
																															// Long ToA acquisition mode
																															else if (bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
														      													  				for (int i = 8; i < 21; i++) superpixel_content[superpixel_segment][superpixel_in_segment][i] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].longtoa[i-8];
																															end
																															// Photon counting acquisition mode
																															else if (bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1) begin
														       													 				for (int i = 8; i < 21; i++) superpixel_content[superpixel_segment][superpixel_in_segment][i] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].photoncount[i-8];
																															end

                           				superpixel_content[superpixel_segment][superpixel_in_segment][21] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].hit_flag;
                           end: all_data_out
																											
																											// This case only occurs when compression is enabled and no hit has occurred or some has occurred but in front-ends that were masked
																											else begin: only_hitflag_out
																											     superpixel_content[superpixel_segment][superpixel_in_segment][0] = column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].hit_flag;
																											end: only_hitflag_out
																											
																											aux_superpixel_content = '0;
																											for (int i = 0; i < superpixel_content[superpixel_segment][superpixel_in_segment].num(); i++) aux_superpixel_content[i] = superpixel_content[superpixel_segment][superpixel_in_segment][i];
																											//$display("DISPLAY - build superpixel content - ss %0d sis %0d, hit flag %b, superpixel content %0d bits = %h",superpixel_segment,superpixel_in_segment,
																										 //column_content_breakdown.superpixel[superpixel_segment][superpixel_in_segment].hit_flag,superpixel_content[superpixel_segment][superpixel_in_segment].num(),aux_superpixel_content);
				     												end
				 												end

endfunction:build_superpixel_content_from_columncontentbreakdown


		
		function void build_expected_columncontent_from_columncontentbreakdown ( 
							input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
             Acquired_word_struct column_content_breakdown,
					  output logic column_content[int]		
		);
  		int offset_position_in_columncontent;
  		int ss,sis;
				logic superpixel_content[int][int][int];
				logic [2815:0] aux_column_content;
    column_content.delete();
				
    build_superpixel_content_from_columncontentbreakdown(  .bits_slowcontrol_reg_obj(bits_slowcontrol_reg_obj), .column_content_breakdown(column_content_breakdown), .superpixel_content(superpixel_content)		);

    offset_position_in_columncontent = 0;
    for (int _ss = 0; _ss < `SUPERPIXEL_SEGMENTS; _ss++) begin
        ss = `SUPERPIXEL_SEGMENTS - 1 - _ss;
        for (int _sis = 0; _sis < `SUPERPIXELS_IN_ONE_SEGMENT; _sis++) begin
            sis = `SUPERPIXELS_IN_ONE_SEGMENT - 1 - _sis;
            for (int i = 0; i < superpixel_content[ss][sis].num(); i++) begin column_content[offset_position_in_columncontent] = superpixel_content[ss][sis][i];  
												                                                                  //$display("DISPLAY - build column content - column content[%0d] = %b",offset_position_in_columncontent,column_content[offset_position_in_columncontent]); 
																																																																														offset_position_in_columncontent++;
																																																																							 end
        end
    end
    superpixel_content.delete();		
				
				
				aux_column_content = '0;
				for (int i = 0; i < column_content.num(); i++) aux_column_content[i] = column_content[i];
				//$display("DISPLAY - build column content - column content %0d bits = %h",column_content.num(),aux_column_content);

		endfunction: build_expected_columncontent_from_columncontentbreakdown
		
		
		
		
		
