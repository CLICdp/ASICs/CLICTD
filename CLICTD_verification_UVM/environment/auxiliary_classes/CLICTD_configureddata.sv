// Filename           : CLICTD_configureddata.sv
// Author             : N�ria Egidos 
// Created on         : 9/11/18
// Last modification  : 9/11/18 
// Project            : Doctoral student, verification of CLICTD
// Description        : Data format and related operations to store the data read out after configuration
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/

// On scoreboards: 
// http://www.sunburst-design.com/papers/CummingsSNUG2013SV_UVM_Scoreboards.pdf
// http://www.chipverify.com/uvm/tlm-analysis-port

// On debugging:
// http://forums.accellera.org/topic/1352-sequence-is-not-running-make-sure-sequencer-name-correct/
// https://verificationacademy.com/forums/uvm/how-can-i-see-content-uvmconfigdb




	
	typedef enum { TPENABLE_DIGITAL_DIFFERENT, MASK_DIFFERENT, TPENABLE_ANALOG_DIFFERENT, TUNINGDAC0_DIFFERENT, TUNINGDAC1_DIFFERENT, TUNINGDAC2_DIFFERENT, TUNINGDAC3_DIFFERENT, TUNINGDAC4_DIFFERENT, TUNINGDAC5_DIFFERENT, TUNINGDAC6_DIFFERENT, TUNINGDAC7_DIFFERENT,
	               SUPERPIXEL_NUMBER_CONFIGURED_DIFFERENT_FROM_EXPECTED, ALL_CONFIG_EQUAL
 } exitcode_compare_two_superpixelconfig_type;
	
	typedef exitcode_compare_two_superpixelconfig_type array_exitcodeconfig_type [exitcode_compare_two_superpixelconfig_type];
	
	typedef array_exitcodeconfig_type array_exitcodeconfig_fullcolumn_type [int][int];
	
	
	typedef struct packed {
	  logic tpEnableDigital;
	  logic [7:0] mask;
			logic [7:0] tpEnableAnalog;
			logic [2:0] tuningDAC0;
			logic [2:0] tuningDAC1;
			logic [2:0] tuningDAC2;
			logic [2:0] tuningDAC3;
			logic [2:0] tuningDAC4;
			logic [2:0] tuningDAC5;
			logic [2:0] tuningDAC6;
			logic [2:0] tuningDAC7;
			int superpixel_segment;
			int superpixel_in_segment;
	} Superpixel_config_struct;
	
	
	typedef struct packed {
	   Superpixel_config_struct [`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] superpixel;
	} Configured_word_struct;
	
	
	function void build_configuredwordstruct_from_matrixconfigobj (
	         input int column_number,
							         FieldsConfigureMatrix config_matrix_obj,
										output Configured_word_struct superpixels_config_word
	);
	         /*
	         if (column_number == 0) begin
										    for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
														     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
										    										$display("build_configuredwordstruct_from_matrixconfigobj config_matrix_obj - ss = %0d sis = %0d, tpEnableDigital = %h, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						          																										ss, sis, config_matrix_obj.tpEnableDigital[0][ss][sis],
																																									 config_matrix_obj.mask[0][ss][sis], config_matrix_obj.tpEnableAnalog[0][ss][sis],
																																									 config_matrix_obj.tuningDAC0[0][ss][sis], config_matrix_obj.tuningDAC1[0][ss][sis],
																																										config_matrix_obj.tuningDAC2[0][ss][sis], config_matrix_obj.tuningDAC3[0][ss][sis],
																																									 config_matrix_obj.tuningDAC4[0][ss][sis], config_matrix_obj.tuningDAC5[0][ss][sis],
																																									 config_matrix_obj.tuningDAC6[0][ss][sis],
																																										config_matrix_obj.tuningDAC7[0][ss][sis]);
																		end
															end
										end
										*/
										// At this point, config_matrix_obj has the same values as in the test	
																																										
	         for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
										     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
															    superpixels_config_word.superpixel[ss][sis].tpEnableDigital = config_matrix_obj.tpEnableDigital[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].mask            = config_matrix_obj.mask[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].tpEnableAnalog  = config_matrix_obj.tpEnableAnalog[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].tuningDAC0      = config_matrix_obj.tuningDAC0[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].tuningDAC1      = config_matrix_obj.tuningDAC1[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].tuningDAC2      = config_matrix_obj.tuningDAC2[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].tuningDAC3      = config_matrix_obj.tuningDAC3[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].tuningDAC4      = config_matrix_obj.tuningDAC4[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].tuningDAC5      = config_matrix_obj.tuningDAC5[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].tuningDAC6      = config_matrix_obj.tuningDAC6[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].tuningDAC7      = config_matrix_obj.tuningDAC7[column_number][ss][sis];
																			superpixels_config_word.superpixel[ss][sis].superpixel_segment = ss;
			                superpixels_config_word.superpixel[ss][sis].superpixel_in_segment = sis;
																			
																			/*
																			if (column_number == 0) begin
																			$display("build_configuredwordstruct_from_matrixconfigobj config_matrix_obj - ss = %0d sis = %0d, tpEnableDigital = %h, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						          																										ss, sis, superpixels_config_word.superpixel[ss][sis].tpEnableDigital,
																																									 superpixels_config_word.superpixel[ss][sis].mask, superpixels_config_word.superpixel[ss][sis].tpEnableAnalog,
																																									 superpixels_config_word.superpixel[ss][sis].tuningDAC0, superpixels_config_word.superpixel[ss][sis].tuningDAC1,
																																										superpixels_config_word.superpixel[ss][sis].tuningDAC2, superpixels_config_word.superpixel[ss][sis].tuningDAC3,
																																									 superpixels_config_word.superpixel[ss][sis].tuningDAC4, superpixels_config_word.superpixel[ss][sis].tuningDAC5,
																																									 superpixels_config_word.superpixel[ss][sis].tuningDAC6,
																																										superpixels_config_word.superpixel[ss][sis].tuningDAC7);
																			end
																			*/
																			// At this point, superpixels_config_word has the same values as in the test
															end
										end
	
	endfunction: build_configuredwordstruct_from_matrixconfigobj
	
	
	function void breakdown_columncontent_into_superpixelconfig (
										input logic column_content [int],
										       bit[1:0] configuration_half,
										output Configured_word_struct superpixels_config_word
	);
	          int offset_columcontent_position;
	         //  How is data stored in column_content?
          //            21 ------- superpixel 0 ------ 0 || 21 -------- superpixel 1 ------ 0 || ... || 21 -------- superpixel 15 ------ 0
          //           MSB ----------------------------------------- column_content ---------------------------------------------------- 0
										offset_columcontent_position = 0;
										for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin 
										    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
														          //superpixels_config_word.superpixel[ss][sis].superpixel_segment = ss;
																								//superpixels_config_word.superpixel[ss][sis].superpixel_in_segment = sis;
																								
																								superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].superpixel_segment = `SUPERPIXEL_SEGMENTS-1-ss;
																								superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].superpixel_in_segment = `SUPERPIXELS_IN_ONE_SEGMENT-1-sis;
																								
																								// First configuration half
																								if (configuration_half == `FIRST_CONFIGURATION_HALF) begin
																								    superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tpEnableDigital = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												for (int j = 0; j < 8; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].mask[j] = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												end
																												for (int j = 0; j < 8; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tpEnableAnalog[j] = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												end
																												for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tuningDAC0[j] = column_content[offset_columcontent_position];	offset_columcontent_position++;
																												end
																												superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tuningDAC1[0] = column_content[offset_columcontent_position];	offset_columcontent_position++;
																												offset_columcontent_position++; // skip dummy bit
																								end
																								// Second configuration half
																								else begin
																								    offset_columcontent_position++; // skip dummy bit
																												for (int j = 0; j < 2; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tuningDAC1[j+1] = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												end
																												for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tuningDAC2[j] = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												end
																												for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tuningDAC3[j] = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												end
																												for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tuningDAC4[j] = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												end
																												for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tuningDAC5[j] = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												end
																												for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tuningDAC6[j] = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												end
																												for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tuningDAC7[j] = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												end
																												offset_columcontent_position++; // skip dummy bit
																								end
																								
																								
										         				 /*
																							 // We expect to read 22 bits for this pixel
																								offset_columcontent_position++; // skip bit 21 (dummy bit)
																								
																								// First configuration half
																								if (configuration_half == `FIRST_CONFIGURATION_HALF) begin
																								    superpixels_config_word.superpixel[ss][sis].tuningDAC1[0] = column_content[column_content.num() - offset_columcontent_position]; 
																												//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												offset_columcontent_position++;
																												for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].tuningDAC0[2-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												end
																												for (int j = 0; j < 8; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].tpEnableAnalog[7-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												end
																												for (int j = 0; j < 8; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].mask[7-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												end
																												superpixels_config_word.superpixel[ss][sis].tpEnableDigital = column_content[column_content.num() - offset_columcontent_position]; 
																												//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												offset_columcontent_position++;
																								end
																								// Second configuration half
																								else begin
																								     for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].tuningDAC7[2-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												 end
																													for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].tuningDAC6[2-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												 end
																													for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].tuningDAC5[2-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												 end
																													for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].tuningDAC4[2-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												 end
																													for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].tuningDAC3[2-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												 end
																													for (int j = 0; j < 3; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].tuningDAC2[2-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												 end
																													for (int j = 0; j < 2; j++) begin
																												     superpixels_config_word.superpixel[ss][sis].tuningDAC1[1-j] = column_content[column_content.num() - offset_columcontent_position]; 
																																	//$display("acqdata - nominal - superpixel %d - tot - %b - offset %d",i,column_content[offset_columcontent_position],offset_columcontent_position);
																												     offset_columcontent_position++;
																												 end
																													// Skip bit 0 (dummy bit)
																													offset_columcontent_position++;
																								end
																							*/
										    end   
										end
	endfunction: breakdown_columncontent_into_superpixelconfig


 function array_exitcodeconfig_type compare_two_superpixelconfig (
										      input Superpixel_config_struct superpixel_config_measured,
																      Superpixel_config_struct superpixel_config_expected
	);
	
	         compare_two_superpixelconfig.delete(); // this is done to avoid that errors are accumulated from one call to this function to the next call
										
										
										if ( superpixel_config_measured.superpixel_segment != superpixel_config_expected.superpixel_segment || superpixel_config_measured.superpixel_in_segment != superpixel_config_expected.superpixel_in_segment )
										    compare_two_superpixelconfig[SUPERPIXEL_NUMBER_CONFIGURED_DIFFERENT_FROM_EXPECTED] = SUPERPIXEL_NUMBER_CONFIGURED_DIFFERENT_FROM_EXPECTED;
																		
	         if ( superpixel_config_measured.tpEnableDigital != superpixel_config_expected.tpEnableDigital ) compare_two_superpixelconfig[TPENABLE_DIGITAL_DIFFERENT] = TPENABLE_DIGITAL_DIFFERENT;
										if ( superpixel_config_measured.mask            != superpixel_config_expected.mask            ) compare_two_superpixelconfig[MASK_DIFFERENT]             = MASK_DIFFERENT;
										if ( superpixel_config_measured.tpEnableAnalog  != superpixel_config_expected.tpEnableAnalog  ) compare_two_superpixelconfig[TPENABLE_ANALOG_DIFFERENT]  = TPENABLE_ANALOG_DIFFERENT;
										if ( superpixel_config_measured.tuningDAC0      != superpixel_config_expected.tuningDAC0      ) compare_two_superpixelconfig[TUNINGDAC0_DIFFERENT]       = TUNINGDAC0_DIFFERENT;
										if ( superpixel_config_measured.tuningDAC1      != superpixel_config_expected.tuningDAC1      ) compare_two_superpixelconfig[TUNINGDAC0_DIFFERENT]       = TUNINGDAC1_DIFFERENT;
										if ( superpixel_config_measured.tuningDAC2      != superpixel_config_expected.tuningDAC2      ) compare_two_superpixelconfig[TUNINGDAC0_DIFFERENT]       = TUNINGDAC2_DIFFERENT;
										if ( superpixel_config_measured.tuningDAC3      != superpixel_config_expected.tuningDAC3      ) compare_two_superpixelconfig[TUNINGDAC0_DIFFERENT]       = TUNINGDAC3_DIFFERENT;
										if ( superpixel_config_measured.tuningDAC4      != superpixel_config_expected.tuningDAC4      ) compare_two_superpixelconfig[TUNINGDAC0_DIFFERENT]       = TUNINGDAC4_DIFFERENT;
										if ( superpixel_config_measured.tuningDAC5      != superpixel_config_expected.tuningDAC5      ) compare_two_superpixelconfig[TUNINGDAC0_DIFFERENT]       = TUNINGDAC5_DIFFERENT;
										if ( superpixel_config_measured.tuningDAC6      != superpixel_config_expected.tuningDAC6      ) compare_two_superpixelconfig[TUNINGDAC0_DIFFERENT]       = TUNINGDAC6_DIFFERENT;
										if ( superpixel_config_measured.tuningDAC7      != superpixel_config_expected.tuningDAC7      ) compare_two_superpixelconfig[TUNINGDAC0_DIFFERENT]       = TUNINGDAC7_DIFFERENT;
			
										
										// If no error has been detected
										if ( compare_two_superpixelconfig.num() == 0 ) compare_two_superpixelconfig[ALL_CONFIG_EQUAL] = ALL_CONFIG_EQUAL;
										
										/*
										foreach (compare_two_superpixelconfig[j]) begin  $display("DISPLAY acqdata - comparison - superpixel ss %0d sis %0d - %s, EXPECTED: tpEnableDigital = %b, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1[0] = %b, MEASURED: tpEnableDigital = %b, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1[0] = %b",superpixel_config_measured.superpixel_segment,superpixel_config_measured.superpixel_in_segment,compare_two_superpixelconfig[j],
										        superpixel_config_expected.tpEnableDigital, superpixel_config_expected.mask, superpixel_config_expected.tpEnableAnalog, superpixel_config_expected.tuningDAC0, superpixel_config_expected.tuningDAC1[0],
																		superpixel_config_measured.tpEnableDigital, superpixel_config_measured.mask, superpixel_config_measured.tpEnableAnalog, superpixel_config_measured.tuningDAC0, superpixel_config_measured.tuningDAC1[0] ); end
										*/
	endfunction: compare_two_superpixelconfig
	
	
	function array_exitcodeconfig_type compare_two_superpixelconfig_half (
										      input Superpixel_config_struct superpixel_config_measured,
																      Superpixel_config_struct superpixel_config_expected,
																						bit [1:0] configuration_half
	);
	
	         compare_two_superpixelconfig_half.delete(); // this is done to avoid that errors are accumulated from one call to this function to the next call
										
										
										if ( superpixel_config_measured.superpixel_segment != superpixel_config_expected.superpixel_segment || superpixel_config_measured.superpixel_in_segment != superpixel_config_expected.superpixel_in_segment )
										    compare_two_superpixelconfig_half[SUPERPIXEL_NUMBER_CONFIGURED_DIFFERENT_FROM_EXPECTED] = SUPERPIXEL_NUMBER_CONFIGURED_DIFFERENT_FROM_EXPECTED;
																		
										if (configuration_half == `FIRST_CONFIGURATION_HALF) begin
	         				if ( superpixel_config_measured.tpEnableDigital != superpixel_config_expected.tpEnableDigital ) compare_two_superpixelconfig_half[TPENABLE_DIGITAL_DIFFERENT] = TPENABLE_DIGITAL_DIFFERENT;
														if ( superpixel_config_measured.mask            != superpixel_config_expected.mask            ) compare_two_superpixelconfig_half[MASK_DIFFERENT]             = MASK_DIFFERENT;
														if ( superpixel_config_measured.tpEnableAnalog  != superpixel_config_expected.tpEnableAnalog  ) compare_two_superpixelconfig_half[TPENABLE_ANALOG_DIFFERENT]  = TPENABLE_ANALOG_DIFFERENT;
														if ( superpixel_config_measured.tuningDAC0      != superpixel_config_expected.tuningDAC0      ) compare_two_superpixelconfig_half[TUNINGDAC0_DIFFERENT]       = TUNINGDAC0_DIFFERENT;
														if ( superpixel_config_measured.tuningDAC1[0]   != superpixel_config_expected.tuningDAC1[0]   ) compare_two_superpixelconfig_half[TUNINGDAC0_DIFFERENT]       = TUNINGDAC1_DIFFERENT;
										end
										else if (configuration_half == `SECOND_CONFIGURATION_HALF) begin
														if ( superpixel_config_measured.tuningDAC1[2:1] != superpixel_config_expected.tuningDAC1[2:1] ) compare_two_superpixelconfig_half[TUNINGDAC0_DIFFERENT]       = TUNINGDAC1_DIFFERENT;
														if ( superpixel_config_measured.tuningDAC2      != superpixel_config_expected.tuningDAC2      ) compare_two_superpixelconfig_half[TUNINGDAC0_DIFFERENT]       = TUNINGDAC2_DIFFERENT;
														if ( superpixel_config_measured.tuningDAC3      != superpixel_config_expected.tuningDAC3      ) compare_two_superpixelconfig_half[TUNINGDAC0_DIFFERENT]       = TUNINGDAC3_DIFFERENT;
														if ( superpixel_config_measured.tuningDAC4      != superpixel_config_expected.tuningDAC4      ) compare_two_superpixelconfig_half[TUNINGDAC0_DIFFERENT]       = TUNINGDAC4_DIFFERENT;
														if ( superpixel_config_measured.tuningDAC5      != superpixel_config_expected.tuningDAC5      ) compare_two_superpixelconfig_half[TUNINGDAC0_DIFFERENT]       = TUNINGDAC5_DIFFERENT;
														if ( superpixel_config_measured.tuningDAC6      != superpixel_config_expected.tuningDAC6      ) compare_two_superpixelconfig_half[TUNINGDAC0_DIFFERENT]       = TUNINGDAC6_DIFFERENT;
														if ( superpixel_config_measured.tuningDAC7      != superpixel_config_expected.tuningDAC7      ) compare_two_superpixelconfig_half[TUNINGDAC0_DIFFERENT]       = TUNINGDAC7_DIFFERENT;
			       end
										
										// If no error has been detected
										if ( compare_two_superpixelconfig_half.num() == 0 ) compare_two_superpixelconfig_half[ALL_CONFIG_EQUAL] = ALL_CONFIG_EQUAL;
										
	endfunction: compare_two_superpixelconfig_half
	
	
	
	function array_exitcodeconfig_fullcolumn_type compare_two_columnconfig_broken_into_superpixelconfig (
										      input Configured_word_struct superpixel_config_measured,
																      Configured_word_struct superpixel_config_expected
	);
	array_exitcodeconfig_type aux;
	compare_two_columnconfig_broken_into_superpixelconfig.delete();
	
	
	         for ( int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++ ) begin
										     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																				aux.delete();
																				
																		
																				aux = compare_two_superpixelconfig ( .superpixel_config_measured(superpixel_config_measured.superpixel[ss][sis]), .superpixel_config_expected(superpixel_config_expected.superpixel[ss][sis]) );
																				/*
																				foreach (aux[j]) begin  $display("DISPLAY acqdata - comparison - superpixel ss %0d sis %0d - %s, EXPECTED: tpEnableDigital = %b, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1[0] = %b, MEASURED: tpEnableDigital = %b, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1[0] = %b",superpixel_config_measured.superpixel[ss][sis].superpixel_segment,superpixel_config_measured.superpixel[ss][sis].superpixel_in_segment,aux[j],
										        superpixel_config_expected.superpixel[ss][sis].tpEnableDigital, superpixel_config_expected.superpixel[ss][sis].mask, superpixel_config_expected.superpixel[ss][sis].tpEnableAnalog, superpixel_config_expected.superpixel[ss][sis].tuningDAC0,
																		 superpixel_config_expected.superpixel[ss][sis].tuningDAC1[0],
																		superpixel_config_measured.superpixel[ss][sis].tpEnableDigital, superpixel_config_measured.superpixel[ss][sis].mask, superpixel_config_measured.superpixel[ss][sis].tpEnableAnalog, superpixel_config_measured.superpixel[ss][sis].tuningDAC0,
																		 superpixel_config_measured.superpixel[ss][sis].tuningDAC1[0] ); end
																		 */
																				//foreach (aux[j]) begin  $display("acqdata - comparison - superpixel ss %0d sis %0d - %s",ss,sis,aux[j]); end
																				compare_two_columnconfig_broken_into_superpixelconfig[ss][sis] = aux;
																				//$display("num compare_two_columnconfig_broken_into_superpixelconfig[%d] = %d",i,compare_two_columnconfig_broken_into_superpixelconfig[i].num());
															end
										end
		        //$display("num compare_two_columnconfig_broken_into_superpixelconfig = %d",compare_two_columnconfig_broken_into_superpixelconfig.num());
	endfunction:compare_two_columnconfig_broken_into_superpixelconfig 
	
	
	function array_exitcodeconfig_fullcolumn_type compare_two_columnconfig_broken_into_superpixelconfig_half (
										      input Configured_word_struct superpixel_config_measured,
																      Configured_word_struct superpixel_config_expected,
																						bit [1:0] configuration_half
	);
	array_exitcodeconfig_type aux;
	compare_two_columnconfig_broken_into_superpixelconfig_half.delete();
	
	
	         for ( int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++ ) begin
										     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																				aux.delete();																		
																				aux = compare_two_superpixelconfig_half ( .superpixel_config_measured(superpixel_config_measured.superpixel[ss][sis]), .superpixel_config_expected(superpixel_config_expected.superpixel[ss][sis]), .configuration_half(configuration_half) );
																				compare_two_columnconfig_broken_into_superpixelconfig_half[ss][sis] = aux;
															end
										end
	endfunction:compare_two_columnconfig_broken_into_superpixelconfig_half
	
	

 function void copy_two_columnconfig_broken_into_superpixelconfig (
										      input Configured_word_struct superpixel_config_original,
																output Configured_word_struct superpixel_config_copied
	);
	         for ( int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++ ) begin
										     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
										     					superpixel_config_copied.superpixel[ss][sis].tpEnableDigital       = superpixel_config_original.superpixel[ss][sis].tpEnableDigital;
	  																	superpixel_config_copied.superpixel[ss][sis].mask                  = superpixel_config_original.superpixel[ss][sis].mask;
																				superpixel_config_copied.superpixel[ss][sis].tpEnableAnalog        = superpixel_config_original.superpixel[ss][sis].tpEnableAnalog;
																				superpixel_config_copied.superpixel[ss][sis].tuningDAC0            = superpixel_config_original.superpixel[ss][sis].tuningDAC0;
																				superpixel_config_copied.superpixel[ss][sis].tuningDAC1            = superpixel_config_original.superpixel[ss][sis].tuningDAC1;
																				superpixel_config_copied.superpixel[ss][sis].tuningDAC2            = superpixel_config_original.superpixel[ss][sis].tuningDAC2;
																				superpixel_config_copied.superpixel[ss][sis].tuningDAC3            = superpixel_config_original.superpixel[ss][sis].tuningDAC3;
																				superpixel_config_copied.superpixel[ss][sis].tuningDAC4            = superpixel_config_original.superpixel[ss][sis].tuningDAC4;
																				superpixel_config_copied.superpixel[ss][sis].tuningDAC5            = superpixel_config_original.superpixel[ss][sis].tuningDAC5;
																				superpixel_config_copied.superpixel[ss][sis].tuningDAC6            = superpixel_config_original.superpixel[ss][sis].tuningDAC6;
																				superpixel_config_copied.superpixel[ss][sis].tuningDAC7            = superpixel_config_original.superpixel[ss][sis].tuningDAC7;
																				superpixel_config_copied.superpixel[ss][sis].superpixel_segment    = superpixel_config_original.superpixel[ss][sis].superpixel_segment;
																				superpixel_config_copied.superpixel[ss][sis].superpixel_in_segment = superpixel_config_original.superpixel[ss][sis].superpixel_in_segment;
															end
										end
	endfunction: copy_two_columnconfig_broken_into_superpixelconfig

  
