// Filename           : CLICTDevents.sv
// Author             : Núria Egidos 
// Created on         : 14/8/18
// Last modification  : 14/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Auxiliary class to store the events that trigger actions in the UVM components/objects


// UVM events: https://www.edaplayground.com/s/4/1093
// https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1c/html/files/base/uvm_event-svh.html#uvm_event.new



`ifndef __CLICTDevents_sv__
`define __CLICTDevents_sv__


class CLICTDevents extends uvm_object;
      `uvm_object_utils( CLICTDevents ) // Register the object to the factory


      uvm_event reset_released_event;
			   uvm_event rise_enableoutpad_event;
      uvm_event fall_enableoutpad_event;
      uvm_event rise_clkout_event;
      uvm_event fall_clkout_event;
      uvm_event read_slowcontrol_finished_event;
      uvm_event write_slowcontrol_finished_event;
      uvm_event enable_clock_100MHz_event;
      uvm_event disable_clock_100MHz_event;

      uvm_event shutter_pad_rise_event;
      uvm_event shutter_pad_fall_event;
      //uvm_event_pool digital_test_pulse_rise_event_pool;
      //uvm_event_pool digital_test_pulse_fall_event_pool;
      uvm_event digital_test_pulse_rise_event;
      uvm_event digital_test_pulse_fall_event;
      //uvm_event_pool OR_disc_rise_event_pool;
      //uvm_event_pool OR_disc_fall_event_pool;
      uvm_event rise_disc_OR_event;
      uvm_event fall_disc_OR_event;

      uvm_event timeout_start_readout_event;

      uvm_event rise_powerEnable_event;
      uvm_event fall_powerEnable_event;
      uvm_event rise_pwrInt_event;
      uvm_event fall_pwrInt_event;
      uvm_event rise_powerInternal_event;
      uvm_event fall_powerInternal_event;
      uvm_event rise_pwren_pad_event;
      uvm_event fall_pwren_pad_event;
      uvm_event timeout_update_powerenable_event;
      uvm_event finish_sequence_power_pulsing_event;
						
						
						// Events used in CLICTD_apply_internalandexternal_pulses_virtual_sequence.sv
						uvm_event fall_testpulseslowcontrol_event;
						uvm_event fall_testpulsepad_event;
						
						
						// Events used in pins_model, test pulse control test
						uvm_event rise_discTp_event;
						uvm_event fall_discTp_event;
						uvm_event timeout_update_discTp_event;
						uvm_event rise_tpulse_pad_event;
      uvm_event fall_tpulse_pad_event;
						uvm_event rise_testPulseIntStrobe_event;
						uvm_event fall_testPulseIntStrobe_event;
						uvm_event rise_testpulseIntStrobe_event;
						uvm_event fall_testpulseIntStrobe_event;
						
						// Auxiliary events used to detect possible timing violations in the synchronization FFs that
						// synchronize readoutStart (output of the multiplexer that selects between READOUT_pad and the
						// internal slow control signal) to the CLK_40 clock
						uvm_event rise_digitalperi_roctrl_clk40_event;
						uvm_event fall_digitalperi_roctrl_clk40_event;
						uvm_event rise_readoutstart_event;
				 	uvm_event fall_readoutstart_event;
						
						// Auxiliary event used to detect possible timing violations in the reset synchronizer
						uvm_event rise_digitalperi_rstnctrl_clk40_event;
      uvm_event fall_digitalperi_rstnctrl_clk40_event;
						uvm_event rise_digitalperi_rstnctrl_rstnin_event;

    // Constructor
		function new(string name = "");
      super.new(name);
      reset_released_event = new("reset_released_event");
      rise_enableoutpad_event = new("uvm_event rise_enableoutpad_event");
      rise_enableoutpad_event = new("rise_enableoutpad_event");
      fall_enableoutpad_event = new("fall_enableoutpad_event");
      rise_clkout_event = new("rise_clkout_event");
      fall_clkout_event = new("fall_clkout_event");
      read_slowcontrol_finished_event = new("read_slowcontrol_finished_event");
      write_slowcontrol_finished_event = new("write_slowcontrol_finished_event");
      enable_clock_100MHz_event = new("enable_clock_100MHz_event");
      disable_clock_100MHz_event = new("disable_clock_100MHz_event");
      shutter_pad_rise_event = new("shutter_pad_rise_event");
      shutter_pad_fall_event = new("shutter_pad_fall_event");
      //digital_test_pulse_rise_event_pool = digital_test_pulse_rise_event_pool.get_global_pool();
      //digital_test_pulse_fall_event_pool = digital_test_pulse_fall_event_pool.get_global_pool();
      digital_test_pulse_rise_event = new("digital_test_pulse_rise_event");
      digital_test_pulse_fall_event = new("digital_test_pulse_fall_event");
      //OR_disc_rise_event_pool = OR_disc_rise_event_pool.get_global_pool();
      //OR_disc_fall_event_pool = OR_disc_fall_event_pool.get_global_pool();
      rise_disc_OR_event = new("rise_disc_OR_event");
      fall_disc_OR_event = new("fall_disc_OR_event");
      timeout_start_readout_event = new("timeout_start_readout_event");
      rise_powerEnable_event = new("rise_powerEnable_event");
      fall_powerEnable_event = new("fall_powerEnable_event");
      rise_pwrInt_event = new("rise_pwrInt_event");
      fall_pwrInt_event = new("fall_pwrInt_event");
      rise_powerInternal_event = new("rise_powerInternal_event");
      fall_powerInternal_event = new("fall_powerInternal_event");
      rise_pwren_pad_event = new("rise_pwren_pad_event");
      fall_pwren_pad_event = new("fall_pwren_pad_event");
      timeout_update_powerenable_event = new("timeout_update_powerenable_event");
      finish_sequence_power_pulsing_event = new("finish_sequence_power_pulsing_event");
						fall_testpulseslowcontrol_event = new("fall_testpulseslowcontrol_event");
						fall_testpulsepad_event = new("fall_testpulsepad_event");
						rise_discTp_event = new("rise_discTp_event");
						fall_discTp_event = new("fall_discTp_event");
						timeout_update_discTp_event = new("timeout_update_discTp_event");
						rise_tpulse_pad_event = new("rise_tpulse_pad_event");
						fall_tpulse_pad_event = new("fall_tpulse_pad_event");
						rise_testPulseIntStrobe_event = new("rise_testPulseIntStrobe_event");
						fall_testPulseIntStrobe_event = new("fall_testPulseIntStrobe_event");
						rise_testpulseIntStrobe_event = new("rise_testpulseIntStrobe_event");
						fall_testpulseIntStrobe_event = new("fall_testpulseIntStrobe_event");
						rise_digitalperi_roctrl_clk40_event = new("rise_digitalperi_roctrl_clk40_event");
						fall_digitalperi_roctrl_clk40_event = new("fall_digitalperi_roctrl_clk40_event");
						rise_readoutstart_event = new("rise_readoutstart_event");
						fall_readoutstart_event = new("fall_readoutstart_event");
						rise_digitalperi_rstnctrl_clk40_event = new("rise_digitalperi_rstnctrl_clk40_event");
						fall_digitalperi_rstnctrl_clk40_event = new("fall_digitalperi_rstnctrl_clk40_event");
						rise_digitalperi_rstnctrl_rstnin_event = new("rise_digitalperi_rstnctrl_rstnin_event");
		endfunction: new


endclass: CLICTDevents




`endif // __CLICTDevents_sv__

