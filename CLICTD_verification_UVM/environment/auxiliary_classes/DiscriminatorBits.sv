// Filename           : DiscriminatorBits.sv
// Author             : Núria Egidos 
// Created on         : 14/9/18
// Last modification  : 14/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Auxiliary class to store the disc bits corresponding to a certain superpixel


`ifndef __DiscriminatorBits_sv__
`define __DiscriminatorBits_sv__


class DiscriminatorBits extends uvm_object;
      `uvm_object_utils( DiscriminatorBits ) // Register the object to the factory
     


			logic [7:0] disc;
      logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][7:0] hit_maps; // hit map corresponding to each superpixel

      // The nexts two associative arrays are used to store the coordinates of the superpixel at which some disc bit has risen or fallen, 
      // both the index and the coordinates are in the format: "%d-%d-%d", column - superpixel segment - superpixel in segment 
      string coordinates_disc_rise[string];
      string coordinates_disc_fall[string];

    //`uvm_object_utils_begin(DiscriminatorBits)
    //`uvm_field_int(disc, UVM_ALL_ON)
    //`uvm_object_utils_end
  // we use the UVM_ALL_ON argument so that all the fields 
  // can be affected (copied, compared, etc.) by the default methods

    // Constructor
		function new(string name = "");
      super.new(name);
		endfunction: new


endclass: DiscriminatorBits


`endif // __DiscriminatorBits_sv__

