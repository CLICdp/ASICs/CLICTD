// Filename           : FieldsTimeMeasurements_for_coverage.sv
// Author             : N�ria Egidos 
// Created on         : 14/12/18
// Last modification  : 15/1/19
// Project            : Doctoral student, verification of CLICTD
// Description        : Auxiliary class to store the expected values of time measurement (ToT, ToA) - used for coverage collection



`ifndef __FieldsTimeMeasurements_for_coverage_sv__
`define __FieldsTimeMeasurements_for_coverage_sv__


class FieldsTimeMeasurements_for_coverage extends FieldsTimeMeasurements;
    `uvm_object_utils( FieldsTimeMeasurements_for_coverage ) // Register the object to the factory
				
				 //rand int delay_to_startpulses_after_shutter_ns;
     rand int tot_ns;
     rand int toa_ns;					
					rand int time_separation_between_pulses_in_two_disc_bits;
					rand int duration_first_pulse_in_disc_bits;
					rand int duration_second_pulse_in_disc_bits;
					
					//int number_of_pulses_to_apply_within_shutter;
					
					// delay_to_startpulses_after_shutter_ns > 100
     constraint cons_delay { delay_to_startpulses_after_shutter_ns inside {[150:300]}; }
					
					
					
					// These constraints override those in the base class because they have the same name
					constraint cons_range_tottoa {
					           
																`ifdef USE_VERY_SHORT_TEST_PULSES
																       tot_ns > 0; 
																							tot_ns < `DELAY_FACTOR_TOT;
																`else
																       tot_ns > `DELAY_FACTOR_TOT; 
																							//tot_ns < 32*`DELAY_FACTOR_TOT; // The maximum recordable value with 5 bits TOT is 31*`DELAY_FACTOR_TOT,
																       // but a larger number is set as the top edge to test cases that saturate the counter
																							tot_ns < 32*8*`DELAY_FACTOR_TOT; // The maximum recordable value with 5 bits TOT and the maximum totDiv is 31*8*`DELAY_FACTOR_TOT,
																       // but a larger number is set as the top edge to test cases that saturate the counter
																`endif
																toa_ns > 0;
																toa_ns < 8192*`DELAY_FACTOR_TOA; // The maximum recordable value with 13 bits TOA is 8191*`DELAY_FACTOR_TOA,
																// but a larger number is set as the top edge to test cases that saturate the counter
					
					           if (number_of_pulses_to_apply_within_shutter == 1 ) { 
                								
																								
																								
																								duration_second_pulse_in_disc_bits > 0; duration_second_pulse_in_disc_bits < tot_ns;
																								duration_first_pulse_in_disc_bits > duration_second_pulse_in_disc_bits; duration_first_pulse_in_disc_bits < tot_ns;
																								
																								
																								time_separation_between_pulses_in_two_disc_bits > duration_first_pulse_in_disc_bits - duration_second_pulse_in_disc_bits;
																								time_separation_between_pulses_in_two_disc_bits < duration_first_pulse_in_disc_bits; // to ensure that the pulses in two disc bits overlap in the OR
																								// of disc bits and thus yield one single pulse ot tot_ns duration
																								
																								
																								time_separation_between_pulses_in_two_disc_bits + duration_second_pulse_in_disc_bits == tot_ns;
																								
																								
																								//               ____________________
																								// shutter   ___|                    |___
																								//                  _______
																								// disc bit x _____|<- a ->|________       a = duration_first_pulse_in_disc_bits, b = duration_second_pulse_in_disc_bits, c = time_separation_between_pulses_in_two_disc_bits
																								//                 <-c-> _______
																								// disc bit y __________|<- b ->|________
																								//                  ____________
																								// OR disc bits ___|            |_______
																								//
																								//  b + c = tot_ns
																								//  c < a < b + c
																}
																else {
																       duration_first_pulse_in_disc_bits > 0; duration_first_pulse_in_disc_bits < tot_ns; duration_first_pulse_in_disc_bits < toa_ns;
																							duration_second_pulse_in_disc_bits > 0; duration_second_pulse_in_disc_bits < tot_ns;
																							duration_first_pulse_in_disc_bits + duration_second_pulse_in_disc_bits == tot_ns;
																							time_separation_between_pulses_in_two_disc_bits > 0; time_separation_between_pulses_in_two_disc_bits < toa_ns - duration_first_pulse_in_disc_bits;
																							
																							//               ____________________
																							// shutter   ___|                    |___
																							//                  _______
																							// disc bit x _____|<- a ->|________       a = duration_first_pulse_in_disc_bits, b = duration_second_pulse_in_disc_bits, c = time_separation_between_pulses_in_two_disc_bits
																							//                         <-c-> _______
																							// disc bit y __________________|<- b ->|________
																							//                  _______      _______
																							// OR disc bits ___|       |____|       |_______
																							//
																							//  b + a = tot_ns
																							//  c + a < toa_ns (otherwise, b pulse wouldn't start within the shutter)
																}
     }
					
					/*
					covergroup covergroup_fields_time_meas;
					           cover_tot_ns: coverpoint tot_ns{
																                         bins very_low = {[0:`DELAY_FACTOR_TOT]};
																																									bins intermediate = {[`DELAY_FACTOR_TOT+1:16*`DELAY_FACTOR_TOT]};
																																									bins high = default; // the rest of values
																              }
																cover_toa_ns: coverpoint toa_ns{
																                         bins very_low = {[0:`DELAY_FACTOR_TOA]};
																																									bins intermediate = {[`DELAY_FACTOR_TOA+1:255*`DELAY_FACTOR_TOT]}; // until the max value achievable with 8 bits
																																									bins high = default; // the rest of values
																              }
					endgroup: covergroup_fields_time_meas
     
					
					// # ** Error at ../environment/auxiliary_classes/FieldsTimeMeasurements_for_coverage.sv(107): Variables of embedded Covergroup type 'covergroup_fields_time_meas' cannot be created.
					
					covergroup_fields_time_meas covgroup_fields_time_meas;
*/
    // Constructor
		function new(string name = "");
      super.new(name);
						number_of_pulses_to_apply_within_shutter = 1;
						//covgroup_fields_time_meas = new();
		endfunction: new
		
		/*
		// This function overrides the built-in post_randomize; it is called automatically by randomize()
		// after randomization has been performed. The idea is that every time this class is randomised,
		// so tot_ns and toa_ns have a new value, we record this update in the covergroup bins
	 function void post_randomize();		
		         covgroup_fields_time_meas.sample();
		endfunction: post_randomize
		*/
		

endclass: FieldsTimeMeasurements_for_coverage

`endif // __FieldsTimeMeasurements_for_coverage_sv__


      
