// Filename           : ReportingEnable.sv
// Author             : Núria Egidos 
// Created on         : 30/8/18
// Last modification  : 30/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Select the level of detail of reporting by enabling printing messages at certain stages of the test or not

`ifndef __ReportingEnable_sv__
`define __ReportingEnable_sv__


class ReportingEnable extends uvm_object;
      `uvm_object_utils( ReportingEnable ) // Register the object to the factory


      // By default, all reporting that depends on these knobs is disabled

      // Reporting in dataout_monitor, breakdown_readout_bits
		   	bit enable_reporting_breakdownreadoutbits;

      // Reporting in slowcontrol_driver
      bit enable_reporting_slowcontroldriver;

      // Reporting in CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence
      bit enable_reporting_matrixconfigonehalfseq;

      // Reporting in matrix_model
      bit enable_reporting_matrixmodel;
						bit enable_reporting_matrixmodelcomp;
						
						// Detailed reporting in dataout_comparator in STATUS_MATRIX_READOUT_ACQUIRED
						bit enable_reporting_dataoutcomparator_readoutacquired;



    // Constructor
		function new(string name = "");
      super.new(name);
		endfunction: new


endclass: ReportingEnable






`endif // __ReportingEnable_sv__

