// Filename           : ChipStatusInfo.sv
// Author             : Núria Egidos 
// Created on         : 14/8/18
// Last modification  : 14/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Auxiliary class to store the chip status (reset, configuration, etc.)
//                      The monitors, golden models, etc. have a handle of this class, and the 
//                      chip status is updated from the virtual interface, and automatically
//                      updated in all components that have those handles.


// UVM events: https://www.edaplayground.com/s/4/1093
// https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1c/html/files/base/uvm_event-svh.html#uvm_event.new



`ifndef __ChipStatusInfo_sv__
`define __ChipStatusInfo_sv__


class ChipStatusInfo extends uvm_object;
      `uvm_object_utils( ChipStatusInfo ) // Register the object to the factory


			chip_status_type chip_status;


			// UVM events are used to synchronize the different modules; the event below is triggered from 
      // the virtual sequence when chip_status is updated, and the components in the testbench
      // (for instance dataout_monitor) are waiting to be notified of this change in order to perform
      // some action
      uvm_event chip_status_update_event;




    // Constructor
		function new(string name = "");
      super.new(name);
      chip_status = STATUS_RESET;
      chip_status_update_event = new("chip_status_update_event");
		endfunction: new


endclass: ChipStatusInfo


/*
class ChipStatusInfo;

			chip_status_type chip_status;

    // Constructor
		function new(chip_status_type chip_status_arg = STATUS_RESET);
      chip_status = chip_status_arg;
		endfunction: new


endclass: ChipStatusInfo
*/



/*

OLD VERSION:
I have a variable, chip_status (reset/configuration/acquisition), which is used by golden models (they will provide one or another expected value according to the status of the chip) and which I want to set from the virtual sequence. The idea is to change chip_status from the virtual sequence; according to its value, one or another sequence will be started and then one or another expected value will be provided by the models.

What I've done is creating chip_status in the test and storing it in the configuration database in the build phase:
uvm_config_db#(chip_status_type)::set( .cntxt(this), .inst_name("*"), .field_name("chip_status"), .value(chip_status) );

Then I retrieve it in all models (in the build phase) into the local handle:  uvm_config_db#(chip_status_type)::get(this, "", "chip_status", chip_status)
 and in the virtual sequence (in the body task) into the local handle: uvm_config_db#(chip_status_type)::get( .cntxt(null), .inst_name(this.get_full_name()), .field_name("chip_status"), .value(chip_status) );
In both cases the initial value of chip_status is correctly retrieved.

Then I change the value of chip_status handle in the virtual sequence, but this change is not updated anywhere else automatically. 
I also tried updating the changed value of chip_status from the virtual sequence into the configuration database: uvm_config_db#(chip_status_type)::set( .cntxt(null), .inst_name("*"), .field_name("chip_status"), .value(chip_status) );
but it's not updated anywhere either.


TUOMAS' RECOMMENDATION:
In general, config db should only used for initial configuration, not during simulation. Your example should work if chip_status_type is an object. If it's not an object, and it's integral value, then each component gets its own copy of the value, and thus updating one will not change the other.

In that case, you should create a new class, say ChipStatusData, and create an object of this type. You can add whatever data you need inside this object. Use uvm_config_db initially to pass this object handle to all components requiring it. Now each component will get a copy of the handle referring to the same object.

An example of status data object:

class ChipStatusData;
    chip_status_type chip_status;
   // whatever else you need
endclass



*/




`endif // __ChipStatusInfo_sv__

