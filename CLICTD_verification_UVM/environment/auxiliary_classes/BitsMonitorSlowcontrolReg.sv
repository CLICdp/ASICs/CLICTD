// Filename           : BitsMonitorSlowcontrolReg.sv
// Author             : Núria Egidos 
// Created on         : 14/8/18
// Last modification  : 14/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Auxiliary class to store the status of certain bits of the slow control registers



`ifndef __BitsMonitorSlowcontrolReg_sv__
`define __BitsMonitorSlowcontrolReg_sv__


class BitsMonitorSlowcontrolReg extends uvm_object;
      `uvm_object_utils( BitsMonitorSlowcontrolReg ) // Register the object to the factory


			// Bits used to monitor the status of some slow control registers
      // globalConfig
			bit pwrExtEn; // set to 1 to enable external power pulsing
			bit tpExtEn;  // set to 1 to enable external test pulse
			bit tpEn;     // set to 1 to enable test pulse

			// internalStrobes
			bit pwrInt;  // set to 1 to start power pulse activated from the slow control, set to 0 to stop it
			bit tpInt;   // set to 1 to start test pulse activated from the slow control, set to 0 to stop it

			// matrixConfig
			bit photonCnt; // set to 1 to enable photon counting mode
			bit noCompr;   // set to 1 to disable compression
			bit longCnt;   // set to 1 to enable 13-bit long ToA counter
			bit [1:0] totDiv; // set to 0 for ToT clock freq = 50 MHz, 
			// set to 1 for 25 MHz, to 2 for 12.5 MHz, to 3 for 6.25 MHz 

			// configCtrl
			bit configSend;        // set to 0 initially; when configData has been filled with the 
			// corresponding configuration bit for the matrix, set configSend to 1 to enable shifting 
			// these data once, then set back to 0
			bit [1:0] configStage; // set to 2'b01 for the first config. half, 2'b10 for the second
		
			// configData7downto0
			// configData15downto8

			// readoutCtrl
			bit roExtEn; // set to 1 to enable readout activated from external pulse
			bit roInt;   // set to 1 to start readout activated from the slow control, set to 0 to stop it

			// tpulseCtrl7downto0
			// tpulseCtrl15downto8





    // Constructor
		function new(string name = "");
      super.new(name);
			// Bits used to monitor the status of some slow control registers
      // globalConfig
		  pwrExtEn = 0; // set to 1 to enable external power pulsing
			tpExtEn = 0;  // set to 1 to enable external test pulse
			tpEn = 1;     // set to 1 to enable test pulse

			// internalStrobes
			//pwrInt = 1;  // set to 1 to start power pulse activated from the slow control, set to 0 to stop it
			pwrInt = 0;  // powered on by default
			tpInt = 0;   // set to 1 to start test pulse activated from the slow control, set to 0 to stop it

			// matrixConfig
			photonCnt = 1; // set to 1 to enable photon counting mode
			noCompr = 1;   // set to 1 to disable compression
			longCnt = 0;   // set to 1 to enable 13-bit long ToA counter
			totDiv = 2'b0; // set to 0 for ToT clock freq = 50 MHz, 
			// set to 1 for 25 MHz, to 2 for 12.5 MHz, to 3 for 6.25 MHz 

			// configCtrl
			configSend = 0;        // set to 0 initially; when configData has been filled with the 
			// corresponding configuration bit for the matrix, set configSend to 1 to enable shifting 
			// these data once, then set back to 0
			configStage = 2'b01; // set to 2'b01 for the first config. half, 2'b10 for the second
		
			// configData7downto0
			// configData15downto8

			// readoutCtrl
			roExtEn = 0; // set to 1 to enable readout activated from external pulse
			roInt = 0;   // set to 1 to start readout activated from the slow control, set to 0 to stop it

			// tpulseCtrl7downto0
			// tpulseCtrl15downto8
		endfunction: new


endclass: BitsMonitorSlowcontrolReg

`endif // __BitsMonitorSlowcontrolReg_sv__


      
