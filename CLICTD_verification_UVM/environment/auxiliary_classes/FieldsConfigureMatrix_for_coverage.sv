// Filename           : FieldsConfigureMatrix_for_coverage.sv
// Author             : N�ria Egidos 
// Created on         : 14/12/18
// Last modification  : 15/1/19
// Project            : Doctoral student, verification of CLICTD
// Description        : Auxiliary class to store the configuration of the whole matrix - used for coverage collection

// http://www.testandverification.com/wp-content/uploads/2018//DVClub_Europe/John_Dickol.pdf
// https://verificationacademy.com/forums/systemverilog/random-associative-array
// we can't randomize the values of the positions of an associative array before being created... 


`ifndef __FieldsConfigureMatrix_for_coverage_sv__
`define __FieldsConfigureMatrix_for_coverage_sv__




typedef struct packed {
	  int column;
		 int superpixel_segment;
		 int superpixel_in_segment;
		 int bit_applydiscpulse_0;
		 int bit_applydiscpulse_1;
			int pulsed_digital_or_forcedisc;
	} Superpixel_hit_coordinates_struct;
	
	
class FieldsConfigureMatrix_for_coverage extends FieldsConfigureMatrix;
    `uvm_object_utils( FieldsConfigureMatrix_for_coverage ) // Register the object to the factory

    //logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED;

				
				rand int occupancy; // number of superpixels hit in the test 
/*
    rand bit value_for_dummy_bit; // value of the dummy bits, i.e. those that are required to be sent during each configuration
     // half (bit 21 in the 1st half, bit 0 and 21 in the 2nd half), but they are not configuration values

    // Superpixels to apply digital pulse
    // pins_if.tpEnableDigital is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains 1 bit
    rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] tpEnableDigital;

    // Pixels to mask
		// pins_if.mask is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] mask;

    // Pixels to apply analog pulse
		// pins_if.tpEnableAnalog is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] tpEnableAnalog;

		// pins_if.tuningDACx are COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed arrays and each of their positions contains 3 bits
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC0;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC1;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC2;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC3;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC4;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC5;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC6;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC7;
		
	*/	
		
		// rand' and 'randc' are not currently supported for struct/union member declarations. Variables declared with the 'rand' modifier must be of an integral or class handle type
		// https://verificationacademy.com/forums/systemverilog/how-constrain-elements-structure
	 Superpixel_hit_coordinates_struct superpixels_to_pulse [int];
		
		// Auxiliary variable used to generate the coordinates of superpixels to pulse; size of X coordinate corresponds to the maximum possible number of hits applicable
		// to the matrix (one per superpixel at a certain time - note: each of this hits will count as 2, to force two discriminator bits when digital pulse is not used for that superpixel);
		// size of coordinate Y is 3: bit 2 = column, bit 1 = superpixel_segment, bit 0 = superpixel_in_segment
		logic [`COLUMNS*`ROWS-1:0][2:0][3:0] aux_coordinates_of_superpixels_to_pulse; // the first index of this matrix corresponds to all possible positions that can be hit (as many as superpixels in the matrix);
		// the second index corresponds to the coordinates of such superpixels (2 for column, 1 for superpixel_segment, 0 for superpixel_in_segment); the third index is for the 4 bits that represent each of thos coordinates
		
		
		constraint cons_occupancy {
		           occupancy > 0; // at least one superpixel hit
													//`ifdef READOUTTESTFORCOVERAGE_DEBUGMODE 
											  //     occupancy == 4;
													//`else 
													//     occupancy < `ROWS*`COLUMNS; // total of superpixels in the matrix
											  //`endif
													//occupancy == 4;
													occupancy == `ROWS*`COLUMNS; // force hits at all suerpixels in the matrix
													
													//occupancy%2==0; // an even number of superpixels are hit, so that half of these are hit with a digital test pulse and the other half, forcing disc bits
		}
		
				
		
		function void initialize_possible_coordinates(); 
											int auxiliary_vector_to_store_used_positions[int];
											int random_index;
											
           // Each possible coordinate is assigned to a random position of the matrix (that defined by random_index),
											// so that when we later iterate the matrix and assign the coordinates in the first half of occupancy/2 positions to superpixels where 
											// digital pulsing will be applied, and the second half to forcing hits at some discriminator bits, none of this halves will be
											//  associated to fixed physical positions of the matrix, but to random coordinates											
											for (int c = 0; c < `COLUMNS; c++) begin
											    for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
															    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin 
																			    do 
																							begin
																							  random_index = $urandom_range(`COLUMNS*`ROWS-1,0);
																							end
																							while (auxiliary_vector_to_store_used_positions.exists(random_index));
																							
																							auxiliary_vector_to_store_used_positions[random_index] = random_index;
																			    aux_coordinates_of_superpixels_to_pulse[random_index][2] = c;
																							aux_coordinates_of_superpixels_to_pulse[random_index][1] = ss;
																							aux_coordinates_of_superpixels_to_pulse[random_index][0] = sis;
																							//$display("DEBUG - fields config matrix for coverage - aux_coordinates_of_superpixels_to_pulse[%0d] = %0d %0d %0d",random_index,aux_coordinates_of_superpixels_to_pulse[random_index][2],
																							//aux_coordinates_of_superpixels_to_pulse[random_index][1],aux_coordinates_of_superpixels_to_pulse[random_index][0]);
																							
																			end
															end
											end
											auxiliary_vector_to_store_used_positions.delete();
											
		endfunction: initialize_possible_coordinates
		
    
		
	
		
		
		// When readout is compressed, hit flag may be 0 if no hit has arrived or, even if some hit has arrived, when masking is enabled in that superpixel;
		// in this function, expected_hit_flag is updated taking into account the hit generation and the masking configuration, assumin readout compression
		// is enabled
		function void update_expected_hit_flag_when_readout_compressed();
		              //expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED = '0; // need to update inside the loop, otherwise wrong values obtained
																foreach (superpixels_to_pulse[i]) begin
																         // If a digital test pulse is applied in this superpixel, or if some hit is forced at some discriminator bit and not all of the discriminator bits (that are hit) are masked 
																									/*
																         if ( superpixels_to_pulse[i].pulsed_digital_or_forcedisc == `SUPERPIXEL_COORDINATE_EXPECTS_DIGITAL_TEST_PULSE ||
																									     !mask[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment][superpixels_to_pulse[i].bit_applydiscpulse_0] ||
																									     !mask[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment][superpixels_to_pulse[i].bit_applydiscpulse_1] 
																									   ) expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment] = 1'b1;
																									else 	expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment] = 1'b0;
																									*/
																										
																										if ( superpixels_to_pulse[i].pulsed_digital_or_forcedisc == `SUPERPIXEL_COORDINATE_EXPECTS_DIGITAL_TEST_PULSE ||
																										     superpixels_to_pulse[i].pulsed_digital_or_forcedisc == `SUPERPIXEL_COORDINATE_EXPECTS_FORCE_PUSE_AT_SOME_DISC_BIT &&  
																															(!mask[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment][superpixels_to_pulse[i].bit_applydiscpulse_0] ||
																									       !mask[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment][superpixels_to_pulse[i].bit_applydiscpulse_1])
																														) expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment] = 1'b1;
																									else 	expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment] = 1'b0;
																																		
																									
																end
		              
		endfunction: update_expected_hit_flag_when_readout_compressed
		
		
		function void check_if_coordinates_are_unique();
		         // Iterate all the possible coordinates to check if there are some which are exactly the same in the three positions:
											// column, superpixel_segment and superpixel_in_segment
											logic [`COLUMNS*`ROWS-2:0] vector_result_xors;
											logic [`COLUMNS*`ROWS-1:0] vector_result_ands;
											int counter;
											for (int i = 0; i < `COLUMNS*`ROWS; i++) begin
											     // Xor the coordinates at position i with the rest of coordinates. The xor will result in 1 if the coordinates are different, which 
																// should be the case
																counter = 0;
																for (int j = 0; j < `COLUMNS*`ROWS; j++) begin
																    if (i != j) begin 
																				                  //vector_result_xors[j] = aux_coordinates_of_superpixels_to_pulse[i] ^ aux_coordinates_of_superpixels_to_pulse[j];
																																						if ( aux_coordinates_of_superpixels_to_pulse[i][2] == aux_coordinates_of_superpixels_to_pulse[j][2] &&
																																						     aux_coordinates_of_superpixels_to_pulse[i][1] == aux_coordinates_of_superpixels_to_pulse[j][1] &&
																																											aux_coordinates_of_superpixels_to_pulse[i][0] == aux_coordinates_of_superpixels_to_pulse[j][0] 
																																						    ) vector_result_xors[counter] = 1'b0;
																																						else vector_result_xors[counter] = 1'b1;
																				                  if (!vector_result_xors[counter]) begin 
																																						    $display("DEBUG - fields configure matrix for coverage - error repeated coordinates: %0d , %0d - %0d - %0d vs %0d, %0d - %0d - %0d",
																																						    i,aux_coordinates_of_superpixels_to_pulse[i][2], aux_coordinates_of_superpixels_to_pulse[i][1], aux_coordinates_of_superpixels_to_pulse[i][0],
																																										j,aux_coordinates_of_superpixels_to_pulse[j][2], aux_coordinates_of_superpixels_to_pulse[j][1], aux_coordinates_of_superpixels_to_pulse[j][0] );
																																						end
																																						counter++;
																				end
																				
																end
																
																vector_result_ands[i] = &vector_result_xors; // it will be 1 only if all coordinates are different
																//if (i == 0) $display("DEBUG - fields config matrix for coverage - %0d vector ands %h, vector xors %h ",i,vector_result_ands[i],vector_result_xors);
																
											end
											
											if (&vector_result_ands) $display("DEBUG - fields configure matrix for coverage - :) all positions in aux_coordinates_of_superpixels_to_pulse are unique");
											else $display("DEBUG - fields configure matrix for coverage - :( some positions in aux_coordinates_of_superpixels_to_pulse are repeated");
		endfunction: check_if_coordinates_are_unique
		
		
		 


    // Constructor
		function new(string name = "");
      super.new(name);						
						
		endfunction: new
		
		
		// This function overrides the built-in post_randomize; it is called automatically by randomize()
		// after randomization has been performed. In other words, when columns_to_pulse, ss_applydigitalpulse,
		// sis_applydigitalpulse, ss_applydiscpulse, sis_applydiscpulse, bit_applydiscpulse_0 and bit_applydiscpulse_1 
		// have their randomized value, fix_some_config_values_if_randomhitpattern is called so that the rest of
		// attributes of this class have a value matching the aforementioned fields
		// https://www.verificationguide.com/p/systemveilog-randomization-methods.html
		//virtual function void post_randomize(); // this gave error
	 function void post_randomize();		
		         int aux_random_int;									
											// to avoid configuration error in tuningDAC1[c][ss][sis][0] and tuningDAC7[c][ss][sis][2]
											value_for_dummy_bit = 1'b1; 
											
											// Every time we randomize the order of the coordinates in aux_coordinates_of_superpixels_to_pulse will change,
											// so the coordinates translated into superpixels_to_pulse will also change
											initialize_possible_coordinates();
											//check_if_coordinates_are_unique(); // DEBUG
										
											// Update the coordinates of superpixels to pulse
											for (int i = 0; i < occupancy; i++) begin
											     if (i < occupancy/2) superpixels_to_pulse[i].pulsed_digital_or_forcedisc = `SUPERPIXEL_COORDINATE_EXPECTS_DIGITAL_TEST_PULSE; 
																else superpixels_to_pulse[i].pulsed_digital_or_forcedisc = `SUPERPIXEL_COORDINATE_EXPECTS_FORCE_PUSE_AT_SOME_DISC_BIT;
											     superpixels_to_pulse[i].column = aux_coordinates_of_superpixels_to_pulse[i][2];
																superpixels_to_pulse[i].superpixel_segment = aux_coordinates_of_superpixels_to_pulse[i][1];
																superpixels_to_pulse[i].superpixel_in_segment = aux_coordinates_of_superpixels_to_pulse[i][0];
																superpixels_to_pulse[i].bit_applydiscpulse_0 = $urandom_range(`PIXELS_IN_ONE_SUPERPIXEL-1,0);
																do 
																begin
																		aux_random_int = $urandom_range(`PIXELS_IN_ONE_SUPERPIXEL-1,0);
																end
																while (superpixels_to_pulse[i].bit_applydiscpulse_0 == aux_random_int);					
																superpixels_to_pulse[i].bit_applydiscpulse_1 = aux_random_int;
																					
																if (superpixels_to_pulse[i].bit_applydiscpulse_0 == superpixels_to_pulse[i].bit_applydiscpulse_1) $display("DEBUG - fields config matrix for coverage - superpixel %0d bits force disc 0 = %b, 1 = %b are the same",
																                                                                                                  i,superpixels_to_pulse[i].bit_applydiscpulse_0,superpixels_to_pulse[i].bit_applydiscpulse_1);
											end
											
											tpEnableDigital = '0; 
											// Update the value of tpEnableDigital according to the superpixel coordinates where an external test pulse will be applied
											foreach (superpixels_to_pulse[i]) begin
																    if (superpixels_to_pulse[i].pulsed_digital_or_forcedisc == `SUPERPIXEL_COORDINATE_EXPECTS_DIGITAL_TEST_PULSE) begin
																							tpEnableDigital[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment] = 1'b1;
																								//$display("DEBUG- field config matrix for coverage - a - %0d tpEnabledigital %b",i,tpEnableDigital[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment]);
																				end
																				else begin tpEnableDigital[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment] = 1'b0; 
																								  //   $display("DEBUG- field config matrix for coverage - b - %0d tpEnabledigital %b",i,tpEnableDigital[superpixels_to_pulse[i].column][superpixels_to_pulse[i].superpixel_segment][superpixels_to_pulse[i].superpixel_in_segment]); 
																				end

											end
											
											
											
											// Set expected_hit_flag to 1 for all superpixels (assuming readout compression is disabled)
											expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED = '1; // unless readout compressed (and masked and/or no hit received), all hit flags should be 1
											
											// Disable analog test pulsing
						     tpEnableAnalog = '0;	
											
											
											
											
		endfunction: post_randomize
   
		
		
		
endclass: FieldsConfigureMatrix_for_coverage

`endif // __FieldsConfigureMatrix_for_coverage_sv__


      
