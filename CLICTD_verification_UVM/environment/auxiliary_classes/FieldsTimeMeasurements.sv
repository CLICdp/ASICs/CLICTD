// Filename           : FieldsTimeMeasurements.sv
// Author             : N�ria Egidos 
// Created on         : 2/11/18
// Last modification  : 2/11/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Auxiliary class to store the expected values of time measurement (ToT, ToA)



`ifndef __FieldsTimeMeasurements_sv__
`define __FieldsTimeMeasurements_sv__


class FieldsTimeMeasurements extends uvm_object;
    `uvm_object_utils( FieldsTimeMeasurements ) // Register the object to the factory
				
				 rand int delay_to_startpulses_after_shutter_ns;
     rand int tot_short_ns;
     rand int tot_long_ns; 
     rand int toa_ns;
					
					
					//rand shortreal delay_to_startpulses_after_shutter_ns;
     //rand shortreal tot_short_ns;
     //rand shortreal tot_long_ns; 
     //rand shortreal toa_ns;
					
					
					bit digital_pulse_lasts_longer_than_shutter;
					int number_of_pulses_to_apply_within_shutter;
					
					// delay_to_startpulses_after_shutter_ns > 100
     constraint cons_delay { delay_to_startpulses_after_shutter_ns inside {[150:300]}; }
					
					/*
     // 32*`DELAY_FACTOR_TOT > tot_long_ns > toa_ns > tot_short_ns + 100 > 300
     constraint cons_range_tottoa { 
                tot_short_ns > 200; // larger than 200 to take into account the delay_to_startpulses_after_shutter_ns 
																                                 // and smaller than 620 so that the largest value can be represented with 5 bits 
																																																	// (620 ns / 20 ns period = 31)
																tot_short_ns < 31*`DELAY_FACTOR_TOT;
                toa_ns > tot_short_ns + 100;
																//toa_ns < 255*`DELAY_FACOTR_TOA; // this would be feasible if we didn't have the constraint tot_long_ns > toa_ns; for this 
																// constraint to apply, toa_ns must be smaller than the max value of tot_ns
																toa_ns < 31*`DELAY_FACTOR_TOT;
                tot_long_ns > toa_ns;
                tot_long_ns < 31*`DELAY_FACTOR_TOT;
     }
					*/
					
					/*
					// From 23/11/18, to be able to allocate one or two test pulses within the shutter
					constraint cons_range_tottoa { 
                tot_short_ns > 400; 
																tot_short_ns < 31*`DELAY_FACTOR_TOT -100;
                //toa_ns > tot_short_ns/2 + 100;
																toa_ns > tot_short_ns;
																//toa_ns < 255*`DELAY_FACOTR_TOA; // this would be feasible if we didn't have the constraint tot_long_ns > toa_ns; for this 
																// constraint to apply, toa_ns must be smaller than the max value of tot_ns
																toa_ns < tot_short_ns + 100;
                tot_long_ns == tot_short_ns + 100;
																
     }
					*/
					
					// From 4/12/18
					constraint cons_range_tottoa {
					
					
					
					           if (number_of_pulses_to_apply_within_shutter == 1 ) { 
                								tot_short_ns > 2*`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION; 
																								`ifdef USE_VERY_SHORT_TEST_PULSES
																								       tot_short_ns < `DELAY_FACTOR_TOT - `MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION;
																								`else
																								       tot_short_ns < 31*`DELAY_FACTOR_TOT - `MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION;
																								`endif
																								toa_ns > tot_short_ns + `MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION;
																								toa_ns < tot_long_ns;
                								
																								`ifdef USE_VERY_SHORT_TEST_PULSES
																								       tot_long_ns < `DELAY_FACTOR_TOT;
																								`else
																								       tot_long_ns < 31*`DELAY_FACTOR_TOT;
																								`endif
																}
																else {
																       tot_short_ns > 4*`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION;
																							`ifdef USE_VERY_SHORT_TEST_PULSES
																								       tot_short_ns < `DELAY_FACTOR_TOT - `MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION;
																								`else
																								       tot_short_ns < 31*`DELAY_FACTOR_TOT - `MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION;
																								`endif
																							toa_ns == tot_short_ns + `MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION;
																							//toa_ns < 31*`DELAY_FACTOR_TOT;
																							tot_long_ns == toa_ns;
																}
     }
     

    // Constructor
		function new(string name = "");
      super.new(name);
						number_of_pulses_to_apply_within_shutter = 1;
		endfunction: new

endclass: FieldsTimeMeasurements

`endif // __FieldsTimeMeasurements_sv__


      
