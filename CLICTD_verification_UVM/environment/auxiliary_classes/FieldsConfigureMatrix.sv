// Filename           : FieldsConfigureMatrix.sv
// Author             : Núria Egidos 
// Created on         : 24/8/18
// Last modification  : 31/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Auxiliary class to store the configuration of the whole matrix



`ifndef __FieldsConfigureMatrix_sv__
`define __FieldsConfigureMatrix_sv__


class FieldsConfigureMatrix extends uvm_object;
    `uvm_object_utils( FieldsConfigureMatrix ) // Register the object to the factory

    logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED;

    matrix_config_fields_type matrix_config_fields;

    rand bit value_for_dummy_bit; // value of the dummy bits, i.e. those that are required to be sent during each configuration
     // half (bit 21 in the 1st half, bit 0 and 21 in the 2nd half), but they are not configuration values

    // Superpixels to apply digital pulse
    // pins_if.tpEnableDigital is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains 1 bit
    rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] tpEnableDigital;

    // Pixels to mask
		// pins_if.mask is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] mask;

    // Pixels to apply analog pulse
		// pins_if.tpEnableAnalog is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] tpEnableAnalog;

		// pins_if.tuningDACx are COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed arrays and each of their positions contains 3 bits
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC0;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC1;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC2;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC3;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC4;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC5;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC6;
		rand bit [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC7;
		
		
		//rand int [8:0] columns_to_pulse; // one of these positions will be the column to apply a digital test pulse; the rest, the columns to apply some pulse at the disc bits
		// # ** Error: ** while parsing file included at ../CLICTD_testbench_top.sv(18)
  // # ** while parsing file included at ../tests//CLICTD_test_pkg.sv(88)
  // # ** at ../environment/auxiliary_classes/FieldsConfigureMatrix.sv(46): Can't have packed array of integer type.
		rand int columns_to_pulse[8:0];
		
		rand int ss_applydigitalpulse, sis_applydigitalpulse; // superpixel segment and superpixel in segment to which the digital test pulse is applied
  rand int ss_applydiscpulse, sis_applydiscpulse, bit_applydiscpulse_0, bit_applydiscpulse_1; // superpixel segment, superpixel in segment and bits out of the 8 disc bits to which some pulse is applied
 
  constraint cons_position_pixels_to_pulse {
		// http://www.asic-world.com/systemverilog/random_constraint8.html
		
		
		
		               /*
						           //matrix_config_fields == RANDOM_HITPATTERN -> { // constraints apply only in this option
																	       // Only one column has digital test pulse enabled
																	foreach (columns_to_pulse[i]) columns_to_pulse[i] inside  {[0:`COLUMNS-1]};
																	unique {columns_to_pulse}; // all column positions different
																	ss_applydigitalpulse  inside {[0:`SUPERPIXEL_SEGMENTS-1]};
																	sis_applydigitalpulse inside {[0:`SUPERPIXELS_IN_ONE_SEGMENT-1]};
																	ss_applydiscpulse     inside {[0:`SUPERPIXEL_SEGMENTS-1]};
																	sis_applydiscpulse    inside {[0:`SUPERPIXELS_IN_ONE_SEGMENT-1]};
																	bit_applydiscpulse_0  inside {[0:`PIXELS_IN_ONE_SUPERPIXEL-1]};
																	bit_applydiscpulse_1  inside {[0:`PIXELS_IN_ONE_SUPERPIXEL-1]};
																	//}
																	*/
																	
																	columns_to_pulse[0] == 0;
																	columns_to_pulse[1] == 1;
																	columns_to_pulse[2] == 2;
																	columns_to_pulse[3] == 3;
																	columns_to_pulse[4] == 4;
																	columns_to_pulse[5] == 5;
																	columns_to_pulse[6] == 6;
																	columns_to_pulse[7] == 7;
																	columns_to_pulse[8] == 8;
																	ss_applydigitalpulse == 0;
																	sis_applydigitalpulse == 0;
																	ss_applydiscpulse == 0;
																	sis_applydiscpulse == 0;
																	bit_applydiscpulse_0 == 0;
																	bit_applydiscpulse_1 == 3;
																	//bit_applydiscpulse_1 == 1;
																	// Why are these values of bit_applydiscpulse_0,1 selected? Because bit 0 (or the same applies to 7) is located at the edge of the superpixel "strip of front-ends",
																	// so signals will experience a longer propagation delay (~4ns in max corner) to the OR of disc bits than signals closer to it, such as bit 3. The idea is to force
																	// timing differences that could lead to miscounts (difference between the measured ToT/ToA and the actual ToT/ToA) larger than +/-1 count (perhaps +/-2)
																	
																	
						}
    
				
		function void fix_some_config_values_if_randomhitpattern();
		              tpEnableDigital = '0; tpEnableDigital[columns_to_pulse[0]][ss_applydigitalpulse][sis_applydigitalpulse] = 1'b1;
																mask = '0; 
																mask[columns_to_pulse[2]][ss_applydiscpulse][sis_applydiscpulse][bit_applydiscpulse_0] = 1'b1; mask[columns_to_pulse[3]][ss_applydiscpulse][sis_applydiscpulse][bit_applydiscpulse_1] = 1'b1;
																mask[columns_to_pulse[4]][ss_applydiscpulse][sis_applydiscpulse][bit_applydiscpulse_0] = 1'b1; mask[columns_to_pulse[4]][ss_applydiscpulse][sis_applydiscpulse][bit_applydiscpulse_1] = 1'b1;
																mask[columns_to_pulse[6]][ss_applydiscpulse][sis_applydiscpulse][bit_applydiscpulse_0] = 1'b1; mask[columns_to_pulse[7]][ss_applydiscpulse][sis_applydiscpulse][bit_applydiscpulse_1] = 1'b1;
																mask[columns_to_pulse[8]][ss_applydiscpulse][sis_applydiscpulse][bit_applydiscpulse_0] = 1'b1; mask[columns_to_pulse[8]][ss_applydiscpulse][sis_applydiscpulse][bit_applydiscpulse_1] = 1'b1;
																tpEnableAnalog = '0;
																expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED = '0;
																expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[columns_to_pulse[0]][ss_applydigitalpulse][sis_applydigitalpulse] = 1'b1;
																expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[columns_to_pulse[1]][ss_applydiscpulse][sis_applydiscpulse] = 1'b1;
																expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[columns_to_pulse[2]][ss_applydiscpulse][sis_applydiscpulse] = 1'b1;
																expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[columns_to_pulse[3]][ss_applydiscpulse][sis_applydiscpulse] = 1'b1;
																expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[columns_to_pulse[5]][ss_applydiscpulse][sis_applydiscpulse] = 1'b1;
																expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[columns_to_pulse[6]][ss_applydiscpulse][sis_applydiscpulse] = 1'b1;
																expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[columns_to_pulse[7]][ss_applydiscpulse][sis_applydiscpulse] = 1'b1;
																
		endfunction: fix_some_config_values_if_randomhitpattern


    // Constructor
		function new(string name = "");
      super.new(name);						
		endfunction: new
		
		
		// This function overrides the built-in post_randomize; it is called automatically by randomize()
		// after randomization has been performed. In other words, when columns_to_pulse, ss_applydigitalpulse,
		// sis_applydigitalpulse, ss_applydiscpulse, sis_applydiscpulse, bit_applydiscpulse_0 and bit_applydiscpulse_1 
		// have their randomized value, fix_some_config_values_if_randomhitpattern is called so that the rest of
		// attributes of this class have a value matching the aforementioned fields
		// https://www.verificationguide.com/p/systemveilog-randomization-methods.html
		//virtual function void post_randomize(); // this gave error
	 function void post_randomize();
		         //if (matrix_config_fields == RANDOM_HITPATTERN) fix_some_config_values_if_randomhitpattern();
											
											// to avoid configuration error in tuningDAC1[c][ss][sis][0] and tuningDAC7[c][ss][sis][2]
											value_for_dummy_bit = 1'b1; 
											
											// The following for is not necessary, the update of tuningDAC1[c][ss][sis][0] and tuningDAC7[c][ss][sis][2]
											// was updated in the end of column
											/*
											for (int c = 0; c < `COLUMNS; c++) begin
											     for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																										tuningDAC1[c][ss][sis][0] = 1'b1;
																										tuningDAC7[c][ss][sis][2] = 1'b1;
																					end
																end
											end
											*/
						     fix_some_config_values_if_randomhitpattern();
		endfunction: post_randomize
   
		
		
		/*
		// This function overrides the built-in post_randomize; it is called automatically by randomize()
		// after randomization has been performed. This is done to ensure that dummy bit is 1, tuningDAC1[0] is 0
		// and tuningDAC7[2] is 1, so as to check for errors detected in these bits during configuration readout
		// https://www.verificationguide.com/p/systemveilog-randomization-methods.html
		function void post_randomize();
		         value_for_dummy_bit = 1'b1;
											//value_for_dummy_bit = 1'b0;
											
											// NOTE: if 1 is used as dummy bit, the configuration is performed correctly; is 0 is used instead,
											// a 0 is forced to tuningDAC1[0] and to tuningDAC7[2] :(
		         for (int c = 0; c < `COLUMNS; c++) begin
											     for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																					     tuningDAC1[c][ss][sis][0] = 1'b0;
																										tuningDAC7[c][ss][sis][2] = 1'b0;
																										
																										//tuningDAC1[c][ss][sis][0] = 1'b1;
																										//tuningDAC7[c][ss][sis][2] = 1'b1;
																										
																										
																										//tuningDAC1[c][ss][sis][1] = 1'b1;
																										//tuningDAC1[c][ss][sis][2] = 1'b0;
																					end
																end
											end 
		endfunction: post_randomize
		*/
		
endclass: FieldsConfigureMatrix

`endif // __FieldsConfigureMatrix_sv__


      
