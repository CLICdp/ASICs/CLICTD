// Filename           : CLICTD_acquireddata.sv
// Author             : N�ria Egidos 
// Created on         : 2/11/18
// Last modification  : 5/11/18 - Replaced expected_hit_flag from [`ROWS-1:0] to [`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0]
// Project            : Doctoral student, verification of CLICTD
// Description        : Data format and related operations to store the data read out after an acquisition
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/

// On scoreboards: 
// http://www.sunburst-design.com/papers/CummingsSNUG2013SV_UVM_Scoreboards.pdf
// http://www.chipverify.com/uvm/tlm-analysis-port

// On debugging:
// http://forums.accellera.org/topic/1352-sequence-is-not-running-make-sure-sequencer-name-correct/
// https://verificationacademy.com/forums/uvm/how-can-i-see-content-uvmconfigdb



	
	typedef enum { TOA_MEASURED_HIGHER_THAN_EXPECTED, TOA_MEASURED_LOWER_THAN_EXPECTED, TOT_MEASURED_HIGHER_THAN_EXPECTED, TOT_MEASURED_LOWER_THAN_EXPECTED,
	               PHCOUNT_MEASURED_HIGHER_THAN_EXPECTED, PHCOUNT_MEASURED_LOWER_THAN_EXPECTED, HITMAP_MEASURED_DIFFERENT_FROM_EXPECTED, HITFLAG_MEASURED_DIFFERENT_FROM_EXPECTED,
																SUPERPIXEL_NUMBER_MEASURED_DIFFERENT_FROM_EXPECTED, ALL_DATA_EQUAL
 } exitcode_compare_two_superpixeldata_type;
	
	typedef exitcode_compare_two_superpixeldata_type array_exitcode_type [exitcode_compare_two_superpixeldata_type];
	
	typedef array_exitcode_type array_exitcode_fullcolumn_type [int][int];
	
	
	typedef struct packed {
	  logic [7:0] hit_map;
	  logic [7:0] toa;
			logic [4:0] tot;
			logic [12:0] longtoa;
			logic [12:0] photoncount;
			logic hit_flag;
			int superpixel_segment;
			int superpixel_in_segment;
	} Superpixel_data_struct;
	
	
	typedef struct packed {
	   Superpixel_data_struct [`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] superpixel;
	} Acquired_word_struct;
	
	
	function void breakdown_columcontent_into_superpixeldata (
										input logic column_content [int],
										      BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
										      logic [`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] expected_hit_flag, // note: expected_hit_flag already contains the correction for masking, 
																                                     // so if the pixels that were hit are masked, the associated expected_hit_flag is 0
										output Acquired_word_struct superpixels_data
	);
	          int offset_columcontent_position;
											// This associative array is filled with '1' at the positions where the acquired hit_flag should be '1'; later, these positions are forced to '1' at 
											// the hit flags in superpixels_data. It was observed though, even the right values of hit flag were captured (those that are stored in this associative\
											// array), for some asthonishing reason some of these positions were corrupted at the end of the function and some hit flag values in superpixels_data
											// were wrong, so that's why they're forced again to the right value
											logic array_of_ones_to_replace_incorrect_positions_at_zero[int][int];
	         //  How is data stored in column_content?
          //            21 ------- superpixel 0 ------ 0 || 21 -------- superpixel 1 ------ 0 || ... || 21 -------- superpixel 15 ------ 0
          //          MSB ----------------------------------------- column_content ----------------------------------------------------- 0
										offset_columcontent_position = 0;
										array_of_ones_to_replace_incorrect_positions_at_zero.delete();
										
										//if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) $display("DISPLAY - breakdown acquired data - expected_hit_flag = %h",expected_hit_flag);
										for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin 
										    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
														          superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].superpixel_segment = `SUPERPIXEL_SEGMENTS-1-ss;
																								superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].superpixel_in_segment = `SUPERPIXELS_IN_ONE_SEGMENT-1-sis;
																			    	//superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag = column_content[offset_columcontent_position];
										         				// We expect to read 1 bit for this superpixel (compression is enabled and hit flag is 0)
										         				if ( expected_hit_flag[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis] == 1'b0 && bits_slowcontrol_reg_obj.noCompr == 1'b0 ) begin
																							    // Read hit flag
																											//if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) $display("DISPLAY - breakdown acquired data - 1 bit ss %0d sis %0d = %b, offset column content = %0d",
																											//	`SUPERPIXEL_SEGMENTS-1-ss,`SUPERPIXELS_IN_ONE_SEGMENT-1-sis,column_content[offset_columcontent_position],offset_columcontent_position);
																												
																												superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												if ( superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag != expected_hit_flag[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis] ) begin
																							    				`uvm_error( `ERROR_BREAKDOWN_COLUMNCONTENT, $sformatf("b - Detected hit flag = %b, expected hit flag = %b, superpixel ss = %d, sis = %d",
																															superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag,expected_hit_flag[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis],`SUPERPIXEL_SEGMENTS-1-ss,`SUPERPIXELS_IN_ONE_SEGMENT-1-sis) )
																							 				end
																												
																												// We are only acquiring the hit flag. The rest of values are forced to zero for comparison purposes
																												superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_map = '0;
																												superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tot = '0;
																												superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].toa = '0;
																												superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].longtoa = '0;
																												superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].photoncount = '0;
																							end
																							// We expect to read 22 bits for this superpixel
																							else begin
																							
																							
																							     // Read hit map
																							 				for (int j = 0; j < 8; j++) begin
																												     //if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) $display("DISPLAY - breakdown acquired data - hit map[%0d][%0d][%0d] = %b, offset column content = %0d",
																												     //`SUPERPIXEL_SEGMENTS-1-ss,`SUPERPIXELS_IN_ONE_SEGMENT-1-sis,j,column_content[offset_columcontent_position],offset_columcontent_position);
																																	
																								     				superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_map[j] = column_content[offset_columcontent_position];	offset_columcontent_position++;
																												end
																												
																												// Nominal acquisition mode
																												if      ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) begin
																												         // Read ToA
																																					for (int j = 0; j < 8; j++) begin 
																																					     //if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) $display("DISPLAY - breakdown acquired data - toa[%0d][%0d][%0d] = %b, offset column content = %0d",
																												              //`SUPERPIXEL_SEGMENTS-1-ss,`SUPERPIXELS_IN_ONE_SEGMENT-1-sis,j,column_content[offset_columcontent_position],offset_columcontent_position);
																																										
																																					     superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].toa[j] = column_content[offset_columcontent_position]; offset_columcontent_position++;	
																																					end
																								         				// Read ToT
																																					for (int j = 0; j < 5; j++) begin 
																																					     superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].tot[j] = column_content[offset_columcontent_position]; offset_columcontent_position++; 
																																										
																																										//if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) $display("DISPLAY - breakdown acquired data - tot[%0d][%0d][%0d] = %b, offset column content = %0d",
																												              //`SUPERPIXEL_SEGMENTS-1-ss,`SUPERPIXELS_IN_ONE_SEGMENT-1-sis,j,column_content[offset_columcontent_position],offset_columcontent_position);
																																					end
																							 				end
																								    // Long ToA acquisition mode
																												else if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1 ) begin
																								    				for (int j = 0; j < 13; j++) begin 	superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].longtoa[j] = column_content[offset_columcontent_position]; offset_columcontent_position++; end
																							 				end
																												// Photon counting acquisition mode
																												else if ( bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1 ) begin
																								    				for (int j = 0; j < 13; j++) begin superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].photoncount[j] = column_content[offset_columcontent_position]; offset_columcontent_position++; end
																							 				end
																												
																												// Read hit flag
																												//if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) $display("DISPLAY - breakdown acquired data - hit flag[%0d][%0d] = %b, offset column content = %0d",
																												//     `SUPERPIXEL_SEGMENTS-1-ss,`SUPERPIXELS_IN_ONE_SEGMENT-1-sis,column_content[offset_columcontent_position],offset_columcontent_position);
																																	
																												superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag = column_content[offset_columcontent_position]; offset_columcontent_position++;
																												if ( superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag != expected_hit_flag[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis] ) begin
																							    				`uvm_error( `ERROR_BREAKDOWN_COLUMNCONTENT, $sformatf("b - Detected hit flag = %b, expected hit flag = %b, superpixel ss = %d, sis = %d",
																											 	   superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag,expected_hit_flag[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis],`SUPERPIXEL_SEGMENTS-1-ss,`SUPERPIXELS_IN_ONE_SEGMENT-1-sis)  )
																							 				end
																								
																								    
																							end
																							if (superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag && 
																							    superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag == expected_hit_flag[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis]) 
																							    array_of_ones_to_replace_incorrect_positions_at_zero[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis] = 1'b1;
																							//$display("DISPLAY - breakdown acquired data - hit_flag[%0d][%0d] = %b",`SUPERPIXEL_SEGMENTS-1-ss,`SUPERPIXELS_IN_ONE_SEGMENT-1-sis,superpixels_data.superpixel[`SUPERPIXEL_SEGMENTS-1-ss][`SUPERPIXELS_IN_ONE_SEGMENT-1-sis].hit_flag);
										    end   
										end
										for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
										     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
															    //$display("DISPLAY - breakdown acquired data BEFORE - superpixel_data hit_flag[%0d][%0d] = %b",ss,sis,superpixels_data.superpixel[ss][sis].hit_flag);
																			// https://verificationacademy.com/forums/systemverilog/how-use-exists-method-multidimensional-associative-array
																			if (array_of_ones_to_replace_incorrect_positions_at_zero.exists(ss) && array_of_ones_to_replace_incorrect_positions_at_zero[ss].exists(sis)) begin
																			    superpixels_data.superpixel[ss][sis].hit_flag = array_of_ones_to_replace_incorrect_positions_at_zero[ss][sis];
																			    //$display("DISPLAY - breakdown acquired data - array_of_ones_to_replace_incorrect_positions_at_zero[%0d][%0d] = %b, replaced hit flag = %b",
																						//	ss,sis,array_of_ones_to_replace_incorrect_positions_at_zero[ss][sis],superpixels_data.superpixel[ss][sis].hit_flag);
																			end
               end
										end
										
										// make associative array with indexes in which the tool puts a 0 when it should be 1 and then replace in the finished vector
	endfunction: breakdown_columcontent_into_superpixeldata


 function array_exitcode_type compare_two_superpixeldata (
	         input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
										      Superpixel_data_struct superpixel_data_measured,
																Superpixel_data_struct superpixel_data_expected
	);
	         logic [`PIXELS_IN_ONE_SUPERPIXEL-1:0] aux_xor_hitmaps;
										int aux_sum_auxxorhitmaps;
										
	         compare_two_superpixeldata.delete(); // this is done to avoid that errors are accumulated from one call to this function to the next call
										
	         if ( superpixel_data_measured.hit_flag          != superpixel_data_expected.hit_flag          ) compare_two_superpixeldata[HITFLAG_MEASURED_DIFFERENT_FROM_EXPECTED]           = HITFLAG_MEASURED_DIFFERENT_FROM_EXPECTED;
										
										
										if ( superpixel_data_measured.hit_map           != superpixel_data_expected.hit_map           ) compare_two_superpixeldata[HITMAP_MEASURED_DIFFERENT_FROM_EXPECTED]            = HITMAP_MEASURED_DIFFERENT_FROM_EXPECTED;
										
										
										
										if ( superpixel_data_measured.superpixel_segment != superpixel_data_expected.superpixel_segment || superpixel_data_measured.superpixel_in_segment != superpixel_data_expected.superpixel_in_segment )
										    compare_two_superpixeldata[SUPERPIXEL_NUMBER_MEASURED_DIFFERENT_FROM_EXPECTED] = SUPERPIXEL_NUMBER_MEASURED_DIFFERENT_FROM_EXPECTED;
														
											/*			
										// TAKING 1 COUNT AS ERROR MARGIN (if ToT or ToA are more than 1 count away from the expected value, it's considered as an error)
			
			
										// We consider ToT, ToA value comparison successful if the measured value is within +/- 1 count distance from the expected value
	         // Nominal acquisition mode
										if      ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) begin
										         //$display("nominal in!");
																			// Overflow conditions: at the ends of the range, we can't tell if when the value we're measuring differs from the expected value 
																			// it's because it's within 1 count from the expected value (overflow) or if there is an actual error and it's further than one count.
																			// For instance, if expected toa is 8'hFF and measured toa is 8'h00, does it mean that they are 1 count or 8'hFF counts away?
																			if      ( superpixel_data_expected.toa == 8'hFF && superpixel_data_measured.toa != 8'hFF && superpixel_data_measured.toa != 8'hFE ) compare_two_superpixeldata[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_expected.toa == 8'h00 && superpixel_data_measured.toa != 8'h00 && superpixel_data_measured.toa != 8'h01 ) compare_two_superpixeldata[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			// The rest of situations
																			else if ( superpixel_data_expected.toa != 8'hFF && superpixel_data_expected.toa != 8'h00 ) begin
										         									if      ( superpixel_data_measured.toa > superpixel_data_expected.toa + 1 ) compare_two_superpixeldata[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																												else if ( superpixel_data_measured.toa < superpixel_data_expected.toa - 1 ) compare_two_superpixeldata[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			end
																			
																			// Overflow conditions
																			if      ( superpixel_data_expected.tot == 5'h1F && superpixel_data_measured.tot != 5'h1F && superpixel_data_measured.tot != 5'h1E ) compare_two_superpixeldata[TOT_MEASURED_HIGHER_THAN_EXPECTED] = TOT_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_expected.tot == 5'h00 && superpixel_data_measured.tot != 5'h00 && superpixel_data_measured.tot != 5'h01 ) compare_two_superpixeldata[TOT_MEASURED_LOWER_THAN_EXPECTED]  = TOT_MEASURED_LOWER_THAN_EXPECTED;
																			// The rest of situations
																			else if ( superpixel_data_expected.tot != 5'h1F && superpixel_data_expected.tot != 5'h00 ) begin
																		 									if      ( superpixel_data_measured.tot > superpixel_data_expected.tot + 1 ) compare_two_superpixeldata[TOT_MEASURED_HIGHER_THAN_EXPECTED] = TOT_MEASURED_HIGHER_THAN_EXPECTED;
																												else if ( superpixel_data_measured.tot < superpixel_data_expected.tot - 1 ) compare_two_superpixeldata[TOT_MEASURED_LOWER_THAN_EXPECTED]  = TOT_MEASURED_LOWER_THAN_EXPECTED;
																			end
										end
										// Long ToA acquisition mode
										else if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1 ) begin
																			// Overflow conditions: at the ends of the range, we can't tell if when the value we're measuring differs from the expected value 
																			// it's because it's within 1 count from the expected value (overflow) or if there is an actual error and it's further than one count.
																			// For instance, if expected toa is 8'hFF and measured toa is 8'h00, does it mean that they are 1 count or 8'hFF counts away?
																			if      ( superpixel_data_expected.longtoa == 13'h1FFF && superpixel_data_measured.longtoa != 13'h1FFF && superpixel_data_measured.longtoa != 13'h1FFE ) compare_two_superpixeldata[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_expected.longtoa == 13'h0000 && superpixel_data_measured.longtoa != 13'h0000 && superpixel_data_measured.longtoa != 13'h0001 ) compare_two_superpixeldata[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			// The rest of situations
																			else if ( superpixel_data_expected.longtoa != 13'h1FFF && superpixel_data_expected.longtoa != 13'h0000 ) begin
										         									if      ( superpixel_data_measured.longtoa > superpixel_data_expected.longtoa + 1 ) compare_two_superpixeldata[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																												else if ( superpixel_data_measured.longtoa < superpixel_data_expected.longtoa - 1 ) compare_two_superpixeldata[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			end
										end
						    // Photon counting acquisition mode
										else if ( bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1 ) begin
										         //$display("ph count in!");
										         if      ( superpixel_data_measured.photoncount > superpixel_data_expected.photoncount ) compare_two_superpixeldata[PHCOUNT_MEASURED_HIGHER_THAN_EXPECTED] = PHCOUNT_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_measured.photoncount < superpixel_data_expected.photoncount ) compare_two_superpixeldata[PHCOUNT_MEASURED_LOWER_THAN_EXPECTED]  = PHCOUNT_MEASURED_LOWER_THAN_EXPECTED;
										end
										*/
										
										
										// TAKING 2 COUNTS AS ERROR MARGIN (if ToT or ToA are more than 2 counts away from the expected value, it's considered as an error)
										// We consider ToT, ToA value comparison successful if the measured value is within +/- 1 count distance from the expected value
	         // Nominal acquisition mode
										if      ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) begin
										         //$display("nominal in!");
																			// Overflow conditions: at the ends of the range, we can't tell if when the value we're measuring differs from the expected value 
																			// it's because it's within 1 count from the expected value (overflow) or if there is an actual error and it's further than one count.
																			// For instance, if expected toa is 8'hFF and measured toa is 8'h00, does it mean that they are 1 count or 8'hFF counts away?
																			if      ( superpixel_data_expected.toa == 8'hFF && superpixel_data_measured.toa != 8'hFF && superpixel_data_measured.toa != 8'hFE && superpixel_data_measured.toa != 8'hFD ) compare_two_superpixeldata[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_expected.toa == 8'h00 && superpixel_data_measured.toa != 8'h00 && superpixel_data_measured.toa != 8'h01 && superpixel_data_measured.toa != 8'h02 ) compare_two_superpixeldata[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			// The rest of situations
																			else if ( superpixel_data_expected.toa != 8'hFF && superpixel_data_expected.toa != 8'h00 ) begin
										         									if      ( superpixel_data_measured.toa > superpixel_data_expected.toa + 2 ) compare_two_superpixeldata[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																												else if ( superpixel_data_measured.toa < superpixel_data_expected.toa - 2 ) compare_two_superpixeldata[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			end
																			
																			// Overflow conditions
																			if      ( superpixel_data_expected.tot == 5'h1F && superpixel_data_measured.tot != 5'h1F && superpixel_data_measured.tot != 5'h1E && superpixel_data_measured.tot != 5'h1D ) compare_two_superpixeldata[TOT_MEASURED_HIGHER_THAN_EXPECTED] = TOT_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_expected.tot == 5'h00 && superpixel_data_measured.tot != 5'h00 && superpixel_data_measured.tot != 5'h01 && superpixel_data_measured.tot != 5'h02) compare_two_superpixeldata[TOT_MEASURED_LOWER_THAN_EXPECTED]  = TOT_MEASURED_LOWER_THAN_EXPECTED;
																			// The rest of situations
																			else if ( superpixel_data_expected.tot != 5'h1F && superpixel_data_expected.tot != 5'h00 ) begin
																		 									if      ( superpixel_data_measured.tot > superpixel_data_expected.tot + 2 ) compare_two_superpixeldata[TOT_MEASURED_HIGHER_THAN_EXPECTED] = TOT_MEASURED_HIGHER_THAN_EXPECTED;
																												else if ( superpixel_data_measured.tot < superpixel_data_expected.tot - 2 ) compare_two_superpixeldata[TOT_MEASURED_LOWER_THAN_EXPECTED]  = TOT_MEASURED_LOWER_THAN_EXPECTED;
																			end
										end
										// Long ToA acquisition mode
										else if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1 ) begin
																			// Overflow conditions: at the ends of the range, we can't tell if when the value we're measuring differs from the expected value 
																			// it's because it's within 1 count from the expected value (overflow) or if there is an actual error and it's further than one count.
																			// For instance, if expected toa is 8'hFF and measured toa is 8'h00, does it mean that they are 1 count or 8'hFF counts away?
																			if      ( superpixel_data_expected.longtoa == 13'h1FFF && superpixel_data_measured.longtoa != 13'h1FFF && superpixel_data_measured.longtoa != 13'h1FFE && superpixel_data_measured.longtoa != 13'h1FFD ) 
																			        compare_two_superpixeldata[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_expected.longtoa == 13'h0000 && superpixel_data_measured.longtoa != 13'h0000 && superpixel_data_measured.longtoa != 13'h0001 && superpixel_data_measured.longtoa != 13'h0002 ) 
																			        compare_two_superpixeldata[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			// The rest of situations
																			else if ( superpixel_data_expected.longtoa != 13'h1FFF && superpixel_data_expected.longtoa != 13'h0000 ) begin
										         									if      ( superpixel_data_measured.longtoa > superpixel_data_expected.longtoa + 2 ) compare_two_superpixeldata[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																												else if ( superpixel_data_measured.longtoa < superpixel_data_expected.longtoa - 2 ) compare_two_superpixeldata[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			end
										end
						    // Photon counting acquisition mode
										else if ( bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1 ) begin
										         //$display("ph count in!");
										         if      ( superpixel_data_measured.photoncount > superpixel_data_expected.photoncount ) compare_two_superpixeldata[PHCOUNT_MEASURED_HIGHER_THAN_EXPECTED] = PHCOUNT_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_measured.photoncount < superpixel_data_expected.photoncount ) compare_two_superpixeldata[PHCOUNT_MEASURED_LOWER_THAN_EXPECTED]  = PHCOUNT_MEASURED_LOWER_THAN_EXPECTED;
										end
										
										
										// If no error has been detected
										if ( compare_two_superpixeldata.num() == 0 ) compare_two_superpixeldata[ALL_DATA_EQUAL] = ALL_DATA_EQUAL;
	endfunction: compare_two_superpixeldata
	
	
	
	function array_exitcode_type compare_two_superpixeldata_for_coverage (
	         input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
										      Superpixel_data_struct superpixel_data_measured,
																Superpixel_data_struct superpixel_data_expected,
												    FieldsTimeMeasurements_for_coverage time_measurements_obj_for_coverage
	);
	         logic [`PIXELS_IN_ONE_SUPERPIXEL-1:0] aux_xor_hitmaps;
										int aux_sum_auxxorhitmaps;
										
	         compare_two_superpixeldata_for_coverage.delete(); // this is done to avoid that errors are accumulated from one call to this function to the next call
										
	         if ( superpixel_data_measured.hit_flag          != superpixel_data_expected.hit_flag          ) compare_two_superpixeldata_for_coverage[HITFLAG_MEASURED_DIFFERENT_FROM_EXPECTED]           = HITFLAG_MEASURED_DIFFERENT_FROM_EXPECTED;
										
										// When is the hit map considered correct?
										// If XOR(expected hit map, measured hit map):
										// - has no '1', it means both are exactly the same, so the comparison yields positive result
										// - there is one '1' and the position of this '1' occurs at the discriminator bit where some pulse is applied and tot_ns <= `DELAY_FACTOR_TOT, it's OK, it could be that the pulse was so short that the FF that provides
										//   that hit_map bit didn't fire 
										// - there are two '1' and their positions correspond to the discriminator bits where some pulse is applied and tot_ns <= `DELAY_FACTOR_TOT, it's OK, it could be that the pulse was so short that the FF that provides
										//   those hit_map bits didn't fire 
										// - there are more than two '1', then it means the chip has detected more discriminator bits hit as expected, so it's wrong 
										//aux_xor_hitmaps = superpixel_data_measured.hit_map ^ superpixel_data_expected.hit_map;
										//aux_sum_auxxorhitmaps = 0; for (int i = 0; i < `PIXELS_IN_ONE_SUPERPIXEL; i++)  aux_sum_auxxorhitmaps += aux_xor_hitmaps[i];
										//if ( (aux_sum_auxxorhitmaps == 1 || aux_sum_auxxorhitmaps == 2 ) && time_measurements_obj_for_coverage.tot_ns > `DELAY_FACTOR_TOT || aux_sum_auxxorhitmaps > 2 ) compare_two_superpixeldata_for_coverage[HITMAP_MEASURED_DIFFERENT_FROM_EXPECTED]  = HITMAP_MEASURED_DIFFERENT_FROM_EXPECTED;
										if ( superpixel_data_measured.hit_map           != superpixel_data_expected.hit_map           ) compare_two_superpixeldata_for_coverage[HITMAP_MEASURED_DIFFERENT_FROM_EXPECTED]            = HITMAP_MEASURED_DIFFERENT_FROM_EXPECTED;
										
										
										
										if ( superpixel_data_measured.superpixel_segment != superpixel_data_expected.superpixel_segment || superpixel_data_measured.superpixel_in_segment != superpixel_data_expected.superpixel_in_segment )
										    compare_two_superpixeldata_for_coverage[SUPERPIXEL_NUMBER_MEASURED_DIFFERENT_FROM_EXPECTED] = SUPERPIXEL_NUMBER_MEASURED_DIFFERENT_FROM_EXPECTED;
														
										
										
										// TAKING 2 COUNTS AS ERROR MARGIN (if ToT or ToA are more than 2 counts away from the expected value, it's considered as an error)
										// We consider ToT, ToA value comparison successful if the measured value is within +/- 1 count distance from the expected value
	         // Nominal acquisition mode
										if      ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b0 ) begin
										         //$display("nominal in!");
																			// Overflow conditions: at the ends of the range, we can't tell if when the value we're measuring differs from the expected value 
																			// it's because it's within 1 count from the expected value (overflow) or if there is an actual error and it's further than one count.
																			// For instance, if expected toa is 8'hFF and measured toa is 8'h00, does it mean that they are 1 count or 8'hFF counts away?
																			if      ( superpixel_data_expected.toa == 8'hFF && superpixel_data_measured.toa != 8'hFF && superpixel_data_measured.toa != 8'hFE && superpixel_data_measured.toa != 8'hFD ) compare_two_superpixeldata_for_coverage[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_expected.toa == 8'h00 && superpixel_data_measured.toa != 8'h00 && superpixel_data_measured.toa != 8'h01 && superpixel_data_measured.toa != 8'h02 ) compare_two_superpixeldata_for_coverage[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			// The rest of situations
																			else if ( superpixel_data_expected.toa != 8'hFF && superpixel_data_expected.toa != 8'h00 ) begin
										         									if      ( superpixel_data_measured.toa > superpixel_data_expected.toa + 2 ) compare_two_superpixeldata_for_coverage[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																												else if ( superpixel_data_measured.toa < superpixel_data_expected.toa - 2 ) compare_two_superpixeldata_for_coverage[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			end
																			
																			// Overflow conditions
																			if      ( superpixel_data_expected.tot == 5'h1F && superpixel_data_measured.tot != 5'h1F && superpixel_data_measured.tot != 5'h1E && superpixel_data_measured.tot != 5'h1D ) compare_two_superpixeldata_for_coverage[TOT_MEASURED_HIGHER_THAN_EXPECTED] = TOT_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_expected.tot == 5'h00 && superpixel_data_measured.tot != 5'h00 && superpixel_data_measured.tot != 5'h01 && superpixel_data_measured.tot != 5'h02) compare_two_superpixeldata_for_coverage[TOT_MEASURED_LOWER_THAN_EXPECTED]  = TOT_MEASURED_LOWER_THAN_EXPECTED;
																			// The rest of situations
																			else if ( superpixel_data_expected.tot != 5'h1F && superpixel_data_expected.tot != 5'h00 ) begin
																		 									if      ( superpixel_data_measured.tot > superpixel_data_expected.tot + 2 ) compare_two_superpixeldata_for_coverage[TOT_MEASURED_HIGHER_THAN_EXPECTED] = TOT_MEASURED_HIGHER_THAN_EXPECTED;
																												else if ( superpixel_data_measured.tot < superpixel_data_expected.tot - 2 ) compare_two_superpixeldata_for_coverage[TOT_MEASURED_LOWER_THAN_EXPECTED]  = TOT_MEASURED_LOWER_THAN_EXPECTED;
																			end
										end
										// Long ToA acquisition mode
										else if ( bits_slowcontrol_reg_obj.photonCnt == 1'b0 && bits_slowcontrol_reg_obj.longCnt == 1'b1 ) begin
																			// Overflow conditions: at the ends of the range, we can't tell if when the value we're measuring differs from the expected value 
																			// it's because it's within 1 count from the expected value (overflow) or if there is an actual error and it's further than one count.
																			// For instance, if expected toa is 8'hFF and measured toa is 8'h00, does it mean that they are 1 count or 8'hFF counts away?
																			if      ( superpixel_data_expected.longtoa == 13'h1FFF && superpixel_data_measured.longtoa != 13'h1FFF && superpixel_data_measured.longtoa != 13'h1FFE && superpixel_data_measured.longtoa != 13'h1FFD ) 
																			        compare_two_superpixeldata_for_coverage[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_expected.longtoa == 13'h0000 && superpixel_data_measured.longtoa != 13'h0000 && superpixel_data_measured.longtoa != 13'h0001 && superpixel_data_measured.longtoa != 13'h0002 ) 
																			        compare_two_superpixeldata_for_coverage[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			// The rest of situations
																			else if ( superpixel_data_expected.longtoa != 13'h1FFF && superpixel_data_expected.longtoa != 13'h0000 ) begin
										         									if      ( superpixel_data_measured.longtoa > superpixel_data_expected.longtoa + 2 ) compare_two_superpixeldata_for_coverage[TOA_MEASURED_HIGHER_THAN_EXPECTED] = TOA_MEASURED_HIGHER_THAN_EXPECTED;
																												else if ( superpixel_data_measured.longtoa < superpixel_data_expected.longtoa - 2 ) compare_two_superpixeldata_for_coverage[TOA_MEASURED_LOWER_THAN_EXPECTED]  = TOA_MEASURED_LOWER_THAN_EXPECTED;
																			end
										end
						    // Photon counting acquisition mode
										else if ( bits_slowcontrol_reg_obj.photonCnt == 1'b1 && bits_slowcontrol_reg_obj.longCnt == 1'b1 ) begin
										         //$display("ph count in!");
										         if      ( superpixel_data_measured.photoncount > superpixel_data_expected.photoncount ) compare_two_superpixeldata_for_coverage[PHCOUNT_MEASURED_HIGHER_THAN_EXPECTED] = PHCOUNT_MEASURED_HIGHER_THAN_EXPECTED;
																			else if ( superpixel_data_measured.photoncount < superpixel_data_expected.photoncount ) compare_two_superpixeldata_for_coverage[PHCOUNT_MEASURED_LOWER_THAN_EXPECTED]  = PHCOUNT_MEASURED_LOWER_THAN_EXPECTED;
										end
										
										
										// If no error has been detected
										if ( compare_two_superpixeldata_for_coverage.num() == 0 ) compare_two_superpixeldata_for_coverage[ALL_DATA_EQUAL] = ALL_DATA_EQUAL;
	endfunction: compare_two_superpixeldata_for_coverage
	
	
	
	
	function array_exitcode_fullcolumn_type compare_two_columndata_broken_into_superpixeldata (
	         input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
										      Acquired_word_struct superpixel_data_measured,
																Acquired_word_struct superpixel_data_expected
	);
	array_exitcode_type aux;
	compare_two_columndata_broken_into_superpixeldata.delete();
	
	
	         for ( int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++ ) begin
										     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																				aux.delete();
																				aux = compare_two_superpixeldata ( .bits_slowcontrol_reg_obj(bits_slowcontrol_reg_obj), .superpixel_data_measured(superpixel_data_measured.superpixel[ss][sis]), 
																				                                   .superpixel_data_expected(superpixel_data_expected.superpixel[ss][sis]) );
																				//foreach (aux[j]) begin  $display("acqdata - comparison - superpixel %d - %s",i,aux[j]); end
																				compare_two_columndata_broken_into_superpixeldata[ss][sis] = aux;
																				//$display("num compare_two_columndata_broken_into_superpixeldata[%d] = %d",i,compare_two_columndata_broken_into_superpixeldata[i].num());
															end
										end
		        //$display("num compare_two_columndata_broken_into_superpixeldata = %d",compare_two_columndata_broken_into_superpixeldata.num());
	endfunction:compare_two_columndata_broken_into_superpixeldata 
	
	
		function array_exitcode_fullcolumn_type compare_two_columndata_broken_into_superpixeldata_for_coverage (
	         input BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj,
										      Acquired_word_struct superpixel_data_measured,
																Acquired_word_struct superpixel_data_expected,
																FieldsTimeMeasurements_for_coverage time_measurements_obj_for_coverage
	);
	array_exitcode_type aux;
	compare_two_columndata_broken_into_superpixeldata_for_coverage.delete();
	
	
	         for ( int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++ ) begin
										     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																				aux.delete();
																				aux = compare_two_superpixeldata_for_coverage ( .bits_slowcontrol_reg_obj(bits_slowcontrol_reg_obj), .superpixel_data_measured(superpixel_data_measured.superpixel[ss][sis]), 
																				                                   .superpixel_data_expected(superpixel_data_expected.superpixel[ss][sis]), .time_measurements_obj_for_coverage(time_measurements_obj_for_coverage) );
																				//foreach (aux[j]) begin  $display("acqdata - comparison - superpixel %d - %s",i,aux[j]); end
																				compare_two_columndata_broken_into_superpixeldata_for_coverage[ss][sis] = aux;
																				//$display("num compare_two_columndata_broken_into_superpixeldata[%d] = %d",i,compare_two_columndata_broken_into_superpixeldata[i].num());
															end
										end
		        //$display("num compare_two_columndata_broken_into_superpixeldata = %d",compare_two_columndata_broken_into_superpixeldata.num());
	endfunction:compare_two_columndata_broken_into_superpixeldata_for_coverage 
	
	


 function void copy_two_columndata_broken_into_superpixeldata (
										      input Acquired_word_struct superpixel_data_original,
																output Acquired_word_struct superpixel_data_copied
	);
	         for ( int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++ ) begin
										     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
										     					superpixel_data_copied.superpixel[ss][sis].hit_map           = superpixel_data_original.superpixel[ss][sis].hit_map;
	  																	superpixel_data_copied.superpixel[ss][sis].toa               = superpixel_data_original.superpixel[ss][sis].toa;
																				superpixel_data_copied.superpixel[ss][sis].tot               = superpixel_data_original.superpixel[ss][sis].tot;
																				superpixel_data_copied.superpixel[ss][sis].longtoa           = superpixel_data_original.superpixel[ss][sis].longtoa;
																				superpixel_data_copied.superpixel[ss][sis].photoncount       = superpixel_data_original.superpixel[ss][sis].photoncount;
																				superpixel_data_copied.superpixel[ss][sis].hit_flag          = superpixel_data_original.superpixel[ss][sis].hit_flag;
																				superpixel_data_copied.superpixel[ss][sis].superpixel_segment = superpixel_data_original.superpixel[ss][sis].superpixel_segment;
																				superpixel_data_copied.superpixel[ss][sis].superpixel_in_segment = superpixel_data_original.superpixel[ss][sis].superpixel_in_segment;
															end
										end
	endfunction: copy_two_columndata_broken_into_superpixeldata

  
