// Filename           : CLICTD_dataout_model.sv
// Author             : Núria Egidos 
// Created on         : 21/6/18
// Last modification  : 9/11/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Golden model or predictor corresponding to the value of the different signals in dataout_interface

`ifndef __CLICTD_dataout_model_sv__
`define __CLICTD_dataout_model_sv__

// This structure will enable having more than one export in the same component; 
// when the respective anlaysis ports call write(), the factory will automatically invoke 
// write_matrix() or write_in(), depending on which of the exports is called
//`uvm_analysis_imp_decl(_matrix)
//`uvm_analysis_imp_decl(_in)

class CLICTD_dataout_model extends uvm_component; 

	`uvm_component_utils(CLICTD_dataout_model) // Register the component to the factory

  // Declare the analysis port to provide the "ideal outputs" to the comparator
	uvm_analysis_port#(CLICTD_dataout_transaction) analysis_port; 

  // Export to retrieve transactions from the dataout_agent; the content of these transactions
  // is not used, but their arrival is used for synchronisation purposes. When one of these 
  // transactions arrives (when reset is released, every time a column content is read out...),
  // a transaction containing the expected value of DATA_OUT in that occasion is sent to   
  // dataout_comparator
  //uvm_analysis_export#(CLICTD_dataout_transaction) analysis_export_in;
  // The component that implements the write function uses a uvm_analysis_imp (subscriber, this model...); the component that 
  // connects an export in the parent component (scoreboard...) to the export that implements the write function uses a uvm_analysis_export
  uvm_analysis_imp_in#(CLICTD_dataout_transaction, CLICTD_dataout_model) analysis_export_in;

  // Export to retrieve transactions from matrix_model. These transactions contain the expected
  // value for DATA_OUT in STATUS_CONFIG_READOUT_FIRST/SECOND_HALF...
  uvm_analysis_export#(CLICTD_dataout_transaction) analysis_export_matrix;
  // The component that implements the write function uses a uvm_analysis_imp (subscriber, this model...); the component that 
  // connects an export in the parent component (scoreboard...) to the export that implements the write function uses a uvm_analysis_export
  //uvm_analysis_imp_matrix#(CLICTD_dataout_transaction, CLICTD_dataout_model) analysis_export_matrix;


  // FIFO used to collect the stream of transactions coming from matrix_model. These transactions
  // will contain the column_content corresponding to each column of the matrix, for chip_status
  // STATUS_MATRIX_CONFIG_FIRST/SECOND_HALF, STATUS_CONFIG_READOUT_FIRST/SECOND_HALF, 
  // STATUS_FORCE_HIT_FROM_SLOWCONTROL/EXTERNAL, STATUS_MATRIX_READOUT_ACQUIRED_FROM_SLOWCONTROL/EXTERNAL
  uvm_tlm_analysis_fifo #(CLICTD_dataout_transaction) matrix_fifo;
  // # UVM_ERROR @ 0: uvm_test_top.env.dataout_scoreboard.dataout_model.analysis_export_matrix [Connection Error] Cannot call an imp port's connect method. An imp is connected only to the component passed in its constructor. (You attempted to bind this imp to uvm_test_top.env.dataout_scoreboard.dataout_model.matrix_fifo.analysis_export)
  // -> alternative: turn dataout_model into a subscriber, use its analysis_export for the fifo and declare an
  // analysis_imp for the connection to dataout_agent


  ChipStatusInfo chip_status_obj;
		FieldsConfigureMatrix config_matrix_obj;
		FieldsTimeMeasurements time_measurements_obj;
		FieldsConfigureMatrix_for_coverage config_matrix_obj_for_coverage;
		FieldsTimeMeasurements_for_coverage time_measurements_obj_for_coverage;
		BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;

  uvm_event get_transaction_from_matrix_event;

  logic [`ROWS*`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED-1:0] aux_vector;

  int column_counter = 0;
  logic auxiliary_dataout_bit;
		
		string testname;
		
  // - - - - - - - - - - - - Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

  // - - - - - - - - - - - - Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
    // Create the analysis port and exports
		analysis_port = new(.name("analysis_port"), .parent(this));
    //analysis_export_in = new(.name("analysis_export_in"), .parent(this));
    //analysis_export_matrix = new(.name("analysis_export_matrix"), .parent(this));
    analysis_export_in = new(.name("analysis_export_in"), .imp(this));
    //analysis_export_matrix = new(.name("analysis_export_matrix"), .imp(this));
    analysis_export_matrix = new(.name("analysis_export_matrix"), .parent(this));


    // Create the FIFO that will store the transactions coming from matrix_model. The first transaction
    // corresponds to column 0, the second to column 1, and so on; the first transaction will be the first
    // to be retrieved in the write_in method below, when a transaction from dataout_agent arrives,
    // so this performance provides the required ordering of transactions
  	matrix_fifo = new("matrix_fifo", this);

    // Retrieve the chip status from the database; it is set (and changed as the test progresses) from the virtual sequence 
    // in its body() task, so by this point it should be available to be retrieved
    if( !uvm_config_db#(ChipStatusInfo)::get(this, "", "chip_status_obj", chip_status_obj) )
       `uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )
							
				if( !uvm_config_db#(FieldsConfigureMatrix)::get(this, "", "config_matrix_obj", config_matrix_obj) )
       `uvm_error( `NOMATRIXCONFIG, $sformatf("config_matrix_obj not found in database for %s", this.get_full_name()) )
							
				if( !uvm_config_db#(FieldsTimeMeasurements)::get(this, "", "time_measurements_obj", time_measurements_obj) )
       `uvm_error( `NOTIMEMEASUREMENTS, $sformatf("time_measurements_obj not found in database for %s", this.get_full_name()) )
				
				if( !uvm_config_db#(BitsMonitorSlowcontrolReg)::get(this, "", "bits_slowcontrol_reg_obj", bits_slowcontrol_reg_obj) )
       `uvm_error( `NOBITSSLOWCONTROLREG, $sformatf("bits_slowcontrol_reg_obj not found in database for %s", this.get_full_name()) )

    if( !uvm_config_db#(string)::get(this, "", "testname", testname) )
       `uvm_error( `NOTESTNAME, $sformatf("testname not found in database for %s", this.get_full_name()) )
							
							
							
				if (testname == `READOUT_TEST_FOR_COVERAGE) begin
				    if( !uvm_config_db#(FieldsConfigureMatrix_for_coverage)::get(this, "", "config_matrix_obj_for_coverage", config_matrix_obj_for_coverage) )
       `uvm_error( `NOMATRIXCONFIG, $sformatf("config_matrix_obj_for_coverage not found in database for %s", this.get_full_name()) )
							
								if( !uvm_config_db#(FieldsTimeMeasurements_for_coverage)::get(this, "", "time_measurements_obj_for_coverage", time_measurements_obj_for_coverage) )
       				`uvm_error( `NOTIMEMEASUREMENTS, $sformatf("time_measurements_obj_for_coverage not found in database for %s", this.get_full_name()) )
				end

    get_transaction_from_matrix_event = new("get_transaction_from_matrix_event");

	endfunction: build_phase


  // - - - - - - - - - - - - Connect phase
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
    analysis_export_matrix.connect(matrix_fifo.analysis_export);

	endfunction: connect_phase

  virtual task run_phase(	uvm_phase 	phase	);
     CLICTD_dataout_transaction auxiliary_dataout_transaction2, auxiliary_get_dataout_transaction;
     
     fork
         forever begin
            
            get_transaction_from_matrix_event.wait_trigger();
												auxiliary_get_dataout_transaction = CLICTD_dataout_transaction::type_id::create(.name("auxiliary_get_dataout_transaction"), .contxt(get_full_name()));
            matrix_fifo.get(auxiliary_get_dataout_transaction); // this provides the expected data_out value
            assert($cast(auxiliary_dataout_transaction2,auxiliary_get_dataout_transaction.clone()));
												/*
												if (chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF) begin
													    for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																	    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																					    $display("DISPLAY - dataout model - tuningDAC1[%0d][%0d][%0d][2] = %b, tuningDAC1[%0d][%0d][%0d][1] = %b",
																																		auxiliary_dataout_transaction2.column_counter,ss,sis,auxiliary_dataout_transaction2.column_config_breakdown.superpixel[ss][sis].tuningDAC1[2],
																																		auxiliary_dataout_transaction2.column_counter,ss,sis,auxiliary_dataout_transaction2.column_config_breakdown.superpixel[ss][sis].tuningDAC1[1]);
																					end
																	end
										  end
										  */
												assert_isunknown_dataout_transaction ( .dataout_transaction(auxiliary_dataout_transaction2), .chip_status_obj(chip_status_obj), .name_of_transaction("auxiliary_dataout_transaction2"), .name_of_calling_component(this.get_full_name()) );
            // why do we clone the transactions we obtain from the FIFO? This is a recommended practice because when we retrieve a transaction, we are in fact
            // pointing to its reference, to where it was created. If we modify the extracted transaction, we would also modify the originary one, which we may
            // not want to do. To avoid this issue, we clone it into a new transaction and operate on the clone. 
            for (int i = 0; i < auxiliary_dataout_transaction2.column_content.size(); i++) begin aux_vector[i] = auxiliary_dataout_transaction2.column_content[i]; end
            `uvm_info("Readout progress", $sformatf("Transaction sent to dataout_comparator - run: column_counter = %d, chip_status = %s, column_content = %h", auxiliary_dataout_transaction2.column_counter, chip_status_obj.chip_status, aux_vector),  UVM_LOW);
            // The value of dataout_bit provided by matrix_model may not reflect the current value of this signal,
												// because it's not provided by the golden model. Since it only matters in STATUS_RESET, for the rest of states we can 
												// simply adopt the value of the incoming transaction to avoid comparison mistakes
             auxiliary_dataout_transaction2.dataout_bit = auxiliary_dataout_bit;
             analysis_port.write(auxiliary_dataout_transaction2);
         end
     join_none

  endtask: run_phase


  // - - - - - - - - - - - - Write function for analysis_export_in
  // This write method is called when the dataout_agent generates a transaction; the content of such
  // transaction is not used, but when it arrives, dataout_model generates a transaction to send to 
  // dataout_comparator
	function void write_in(CLICTD_dataout_transaction t); // t is used to match the declaration of pure virtual function write in uvm_subscriber

     
     int aux_i;
     CLICTD_dataout_transaction auxiliary_dataout_transaction;
     if (chip_status_obj.chip_status != STATUS_RESET) begin
						`uvm_info("Readout progress", $sformatf("Transaction from dataout_agent: column_counter = %d, chip_status = %s", t.column_counter, chip_status_obj.chip_status),  UVM_LOW);
     end
   	 
		   if      (chip_status_obj.chip_status == STATUS_RESET) begin: status_reset
					         assert( $cast(auxiliary_dataout_transaction, t.clone()) );
		            auxiliary_dataout_transaction.dataout_bit = 1'b0;
             //auxiliary_dataout_transaction.timeout_alarm = 1'b0;
             analysis_port.write(auxiliary_dataout_transaction); // send the predicted values to the scoreboard
     end: status_reset
     else if (chip_status_obj.chip_status == STATUS_SECOND_READOUT_ALL_ZEROS || chip_status_obj.chip_status == STATUS_FOURTH_READOUT_ALL_ZEROS) begin: status_readout_zeros
					        assert( $cast(auxiliary_dataout_transaction, t.clone()) );
													
             //auxiliary_dataout_transaction.dataout_bit = t.dataout_bit; // this value is not used in this state, and like this we avoid miscopare error
             auxiliary_dataout_transaction.column_counter = column_counter;
		           //auxiliary_dataout_transaction.column_header = {10'b1011000101,column_counter[3:0],8'b0};
													case (column_counter[3:0])
													      0: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_0;
																			1: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_1;
																			2: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_2;
																			3: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_3;
																			4: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_4;
																			5: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_5;
																			6: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_6;
																			7: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_7;
																			8: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_8;
																			9: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_9;
																			10: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_10;
																			11: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_11;
																			12: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_12;
																			13: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_13;
																			14: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_14;
																			15: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_15;
													endcase
             
             //int i = `ROWS*`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED-1;
             //while (i>=0) begin
             //      
             //      i = i - `NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED;
             //end
             
		           for (int i = 0; i < `ROWS*`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED; i++) begin
                aux_i = `ROWS*`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED - 1 - i;
                if (i%(`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED) == 0) auxiliary_dataout_transaction.column_content[aux_i] = 1'b1; // hit flag
                else auxiliary_dataout_transaction.column_content[aux_i] = 1'b0;
								
                aux_vector[aux_i] = auxiliary_dataout_transaction.column_content[aux_i]; 
             end
             `uvm_info("Readout progress", $sformatf("Transaction sent to dataout_comparator - write_in: column_counter = %d, chip_status = %s, column_content = %h", auxiliary_dataout_transaction.column_counter, chip_status_obj.chip_status, aux_vector),  UVM_LOW);
		         analysis_port.write(auxiliary_dataout_transaction); // send the predicted values to the scoreboard

             if (column_counter < `COLUMNS - 1) column_counter = column_counter + 1;
             else column_counter = 0;

		 end: status_readout_zeros 
			
			
			// In this state, the word read out shoudl contain the acquired hit pattern 
			else if (chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0 || 
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0 ||
												chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0 ||
												chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS ||
												chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS )  begin: status_readout_acquired
			         //auxiliary_dataout_transaction.copy(t);
												assert( $cast(auxiliary_dataout_transaction, t.clone()) );
			         // According to the expected acquisition mode and matrix configuration, obtain the expected ToT/ToA/photon counting values for the present column
												if (testname == `READOUT_TEST_FOR_COVERAGE) build_expected_measurements_from_matrixconfig_obj_for_coverage (.column_number(t.column_counter), .bits_slowcontrol_reg_obj(bits_slowcontrol_reg_obj),.config_matrix_obj(config_matrix_obj_for_coverage), 
												                                            .time_measurements_obj(time_measurements_obj_for_coverage), .testname(testname),
												                                                   .column_content_breakdown(auxiliary_dataout_transaction.column_content_breakdown) );
												else                                       	build_expected_measurements_from_matrixconfig_obj (.column_number(t.column_counter), .bits_slowcontrol_reg_obj(bits_slowcontrol_reg_obj),.config_matrix_obj(config_matrix_obj), 
												                                            .time_measurements_obj(time_measurements_obj), .testname(testname), .column_content_breakdown(auxiliary_dataout_transaction.column_content_breakdown) );
											 
																																																															
												
												auxiliary_dataout_transaction.column_content.delete();
												build_expected_columncontent_from_columncontentbreakdown ( .bits_slowcontrol_reg_obj(bits_slowcontrol_reg_obj), .column_content_breakdown(auxiliary_dataout_transaction.column_content_breakdown), .column_content(auxiliary_dataout_transaction.column_content)	);
												
												analysis_port.write(auxiliary_dataout_transaction); // send the predicted values to the scoreboard
												
												
			        
			end: status_readout_acquired
		 // one transaction per column will be sent to the dataout_comparator
		 else begin
			        //assert( $cast(auxiliary_dataout_transaction, t.clone()) );
             auxiliary_dataout_bit = t.dataout_bit;
             //$display("%t trigger get_transaction_from_matrix_event", $time);
		         get_transaction_from_matrix_event.trigger();

     end        
             
		         
		         // CANNOT CALL TASK MATRIX_FIFO.GET HERE BECAUSE IT IMPLIES DELAY, AND WRITE_IN IS A FUNCTION!
		         //  ** Error: (vsim-8804) ../environment//CLICTD_dataout_model.sv(98): Illegal to call task "get" in function "write_in" as it has delay or DPI task call.
		 // for other chip_status, auxiliary_datout_transaction is retrieved from the FIFO that stores the transactions
		 // coming from matrix_model  
		         
	endfunction: write_in


  // - - - - - - - - - - - - Write function for analysis_export (connected to matrix_model)
  // This write method is called when the dataout_agent generates a transaction; the content of such
  // transaction is not used, but when it arrives, dataout_model generates a transaction to send to 
  // dataout_comparator
	//function void write_matrix(CLICTD_dataout_transaction t); // t is used to match the declaration of pure virtual function write in uvm_subscriber
  // 
  // $display("in write_matrix");
	//endfunction: write_matrix




endclass: CLICTD_dataout_model






`endif // __CLICTD_dataout_model_sv__
