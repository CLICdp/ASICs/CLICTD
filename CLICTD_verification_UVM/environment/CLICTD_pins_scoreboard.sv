// Filename           : CLICTD_pins_scoreboard.sv
// Author             : Núria Egidos 
// Created on         : 5/6/18
// Last modification  : 5/6/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Scoreboard

`ifndef __CLICTD_pins_scoreboard_sv__
`define __CLICTD_pins_scoreboard_sv__


//`uvm_analysis_imp_decl(_in)
//`uvm_analysis_imp_decl(_out)

//import uvm_pkg::*;
//`include <uvm_macros.svh>

class CLICTD_pins_scoreboard extends uvm_scoreboard;
	`uvm_component_utils(CLICTD_pins_scoreboard) // Register the component to the factory

  // Declaration of the exports used to connect to the analysis ports 
  // in the agent (which are then connected to the monitors):
  // for the probed inputs to reach the golden model
  uvm_analysis_export #(CLICTD_pins_transaction) analysis_export_in; 
  // for the probed outputs to reach the comparator
  uvm_analysis_export #(CLICTD_pins_transaction) analysis_export_out; 
  // Note that we declare pairs of exports, we will only create those required by the agents in the present test

  // Export for matrix_model
  uvm_analysis_export #(CLICTD_pins_transaction) analysis_export_matrix; 

	// Declare all possible golden models; in the build_phase we will
  // create those corresponding to the present test
  CLICTD_pins_model model;

  CLICTD_pins_comparator comparator;

 

  // Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
  endfunction: new


  // Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
    analysis_export_in = new("analysis_export_in", this);
    analysis_export_out = new("analysis_export_out", this);
    analysis_export_matrix = new("analysis_export_matrix", this);
    // Create the model
    model = CLICTD_pins_model::type_id::create("pins_model", this);
    // Create the comparator
    comparator = CLICTD_pins_comparator::type_id::create("pins_comparator", this);
	endfunction: build_phase


   // Connect phase: connect the analysis ports and exports
	function void connect_phase(uvm_phase phase);
    super.connect_phase(phase);
    // Connect the analysis export of the golden model to the analysis port of the agent
		// probing the input signals
		//analysis_export_in.connect(model.model_analysisexport);
    analysis_export_in.connect(model.analysis_export_in);
		// Connect the analysis port of the golden model (where the expected outputs are available)
		// to an analysis export of the comparator 
		model.analysis_port.connect(comparator.analysis_export_ideal);
		// Connect the analysis port of the agent (from the monitor probing the DUT outputs or internal signals)
		// to an analysis export of the comparator 
		analysis_export_out.connect(comparator.analysis_export_actual); 
    analysis_export_matrix.connect(model.analysis_export_matrix);
	endfunction: connect_phase




endclass: CLICTD_pins_scoreboard



`endif // __CLICTD_pins_scoreboard_sv__

