// Filename           : CLICTD_regslowcontrol_monitor.sv
// Author             : Núria Egidos 
// Created on         : 22/8/18
// Last modification  : 27/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Monitor to probe the signals in slowcontrol_interface

`ifndef __CLICTD_regslowcontrol_monitor_sv__
`define __CLICTD_regslowcontrol_monitor_sv__

class CLICTD_regslowcontrol_monitor extends uvm_monitor;
	`uvm_component_utils(CLICTD_regslowcontrol_monitor) // Register the component to the factory


	// Declare the analysis port that will be connected to the agent's
  // analysis port and this, in turn, to the scoreboard's
  uvm_analysis_port#(CLICTD_slowcontrol_transaction) analysis_port; 

  

  // Instance of pins_interface on which we are going to retrieve
  // the virtual interface handle from the factory
  virtual CLICTD_slowcontrol_interface slowcontrol_vif;


  CLICTDevents events_obj;

  // - - - - - - - - - - - - Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

  // - - - - - - - - - - - - Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
    // Create the monitor analysis port
		analysis_port = new(.name("analysis_port"), .parent(this));

    if( !uvm_config_db#(virtual CLICTD_slowcontrol_interface)::get(this, "", "slowcontrol_if", slowcontrol_vif) )
       `uvm_error( `NOVIF_SLOWCONTROL, $sformatf("Slowcontrol virtual interface not found in database for %s", this.get_full_name()) )

    if( !uvm_config_db#(CLICTDevents)::get(this, "", "events_obj", events_obj) )
         `uvm_error( `NOCLICTDEVENTS, $sformatf("CLICTDevents not found in database for %s", this.get_full_name()) )

	endfunction: build_phase


  // - - - - - - - - - - - - Run phase: here take place the test actions
	virtual task run_phase(uvm_phase phase);

    fork
		  detect_read_slowcontrol_finished();
      detect_write_slowcontrol_finished();
      regslowcontrolmonitor_action_writeregfinished();
      regslowcontrolmonitor_action_readregfinished(); 
    join_none
    
    // The fork join_none is used to avoid that this forever begin blocks the execution of other 
    // run_phase tasks in the hierarchy
	endtask: run_phase

  // - - - - - - - - - - - - Other tasks
  // 
  task detect_read_slowcontrol_finished();
       forever begin @(posedge slowcontrol_vif.high_when_regreadout_finished) begin events_obj.read_slowcontrol_finished_event.trigger(); end end
  endtask: detect_read_slowcontrol_finished
  //
  task detect_write_slowcontrol_finished();
       forever begin @(posedge slowcontrol_vif.high_when_regwrite_finished) begin events_obj.write_slowcontrol_finished_event.trigger(); end end
  endtask: detect_write_slowcontrol_finished
  //
  function void collect_values_for_transaction(CLICTD_slowcontrol_transaction slowcontrol_trans);
       slowcontrol_trans.register_address = slowcontrol_vif.register_address_from_operation;
			 slowcontrol_trans.register_content = slowcontrol_vif.register_content_from_operation;
			 slowcontrol_trans.read_or_write = slowcontrol_vif.operation;
  endfunction: collect_values_for_transaction
  //
  // Generate a transaction when a register write operation is complete
  task regslowcontrolmonitor_action_writeregfinished();
       // Declare the transaction used to exchange information between the 
			 // monitor and the scoreboard
			 CLICTD_slowcontrol_transaction slowcontrol_transaction; 
       forever begin
							 events_obj.write_slowcontrol_finished_event.wait_trigger();
               slowcontrol_transaction = CLICTD_slowcontrol_transaction::type_id::create(.name("slowcontrol_transaction"), .contxt(get_full_name()));
							 collect_values_for_transaction(slowcontrol_transaction);
							 analysis_port.write(slowcontrol_transaction);
       end
  endtask: regslowcontrolmonitor_action_writeregfinished
  //
  // Generate a transaction when a register read operation is complete
  task regslowcontrolmonitor_action_readregfinished();
       // Declare the transaction used to exchange information between the 
			 // monitor and the scoreboard
			 CLICTD_slowcontrol_transaction slowcontrol_transaction; 
       forever begin
							 events_obj.read_slowcontrol_finished_event.wait_trigger();
               slowcontrol_transaction = CLICTD_slowcontrol_transaction::type_id::create(.name("slowcontrol_transaction"), .contxt(get_full_name()));
							 collect_values_for_transaction(slowcontrol_transaction);
							 analysis_port.write(slowcontrol_transaction);
       end
  endtask: regslowcontrolmonitor_action_readregfinished


endclass: CLICTD_regslowcontrol_monitor





`endif // __CLICTD_regslowcontrol_monitor_sv__

