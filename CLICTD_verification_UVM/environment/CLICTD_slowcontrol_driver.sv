// Filename           : CLICTD_slowcontrol_driver.sv
// Author             : Núria Egidos 
// Created on         : 26/7/18
// Last modification  : 26/7/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Driver 
// Derived from       : analysis_master_driver.sv by Elia Conti and Sara Marconi for VEPIX53
//                      https://github.com/cluelogic/uvm-tutorial-for-candy-lovers/blob/master/src/tutorial_9.sv

`ifndef __CLICTD_slowcontrol_driver_sv__
`define __CLICTD_slowcontrol_driver_sv__



class CLICTD_slowcontrol_driver extends uvm_driver#(CLICTD_slowcontrol_transaction);
     `uvm_component_utils(CLICTD_slowcontrol_driver) // Register the component to the factory
 
     ReportingEnable reporting_enable_obj;
					ChipStatusInfo chip_status_obj;

     // Instance of slowcontrol_interface on which we are going to retrieve
     // the virtual interface handle from the factory. This will be used
     // to interact with the slow control registers
     virtual CLICTD_slowcontrol_interface slowcontrol_vif; 
 
     // Constructor
     function new(string name, uvm_component parent);
          super.new(name, parent);
     endfunction: new
 
     // Build phase: build the hierarchy
     function void build_phase(uvm_phase phase);
          super.build_phase(phase);

          

          if( !uvm_config_db#(virtual CLICTD_slowcontrol_interface)::get(this, "", "slowcontrol_if", slowcontrol_vif) )
         `uvm_error( `NOVIF_SLOWCONTROL, $sformatf("Slowcontrol virtual interface not found in database for %s", this.get_full_name()) )
       
         if( !uvm_config_db#(ReportingEnable)::get(this, "", "reporting_enable_obj", reporting_enable_obj) )
         `uvm_error( `NOREPORTINGENABLE, $sformatf("ReportingEnable not found in database for %s", this.get_full_name()) )
									
									if( !uvm_config_db#(ChipStatusInfo)::get(this, "", "chip_status_obj", chip_status_obj) )
						   					`uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )

          //void'(uvm_resource_db#(virtual simpleadder_if)::read_by_name(.scope("ifs"), .name("simpleadder_if"), .val(vif)));
     endfunction: build_phase
 
     task run_phase(uvm_phase phase);
        
         
          CLICTD_slowcontrol_transaction sc_trans;
          logic [7:0] dummy;
          fork
				      forever begin
				            seq_item_port.get_next_item(sc_trans);
																assert_isunknown_slowcontrol_transaction ( .slowcontrol_transaction(sc_trans), .chip_status_obj(chip_status_obj), .name_of_transaction("sc_trans"), .name_of_calling_component(this.get_full_name()) );
                    if (sc_trans.read_or_write == `READ) begin 
												            #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_READ_SLOWCONTROL);
                        if (reporting_enable_obj.enable_reporting_slowcontroldriver == `YES) begin  
				                	     	`uvm_info("read slow control register", $sformatf("time = %12d, register address = %2h",$time, sc_trans.register_address), UVM_LOW);
																								end
																								slowcontrol_vif.read_register_slowcontrol(.register_address(sc_trans.register_address), .register_content(dummy));
																								#(`NUMBER_TIMEUNITS_WAIT_BETWEEN_READ_SLOWCONTROL);
                    end
                    else if (sc_trans.read_or_write == `WRITE) begin 
																								if (reporting_enable_obj.enable_reporting_slowcontroldriver == `YES) begin 
                        														`uvm_info("write slow control register", $sformatf("time = %12d, register address = %2h, register content = %2h",$time, sc_trans.register_address, sc_trans.register_content ), UVM_LOW);
																								end
                        slowcontrol_vif.write_register_slowcontrol(.register_address(sc_trans.register_address), .register_content(sc_trans.register_content) );
                        #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);
                    end
				           seq_item_port.item_done();
				      end
          join_none
        
        // The fork join_none is used to avoid that this forever begin blocks the execution of other 
        // run_phase tasks in the hierarchy
     endtask: run_phase


endclass: CLICTD_slowcontrol_driver

`endif // __CLICTD_slowcontrol_driver_sv__
