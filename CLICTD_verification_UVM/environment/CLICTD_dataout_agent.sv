// Filename           : CLICTD_dataout_agent.sv
// Author             : Núria Egidos 
// Created on         : 20/6/18
// Last modification  : 20/6/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Agent
// Derived from       : analysis_master_agent.sv by Elia Conti and Sara Marconi for VEPIX53

`ifndef __CLICTD_dataout_agent_sv__
`define __CLICTD_dataout_agent_sv__

//import uvm_pkg::*;
//`include <uvm_macros.svh>

class CLICTD_dataout_agent extends CLICTD_agent;
  `uvm_component_utils( CLICTD_dataout_agent ) // Register the component to the factory
	//
  // Declaration of all possible components. In the build_phase, we will
  // only create those indicated by the configuration object
  CLICTD_dataout_monitor monitor;
  //CLICTD_funccoverage_collector funccoverage_collector;

  // Declaration of the analysis ports that connect the analysis ports 
  // from the monitor to the analysis exports of the scoreboard in the environment
  uvm_analysis_port#(CLICTD_dataout_transaction) analysis_port;
  

  


	// Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new


	// Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase); // here it's checked whether the virtual interface handles and configuration object exist
    //
    // No driver or sequencer are created in dataout_agent because it's not active
    //
    // Create the sequencer and driver if the agent is active
    /*
    if      ( (agentname == "slowcontrol_agent"   ) && (agent_config.instantiate_driver == YES) ) begin
    		sequencer = CLICTD_sequencer::type_id::create( .name( "slowcontrol_sequencer" ), .parent( this ) );
        driver = CLICTD_driver::type_id::create( .name( "slowcontrol_driver" ), .parent( this ) );
    end
    else if ( (agentname == "regslowcontrol_agent") && (agent_config.instantiate_driver == YES) ) begin
				sequencer = CLICTD_sequencer::type_id::create( .name( "regslowcontrol_sequencer" ), .parent( this ) );
        driver = CLICTD_driver::type_id::create( .name( "regslowcontrol_driver" ), .parent( this ) );
    end
    else if ( (agentname == "dataout_agent"       ) && (agent_config.instantiate_driver        == YES) ) begin
				sequencer = CLICTD_sequencer::type_id::create( .name( "dataout_sequencer" ), .parent( this ) );
        driver = CLICTD_driver::type_id::create( .name( "dataout_driver" ), .parent( this ) );
    end
    else if ( (agentname == "dataout_agent"          ) && (agent_config.instantiate_driver  == YES) ) begin
				sequencer = CLICTD_sequencer::type_id::create( .name( "pins_sequencer" ), .parent( this ) );
        driver = CLICTD_driver::type_id::create( .name( "pins_driver" ), .parent( this ) );
    end
    */
			
		//
    /*
    // Create the functional coverage subscriber if the agent configuration object indicates so
		if      ( (agentname == "slowcontrol_agent"   ) && (agent_config.instantiate_functionalcoveragecollector == YES) ) begin
    		funccoverage_collector = CLICTD_funccoverage_collector::type_id::create( .name( "slowcontrol_funccoverage_collector" ), .parent( this ) );
    end
    else if ( (agentname == "regslowcontrol_agent") && (agent_config.instantiate_functionalcoveragecollector == YES) ) begin
				funccoverage_collector = CLICTD_funccoverage_collector::type_id::create( .name( "regslowcontrol_funccoverage_collector" ), .parent( this ) );
    end
    else if ( (agentname == "dataout_agent"       ) && (agent_config.instantiate_functionalcoveragecollector == YES) ) begin
				funccoverage_collector = CLICTD_funccoverage_collector::type_id::create( .name( "dataout_funccoverage_collector" ), .parent( this ) );
    end
    else if ( (agentname == "dataout_agent"          ) && (agent_config.instantiate_functionalcoveragecollector == YES) ) begin
				funccoverage_collector = CLICTD_funccoverage_collector::type_id::create( .name( "pins_funccoverage_collector" ), .parent( this ) );
    end

    */
		//

    // Create the agent analysis ports 
    analysis_port	= new(.name("analysis_port"), .parent(this));

    // Create the monitor
    monitor	= CLICTD_dataout_monitor::type_id::create(.name("dataout_monitor"), .parent(this));
		
    // 
	endfunction: build_phase


   // Connect phase: connect ports and exports
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
    //
    // If the agent is active, connect the driver 
    // seq_item_port to the sequencer seq_item_export 
    //if (driver != null)  driver.seq_item_port.connect(sequencer.seq_item_export);
		//
		// Connect the monitors' analysis ports to the agent ports,
    // which will eventually be connected to the scoreaboard
    // exports in the environment
		monitor.analysis_port.connect(analysis_port);
    //
    // If the agent configuration object indicates that there's a 
    // functional coverage subscriber, connect to it.
    // Connect the analysis port of the monitor 
    // to the built-in analysis export of the functional coverage subscriber
    //if (funccoverage_collector != null) begin
    //    monitor.monitor_analysisport.connect(funccoverage_collector.analysis_export);
    //end
    //
    /*
    // If the configuration indicates so, connect the analysis port of the agent
    // to the analysis export of some golden models so that they can tap
    // into the values of some signals present in the transactions generated by
    // the agent (this applies to knowing when the reset falls or rises, for instance)
    if ( (agentname == "dataout_agent" ) && (agent_config.scoreboards_to_probepins.size() > 0) ) begin

       //if (agent_config.scoreboards_to_probepins.exists(REGSLOWCONTROL_SCOREBOARD) && regslowcontrol_scoreboard != null )
       //    monitor.monitor_analysisport.connect(regslowcontrol_scoreboard.scoreboard_analysisexport_out_pins);
       //if (agent_config.scoreboards_to_probepins.exists(DATAOUT_SCOREBOARD) && dataout_scoreboard != null )
       //    monitor.monitor_analysisport.connect(dataout_scoreboard.scoreboard_analysisexport_out_pins);
       if (agent_config.scoreboards_to_probepins.exists(PINS_SCOREBOARD) && pins_scoreboard != null )
           monitor.monitor_analysisport.connect(pins_scoreboard.scoreboard_analysisexport_out_pins);

    end
    */
    //
	endfunction: connect_phase

endclass: CLICTD_dataout_agent

`endif // __CLICTD_dataout_agent_sv__

