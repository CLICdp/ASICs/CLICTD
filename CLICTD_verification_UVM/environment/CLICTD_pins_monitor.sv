// Filename           : CLICTD_pins_monitor.sv
// Author             : Núria Egidos 
// Created on         : 5/6/18
// Last modification  : 23/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Monitor to probe the signals in pins_interface

`ifndef __CLICTD_pins_monitor_sv__
`define __CLICTD_pins_monitor_sv__



class CLICTD_pins_monitor extends uvm_monitor;
	`uvm_component_utils(CLICTD_pins_monitor) // Register the component to the factory


	// Declare the analysis port that will be connected to the agent's
  // analysis port and this, in turn, to the scoreboard's
  uvm_analysis_port#(CLICTD_pins_transaction) analysis_port; 

  // Declare the transaction used to exchange information between the 
  // monitor and the scoreboard
	//CLICTD_pins_transaction pins_transaction; 


  CLICTDevents events_obj;
  ChipStatusInfo chip_status_obj;

  // Instance of pins_interface on which we are going to retrieve
  // the virtual interface handle from the factory
  virtual CLICTD_pins_interface pins_vif;


  int counter_clk100gated_while_powerEnable_is_low;

  // - - - - - - - - - - - - Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

  // - - - - - - - - - - - - Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
    // Create the monitor analysis port
		analysis_port = new(.name("analysis_port"), .parent(this));

    if( !uvm_config_db#(virtual CLICTD_pins_interface)::get(this, "", "pins_if", pins_vif) )
       `uvm_error( `NOVIF_PINS, $sformatf("Pins virtual interface not found in database for %s", this.get_full_name()) )

    // Retrieve the chip status from the database; it is set (and changed as the test progresses) from the virtual sequence 
    // in its body() task, so by this point it should be available to be retrieved
    if( !uvm_config_db#(ChipStatusInfo)::get(this, "", "chip_status_obj", chip_status_obj) )
         `uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )

    if( !uvm_config_db#(CLICTDevents)::get(this, "", "events_obj", events_obj) )
         `uvm_error( `NOCLICTDEVENTS, $sformatf("CLICTDevents not found in database for %s", this.get_full_name()) )
    

	endfunction: build_phase


  // - - - - - - - - - - - - Run phase: here take place the test actions
	task run_phase(uvm_phase phase);
       
    fork

        
        // Event detection
        detect_rise_enableoutpad();
        detect_fall_enableoutpad();
        detect_reset_released();
        detect_rise_powerEnable();
        detect_fall_powerEnable();
        detect_rise_pwren_pad();
        detect_fall_pwren_pad();
        detect_rise_powerInternal();
        detect_fall_powerInternal();
								detect_rise_discTp();
								detect_fall_discTp();
								detect_rise_tpulse_pad();
        detect_fall_tpulse_pad();
								detect_rise_testPulseIntStrobe();
        detect_fall_testPulseIntStrobe();
								detect_rise_digitalperi_roctrl_clk40();
								detect_fall_digitalperi_roctrl_clk40();
								detect_rise_readoutstart();
								detect_fall_readoutstart();
								detect_rise_digitalperi_rstnctrl_clk40();
								detect_fall_digitalperi_rstnctrl_clk40();
								detect_rise_digitalperi_rstnctrl_rstnin();

        // Generating transactions
        pinsmonitor_action_resetreleased(); // when reset is released
        pinsmonitor_action_configreadout(); //  chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF
        pinsmonitor_action_powerEnable_updated(); // generate a transaction when powerEnable rises or falls
								pinsmonitor_action_discTp_updated(); // generate a transaction when discTp rises
        // Count number of CLK_100_GATED cycles that occur while powerEnable is low
        count_clk100gated_while_powerEnable_is_low();

    join_none
    
    // The fork join_none is used to avoid that this forever begin blocks the execution of other 
    // run_phase tasks in the hierarchy
	endtask: run_phase

  //  - - - - - - - - - - - - Other tasks
  //
  task count_clk100gated_while_powerEnable_is_low();
        forever begin
                
                events_obj.fall_powerEnable_event.wait_trigger();
                //counter_clk100gated_while_powerEnable_is_low = 0;
                //$display("%t pins monitor - power enable falling edge",$realtime);
                while(!pins_vif.powerEnable) begin
                      @(posedge pins_vif.CLK_100_GATED) begin
                                counter_clk100gated_while_powerEnable_is_low++;
                                //$display("%t pins monitor - counter_clk100gated_while_powerEnable_is_low = %d",$realtime,counter_clk100gated_while_powerEnable_is_low);
                      end
                end
                //counter_clk100gated_while_powerEnable_is_low = 0;
        end
  endtask: count_clk100gated_while_powerEnable_is_low
  //
  task detect_rise_enableoutpad();
				forever begin @(posedge pins_vif.ENABLE_OUT_pad) begin events_obj.rise_enableoutpad_event.trigger(); `uvm_info("Start readout", $sformatf("ENABLE_OUT_pad rise"), UVM_LOW ) end end
  endtask: detect_rise_enableoutpad
  //
  task detect_fall_enableoutpad();
				forever begin @(negedge pins_vif.ENABLE_OUT_pad) begin events_obj.fall_enableoutpad_event.trigger(); end end
  endtask: detect_fall_enableoutpad
  //
  task detect_reset_released();
        // Detect falling edge in RSTN_pad followed by a rising edge
        forever begin 
                @(negedge pins_vif.RSTN_pad) begin 
                          @(posedge pins_vif.RSTN_pad) events_obj.reset_released_event.trigger(); 
                end 
        end
  endtask: detect_reset_released
  //
  task detect_rise_powerEnable();
				forever begin @(posedge pins_vif.powerEnable) begin events_obj.rise_powerEnable_event.trigger(); end end
  endtask: detect_rise_powerEnable
  //
  task detect_fall_powerEnable();
				forever begin @(negedge pins_vif.powerEnable) begin events_obj.fall_powerEnable_event.trigger(); end end
  endtask: detect_fall_powerEnable
  //
  task detect_rise_pwren_pad();
      forever begin @(posedge pins_vif.PWREN_pad) begin events_obj.rise_pwren_pad_event.trigger(); end end 
  endtask: detect_rise_pwren_pad
  //
  task detect_fall_pwren_pad();
      forever begin @(negedge pins_vif.PWREN_pad) begin events_obj.fall_pwren_pad_event.trigger(); end end 
  endtask: detect_fall_pwren_pad
  //
  task detect_rise_powerInternal();
      forever begin @(posedge pins_vif.powerInternal) begin events_obj.rise_powerInternal_event.trigger(); end end 
  endtask: detect_rise_powerInternal
  //
  task detect_fall_powerInternal();
      forever begin @(negedge pins_vif.powerInternal) begin events_obj.fall_powerInternal_event.trigger(); end end 
  endtask: detect_fall_powerInternal
		//
		task detect_rise_discTp();
		    forever begin @(posedge pins_vif.discTp) begin events_obj.rise_discTp_event.trigger(); end end 
		endtask: detect_rise_discTp
  //
		task detect_fall_discTp();
		    forever begin @(negedge pins_vif.discTp) begin events_obj.fall_discTp_event.trigger(); end end 
		endtask: detect_fall_discTp
  //
		task detect_rise_tpulse_pad();
      forever begin @(posedge pins_vif.TPULSE_pad) begin events_obj.rise_tpulse_pad_event.trigger(); end end 
  endtask: detect_rise_tpulse_pad
  //
  task detect_fall_tpulse_pad();
      forever begin @(negedge pins_vif.TPULSE_pad) begin events_obj.fall_tpulse_pad_event.trigger(); end end 
  endtask: detect_fall_tpulse_pad
		//
		task detect_rise_testPulseIntStrobe();
      forever begin @(posedge pins_vif.testPulseIntStrobe) begin events_obj.rise_testPulseIntStrobe_event.trigger(); end end 
  endtask: detect_rise_testPulseIntStrobe
  //
  task detect_fall_testPulseIntStrobe();
      forever begin @(negedge pins_vif.testPulseIntStrobe) begin events_obj.fall_testPulseIntStrobe_event.trigger(); end end 
  endtask: detect_fall_testPulseIntStrobe
		//
		task detect_rise_digitalperi_roctrl_clk40();
		    forever begin @(posedge pins_vif.digitalperi_roctrl_clk40) begin events_obj.rise_digitalperi_roctrl_clk40_event.trigger(); end end 
		endtask: detect_rise_digitalperi_roctrl_clk40
		//
		task detect_fall_digitalperi_roctrl_clk40();
		    forever begin @(negedge pins_vif.digitalperi_roctrl_clk40) begin events_obj.fall_digitalperi_roctrl_clk40_event.trigger(); end end 
		endtask: detect_fall_digitalperi_roctrl_clk40
		//
		task detect_rise_readoutstart();
		    forever begin @(posedge pins_vif.digitalperi_roctrl_readoutStart) begin events_obj.rise_readoutstart_event.trigger(); end end 
		endtask: detect_rise_readoutstart
		//
		task detect_fall_readoutstart();
		    forever begin @(negedge pins_vif.digitalperi_roctrl_readoutStart) begin events_obj.fall_readoutstart_event.trigger(); end end 
		endtask: detect_fall_readoutstart
		//
		task detect_rise_digitalperi_rstnctrl_clk40();
		     forever begin @(posedge pins_vif.digitalperi_rstnctrl_clk40) begin events_obj.rise_digitalperi_rstnctrl_clk40_event.trigger(); end end  
		endtask: detect_rise_digitalperi_rstnctrl_clk40
		//
		task detect_fall_digitalperi_rstnctrl_clk40();
		     forever begin @(negedge pins_vif.digitalperi_rstnctrl_clk40) begin events_obj.fall_digitalperi_rstnctrl_clk40_event.trigger(); end end  
		endtask: detect_fall_digitalperi_rstnctrl_clk40
		//
		task detect_rise_digitalperi_rstnctrl_rstnin();
		     forever begin @(posedge pins_vif.digitalperi_rstnctrl_rstnin) begin events_obj.rise_digitalperi_rstnctrl_rstnin_event.trigger(); end end  
		endtask: detect_rise_digitalperi_rstnctrl_rstnin
		//
  function void collect_values_for_transaction(CLICTD_pins_transaction pins_trans);
       // Checked in STATUS_RESET
       pins_trans.columnDone      = pins_vif.columnDone;
			 			pins_trans.dataIn          = pins_vif.dataIn;
			 			pins_trans.commonToken     = pins_vif.commonToken;
       // Checked in STATUS_CONFIG_READOUT_SECOND_HALF
       pins_trans.tpEnableDigital = pins_vif.tpEnableDigital;
			 			pins_trans.mask            = pins_vif.mask;
			 			pins_trans.tpEnableAnalog  = pins_vif.tpEnableAnalog;
			 			pins_trans.tuningDAC0      = pins_vif.tuningDAC0;
			 			pins_trans.tuningDAC1      = pins_vif.tuningDAC1;
			 			pins_trans.tuningDAC2      = pins_vif.tuningDAC2;
			 			pins_trans.tuningDAC3      = pins_vif.tuningDAC3;
			 			pins_trans.tuningDAC4      = pins_vif.tuningDAC4;
			 			pins_trans.tuningDAC5      = pins_vif.tuningDAC5;
			 			pins_trans.tuningDAC6      = pins_vif.tuningDAC6;
			 			pins_trans.tuningDAC7      = pins_vif.tuningDAC7;
       // Checked in STATUS_POWERPULSE_EXTERNAL_WORKING, STATUS_POWERPULSE_INTERNAL_WORKING
       pins_trans.powerEnable     = pins_vif.powerEnable;
       pins_trans.counter_clk100gated_while_powerEnable_is_low = counter_clk100gated_while_powerEnable_is_low;
							// Checked in STATUS_TESTPULSE_EXTERNAL_WORKING, STATUS_TESTPULSE_INTERNAL_WORKING
							pins_trans.discTp          = pins_vif.discTp;
							// Other signals
  					pins_trans.PWREN_pad          = pins_vif.PWREN_pad;    
  					pins_trans.TPULSE_pad         = pins_vif.TPULSE_pad;   
  					pins_trans.READOUT_pad        = pins_vif.READOUT_pad;	
  					pins_trans.SHUTTER_pad        = pins_vif.SHUTTER_pad; 
  					pins_trans.RSTN_pad           = pins_vif.RSTN_pad; 

  					pins_trans.ENABLE_OUT_pad     = pins_vif.ENABLE_OUT_pad;

  					pins_trans.readoutStartExtEn  = pins_vif.readoutStartExtEn;
  					pins_trans.readoutStart       = pins_vif.readoutStart;

  					pins_trans.testPulseExtEnable = pins_vif.testPulseExtEnable; 
  					pins_trans.testPulse          = pins_vif.testPulse;         

  					pins_trans.powerExtEnable     = pins_vif.powerExtEnable; 
  					pins_trans.powerEnable        = pins_vif.powerEnable; 
  					pins_trans.CLK_100_GATED      = pins_vif.CLK_100_GATED;

	 					pins_trans.disc               = pins_vif.disc;
	
	
							

  endfunction: collect_values_for_transaction
  //
  // Generate a transaction when the reset is released
  task pinsmonitor_action_resetreleased();
       CLICTD_pins_transaction pins_transaction;
       forever begin
							 events_obj.reset_released_event.wait_trigger();
               pins_transaction = CLICTD_pins_transaction::type_id::create(.name("pins_transaction"), .contxt(get_full_name()));
							 collect_values_for_transaction(pins_transaction);
							 analysis_port.write(pins_transaction);
								//$display("DISPLAY - pins monitor - %t pins monitor sent transaction, reset released, chip status = %s",$realtime,chip_status_obj.chip_status);
       end
  endtask: pinsmonitor_action_resetreleased
  //
  // Generate a transaction when the matrix is configured
  task pinsmonitor_action_configreadout();
       CLICTD_pins_transaction pins_transaction;
       forever begin
               chip_status_obj.chip_status_update_event.wait_trigger();
               if(chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF) begin // at this state, we can be sure that both configuration halves have been completed
                  pins_transaction = CLICTD_pins_transaction::type_id::create(.name("pins_transaction"), .contxt(get_full_name()));
                  collect_values_for_transaction(pins_transaction);
							           analysis_port.write(pins_transaction);
																		`uvm_info("Pins monitor sent", $sformatf("Transaction sent to pins_comparator and pins_model, config readout, %s", chip_status_obj.chip_status),  UVM_LOW);
               end
       end
  endtask: pinsmonitor_action_configreadout
  // 
  // Generate a transaction when power pulsing is enabled or disabled (powerEnable should change accordingly)
  task pinsmonitor_action_powerEnable_updated();
       CLICTD_pins_transaction pins_transaction;
       forever begin
               fork
							        begin events_obj.rise_powerEnable_event.wait_trigger(); //events_obj.rise_powerEnable_event.reset(); 
               end
               begin events_obj.fall_powerEnable_event.wait_trigger(); //events_obj.fall_powerEnable_event.reset(); 
               end
               join_any
               disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
															// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
															// which could otherwise lead to unexpected behavior (messages printing twice...)

							        if (chip_status_obj.chip_status == STATUS_POWERPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS ||
                   chip_status_obj.chip_status == STATUS_POWERPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS ) begin
                   pins_transaction = CLICTD_pins_transaction::type_id::create(.name("pins_transaction"), .contxt(get_full_name()));
							            collect_values_for_transaction(pins_transaction);
                   //$display("DISPLAY - pins monitor - %t pins monitor BEFORE sent transaction, powerEnable = %h, counter clk100gated = %d, chip status = %s",
																			// $realtime,pins_transaction.powerEnable,pins_transaction.counter_clk100gated_while_powerEnable_is_low,chip_status_obj.chip_status);
																			`uvm_info("Pins monitor sent", $sformatf("Transaction sent to pins_comparator, chip_status = %s, powerEnable = %h, counter CLK_100_GATED = %d", chip_status_obj.chip_status,
                    pins_transaction.powerEnable,pins_transaction.counter_clk100gated_while_powerEnable_is_low),  UVM_LOW);
                   analysis_port.write(pins_transaction);
                   //$display("DISPLAY - pins monitor - %t pins monitor sent transaction, powerEnable = %h, counter clk100gated = %d, chip status = %s",$realtime,pins_transaction.powerEnable,pins_transaction.counter_clk100gated_while_powerEnable_is_low,chip_status_obj.chip_status);
                   if (pins_vif.powerEnable > 0) counter_clk100gated_while_powerEnable_is_low = 0; // reset the counter when powerEnable goes back to the OFF state
               end
       end
  endtask: pinsmonitor_action_powerEnable_updated
		// 
  // Generate a transaction when a test pulse is applied
  task pinsmonitor_action_discTp_updated();
       CLICTD_pins_transaction pins_transaction;
       forever begin
															fork
							        begin events_obj.rise_discTp_event.wait_trigger(); 
               end
               begin events_obj.fall_discTp_event.wait_trigger(); 
               end
               join_any
               disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
															// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
															// which could otherwise lead to unexpected behavior (messages printing twice...)
               
							        if (chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS ||
                   chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS ) begin
                   pins_transaction = CLICTD_pins_transaction::type_id::create(.name("pins_transaction"), .contxt(get_full_name()));
							            collect_values_for_transaction(pins_transaction);
																			`uvm_info("Pins monitor sent", $sformatf("Transaction sent to pins_comparator, chip_status = %s, discTp = %h", chip_status_obj.chip_status, pins_transaction.discTp),  UVM_LOW);
                   analysis_port.write(pins_transaction);
																			//$display("DISPLAY - pins monitor - %t pins monitor sent transaction, discTp, chip status = %s",$realtime,chip_status_obj.chip_status);
               end
       end
  endtask: pinsmonitor_action_discTp_updated

endclass: CLICTD_pins_monitor





`endif // __CLICTD_pins_monitor_sv__

