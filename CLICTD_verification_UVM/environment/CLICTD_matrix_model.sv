// Filename           : CLICTD_matrix_model.sv
// Author             : Núria Egidos 
// Created on         : 9/8/18
// Last modification  : 30/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : 
//											STATUS_MATRIX_CONFIG_FIRST/SECOND_HALF:
//                      From the configuration values sent via the slow control (sampled from slowcontrol_monitor), 
//											the expected value of tpEnableDigital, mask[7:0], tpEnableAnalog[7:0] and tuningDAC7-0[2:0]
//											is built and provided to pins_model via the devoted analysis port (using a pins_transaction). 
//                      The expected value of DATA_OUT (in fact, the expected value of the different fields of the 
//											slowcontrol_transactions corresponding to each column) are also built, but not yet sent.
//
//											STATUS_CONFIG_READOUT_FIRST/SECOND_HALF:
//											The slowcontrol_transactions corresponding to the expected values of DATA_OUT that were 
//											generated in STATUS_MATRIX_CONFIG_FIRST/SECOND_HALF are sent to dataout_model.
//
//											STATUS_FORCE_HIT_FROM_SLOWCONTROL/EXTERNAL:
//											Based on the configuration values set in STATUS_MATRIX_CONFIG_FIRST/SECOND_HALF and on 
//											the specified pattern of pixels hit/masked in the present status, the expected value of 
//											DATA_OUT (in fact, the expected value of the different fields of the 
//											slowcontrol_transactions corresponding to each column) is built, but not yet sent.
//
//											STATUS_MATRIX_READOUT_ACQUIRED_FROM_SLOWCONTROL/EXTERNAL:
//											The slowcontrol_transactions corresponding to the expected values of DATA_OUT that were 
//											generated in STATUS_FORCE_HIT_FROM_SLOWCONTROL/EXTERNAL are sent to dataout_model.

// http://www.chipverify.com/uvm/tlm-analysis-port
// https://www.quora.com/What-is-the-difference-between-an-uvm_analysis_export-and-an-uvm_analysis_imp-in-UVM
// https://verificationacademy.com/forums/uvm/how-override-default-parameterized-class-while-also-using-uvmanalysisimp
// https://dvteclipse.com/uvm-1.2_Public_API/uvm_pkg-uvm_analysis_imp.html// https://daffy1108.wordpress.com/2011/05/10/128/

`ifndef __CLICTD_matrix_model_sv__
`define __CLICTD_matrix_model_sv__



class CLICTD_matrix_model extends uvm_component; 

	`uvm_component_utils(CLICTD_matrix_model) // Register the component to the factory

  // Analysis port to connect to pins_scoreboard and provide the expected pin values to pins_model
	uvm_analysis_port#(CLICTD_pins_transaction) analysis_port_pins;

  // Analysis port to connect to dataout_scoreboard and provide the expected DATA_OUT values to dataout_model
  uvm_analysis_port#(CLICTD_dataout_transaction) analysis_port_dataout;

  // Export to connect to slowcontrol_agent and obtain the configuration sent to the slow control from the slowcontrol_monitor
  //uvm_analysis_export#(CLICTD_slowcontrol_transaction) analysis_export;
  // The component that implements the write function uses a uvm_analysis_imp (subscriber, this model...); the component that 
  // connects an export in the parent component (scoreboard...) to the export that implements the write function uses a uvm_analysis_export
  uvm_analysis_imp#(CLICTD_slowcontrol_transaction, CLICTD_matrix_model) analysis_export;
  
 
  ChipStatusInfo chip_status_obj;
  ReportingEnable reporting_enable_obj;
  CLICTDevents events_obj;
		FieldsConfigureMatrix config_matrix_obj;
		
		
		
		Configured_word_struct [`COLUMNS-1:0] configured_data;
		logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] dummybit_2ndconfighalf_bit0;


  // Counters for bits received
  int superpixel_segment_counter,superpixel_in_segment_counter;
  int bit_counter = `NUMBER_OF_FF_PER_SUPERPIXEL-1;
  int matrix_row = 0; // bottom row (superpixel 0), which is translated into the MSB of the column_content

  logic [`NUMBER_OF_FF_PER_SUPERPIXEL*`SUPERPIXELS_IN_ONE_SEGMENT*`SUPERPIXEL_SEGMENTS-1:0] aux_vector;
  logic [`COLUMNS-1:0] config_word;





  // - - - - - - - - - - - - Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

  // - - - - - - - - - - - - Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
    // Create the ports and exports
		analysis_port_pins = new(.name("analysis_port_pins"), .parent(this));
    analysis_port_dataout = new(.name("analysis_port_dataout"), .parent(this));
    analysis_export = new(.name("analysis_export"), .imp(this));

    // Retrieve the chip status from the database; it is set (and changed as the test progresses) from the virtual sequence 
    // in its body() task, so by this point it should be available to be retrieved
    if( !uvm_config_db#(ChipStatusInfo)::get(this, "", "chip_status_obj", chip_status_obj) )
       `uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )

    if( !uvm_config_db#(CLICTDevents)::get(this, "", "events_obj", events_obj) )
         `uvm_error( `NOCLICTDEVENTS, $sformatf("CLICTDevents not found in database for %s", this.get_full_name()) )
									
    if( !uvm_config_db#(ReportingEnable)::get(this, "", "reporting_enable_obj", reporting_enable_obj) )
         `uvm_error( `NOREPORTINGENABLE, $sformatf("ReportingEnable not found in database for %s", this.get_full_name()) )
    

    if( !uvm_config_db#(FieldsConfigureMatrix)::get(this, "", "config_matrix_obj", config_matrix_obj) )
         `uvm_error( `NOMATRIXCONFIG, $sformatf("config_matrix_obj not found in database for %s", this.get_full_name()) )

	endfunction: build_phase

 
  // - - - - - - - - - - - - Other tasks
		
		
		
		
		// ---------------------------------------------------------------------------------------------------------------
		// Build the content of all superpixels from the configured values
		// ---------------------------------------------------------------------------------------------------------------
		function void build_superpixel_content_from_configured_values(  input int column_number, output logic [`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0] superpixel_content		);

																for ( int superpixel_segment = 0; superpixel_segment < `SUPERPIXEL_SEGMENTS; superpixel_segment++) begin
				     												for ( int superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment++) begin

											      										if      (chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_FIRST_HALF) begin
																	         										superpixel_content[superpixel_segment][superpixel_in_segment][0] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tpEnableDigital;
																																				superpixel_content[superpixel_segment][superpixel_in_segment][1] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].mask[0];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][2] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].mask[1];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][3] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].mask[2];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][4] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].mask[3];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][5] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].mask[4];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][6] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].mask[5];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][7] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].mask[6];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][8] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].mask[7];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][9] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tpEnableAnalog[0];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][10] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tpEnableAnalog[1];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][11] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tpEnableAnalog[2];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][12] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tpEnableAnalog[3];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][13] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tpEnableAnalog[4];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][14] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tpEnableAnalog[5];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][15] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tpEnableAnalog[6];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][16] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tpEnableAnalog[7];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][17] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC0[0];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][18] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC0[1];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][19] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC0[2];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][20] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC1[0];
																																				superpixel_content[superpixel_segment][superpixel_in_segment][21] = 1'b1; // hit flag is forced to 1 when configuration readout is performed

																											end
																											else if (chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_SECOND_HALF) begin
																	        										superpixel_content[superpixel_segment][superpixel_in_segment][0] = dummybit_2ndconfighalf_bit0[column_number][superpixel_segment][superpixel_in_segment];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][1] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC1[1];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][2] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC1[2];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][3] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC2[0];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][4] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC2[1];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][5] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC2[2];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][6] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC3[0];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][7] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC3[1];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][8] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC3[2];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][9] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC4[0];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][10] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC4[1];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][11] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC4[2];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][12] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC5[0];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][13] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC5[1];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][14] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC5[2];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][15] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC6[0];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][16] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC6[1];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][17] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC6[2];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][18] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC7[0];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][19] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC7[1];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][20] = configured_data[column_number].superpixel[superpixel_segment][superpixel_in_segment].tuningDAC7[2];
																																			superpixel_content[superpixel_segment][superpixel_in_segment][21] = 1'b1; // hit flag is forced to 1 when configuration readout is performed
																											end


				     												end
				 												end

		endfunction:build_superpixel_content_from_configured_values


		// ---------------------------------------------------------------------------------------------------------------
		// Build the content of all columns from the configured values
		// ---------------------------------------------------------------------------------------------------------------
		function void build_column_content_from_configured_values ( input int column_number, output logic column_content[int]	);
  		int offset_position_in_columncontent;
  		int ss,sis;
				logic [`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0] superpixel_content;

    build_superpixel_content_from_configured_values(  .column_number(column_number), .superpixel_content(superpixel_content)		);

    offset_position_in_columncontent = 0;
    for (int _ss = 0; _ss < `SUPERPIXEL_SEGMENTS; _ss++) begin
        ss = `SUPERPIXEL_SEGMENTS - 1 - _ss;
        for (int _sis = 0; _sis < `SUPERPIXELS_IN_ONE_SEGMENT; _sis++) begin
            sis = `SUPERPIXELS_IN_ONE_SEGMENT - 1 - _sis;
            for (int i = 0; i < `NUMBER_OF_FF_PER_SUPERPIXEL; i++) column_content[offset_position_in_columncontent+i] = superpixel_content[ss][sis][i];
            offset_position_in_columncontent = offset_position_in_columncontent + `NUMBER_OF_FF_PER_SUPERPIXEL;
        end
    end
    		

		endfunction: build_column_content_from_configured_values
		

  // Convert the bits received from the slow control into dataout_transaction for dataout_model
  // and send the generated transactions
  function void convert_from_bits_to_dataout_and_send();
             CLICTD_dataout_transaction auxiliary_dataout_transaction;
													Configured_word_struct config_word_ideal;
													array_exitcodeconfig_fullcolumn_type comparison_result_acquired;
													array_exitcodeconfig_type aux_comparison_result_acquired;
             // http://forums.accellera.org/topic/723-all-the-get-item-from-uvm_tlm_analysis_fifo-are-the-last-one-been-pushed-into/
             
             // Note: the format in column_content is the following:
						       //     COLUMN 0       COLUMN 1     ...   COLUMN 15
						       // 21 ......... 0 21 ......... 0   ... 21 ......... 0
													// 2815 ............ column_content ............... 0

             for (int _column = 0; _column < `COLUMNS; _column++) begin
                 auxiliary_dataout_transaction = CLICTD_dataout_transaction::type_id::create(.name("auxiliary_dataout_transaction"), .contxt(get_full_name()));
                 auxiliary_dataout_transaction.column_counter = _column;
                 //auxiliary_dataout_transaction.column_header = {10'b1011000101,_column[3:0],8'b0};
																	case (_column[3:0])
													      0: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_0;
																			1: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_1;
																			2: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_2;
																			3: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_3;
																			4: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_4;
																			5: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_5;
																			6: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_6;
																			7: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_7;
																			8: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_8;
																			9: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_9;
																			10: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_10;
																			11: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_11;
																			12: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_12;
																			13: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_13;
																			14: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_14;
																			15: auxiliary_dataout_transaction.column_header = `HEADER_COLUMN_15;
													    endcase
                 
																	build_column_content_from_configured_values ( .column_number(_column), .column_content(auxiliary_dataout_transaction.column_content)	);			
                 for (int i = 0; i < `NUMBER_OF_FF_PER_SUPERPIXEL*`SUPERPIXELS_IN_ONE_SEGMENT*`SUPERPIXEL_SEGMENTS; i++) begin
                     aux_vector[i] =  auxiliary_dataout_transaction.column_content[i]; // pixel 0 becomes the MSB when readout
                 end
																															
																	
																 //if (chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_FIRST_HALF || chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_SECOND_HALF) begin
																	
																	         //if (chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_FIRST_HALF)
																										//     breakdown_columcontent_into_superpixelconfig ( .column_content(auxiliary_dataout_transaction.column_content),.configuration_half(`FIRST_CONFIGURATION_HALF),.superpixels_config_word(auxiliary_dataout_transaction.column_config_breakdown) );
																										//else breakdown_columcontent_into_superpixelconfig ( .column_content(auxiliary_dataout_transaction.column_content),.configuration_half(`SECOND_CONFIGURATION_HALF),.superpixels_config_word(auxiliary_dataout_transaction.column_config_breakdown) );
																										auxiliary_dataout_transaction.column_config_breakdown = configured_data[_column];
																	//end
																	
																	    /*
																	    if (chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_SECOND_HALF) begin
                 
																					     for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																										     for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																					              $display("DISPLAY matrix model - convert dataout - tuningDAC1[%0d][%0d][%0d][2] = %b, tuningDAC1[%0d][%0d][%0d][1] = %b",
																																			auxiliary_dataout_transaction.column_counter,ss,sis,auxiliary_dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC1[2],
																																			auxiliary_dataout_transaction.column_counter,ss,sis,auxiliary_dataout_transaction.column_config_breakdown.superpixel[ss][sis].tuningDAC1[1]);
																										     end
																										end
																					end
																	    */
                 analysis_port_dataout.write(auxiliary_dataout_transaction); // write one transaction per column
                 `uvm_info("Matrix model sent", $sformatf("Transaction sent to dataout_model, column_header = %h, column_content = %h", auxiliary_dataout_transaction.column_header, aux_vector), UVM_LOW)
               end
  endfunction: convert_from_bits_to_dataout_and_send

  // Convert the bits received from the slow control into pins_transaction for pins_model
  function void convert_from_bits_to_pins_and_send();
              CLICTD_pins_transaction pins_transaction;
																
														pins_transaction = CLICTD_pins_transaction::type_id::create(.name("pins_transaction"), .contxt(get_full_name()));
														for (int c = 0; c < `COLUMNS; c++) begin
														    for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																		    for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																						     pins_transaction.tpEnableDigital[c][ss][sis] = configured_data[c].superpixel[ss][sis].tpEnableDigital;
																											pins_transaction.mask[c][ss][sis]            = configured_data[c].superpixel[ss][sis].mask;
																											pins_transaction.tpEnableAnalog[c][ss][sis]  = configured_data[c].superpixel[ss][sis].tpEnableAnalog;
																											pins_transaction.tuningDAC0[c][ss][sis]      = configured_data[c].superpixel[ss][sis].tuningDAC0;
																											pins_transaction.tuningDAC1[c][ss][sis]      = configured_data[c].superpixel[ss][sis].tuningDAC1;
																											pins_transaction.tuningDAC2[c][ss][sis]      = configured_data[c].superpixel[ss][sis].tuningDAC2;
																											pins_transaction.tuningDAC3[c][ss][sis]      = configured_data[c].superpixel[ss][sis].tuningDAC3;
																											pins_transaction.tuningDAC4[c][ss][sis]      = configured_data[c].superpixel[ss][sis].tuningDAC4;
																											pins_transaction.tuningDAC5[c][ss][sis]      = configured_data[c].superpixel[ss][sis].tuningDAC5;
																											pins_transaction.tuningDAC6[c][ss][sis]      = configured_data[c].superpixel[ss][sis].tuningDAC6;
																											pins_transaction.tuningDAC7[c][ss][sis]      = configured_data[c].superpixel[ss][sis].tuningDAC7;
																											
																											
																											//$display("DISPLAY matrix model - convert pins - tuningDAC1[%0d][%0d][%0d][2] = %b, tuningDAC1[%0d][%0d][%0d][1] = %b, tuningDAC1[%0d][%0d][%0d][0] = %b, chip status %s",
																											//c,ss,sis,pins_transaction.tuningDAC1[c][ss][sis][2], c,ss,sis,pins_transaction.tuningDAC1[c][ss][sis][1], c,ss,sis,pins_transaction.tuningDAC1[c][ss][sis][0], chip_status_obj.chip_status);
																						end
																		end
														end
														analysis_port_pins.write(pins_transaction);
														
														
																										
														                             /*
													                             	for (int c = 0; c < `COLUMNS; c++) begin
																																																	for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						    																																											for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																																																										$display("DISPLAY - matrix model - c %0d, ss %0d, sis %0d, EXPECTED: tpEnableDigital = %b, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
                          																																	c, ss,sis,
																																																											pins_transaction.tpEnableDigital[c][ss][sis], pins_transaction.mask[c][ss][sis], pins_transaction.tpEnableAnalog[c][ss][sis], pins_transaction.tuningDAC0[c][ss][sis],
																																																											pins_transaction.tuningDAC1[c][ss][sis], pins_transaction.tuningDAC2[c][ss][sis], pins_transaction.tuningDAC3[c][ss][sis], pins_transaction.tuningDAC4[c][ss][sis],
																																																											pins_transaction.tuningDAC5[c][ss][sis], pins_transaction.tuningDAC6[c][ss][sis], pins_transaction.tuningDAC7[c][ss][sis]
																																																											);
																																																					end
																																																	end
																																												end
																            												    */

      endfunction: convert_from_bits_to_pins_and_send

  // Implementation of the write() function, which receives transactions from the slowcontrol_agent
	function void write(CLICTD_slowcontrol_transaction t); // t is used to match the declaration of pure virtual function write in uvm_subscriber
		
					 assert_isunknown_slowcontrol_transaction ( .slowcontrol_transaction(t), .chip_status_obj(chip_status_obj), .name_of_transaction("t"), .name_of_calling_component(this.get_full_name()) );

					 if (chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_FIRST_HALF || chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_SECOND_HALF) begin: status_matrix_config
					 		
								     		//
										 				if      (t.register_address == `ADDRESS_configData7downto0)  begin: first_configdata 
																									for (int c = 0; c < 8; c++) begin 
																									    configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].superpixel_segment = superpixel_segment_counter;
																													configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].superpixel_in_segment = superpixel_in_segment_counter;
																									end
																									      
																									if ( chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_FIRST_HALF ) begin
																													case (bit_counter)
																													      `MATRIXCONFIG_tpEnableDigital : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableDigital   = t.register_content[c];     
   																																`MATRIXCONFIG_mask0           : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[0]           = t.register_content[c];
		 																																`MATRIXCONFIG_mask1           : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[1]           = t.register_content[c];
		 																																`MATRIXCONFIG_mask2           : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[2]           = t.register_content[c];
		 																																`MATRIXCONFIG_mask3           : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[3]           = t.register_content[c];
		 																																`MATRIXCONFIG_mask4           : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[4]           = t.register_content[c];
		 																																`MATRIXCONFIG_mask5           : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[5]           = t.register_content[c];
		 																																`MATRIXCONFIG_mask6           : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[6]           = t.register_content[c];
		 																																`MATRIXCONFIG_mask7           : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[7]           = t.register_content[c];
	  																																`MATRIXCONFIG_tpEnableAnalog0 : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[0] = t.register_content[c];
		 																																`MATRIXCONFIG_tpEnableAnalog1 : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[1] = t.register_content[c];
		 																																`MATRIXCONFIG_tpEnableAnalog2 : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[2] = t.register_content[c];
		 																																`MATRIXCONFIG_tpEnableAnalog3 : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[3] = t.register_content[c];
		 																																`MATRIXCONFIG_tpEnableAnalog4 : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[4] = t.register_content[c];     
		 																																`MATRIXCONFIG_tpEnableAnalog5 : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[5] = t.register_content[c];
		 																																`MATRIXCONFIG_tpEnableAnalog6 : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[6] = t.register_content[c];
		 																																`MATRIXCONFIG_tpEnableAnalog7 : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[7] = t.register_content[c];
		 																																`MATRIXCONFIG_tuningDAC00     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC0[0]     = t.register_content[c];
		 																																`MATRIXCONFIG_tuningDAC01     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC0[1]     = t.register_content[c];
		 																																`MATRIXCONFIG_tuningDAC02     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC0[2]     = t.register_content[c];
		 																																`MATRIXCONFIG_tuningDAC10     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC1[0]     = t.register_content[c];
																													endcase
																									end
																									else if ( chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_SECOND_HALF ) begin
																									    case (bit_counter)
																													      `MATRIXCONFIG_tuningDAC11     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC1[1] = t.register_content[c];           
	 																																	`MATRIXCONFIG_tuningDAC12     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC1[2] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC20     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC2[0] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC21     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC2[1] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC22     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC2[2] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC30     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC3[0] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC31     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC3[1] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC32     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC3[2] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC40     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC4[0] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC41     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC4[1] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC42     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC4[2] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC50     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC5[0] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC51     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC5[1] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC52     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC5[2] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC60     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC6[0] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC61     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC6[1] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC62     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC6[2] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC70     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC7[0] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC71     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC7[1] = t.register_content[c];
	 																																	`MATRIXCONFIG_tuningDAC72     : for (int c = 0; c < 8; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC7[2] = t.register_content[c];
																																			`MATRIXCONFIG_dummy_bit_0_secondhalf : for (int c = 0; c < 8; c++) dummybit_2ndconfighalf_bit0[c][superpixel_segment_counter][superpixel_in_segment_counter] = t.register_content[c];
																													endcase
																									end
																									
																									
								     		end: first_configdata 
								     		//
										 				else if (t.register_address == `ADDRESS_configData15downto8) begin: second_configdata 
															
															          for (int c = 8; c < `COLUMNS; c++) begin 
																									    configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].superpixel_segment = superpixel_segment_counter;
																													configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].superpixel_in_segment = superpixel_in_segment_counter;
																									end
																									
															          if ( chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_FIRST_HALF ) begin
																													case (bit_counter)
																													      `MATRIXCONFIG_tpEnableDigital : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableDigital   = t.register_content[c-8];     
   																																`MATRIXCONFIG_mask0           : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[0]           = t.register_content[c-8];
		 																																`MATRIXCONFIG_mask1           : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[1]           = t.register_content[c-8];
		 																																`MATRIXCONFIG_mask2           : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[2]           = t.register_content[c-8];
		 																																`MATRIXCONFIG_mask3           : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[3]           = t.register_content[c-8];
		 																																`MATRIXCONFIG_mask4           : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[4]           = t.register_content[c-8];
		 																																`MATRIXCONFIG_mask5           : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[5]           = t.register_content[c-8];
		 																																`MATRIXCONFIG_mask6           : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[6]           = t.register_content[c-8];
		 																																`MATRIXCONFIG_mask7           : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].mask[7]           = t.register_content[c-8];
	  																																`MATRIXCONFIG_tpEnableAnalog0 : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[0] = t.register_content[c-8];
		 																																`MATRIXCONFIG_tpEnableAnalog1 : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[1] = t.register_content[c-8];
		 																																`MATRIXCONFIG_tpEnableAnalog2 : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[2] = t.register_content[c-8];
		 																																`MATRIXCONFIG_tpEnableAnalog3 : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[3] = t.register_content[c-8];
		 																																`MATRIXCONFIG_tpEnableAnalog4 : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[4] = t.register_content[c-8];     
		 																																`MATRIXCONFIG_tpEnableAnalog5 : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[5] = t.register_content[c-8];
		 																																`MATRIXCONFIG_tpEnableAnalog6 : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[6] = t.register_content[c-8];
		 																																`MATRIXCONFIG_tpEnableAnalog7 : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tpEnableAnalog[7] = t.register_content[c-8];
		 																																`MATRIXCONFIG_tuningDAC00     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC0[0]     = t.register_content[c-8];
		 																																`MATRIXCONFIG_tuningDAC01     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC0[1]     = t.register_content[c-8];
		 																																`MATRIXCONFIG_tuningDAC02     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC0[2]     = t.register_content[c-8];
		 																																`MATRIXCONFIG_tuningDAC10     : for (int c = 8; c < `COLUMNS; c++) begin configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC1[0]     = t.register_content[c-8];
																																			                                //$display("DISPLAY matrix model -half 1- tuningDAC1[0] col%0d ss%0d sis%0d = %b",
																																																																			//c,superpixel_segment_counter,superpixel_in_segment_counter,configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC1[0]);
																																																																			end
																													endcase
																									end
																									else if ( chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_SECOND_HALF ) begin
																									    case (bit_counter)
																													      `MATRIXCONFIG_tuningDAC11     : for (int c = 8; c < `COLUMNS; c++) begin configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC1[1] = t.register_content[c-8];
																																			                                //$display("DISPLAY matrix model -half 2- tuningDAC1[1] col%0d ss%0d sis%0d = %b",c,superpixel_segment_counter,superpixel_in_segment_counter,
																																																																			//configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC1[1]);
																																																																			end																																			           
	 																																	`MATRIXCONFIG_tuningDAC12     : for (int c = 8; c < `COLUMNS; c++) begin configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC1[2] = t.register_content[c-8];
																																				                               //$display("DISPLAY matrix model -half 2- tuningDAC1[2] col%0d ss%0d sis%0d = %b",c,superpixel_segment_counter,superpixel_in_segment_counter,
																																																																			//configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC1[2]);
																																																																			end																																		
	 																																	`MATRIXCONFIG_tuningDAC20     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC2[0] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC21     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC2[1] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC22     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC2[2] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC30     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC3[0] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC31     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC3[1] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC32     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC3[2] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC40     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC4[0] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC41     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC4[1] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC42     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC4[2] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC50     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC5[0] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC51     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC5[1] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC52     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC5[2] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC60     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC6[0] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC61     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC6[1] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC62     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC6[2] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC70     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC7[0] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC71     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC7[1] = t.register_content[c-8];
	 																																	`MATRIXCONFIG_tuningDAC72     : for (int c = 8; c < `COLUMNS; c++) configured_data[c].superpixel[superpixel_segment_counter][superpixel_in_segment_counter].tuningDAC7[2] = t.register_content[c-8];
																																			`MATRIXCONFIG_dummy_bit_0_secondhalf : for (int c = 8; c < `COLUMNS; c++) dummybit_2ndconfighalf_bit0[c][superpixel_segment_counter][superpixel_in_segment_counter] = t.register_content[c-8];
																													endcase
																									end
																									
																									
																									
								             				
								             				if (reporting_enable_obj.enable_reporting_matrixmodel == `YES) begin
								                 				`uvm_info("Config sequence - matrix_model", $sformatf("Configuration ongoing, config_word = %h, bit_counter = %d, superpixel_in_segment_counter = %d, superpixel_segment_counter = %d, chip_status = %s",
                                  				config_word, bit_counter, superpixel_in_segment_counter, superpixel_segment_counter, chip_status_obj.chip_status), UVM_LOW) 
														 				      end

								             				if(matrix_row < `NUMBER_OF_FF_PER_SUPERPIXEL*`SUPERPIXELS_IN_ONE_SEGMENT*`SUPERPIXEL_SEGMENTS - 1) matrix_row = matrix_row + 1;
								             				else matrix_row = 0; // bottom row (superpixel 0), which is translated into the MSB of the column_content

								             				if(bit_counter > 0) bit_counter = bit_counter - 1;
														 			      	else begin
																  				        bit_counter = `NUMBER_OF_FF_PER_SUPERPIXEL - 1; 
																  				        if (superpixel_in_segment_counter < `SUPERPIXELS_IN_ONE_SEGMENT - 1) superpixel_in_segment_counter = superpixel_in_segment_counter + 1;
																  				        else begin
																       				        superpixel_in_segment_counter = 0;
																       				        if(superpixel_segment_counter < `SUPERPIXEL_SEGMENTS - 1) superpixel_segment_counter = superpixel_segment_counter + 1;
																       			        	else begin   
								                            				superpixel_segment_counter = 0;
																																								
																																								//$display("DISPLAY matrix model - tuningDAC1 col0 ss0 sis0 = %h",configured_data[0].superpixel[0][0].tuningDAC1);
								                            				if (chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_SECOND_HALF) begin
																				        																// At this point, we have all the configuration bits for all the superpixels in all of the columns
																				        																// -> send the transaction corresponding to the expected pin values to pins_model
																																												convert_from_bits_to_pins_and_send();
																				        																`uvm_info("Matrix model sent", $sformatf("Transaction sent to pins_model"), UVM_LOW)
																       			        	    end
																																							// -> send the transactions corresponding to the expected DATA_OUT to dataout_model via the devoted FIFO
																            										 convert_from_bits_to_dataout_and_send();
																  			       	      end
														 				           end
																								end
															end: second_configdata

 
      end: status_matrix_config
           
  endfunction: write
  
endclass: CLICTD_matrix_model






`endif // __CLICTD_matrix_model_sv__
