// Filename           : CLICTD_pins_model.sv
// Author             : Núria Egidos 
// Created on         : 6/6/18
// Last modification  : 22/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Golden model or predictor corresponding to the value of the different signals in pins_interface

`ifndef __CLICTD_pins_model_sv__
`define __CLICTD_pins_model_sv__

//`uvm_analysis_imp_decl(_matrix)
//`uvm_analysis_imp_decl(_in)

class CLICTD_pins_model extends uvm_component; 

	`uvm_component_utils(CLICTD_pins_model) // Register the component to the factory

  // Declare the analysis port to provide the "ideal outputs" to the comparator
	uvm_analysis_port#(CLICTD_pins_transaction) analysis_port; 

  // Export to retrieve transactions from the pins_agent; the content of these transactions
  // is not used, but their arrival is used for synchronisation purposes. When one of these 
  // transactions arrives (when reset is released, every time a column content is read out...),
  // a transaction containing the expected value of the pins in that occasion is sent to   
  // pins_comparator
  //uvm_analysis_export#(CLICTD_pins_transaction) analysis_export_in;
  // The component that implements the write function uses a uvm_analysis_imp (subscriber, this model...); the component that 
  // connects an export in the parent component (scoreboard...) to the export that implements the write function uses a uvm_analysis_export
  uvm_analysis_imp_in#(CLICTD_pins_transaction, CLICTD_pins_model) analysis_export_in;

  // Export to retrieve transactions from matrix_model. These transactions contain the expected
  // value for the pins in STATUS_MATRIX_CONFIG_FIRST/SECOND_HALF...
  //uvm_analysis_export#(CLICTD_pins_transaction) analysis_export_matrix;
  // The component that implements the write function uses a uvm_analysis_imp (subscriber, this model...); the component that 
  // connects an export in the parent component (scoreboard...) to the export that implements the write function uses a uvm_analysis_export
  uvm_analysis_imp_matrix#(CLICTD_pins_transaction, CLICTD_pins_model) analysis_export_matrix;

  // We need an analysis export to probe the CLICTD_pins_transaction transactions sent by the
  // pinsinterface_agent, for instance, to know when the reset is applied or released.
  // In the case of CLICTD_pinsinterface_model, it already counts on the built-in analysis 
  // export of uvm_subscriber which, in this case, handles CLICTD_pins_transaction transactions.
  // However, we will declare explicitely another of such analysis exports in order to mantain
  // the same structure as the rest of golden models, which also have a devoted analysis export
  // for this purpose (otherwhise they wouldn't be able to capture CLICTD_pins_transaction transactions).
  // THIS IS CONNECTED IN THE SCOREBOARD!!
  //uvm_analysis_export#(CLICTD_pins_transaction) model_analysisexport;  

  // NOTE: WE USE THE IMPLICIT ANALYSIS_EXPORT OF UVM_SUBSCRIBER, otherwise, we see an error in the
  // port-export connection:
  // # UVM_ERROR @ 422000: uvm_test_top.env.pins_scoreboard.pins_model.model_analysisexport [Connection Error] connection count of 0 does not meet   required minimum of 1
  // # UVM_ERROR @ 422000: uvm_test_top.env.pins_scoreboard.scoreboard_analysisexport_in [Connection Error] connection count of 0 does not meet required minimum of 1



  CLICTD_pins_transaction auxiliary_pins_transaction_from_matrix;

  ChipStatusInfo chip_status_obj;
  CLICTDevents events_obj;
		FieldsConfigureMatrix config_matrix_obj;

  bit rise_pwren_pad_event_arrived, fall_pwren_pad_event_arrived, rise_pwrInt_event_arrived, fall_pwrInt_event_arrived, rise_powerInternal_event_arrived, fall_powerInternal_event_arrived, rise_tpulse_pad_event_arrived, fall_tpulse_pad_event_arrived,
		    rise_testpulseIntStrobe_event_arrived, fall_testpulseIntStrobe_event_arrived; 

 //`uvm_component_utils_begin( CLICTD_pins_model )
 //     `uvm_field_object( chip_status_obj, UVM_REFERENCE )
 // `uvm_component_utils_end



// FROM THE EVENTS, DECIDE WHICH IS THE EXPECTED VALUE
// WHEN THE TRANSACTION ARRIVES, ASSIGN THE EXPECTED VALUE TO THE TRANSACTION FOR THE COMPARATOR


  // Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

  // Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
    // Create the analysis port to provide the "ideal outputs" to the scoreboard
		analysis_port = new(.name("analysis_port"), .parent(this));
    // Create the analysis export to probe some signals from pins_interface
    //model_analysisexport = new(.name("model_analysisexport"), .parent(this));

    //analysis_export_in = new(.name("analysis_export_in"), .parent(this));
    //analysis_export_matrix = new(.name("analysis_export_matrix"), .parent(this));
    analysis_export_in = new(.name("analysis_export_in"), .imp(this));
    analysis_export_matrix = new(.name("analysis_export_matrix"), .imp(this));


    // Retrieve the chip status from the database; it is set (and changed as the test progresses) from the virtual sequence 
    // in its body() task, so by this point it should be available to be retrieved
    if( !uvm_config_db#(ChipStatusInfo)::get(this, "", "chip_status_obj", chip_status_obj) )
       `uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )

    if( !uvm_config_db#(CLICTDevents)::get(this, "", "events_obj", events_obj) )
         `uvm_error( `NOCLICTDEVENTS, $sformatf("CLICTDevents not found in database for %s", this.get_full_name()) )
									
				if( !uvm_config_db#(FieldsConfigureMatrix)::get(this, "", "config_matrix_obj", config_matrix_obj) )
         `uvm_error( `NOMATRIXCONFIG, $sformatf("config_matrix_obj not found in database for %s", this.get_full_name()) )

    //auxiliary_pins_transaction_from_matrix = CLICTD_pins_transaction::type_id::create(.name("auxiliary_pins_transaction_from_matrix"), .contxt(get_full_name()));

	endfunction: build_phase


  // - - - - - - - - - - - - Run phase: here take place the test actions
	task run_phase(uvm_phase phase);

       fork      
           action_status_powerpulse_external();
           action_status_powerpulse_internal();
											action_status_testpulse_external();
           action_status_testpulse_internal();
											action_timeout_discTp();
       join_none
       // The fork join_none is used to avoid that this forever begin blocks the execution of other 
       // run_phase tasks in the hierarchy

  endtask: run_phase

  

  // Implementation of the write() function, which sends transactions (ideal outputs) to the scoreboard
  // This function is called every time the pins_agent sends a transaction to the pins_scoreboard
	function void write_in(CLICTD_pins_transaction t); // t is used to match the declaration of pure virtual function write in uvm_subscriber
		// the predicted output is obtained by adding the monitored inputs

    CLICTD_pins_transaction auxiliary_pins_transaction; //= CLICTD_pins_transaction::type_id::create(.name("auxiliary_pins_transaction"), .contxt(get_full_name()));

    // Clone the transaction t into another transaction, so as not to change the transaction sent to the scoreboard
  
		
		  //$display("%t DISPLAY - pins model - transaction received from pins monitor - powerEnable = %b, counter clk 100 gated = %0d, chip status %s",$realtime,t.powerEnable,t.counter_clk100gated_while_powerEnable_is_low, chip_status_obj.chip_status);
    

    if(chip_status_obj.chip_status == STATUS_RESET) begin
            //auxiliary_pins_transaction = CLICTD_pins_transaction::type_id::create(.name("auxiliary_pins_transaction"), .contxt(get_full_name()));
            $cast(auxiliary_pins_transaction, t.clone());
												assert_isunknown_pins_transaction ( .pins_transaction(auxiliary_pins_transaction), .chip_status_obj(chip_status_obj), .name_of_transaction("auxiliary_pins_transaction"), .name_of_calling_component(this.get_full_name()) );
            //$cast(auxiliary_pins_transaction, t.clone()); // we clone the transaction so that the rest of fields, which are not checked for this test,
            // are the same for both transactions reaching the comparator and thus no "non-relevant" miscomparison occurs 
            //auxiliary_pins_transaction = CLICTD_pins_transaction::type_id::create(.name("auxiliary_pins_transaction"), .contxt(get_full_name()));
        		  auxiliary_pins_transaction.columnDone  = {`COLUMNS{1'b0}};
            auxiliary_pins_transaction.dataIn      = {`COLUMNS{1'b0}};
            auxiliary_pins_transaction.commonToken = {`COLUMNS{1'b0}};
            `uvm_info("Pins model sent", $sformatf("Transaction sent to pins_comparator, chip_status = %s, columnDone = %h, dataIn = %h, commonToken = %h", chip_status_obj.chip_status,
            auxiliary_pins_transaction.columnDone,auxiliary_pins_transaction.dataIn,auxiliary_pins_transaction.commonToken),  UVM_LOW);
            analysis_port.write(auxiliary_pins_transaction);
		  end
    else if ( chip_status_obj.chip_status == STATUS_POWERPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS ) begin
            //auxiliary_pins_transaction = CLICTD_pins_transaction::type_id::create(.name("auxiliary_pins_transaction"), .contxt(get_full_name()));
            $cast(auxiliary_pins_transaction, t.clone()); // we clone the transaction so that the rest of fields, which are not checked for this test,
            // are the same for both transactions reaching the comparator and thus no "non-relevant" miscomparison occurs 
            assert_isunknown_pins_transaction ( .pins_transaction(auxiliary_pins_transaction), .chip_status_obj(chip_status_obj), .name_of_transaction("auxiliary_pins_transaction"), .name_of_calling_component(this.get_full_name()) );
            // if PWREN_pad has risen, powerEnable should rise as well. Plus we free the rise_pwren_pad_event_arrived variable.
            if      (rise_pwren_pad_event_arrived == `YES) begin auxiliary_pins_transaction.powerEnable = 1'b1; rise_pwren_pad_event_arrived = `NO; end
            // if PWREN_pad has fallen, powerEnable should fall as well. Plus we free the fall_pwren_pad_event_arrived variable.
            else if (fall_pwren_pad_event_arrived == `YES) begin auxiliary_pins_transaction.powerEnable = 1'b0; fall_pwren_pad_event_arrived = `NO; end
            `uvm_info("Pins model sent", $sformatf("Transaction sent to pins_comparator, chip_status = %s, powerEnable = %h, counter CLK_100_GATED = %d", chip_status_obj.chip_status,
            auxiliary_pins_transaction.powerEnable,auxiliary_pins_transaction.counter_clk100gated_while_powerEnable_is_low),  UVM_LOW);
            analysis_port.write(auxiliary_pins_transaction);

    end
    else if ( chip_status_obj.chip_status == STATUS_POWERPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS ) begin
            //auxiliary_pins_transaction = CLICTD_pins_transaction::type_id::create(.name("auxiliary_pins_transaction"), .contxt(get_full_name()));
            $cast(auxiliary_pins_transaction, t.clone());
            //$cast(auxiliary_pins_transaction, t.clone()); // we clone the transaction so that the rest of fields, which are not checked for this test,
            // are the same for both transactions reaching the comparator and thus no "non-relevant" miscomparison occurs 
            assert_isunknown_pins_transaction ( .pins_transaction(auxiliary_pins_transaction), .chip_status_obj(chip_status_obj), .name_of_transaction("auxiliary_pins_transaction"), .name_of_calling_component(this.get_full_name()) );
            // if pwrInt has risen, powerEnable should rise as well. Plus we free the rise_pwrInt_event_arrived variable.
            //if      (rise_pwrInt_event_arrived == `YES) begin auxiliary_pins_transaction.powerEnable = 1'b1; //rise_pwrInt_event_arrived = `NO; 
            //         end
            // if pwrInt has fallen, powerEnable should fall as well. Plus we free the fall_pwrInt_event_arrived variable.
            //else if (fall_pwrInt_event_arrived == `YES) begin auxiliary_pins_transaction.powerEnable = 1'b0; //fall_pwrInt_event_arrived = `NO; 
           //         end


            if      (rise_powerInternal_event_arrived == `YES) begin auxiliary_pins_transaction.powerEnable = 1'b1; rise_powerInternal_event_arrived = `NO; 
                     end
            // if pwrInt has fallen, powerEnable should fall as well. Plus we free the fall_pwrInt_event_arrived variable.
            else if (fall_powerInternal_event_arrived == `YES) begin auxiliary_pins_transaction.powerEnable = 1'b0; fall_powerInternal_event_arrived = `NO; 
                    end

            `uvm_info("Pins model sent", $sformatf("Transaction sent to pins_comparator, chip_status = %s, powerEnable = %h, counter CLK_100_GATED = %d", chip_status_obj.chip_status,
            auxiliary_pins_transaction.powerEnable,auxiliary_pins_transaction.counter_clk100gated_while_powerEnable_is_low),  UVM_LOW);
            analysis_port.write(auxiliary_pins_transaction);

    end
				
				
				// discTp reflects when a test pulse has been applied, either from TPULSE_pad or from the slow control
				else if ( chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS ) begin
            $cast(auxiliary_pins_transaction, t.clone()); // we clone the transaction so that the rest of fields, which are not checked for this test,
            // are the same for both transactions reaching the comparator and thus no "non-relevant" miscomparison occurs 
            assert_isunknown_pins_transaction ( .pins_transaction(auxiliary_pins_transaction), .chip_status_obj(chip_status_obj), .name_of_transaction("auxiliary_pins_transaction"), .name_of_calling_component(this.get_full_name()) );
            // if TPULSE_pad has risen, discTp should rise as well. Plus we free the rise_tpulse_pad_event_arrived variable.
            if      (rise_tpulse_pad_event_arrived == `YES) begin
												         // In this test, we are only enabling digital test pulse in one superpixel, and that's the only kind of pulsing we apply, so the discTp bit that is expected to rise is this one
												         auxiliary_pins_transaction.discTp[config_matrix_obj.columns_to_pulse[0]][config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse] = 1'b1; rise_tpulse_pad_event_arrived = `NO; 
													end
            // if TPULSE_pad has fallen, discTp should fall as well. Plus we free the fall_tpulse_pad_event_arrived variable.
            else if (fall_tpulse_pad_event_arrived == `YES) begin 
												        auxiliary_pins_transaction.discTp[config_matrix_obj.columns_to_pulse[0]][config_matrix_obj.ss_applydigitalpulse][config_matrix_obj.sis_applydigitalpulse] = 1'b0; fall_tpulse_pad_event_arrived = `NO; 
												end

            `uvm_info("Pins model sent", $sformatf("Transaction sent to pins_comparator, chip_status = %s, discTp = %b", chip_status_obj.chip_status,auxiliary_pins_transaction.discTp),  UVM_LOW);
            analysis_port.write(auxiliary_pins_transaction);

    end
    else if ( chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS ) begin
            $cast(auxiliary_pins_transaction, t.clone()); // we clone the transaction so that the rest of fields, which are not checked for this test,
            // are the same for both transactions reaching the comparator and thus no "non-relevant" miscomparison occurs 
            assert_isunknown_pins_transaction ( .pins_transaction(auxiliary_pins_transaction), .chip_status_obj(chip_status_obj), .name_of_transaction("auxiliary_pins_transaction"), .name_of_calling_component(this.get_full_name()) );
            
												// if testpulseIntStrobe has fallen, discTp should fall as well. Plus we free the fall_testpulseIntStrobe_event_arrived variable.
            if      (rise_testpulseIntStrobe_event_arrived == `YES) begin auxiliary_pins_transaction.discTp = 1'b1; rise_testpulseIntStrobe_event_arrived = `NO; 
                     end
            // if testpulseIntStrobe has fallen, discTp should fall as well. Plus we free the fall_testpulseIntStrobe_event_arrived variable.
            else if (fall_testpulseIntStrobe_event_arrived == `YES) begin auxiliary_pins_transaction.discTp = 1'b0; fall_testpulseIntStrobe_event_arrived = `NO; 
                    end

            `uvm_info("Pins model sent", $sformatf("Transaction sent to pins_comparator, chip_status = %s, discTp = %h", chip_status_obj.chip_status, auxiliary_pins_transaction.discTp),  UVM_LOW);
            analysis_port.write(auxiliary_pins_transaction);

    end
				
				
    //else if ( chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_SECOND_HALF ) begin
				else if ( chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF ) begin // at this state, we can be sure that both configuration halves have been completed
            //auxiliary_pins_transaction = CLICTD_pins_transaction::type_id::create(.name("auxiliary_pins_transaction"), .contxt(get_full_name()));
            $cast(auxiliary_pins_transaction, auxiliary_pins_transaction_from_matrix.clone());
												assert_isunknown_pins_transaction ( .pins_transaction(auxiliary_pins_transaction), .chip_status_obj(chip_status_obj), .name_of_transaction("auxiliary_pins_transaction"), .name_of_calling_component(this.get_full_name()) );
            // for other chip_status, auxiliary_pins_transaction is filled from matrix_model
            //$cast(auxiliary_pins_transaction, auxiliary_pins_transaction_from_matrix.clone());
            // The value of columnDone, dataIn, commonToken provided by matrix_model may not reflect the current value of these signals,
												// because they're not provided by the golden model. Since they only matter in STATUS_RESET, for the rest of states we can 
												// simply adopt the values of the incoming transaction to avoid comparison mistakes
												auxiliary_pins_transaction.PWREN_pad = t.PWREN_pad;     
   									auxiliary_pins_transaction.TPULSE_pad = t.TPULSE_pad;   
   									auxiliary_pins_transaction.READOUT_pad = t.READOUT_pad;	
   									auxiliary_pins_transaction.SHUTTER_pad = t.SHUTTER_pad;  
   									auxiliary_pins_transaction.RSTN_pad = t.RSTN_pad;
												auxiliary_pins_transaction.ENABLE_OUT_pad = t.ENABLE_OUT_pad;
												auxiliary_pins_transaction.columnDone = t.columnDone;
												auxiliary_pins_transaction.dataIn = t.dataIn;
												auxiliary_pins_transaction.commonToken = t.commonToken;
												auxiliary_pins_transaction.readoutStartExtEn = t.readoutStartExtEn; 
  										auxiliary_pins_transaction.readoutStart = t.readoutStart;
  										auxiliary_pins_transaction.testPulseExtEnable = t.testPulseExtEnable; 
  										auxiliary_pins_transaction.testPulse = t.testPulse;         
												auxiliary_pins_transaction.testPulseIntStrobe = t.testPulseIntStrobe; 
  										auxiliary_pins_transaction.powerExtEnable = t.powerExtEnable; 
            auxiliary_pins_transaction.powerEnable = t.powerEnable;
												auxiliary_pins_transaction.CLK_100_GATED = t.CLK_100_GATED;
            auxiliary_pins_transaction.counter_clk100gated_while_powerEnable_is_low = t.counter_clk100gated_while_powerEnable_is_low;
												auxiliary_pins_transaction.disc = t.disc;
												auxiliary_pins_transaction.discTp = t.discTp;
	
            `uvm_info("Pins model sent", $sformatf("Transaction sent to pins_comparator, %s", chip_status_obj.chip_status),  UVM_LOW);
            analysis_port.write(auxiliary_pins_transaction);
												/*
												for (int c = 0; c < `COLUMNS; c++) begin
																for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						    										for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																									$display("DISPLAY - pins model - c %0d, ss %0d, sis %0d, EXPECTED: tpEnableDigital = %b, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
                          c, ss,sis,
																										auxiliary_pins_transaction.tpEnableDigital[c][ss][sis], auxiliary_pins_transaction.mask[c][ss][sis], auxiliary_pins_transaction.tpEnableAnalog[c][ss][sis], auxiliary_pins_transaction.tuningDAC0[c][ss][sis],
																										auxiliary_pins_transaction.tuningDAC1[c][ss][sis], auxiliary_pins_transaction.tuningDAC2[c][ss][sis], auxiliary_pins_transaction.tuningDAC3[c][ss][sis], auxiliary_pins_transaction.tuningDAC4[c][ss][sis],
																										auxiliary_pins_transaction.tuningDAC5[c][ss][sis], auxiliary_pins_transaction.tuningDAC6[c][ss][sis], auxiliary_pins_transaction.tuningDAC7[c][ss][sis]
																										);
																				end
																end
											end
												*/				
    end
				
				/*
    else if ( chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS ) begin
            $cast(auxiliary_pins_transaction, t.clone());
            heeere

            if      ( chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_WORKING && rise_testPulseIntStrobe_event_arrived == `YES && config_matrix_obj.tpE) begin auxiliary_pins_transaction.powerEnable = 1'b1; rise_powerInternal_event_arrived = `NO; 
                     end
            else if (fall_powerInternal_event_arrived == `YES) begin auxiliary_pins_transaction.powerEnable = 1'b0; fall_powerInternal_event_arrived = `NO; 
                    end

            `uvm_info("Pins model sent", $sformatf("Transaction sent to pins_comparator, chip_status = %s, discTp = %b", chip_status_obj.chip_status,
            auxiliary_pins_transaction.discTp,auxiliary_pins_transaction.counter_clk100gated_while_powerEnable_is_low),  UVM_LOW);
            analysis_port.write(auxiliary_pins_transaction);

    end
    */
    

    
 
	endfunction: write_in


  function void write_matrix(CLICTD_pins_transaction t); // t is used to match the declaration of pure virtual function write in uvm_subscriber
		// the predicted output is obtained by adding the monitored inputs
    `uvm_info("Pins model received", $sformatf("Transaction received from matrix_model, chip_status = %s", chip_status_obj.chip_status),  UVM_LOW);
    $cast(auxiliary_pins_transaction_from_matrix, t.clone());
				assert_isunknown_pins_transaction ( .pins_transaction(auxiliary_pins_transaction_from_matrix), .chip_status_obj(chip_status_obj), .name_of_transaction("auxiliary_pins_transaction_from_matrix"), .name_of_calling_component(this.get_full_name()) );
	 endfunction: write_matrix

  // Task to wait the timeout when powerEnable is not updated and report consequently to the comparator
  task action_timeout_powerEnable();
      // Once there has been an update at PWREN_pad (STATUS_POWERPULSE_EXTERNAL_WORKING or STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS)
      // or pwrInt (STATUS_POWERPULSE_INTERNAL_WORKING or STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS) we expect an update at powerEnable. 
      // If such update occurs (we will decide if it's updated to the correct value at the comparator), it is reported to the comparator 
      // when the write_in function is called from the agent. If no update occurs, a timeout is waited and when it expires, 
      // the lack of update is reported to the comparator.
      fork
           // Detect rising edge at powerEnable
           begin events_obj.rise_powerEnable_event.wait_trigger(); //events_obj.rise_powerEnable_event.reset(); //$display("%t, pins model, rise_powerEnable_event", $realtime); 
           end 
           // Detect falling edge at powerEnable
           begin events_obj.fall_powerEnable_event.wait_trigger(); //events_obj.fall_powerEnable_event.reset(); //$display("%t, pins model, fall_powerEnable_event", $realtime); 
           end  
           // When no update occurs at powerEnable, wait the timeout and report when completed
           begin #(`TIMEOUT_UPDATE_POWERENABLE); events_obj.timeout_update_powerenable_event.trigger(); //$display("%t, pins model, timeout_update_powerenable_event", $realtime);      
           end
      join_any
      disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
      // once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
      // which could otherwise lead to unexpected behavior (messages printing twice...)
  endtask: action_timeout_powerEnable
		
		
		// Task to wait the timeout when discTp is not updated (and some test pulse was applied) and report consequently to the comparator
  task action_timeout_discTp();
      
						forever begin
						        // Detect an update in chip status
														chip_status_obj.chip_status_update_event.wait_trigger();
														if ( chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS || chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_WORKING ||
														     chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS ) begin
																			// At this point, check if a timeout expires when waiting for an update in discTp
																			fork
           								// Detect rising edge at discTp
           								begin  events_obj.rise_discTp_event.wait_trigger();  
           								end  
           								// When no update occurs at discTp, wait the timeout and report when completed
           								begin #(`TIMEOUT_UPDATE_DISCTP_WHEN_TESTPULSE_IS_APPLIED); events_obj.timeout_update_discTp_event.trigger(); //$display("%t, pins model, timeout_update_powerenable_event", $realtime);      
           								end
      													join_any
      													disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
      													// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
      													// which could otherwise lead to unexpected behavior (messages printing twice...)
														end					
						end
      
  endtask: action_timeout_discTp
		
		
		

  // Actions to perform at STATUS_POWERPULSE_EXTERNAL_WORKING or STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS
  task action_status_powerpulse_external();

       forever begin 
       
             //chip_status_obj.chip_status_update_event.wait_trigger(); // this avoids that the forever begin hogs the processor
             //$display("%t, pins model, detected update chip status = %s", $realtime,chip_status_obj.chip_status);
             //if ( chip_status_obj.chip_status == STATUS_POWERPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS ) begin
                  fork
                      // These events are triggered from pins_monitor
                      begin events_obj.rise_pwren_pad_event.wait_trigger(); //events_obj.rise_pwren_pad_event.reset(); 
														              rise_pwren_pad_event_arrived = `YES; end
                      begin events_obj.fall_pwren_pad_event.wait_trigger(); //events_obj.fall_pwren_pad_event.reset(); 
														              fall_pwren_pad_event_arrived = `YES; end
                  join_any
                  disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
									// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
									// which could otherwise lead to unexpected behavior (messages printing twice...)
                  //$display("%t, pins model, rise_pwren_pad_event_arrived = %d, fall_pwren_pad_event_arrived = %d", $realtime,rise_pwren_pad_event_arrived,fall_pwren_pad_event_arrived);
                  // Once there has been an update at PWREN_pad, we expect an update at powerEnable. If such update occurs (we will decide if it's updated to the correct value
                  // at the comparator), it is reported to the comparator when the write_in function is called from the agent. If no update occurs, a timeout is waited and when
                  // it expires, the lack of update is reported to the comparator.
                  action_timeout_powerEnable();
                  //$display("%t, pins model, action timeout ended", $realtime);
              //end
       end
  endtask: action_status_powerpulse_external

  // Actions to perform at STATUS_POWERPULSE_INTERNAL_WORKING or STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS
  task action_status_powerpulse_internal();
       forever begin
              //$display("%t, pins model, rise_pwrInt_event_arrived = %d, fall_pwrInt_event_arrived = %d", $realtime,rise_pwrInt_event_arrived,fall_pwrInt_event_arrived);
              //use some variable like "readout ongoing" used in dataout monitor to regulate the call to breakdown readout bits?
              //chip_status_obj.chip_status_update_event.wait_trigger(); // this avoids that the forever begin hogs the processor
              //if ( chip_status_obj.chip_status == STATUS_POWERPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS ) begin
                 //$display("%t, pins model 2, rise_pwrInt_event_arrived = %d, fall_pwrInt_event_arrived = %d", $realtime,rise_pwrInt_event_arrived,fall_pwrInt_event_arrived);
                  fork
                      // These events are triggered from matrix_model
                      begin events_obj.rise_powerInternal_event.wait_trigger(); rise_powerInternal_event_arrived = `YES; 
													// events_obj.rise_pwrInt_event.wait_trigger(); events_obj.rise_pwrInt_event.reset(); rise_pwrInt_event_arrived = `YES; #200; rise_pwrInt_event_arrived = `NO;
                           //$display("%t, pins model 2, rise_pwrInt_event_arrived = %d, fall_pwrInt_event_arrived = %d", $realtime,rise_pwrInt_event_arrived,fall_pwrInt_event_arrived); 
                      end
                      begin events_obj.fall_powerInternal_event.wait_trigger(); fall_powerInternal_event_arrived = `YES;
													// events_obj.fall_pwrInt_event.wait_trigger(); events_obj.fall_pwrInt_event.reset(); fall_pwrInt_event_arrived = `YES; #200; fall_pwrInt_event_arrived = `NO;
                          //$display("%t, pins model 3, rise_pwrInt_event_arrived = %d, fall_pwrInt_event_arrived = %d", $realtime,rise_pwrInt_event_arrived,fall_pwrInt_event_arrived); 
                      end
                      // Note: the 20000 ns delay accounts for the time measured in simulation that the change in pwrInt (once pwrInt has changed on chip) takes to propagate until powerEnable
                  join_any
                  disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
									// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
									// which could otherwise lead to unexpected behavior (messages printing twice...)
                  // Once there has been an update at pwrInt, we expect an update at powerEnable. If such update occurs (we will decide if it's updated to the correct value
                  // at the comparator), it is reported to the comparator when the write_in function is called from the agent. If no update occurs, a timeout is waited and when
                  // it expires, the lack of update is reported to the comparator.

                  // It was seen in the simulation that rise_powerInternal_event_arrived, fall_powerInternal_event_arrived changed delayed with respect to powerEnable, instead of 
                  // simultaneously, and this lead to generating transactions with the wrong powerEnable value. To avoid this issue, powerInternal is used instead.
                  action_timeout_powerEnable();
              //end
       end
  endtask: action_status_powerpulse_internal
		
		
		
		
		// Actions to perform at STATUS_TESTPULSE_EXTERNAL_WORKING or STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS
  task action_status_testpulse_external();

       forever begin 
       
             fork
                 begin events_obj.rise_tpulse_pad_event.wait_trigger(); rise_tpulse_pad_event_arrived = `YES; end
                 begin events_obj.fall_tpulse_pad_event.wait_trigger(); fall_tpulse_pad_event_arrived = `YES; end
             join_any
             disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
													// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
													// which could otherwise lead to unexpected behavior (messages printing twice...)
                  
       end
  endtask: action_status_testpulse_external

  // Actions to perform at STATUS_TESTPULSE_INTERNAL_WORKING or STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS
  task action_status_testpulse_internal();
       forever begin
              
                  fork
                      // These events are triggered from matrix_model
                      begin events_obj.rise_testpulseIntStrobe_event.wait_trigger(); rise_testpulseIntStrobe_event_arrived = `YES; 
													
                      end
                      begin events_obj.fall_testpulseIntStrobe_event.wait_trigger(); fall_testpulseIntStrobe_event_arrived = `YES;
					
                      end
                  join_any
                  disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
									// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
									// which could otherwise lead to unexpected behavior (messages printing twice...)
                  
       end
  endtask: action_status_testpulse_internal
		
		


endclass: CLICTD_pins_model






`endif // __CLICTD_pins_model_sv__
