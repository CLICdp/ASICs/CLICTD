// Filename           : CLICTD_slowcontrol_agent.sv
// Author             : Núria Egidos 
// Created on         : 26/7/18
// Last modification  : 27/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Agent - this version doesn't use QVIP, a workaround version of clictdTop
//                      (SDA_pad bypassed, I2C signals from slowControl propagated as output ports
//                      of clictdTop) is used 
// Derived from       : analysis_master_agent.sv by Elia Conti and Sara Marconi for VEPIX53

`ifndef __CLICTD_slowcontrol_agent_sv__
`define __CLICTD_slowcontrol_agent_sv__




// In this version of the slowcontrol_agent there is no monitor nor scoreboard in the environment, 
// so neither the monitor nor the port to connect to the scoreboard will be created 


class CLICTD_slowcontrol_agent extends CLICTD_agent;
  `uvm_component_utils( CLICTD_slowcontrol_agent ) // Register the component to the factory
	//
  // Declaration of all possible components. In the build_phase, we will
  // only create those indicated by the configuration object
  CLICTD_slowcontrol_driver driver;
  CLICTD_slowcontrol_sequencer sequencer;
  //CLICTD_funccoverage_collector funccoverage_collector;



	// Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new


	// Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase); // here it's checked whether the virtual interface handles and configuration object exist
    
		
    // Create the driver
    driver	= CLICTD_slowcontrol_driver::type_id::create(.name("slowcontrol_driver"), .parent(this));

    // Create the sequencer
    sequencer	= CLICTD_slowcontrol_sequencer::type_id::create(.name("slowcontrol_sequencer"), .parent(this));

	endfunction: build_phase


   // Connect phase: connect ports and exports
	function void connect_phase(uvm_phase phase);
		super.connect_phase(phase);
    //
    // If the agent is active, connect the driver 
    // seq_item_port to the sequencer seq_item_export 
    driver.seq_item_port.connect(sequencer.seq_item_export);
		
    //
	endfunction: connect_phase

endclass: CLICTD_slowcontrol_agent

`endif // __CLICTD_slowcontrol_agent_sv__

