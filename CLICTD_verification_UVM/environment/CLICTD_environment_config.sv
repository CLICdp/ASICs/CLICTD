// Filename           : CLICTD_environment_config.sv
// Author             : Núria Egidos 
// Created on         : 1/6/18
// Last modification  : 1/6/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Configuration object for the environment
//											Information for the environment: which agents and scoreboards to instantiate, 
//											whether to instantiate a cross-coverage collector or not 


// Associative arrays: http://electrosofts.com/systemverilog/arrays.html


`ifndef __CLICTD_environment_config_sv__
`define __CLICTD_environment_config_sv__


class CLICTD_environment_config extends uvm_object;
  `uvm_object_utils( CLICTD_environment_config ) // Register the object to the factory

  // Knobs used to enable or disable the instantiation of components within the environment
  bit instantiate_crosscoveragecollector;                 // Whether to instantiate a cross-coverage collector or not
  bit instantiate_virtualsequencer;                       // Whether to instantiate a virtual sequencer or not
  // These are unidimensional associative arrays (dynamic arrays for which we don't need to specify the size initially).
  // The positions in the arrays are created when filled and indexed with an unsigned int (0, 1, 2...), as indicated within brackets [].
  // We will use the exists() method to sort the content of the array and thus know which agents or scoreboards to instantiate.
  // http://www.asic-world.com/systemverilog/data_types13.html
  string agents_to_instantiate [string];      // Which agents to instantiate
  string scoreboards_to_instantiate [string]; // Which scoreboards to instantiate
 
  
  // Constructor
  function new(string name = "");
   // function new(string name = "", 
   //            bit instantiate_crosscoveragecollector_arg = `NO,
	//						 bit instantiate_virtualsequencer_arg = `NO,
  //             int agents_to_instantiate_arg [int] = '{0,0}, 
  //            int scoreboards_to_instantiate_arg [int] = '{0,0}
    //           );
		super.new(name);
		//instantiate_crosscoveragecollector = instantiate_crosscoveragecollector_arg;
		//instantiate_virtualsequencer = instantiate_virtualsequencer_arg;
		//agents_to_instantiate = agents_to_instantiate_arg;
    //scoreboards_to_instantiate = scoreboards_to_instantiate_arg;
	endfunction: new


endclass: CLICTD_environment_config


`endif // __CLICTD_environment_config_sv__
