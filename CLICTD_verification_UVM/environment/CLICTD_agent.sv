// Filename           : CLICTD_agent.sv
// Author             : Núria Egidos 
// Created on         : 11/6/18
// Last modification  : 11/6/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Agent
// Derived from       : analysis_master_agent.sv by Elia Conti and Sara Marconi for VEPIX53

`ifndef __CLICTD_agent_sv__
`define __CLICTD_agent_sv__

//import uvm_pkg::*;
//`include <uvm_macros.svh>

class CLICTD_agent extends uvm_agent;
  //`uvm_component_utils( CLICTD_agent ) // Register the component to the factory
	//
  
  // Name of the agent
  string agentname;

  // Virtual interface handles. Here we declare all possible interface handles,
  // in the build phase we create only those that will be used for this particular 
  // agent
  virtual CLICTD_slowcontrol_interface slowcontrol_vif;
  virtual CLICTD_readout_interface     readout_vif;
  virtual CLICTD_pins_interface        pins_vif;

  // Configuration object of the agent; this is created by the test and stored
  // in the configuration database, it is retrieved in the build phase of the agent
  CLICTD_agent_config agent_config;


	// Register the component to the factory
  `uvm_component_utils_begin(CLICTD_agent)
  		`uvm_field_object(agent_config, UVM_DEFAULT)
  `uvm_component_utils_end



	// Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
    agentname = name;
	endfunction: new


	// Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);

    // Check that the virtual interface handles have been set in the configuration database

    //assert(uvm_config_db#(virtual CLICTD_slowcontrol_interface)::get(this, get_full_name(), "slowcontrol_if", slowcontrol_vif))
    //else `uvm_error(get_full_name(), "Agent's virtual interface not configured")

    //assert(uvm_config_db#(virtual CLICTD_dataout_interface)::get(this, get_full_name(), "dataout_if", dataout_vif))
    //else `uvm_error(get_full_name(), "Agent's virtual interface not configured")

    //assert(uvm_config_db#(virtual CLICTD_pins_interface)::get(this, get_full_name(), "pins_if", pins_vif))
    //else `uvm_error(get_full_name(), "Agent's virtual interface not configured")

    // Check that the configuration object has been set in the database
  

    if( !uvm_config_db#(CLICTD_agent_config)::get(this, "", {agentname,"_config"}, agent_config) )
       `uvm_error( `NOCONFIG_AGENT, $sformatf("Agent configuration not found in database for %s", this.get_full_name()) )

    //assert( uvm_config_db#(agent_config)::get(this, "", {agentname,"_config"}, agent_config) ) 
    //else `uvm_error("NOCONFIG", {"CLICTD_agent_config not set for ", agentname}) // PENDING: check that this works!
       // https://verificationacademy.com/forums/systemverilog/how-concatenate-decimal-or-hex-values-string-name


    

	endfunction: build_phase

endclass: CLICTD_agent

`endif // __CLICTD_agent_sv__

