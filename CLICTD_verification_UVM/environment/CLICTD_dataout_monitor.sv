// Filename           : CLICTD_dataout_monitor.sv
// Author             : Núria Egidos 
// Created on         : 21/6/18
// Last modification  : 9/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Monitor to probe the signals in readout_interface

`ifndef __CLICTD_dataout_monitor_sv__
`define __CLICTD_dataout_monitor_sv__

//import uvm_pkg::*;
//`include <uvm_macros.svh>

// Include the parameter definition
//`include "CLICTD_parameters.sv" 

class CLICTD_dataout_monitor extends uvm_monitor;
	`uvm_component_utils(CLICTD_dataout_monitor) // Register the component to the factory


  string testname;
  
	// Declare the analysis port that will be connected to the agent's
  // analysis port and this, in turn, to the scoreboard's
  uvm_analysis_port#(CLICTD_dataout_transaction) analysis_port; 

  // Declare the transaction used to exchange information between the 
  // monitor and the scoreboard
	CLICTD_dataout_transaction dataout_transaction; 

  ChipStatusInfo chip_status_obj;
  CLICTDevents events_obj;
  ReportingEnable reporting_enable_obj;
		BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
		FieldsConfigureMatrix config_matrix_obj;
		FieldsConfigureMatrix_for_coverage config_matrix_obj_for_coverage;

  //uvm_event rise_enableoutpad_event;

  bit first_time_timeout_is_counted;
  int counter_clkout_riseedge;
  //real initial_time_to_start_counting_timeout;

  int bit_readout_counter = 0; // counter to store the position of the output stream that is being read out; 
  // this is used to keep track of the total bits read out
  int aux_bit_counter = 0; // counter to store the position of the output stream that is being read out; 
  // this is used to keep track of the bits read out in each of the readout stages (STATUS_START_HEADER, etc.)
  //int superpixel_counter_in_one_column = 0; // counter to store the position of superpixel (0-127) being read out in a given column
  int column_counter = 0; // counter to store the position of column (0-15) being read out 
  logic accumulated_readout_bits [int]; // associative array where the readout bits are dumped
  readout_stages_type readout_status;
  
  int aux; // auxiliary scalar
  
  
  bit print_this_time = `YES;

  bit readout_ongoing;
  int timeout_counter;
		
		// The value of these signals has to be maintained between calls to breakdown_readout_bits, so it can't be inside the task
		logic [`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED-1:0] window; // vector to store windows of `NUMBER_BITS_READOUT_PER_COLUMN_COMPRESSION_DISABLED bits to 
  // determine if this amount of bits read from DATA_OUT_pad correspond to a header
		logic [`NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED*`ROWS + `NUMBER_BITS_COLUMN_HEADER -1:0] accumulated_column_content; // maximum size that the column content can have
		

  `include "breakdown_readout_bits.sv"


  // Instance of readout_interface on which we are going to retrieve
  // the virtual interface handle from the factory
  virtual CLICTD_readout_interface readout_vif;
  // Instance of pins_interface that is used to probe RSTN_pad and know
  // when a reset is applied; it will also be retrieved from the factory
  virtual CLICTD_pins_interface pins_aux_vif;

  

  // - - - - - - - - - - - - Constructor
	function new(string name, uvm_component parent);
		super.new(name, parent);
	endfunction: new

  // Build phase: build the hierarchy
	function void build_phase(uvm_phase phase);
		super.build_phase(phase);
    // Create the monitor analysis port
		analysis_port = new(.name("analysis_port"), .parent(this));

    if( !uvm_config_db#(virtual CLICTD_readout_interface)::get(this, "", "readout_if", readout_vif) )
       `uvm_error( `NOVIF_READOUT, $sformatf("Readout virtual interface not found in database for %s", this.get_full_name()) )

    if( !uvm_config_db#(virtual CLICTD_pins_interface)::get(this, "", "pins_if", pins_aux_vif) )
       `uvm_error( `NOVIF_PINS, $sformatf("Pins virtual interface not found in database for %s", this.get_full_name()) )


    // Retrieve the chip status from the database; it is set (and changed as the test progresses) from the virtual sequence 
    // in its body() task, so by this point it should be available to be retrieved
    if( !uvm_config_db#(ChipStatusInfo)::get(this, "", "chip_status_obj", chip_status_obj) )
         `uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )



    if( !uvm_config_db#(CLICTDevents)::get(this, "", "events_obj", events_obj) )
         `uvm_error( `NOCLICTDEVENTS, $sformatf("CLICTDevents not found in database for %s", this.get_full_name()) )

    if( !uvm_config_db#(ReportingEnable)::get(this, "", "reporting_enable_obj", reporting_enable_obj) )
         `uvm_error( `NOREPORTINGENABLE, $sformatf("ReportingEnable not found in database for %s", this.get_full_name()) )
									
									
				if( !uvm_config_db#(BitsMonitorSlowcontrolReg)::get(this, "", "bits_slowcontrol_reg_obj", bits_slowcontrol_reg_obj) )
         `uvm_error( `NOBITSSLOWCONTROLREG, $sformatf("bits_slowcontrol_reg_obj not found in database for %s", this.get_full_name()) )
									
				if( !uvm_config_db#(FieldsConfigureMatrix)::get(this, "", "config_matrix_obj", config_matrix_obj) )
         `uvm_error( `NOMATRIXCONFIG, $sformatf("config_matrix_obj not found in database for %s", this.get_full_name()) )
									
									
				if( !uvm_config_db#(string)::get(this, "", "testname", testname) )
         `uvm_error( `NOTESTNAME, $sformatf("testname not found in database for %s", this.get_full_name()) )
									
				if (testname == `READOUT_TEST_FOR_COVERAGE) begin
				    if( !uvm_config_db#(FieldsConfigureMatrix_for_coverage)::get(this, "", "config_matrix_obj_for_coverage", config_matrix_obj_for_coverage) )
         `uvm_error( `NOMATRIXCONFIG, $sformatf("config_matrix_obj_for_coverage not found in database for %s", this.get_full_name()) )
				end

	endfunction: build_phase

  

  


  // - - - - - - - - - - - - Run phase: here take place the test actions
	task run_phase(uvm_phase phase);

    fork 
        // Create first transaction
        dataout_transaction = CLICTD_dataout_transaction::type_id::create(.name("dataout_transaction"), .contxt(get_full_name()));
        
				dataoutmonitor_action_resetreleased();
        dataoutmonitor_action_readout();
         
        detect_rise_clkout();
        detect_fall_clkout();
        detect_readout_ongoig();
				
    join_none
    
    // The fork join_none is used to avoid that this forever begin blocks the execution of other 
    // run_phase tasks in the hierarchy
	endtask: run_phase

  // - - - - - - - - - - - - Other tasks
  //

  task detect_readout_ongoig();
       readout_ongoing = `NO;
       //forever begin
       //   events_obj.rise_enableoutpad_event.wait_trigger(); //readout_ongoing = `YES; print_this_time = `YES;
       //   events_obj.fall_enableoutpad_event.wait_trigger(); readout_ongoing = `NO; 
       //end
  endtask: detect_readout_ongoig
  //
  task detect_rise_clkout();
        counter_clkout_riseedge = 0;
				forever begin 
							 @(posedge readout_vif.CLK_OUT) begin 
												events_obj.rise_clkout_event.trigger();
												if (readout_ongoing == `YES) counter_clkout_riseedge = counter_clkout_riseedge + 1; 
 								end 
				end
  endtask: detect_rise_clkout
  //
  task detect_fall_clkout();
				forever begin @(negedge readout_vif.CLK_OUT) begin events_obj.fall_clkout_event.trigger(); end end
  endtask: detect_fall_clkout
  //
  // Generate a transaction when reset is released
  task dataoutmonitor_action_resetreleased();
       forever begin
							 events_obj.reset_released_event.wait_trigger();
							 dataout_transaction.dataout_bit = readout_vif.DATA_OUT;
							 analysis_port.write(dataout_transaction);
               // Create next transaction to use, so that modifying the new one doesn't affect the values associated to the 
               // reference of the former one (otherwise, we could observe comparator errors MISCMP - miscomparison - when
               // we didn't expect them) 
               dataout_transaction = CLICTD_dataout_transaction::type_id::create(.name("dataout_transaction"), .contxt(get_full_name()));
               //dataout_transaction.timeout_alarm = 1'b0;
       end
  endtask: dataoutmonitor_action_resetreleased
  //
  // Generate a series of transactions when a readout operation is performed 
  task dataoutmonitor_action_readout();
       //initial_time_to_start_counting_timeout = $realtime;
       //readout_ongoing = `NO;
       forever begin
               //if (chip_status_obj.chip_status != STATUS_SECOND_READOUT_ALL_ZEROS) $display("%t readout ongoing 2 = %d",$realtime,readout_ongoing);
               
               if (readout_ongoing == `NO) begin 
                  //$display("%t waiting for update chip status",$time);
                  
                      chip_status_obj.chip_status_update_event.wait_trigger(); 

                    
                  //$display("%t READOUT ONGOING = %d",$time,readout_ongoing);
                  if ( chip_status_obj.chip_status == STATUS_SECOND_READOUT_ALL_ZEROS || chip_status_obj.chip_status == STATUS_FOURTH_READOUT_ALL_ZEROS ||
                   chip_status_obj.chip_status == STATUS_THIRD_READOUT_NOTHING_HAPPENS || chip_status_obj.chip_status == STATUS_FIFTH_READOUT_NOTHING_HAPPENS || 
                   chip_status_obj.chip_status == STATUS_CONFIG_READOUT_FIRST_HALF || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF || 
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0 || 
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0 ||
																			chip_status_obj.chip_status == STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0 ||
																			chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS ||
																			chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS  ) begin

                   //events_obj.timeout_start_readout_event.reset();
                   readout_ongoing = `YES;
                   first_time_timeout_is_counted = `YES;  
                   //$display("%t STATUS = %s, readout ongoing %d",$time,chip_status_obj.chip_status,readout_ongoing );
                  end
               end
               //else if (chip_status_obj.chip_status == STATUS_SECOND_READOUT_ALL_ZEROS || chip_status_obj.chip_status == STATUS_THIRD_READOUT_ALL_ZEROS || 
               //    chip_status_obj.chip_status == STATUS_CONFIG_READOUT_FIRST_HALF || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF ) begin
               else begin
                   //if (chip_status_obj.chip_status != STATUS_SECOND_READOUT_ALL_ZEROS) $display("%t call breakdown",$realtime);
               //if (chip_status_obj.chip_status == STATUS_SECOND_READOUT_ALL_ZEROS || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_FIRST_HALF || chip_status_obj.chip_status == STATUS_CONFIG_READOUT_SECOND_HALF 
               //    || chip_status_obj.chip_status == STATUS_READOUT_THAT_DOESNT_HAPPEN) begin
                   //readout_ongoing = `YES;
				           //breakdown_readout_bits (.chip_status(chip_status_obj.chip_status), .DATA_OUT(readout_vif.DATA_OUT));//
                   //fork
                       //breakdown_readout_bits (.DATA_OUT(readout_vif.DATA_OUT), .testname(testname));
																							breakdown_readout_bits (.DATA_OUT(readout_vif.DATA_OUT));
                   //    events_obj.timeout_start_readout_event.wait_trigger(); // this event is reset at the dataout_comparator
                   //join_any // this for is used so that the execution doesn't get blocked when a timeout occurs, so this brach of the if is exited and the 
                            // monitor can wait for the update of the next chip status 
                   //disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
										// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
										// which could otherwise lead to unexpected behavior (messages printing twice...)
                   //if (chip_status_obj.chip_status != STATUS_SECOND_READOUT_ALL_ZEROS) $display("%t readout ongoing = %d",$realtime,readout_ongoing);
               end
               
       end
  endtask: dataoutmonitor_action_readout


endclass: CLICTD_dataout_monitor



`endif // __CLICTD_dataout_monitor_sv__

