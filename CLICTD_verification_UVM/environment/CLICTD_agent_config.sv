// Filename           : CLICTD_agent_config.sv
// Author             : Núria Egidos 
// Created on         : 1/6/18
// Last modification  : 1/6/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Configuration object for the agent
//											Information for the environment: whether to instantiate a driver and a
//										  functional coverage collector 


`ifndef __CLICTD_agent_config_sv__
`define __CLICTD_agent_config_sv__

//import uvm_pkg::*;
//`include <uvm_macros.svh>

class CLICTD_agent_config extends uvm_object;
  `uvm_object_utils(CLICTD_agent_config) // Register the object to the factory

  // Knobs used to enable or disable the instantiation of components within the agent
  bit instantiate_driver;                      // Whether to instantiate a driver or not (passive agent)
  bit instantiate_functionalcoveragecollector; // Whether to instantiate a functional coverage collector or not
  // This is an unidimensional associative array (dynamic array for which we don't need to specify the size initially).
  // The positions in the arrays are created when filled and indexed with an unsigned int (0, 1, 2...), as indicated within brackets [].
  // We will use the exists() method to sort the content of the array and thus know to which scoreboards to connect so that they
  // probe the value of some pins_interface signals in the case of pinsinterface_agent.
  // http://www.asic-world.com/systemverilog/data_types13.html
  //int scoreboards_to_probepins [int];      
  
  // Constructor
  //function new(string name = "", 
  //             bit instantiate_driver_arg = `NO,
  //             bit instantiate_functionalcoveragecollector_arg = `NO,
  //             int scoreboards_to_probepins_arg [int] = '{}
  //             );
   function new(string name = "");
		super.new(name);
		//instantiate_driver = instantiate_driver_arg;
		//instantiate_functionalcoveragecollector = instantiate_functionalcoveragecollector_arg;
    //scoreboards_to_probepins = scoreboards_to_probepins_arg;
	endfunction: new


endclass: CLICTD_agent_config


`endif // __CLICTD_agent_config_sv__
