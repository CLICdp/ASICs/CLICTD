// Filename           : CLICTD_pins_driver.sv
// Author             : Núria Egidos 
// Created on         : 22/8/18
// Last modification  : 22/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Driver 
// Derived from       : analysis_master_driver.sv by Elia Conti and Sara Marconi for VEPIX53
//                      https://github.com/cluelogic/uvm-tutorial-for-candy-lovers/blob/master/src/tutorial_9.sv

`ifndef __CLICTD_pins_driver_sv__
`define __CLICTD_pins_driver_sv__



class CLICTD_pins_driver extends uvm_driver#(CLICTD_pins_transaction);
     `uvm_component_utils(CLICTD_pins_driver) // Register the component to the factory
 
     CLICTD_pins_transaction pins_trans;

     ChipStatusInfo chip_status_obj;

     // Instance of pins_interface on which we are going to retrieve
     // the virtual interface handle from the factory. This will be used
     // to interact with the slow control registers
     virtual CLICTD_pins_interface pins_vif; 

     CLICTDevents events_obj;
 
     // - - - - - - - - - - - - Constructor
     function new(string name, uvm_component parent);
          super.new(name, parent);
     endfunction: new
 
     // - - - - - - - - - - - - Build phase: build the hierarchy
     function void build_phase(uvm_phase phase);
          super.build_phase(phase); 
          if( !uvm_config_db#(virtual CLICTD_pins_interface)::get(this, "", "pins_if", pins_vif) ) `uvm_error( `NOVIF_PINS, $sformatf("pins virtual interface not found in database for %s", this.get_full_name()) )
          if( !uvm_config_db#(CLICTDevents)::get(this, "", "events_obj", events_obj) )
         `uvm_error( `NOCLICTDEVENTS, $sformatf("CLICTDevents not found in database for %s", this.get_full_name()) )

          // Retrieve the chip status from the database; it is set (and changed as the test progresses) from the virtual sequence 
										// in its body() task, so by this point it should be available to be retrieved
										if( !uvm_config_db#(ChipStatusInfo)::get(this, "", "chip_status_obj", chip_status_obj) )
						   					`uvm_error( `NOCHIPSTATUS, $sformatf("chip_status not found in database for %s", this.get_full_name()) )
     endfunction: build_phase

 
     // - - - - - - - - - - - - Run phase
     task run_phase(uvm_phase phase);
          fork
				          pinsdriver_action();
              pinsdriver_enable_100MHz();
          join_none
        		// The fork join_none is used to avoid that this forever begin blocks the execution of other 
        		// run_phase tasks in the hierarchy
     endtask: run_phase


     // - - - - - - - - - - - - Other tasks
     //
     // Drive the values from the transaction into the chip pins
     task pinsdriver_action();
																
          forever begin
				            seq_item_port.get_next_item(pins_trans); // blocked by the sequence until transaction is available
																assert_isunknown_pins_transaction ( .pins_transaction(pins_trans), .chip_status_obj(chip_status_obj), .name_of_transaction("pins_trans"), .name_of_calling_component(this.get_full_name()) );
                pins_vif.RSTN_pad    = pins_trans.RSTN_pad;
                pins_vif.PWREN_pad    = pins_trans.PWREN_pad;     
																pins_vif.TPULSE_pad  = pins_trans.TPULSE_pad;    
																pins_vif.READOUT_pad = pins_trans.READOUT_pad;	
																pins_vif.SHUTTER_pad = pins_trans.SHUTTER_pad;
															 pins_vif.disc        = pins_trans.disc;
				            seq_item_port.item_done();
				   end
     endtask: pinsdriver_action
     // 
     // Enable or disable the the 100 MHz clock
     // The events we listen to are triggered from sequences
     task pinsdriver_enable_100MHz();
									forever begin
                  				chip_status_obj.chip_status_update_event.wait_trigger(); // the forever is not continuously running, but it is paused until a change in chip_status is notified,
																						// to avoid locking the execution

																						if ( chip_status_obj.chip_status == STATUS_POWERPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS ||
                      					chip_status_obj.chip_status == STATUS_POWERPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS ||
																											chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS ||
                      					chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_WORKING || chip_status_obj.chip_status == STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS ||
																											chip_status_obj.chip_status == STATUS_CONFIG_ACQUISITION_MODE || chip_status_obj.chip_status == STATUS_FORCE_HIT_SHORTTOT || chip_status_obj.chip_status == STATUS_FORCE_HIT_LONGTOT
																										 ) begin
                      					    //$display("%t Waiting enable clock 100 MHz - pins driver",$realtime);
				              													events_obj.enable_clock_100MHz_event.wait_trigger();
                      									//$display("DISPLAY - pins driver - %t Enable clock 100 MHz!",$realtime);
				              													pins_vif.enable_clock_100MHz = 1'b1;
				              													events_obj.disable_clock_100MHz_event.wait_trigger();
                      									//$display("%t Disable clock 100 MHz! - pins driver",$realtime);
				              													pins_vif.enable_clock_100MHz = 1'b0;
																						end
          end
     endtask: pinsdriver_enable_100MHz


endclass: CLICTD_pins_driver

`endif // __CLICTD_pins_driver_sv__
