# Filename:          README.md
# Author:            N�ria Egidos 
# Created on:        6/2/2019
# Last modified on:  6/2/2019
# Project:           Doctoral student, verification of CLICTD
# Description:       README file to clarify the structure of the verification files and provide intructions on how to use them


The verification files of CLICTD (latest version: 6/2/19) are located under the CLICTD_verification_UVM folder. A graphical structure of the UVM framework can be found in:
https://indico.cern.ch/event/766032/ (presentation on the Verification status)
The framework has been built profiting from the experience and advice of Adrian Fiergolski (Clicpix2 verification environment), Tuomas Poikela and Sara Marconi.
All the scripts are written in TCL or SV and they're oriented to performing simulations with Questasim. 

To ensure a correct performance of the scripts, make sure that:
- your Questasim version is compatible with /eda/mentor/2017-18/RHELx86/QUESTA-CORE-PRIME_10.6c-1/questasim/bin/vsim
- you have created the necessary folder structure and count on all the required files
- you use a netlist version of those defined in launcher_all_tests.tcl
- you use a postlayout netlist (back annotation is supported)
- you're not running more than one simulation simultaneously at the same folder


The folder is structured as follows:

1. environment
   All the UVM components and objects that compose the UVM framework. 
			Under auxiliary_classes there are auxiliary objects used to exchange information among the different components and sequences.
			Under tasks_and_functions there are SV tasks and functions called from within the components and sequences.

2. rtl
			- CLICTD_slowcontrol_interface.sv, CLICTD_pins_interface.sv, CLICTD_readout_interface.sv : the different interfaces used to interconnect the DUT to the UVM test
			- CLICTD_dut_wrapper_rtl.sv, CLICTD_dut_wrapper_pnr.sv : wrappers that interconnect the DUT pads (and some internal signals) to the interfaces
   - generated_assign_internalconfigsignals_to_pinsif.v : file that is updated automatically when a different netlist version is used; it contains the interconnection from 
			                                                       internal signals of the DUT to interface signals. It's used inside the wrappers.
			- clictdTop_pnr_workaroundi2c_somecolpnr.v : file that is updated automatically when a different netlist version is used. Starting from the selected netlistPostlayout folder,
			                                             the clictd_export.v in that location is read; the I2C pads are replaced by internal signals of the slow control (this is required 
																																																to use the unidirectional I2C master by Adrian Fiergolski in i2cMaster.v); and, if specified by column_is_rtl (see the explanation
																																																on launcher_all_tests.tcl under point 5), some columns are replaced by the corresponding RTL netlist (this was done to speed up 
																																																the simulation at the beginning, but in the latest version all columns are in postlayout). 

   - i2cMaster.v: I2C master (written by Adrian Fiergolski for C3PD) used to drive the slow control commands at the pin wiggle level into the chip.

3. sequences
   All the sequences used in the different tests. The sequence calls are coordinated from CLICTD_virtual_sequence.sv, which is started from the tests we're running.

4. sim/wave_dos 
   Waveform files used to display predefined signals when the GUI mode is used.

5. sim_annotated
   - actions.do : this file contains processes called from launcher.do, launcher_all_tests.tcl and launcher_one_test.tcl 
			                     
			- launcher.do : this script is used from the Questasim command line to perform one single test at a time and to visualize the corresponding waveforms (automatically selected) at he end of the simulation.
			                To use it, cd sim_annotated, sim_annotated_min, sim_annotated_typ or sim_annotated_max
																			Type vsim &
																			In the Questasim command line:
																			  a) Standard compilation: do launcher.do compile <TESTNAME> pnr (the list of available test names is provided in the script)
																					b) Compilation to update all the files included in the simulation files when there has been some update in the netlist: do launcher.do compile_first_time <TESTNAME> pnr
																			Finally, to start the simulation: 
																			  a) Without back annotation: do launcher.do sim <TESTNAME> pnr (it's mandatory that the TESTNAME used here it's the same that was compiled)
																			  b) With back annotation:  do launcher.do sim <TESTNAME> pnr BACK_ANNOTATED <min, typ or max>
																			
			- launcher_all_tests.tcl : this script runs all the test in the command line mode (no GUI, no waveforms are shown or stored) in a row. After each test, the test report is stored in the corresponding
			                           logs folder, the simulator is closed and restarted to perform the following test. 
																														The idea is that this script can be called simultaneously from the sim_annotated_min, sim_annotated_typ and sim_annotated_max folders, and in each of them all the tests will
																														run sequentially (the next test starts when the former has finished) in the corner corresponding to the folder where the script is called from, and a report will be stored 
																														for each tests in each corner in the logs folder.
																												  To use it,
																														1. Select the simulation knobs (these are explained in the script)
																														   - USE_HIGHER_CLOCK_FREQ
																																	- USE_VERY_SHORT_TEST_PULSES
																																	- USE_FAST_I2C_CLOCK 
																																	- READOUTTESTFORCOVERAGE_DEBUGMODE: not supported, just set to ""
																																	- PATH_TO_DUT_FOLDER 
																																	- USE_WAVEFORM: not supported, just set to "NO"
																																	- RUN_SCRIPT_MODE (recommended: "COMMAND_LINE", to have an interim report as the simulation runs in the transcript file as well as the final report in the logs folder)
																																	- At the bottom of the script, comment or uncomment which tests are simulated 
																																	- At the bottom of the script, select which columns are to be used in postlayout or in RTL netlist to speed up simulation with COLUMN_IS_RTL. In the latest version, all 
																																	  columns are used in postlayout.
																														2. If it's the first time we run it or there has been some change in COLUMN_IS_RTL or in the netlist, uncomment the call:
																														   update_value_columnisrtl_and_associated_files
																																	to update all the affected files, otherwise leave it commented.
																														3. In three separate terminals, change to each corner folder and start the simulation in each of them:
																														   cd sim_annotated_min -> tclsh launcher_all_tests.tcl
																																	cd sim_annotated_typ -> tclsh launcher_all_tests.tcl
																																	cd sim_annotated_max -> tclsh launcher_all_tests.tcl
																																	As the simulations proceed, a message will be printed in the terminal where we've called the script indicating which test is running at the moment (and which tests have
																																	run so far). 
																																	### Note: if you uncomment update_value_columnisrtl_and_associated_files, it's recommendable to run the script first only in one corner, for instance,
																																	### cd sim_annotated_min -> tclsh launcher_all_tests.tcl
																																	### When you see the message "DEBUG LAUNCHER_ALL_TESTS.TCL RESET_TEST CLICTD_reset_test" in the terminal where you've called the script, it means all the files have been 
																																	### correctly updated and the simulation of the first test, the reset test, has started. 
																																	### At this point, you can comment the call to update_value_columnisrtl_and_associated_files and start the simulations for the other two corners as well:
																																	### cd sim_annotated_typ -> tclsh launcher_all_tests.tcl
																																	### cd sim_annotated_max -> tclsh launcher_all_tests.tcl
			
			- launcher_one_test.tcl : script called from launcher_all_tests.tcl to compile and simulate a certain test
			
			- generate_verilog_to_include.tcl : script called from launcher_all_tests.tcl to update automatically all the files included in the different files used for the simulation
			                                    when the netlist or some simulation knob change
																																						
   - clictd_output_replaced.sdf : file used to back annotate the CLICTD modules (except for the columns, which have a separated sdf file). It's obtained from clictd_output.sdf (in the folder where the 
			                               postlayout netlist version we're using is located); the original file is copied to sim_annotated and some of its lines are replaced so that the hierarchical paths are 
																																		compatible with the syntax required by the tool to perform the back annotation (i.e. some names with special characters are replaced so that the simulator doesn't crash).
																																		The resulting file in sim_annotated is read by the tool, which generates the compiled version (clictd_output_replaced.sdf_min.csd, clictd_output_replaced.sdf_typ.csd, 
																																		clictd_output_replaced.sdf_max.csd) that is then used to perform the back annotated simulation. 
			       
			- column_output_localcopy.sdf : file used to back annotate the columns. It's a copy from column_output.sdf (in the folder where the postlayout netlist version we're using is located). The copy is read 
			                                by the tool, which generates the compiled version that is then used to perform the back annotated simulation. It was necessary to use this local copy so that Questasim
																																			could have the read and write access to generate the compiled versions (in the original path it didn't have these accesses).
			       
			- sdfAssignments : file that contains the assignment of the corresponding sdf file to each instance (columns and clictdTop) in the top level postlayout netlist. This is used to perform the back annotated
			                   simulation and it's automatically generated when update_value_columnisrtl_and_associated_files is called.

6. sim_annotated_min, sim_annotated_typ, sim_annotated_max
   In these folders is where the simulations are run. The scripts in these folders source those in sim_annotated, and the required paths, filenames, etc. are automatically updated 
			depending on the folder where we're calling the simulator from.
			
			If launcher_all_tests.tcl is run (from sim_annotated_min/typ/max, type in a terminal: tclsh launcher_all_tests.tcl), the corresponding reports are written into the logs folder.
			In launcher.do is run (from sim_annotated_min/typ/max, type in a terminal: vsim & , and inside the Questasim command line, type: do launcher.do compile <TESTNAME> pnr, and then:
			do launcher.do sim <TESTNAME> pnr BACK_ANNOTATED <min, typ or max>), the waveform file corresponding to the selected test will be automatically chosen and displayed, and the 
			test report will be available at the transcript file in the folder where the simulation was started.
			
		
7. tests
   All the tests performed to verify CLICTD.

8. transactions
   Sequence items used for each interface to exchange information at the TLM level on the chip activity at its pads and at some internal signals. 

9. Files under the root folder

   All simulation parameters: clock period, paths to read or write files, test configuration...        
			- CLICTD_parameters.sv 

   Files updated automatically depending on the simulation knobs defined in launcher_all_tests.tcl when that script is called:
			- CLICTD_instantiation_assocarray_defaultNaddr_regs.sv : instantiation of the associative arrays that contain the slow control registers address and default value, this is used in CLICTD_test.
			- CLICTD_instantiation_dut_in_dutwrapperpnr.sv : instantiation of the DUT in the DUT wrapper (the ports depend on the netlist version)
			- CLICTD_paths_including_dut_folder.sv : paths to be included in CLICTD_parameters.sv                 
			- CLICTD_registers_info.sv : addresses and default values of the slow control registers, to be included in CLICTD_parameters.sv  
			- which_column_is_rtl.sv : file containing the value of COLUMN_IS_RTL specified in launcher_all_tests.tcl
			
			Simulation top files:
			- CLICTD_testbench_common.sv, included in CLICTD_testbench_top.sv 
