// Filename           : CLICTD_testbench_top.sv
// Author             : N�ria Egidos 
// Created on         : 30/5/18
// Last modification  : 11/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Testbench top
// Derived from       : pixel_chip_tb.sv developed by Sara Marconi and Elia Conti for VEPIX53 verification

// Randomization seed: https://stackoverflow.com/questions/31084419/verilog-changing-random-seed

// Include the parameter definition
`include "CLICTD_parameters.sv" 




// UVM test files
`include "CLICTD_test_pkg.sv"
//import CLICTD_test_pkg::*;




// Interfaces
//`include "CLICTD_pins_interface.sv"
//`include "CLICTD_readout_interface.sv"
//`include "CLICTD_slowcontrol_interface.sv"

// Wrapper that interconnects the interfaces and the DUT
//`include "CLICTD_dut_wrapper.sv"



// Testbench top
module CLICTD_testbench_top;

	`include "CLICTD_testbench_common.sv";
  
    


endmodule: CLICTD_testbench_top
