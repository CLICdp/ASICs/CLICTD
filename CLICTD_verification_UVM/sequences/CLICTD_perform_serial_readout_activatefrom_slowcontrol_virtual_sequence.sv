// Filename           : CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence.sv
// Author             : Núria Egidos 
// Created on         : 30/10/18
// Last modification  : 30/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to activate the serial readout via a slow control command
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence_sv__
`define __CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence_sv__


class CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence extends uvm_sequence#(uvm_sequence_item);

     `uvm_object_utils(CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence) // Register the component to the factory

     CLICTD_slowcontrol_transaction sc_trans;
					
     CLICTDevents events_obj;
					

     BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;

     ChipStatusInfo chip_status_obj;
    
     ReportingEnable reporting_enable_obj;

     // Randomizable fields
     FieldsConfigureMatrix config_matrix_obj;

     CLICTD_slowcontrol_sequencer slowcontrol_sqr;

     CLICTD_configure_internal_readout_sequence configure_internal_readout_seq;
     CLICTD_startstop_internal_readout_sequence startstop_internal_readout_seq;


     // Constructor
     function new(string name = "CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence");
          super.new(name);
     endfunction: new


     task body ();


          sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));

          configure_internal_readout_seq = CLICTD_configure_internal_readout_sequence::type_id::create( .name( "configure_internal_readout_seq" ), .contxt( get_full_name() ) );
          configure_internal_readout_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;

          startstop_internal_readout_seq = CLICTD_startstop_internal_readout_sequence::type_id::create( .name( "startstop_internal_readout_seq" ), .contxt( get_full_name() ) );
          startstop_internal_readout_seq.events_obj = events_obj;
			       startstop_internal_readout_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
			       startstop_internal_readout_seq.chip_status_obj = chip_status_obj;
			       startstop_internal_readout_seq.reporting_enable_obj = reporting_enable_obj;
			       startstop_internal_readout_seq.config_matrix_obj = config_matrix_obj;

          // Configure readout as external
          configure_internal_readout_seq.start( .sequencer( slowcontrol_sqr ) );

          // Perform readout by applying an external pulse at READOUT_pad
          startstop_internal_readout_seq.start( .sequencer( slowcontrol_sqr ) );


     endtask: body




endclass: CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence

`endif // __CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence_sv__
