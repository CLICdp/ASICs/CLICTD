// Filename           : CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence.sv
// Author             : Núria Egidos 
// Created on         : 22/8/18
// Last modification  : 22/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to activate the serial readout by applying an external test pulse
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence_sv__
`define __CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence_sv__



class CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence extends uvm_sequence#(uvm_sequence_item);
     `uvm_object_utils(CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence) // Register the component to the factory

     CLICTDevents events_obj;
     BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;

     CLICTD_slowcontrol_sequencer slowcontrol_sqr;
     CLICTD_pins_sequencer pins_sqr;

     CLICTD_configure_external_readout_sequence configure_external_readout_seq;
     CLICTD_startstop_external_readout_sequence startstop_external_readout_seq;

     

     // Constructor
     function new(string name = "CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence");
          super.new(name);
     endfunction: new


     task body ();
          configure_external_readout_seq = CLICTD_configure_external_readout_sequence::type_id::create( .name( "configure_external_readout_seq" ), .contxt( get_full_name() ) );
          configure_external_readout_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;

          startstop_external_readout_seq = CLICTD_startstop_external_readout_sequence::type_id::create( .name( "startstop_external_readout_seq" ), .contxt( get_full_name() ) );
          startstop_external_readout_seq.events_obj = events_obj;

          // Configure readout as external
          configure_external_readout_seq.start( .sequencer( slowcontrol_sqr ) );

          // Perform readout by applying an external pulse at READOUT_pad
          startstop_external_readout_seq.start( .sequencer( pins_sqr ) );


     endtask: body




endclass: CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence

`endif // __CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence_sv__
