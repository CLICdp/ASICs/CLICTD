// Filename           : CLICTD_force_initialvalue_pwrenpad_sequence.sv
// Author             : Núria Egidos 
// Created on         : 1/10/18
// Last modification  : 1/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to force an initial value to PWREN_pad
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_force_initialvalue_pwrenpad_sequence_sv__
`define __CLICTD_force_initialvalue_pwrenpad_sequence_sv__



class CLICTD_force_initialvalue_pwrenpad_sequence extends uvm_sequence#(CLICTD_pins_transaction);
     `uvm_object_utils(CLICTD_force_initialvalue_pwrenpad_sequence) // Register the component to the factory

     CLICTD_pins_transaction pins_trans;

     // Constructor
     function new(string name = "CLICTD_force_initialvalue_pwrenpad_sequence");
          super.new(name);
     endfunction: new


     task body ();

    
         
         CLICTD_pins_transaction pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
         start_item(pins_trans);
									// Default values of signals to drive
											pins_trans.TPULSE_pad = 1'b0;
											pins_trans.SHUTTER_pad = 1'b0;
											pins_trans.RSTN_pad = 1'b1;
											pins_trans.READOUT_pad = 1'b0;
										// Force an initial value to PWREN_pad before enabling external power pulsing, otherwise powerEnable 
         // would also be undefined 	
         pins_trans.PWREN_pad = 1'b1;
         finish_item(pins_trans);
         //#10;
          
           


     endtask: body




endclass: CLICTD_force_initialvalue_pwrenpad_sequence

`endif // __CLICTD_force_initialvalue_pwrenpad_sequence_sv__
