// Filename           : CLICTD_slowcontroltrans_base_sequence.sv
// Author             : Núria Egidos 
// Created on         : 4/9/18
// Last modification  : 4/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Base sequence with the required methods to interact with the slowcontrol_driver
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_slowcontroltrans_base_sequence_sv__
`define __CLICTD_slowcontroltrans_base_sequence_sv__



class CLICTD_slowcontroltrans_base_sequence extends uvm_sequence#(CLICTD_slowcontrol_transaction);
     `uvm_object_utils(CLICTD_slowcontroltrans_base_sequence) // Register the component to the factory

     CLICTD_slowcontrol_transaction sc_trans;

     BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;

     ChipStatusInfo chip_status_obj;
    
     ReportingEnable reporting_enable_obj;

     // Randomizable fields
     FieldsConfigureMatrix config_matrix_obj;

     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_perform_serial_readout_activatefrom_slowcontrol_sequence");
          super.new(name);
     endfunction: new


		// - - - - - - - - - - - - Send slowcontrol_transaction to the slowcontrol_driver, write operation
      task send_slowcontrol_transaction_write(input logic [7:0] register_address, input logic [7:0] register_content);
						     sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
           start_item(sc_trans);
           sc_trans.register_address = register_address;
           sc_trans.register_content = register_content;
           sc_trans.read_or_write = `WRITE;
           finish_item(sc_trans);
      endtask: send_slowcontrol_transaction_write

    // - - - - - - - - - - - - Send slowcontrol_transaction to the slowcontrol_driver, read operation
      task send_slowcontrol_transaction_read(input logic [7:0] register_address);
						     sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
           start_item(sc_trans);
           sc_trans.register_address = register_address;
           sc_trans.read_or_write = `READ;
           finish_item(sc_trans);
      endtask: send_slowcontrol_transaction_read



endclass: CLICTD_slowcontroltrans_base_sequence

`endif // __CLICTD_slowcontroltrans_base_sequence_sv__





  



