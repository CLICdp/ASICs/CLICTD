// Filename           : CLICTD_startstop_internal_readout_sequence.sv
// Author             : Núria Egidos 
// Created on         : 24/9/18
// Last modification  : 24/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to activate the serial readout via a slow control command
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_startstop_internal_readout_sequence_sv__
`define __CLICTD_startstop_internal_readout_sequence_sv__



class CLICTD_startstop_internal_readout_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_startstop_internal_readout_sequence) // Register the component to the factory

     CLICTDevents events_obj;



     // Constructor
     function new(string name = "CLICTD_startstop_internal_readout_sequence");
          super.new(name);
     endfunction: new


     task body ();


          sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
  

           // Start readout
           // Write register readoutCtrl: set roInt to 1'b1 (indicate readout control via slow control). 
           // The moment this bit is set high, the readout starts.
           bits_slowcontrol_reg_obj.roInt = 1'b1;
           send_slowcontrol_transaction_write(.register_address(`ADDRESS_readoutCtrl), .register_content({6'b0, bits_slowcontrol_reg_obj.roExtEn, bits_slowcontrol_reg_obj.roInt}) );

           sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));

           // Stop readout
           fork
							 // To ensure a full readout is performed, clear roInt when ENABLE_OUT_pad 
							 // goes low (this signal will be high while the readout is ongoing)
           		 events_obj.fall_enableoutpad_event.wait_trigger();
               // In case the chip is not configured to perform a readout operation activated
               // externally, the above condition will never happen. In such case, we wait
               // for the start readout timeout to happen and later abort the readout
               begin
               events_obj.timeout_start_readout_event.wait_trigger(); // this event is reset at the dataout_comparator
               //$display("%t seq - timeout arrived",$realtime);
               end
           join_any
           disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
				  // once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
				  // which could otherwise lead to unexpected behavior (messages printing twice...)
           // Stop/abort readout
           // The moment roInt is cleared to 1'b0, the readout stops
           bits_slowcontrol_reg_obj.roInt = 1'b0;
           send_slowcontrol_transaction_write(.register_address(`ADDRESS_readoutCtrl), .register_content({6'b0, bits_slowcontrol_reg_obj.roExtEn, bits_slowcontrol_reg_obj.roInt}) );


     endtask: body




endclass: CLICTD_startstop_internal_readout_sequence

`endif // __CLICTD_startstop_internal_readout_sequence_sv__
