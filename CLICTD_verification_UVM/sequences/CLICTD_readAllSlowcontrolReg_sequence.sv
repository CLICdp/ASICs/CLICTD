// Filename           : CLICTD_readAllSlowcontrolReg_sequence.sv
// Author             : Núria Egidos 
// Created on         : 7/8/18
// Last modification  : 10/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence for the reset test: read all slow control registers in a row
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_readAllSlowcontrolReg_sequence_sv__
`define __CLICTD_readAllSlowcontrolReg_sequence_sv__



class CLICTD_readAllSlowcontrolReg_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_readAllSlowcontrolReg_sequence) // Register the component to the factory

     addresses_registers_slowcontrol_type addresses_registers_slowcontrol;

     // Constructor
     function new(string name = "CLICTD_readAllSlowcontrolReg_sequence");
          super.new(name);
     endfunction: new


     task body ();
          logic [7:0] register_address = 0;
          //CLICTD_slowcontrol_transaction sc_trans;
					// Read all slow control registers to check their default value

          foreach(addresses_registers_slowcontrol[i]) begin
                // http://www.verificationguide.com/p/systemverilog-associative-array.html
                 sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 send_slowcontrol_transaction_read(.register_address(addresses_registers_slowcontrol[i]));
          end
          
     endtask: body




endclass: CLICTD_readAllSlowcontrolReg_sequence

`endif // __CLICTD_readAllSlowcontrolReg_sequence_sv__
