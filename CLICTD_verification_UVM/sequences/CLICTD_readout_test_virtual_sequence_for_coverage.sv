// Filename           : CLICTD_readout_test_virtual_sequence_for_coverage.sv
// Author             : N�ria Egidos 
// Created on         : 17/1/19
// Last modification  : 17/1/19
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to configure the acquisition mode
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_readout_test_virtual_sequence_for_coverage_sv__
`define __CLICTD_readout_test_virtual_sequence_for_coverage_sv__



class CLICTD_readout_test_virtual_sequence_for_coverage extends CLICTD_readout_test_virtual_sequence;
     `uvm_object_utils(CLICTD_readout_test_virtual_sequence_for_coverage) // Register the component to the factory
/*
					// Sequence to configure the aquistion mode (nominal/long ToA/photon counting, compression...)
					CLICTD_configure_acquisition_mode_sequence configure_acquisition_mode_seq;
					
					// Sequence to enable the injection of test pulses and configure this injection to be performed from TPULSE_pad
     CLICTD_configure_external_testpulse_sequence configure_external_testpulse_seq;
					
					// Sequence to apply digital test pulses via TPULSE_pad and force pulses at some disc bits (on pixel coordinates 
					// specified with matrixconfig_virtual_seq). 
     CLICTD_apply_hits_external_sequence_for_coverage apply_hits_external_seq;


     // Sequence to activate serial readout with a slow control command
     CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence perform_serial_readout_activatefrom_slowcontrol_virtual_seq;
					

     BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
     ChipStatusInfo chip_status_obj;
     CLICTDevents events_obj;
     ReportingEnable reporting_enable_obj;

     CLICTD_slowcontrol_sequencer slowcontrol_sqr;
     CLICTD_pins_sequencer pins_sqr;

     FieldsTimeMeasurements_for_coverage time_measurements_obj;
     FieldsConfigureMatrix_for_coverage config_matrix_obj;
*/

     FieldsTimeMeasurements_for_coverage time_measurements_obj_for_coverage;
     FieldsConfigureMatrix_for_coverage config_matrix_obj_for_coverage;
					
					// Sequence to apply digital test pulses via TPULSE_pad and force pulses at some disc bits (on pixel coordinates 
					// specified with matrixconfig_virtual_seq). 
     CLICTD_apply_hits_external_sequence_for_coverage apply_hits_external_seq_for_coverage;

     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_readout_test_virtual_sequence_for_coverage");
          super.new(name);
     endfunction: new


		// - - - - - - - - - - - - Actions to perform for all readout modes

      virtual task common_actions_allreadoutmodes(chip_status_type chip_status);
						
						     string coordinates_pulsed[string];
											bit one_if_not_all_discriminator_bits_hit_are_masked[string];
											string current_coordinate,aux_coordinate;
											int aux_counter_bits_readout, aux_counter_events;
											
						
						     // Update the expected hit flag according to the compression configuration:
											// if compression is disabled, all hit flags should be 1
											if (bits_slowcontrol_reg_obj.noCompr) begin
											    aux_counter_events = 0;
															aux_counter_bits_readout = 0;
											    for (int c = 0; c < `COLUMNS; c++) begin
															     aux_counter_bits_readout += `NUMBER_BITS_COLUMN_HEADER;
															     for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																				     for (int sis =0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
						                        config_matrix_obj_for_coverage.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis] = 1'b1;
																														aux_counter_bits_readout += `NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED;
																														aux_counter_events++;
																									end
																				end
																				$display("DISPLAY - readout seq - expected hit flag col %0d = %h",c, config_matrix_obj_for_coverage.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c]);
															end
															$display("DISPLAY - readout seq - number of events expected = %0d, number of readout bits expected %0d",aux_counter_events,aux_counter_bits_readout + `NUMBER_BITS_START_HEADER + `NUMBER_BITS_STOP_HEADER);
											end
											else begin
											    /*
											    foreach (config_matrix_obj.superpixels_to_pulse[i]) begin
															   aux_coordinate = $sformatf("%0d-%0d-%0d",config_matrix_obj_for_coverage.superpixels_to_pulse[i].column,config_matrix_obj_for_coverage.superpixels_to_pulse[i].superpixel_segment,config_matrix_obj_for_coverage.superpixels_to_pulse[i].superpixel_in_segment);
											   				coordinates_pulsed[aux_coordinate] = aux_coordinate;
																		
																		// In this variable we don't care if digital test pulse is enabled or not in this superpixel, this will be handled afterwards.
																		// This variable will be 1 if some of the discriminator bits where some hits are applied is not masked (if all of them are masked, it will be 0)
																		if ( config_matrix_obj_for_coverage.mask[config_matrix_obj_for_coverage.superpixels_to_pulse[i].column][config_matrix_obj_for_coverage.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj_for_coverage.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj_for_coverage.superpixels_to_pulse[i].bit_applydiscpulse_0]  &&
																		     config_matrix_obj_for_coverage.mask[config_matrix_obj_for_coverage.superpixels_to_pulse[i].column][config_matrix_obj_for_coverage.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj_for_coverage.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj_for_coverage.superpixels_to_pulse[i].bit_applydiscpulse_1]
																						) one_if_not_all_discriminator_bits_hit_are_masked[aux_coordinate] = 1'b0;
																		else one_if_not_all_discriminator_bits_hit_are_masked[aux_coordinate] = 1'b1;
																		 
															end
											    for (int c = 0; c < `COLUMNS; c++) begin
															     for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																				     for (int sis =0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																									     current_coordinate = $sformatf("%0d-%0d-%0d",c,ss,sis);
																									     // If a digital test pulse is applied (it's the case in this test) and digital test pulsing is enabled in this superpixel;
																														// or if some hits are forced at some discriminator bits of this superpixel, digital test pulsing is disabled in this superpixel,
																														// and the discriminator bits where some of these hits are applied are not masked;
																														// in either of these two cases, the expected hit flag is 1, but if some of these conditions are not fulfilled, then it will be 0
																														if ( config_matrix_obj_for_coverage.tpEnableDigital[c][ss][sis] ||
																														     ( !config_matrix_obj_for_coverage.tpEnableDigital[c][ss][sis] && coordinates_pulsed.exists(current_coordinate) && one_if_not_all_discriminator_bits_hit_are_masked[current_coordinate] )
																																		) config_matrix_obj_for_coverage.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis] = 1'b1;
																														else  config_matrix_obj_for_coverage.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis] = 1'b0;
																									end
																				end
																				$display("DISPLAY - readout seq - expected hit map col %0d = %h",c, config_matrix_obj_for_coverage.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c]);
															end
															coordinates_pulsed.delete();
															one_if_not_all_discriminator_bits_hit_are_masked.delete();
															*/
															config_matrix_obj_for_coverage.update_expected_hit_flag_when_readout_compressed();
															aux_counter_events = 0;
															aux_counter_bits_readout = 0;
															for (int c = 0; c < `COLUMNS; c++) begin
															    $display("DISPLAY - readout seq - expected hit flag col %0d = %h",c, config_matrix_obj_for_coverage.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c]);
																			aux_counter_bits_readout += `NUMBER_BITS_COLUMN_HEADER;
															     for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																				     for (int sis =0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
						                        if (config_matrix_obj_for_coverage.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis]) begin
																																		aux_counter_bits_readout += `NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED;
																																		aux_counter_events++;
																														end
																														else aux_counter_bits_readout += 1;
																									end
																				end
															end
															$display("DISPLAY - readout seq - number of events expected = %0d, number of readout bits expected %0d",aux_counter_events,aux_counter_bits_readout + `NUMBER_BITS_START_HEADER + `NUMBER_BITS_STOP_HEADER);
											end
											

           // Send the acquisition configuration to the slow control
           chip_status_obj.chip_status = STATUS_CONFIG_ACQUISITION_MODE; 
		         chip_status_obj.chip_status_update_event.trigger();
           `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           configure_acquisition_mode_seq.start(  .sequencer(slowcontrol_sqr) );
           
           
           // Enable 100 MHz clock
           events_obj.enable_clock_100MHz_event.trigger();
											
											// Wait some time for the clock to propagate across the chip
											#100;
           
           // Force hits at the matrix (activated from external test pulse)
											// The last digital test pulse inside the shutter finishes before the shutter falls
											if ( chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 && chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 && 
													   chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 && chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 &&
													   chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0 && chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1 && 
													   chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2 && chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3 )
                chip_status_obj.chip_status = STATUS_FORCE_HIT_SHORTTOT; 
											else chip_status_obj.chip_status = STATUS_FORCE_HIT_LONGTOT;
		         chip_status_obj.chip_status_update_event.trigger();
           `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           apply_hits_external_seq_for_coverage.start(  .sequencer(pins_sqr) );
											
											
											// Wait some time before deactivating the clock 
											#100;

           // Disable 100 MHz clock
           events_obj.disable_clock_100MHz_event.trigger();
											

           // Readout acquisition (activated from the slow control)
           chip_status_obj.chip_status = chip_status; 
		         chip_status_obj.chip_status_update_event.trigger();
           `uvm_info("chip_status", $sformatf("Chip status: %s, # pulses in shutter %0d", chip_status_obj.chip_status,time_measurements_obj.number_of_pulses_to_apply_within_shutter), UVM_LOW);
           perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( slowcontrol_sqr ));
          
											
      endtask: common_actions_allreadoutmodes
						
						
						virtual task common_actions_sametotDiv(chip_status_type chip_status);
						
						
						     //config_matrix_obj_for_coverage.superpixels_to_pulse.delete(); assert (config_matrix_obj_for_coverage.randomize()); // this makes no sense at this point because we don't repeat matrix configuration
						     time_measurements_obj_for_coverage.number_of_pulses_to_apply_within_shutter = 1; assert(time_measurements_obj_for_coverage.randomize());  
												`uvm_info("Timing injected hits", $sformatf("%s, 1 pulse within shutter, toa_ns = %d, tot_ns = %d, delay 1st event within shutter = %d, # superpixels hit = %0d",
						 					     chip_status,time_measurements_obj_for_coverage.toa_ns, time_measurements_obj_for_coverage.tot_ns, time_measurements_obj_for_coverage.delay_to_startpulses_after_shutter_ns, config_matrix_obj_for_coverage.occupancy), UVM_LOW);
												common_actions_allreadoutmodes(chip_status);
												
												
												//config_matrix_obj_for_coverage.superpixels_to_pulse.delete(); assert (config_matrix_obj_for_coverage.randomize()); // this makes no sense at this point because we don't repeat matrix configuration
												time_measurements_obj_for_coverage.number_of_pulses_to_apply_within_shutter = 2;  assert(time_measurements_obj_for_coverage.randomize()); 
												`uvm_info("Timing injected hits", $sformatf("%s, 2 pulses within shutter, toa_ns = %d, tot_ns = %d, delay 1st event within shutter = %d, # superpixels hit = %0d",
						 					     chip_status,time_measurements_obj_for_coverage.toa_ns, time_measurements_obj_for_coverage.tot_ns, time_measurements_obj_for_coverage.delay_to_startpulses_after_shutter_ns, config_matrix_obj_for_coverage.occupancy), UVM_LOW);
												common_actions_allreadoutmodes(chip_status);
												
											
						endtask: common_actions_sametotDiv



		  virtual task body ();

            chip_status_type chip_status;

            configure_acquisition_mode_seq = CLICTD_configure_acquisition_mode_sequence::type_id::create( .name( "configure_acquisition_mode_seq" ), .contxt( get_full_name() ) );
            configure_acquisition_mode_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
					       configure_acquisition_mode_seq.chip_status_obj = chip_status_obj;
					       configure_acquisition_mode_seq.reporting_enable_obj = reporting_enable_obj;
												configure_acquisition_mode_seq.config_matrix_obj = config_matrix_obj;
												
												configure_external_testpulse_seq = CLICTD_configure_external_testpulse_sequence::type_id::create( .name( "configure_external_testpulse_seq" ), .contxt( get_full_name() ) );
												configure_external_testpulse_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
     							configure_external_testpulse_seq.chip_status_obj = chip_status_obj;
     							configure_external_testpulse_seq.reporting_enable_obj = reporting_enable_obj;
     							configure_external_testpulse_seq.config_matrix_obj = config_matrix_obj;
						
            apply_hits_external_seq_for_coverage = CLICTD_apply_hits_external_sequence_for_coverage::type_id::create( .name( "apply_hits_external_seq_for_coverage" ), .contxt( get_full_name() ) );
												apply_hits_external_seq_for_coverage.config_matrix_obj = config_matrix_obj_for_coverage; // assign after the randomize so as to preserve the values in the redout_test_seq
												apply_hits_external_seq_for_coverage.time_measurements_obj = time_measurements_obj_for_coverage;
												
												

            perform_serial_readout_activatefrom_slowcontrol_virtual_seq = CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence::type_id::create( .name( "perform_serial_readout_activatefrom_slowcontrol_virtual_seq" ), .contxt( get_full_name() ) );
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.chip_status_obj = chip_status_obj;
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.events_obj = events_obj;
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
												perform_serial_readout_activatefrom_slowcontrol_virtual_seq.config_matrix_obj = config_matrix_obj;
            perform_serial_readout_activatefrom_slowcontrol_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
												
												// Enable test pulse injection and configure test pulse to be injected from TPULSE_pad
											 configure_external_testpulse_seq.start ( .sequencer(slowcontrol_sqr) );





           //reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES;




           /*
            // Nominal readout, compression disabled
            bits_slowcontrol_reg_obj.noCompr = 1'b1;
            bits_slowcontrol_reg_obj.photonCnt = 1'b0; bits_slowcontrol_reg_obj.longCnt = 1'b0;
												
            // totDiv = 0
												bits_slowcontrol_reg_obj.totDiv = 2'b00; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0; common_actions_sametotDiv(chip_status);
												
												
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0;  common_actions_sametotDiv(chip_status);
												
												
            // totDiv = 1
            bits_slowcontrol_reg_obj.totDiv = 2'b01; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1; common_actions_sametotDiv(chip_status);
            // totDiv = 2
            bits_slowcontrol_reg_obj.totDiv = 2'b10; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2; common_actions_sametotDiv(chip_status);
            // totDiv = 3
            bits_slowcontrol_reg_obj.totDiv = 2'b11; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3; common_actions_sametotDiv(chip_status);
											
            // Long ToA readout, totDiv = 0, compression disabled
            bits_slowcontrol_reg_obj.photonCnt = 1'b0; bits_slowcontrol_reg_obj.longCnt = 1'b1;
            bits_slowcontrol_reg_obj.totDiv = 2'b00; bits_slowcontrol_reg_obj.noCompr = 1'b1; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0; common_actions_sametotDiv(chip_status);
											
												
												// Photon counting readout, totDiv = 0, compression disabled
            bits_slowcontrol_reg_obj.photonCnt = 1'b1; bits_slowcontrol_reg_obj.longCnt = 1'b1;
            bits_slowcontrol_reg_obj.totDiv = 2'b00; bits_slowcontrol_reg_obj.noCompr = 1'b1; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0; common_actions_sametotDiv(chip_status);
												*/
												// ---------------------------------------------------------------------------------------------------------------------------
												// Additional steps to enable compressed readout (since we've already configured and readout before, it's only required to...)
												// Enable compression
												bits_slowcontrol_reg_obj.noCompr = 1'b0;
												// Send the acquisition configuration to the slow control
           	chip_status_obj.chip_status = STATUS_CONFIGURE_ENABLE_COMPRESSION; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Enabling compressed readout, chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	configure_acquisition_mode_seq.start(  .sequencer(slowcontrol_sqr) );
											
												// Readout twice in a row to clear hit flags
           	chip_status_obj.chip_status = STATUS_FIRST_READOUT_ENABLE_COMPRESSION; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( slowcontrol_sqr ));
												chip_status_obj.chip_status = STATUS_SECOND_READOUT_ENABLE_COMPRESSION; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( slowcontrol_sqr ));
												
												// ---------------------------------------------------------------------------------------------------------------------------
            //reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES;
												
            // Nominal readout, compression enabled
            bits_slowcontrol_reg_obj.noCompr = 1'b0;
            bits_slowcontrol_reg_obj.photonCnt = 1'b0; bits_slowcontrol_reg_obj.longCnt = 1'b0;
            // totDiv = 0
            bits_slowcontrol_reg_obj.totDiv = 2'b00; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0; common_actions_sametotDiv(chip_status);
												
												
            // totDiv = 1
            bits_slowcontrol_reg_obj.totDiv = 2'b01; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1; common_actions_sametotDiv(chip_status);
            // totDiv = 2
            bits_slowcontrol_reg_obj.totDiv = 2'b10; common_actions_allreadoutmodes(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2; common_actions_sametotDiv(chip_status);
            // totDiv = 3
            bits_slowcontrol_reg_obj.totDiv = 2'b11; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3; common_actions_sametotDiv(chip_status);

            // Long ToA readout, totDiv = 0, compression enabled
            bits_slowcontrol_reg_obj.photonCnt = 1'b0; bits_slowcontrol_reg_obj.longCnt = 1'b1;
            bits_slowcontrol_reg_obj.totDiv = 2'b00; bits_slowcontrol_reg_obj.noCompr = 1'b0;	
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0; common_actions_sametotDiv(chip_status);

            // Photon counting readout, totDiv = 0, compression enabled
            bits_slowcontrol_reg_obj.photonCnt = 1'b1; bits_slowcontrol_reg_obj.longCnt = 1'b1;
            bits_slowcontrol_reg_obj.totDiv = 2'b00; bits_slowcontrol_reg_obj.noCompr = 1'b0; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0; common_actions_sametotDiv(chip_status);
         
            
            

		  endtask: body




endclass: CLICTD_readout_test_virtual_sequence_for_coverage

`endif // __CLICTD_readout_test_virtual_sequence_for_coverage_sv__





  



