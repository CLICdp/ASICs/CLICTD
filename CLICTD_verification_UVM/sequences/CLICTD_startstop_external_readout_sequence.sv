// Filename           : CLICTD_startstop_external_readout_sequence.sv
// Author             : Núria Egidos 
// Created on         : 22/8/18
// Last modification  : 22/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to start/stop the serial readout by applying an external test pulse
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_startstop_external_readout_sequence_sv__
`define __CLICTD_startstop_external_readout_sequence_sv__



class CLICTD_startstop_external_readout_sequence extends uvm_sequence#(CLICTD_pins_transaction);
     `uvm_object_utils(CLICTD_startstop_external_readout_sequence) // Register the component to the factory

     CLICTD_pins_transaction pins_trans;
     CLICTDevents events_obj;

     // Constructor
     function new(string name = "CLICTD_startstop_external_readout_sequence");
          super.new(name);
     endfunction: new


     task body ();
          pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
										
										
										 // Force the update of READOUT_pad at the same time as the edge of the clock that drives the
											// synchronizer FFs that sychronize readoutStart to the CLK_40, so as to trigger timing violations
											//events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
											
											// NOTE: this event doesn't guarantee that the READOUT_pad will be updated at the same time as clk40! Because to reach the 
											// synchronizer FF, READOUT_pad crosses a multiplexer to become readoutStart, and that crossing has a propagation delay that
											// depends on the corner, so READOUT_pad update will not happen at the clk40 edge!
           
           start_item(pins_trans);
											// Default values of signals to drive
											pins_trans.TPULSE_pad = 1'b0;
											pins_trans.SHUTTER_pad = 1'b0;
											pins_trans.RSTN_pad = 1'b1;
											pins_trans.PWREN_pad = 1'b1;
											// Start readout
           // Force a pulse at READOUT_pad
           pins_trans.READOUT_pad = 1'b1;
           finish_item(pins_trans);
											
											
											//$display("DISPLAY - starstop external readout - %t clk40 rise, readout pad change", $realtime);

           pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
           // Stop readout
           fork
							 						// To ensure a full readout is performed, clear READOUT_pad when ENABLE_OUT_pad 
							 						// goes low (this signal will be high while the readout is ongoing)
           		 events_obj.fall_enableoutpad_event.wait_trigger();
               // In case the chip is not configured to perform a readout operation activated
               // externally, the above condition will never happen. In such case, we wait
               // for the start readout timeout to happen and later abort the readout
               begin
               events_obj.timeout_start_readout_event.wait_trigger(); // this event is reset at the dataout_comparator
               //$display("%t seq - timeout arrived",$realtime);
               end
           join_any
           disable fork; // in the case of the join_any, we are only interested in one of the events finishing, we don't want that the rest continue running
				  					// once we have the information that the event of interest finished. With the disable fork, we stop the "ghost" events that would continue to run,
				  					// which could otherwise lead to unexpected behavior (messages printing twice...)
											
											// Force the update of READOUT_pad at the same time as the edge of the clock that drives the
											// synchronizer FFs that sychronize readoutStart to the CLK_40, so as to trigger timing violations
											//events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
											
											
											//$display("DISPLAY - starstop external readout - %t clk40 rise, readout pad change", $realtime);
											
           // Stop/abort readout
           start_item(pins_trans);
											// Default values of signals to drive
											pins_trans.TPULSE_pad = 1'b0;
											pins_trans.SHUTTER_pad = 1'b0;
											pins_trans.RSTN_pad = 1'b1;
											pins_trans.PWREN_pad = 1'b1;
           pins_trans.READOUT_pad = 1'b0;
           finish_item(pins_trans);
           


     endtask: body




endclass: CLICTD_startstop_external_readout_sequence

`endif // __CLICTD_startstop_external_readout_sequence_sv__
