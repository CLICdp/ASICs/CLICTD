// Filename           : CLICTD_readoutcontrol_test_virtual_sequence.sv
// Author             : Núria Egidos 
// Created on         : 27/9/18
// Last modification  : 27/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to perform the readout control test
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_readoutcontrol_test_virtual_sequence_sv__
`define __CLICTD_readoutcontrol_test_virtual_sequence_sv__



class CLICTD_readoutcontrol_test_virtual_sequence extends uvm_sequence#(uvm_sequence_item);
     `uvm_object_utils(CLICTD_readoutcontrol_test_virtual_sequence) // Register the component to the factory


     CLICTD_slowcontrol_sequencer slowcontrol_sqr;
     CLICTD_pins_sequencer pins_sqr;


     ChipStatusInfo chip_status_obj;
     CLICTDevents events_obj;
     BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
     ReportingEnable reporting_enable_obj;
     FieldsConfigureMatrix config_matrix_obj;

     // Sequence to configure serial readout (activated from external pulse) and activate it using an external pulse
     CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence perform_serial_readout_activatefrom_external_virtual_seq;
     // Sequence to configure serial readout (activated from slow control) and activate it using a slow control command
     CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence perform_serial_readout_activatefrom_slowcontrol_virtual_seq;
     // Sequence to start and stop readout using an external pulse
     CLICTD_startstop_external_readout_sequence startstop_external_readout_seq;
     // Sequence to start and stop readout using a slow control command
     CLICTD_startstop_internal_readout_sequence startstop_internal_readout_seq;

     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_readoutcontrol_test_virtual_sequence");
          super.new(name);
     endfunction: new


		  task body ();

            perform_serial_readout_activatefrom_slowcontrol_virtual_seq = CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence::type_id::create( .name( "perform_serial_readout_activatefrom_slowcontrol_virtual_seq" ), .contxt( get_full_name() ) );
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.chip_status_obj = chip_status_obj;
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.events_obj = events_obj;
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
            perform_serial_readout_activatefrom_slowcontrol_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;

            perform_serial_readout_activatefrom_external_virtual_seq = CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence::type_id::create( .name( "perform_serial_readout_activatefrom_external_virtual_seq" ), .contxt( get_full_name() ) );
						      perform_serial_readout_activatefrom_external_virtual_seq.events_obj = events_obj;
					      	perform_serial_readout_activatefrom_external_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
					      	perform_serial_readout_activatefrom_external_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
					      	perform_serial_readout_activatefrom_external_virtual_seq.pins_sqr = pins_sqr;
      


            startstop_external_readout_seq = CLICTD_startstop_external_readout_sequence::type_id::create( .name( "startstop_external_readout_seq" ), .contxt( get_full_name() ) );
						      startstop_external_readout_seq.events_obj = events_obj;

						      startstop_internal_readout_seq = CLICTD_startstop_internal_readout_sequence::type_id::create( .name( "startstop_internal_readout_seq" ), .contxt( get_full_name() ) );
					      	startstop_internal_readout_seq.events_obj = events_obj;
						      startstop_internal_readout_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
						      startstop_internal_readout_seq.chip_status_obj = chip_status_obj;
						      startstop_internal_readout_seq.reporting_enable_obj = reporting_enable_obj;
						      startstop_internal_readout_seq.config_matrix_obj = config_matrix_obj;

            // delay to avoid that READOUT_pad arrives with the rising edge of the clock, which would cause a violation at the 2-FF
												// synchronizer that comes after the redout ext/end multiplexer (readoutStart), which would be translated into a 1-clock
												// cycle X, which would then be sampled by the logic afterwards and propagated eternally 
            //#(`CLK_HALF_PERIOD_40MHz);

            // After reset, the chip is by default configured to readout from the slow control.
            // We have already readout twice (the second time, all zeros) by applying a slow control command.
            // Try to readout a third time by applying a pulse at READOUT_pad -> nothing should happen
            chip_status_obj.chip_status = STATUS_THIRD_READOUT_NOTHING_HAPPENS; 
            chip_status_obj.chip_status_update_event.trigger(); 
			         `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
            startstop_external_readout_seq.start( .sequencer( pins_sqr ) );
         
									
									   
        
            // Configure the chip to readout by applying a pulse at READOUT_pad. 
            // Readout a fourth time (all zeros) activating from the external test pulse.
            chip_status_obj.chip_status = STATUS_FOURTH_READOUT_ALL_ZEROS; 
            chip_status_obj.chip_status_update_event.trigger(); 
			         `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
            perform_serial_readout_activatefrom_external_virtual_seq.start( .sequencer( null ) );
         
         
           // Try to readout a fifth time activating from the slow control -> nothing should happen
           chip_status_obj.chip_status = STATUS_FIFTH_READOUT_NOTHING_HAPPENS; 
           chip_status_obj.chip_status_update_event.trigger(); 
			        `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           startstop_internal_readout_seq.start( .sequencer( slowcontrol_sqr ));
         
            
		  endtask: body




endclass: CLICTD_readoutcontrol_test_virtual_sequence

`endif // __CLICTD_readoutcontrol_test_virtual_sequence_sv__





  



