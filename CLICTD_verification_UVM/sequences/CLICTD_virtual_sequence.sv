// Filename           : CLICTD_virtual_sequence.sv
// Author             : Núria Egidos 
// Created on         : 7/8/18
// Last modification  : 27/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Virtual sequence that starts other sequences arbitring the order in which
//                      they are run
// Derived from       : http://cluelogic.com/2012/01/uvm-tutorial-for-candy-lovers-virtual-sequence/
//                      https://blogs.synopsys.com/vip-central/2015/03/31/virtual-sequences-in-uvm-why-how/
//                      https://blogs.synopsys.com/vip-central/2015/03/26/reusable-sequences-in-uvm/
//                      http://forums.accellera.org/topic/4755-how-to-get-virtual-interface-in-sequence/
//                      https://www.verilab.com/files/configdb_dvcon2014_1.pdf

// http://forums.accellera.org/topic/1246-how-to-pass-the-value-to-the-variable-of-uvm_sequence-object/
// Sequences/sequence_items do not have hierarchy until they have been started on a sequencer.   
// Once started, get_full_name() will return a hierarchy string for your sequence that includes either 
// the parent sequence's hierarchy (if there is a parent sequence), or the hierarchy of the sequencer 
// that the sequence was started on.

`ifndef __CLICTD_virtual_sequence_sv__
`define __CLICTD_virtual_sequence_sv__



class CLICTD_virtual_sequence extends uvm_sequence#(uvm_sequence_item);

      CLICTD_slowcontrol_sequencer slowcontrol_sqr;
      CLICTD_pins_sequencer pins_sqr;
      
      // Sequence to apply reset pulse
      CLICTD_apply_reset_pulse_sequence apply_reset_pulse_seq;
      // Sequence to read all the slow control registers
      CLICTD_readAllSlowcontrolReg_sequence readAllSlowcontrolReg_seq;
      // Sequence to activate serial readout using an external pulse
      CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence perform_serial_readout_activatefrom_external_virtual_seq;
      // Sequence to activate serial readout with a slow control command
      CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence perform_serial_readout_activatefrom_slowcontrol_virtual_seq;
      // Sequence to configure (both halves of the configuration bits) the matrix
      CLICTD_matrixconfig_virtual_sequence matrixconfig_virtual_seq;
      // Sequence to perform the readout test
      CLICTD_readout_test_virtual_sequence readout_test_virtual_seq;
						// Sequence to perform the readout test (with coverage collection)
						CLICTD_readout_test_virtual_sequence_for_coverage readout_test_virtual_seq_for_coverage;
      // Sequence to perform the readout control test
      CLICTD_readoutcontrol_test_virtual_sequence readoutcontrol_test_virtual_seq;
      // Sequence to perform the write and read slow control registers test
      CLICTD_wrregisters_test_sequence wrregisters_test_seq;
      // Sequence to perform the power pulse control test
      CLICTD_powercontrol_test_virtual_sequence powercontrol_test_virtual_seq;
						// Sequence to perform the test pulse control test
      CLICTD_testpulsecontrol_test_virtual_sequence testpulsecontrol_test_virtual_seq;
						// Sequence to check if there are timing violations at the reset synchronizer
						CLICTD_reset_synchronizer_violations_sequence reset_synchronizer_violations_seq;
						// Sequence to check if there are timing violations at the readoutStart synchronizer (after the multiplexer that selects between the pulse at READOUT_pad and 
						// the internal slow control signal)
						CLICTD_readoutstart_synchronizer_violations_virtual_sequence readoutstart_synchronizer_violations_virtual_seq;

      string testname;
      
      
      ChipStatusInfo chip_status_obj;
      CLICTDevents events_obj;
      BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
      ReportingEnable reporting_enable_obj;
      FieldsConfigureMatrix config_matrix_obj;
						FieldsTimeMeasurements time_measurements_obj;
						FieldsConfigureMatrix_for_coverage config_matrix_obj_for_coverage;
						FieldsTimeMeasurements_for_coverage time_measurements_obj_for_coverage;

      addresses_registers_slowcontrol_type addresses_registers_slowcontrol;

      // Constructor
      function new(string name = "CLICTD_virtual_sequence");
           super.new(name);
           //bits_slowcontrol_reg_obj = new();
      endfunction: new
     
     
      task body();
			  												 // At this point, the UVM hierarchy is already built, 
															   // so it should be possible to retrieve the interface
															   // from the database

															   apply_reset_pulse_seq = CLICTD_apply_reset_pulse_sequence::type_id::create( .name( "apply_reset_pulse_seq" ), .contxt( get_full_name() ) );
																		
																		reset_synchronizer_violations_seq = CLICTD_reset_synchronizer_violations_sequence::type_id::create( .name( "reset_synchronizer_violations_seq" ), .contxt( get_full_name() ) );
																		reset_synchronizer_violations_seq.chip_status_obj = chip_status_obj;
																		reset_synchronizer_violations_seq.events_obj = events_obj;
																		
																		
																		readoutstart_synchronizer_violations_virtual_seq = CLICTD_readoutstart_synchronizer_violations_virtual_sequence::type_id::create( .name( "readoutstart_synchronizer_violations_virtual_seq" ), .contxt( get_full_name() ) );
																		readoutstart_synchronizer_violations_virtual_seq.chip_status_obj = chip_status_obj;
																		readoutstart_synchronizer_violations_virtual_seq.events_obj = events_obj;
																		readoutstart_synchronizer_violations_virtual_seq.pins_sqr = pins_sqr;
																		readoutstart_synchronizer_violations_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
																		readoutstart_synchronizer_violations_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;    
          								readoutstart_synchronizer_violations_virtual_seq.reporting_enable_obj = reporting_enable_obj;
          								readoutstart_synchronizer_violations_virtual_seq.config_matrix_obj = config_matrix_obj;
										
										
																		

      												readAllSlowcontrolReg_seq = CLICTD_readAllSlowcontrolReg_sequence::type_id::create( .name( "readAllSlowcontrolReg_seq" ), .contxt( get_full_name() ) );
      												readAllSlowcontrolReg_seq.addresses_registers_slowcontrol = addresses_registers_slowcontrol;

      												perform_serial_readout_activatefrom_slowcontrol_virtual_seq = CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence::type_id::create( .name( "perform_serial_readout_activatefrom_slowcontrol_virtual_seq" ), .contxt( get_full_name() ) );
      												perform_serial_readout_activatefrom_slowcontrol_virtual_seq.chip_status_obj = chip_status_obj;
      												perform_serial_readout_activatefrom_slowcontrol_virtual_seq.events_obj = events_obj;
      												perform_serial_readout_activatefrom_slowcontrol_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
      												perform_serial_readout_activatefrom_slowcontrol_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;





      												perform_serial_readout_activatefrom_external_virtual_seq = CLICTD_perform_serial_readout_activatefrom_external_virtual_sequence::type_id::create( .name( "perform_serial_readout_activatefrom_external_virtual_seq" ), .contxt( get_full_name() ) );
      												perform_serial_readout_activatefrom_external_virtual_seq.events_obj = events_obj;
      												perform_serial_readout_activatefrom_external_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
      												perform_serial_readout_activatefrom_external_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
      												perform_serial_readout_activatefrom_external_virtual_seq.pins_sqr = pins_sqr;



      												readout_test_virtual_seq = CLICTD_readout_test_virtual_sequence::type_id::create( .name( "readout_test_virtual_seq" ), .contxt( get_full_name() ) );
      												readout_test_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
      												readout_test_virtual_seq.chip_status_obj = chip_status_obj;
      												readout_test_virtual_seq.events_obj = events_obj;
      												readout_test_virtual_seq.reporting_enable_obj = reporting_enable_obj;
      												readout_test_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
      												readout_test_virtual_seq.pins_sqr = pins_sqr;
																		readout_test_virtual_seq.time_measurements_obj = time_measurements_obj;
                  readout_test_virtual_seq.config_matrix_obj = config_matrix_obj;

    
																		
																		readout_test_virtual_seq_for_coverage = CLICTD_readout_test_virtual_sequence_for_coverage::type_id::create( .name( "readout_test_virtual_seq_for_coverage" ), .contxt( get_full_name() ) );
      												readout_test_virtual_seq_for_coverage.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
      												readout_test_virtual_seq_for_coverage.chip_status_obj = chip_status_obj;
      												readout_test_virtual_seq_for_coverage.events_obj = events_obj;
      												readout_test_virtual_seq_for_coverage.reporting_enable_obj = reporting_enable_obj;
      												readout_test_virtual_seq_for_coverage.slowcontrol_sqr = slowcontrol_sqr;
      												readout_test_virtual_seq_for_coverage.pins_sqr = pins_sqr;
																		readout_test_virtual_seq_for_coverage.time_measurements_obj = time_measurements_obj;
                  readout_test_virtual_seq_for_coverage.config_matrix_obj = config_matrix_obj;
																		readout_test_virtual_seq_for_coverage.time_measurements_obj_for_coverage = time_measurements_obj_for_coverage;
                  readout_test_virtual_seq_for_coverage.config_matrix_obj_for_coverage = config_matrix_obj_for_coverage;
																		
																		
																		/*
																		for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						    													for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																												$display("CLICTD_virtual_seq config_matrix_obj - ss = %0d sis = %0d, tpEnableDigital = %h, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						          																						ss, sis, config_matrix_obj.tpEnableDigital[0][ss][sis], config_matrix_obj.mask[0][ss][sis], config_matrix_obj.tpEnableAnalog[0][ss][sis], config_matrix_obj.tuningDAC0[0][ss][sis], config_matrix_obj.tuningDAC1[0][ss][sis],
																																						config_matrix_obj.tuningDAC2[0][ss][sis], config_matrix_obj.tuningDAC3[0][ss][sis], config_matrix_obj.tuningDAC4[0][ss][sis], config_matrix_obj.tuningDAC5[0][ss][sis], config_matrix_obj.tuningDAC6[0][ss][sis],
																																						config_matrix_obj.tuningDAC7[0][ss][sis]);
																							end
																			end
																			*/
                   // At this point, config_matrix_obj has the same values as in the test



      												// IT HAS THIS VALUE BY DEFAULT
      												//chip_status_obj.chip_status = STATUS_RESET;
      												//chip_status_obj.chip_status_update_event.trigger();


      												// The instructions inside the fork happen for all tests

      												`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
			 												  //uvm_config_db#(ChipStatusInfo)::set( .cntxt(null), .inst_name("*"), .field_name("chip_status_obj"), .value(chip_status_obj) );
																		
																		//$display("DISPLAY - virtual seq - %s",testname);
																		if (testname == `RESET_SYNCHRONIZER_VIOLATIONS_TEST) begin
																		         reset_synchronizer_violations_seq.start( .sequencer( pins_sqr ) );
																		end
																		else if (testname == `READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST) begin
																		         readoutstart_synchronizer_violations_virtual_seq.start( .sequencer( null ) );
																		end
																		else begin
      																	fork
      																	begin
			     											   						// Apply reset pulse     
			     												   					`uvm_info("apply_reset_pulse", $sformatf("Reset applied at time: %12d", $time), UVM_LOW);
           																	assert( apply_reset_pulse_seq.randomize() );
																												
           																	apply_reset_pulse_seq.start( .sequencer( pins_sqr ) );
			     											   						`uvm_info("apply_reset_pulse", $sformatf("Reset released at time: %12d", $time), UVM_LOW);
      																	end
      																	begin
			     											   						// Read all slow control registers
			     												   					// We start when the reset is released, to make sure it has arrived to all blocks
           																	events_obj.reset_released_event.wait_trigger();
				   												    					readAllSlowcontrolReg_seq.start( .sequencer( slowcontrol_sqr ));
														   						end
														   						join
                   end



														   	// From now on, perform actions corresponding to each test in particular

      												if(testname != `RESET_TEST && testname != `RESET_SYNCHRONIZER_VIOLATIONS_TEST && testname != `READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST && testname != `WRREGISTERS_TEST && testname != `POWERCONTROL_TEST) begin

         												chip_status_obj.chip_status = STATUS_FIRST_READOUT_UNDEFINED; 
         												chip_status_obj.chip_status_update_event.trigger(); // not necessary, no monitor performs any action at this chip status
			   											   	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
         												// the first time we read out, the matrix is reset by shifting zeros to all its positions, 
         												// but what we read out is the previous value, which is undefined in this case
			   										   		perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( null ));


          												//reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES;

          												//reporting_enable_obj.enable_reporting_slowcontroldriver = `YES; // Reporting in slowcontrol_driver
          												chip_status_obj.chip_status = STATUS_SECOND_READOUT_ALL_ZEROS; 
          												chip_status_obj.chip_status_update_event.trigger(); //trigger from within the sequence, so that the start readout condition has already occurred
		      										  		`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
			    											   	// the second time we read out, we are reading out the zeros shifted with the first time, so we are reading all zeros
			    										   		perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( null ));
      												end
																		
																		
																		if (testname == `CONFIGURATION_TEST || testname == `READOUT_TEST || testname == `READOUT_TEST_FOR_COVERAGE || testname == `TESTPULSECONTROL_TEST) begin
																		
																		     matrixconfig_virtual_seq = CLICTD_matrixconfig_virtual_sequence::type_id::create( .name("matrixconfig_virtual_seq"), .contxt( get_full_name() ) );
           												matrixconfig_virtual_seq.chip_status_obj = chip_status_obj;
				       												matrixconfig_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
           												matrixconfig_virtual_seq.reporting_enable_obj = reporting_enable_obj;
				       												matrixconfig_virtual_seq.config_matrix_obj = config_matrix_obj;
																							matrixconfig_virtual_seq.config_matrix_obj_for_coverage = config_matrix_obj_for_coverage;
           												matrixconfig_virtual_seq.events_obj = events_obj;
           												matrixconfig_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
           												matrixconfig_virtual_seq.ENABLE_CONFIGURE_FIRST_HALF = `YES;
           												if (testname == `CONFIGURATION_TEST) matrixconfig_virtual_seq.ENABLE_CONFIGURE_SECOND_HALF = `YES;
																							else matrixconfig_virtual_seq.ENABLE_CONFIGURE_SECOND_HALF = `NO; // skip second configuration half and its readout to speed up simulation
																							matrixconfig_virtual_seq.testname = testname;
																							
																							
																							/*
																							for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						    																		for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																																	$display("CLICTD_virtual_seq config_matrix_obj - ss = %0d sis = %0d, tpEnableDigital = %h, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						          																											ss, sis, config_matrix_obj.tpEnableDigital[0][ss][sis], config_matrix_obj.mask[0][ss][sis], config_matrix_obj.tpEnableAnalog[0][ss][sis], config_matrix_obj.tuningDAC0[0][ss][sis], config_matrix_obj.tuningDAC1[0][ss][sis],
																																											config_matrix_obj.tuningDAC2[0][ss][sis], config_matrix_obj.tuningDAC3[0][ss][sis], config_matrix_obj.tuningDAC4[0][ss][sis], config_matrix_obj.tuningDAC5[0][ss][sis], config_matrix_obj.tuningDAC6[0][ss][sis],
																																											config_matrix_obj.tuningDAC7[0][ss][sis]);
																												end
																								end
																								*/
																								// At this point, config_matrix_obj has the same values as in the test
																							
																							// Configuration test
														   						if (testname == `CONFIGURATION_TEST) begin
           																	//reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES; // Reporting in dataout_monitor, breakdown_readout_bits
           																	//reporting_enable_obj.enable_reporting_slowcontroldriver = `YES; // Reporting in slowcontrol_driver
           																	//reporting_enable_obj.enable_reporting_slowcontroldriver = `NO;
																												reporting_enable_obj.enable_reporting_matrixmodelcomp = `YES;
			        																	//config_matrix_obj.matrix_config_fields = FIXED_DEBUG;
           																	matrixconfig_virtual_seq.start( .sequencer( null ));
																												/*
																												for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						    																							for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																																						$display("CLICTD_virtual_seq config_matrix_obj - ss = %0d sis = %0d, tpEnableDigital = %h, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						          																																ss, sis, config_matrix_obj.tpEnableDigital[0][ss][sis], config_matrix_obj.mask[0][ss][sis], config_matrix_obj.tpEnableAnalog[0][ss][sis], config_matrix_obj.tuningDAC0[0][ss][sis], config_matrix_obj.tuningDAC1[0][ss][sis],
																																																config_matrix_obj.tuningDAC2[0][ss][sis], config_matrix_obj.tuningDAC3[0][ss][sis], config_matrix_obj.tuningDAC4[0][ss][sis], config_matrix_obj.tuningDAC5[0][ss][sis], config_matrix_obj.tuningDAC6[0][ss][sis],
																																																config_matrix_obj.tuningDAC7[0][ss][sis]);
																																	end
																													end
																													*/
																													// At this point, config_matrix_obj has the same values as in the test
														  						 end
																							
																							
																							// Readout test: apply hits and check the readout pattern
														  							else if (testname == `READOUT_TEST) begin

																	      						// No need to randomize these sequences because config_matrix_obj and time_measurements_obj have already been randomized at CLICTD_test

           																		//reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES; // Reporting in dataout_monitor, breakdown_readout_bits
           																		//reporting_enable_obj.enable_reporting_slowcontroldriver = `YES; // Reporting in slowcontrol_driver
           																		//reporting_enable_obj.enable_reporting_slowcontroldriver = `NO;
																													//reporting_enable_obj.enable_reporting_dataoutcomparator_readoutacquired = `YES;

           																		//config_matrix_obj.matrix_config_fields = RANDOM_HITPATTERN;


																													//assert( matrixconfig_virtual_seq.randomize() ); // at this point, the matrix_config_fields inside this sequence will be randomised and the obtained values will be 
																													// updated to the matrix_config_field in the uppermost virtual sequence. When the readout_test_virtual_seq is created, the matrix_config_fields provided to it already
																													// contains the randomised values, so no additional call to randomize() is needed for that sequence!
           																		matrixconfig_virtual_seq.start( .sequencer( null ));
																													//$display("virtual seq 1 - toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
								         												//						time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
																													//						config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
																													//						config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
																													//						config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
																													//						config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse);
																													
																												reporting_enable_obj.enable_reporting_dataoutcomparator_readoutacquired = `YES;
																													//reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES;

	          																		//assert( readout_test_virtual_seq.randomize() );
																													readout_test_virtual_seq.config_matrix_obj = config_matrix_obj; // this is assigned after randomize() to avoid changing the values obtained when matrixconfig_virtual_seq was randomized
																													readout_test_virtual_seq.time_measurements_obj = time_measurements_obj;
																													//$display("virtual seq 2 - toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
								         												//						time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
																													//						config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
																													//						config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
																													//						config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
																													//						config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse);
			        																		readout_test_virtual_seq.start( .sequencer( null ) );

														  							end
																							
																							else if (testname == `READOUT_TEST_FOR_COVERAGE) begin
																							      // No need to randomize these sequences because config_matrix_obj and time_measurements_obj have already been randomized at CLICTD_test

           																		//reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES; // Reporting in dataout_monitor, breakdown_readout_bits
           																		//reporting_enable_obj.enable_reporting_slowcontroldriver = `YES; // Reporting in slowcontrol_driver
           																		//reporting_enable_obj.enable_reporting_slowcontroldriver = `NO;
																													//reporting_enable_obj.enable_reporting_dataoutcomparator_readoutacquired = `YES;

           																		//config_matrix_obj.matrix_config_fields = RANDOM_HITPATTERN;


																													//assert( matrixconfig_virtual_seq.randomize() ); // at this point, the matrix_config_fields inside this sequence will be randomised and the obtained values will be 
																													// updated to the matrix_config_field in the uppermost virtual sequence. When the readout_test_virtual_seq is created, the matrix_config_fields provided to it already
																													// contains the randomised values, so no additional call to randomize() is needed for that sequence!
           																		matrixconfig_virtual_seq.start( .sequencer( null ));
																													//$display("virtual seq 1 - toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
								         												//						time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
																													//						config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
																													//						config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
																													//						config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
																													//						config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse);
																													
																												reporting_enable_obj.enable_reporting_dataoutcomparator_readoutacquired = `YES;
																													//reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES;

	          																		//assert( readout_test_virtual_seq.randomize() );
																													readout_test_virtual_seq.config_matrix_obj = config_matrix_obj; // this is assigned after randomize() to avoid changing the values obtained when matrixconfig_virtual_seq was randomized
																													readout_test_virtual_seq.time_measurements_obj = time_measurements_obj;
																													//$display("virtual seq 2 - toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
								         												//						time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
																													//						config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
																													//						config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
																													//						config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
																													//						config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse);
			        																		readout_test_virtual_seq_for_coverage.start( .sequencer( null ) );
																							end
																							
																							else if (testname == `TESTPULSECONTROL_TEST) begin
																							       // Configure matrix to enable digital test pulse in one column
																														config_matrix_obj.matrix_config_fields = RANDOM_HITPATTERN;
																														matrixconfig_virtual_seq.start( .sequencer( null ));
																							end
																							
																		end

														   	
																	 
																  // Readout control test
      											 if (testname == `READOUTCONTROL_TEST) begin
         													readoutcontrol_test_virtual_seq = CLICTD_readoutcontrol_test_virtual_sequence::type_id::create( .name("readoutcontrol_test_virtual_seq"), .contxt( get_full_name() ) );
         													readoutcontrol_test_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
         													readoutcontrol_test_virtual_seq.pins_sqr = pins_sqr;
         													readoutcontrol_test_virtual_seq.chip_status_obj = chip_status_obj;
				 																	readoutcontrol_test_virtual_seq.events_obj = events_obj;
				 																	readoutcontrol_test_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
				 																	readoutcontrol_test_virtual_seq.reporting_enable_obj = reporting_enable_obj;
				 																	readoutcontrol_test_virtual_seq.config_matrix_obj = config_matrix_obj;
         													readoutcontrol_test_virtual_seq.start( .sequencer( null ) );
      													end
																	

														 	 

      												// Write and read slow control registers test
      												if (testname == `WRREGISTERS_TEST) begin
         												wrregisters_test_seq = CLICTD_wrregisters_test_sequence::type_id::create( .name("wrregisters_test_seq"), .contxt( get_full_name() ) );
         												wrregisters_test_seq.chip_status_obj = chip_status_obj;
         												wrregisters_test_seq.addresses_registers_slowcontrol = addresses_registers_slowcontrol;
         												assert( wrregisters_test_seq.randomize() );
         												wrregisters_test_seq.start( .sequencer( slowcontrol_sqr ) );
      												end

      												// Power pulsing control test
      												if (testname == `POWERCONTROL_TEST) begin


         												powercontrol_test_virtual_seq = CLICTD_powercontrol_test_virtual_sequence::type_id::create( .name("powercontrol_test_virtual_seq"), .contxt( get_full_name() ) );
         												powercontrol_test_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
         												powercontrol_test_virtual_seq.pins_sqr = pins_sqr;
         												powercontrol_test_virtual_seq.chip_status_obj = chip_status_obj;
         												powercontrol_test_virtual_seq.events_obj = events_obj;
				 																powercontrol_test_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
				 																powercontrol_test_virtual_seq.reporting_enable_obj = reporting_enable_obj;
				 																powercontrol_test_virtual_seq.config_matrix_obj = config_matrix_obj;
         												assert( powercontrol_test_virtual_seq.randomize() );
         												powercontrol_test_virtual_seq.start( .sequencer( null ) );


     												end
																	
																	// Test pulsing control test
      												if (testname == `TESTPULSECONTROL_TEST) begin

                     
         												testpulsecontrol_test_virtual_seq = CLICTD_testpulsecontrol_test_virtual_sequence::type_id::create( .name("testpulsecontrol_test_virtual_seq"), .contxt( get_full_name() ) );
         												testpulsecontrol_test_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
         												testpulsecontrol_test_virtual_seq.pins_sqr = pins_sqr;
         												testpulsecontrol_test_virtual_seq.chip_status_obj = chip_status_obj;
         												testpulsecontrol_test_virtual_seq.events_obj = events_obj;
				 																testpulsecontrol_test_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
				 																testpulsecontrol_test_virtual_seq.reporting_enable_obj = reporting_enable_obj;
				 																testpulsecontrol_test_virtual_seq.config_matrix_obj = config_matrix_obj;
																					testpulsecontrol_test_virtual_seq.time_measurements_obj = time_measurements_obj;
         												testpulsecontrol_test_virtual_seq.start( .sequencer( null ) );
                     
     												end


      
			endtask: body
			
			`uvm_object_utils_begin( CLICTD_virtual_sequence )
      //`uvm_field_object( readAllSlowcontrolReg_seq, UVM_ALL_ON )
      //`uvm_field_object( chip_status_obj, UVM_REFERENCE )
      //`uvm_field_enum( chip_status_type, chip_status, UVM_REFERENCE )
      //`uvm_field_int( pwrExtEn, UVM_ALL_ON )
      //`uvm_field_int( tpExtEn, UVM_ALL_ON )
      //`uvm_field_int( tpEn, UVM_ALL_ON )
      //`uvm_field_int( tpInt, UVM_ALL_ON )
      //`uvm_field_int( photonCnt, UVM_ALL_ON )
      //`uvm_field_int( noCompr, UVM_ALL_ON )
      //`uvm_field_int( longCnt, UVM_ALL_ON )
      //`uvm_field_int( totDiv, UVM_ALL_ON )
      //`uvm_field_int( configSend, UVM_ALL_ON )
      //`uvm_field_int( configStage, UVM_ALL_ON )
      //`uvm_field_int( roExtEn, UVM_ALL_ON )
      //`uvm_field_int( roInt, UVM_ALL_ON )
      
      
      
      `uvm_object_utils_end

endclass: CLICTD_virtual_sequence

`endif // __CLICTD_virtual_sequence_sv__
