// Filename           : CLICTD_apply_reset_pulse_sequence.sv
// Author             : Núria Egidos 
// Created on         : 22/8/18
// Last modification  : 22/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to apply a pulse active low at RSTN_pad



`ifndef __CLICTD_apply_reset_pulse_sequence_sv__
`define __CLICTD_apply_reset_pulse_sequence_sv__



class CLICTD_apply_reset_pulse_sequence extends uvm_sequence#(CLICTD_pins_transaction);
     `uvm_object_utils(CLICTD_apply_reset_pulse_sequence) // Register the component to the factory


  

     CLICTD_pins_transaction pins_trans;

     rand int time_duration_of_reset;
     //constraint cons_duration_reset { time_duration_of_reset inside {[0:`RESET_DURATION_NUMTIMEUNITS]}; }
					constraint cons_duration_reset { time_duration_of_reset inside {[8000:10000]}; } // "long" reset required
					// for the signal to be propagated across the whole periphery

     // Constructor
     function new(string name = "CLICTD_apply_reset_pulse_sequence");
          super.new(name);
     endfunction: new


     task body ();

					//int time_duration_of_reset = $urandom_range(`RESET_DURATION_NUMTIMEUNITS,0); // max, min
          
										
										
										// Default values for signals
										pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
										start_item(pins_trans);
          pins_trans.RSTN_pad = 1'b1;
										pins_trans.PWREN_pad = 1'b1;     
          pins_trans.READOUT_pad = 1'b0;	
		        pins_trans.SHUTTER_pad = 1'b0;
          pins_trans.disc = '0; 
          pins_trans.TPULSE_pad = 1'b0;
          finish_item(pins_trans); 
										
										#(`DELAY_NS_USED_TO_AVOID_VIOLATIONS_DUE_TO_ASYNC_SIGNALS);

          #500;
										pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
          start_item(pins_trans);
          pins_trans.RSTN_pad = 1'b0;
										pins_trans.PWREN_pad = 1'b1;     
          pins_trans.READOUT_pad = 1'b0;	
		        pins_trans.SHUTTER_pad = 1'b0;
          pins_trans.disc = '0; 
          pins_trans.TPULSE_pad = 1'b0;
          finish_item(pins_trans);
          
				  #(time_duration_of_reset);
						
          pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
          start_item(pins_trans);
          pins_trans.RSTN_pad = 1'b1;
										pins_trans.PWREN_pad = 1'b1;     
          pins_trans.READOUT_pad = 1'b0;	
		        pins_trans.SHUTTER_pad = 1'b0;
          pins_trans.disc = '0; 
          pins_trans.TPULSE_pad = 1'b0;
          finish_item(pins_trans);
          #500;

     endtask: body




endclass: CLICTD_apply_reset_pulse_sequence

`endif // __CLICTD_apply_reset_pulse_sequence_sv__
