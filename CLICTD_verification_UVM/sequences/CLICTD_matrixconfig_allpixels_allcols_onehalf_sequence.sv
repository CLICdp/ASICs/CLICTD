// Filename           : CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence.sv
// Author             : Núria Egidos 
// Created on         : 10/8/18
// Last modification  : 21/1/19
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to perform one configuration half of the matrix
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence_sv__
`define __CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence_sv__



class CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence) // Register the component to the factory

     string testname;
					
					FieldsConfigureMatrix_for_coverage config_matrix_obj_for_coverage;

     // Signals to store the status of the matrix configuration 
     int config_bit_counter; // // count configuration bits as they are sent, from `NUMBER_OF_FF_PER_SUPERPIXEL-1 to 0
     int aux_config_bit_counter = 0; // count configuration bits as they are sent, from 0 to `NUMBER_OF_FF_PER_SUPERPIXEL-1
     int superpixel_segment_counter = 0;
     int superpixel_in_segment_counter = 0;
     logic [`COLUMNS-1:0] config_word;

     

     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_perform_serial_readout_activatefrom_slowcontrol_sequence");
          super.new(name);
     endfunction: new


		


		// - - - - - - - - - - - - Convert the randomized configuration into the bits to send through the slow control
    // interface (via transactions to the slowcontrol_driver)
      function void convert_from_configbits_to_configword();
						 		
									  // $display("DISPLAY - MATRIXCONFIG_ALLPIXELS_ALLCOLS_ONEHALF_SEQ - testname = %s",testname);
							    if (testname == `READOUT_TEST_FOR_COVERAGE) begin: readout_for_coverage
																						
							     								if (bits_slowcontrol_reg_obj.configStage == `FIRST_CONFIGURATION_HALF) begin: first_config_half_for_coverage
																												if      (config_bit_counter == `MATRIXCONFIG_tpEnableDigital)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tpEnableDigital[i][superpixel_segment_counter][superpixel_in_segment_counter];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask0)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask1)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask2)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][2]; 
                  										else if (config_bit_counter == `MATRIXCONFIG_mask3)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][3];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask4)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][4];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask5)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][5];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask6)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][6];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask7)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][7];
																												else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog0)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog1)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog2)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog3)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][3];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog4)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][4];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog5)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][5];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog6)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][6];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog7)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][7];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC00)            for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tuningDAC0     [i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC01)            for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tuningDAC0     [i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC02)            for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tuningDAC0     [i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC10)            for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj_for_coverage.tuningDAC1     [i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_dummy_bit_21_firsthalf) for (int i = 0; i < `COLUMNS; i++) config_word[i] =  {`COLUMNS{config_matrix_obj_for_coverage.value_for_dummy_bit}};
              								end: first_config_half_for_coverage

              								else begin: second_config_half_for_coverage
																												if      (config_bit_counter == `MATRIXCONFIG_dummy_bit_0_secondhalf)  config_word = {`COLUMNS{config_matrix_obj_for_coverage.value_for_dummy_bit}};
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC11)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC1[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC12)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC1[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC20)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC2[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC21)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC2[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC22)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC2[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC30)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC3[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC31)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC3[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC32)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC3[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
																												else if (config_bit_counter == `MATRIXCONFIG_tuningDAC40)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC4[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC41)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC4[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC42)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC4[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC50)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC5[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC51)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC5[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC52)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC5[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC60)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC6[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC61)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC6[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC62)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC6[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC70)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC7[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC71)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC7[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC72)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj_for_coverage.tuningDAC7[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_dummy_bit_21_secondhalf) config_word = {`COLUMNS{config_matrix_obj.value_for_dummy_bit}};
              								end: second_config_half_for_coverage
											
											end: readout_for_coverage
											
											else begin: other_tests

							     								if (bits_slowcontrol_reg_obj.configStage == `FIRST_CONFIGURATION_HALF) begin: first_config_half_other_tests
																												if      (config_bit_counter == `MATRIXCONFIG_tpEnableDigital)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tpEnableDigital[i][superpixel_segment_counter][superpixel_in_segment_counter];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask0)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask1)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask2)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][2]; 
                  										else if (config_bit_counter == `MATRIXCONFIG_mask3)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][3];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask4)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][4];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask5)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][5];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask6)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][6];
                  										else if (config_bit_counter == `MATRIXCONFIG_mask7)                  for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.mask           [i][superpixel_segment_counter][superpixel_in_segment_counter][7];
																												else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog0)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog1)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog2)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog3)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][3];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog4)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][4];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog5)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][5];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog6)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][6];
                  										else if (config_bit_counter == `MATRIXCONFIG_tpEnableAnalog7)        for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tpEnableAnalog [i][superpixel_segment_counter][superpixel_in_segment_counter][7];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC00)            for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tuningDAC0     [i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC01)            for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tuningDAC0     [i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC02)            for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tuningDAC0     [i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC10)            for (int i = 0; i < `COLUMNS; i++) config_word[i] =  config_matrix_obj.tuningDAC1     [i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_dummy_bit_21_firsthalf) for (int i = 0; i < `COLUMNS; i++) config_word[i] =  {`COLUMNS{config_matrix_obj.value_for_dummy_bit}};
              								end: first_config_half_other_tests

              								else begin: second_config_half_other_tests
																												if      (config_bit_counter == `MATRIXCONFIG_dummy_bit_0_secondhalf)  config_word = {`COLUMNS{config_matrix_obj.value_for_dummy_bit}};
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC11)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC1[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC12)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC1[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC20)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC2[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC21)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC2[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC22)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC2[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC30)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC3[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC31)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC3[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC32)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC3[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
																												else if (config_bit_counter == `MATRIXCONFIG_tuningDAC40)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC4[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC41)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC4[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC42)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC4[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC50)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC5[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC51)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC5[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC52)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC5[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC60)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC6[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC61)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC6[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC62)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC6[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC70)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC7[i][superpixel_segment_counter][superpixel_in_segment_counter][0];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC71)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC7[i][superpixel_segment_counter][superpixel_in_segment_counter][1];
                  										else if (config_bit_counter == `MATRIXCONFIG_tuningDAC72)             for (int i = 0; i < `COLUMNS; i++) config_word[i] = config_matrix_obj.tuningDAC7[i][superpixel_segment_counter][superpixel_in_segment_counter][2];
                  										else if (config_bit_counter == `MATRIXCONFIG_dummy_bit_21_secondhalf) config_word = {`COLUMNS{config_matrix_obj.value_for_dummy_bit}};
              								end: second_config_half_other_tests
																						
            end: other_tests

      endfunction: convert_from_configbits_to_configword
     

      // -----------------------------------------------------------------------------
			// Matrix configuration: configure one bit, one pixel, all columns 
			// (the position of the pixel and which bit to configure are not specified, 
			// but these are determined inherently by the time when this task is called,
			// i.e.: it will be called first for the first configuration bit of the first
			// configuration half of pixel 0; next, for the second configuration bit of 
			// the first configuration half of pixel 1, etc.)
			// -----------------------------------------------------------------------------
			task matrixconfig_onebit_onesuperpixel_allcols_onehalf();
		
				       // Write register configData[7:0]: set the value of the corresponding bit for columns 7-0
               send_slowcontrol_transaction_write(.register_address(`ADDRESS_configData7downto0), .register_content(config_word[7:0]));
				       // Write register configData[15:8]: set the value of the corresponding bit for columns 15-8
               send_slowcontrol_transaction_write(.register_address(`ADDRESS_configData15downto8), .register_content(config_word[15:8]));
															
															
															//$display("matrixconfig_all_seq slow control - sent word: %h",config_word);

				       // Write configCtrl to set configSend to 1
				       bits_slowcontrol_reg_obj.configSend = 1'b1;
               send_slowcontrol_transaction_write(.register_address(`ADDRESS_configCtrl), .register_content({3'b0, bits_slowcontrol_reg_obj.configSend, 2'b0, bits_slowcontrol_reg_obj.configStage}));

				       // configSend is high during the time required by the I2C to send the command  

				       // Write configCtrl to clear configSend to 0
				       bits_slowcontrol_reg_obj.configSend = 1'b0;
				       send_slowcontrol_transaction_write(.register_address(`ADDRESS_configCtrl), .register_content({3'b0, bits_slowcontrol_reg_obj.configSend, 2'b0, bits_slowcontrol_reg_obj.configStage}));

               /*
				       // Update configuration status
				       if (config_bit_counter > 0) config_bit_counter = config_bit_counter-1;
				       else begin 
                    // All the FFs have been filled with the configuration word corresponding to this half
                    config_bit_counter = `NUMBER_OF_FF_PER_SUPERPIXEL-1;
                    if (superpixel_in_segment_counter < `SUPERPIXELS_IN_ONE_SEGMENT) superpixel_in_segment_counter = superpixel_in_segment_counter + 1;
                    else begin
                         superpixel_in_segment_counter = 0;
                         if (superpixel_segment_counter < `SUPERPIXEL_SEGMENTS) superpixel_segment_counter = superpixel_segment_counter + 1;
                         else `uvm_info("Configuration sequence", $sformatf("Configuration half %d finished, chip_status = %s, %s", bits_slowcontrol_reg_obj.configSend, chip_status_obj.chip_status, this.get_full_name()), UVM_LOW)
                    end
                
               end
							 */
				       

			endtask: matrixconfig_onebit_onesuperpixel_allcols_onehalf


      // - - - - - - - - - - - - 
						task matrixconfig_allsuperpixels_allcols_onehalf();

           `uvm_info("Config sequence - seq", $sformatf("Configuration half %d started, chip_status = %s, %s", bits_slowcontrol_reg_obj.configStage, chip_status_obj.chip_status, this.get_full_name()), UVM_LOW)
           //config_bit_counter = `NUMBER_OF_FF_PER_SUPERPIXEL-1;
           for ( superpixel_segment_counter = 0; superpixel_segment_counter < `SUPERPIXEL_SEGMENTS; superpixel_segment_counter++ ) begin: for_superpixel_segment_counter
                  for ( superpixel_in_segment_counter = 0; superpixel_in_segment_counter < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment_counter++) begin: for_superpixel_in_segment_counter
																		
																		      /*
																							$display("matrixconfig_all_onehalf_seq config_matrix_obj - ss = %0d sis = %0d, tpEnableDigital = %h, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						          																										superpixel_segment_counter, superpixel_in_segment_counter, config_matrix_obj.tpEnableDigital[0][superpixel_segment_counter][superpixel_in_segment_counter],
																																									 config_matrix_obj.mask[0][superpixel_segment_counter][superpixel_in_segment_counter], config_matrix_obj.tpEnableAnalog[0][superpixel_segment_counter][superpixel_in_segment_counter],
																																									 config_matrix_obj.tuningDAC0[0][superpixel_segment_counter][superpixel_in_segment_counter], config_matrix_obj.tuningDAC1[0][superpixel_segment_counter][superpixel_in_segment_counter],
																																										config_matrix_obj.tuningDAC2[0][superpixel_segment_counter][superpixel_in_segment_counter], config_matrix_obj.tuningDAC3[0][superpixel_segment_counter][superpixel_in_segment_counter],
																																									 config_matrix_obj.tuningDAC4[0][superpixel_segment_counter][superpixel_in_segment_counter], config_matrix_obj.tuningDAC5[0][superpixel_segment_counter][superpixel_in_segment_counter],
																																									 config_matrix_obj.tuningDAC6[0][superpixel_segment_counter][superpixel_in_segment_counter],
																																										config_matrix_obj.tuningDAC7[0][superpixel_segment_counter][superpixel_in_segment_counter]);
																							*/
																							// At this point, config_matrix_obj has the same values as in the test	

                      // For each superpixel, we will send `NUMBER_OF_FF_PER_SUPERPIXEL bits, starting from MSB
                      for (aux_config_bit_counter = 0; aux_config_bit_counter <  `NUMBER_OF_FF_PER_SUPERPIXEL; aux_config_bit_counter++) begin: for_config_bit_counter
												              //f$display("Loop matrixconfig_allpixels_allcols_onehalf, superpixel_index = %d",superpixel_index );
                          config_bit_counter = `NUMBER_OF_FF_PER_SUPERPIXEL - 1 - aux_config_bit_counter;
                          //config_bit_counter = aux_config_bit_counter;
								                  convert_from_configbits_to_configword(); // update config_word
                          if (reporting_enable_obj.enable_reporting_matrixconfigonehalfseq == `YES) begin
																              `uvm_info("Config sequence - seq", $sformatf("Configuration half %d ongoing, config_word = %h, aux_config_bit_counter = %d, config_bit_counter = %d, superpixel_in_segment_counter = %d, superpixel_segment_counter = %d, chip_status = %s",
																														  bits_slowcontrol_reg_obj.configStage, config_word, aux_config_bit_counter, config_bit_counter, superpixel_in_segment_counter, superpixel_segment_counter, chip_status_obj.chip_status), UVM_LOW)
                          end
												              matrixconfig_onebit_onesuperpixel_allcols_onehalf(); // send config_word via the slow control
                          if (config_bit_counter == 0  && superpixel_in_segment_counter == `SUPERPIXELS_IN_ONE_SEGMENT - 1 && superpixel_segment_counter == `SUPERPIXEL_SEGMENTS - 1) begin
                             `uvm_info("Config sequence - seq", $sformatf("Configuration half %d finished, chip_status = %s, %s", bits_slowcontrol_reg_obj.configStage, chip_status_obj.chip_status, this.get_full_name()), UVM_LOW)
                          end
										  end: for_config_bit_counter
									end: for_superpixel_in_segment_counter
				   end: for_superpixel_segment_counter
      endtask: matrixconfig_allsuperpixels_allcols_onehalf




		  task body ();
      
            if(chip_status_obj.chip_status == STATUS_MATRIX_CONFIG_FIRST_HALF) bits_slowcontrol_reg_obj.configStage = `FIRST_CONFIGURATION_HALF;
            else bits_slowcontrol_reg_obj.configStage = `SECOND_CONFIGURATION_HALF;
            // Set configSend to 0 to ensure that it's ready to start the configuration of the matrix
            bits_slowcontrol_reg_obj.configSend = 1'b0;
            send_slowcontrol_transaction_write(.register_address(`ADDRESS_configCtrl), .register_content({3'b0, bits_slowcontrol_reg_obj.configSend, 2'b0, bits_slowcontrol_reg_obj.configStage}));

            
												matrixconfig_allsuperpixels_allcols_onehalf();


               // At this point, we have finished the present configuration half, so we restore configStage to a value that switches 
               // the chip to a non-configuration state (for instance, acquisition or readout)
															
															// Introduce some artificial delay to force that this signal changes slightly later and try to understand if this is the cause of the timing violation
															// # UVM_INFO ../sequences//CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence.sv(237) @ 938826816324: uvm_test_top.env.slowcontrol_agent.slowcontrol_sequencer@@matrixconfig_allpixels_allcols_onehalf_seq [Config sequence - seq] 
															// Configuration half 2 finished, chip_status =   STATUS_MATRIX_CONFIG_SECOND_HALF, uvm_test_top.env.slowcontrol_agent.slowcontrol_sequencer.matrixconfig_allpixels_allcols_onehalf_seq
               // # ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(9186): $setup( negedge D:938856138624 ps, posedge CK:938856138640 ps, 88 ps );
               // #    Time: 938856138819 ps  Iteration: 0  Instance: /CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/\config_word_reg[0]
															// (observed in the typ corner for netlistPostlayout_20190130v2 netlist)
               //#10;
       
															
               bits_slowcontrol_reg_obj.configStage = 2'b00;
               send_slowcontrol_transaction_write(.register_address(`ADDRESS_configCtrl), .register_content({3'b0, bits_slowcontrol_reg_obj.configSend, 2'b0, bits_slowcontrol_reg_obj.configStage}));
															
											/*				
											for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						    					for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																				$display("matrixconfig_all_seq config_matrix_obj - ss = %0d sis = %0d, tpEnableDigital = %h, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						          														ss, sis, config_matrix_obj.tpEnableDigital[0][ss][sis], config_matrix_obj.mask[0][ss][sis], config_matrix_obj.tpEnableAnalog[0][ss][sis], config_matrix_obj.tuningDAC0[0][ss][sis], config_matrix_obj.tuningDAC1[0][ss][sis],
																														config_matrix_obj.tuningDAC2[0][ss][sis], config_matrix_obj.tuningDAC3[0][ss][sis], config_matrix_obj.tuningDAC4[0][ss][sis], config_matrix_obj.tuningDAC5[0][ss][sis], config_matrix_obj.tuningDAC6[0][ss][sis],
																														config_matrix_obj.tuningDAC7[0][ss][sis]);
															end
											end
											*/
											// At this point, config_matrix_obj has the same values as in the test	
            

		  endtask: body




endclass: CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence

`endif // __CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence_sv__





  



