// Filename           : CLICTD_readoutstart_synchronizer_violations_virtual_sequence.sv
// Author             : N�ria Egidos 
// Created on         : 7/12/18
// Last modification  : 7/12/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to apply multiple readoutstart pulses at READOUT_pad, with the rising and falling edges at a certain distance
//                      from the rising edge of the 40 MHz clock (a separation short enough to trigger possible timing 
//                      violations at the readoutstart synchronizer)



`ifndef __CLICTD_readoutstart_synchronizer_violations_virtual_sequence_sv__
`define __CLICTD_readoutstart_synchronizer_violations_virtual_sequence_sv__



class CLICTD_readoutstart_synchronizer_violations_virtual_sequence extends uvm_sequence#(uvm_sequence_item);
     `uvm_object_utils(CLICTD_readoutstart_synchronizer_violations_virtual_sequence) // Register the component to the factory

     CLICTD_slowcontrol_sequencer slowcontrol_sqr;
     CLICTD_pins_sequencer pins_sqr;
						
					ChipStatusInfo chip_status_obj;
					CLICTDevents events_obj;
					BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
     ReportingEnable reporting_enable_obj;
     FieldsConfigureMatrix config_matrix_obj;
			
     // Sequence to apply reset pulse
     CLICTD_apply_reset_pulse_sequence apply_reset_pulse_seq;
					
					// Sequence to configure readout to be activated from READOUT_pad
					CLICTD_configure_external_readout_sequence configure_external_readout_seq;
					
					// Sequence to apply multiple READOUT_pad pulses in a row, changing the distance with respect to the rising edge of the clock
					// so as to detect timing violations in the readoutStart synchronizer
					CLICTD_apply_readoutpad_pulses_sequence apply_readoutpad_pulses_seq;
						
     
     // Constructor
     function new(string name = "CLICTD_readoutstart_synchronizer_violations_virtual_sequence");
          super.new(name);
     endfunction: new
     			
			

     task body ();
          
										uvm_event finish_simulation_event = new("finish_simulation_event");
										
										apply_reset_pulse_seq = CLICTD_apply_reset_pulse_sequence::type_id::create( .name( "apply_reset_pulse_seq" ), .contxt( get_full_name() ) );
      				
										
										configure_external_readout_seq = CLICTD_configure_external_readout_sequence::type_id::create( .name( "configure_external_readout_seq" ), .contxt( get_full_name() ) );
      				configure_external_readout_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
      				configure_external_readout_seq.chip_status_obj = chip_status_obj;
          configure_external_readout_seq.reporting_enable_obj = reporting_enable_obj;
          configure_external_readout_seq.config_matrix_obj = config_matrix_obj;
					
										
										
										
										apply_readoutpad_pulses_seq = CLICTD_apply_readoutpad_pulses_sequence::type_id::create( .name( "apply_readoutpad_pulses_seq" ), .contxt( get_full_name() ) );
      				apply_readoutpad_pulses_seq.chip_status_obj = chip_status_obj;
      				apply_readoutpad_pulses_seq.events_obj = events_obj;
      										
										
										
										//We need to apply a reset pulse to enable the propagation of the clock to the reset synchronizer,
										// otherwise the clock doesn't reach it and the rest of this test was not working!
										assert( apply_reset_pulse_seq.randomize() );
										apply_reset_pulse_seq.start( .sequencer(pins_sqr) );
																				
																				
										// Configure readout to be activated from READOUT_pad, where we will apply the different readout pulses
										// that will come at a certain distace (close) from the clock rising edge
										configure_external_readout_seq.start( .sequencer(slowcontrol_sqr) );
										
										// Apply the pulses at READOUT_pad
										apply_readoutpad_pulses_seq.start( .sequencer(pins_sqr) );																		
										
										
     endtask: body




endclass: CLICTD_readoutstart_synchronizer_violations_virtual_sequence

`endif // __CLICTD_readoutstart_synchronizer_violations_virtual_sequence_sv__
