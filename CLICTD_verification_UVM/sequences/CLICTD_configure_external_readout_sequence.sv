// Filename           : CLICTD_configure_external_readout_sequence.sv
// Author             : Núria Egidos 
// Created on         : 22/8/18
// Last modification  : 22/8/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to configure the serial readout to be performed by applying an external test pulse
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_configure_external_readout_sequence_sv__
`define __CLICTD_configure_external_readout_sequence_sv__



class CLICTD_configure_external_readout_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_configure_external_readout_sequence) // Register the component to the factory

    
     // Constructor
     function new(string name = "CLICTD_configure_external_readout_sequence");
          super.new(name);
     endfunction: new


     task body ();
          sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));

          // Enable readout activated externally (set roExtEn in readoutCtrl)
          bits_slowcontrol_reg_obj.roExtEn = 1'b1; bits_slowcontrol_reg_obj.roInt = 1'b0;
          send_slowcontrol_transaction_write( .register_address(`ADDRESS_readoutCtrl), .register_content({6'b0, bits_slowcontrol_reg_obj.roExtEn, bits_slowcontrol_reg_obj.roInt}) );

     endtask: body




endclass: CLICTD_configure_external_readout_sequence

`endif // __CLICTD_configure_external_readout_sequence_sv__
