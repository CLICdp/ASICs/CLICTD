// Filename           : CLICTD_startstop_external_powerpulse_sequence.sv
// Author             : Núria Egidos 
// Created on         : 28/9/18
// Last modification  : 28/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to start/stop the power pulsing by applying an external test pulse
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_startstop_external_powerpulse_sequence_sv__
`define __CLICTD_startstop_external_powerpulse_sequence_sv__



class CLICTD_startstop_external_powerpulse_sequence extends uvm_sequence#(CLICTD_pins_transaction);
     `uvm_object_utils(CLICTD_startstop_external_powerpulse_sequence) // Register the component to the factory

     CLICTD_pins_transaction pins_trans;
     CLICTDevents events_obj;

     rand int time_wait_after_power_change;
     constraint cons_duration_wait { time_wait_after_power_change inside {[5000:10000]}; }

     // Constructor
     function new(string name = "CLICTD_startstop_external_powerpulse_sequence");
          super.new(name);
     endfunction: new


     task body ();
           // Force a pulse at PWREN_pad
    
           /*
           // Initial, idle state
           pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
           start_item(pins_trans);
           pins_trans.PWREN_pad = 1'b1;
           finish_item(pins_trans);

           #10000;
           */

           // Start power pulse
           pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
           start_item(pins_trans);
           pins_trans.PWREN_pad = 1'b0;
											// Default values
											pins_trans.TPULSE_pad = 1'b0;
											pins_trans.RSTN_pad = 1'b1;
											pins_trans.READOUT_pad = 1'b0;
											pins_trans.SHUTTER_pad = 1'b0;
           finish_item(pins_trans);

           // The duration of the power pulse will be determined by the I2C transfer. We add some additional delay here
           // to ensure that the time that the pulse is active (low) is at least 10 us for the circuit to be able to
           // process it properly
           #(time_wait_after_power_change); // 10 us  

           // Stop power pulse
           pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
           start_item(pins_trans);
           pins_trans.PWREN_pad = 1'b1;
											// Default values
											pins_trans.TPULSE_pad = 1'b0;
											pins_trans.RSTN_pad = 1'b1;
											pins_trans.READOUT_pad = 1'b0;
											pins_trans.SHUTTER_pad = 1'b0;
           finish_item(pins_trans);
           
           // This event is used to indicate when to finish the sequence (it is triggered from CLICTD_pins_comparator). 
           // For STATUS_POWERPULSE_EXTERNAL_WORKING, we know that 3 transactions are to be handled, so when this number 
           // is reached, we can stop the sequence. This is required because a delay is to be introduced afterwards, and 
           // if the sequence is not stopped, the delay may occur "in parallel" and lead to triggering timeouts that shouldn't happen
           //events_obj.finish_sequence_power_pulsing_event.wait_trigger(); 
           //events_obj.finish_sequence_power_pulsing_event.reset();

     endtask: body




endclass: CLICTD_startstop_external_powerpulse_sequence

`endif // __CLICTD_startstop_external_powerpulse_sequence_sv__
