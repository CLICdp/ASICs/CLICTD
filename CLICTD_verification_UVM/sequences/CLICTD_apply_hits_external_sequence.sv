// Filename           : CLICTD_apply_hits_external_sequence.sv
// Author             : Núria Egidos 
// Created on         : 19/10/18
// Last modification  : 19/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to apply hits to the matrix, forcing digital test pulses (applied from TPULSE_pad) and pulses at the disc bits

// Common constraints: https://www.chipverify.com/systemverilog/constraint-examples-sv

`ifndef __CLICTD_apply_hits_external_sequence_sv__
`define __CLICTD_apply_hits_external_sequence_sv__



class CLICTD_apply_hits_external_sequence extends uvm_sequence#(CLICTD_pins_transaction);
     `uvm_object_utils(CLICTD_apply_hits_external_sequence) // Register the component to the factory


  

     CLICTD_pins_transaction pins_trans;
	
					FieldsTimeMeasurements time_measurements_obj;
					
					FieldsConfigureMatrix config_matrix_obj;


     // Constructor
     function new(string name = "CLICTD_apply_hits_external_sequence");
          super.new(name);
     endfunction: new


     task body ();

        //  uvm_info expected tot, toa
								
								
								//$display("apply hits seq - toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
								//         time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
								//									config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
								//									config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
								//									config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
								//									config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse);

        pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
								
								// Default values
								pins_trans.PWREN_pad = 1'b1;     // Pad to provide the power pulse
        pins_trans.READOUT_pad = 1'b0;	// Pad to activate readout with an external pulse
        pins_trans.RSTN_pad = 1'b1;
								
								
								
        start_item(pins_trans);
        pins_trans.SHUTTER_pad = 1'b0;
        pins_trans.disc = '0; 
        pins_trans.TPULSE_pad = 1'b0;
        finish_item(pins_trans);
        #(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);

        // Columns 1-8, segment 0, superpixel 0, bit 0; and digital test pulse rise before the shutter -> this first pulse won't be taken into account
        start_item(pins_trans);
        pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1;
        pins_trans.TPULSE_pad = 1'b1; 
        finish_item(pins_trans);
        #(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);

        // TIME POINT 1 = t0
        // START OF SHUTTER
        start_item(pins_trans);
        pins_trans.SHUTTER_pad = 1'b1;
        finish_item(pins_trans);
								
								
								// TIME POINT 2 = TIME POINT 1 + 100
        #(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
        // Columns 1-8, segment 0, superpixel 0, bit 0; and digital test pulse fall
        start_item(pins_trans);
        pins_trans.TPULSE_pad = 1'b0;
        pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0;
        finish_item(pins_trans);

        // TIME POINT 3 = TIME POINT 2 + delay_to_startpulses_after_shutter_ns - 100
        #(time_measurements_obj.delay_to_startpulses_after_shutter_ns-`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
        // MAIN PULSE -> USED TO COMPUTE TOT, TOA, PHOTON COUNT
        start_item(pins_trans);
        pins_trans.TPULSE_pad = 1'b1;
        // Columns 1-8, segment 0, superpixel 0, bit 0 rises 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1;
        finish_item(pins_trans);
								
								
								
								
								
								if (time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1) begin: one_pulse_in_shutter
								
								
								            // TIME POINT 4 = TIME POINT 3 + 100
        												#(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
																				start_item(pins_trans);
        												// Columns 1-8, segment 0, superpixel 0, bit 1 rises -> it will be superimposed to bit 0 in the OR of disc
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1;  
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1;
																				finish_item(pins_trans);

        												// TIME POINT 5 = TIME POINT 4 + 100
        												#(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
																				start_item(pins_trans);
        												// Columns 1-8, segment 0, superpixel 0, bit 0 falls -> the OR of disc still high with bit 1
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0;  
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0;
																				finish_item(pins_trans);
								
								            // TIME POINT 6 = TIME POINT 5 + tot_short_ns - 200
        												#(time_measurements_obj.tot_short_ns-2*`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION); // tot must be larger than 100 ns/DELAY_FACTOR_TOT = 5, to make it detectable let's set the minimum tot at 120 ns/DELAY_FACTOR_TOT = 6
        												// Columns 1-4, segment 0, superpixel 0, bit 1 falls -> the OR of disc falls; and digital test pulse fall
																				start_item(pins_trans);
        												if (time_measurements_obj.digital_pulse_lasts_longer_than_shutter == `NO) pins_trans.TPULSE_pad = 1'b0;
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
																				finish_item(pins_trans);
																				
																				// TIME POINT 9 = TIME POINT 6 + (toa_ns - tot_short_ns - 100)
																				#(time_measurements_obj.toa_ns - time_measurements_obj.tot_short_ns);
																				start_item(pins_trans);
        												// END OF SHUTTER
        												pins_trans.SHUTTER_pad = 1'b0;
        												finish_item(pins_trans);
								            
																				// TIME POINT 10 = TIME POINT 9 + (tot_long_ns - toa_ns)
        												#(time_measurements_obj.tot_long_ns - time_measurements_obj.toa_ns);
																				start_item(pins_trans);
        												pins_trans.TPULSE_pad = 1'b0;
        												// Columns 1-4, segment 0, superpixel 0, bit 0 falls
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
        												// Columns 5-8, segment 0, superpixel 0, bit 1 falls
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
        												finish_item(pins_trans);
																				
								end: one_pulse_in_shutter
								else begin: two_pulses_in_shutter
								
								            // TIME POINT 4 = TIME POINT 3 + time_measurements_obj.tot_short_ns/4
        												#(time_measurements_obj.tot_short_ns/4);
																				start_item(pins_trans);
        												// Columns 1-8, segment 0, superpixel 0, bit 0 falls 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0;  
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0;
																				finish_item(pins_trans);
																				
																				
																				// TIME POINT 5 = TIME POINT 4 + 100
        												#(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
																				start_item(pins_trans);
        												// Columns 1-8, segment 0, superpixel 0, bit 1 rises 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1;  
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1;
																				finish_item(pins_trans);
																				
																				// TIME POINT 6 = TIME POINT 5 + tot_short_ns/4 - 100
																				#(time_measurements_obj.tot_short_ns/4 - `MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
																				start_item(pins_trans);
        												// Digital test pulse falls
        												pins_trans.TPULSE_pad = 1'b0; 
																				finish_item(pins_trans);
																				
																				
																				
																				// TIME POINT 7 = TIME POINT 6 + 100
																				#(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
																				start_item(pins_trans);
        												// Columns 1-4, segment 0, superpixel 0, bit 1 falls
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0;  
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0;  
																				finish_item(pins_trans);
																				
																				
																				// TIME POINT 8 = TIME POINT 7 + 100
																				#(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
																				start_item(pins_trans);
        												// Digital test pulse rises; columns 1-4, segment 0, superpixel 0, bit 0 rises
																				pins_trans.TPULSE_pad = 1'b1;
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1;  
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b1;  
																				finish_item(pins_trans);
																				
																				
																				// TIME POINT 9 = TIME POINT 8 + toa - tot_short_ns/2 - 200
																				#(time_measurements_obj.toa_ns - time_measurements_obj.tot_short_ns - 2*`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
																				start_item(pins_trans);
        												// END OF SHUTTER
																				pins_trans.SHUTTER_pad = 1'b0;
																				finish_item(pins_trans);
																				
																				
																				// TIME POINT 10 = TIME POINT 9 + 100
																				#(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
																				start_item(pins_trans);
        												// Fall digital test pulse; columns 1-4 segment 0, superpixel 0, bit 0 falls; columns 5-8 segment 0, superpixel 0, bit 1 falls
																				pins_trans.TPULSE_pad = 1'b0;
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0;  
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_0] = 1'b0;
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0;  
        												pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
																				pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0;
																				finish_item(pins_trans);
																				
								            
								end: two_pulses_in_shutter

        
								
        
								
        
								
        // TIME POINT 11 = TIME POINT 10 + 100
        #100;
								start_item(pins_trans);
        // This pulse comes outside the shutter, so it won't be handled
        pins_trans.TPULSE_pad = 1'b1;
        // Columns 1-8, segment 0, superpixel 0, bit 1 rises -> this event won't be handled because it arrives outside of the shutter
        pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b1;
        finish_item(pins_trans);
								
        // TIME POINT 12 = TIME POINT 11 + 100
        #100;
								start_item(pins_trans);
        pins_trans.TPULSE_pad = 1'b0;
        // Columns 1-8, segment 0, superpixel 0, bit 1 falls 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[1]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[2]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[3]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[4]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[5]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[6]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
        pins_trans.disc[config_matrix_obj.columns_to_pulse[7]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0; 
								pins_trans.disc[config_matrix_obj.columns_to_pulse[8]][config_matrix_obj.ss_applydiscpulse][config_matrix_obj.sis_applydiscpulse][config_matrix_obj.bit_applydiscpulse_1] = 1'b0;
								finish_item(pins_trans);
        #100;

     endtask: body




endclass: CLICTD_apply_hits_external_sequence

`endif // __CLICTD_apply_hits_external_sequence_sv__
