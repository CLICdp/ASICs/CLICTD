// Filename           : CLICTD_apply_readoutpad_pulses_sequence.sv
// Author             : N�ria Egidos 
// Created on         : 7/12/18
// Last modification  : 7/12/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to apply multiple readoutstart pulses at READOUT_pad, with the rising and falling edges at a certain distance
//                      from the rising edge of the 40 MHz clock (a separation short enough to trigger possible timing 
//                      violations at the readoutstart synchronizer)



`ifndef __CLICTD_apply_readoutpad_pulses_sequence_sv__
`define __CLICTD_apply_readoutpad_pulses_sequence_sv__

// It was observed that the delay between the command is given to change the value of READOUT_pad or the clock and the time when 
// it actually changes can vary between executions in the order of hundreds of picoseconds. To try to counteract it, an artificial 
// delay of an amount measured for each corner is introduced so that the behavior is the same as that observed (without this artificial
/// delay) in tbaux_multipleresetpulses.sv)


`define DELAY_MANUAL_CORRECTION_NOSDF_NS 0.4
`define DELAY_MANUAL_CORRECTION_MIN_NS 0.4
`define DELAY_MANUAL_CORRECTION_TYP_NS 0.72
`define DELAY_MANUAL_CORRECTION_MAX_NS 0.8


class CLICTD_apply_readoutpad_pulses_sequence extends uvm_sequence#(CLICTD_pins_transaction);
     `uvm_object_utils(CLICTD_apply_readoutpad_pulses_sequence) // Register the component to the factory

     
					ChipStatusInfo chip_status_obj;
					CLICTDevents events_obj;
					

     CLICTD_pins_transaction pins_trans;
					
					shortreal actual_duration_low_level,aux_distance_between_clocks, delay_manual_correction;
					shortreal setup_duration, hold_duration,distance_readoutrise_and_clockrise,distance_readoutfall_and_clockrise,aux_time_falledge_clk40; // shorteal is used to be able to handle decimal numbers
			  int counter_falling_edges_clk40,counter_readout_pulses;
			
     // Sequence to apply reset pulse
     CLICTD_apply_reset_pulse_sequence apply_reset_pulse_seq;
						
     
     // Constructor
     function new(string name = "CLICTD_apply_readoutpad_pulses_sequence");
          super.new(name);
     endfunction: new
     			
			
			
			task detect_distance_readoutupdate_and_clockrise();
			     shortreal aux_distance_readoutrise_and_clockrise, aux_distance_readoutfall_and_clockrise; // shorteal is used to be able to handle decimal numbers
								
								
														
			     forever begin
								       
															events_obj.rise_readoutstart_event.wait_trigger();
															if (hold_duration == 0 && setup_duration == 0) begin
																			distance_readoutrise_and_clockrise = 0;
																			`uvm_info("readoutstart RISE", $sformatf("A - Distance between readoutstart rise and clk rising edge: %f, readoutstart pulse %0d/%0d, chip_status = %s",
																				 distance_readoutrise_and_clockrise,counter_readout_pulses,`EXPECTED_NUMBER_OF_READOUT_PULSES,chip_status_obj.chip_status),  UVM_LOW);
															end
															else if (setup_duration > 0) begin
																				aux_distance_readoutrise_and_clockrise = $realtime;
																				events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																				distance_readoutrise_and_clockrise = $realtime - aux_distance_readoutrise_and_clockrise;
																				`uvm_info("readoutstart RISE", $sformatf("B - Distance between readoutstart rise and clk rising edge: %f, setup duration %f, readoutstart pulse %0d/%0d, chip_status = %s", distance_readoutrise_and_clockrise,
																					setup_duration,counter_readout_pulses,`EXPECTED_NUMBER_OF_READOUT_PULSES,chip_status_obj.chip_status),  UVM_LOW);
															end
															else if (hold_duration > 0) begin
																				aux_distance_readoutrise_and_clockrise = $realtime;
																				events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																				distance_readoutrise_and_clockrise = 2*(`CLK_HALF_PERIOD_40MHz) - ($realtime - aux_distance_readoutrise_and_clockrise);
																				`uvm_info("readoutstart RISE", $sformatf("C - Distance between readoutstart rise and clk rising edge: %f, hold duration %f, readoutstart pulse %0d/%0d, chip_status = %s", distance_readoutrise_and_clockrise,
																					hold_duration-`INCREMENT_STEP_HOLD_DURATION,counter_readout_pulses,`EXPECTED_NUMBER_OF_READOUT_PULSES,chip_status_obj.chip_status),  UVM_LOW);
																					
															end
																
																
																
																events_obj.fall_readoutstart_event.wait_trigger();
																if (hold_duration == 0 && setup_duration == 0) begin
																				distance_readoutfall_and_clockrise = 0;
																				`uvm_info("readoutstart FALL", $sformatf("A - Distance between readoutstart fall and clk rising edge: %f, readoutstart pulse %0d/%0d, chip_status = %s",
																				 distance_readoutfall_and_clockrise,counter_readout_pulses,`EXPECTED_NUMBER_OF_READOUT_PULSES,chip_status_obj.chip_status),  UVM_LOW);
																end
																else if (setup_duration > 0) begin
																				 aux_distance_readoutfall_and_clockrise = $realtime;
																				 events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																					distance_readoutfall_and_clockrise = $realtime - aux_distance_readoutfall_and_clockrise;
																				`uvm_info("readoutstart FALL", $sformatf("B - Distance between readoutstart fall and clk rising edge: %f, setup duration %f, readoutstart pulse %0d/%0d, chip_status = %s", distance_readoutfall_and_clockrise,
																					setup_duration,counter_readout_pulses,`EXPECTED_NUMBER_OF_READOUT_PULSES,chip_status_obj.chip_status),  UVM_LOW);
																end
																else if (hold_duration > 0) begin
																				 aux_distance_readoutfall_and_clockrise = $realtime;
																					events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																					distance_readoutfall_and_clockrise = 2*(`CLK_HALF_PERIOD_40MHz) - ($realtime - aux_distance_readoutfall_and_clockrise);
																					`uvm_info("readoutstart FALL", $sformatf("C - Distance between readoutstart fall and clk rising edge: %f, hold duration %f, readoutstart pulse %0d/%0d, chip_status = %s", distance_readoutfall_and_clockrise,
																					hold_duration,counter_readout_pulses,`EXPECTED_NUMBER_OF_READOUT_PULSES,chip_status_obj.chip_status),  UVM_LOW);
																end
																
																
																
								end
			endtask: detect_distance_readoutupdate_and_clockrise
			
			
			
			
			
			

     task body ();
          
										uvm_event finish_simulation_event = new("finish_simulation_event");
										
										
																														
										fork
										    begin
														     events_obj.fall_digitalperi_roctrl_clk40_event.wait_trigger();
																			events_obj.fall_digitalperi_roctrl_clk40_event.wait_trigger();
																			actual_duration_low_level = $realtime;
																			events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																			actual_duration_low_level = $realtime - actual_duration_low_level;
																			//$display("DISPLAY - readout start sync seq - %t actual duration low level %f",$realtime,actual_duration_low_level);
																			
																			events_obj.rise_clkout_event.wait_trigger();
																			aux_distance_between_clocks = $realtime;
																			//$display("DISPLAY - readoutstart sync seq - %t rise clk pad",$realtime);
																			events_obj.fall_clkout_event.wait_trigger();
																			//$display("DISPLAY - readoutstart sync seq - %t fall clk pad",$realtime);
																			events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																			aux_distance_between_clocks = $realtime - aux_distance_between_clocks;
																			//$display("DISPLAY - readoutstart sync seq - %t rise clk digital peri",$realtime);
																			
																			if ( aux_distance_between_clocks > `CLK_HALF_PERIOD_40MHz)  begin 
																			    $display("DISPLAY - readoutstart sync seq - %t aux_distance_between_clocks BEFORE = %f, aux_distance_between_clocks AFTER = %f",$realtime,aux_distance_between_clocks,aux_distance_between_clocks-2*actual_duration_low_level);
																			
																			    aux_distance_between_clocks = aux_distance_between_clocks - 2*actual_duration_low_level; // since the separation of 
																			    // edges is very small, it may have skipped it and searched in the following period
																			end
																			if ( aux_distance_between_clocks < 0 ) aux_distance_between_clocks = -aux_distance_between_clocks;
																			//if ( aux_distance_between_clocks < 2) delay_manual_correction = `DELAY_MANUAL_CORRECTION_NOSDF_NS; // measured distance min: 2.396 ns
																			//else if ( aux_distance_between_clocks < 2.5) delay_manual_correction = `DELAY_MANUAL_CORRECTION_MIN_NS; // measured distance typ: 2.826 ns
																			//else if ( aux_distance_between_clocks < 3) delay_manual_correction = `DELAY_MANUAL_CORRECTION_TYP_NS; // measured distance max: 3.925 ns
																			//else delay_manual_correction = `DELAY_MANUAL_CORRECTION_MAX_NS;
																			
																			
																			
																			
																			if ( aux_distance_between_clocks > 2.2) delay_manual_correction = `DELAY_MANUAL_CORRECTION_MIN_NS; // measured distance min: 2.396 ns
																			else if ( aux_distance_between_clocks > 1.5) delay_manual_correction = `DELAY_MANUAL_CORRECTION_TYP_NS; // measured distance typ: 2.826 ns
																			else if ( aux_distance_between_clocks > 1) delay_manual_correction = `DELAY_MANUAL_CORRECTION_MAX_NS; // measured distance max: 3.925 ns
																			else delay_manual_correction = `DELAY_MANUAL_CORRECTION_NOSDF_NS;
																			
																			
																			$display("DISPLAY - readoutstart sync seq - %t aux_distance_between_clocks = %f, delay_manual_correction = %f",$realtime,aux_distance_between_clocks,delay_manual_correction);
																			
																			finish_simulation_event.wait_trigger();
														end
										    begin
														     detect_distance_readoutupdate_and_clockrise();
														end
														begin
														      pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
														
														      // Some delay to ensure that the correct value of actual_duration_low_level is used
																				events_obj.fall_digitalperi_roctrl_clk40_event.wait_trigger();
																			 events_obj.fall_digitalperi_roctrl_clk40_event.wait_trigger();
																				events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																				events_obj.rise_clkout_event.wait_trigger();
																				events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
								
																				// Default values
																			pins_trans.RSTN_pad = 1'b1;
																			pins_trans.PWREN_pad = 1'b1;     
          									pins_trans.READOUT_pad = 1'b0;	
		        									pins_trans.SHUTTER_pad = 1'b0;
          									pins_trans.disc = '0; 
          									pins_trans.TPULSE_pad = 1'b0;
																				
														      // Maximum values in ns observed in clictd_output.sdf and column_output.sdf, with some margin
																			setup_duration = `MAX_SETUP_DURATION_NS; hold_duration = 0;
																			counter_readout_pulses = 0;
																			
														     // Rising and falling edges of READOUT_pad within the setup window
																			while (setup_duration > 0) begin
																			       start_item(pins_trans);
										       									pins_trans.READOUT_pad = 1'b0;
																										finish_item(pins_trans);
																										counter_falling_edges_clk40 = 0;
																										
																										
																										// When we're moving in the order of ps time differences, the delay introduced by the print function can alter the timing!
																										
																										// Rising edge of READOUT_pad
																										//events_obj.fall_digitalperi_roctrl_clk40_event.wait_trigger();  
																										events_obj.fall_clkout_event.wait_trigger(); // we use the event connected to CLK_40_p_pad instead because if we ensure setup_duration distance
																										// between READOUT_pad and CLK_40_p_pad, a similar distance should be observed between clk40 and startReadout at ROCTRL
																										//$display("DISPLAY - readout start sync seq - %t clk 40 pad fall",$realtime);
																										#(actual_duration_low_level - setup_duration - delay_manual_correction); // it was observed in simulation that there was a 0.4ns offset (not yet understood where it came from), 
																										// so it's removed to achieve the expected behavior
																										//#(actual_duration_low_level - setup_duration);
																										start_item(pins_trans); 
																										pins_trans.READOUT_pad = 1'b1; 
																										finish_item(pins_trans);
																										//$display("DISPLAY - readout start sync seq - %t actual duration - setup duration %f",$realtime,actual_duration_low_level - setup_duration);
																										counter_readout_pulses++;  
																										
																										
																										// Duration of READOUT_pad high
																										
																										// Wait 50 clock periods
																										while (counter_falling_edges_clk40 < 50) begin
																	       									//events_obj.fall_digitalperi_roctrl_clk40_event.wait_trigger();
																																	events_obj.fall_clkout_event.wait_trigger(); // we use the event connected to CLK_40_p_pad instead because if we ensure setup_duration distance
																										       // between READOUT_pad and CLK_40_p_pad, a similar distance should be observed between clk40 and startReadout at ROCTRL
																																 counter_falling_edges_clk40++;
																										end

                          // Falling edge of READOUT_pad
																										#(actual_duration_low_level - setup_duration - delay_manual_correction);
																										start_item(pins_trans);
																									 pins_trans.READOUT_pad = 1'b0;
																										finish_item(pins_trans);
																										

																										#(setup_duration);

																										// Here comes the clock rising edge

																										// Wait 50 clock periods
																										counter_falling_edges_clk40 = 0;
																										while (counter_falling_edges_clk40 < 50) begin
																	       									//events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																																	events_obj.rise_clkout_event.wait_trigger(); // we use the event connected to CLK_40_p_pad instead because if we ensure setup_duration distance
																										       // between READOUT_pad and CLK_40_p_pad, a similar distance should be observed between clk40 and startReadout at ROCTRL
																																 counter_falling_edges_clk40++;
																										end
																										
										       									// Decrement times the timescale resolution
										       									setup_duration = setup_duration - 0.001; 
																			end



																			
																			// Rising and falling edges of READOUT_pad within the hold window
																			while (hold_duration < `MAX_HOLD_DURATION_NS) begin
																			       start_item(pins_trans);
										       									pins_trans.READOUT_pad = 1'b0;
																										finish_item(pins_trans);
																										counter_falling_edges_clk40 = 0;
																										
																										
																										// Rising edge of READOUT_pad
																									 //events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																										//events_obj.rise_clkout_event.wait_trigger(); // we use the event connected to CLK_40_p_pad instead because if we ensure setup_duration distance
																										// between READOUT_pad and CLK_40_p_pad, a similar distance should be observed between clk40 and startReadout at ROCTRL
																										
																										
																										// Instead of triggering with the rising edge, we trigger with the falling edge and add the remaining half a period minus 400 ps, 
																										// which is the offset observed in simulation (not yet understood) that would make the READOUT_pad rise too late otherwise
																										events_obj.fall_clkout_event.wait_trigger();
																										#(actual_duration_low_level - delay_manual_correction);
																										
																									 #(hold_duration); 
																										start_item(pins_trans);
																										pins_trans.READOUT_pad = 1'b1;
																										finish_item(pins_trans); 
																										counter_readout_pulses++;  
																										
																										
																										// Duration of READOUT_pad high
																										
																										// Wait 50 clock periods
																										while (counter_falling_edges_clk40 < 50) begin
																	       									//events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																																	events_obj.rise_clkout_event.wait_trigger(); // we use the event connected to CLK_40_p_pad instead because if we ensure setup_duration distance
																										       // between READOUT_pad and CLK_40_p_pad, a similar distance should be observed between clk40 and startReadout at ROCTRL
																																	counter_falling_edges_clk40++;
																										end
																										
																										
																										// Instead of triggering with the rising edge, we trigger with the falling edge and add the remaining half a period minus 400 ps, 
																										// which is the offset observed in simulation (not yet understood) that would make the READOUT_pad rise too late otherwise
																										events_obj.fall_clkout_event.wait_trigger();
																										#(actual_duration_low_level - delay_manual_correction);

                          // Falling edge of READOUT_pad
																										#(hold_duration);
																										start_item(pins_trans);
																										pins_trans.READOUT_pad = 1'b0;
																									 finish_item(pins_trans);
																										

																										
																										#(`CLK_HALF_PERIOD_40MHz - hold_duration);
																										
																										
																										// Wait 50 clock periods
																										counter_falling_edges_clk40 = 0;
																										while (counter_falling_edges_clk40 < 50) begin
																	       								//events_obj.rise_digitalperi_roctrl_clk40_event.wait_trigger();
																																events_obj.rise_clkout_event.wait_trigger(); // we use the event connected to CLK_40_p_pad instead because if we ensure setup_duration distance
																										       // between READOUT_pad and CLK_40_p_pad, a similar distance should be observed between clk40 and startReadout at ROCTRL
																																counter_falling_edges_clk40++;
																										end
																										
										       									// Increment times the timescale resolution
										       									hold_duration = hold_duration + 0.001; 
																			end
																			
																			#(`CLK_HALF_PERIOD_40MHz); // to be able to measure correctly the last reset pulse in task detect_distance_readoutupdate_and_clockrise
																			
																			
																			finish_simulation_event.trigger();
														end
														
														
																			
										join_any
										disable fork;
										
										
     endtask: body




endclass: CLICTD_apply_readoutpad_pulses_sequence






`endif // __CLICTD_apply_readoutpad_pulses_sequence_sv__
