// Filename           : CLICTD_powercontrol_test_virtual_sequence.sv
// Author             : Núria Egidos 
// Created on         : 27/9/18
// Last modification  : 28/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to perform the power pulsing control test
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/


// Sample waveforms:
//                                                     at least 10 us to allow time  at least 10 us to allow time for the circuit to power off
// PWREN_pad (activated externally) or  ___________ <- for the circuit to wake up -><-______________________->
// pwrInt (activated from slow control)            |_________________________________|                      |___________
//                                          OFF                  ON                            OFF               ON
//                                      ___________                                   ______________________
// powerEnable                                     |_________________________________|                      |___________
//                                          OFF                  ON                            OFF               ON

`ifndef __CLICTD_powercontrol_test_virtual_sequence_sv__
`define __CLICTD_powercontrol_test_virtual_sequence_sv__



class CLICTD_powercontrol_test_virtual_sequence extends uvm_sequence#(uvm_sequence_item);
     `uvm_object_utils(CLICTD_powercontrol_test_virtual_sequence) // Register the component to the factory

     CLICTD_slowcontrol_sequencer slowcontrol_sqr;
     CLICTD_pins_sequencer pins_sqr;


     ChipStatusInfo chip_status_obj;
     CLICTDevents events_obj;
     BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
     ReportingEnable reporting_enable_obj;
     FieldsConfigureMatrix config_matrix_obj;

     // Sequence to configure power pulsing as to be activated using an external pulse
     CLICTD_configure_external_powerpulse_sequence configure_external_powerpulse_seq;
     // Sequence to configure power pulsing as to be activated using a slow control command
     CLICTD_configure_internal_powerpulse_sequence configure_internal_powerpulse_seq;
     // Sequence to start and stop power pulsing using an external pulse
     CLICTD_startstop_external_powerpulse_sequence startstop_external_powerpulse_seq;
     // Sequence to start and stop power pulsing using a slow control command
     CLICTD_startstop_internal_powerpulse_sequence startstop_internal_powerpulse_seq;
     // Sequence to force an initial value to PWREN_pad 
     CLICTD_force_initialvalue_pwrenpad_sequence force_initialvalue_pwrenpad_seq;
     // Dummy sequence to introduce some delay
     //CLICTD_wait_10us_dummy_sequence wait_10us_dummy_seq;


     rand int time_wait_after_power_change;
     constraint cons_duration_wait { time_wait_after_power_change inside {[5000:10000]}; }

     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_powercontrol_test_virtual_sequence");
          super.new(name);
     endfunction: new


		 task body ();


         configure_external_powerpulse_seq = CLICTD_configure_external_powerpulse_sequence::type_id::create( .name("configure_external_powerpulse_seq"), .contxt( get_full_name() ) );
         configure_external_powerpulse_seq.chip_status_obj = chip_status_obj;
				 configure_external_powerpulse_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
				 configure_external_powerpulse_seq.reporting_enable_obj = reporting_enable_obj;
				 configure_external_powerpulse_seq.config_matrix_obj = config_matrix_obj;


         configure_internal_powerpulse_seq = CLICTD_configure_internal_powerpulse_sequence::type_id::create( .name("configure_internal_powerpulse_seq"), .contxt( get_full_name() ) );
         configure_internal_powerpulse_seq.chip_status_obj = chip_status_obj;
				 configure_internal_powerpulse_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
				 configure_internal_powerpulse_seq.reporting_enable_obj = reporting_enable_obj;
				 configure_internal_powerpulse_seq.config_matrix_obj = config_matrix_obj;

         startstop_external_powerpulse_seq = CLICTD_startstop_external_powerpulse_sequence::type_id::create( .name("startstop_external_powerpulse_seq"), .contxt( get_full_name() ) );
         startstop_external_powerpulse_seq.events_obj = events_obj;
         assert( startstop_external_powerpulse_seq.randomize() );

         startstop_internal_powerpulse_seq = CLICTD_startstop_internal_powerpulse_sequence::type_id::create( .name("startstop_internal_powerpulse_seq"), .contxt( get_full_name() ) );
         startstop_internal_powerpulse_seq.chip_status_obj = chip_status_obj;
				 startstop_internal_powerpulse_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
				 startstop_internal_powerpulse_seq.reporting_enable_obj = reporting_enable_obj;
				 startstop_internal_powerpulse_seq.config_matrix_obj = config_matrix_obj;
         assert( startstop_internal_powerpulse_seq.randomize() );
    
         force_initialvalue_pwrenpad_seq = CLICTD_force_initialvalue_pwrenpad_sequence::type_id::create( .name("force_initialvalue_pwrenpad_seq"), .contxt( get_full_name() ) );

         //wait_10us_dummy_seq = CLICTD_wait_10us_dummy_sequence::type_id::create( .name("wait_10us_dummy_seq"), .contxt( get_full_name() ) );


         chip_status_obj.chip_status = STATUS_POWERPULSE_EXTERNAL_WORKING; 
         chip_status_obj.chip_status_update_event.trigger(); 
		     `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);

         #100; // wait some time before starting the sequence so that the pin driver is able to capture the
         // following event and thus enable the 100 MHz clock

         // Enable 100 MHz clock -> this is required to observe a change in CLK_100_GATED
         events_obj.enable_clock_100MHz_event.trigger();
         //$display("%t Enable clock 100 MHz! - power seq",$realtime);

         // Force an initial value to PWREN_pad before enabling external power pulsing, otherwise powerEnable 
         // would also be undefined 
         force_initialvalue_pwrenpad_seq.start( .sequencer( pins_sqr ) );


         // Configure power pulsing: enable activation from external pulse
         configure_external_powerpulse_seq.start( .sequencer( slowcontrol_sqr ) );
          
         // Apply a pulse at PWREN_pad -> check that powerEnable follows its shape
         startstop_external_powerpulse_seq.start( .sequencer( pins_sqr ) );

         // This event is used to indicate when to finish the sequence (it is triggered from CLICTD_pins_comparator). 
         // For STATUS_POWERPULSE_EXTERNAL_WORKING, we know that 3 transactions are to be handled, so when this number 
         // is reached, we can stop the sequence. This is required because a delay is to be introduced afterwards, and 
         // if the sequence is not stopped, the delay may occur "in parallel" and lead to triggering timeouts that shouldn't happen
         //events_obj.finish_sequence_power_pulsing_event.wait_trigger(); 
         //events_obj.finish_sequence_power_pulsing_event.reset();
         //startstop_external_powerpulse_seq.kill();


         //chip_status_obj.chip_status = STATUS_POWERPULSE_WAIT_LOGIC_RECOVER; 
         //chip_status_obj.chip_status_update_event.trigger(); 
		     //`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);

         //$display("%t sequence - start waiting",$realtime);
         // Wait some time for the logic to recover from power off
         #(time_wait_after_power_change); // 10 us
         //$display("%t sequence - stop waiting",$realtime);


         // If we use the plain delay (#10000;), it starts executing while the former sequence hasn't finished yet,
         // which leads to non-controlled, unexpected results (timeouts detected when they shouldn't)...
         // By creating a dummy sequence than runs on the same sequencer as the former one, we force that the 
         // useful sequence finishes before the dummy one starts, and thus the waiting time is executed after
         // the useful sequence and no interference occurs.
         //$display("%t sequence - start waiting",$realtime);
         //wait_10us_dummy_seq.start( .sequencer( pins_sqr ) );
         //$display("%t sequence - stop waiting",$realtime);
         
         // Apply a power pulse using the corresponding slow control command
         // (nothing should happen)
         chip_status_obj.chip_status = STATUS_POWERPULSE_INTERNAL_NOTHING_HAPPENS; 
         chip_status_obj.chip_status_update_event.trigger(); 
		     `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
         startstop_internal_powerpulse_seq.start( .sequencer( slowcontrol_sqr ) );

         //$display("%t sequence - start waiting",$realtime);
         // Wait some time for the logic to recover from power off
         #(time_wait_after_power_change); // 10 us
         //$display("%t sequence - stop waiting",$realtime);

         
         chip_status_obj.chip_status = STATUS_POWERPULSE_EXTERNAL_NOTHING_HAPPENS; 
         chip_status_obj.chip_status_update_event.trigger(); 
		     `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
         // Configure power pulsing: enable activation from slow control command
         configure_internal_powerpulse_seq.start( .sequencer( slowcontrol_sqr ) );


         // Apply a pulse at PWREN_pad (nothing should happen)
         startstop_external_powerpulse_seq.start( .sequencer( pins_sqr ) );


         //$display("%t sequence - start waiting",$realtime);
         // Wait some time for the logic to recover from power off
         #100000; // 100 us 
         //$display("%t sequence - stop waiting",$realtime);

         // Apply a power pulse using the corresponding slow control command
         // -> check that powerEnable follows its shape
         chip_status_obj.chip_status = STATUS_POWERPULSE_INTERNAL_WORKING; 
         chip_status_obj.chip_status_update_event.trigger(); 
		     `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
         startstop_internal_powerpulse_seq.start( .sequencer( slowcontrol_sqr ) );

         // Disable 100 MHz clock
         events_obj.disable_clock_100MHz_event.trigger();
         //$display("%t Disable clock 100 MHz! - power seq",$realtime);
         
     endtask: body



endclass: CLICTD_powercontrol_test_virtual_sequence

`endif // __CLICTD_powercontrol_test_virtual_sequence_sv__





  
