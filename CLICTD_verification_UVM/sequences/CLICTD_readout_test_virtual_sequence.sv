// Filename           : CLICTD_readout_test_virtual_sequence.sv
// Author             : Núria Egidos 
// Created on         : 29/8/18
// Last modification  : 4/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to configure the acquisition mode
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_readout_test_virtual_sequence_sv__
`define __CLICTD_readout_test_virtual_sequence_sv__



class CLICTD_readout_test_virtual_sequence extends uvm_sequence#(CLICTD_slowcontrol_transaction);
     `uvm_object_utils(CLICTD_readout_test_virtual_sequence) // Register the component to the factory

					// Sequence to configure the aquistion mode (nominal/long ToA/photon counting, compression...)
					CLICTD_configure_acquisition_mode_sequence configure_acquisition_mode_seq;
					
					// Sequence to enable the injection of test pulses and configure this injection to be performed from TPULSE_pad
     CLICTD_configure_external_testpulse_sequence configure_external_testpulse_seq;
					
					// Sequence to apply digital test pulses via TPULSE_pad and force pulses at some disc bits (on pixel coordinates 
					// specified with matrixconfig_virtual_seq). 
     CLICTD_apply_hits_external_sequence apply_hits_external_seq;


     // Sequence to activate serial readout with a slow control command
     CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence perform_serial_readout_activatefrom_slowcontrol_virtual_seq;
					

     BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
     ChipStatusInfo chip_status_obj;
     CLICTDevents events_obj;
     ReportingEnable reporting_enable_obj;

     CLICTD_slowcontrol_sequencer slowcontrol_sqr;
     CLICTD_pins_sequencer  pins_sqr;

     FieldsTimeMeasurements time_measurements_obj;
     FieldsConfigureMatrix config_matrix_obj;


     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_readout_test_virtual_sequence");
          super.new(name);
     endfunction: new


		// - - - - - - - - - - - - Actions to perform for all readout modes

      virtual task common_actions_allreadoutmodes(chip_status_type chip_status);
						
						     // Update the expected hit flag according to the compression configuration:
											// if compression is disabled, all hit flags should be 1
											if (bits_slowcontrol_reg_obj.noCompr) begin
											    for (int c = 0; c < `COLUMNS; c++) begin
															     for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																				     for (int sis =0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
						                        config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis] = 1'b1;
																									end
																				end
																				$display("DISPLAY - readout seq - expected hit flag col %0d = %h",c, config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c]);
															end
											end
											else begin
											    for (int c = 0; c < `COLUMNS; c++) begin
															     for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																				     for (int sis =0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																									     if ( c == config_matrix_obj.columns_to_pulse[0] && ss == config_matrix_obj.ss_applydigitalpulse && sis == config_matrix_obj.sis_applydigitalpulse  ||
																														     ( ( c == config_matrix_obj.columns_to_pulse[1] ||	c == config_matrix_obj.columns_to_pulse[2] || c == config_matrix_obj.columns_to_pulse[3] || c == config_matrix_obj.columns_to_pulse[5] || c == config_matrix_obj.columns_to_pulse[6] || 
																																			    c == config_matrix_obj.columns_to_pulse[7] ) &&  ss == config_matrix_obj.ss_applydiscpulse && sis == config_matrix_obj.sis_applydiscpulse 
																																				)
																																		)
						                        config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis] = 1'b1;
																														else config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis] = 1'b0;
																									end
																				end
																				$display("DISPLAY - readout seq - expected hit map col %0d = %h",c, config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c]);
															end
											end

           // Send the acquisition configuration to the slow control
           chip_status_obj.chip_status = STATUS_CONFIG_ACQUISITION_MODE; 
		         chip_status_obj.chip_status_update_event.trigger();
           `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           configure_acquisition_mode_seq.start(  .sequencer(slowcontrol_sqr) );
           
           
           // Enable 100 MHz clock
           events_obj.enable_clock_100MHz_event.trigger();
											
											// Wait some time for the clock to propagate across the chip
											#100;
           
           // Force hits at the matrix (activated from external test pulse)
											// The last digital test pulse inside the shutter finishes before the shutter falls
											time_measurements_obj.digital_pulse_lasts_longer_than_shutter = `NO;
											if ( chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0 && chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1 && 
													   chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2 && chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3 &&
													   chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0 && chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1 && 
													   chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2 && chip_status != STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3 )
                chip_status_obj.chip_status = STATUS_FORCE_HIT_SHORTTOT; 
											else chip_status_obj.chip_status = STATUS_FORCE_HIT_LONGTOT;
		         chip_status_obj.chip_status_update_event.trigger();
           `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           apply_hits_external_seq.start(  .sequencer(pins_sqr) );
											
											
											// Wait some time before deactivating the clock 
											#100;

           // Disable 100 MHz clock
           events_obj.disable_clock_100MHz_event.trigger();
											

           // Readout acquisition (activated from the slow control)
           chip_status_obj.chip_status = chip_status; 
		         chip_status_obj.chip_status_update_event.trigger();
           `uvm_info("chip_status", $sformatf("Chip status: %s, # pulses in shutter %0d", chip_status_obj.chip_status,time_measurements_obj.number_of_pulses_to_apply_within_shutter), UVM_LOW);
           perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( slowcontrol_sqr ));
          
											
      endtask: common_actions_allreadoutmodes
						
						
						virtual task common_actions_sametotDiv(chip_status_type chip_status);
						
						
						
						     time_measurements_obj.number_of_pulses_to_apply_within_shutter = 1; assert(time_measurements_obj.randomize());  
												`uvm_info("Timing injected hits", $sformatf("%s, 1 pulse within shutter, toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
						 					     chip_status,time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
																	config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
																	config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
																	config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
																	config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse), UVM_LOW);
												common_actions_allreadoutmodes(chip_status);
												
												/*
												time_measurements_obj.number_of_pulses_to_apply_within_shutter = 2;  assert(time_measurements_obj.randomize()); 
												`uvm_info("Timing injected hits", $sformatf("%s, 2 pulses within shutter, toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
						 					     chip_status_obj.chip_status,time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
																	config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
																	config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
																	config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
																	config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse), UVM_LOW);
												common_actions_allreadoutmodes(chip_status);
												*/
						endtask: common_actions_sametotDiv



		  task body ();

            chip_status_type chip_status;

            configure_acquisition_mode_seq = CLICTD_configure_acquisition_mode_sequence::type_id::create( .name( "configure_acquisition_mode_seq" ), .contxt( get_full_name() ) );
            configure_acquisition_mode_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
					       configure_acquisition_mode_seq.chip_status_obj = chip_status_obj;
					       configure_acquisition_mode_seq.reporting_enable_obj = reporting_enable_obj;
												configure_acquisition_mode_seq.config_matrix_obj = config_matrix_obj;
												
												configure_external_testpulse_seq = CLICTD_configure_external_testpulse_sequence::type_id::create( .name( "configure_external_testpulse_seq" ), .contxt( get_full_name() ) );
												configure_external_testpulse_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
     							configure_external_testpulse_seq.chip_status_obj = chip_status_obj;
     							configure_external_testpulse_seq.reporting_enable_obj = reporting_enable_obj;
     							configure_external_testpulse_seq.config_matrix_obj = config_matrix_obj;
						
            apply_hits_external_seq = CLICTD_apply_hits_external_sequence::type_id::create( .name( "apply_hits_external_seq" ), .contxt( get_full_name() ) );
												//apply_hits_external_seq.digital_pulse_lasts_longer_than_shutter = `NO;
												//assert ( apply_hits_external_seq.randomize() );
												apply_hits_external_seq.config_matrix_obj = config_matrix_obj; // assign after the randomize so as to preserve the values in the redout_test_seq
												apply_hits_external_seq.time_measurements_obj = time_measurements_obj;
												
												

            perform_serial_readout_activatefrom_slowcontrol_virtual_seq = CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence::type_id::create( .name( "perform_serial_readout_activatefrom_slowcontrol_virtual_seq" ), .contxt( get_full_name() ) );
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.chip_status_obj = chip_status_obj;
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.events_obj = events_obj;
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
												perform_serial_readout_activatefrom_slowcontrol_virtual_seq.config_matrix_obj = config_matrix_obj;
            perform_serial_readout_activatefrom_slowcontrol_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
												
												// Enable test pulse injection and configure test pulse to be injected from TPULSE_pad
											 configure_external_testpulse_seq.start ( .sequencer(slowcontrol_sqr) );

           
            // Nominal readout, compression disabled
            bits_slowcontrol_reg_obj.noCompr = 1'b1;
            bits_slowcontrol_reg_obj.photonCnt = 1'b0; bits_slowcontrol_reg_obj.longCnt = 1'b0;
												
            // totDiv = 0
												bits_slowcontrol_reg_obj.totDiv = 2'b00; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv0;  common_actions_sametotDiv(chip_status);
												
												
            // totDiv = 1
            bits_slowcontrol_reg_obj.totDiv = 2'b01; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv1; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv1; common_actions_sametotDiv(chip_status);
            // totDiv = 2
            bits_slowcontrol_reg_obj.totDiv = 2'b10; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv2; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv2; common_actions_sametotDiv(chip_status);
            // totDiv = 3
            bits_slowcontrol_reg_obj.totDiv = 2'b11; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv3; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_nocompr_totDiv3; common_actions_sametotDiv(chip_status);
											
            // Long ToA readout, totDiv = 0, compression disabled
            bits_slowcontrol_reg_obj.photonCnt = 1'b0; bits_slowcontrol_reg_obj.longCnt = 1'b1;
            bits_slowcontrol_reg_obj.totDiv = 2'b00; bits_slowcontrol_reg_obj.noCompr = 1'b1; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_nocompr_totDiv0; common_actions_sametotDiv(chip_status);
											
												
												// Photon counting readout, totDiv = 0, compression disabled
            bits_slowcontrol_reg_obj.photonCnt = 1'b1; bits_slowcontrol_reg_obj.longCnt = 1'b1;
            bits_slowcontrol_reg_obj.totDiv = 2'b00; bits_slowcontrol_reg_obj.noCompr = 1'b1; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_nocompr_totDiv0; common_actions_sametotDiv(chip_status);
												
												// ---------------------------------------------------------------------------------------------------------------------------
												// Additional steps to enable compressed readout (since we've already configured and readout before, it's only required to...)
												// Enable compression
												bits_slowcontrol_reg_obj.noCompr = 1'b0;
												// Send the acquisition configuration to the slow control
           	chip_status_obj.chip_status = STATUS_CONFIGURE_ENABLE_COMPRESSION; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Enabling compressed readout, chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	configure_acquisition_mode_seq.start(  .sequencer(slowcontrol_sqr) );
											
												// Readout twice in a row to clear hit flags
           	chip_status_obj.chip_status = STATUS_FIRST_READOUT_ENABLE_COMPRESSION; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( slowcontrol_sqr ));
												chip_status_obj.chip_status = STATUS_SECOND_READOUT_ENABLE_COMPRESSION; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( slowcontrol_sqr ));
												
												// ---------------------------------------------------------------------------------------------------------------------------
            //reporting_enable_obj.enable_reporting_breakdownreadoutbits = `YES;
												
            // Nominal readout, compression enabled
            bits_slowcontrol_reg_obj.noCompr = 1'b0;
            bits_slowcontrol_reg_obj.photonCnt = 1'b0; bits_slowcontrol_reg_obj.longCnt = 1'b0;
            // totDiv = 0
            bits_slowcontrol_reg_obj.totDiv = 2'b00; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv0; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv0; common_actions_sametotDiv(chip_status);
            // totDiv = 1
            bits_slowcontrol_reg_obj.totDiv = 2'b01; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv1; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv1; common_actions_sametotDiv(chip_status);
            // totDiv = 2
            bits_slowcontrol_reg_obj.totDiv = 2'b10; common_actions_allreadoutmodes(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv2; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv2; common_actions_sametotDiv(chip_status);
            // totDiv = 3
            bits_slowcontrol_reg_obj.totDiv = 2'b11; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_compr_totDiv3; common_actions_sametotDiv(chip_status);
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_longtot_nominal_compr_totDiv3; common_actions_sametotDiv(chip_status);

            // Long ToA readout, totDiv = 0, compression enabled
            bits_slowcontrol_reg_obj.photonCnt = 1'b0; bits_slowcontrol_reg_obj.longCnt = 1'b1;
            bits_slowcontrol_reg_obj.totDiv = 2'b00; bits_slowcontrol_reg_obj.noCompr = 1'b0;	
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_longtoa_compr_totDiv0; common_actions_sametotDiv(chip_status);

            // Photon counting readout, totDiv = 0, compression enabled
            bits_slowcontrol_reg_obj.photonCnt = 1'b1; bits_slowcontrol_reg_obj.longCnt = 1'b1;
            bits_slowcontrol_reg_obj.totDiv = 2'b00; bits_slowcontrol_reg_obj.noCompr = 1'b0; 
												chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_phcount_compr_totDiv0; common_actions_sametotDiv(chip_status);
         
            
            

		  endtask: body




endclass: CLICTD_readout_test_virtual_sequence

`endif // __CLICTD_readout_test_virtual_sequence_sv__





  



