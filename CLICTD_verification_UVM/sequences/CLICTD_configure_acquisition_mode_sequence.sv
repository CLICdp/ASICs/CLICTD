// Filename           : CLICTD_configure_acquisition_mode_sequence.sv
// Author             : Núria Egidos 
// Created on         : 11/9/18
// Last modification  : 11/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to configure the acquisition mode (nominal/long ToA/photon counting, totDiv)
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_configure_acquisition_mode_sequence_sv__
`define __CLICTD_configure_acquisition_mode_sequence_sv__



class CLICTD_configure_acquisition_mode_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_configure_acquisition_mode_sequence) // Register the component to the factory


     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_perform_serial_readout_activatefrom_slowcontrol_sequence");
          super.new(name);
     endfunction: new
    
     
		  task body ();
          sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
		      // Send the acquisition configuration to the slow control
          send_slowcontrol_transaction_write(.register_address(`ADDRESS_matrixConfig), .register_content({3'b0, bits_slowcontrol_reg_obj.photonCnt, bits_slowcontrol_reg_obj.noCompr, bits_slowcontrol_reg_obj.longCnt, bits_slowcontrol_reg_obj.totDiv }));
		  endtask: body




endclass: CLICTD_configure_acquisition_mode_sequence

`endif // __CLICTD_configure_acquisition_mode_sequence_sv__





  



