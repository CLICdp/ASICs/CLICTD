// Filename           : CLICTD_testpulse_activatefrom_external_sequence.sv
// Author             : Núria Egidos 
// Created on         : 4/9/18
// Last modification  : 4/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to force a test pulse applied externally via TPULSE_pad
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_testpulse_activatefrom_external_sequence_sv__
`define __CLICTD_testpulse_activatefrom_external_sequence_sv__



class CLICTD_testpulse_activatefrom_external_sequence extends uvm_sequence#(CLICTD_pins_transaction);
     `uvm_object_utils(CLICTD_testpulse_activatefrom_external_sequence) // Register the component to the factory

     CLICTD_pins_transaction pins_trans;

     rand int duration_tpulse_pad;
     rand int duration_shutter_pad;
     rand int delay_between_rise_shutter_and_tpulse;

     constraint c_delay_between_rise_shutter_and_tpulse { delay_between_rise_shutter_and_tpulse inside { [10:duration_shutter_pad-10] };  } //ns
     constraint c_duration_tpulse_pad                   { duration_tpulse_pad  inside { [20:200] }; } //ns
     constraint c_duration_shutter_pad                  { duration_shutter_pad inside { [30:500] }; } //ns
     // avoid that falling edges happen at the very same time, so that transactions are not sent simultaneously
     constraint c_falling_edge_tpulse_pad               { (delay_between_rise_shutter_and_tpulse + duration_tpulse_pad) != duration_shutter_pad; }

     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_testpulse_activatefrom_external_sequence");
          super.new(name);
     endfunction: new


		



		  task body ();
		        pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));


            // Shape to obtain:
            //                    __________________________________________________________
            // SHUTTER_pad:  ____|<------------------ duration_shutter_pad ---------------->|______________
            //                   |
            //                   |<- delay_between_rise_shutter_and_tpulse ->|_________________________
            // TPULSE_pad:   ________________________________________________|<- duration_tpulse_pad ->|____ (falling edge of TPULSE_pad can arrive before or after the falling edge of SHUTTER_pad)

            // Force idle values
            start_item(pins_trans);
            pins_trans.RSTN_pad    = 1'b1; // idle value
            pins_trans.PWREN_pad    = 1'b0; // idle value     
						pins_trans.TPULSE_pad  = 1'b0; // idle value    
						pins_trans.READOUT_pad = 1'b0; // idle value	
						pins_trans.SHUTTER_pad = 1'b0; // idle value
						pins_trans.disc        = 1'b0; // idle value
            finish_item(pins_trans);
            #500;

            // Set SHUTTER_pad
            start_item(pins_trans);	
						pins_trans.SHUTTER_pad = 1'b1; // force pulse
            finish_item(pins_trans);
          
				    #(delay_between_rise_shutter_and_tpulse);

            $display("duration_tpulse_pad = %f, duration_shutter_pad = %f, delay_between_rise_shutter_and_tpulse = %f",duration_tpulse_pad,duration_shutter_pad,delay_between_rise_shutter_and_tpulse);

            // Set TPULSE_pad
            start_item(pins_trans);
            pins_trans.TPULSE_pad  = 1'b1; // force pulse
            finish_item(pins_trans);

            fork
            begin
                  #(duration_tpulse_pad);
						      // Clear TPULSE_pad
                  start_item(pins_trans);
						      pins_trans.TPULSE_pad  = 1'b0; // idle value
                  finish_item(pins_trans);
            end
            begin
                  #(duration_shutter_pad);
						      // Clear SHUTTER_pad
                  start_item(pins_trans);
						      pins_trans.SHUTTER_pad  = 1'b0; // idle value
                  finish_item(pins_trans);
            end
            join

            
            #500;

		  endtask: body




endclass: CLICTD_testpulse_activatefrom_external_sequence

`endif // __CLICTD_testpulse_activatefrom_external_sequence_sv__





  



