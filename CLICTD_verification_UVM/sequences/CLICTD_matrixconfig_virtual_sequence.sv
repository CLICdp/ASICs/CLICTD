// Filename           : CLICTD_matrixconfig_virtual_sequence.sv
// Author             : Núria Egidos 
// Created on         : 31/10/18
// Last modification  : 31/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to perform both configuration halves of the matrix
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_matrixconfig_virtual_sequence_sv__
`define __CLICTD_matrixconfig_virtual_sequence_sv__


class CLICTD_matrixconfig_virtual_sequence extends uvm_sequence#(uvm_sequence_item);

     `uvm_object_utils(CLICTD_matrixconfig_virtual_sequence) // Register the component to the factory

     string testname;
     
     CLICTD_slowcontrol_sequencer slowcontrol_sqr;
					
					BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;

     ChipStatusInfo chip_status_obj;
    
     ReportingEnable reporting_enable_obj;

     // Randomizable fields - this is randomized when the sequence that calls this sequence uses matrixconfig_virtual_seq.randomize()
     FieldsConfigureMatrix config_matrix_obj;
					FieldsConfigureMatrix_for_coverage config_matrix_obj_for_coverage;
					

     bit ENABLE_CONFIGURE_FIRST_HALF;
     bit ENABLE_CONFIGURE_SECOND_HALF;
    
     // Sequence to configure (one half of the configuration bits) the matrix
      CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence matrixconfig_allpixels_allcols_onehalf_seq;

     // Sequence to activate serial readout with a slow control command
      CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence perform_serial_readout_activatefrom_slowcontrol_virtual_seq;

      CLICTDevents events_obj;
     
      real time_to_fully_config_matrix;

      
     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_matrixconfig_virtual_sequence");
          super.new(name);
     endfunction: new



    task configure_matrix_with_fixed_pattern(bit value_for_dummy_bit);
           // value of the dummy bits, i.e. those that are required to be sent during each configuration
           // half (bit 21 in the 1st half, bit 0 and 21 in the 2nd half), but they are not configuration values
           config_matrix_obj.value_for_dummy_bit = value_for_dummy_bit; 
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                   for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
       
                       // Superpixels to pulse: digital -> apply digital pulse to superpixels (0,0), (1,1) ... (7,7)
                       if (c < 8) begin
                           if (ss == 0 && sis < 8 && sis == c) begin 
                              config_matrix_obj.tpEnableDigital[c][ss][sis] = 1'b1;
                              // Pixels to mask: one in each of the superpixels that we pulse, 
                              // except for superpixel (0,0), where we mask all pixels
                              if (sis == 0) config_matrix_obj.mask[c][ss][sis] = 8'hFF;
                              else          config_matrix_obj.mask[c][ss][sis] = 8'h01;
                           end
                           else config_matrix_obj.tpEnableDigital[c][ss][sis] = 1'b0;
                       end
                       // Pixels to pulse: analog (none)
                       config_matrix_obj.tpEnableAnalog[c][ss][sis] = 8'b0;

                       // Expected readout configuration for the first half, column 0, superpixel segment 0, superpixel in segment 0:
                       // hit_flag                |- mask -| tpEnableDigital
                       //     1    0000 0000 0000  1111 1111      1

                       // Expected readout configuration for the first half, column i, superpixel segment 0, superpixel in segment i (i = 1..7):
                       // hit_flag                |- mask -| tpEnableDigital
                       //     1    0000 0000 0000  0000 0001      1
		
                       // Expected readout configuration for the first half, the rest of superpixels in segment, superpixel segments and columns,
                       // and for the second half, all superpixels in segment, superpixel segments and columns:
                       // hit_flag                
                       //     1    0000 0000 0000  0000 0000 0

                       // According to the aforementioned expected values for the superpixel configuration, these are the expected column_content values:
                       // First configuration half
                       // COLUMN 0 = 8007fe00000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000
                       // COLUMN 1 =  80000200003800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000
                       // COLUMN 2 =  8000020000080000e000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000
                       // COLUMN 3 =  80000200000800002000038000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000
                       // COLUMN 4 = 800002000008000020000080000e0000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000
                       // COLUMN 5 = 80000200000800002000008000020000380000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000
                       // COLUMN 6 = 80000200000800002000008000020000080000e00000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000
                       // COLUMN 7 = 80000200000800002000008000020000080000200003800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000
                       // COLUMN 8-15 = 80000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000 

                       // Second configuration half
                       // COLUMN 0-15 = 80000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000800002000008000020000080000200000

                       
                       // We don't care about the configuration of tuningDAC for this test
                       config_matrix_obj.tuningDAC0[c][ss][sis] = 3'b0;
                       config_matrix_obj.tuningDAC1[c][ss][sis] = 3'b0;
                       config_matrix_obj.tuningDAC2[c][ss][sis] = 3'b0;
                       config_matrix_obj.tuningDAC3[c][ss][sis] = 3'b0;
                       config_matrix_obj.tuningDAC4[c][ss][sis] = 3'b0;
                       config_matrix_obj.tuningDAC5[c][ss][sis] = 3'b0;
                       config_matrix_obj.tuningDAC6[c][ss][sis] = 3'b0;
                       config_matrix_obj.tuningDAC7[c][ss][sis] = 3'b0;
                     
                   end
               end
           end
		
     endtask: configure_matrix_with_fixed_pattern

     task configure_matrix_debug();
           config_matrix_obj.value_for_dummy_bit = 1'b0; // to try both values
           for (int c = 0; c < `COLUMNS; c++) begin
               for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
                   for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
                        config_matrix_obj.tuningDAC1[c][ss][sis][0] = 1'b1;
												            config_matrix_obj.tuningDAC7[c][ss][sis][2] = 1'b1;
                   end
               end
           end
           // With this configuration pattern, it is expected that column_content for both configuration halves and for all columns
           // yields in the readout:
           // c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000c0000300000
     endtask: configure_matrix_debug
     
		  task body ();
			
		         matrixconfig_allpixels_allcols_onehalf_seq = CLICTD_matrixconfig_allpixels_allcols_onehalf_sequence::type_id::create( .name( "matrixconfig_allpixels_allcols_onehalf_seq" ), .contxt( get_full_name() ) );
				       matrixconfig_allpixels_allcols_onehalf_seq.chip_status_obj = chip_status_obj;
				       matrixconfig_allpixels_allcols_onehalf_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
           matrixconfig_allpixels_allcols_onehalf_seq.reporting_enable_obj = reporting_enable_obj;
				       matrixconfig_allpixels_allcols_onehalf_seq.config_matrix_obj = config_matrix_obj;
											matrixconfig_allpixels_allcols_onehalf_seq.config_matrix_obj_for_coverage = config_matrix_obj_for_coverage;
											matrixconfig_allpixels_allcols_onehalf_seq.testname = testname;



           perform_serial_readout_activatefrom_slowcontrol_virtual_seq = CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence::type_id::create( .name( "perform_serial_readout_activatefrom_slowcontrol_virtual_seq" ), .contxt( get_full_name() ) );
					      perform_serial_readout_activatefrom_slowcontrol_virtual_seq.chip_status_obj = chip_status_obj;
					      perform_serial_readout_activatefrom_slowcontrol_virtual_seq.events_obj = events_obj;
					      perform_serial_readout_activatefrom_slowcontrol_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
           perform_serial_readout_activatefrom_slowcontrol_virtual_seq.slowcontrol_sqr = slowcontrol_sqr; 



           // Obtain values of matrix configuration
           //if      ( config_matrix_obj.matrix_config_fields == FIXED_DEBUG      ) configure_matrix_debug();
           //else if ( config_matrix_obj.matrix_config_fields == FIXED_HITPATTERN ) configure_matrix_with_fixed_pattern(.value_for_dummy_bit(1'b1));
          
										/*
										for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
						    					for (int sis = 0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
																				$display("matrixconfig_virtual_seq config_matrix_obj - ss = %0d sis = %0d, tpEnableDigital = %h, mask = %h, tpEnableAnalog = %h, tuningDAC0 = %h, tuningDAC1 = %h, tuningDAC2 = %h, tuningDAC3 = %h, tuningDAC4 = %h, tuningDAC5 = %h, tuningDAC6 = %h, tuningDAC7 = %h", 
						          														ss, sis, config_matrix_obj.tpEnableDigital[0][ss][sis], config_matrix_obj.mask[0][ss][sis], config_matrix_obj.tpEnableAnalog[0][ss][sis], config_matrix_obj.tuningDAC0[0][ss][sis], config_matrix_obj.tuningDAC1[0][ss][sis],
																														config_matrix_obj.tuningDAC2[0][ss][sis], config_matrix_obj.tuningDAC3[0][ss][sis], config_matrix_obj.tuningDAC4[0][ss][sis], config_matrix_obj.tuningDAC5[0][ss][sis], config_matrix_obj.tuningDAC6[0][ss][sis],
																														config_matrix_obj.tuningDAC7[0][ss][sis]);
															end
											end
											*/
											// At this point, config_matrix_obj has the same values as in the test																		
          
          time_to_fully_config_matrix = $realtime;
          // First configuration half
          if (ENABLE_CONFIGURE_FIRST_HALF == `YES) begin
             chip_status_obj.chip_status = STATUS_MATRIX_CONFIG_FIRST_HALF; // the configuration bits are shifted into the matrix
		         chip_status_obj.chip_status_update_event.trigger();
		         `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
		         matrixconfig_allpixels_allcols_onehalf_seq.start( .sequencer( slowcontrol_sqr ));

            #(`DELAY_NS_USED_TO_AVOID_VIOLATIONS_DUE_TO_ASYNC_SIGNALS);
             // Readout 1st configuration half
		         chip_status_obj.chip_status = STATUS_CONFIG_READOUT_FIRST_HALF; // the configuration bits are shifted into the matrix
		         chip_status_obj.chip_status_update_event.trigger();
		         `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
		         perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( null ));
          end

          // Second configuration half
          if (ENABLE_CONFIGURE_SECOND_HALF == `YES) begin
						  //configure_matrix_with_fixed_pattern(1'b0); // like this we try with both possible values of dummy_bit
 
             chip_status_obj.chip_status = STATUS_MATRIX_CONFIG_SECOND_HALF; // the configuration bits are shifted into the matrix
		         chip_status_obj.chip_status_update_event.trigger();
		         `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
		         matrixconfig_allpixels_allcols_onehalf_seq.start( .sequencer( slowcontrol_sqr ));


            #(`DELAY_NS_USED_TO_AVOID_VIOLATIONS_DUE_TO_ASYNC_SIGNALS);
             // Readout 2nd configuration half
		         chip_status_obj.chip_status = STATUS_CONFIG_READOUT_SECOND_HALF; // the configuration bits are shifted into the matrix
		         chip_status_obj.chip_status_update_event.trigger();
		         `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
					   perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( null ));
          end
          time_to_fully_config_matrix = $realtime - time_to_fully_config_matrix;
          `uvm_info("time measurement", $sformatf("Time to fully configure matrix: %t, chip status: %s", time_to_fully_config_matrix, chip_status_obj.chip_status), UVM_LOW);

		  endtask: body




endclass: CLICTD_matrixconfig_virtual_sequence

`endif // __CLICTD_matrixconfig_virtual_sequence_sv__





  



