// Filename           : CLICTD_configure_internal_powerpulse_sequence.sv
// Author             : Núria Egidos 
// Created on         : 27/9/18
// Last modification  : 27/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to configure the power pulse to be activated from the slow control
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_configure_internal_powerpulse_sequence_sv__
`define __CLICTD_configure_internal_powerpulse_sequence_sv__



class CLICTD_configure_internal_powerpulse_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_configure_internal_powerpulse_sequence) // Register the component to the factory

    
     // Constructor
     function new(string name = "CLICTD_configure_internal_powerpulse_sequence");
          super.new(name);
     endfunction: new


     task body ();
          sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));

          // Enable powerpulse activated internally 
          bits_slowcontrol_reg_obj.pwrExtEn = 1'b0; 
          send_slowcontrol_transaction_write( .register_address(`ADDRESS_globalConfig), .register_content({5'b0, bits_slowcontrol_reg_obj.pwrExtEn, 2'b0}) );

     endtask: body




endclass: CLICTD_configure_internal_powerpulse_sequence

`endif // __CLICTD_configure_internal_powerpulse_sequence_sv__
