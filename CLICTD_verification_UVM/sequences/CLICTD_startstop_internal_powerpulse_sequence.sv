// Filename           : CLICTD_startstop_internal_powerpulse_sequence.sv
// Author             : Núria Egidos 
// Created on         : 28/9/18
// Last modification  : 28/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to activate the power pulsing via a slow control command
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_startstop_internal_powerpulse_sequence_sv__
`define __CLICTD_startstop_internal_powerpulse_sequence_sv__



class CLICTD_startstop_internal_powerpulse_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_startstop_internal_powerpulse_sequence) // Register the component to the factory

     rand int time_wait_after_power_change;
     constraint cons_duration_wait { time_wait_after_power_change inside {[5000:10000]}; }

     // Constructor
     function new(string name = "CLICTD_startstop_internal_powerpulse_sequence");
          super.new(name);
     endfunction: new


     task body ();
  
           // Set default power status: ON
           sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
           bits_slowcontrol_reg_obj.pwrInt = 1'b0;
           send_slowcontrol_transaction_write(.register_address(`ADDRESS_internalStrobes), .register_content({6'b0, bits_slowcontrol_reg_obj.pwrInt, 1'b0}) );

           // Allow some time for the circuit to react
           #(time_wait_after_power_change); // 10 us 

           // Set power off
           sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
           bits_slowcontrol_reg_obj.pwrInt = 1'b1;
           send_slowcontrol_transaction_write(.register_address(`ADDRESS_internalStrobes), .register_content({6'b0, bits_slowcontrol_reg_obj.pwrInt, 1'b0}) );

            // Allow some time for the circuit to react
            #(time_wait_after_power_change); // 10 us 

           // Set power back on
           sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
           bits_slowcontrol_reg_obj.pwrInt = 1'b0;
           send_slowcontrol_transaction_write(.register_address(`ADDRESS_internalStrobes), .register_content({6'b0, bits_slowcontrol_reg_obj.pwrInt, 1'b0}) );


     endtask: body




endclass: CLICTD_startstop_internal_powerpulse_sequence

`endif // __CLICTD_startstop_internal_powerpulse_sequence_sv__
