// Filename           : CLICTD_apply_hits_external_sequence_for_coverage.sv
// Author             : Núria Egidos 
// Created on         : 17/1/19
// Last modification  : 17/1/19
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to apply hits to the matrix, forcing digital test pulses (applied from TPULSE_pad) and pulses at the disc bits

// Common constraints: https://www.chipverify.com/systemverilog/constraint-examples-sv

`ifndef __CLICTD_apply_hits_external_sequence_for_coverage_sv__
`define __CLICTD_apply_hits_external_sequence_for_coverage_sv__



class CLICTD_apply_hits_external_sequence_for_coverage extends uvm_sequence#(CLICTD_pins_transaction);
     `uvm_object_utils(CLICTD_apply_hits_external_sequence_for_coverage) // Register the component to the factory


  

     CLICTD_pins_transaction pins_trans;
	
					FieldsTimeMeasurements_for_coverage time_measurements_obj;
					
					FieldsConfigureMatrix_for_coverage config_matrix_obj;


     // Constructor
     function new(string name = "CLICTD_apply_hits_external_sequence_for_coverage");
          super.new(name);
     endfunction: new


     task body ();

        // Note: the pulses applied to the discriminator bits are sent to all superpixels to pulse, both to those that will have digital test pulsing enabled and disabled;
								// the idea is to verify that digital test pulsing overrides the discriminator bits in those superpixels where digital test pulsing is enabled 
								
        //  uvm_info expected tot, toa
								
								
								//$display("apply hits seq - toa_ns = %d, tot_short_ns = %d, tot_long_ns = %d, delay 1st even within shutter = %d, colpulse = %d %d %d %d %d %d %d %d %d, ss disc = %d, sis disc = %d, bit0 = %d, bit1 = %d, ss dig = %d, sis dig = %d",
								//         time_measurements_obj.toa_ns, time_measurements_obj.tot_short_ns, time_measurements_obj.tot_long_ns, time_measurements_obj.delay_to_startpulses_after_shutter_ns,
								//									config_matrix_obj.columns_to_pulse[0],config_matrix_obj.columns_to_pulse[1],config_matrix_obj.columns_to_pulse[2],config_matrix_obj.columns_to_pulse[3],
								//									config_matrix_obj.columns_to_pulse[4],config_matrix_obj.columns_to_pulse[5],config_matrix_obj.columns_to_pulse[6],config_matrix_obj.columns_to_pulse[7],config_matrix_obj.columns_to_pulse[8],
								//									config_matrix_obj.ss_applydiscpulse, config_matrix_obj.sis_applydiscpulse,config_matrix_obj.bit_applydiscpulse_0,config_matrix_obj.bit_applydiscpulse_1,
								//									config_matrix_obj.ss_applydigitalpulse, config_matrix_obj.sis_applydigitalpulse);

        pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
								
								// Default values
								pins_trans.PWREN_pad = 1'b1;     // Pad to provide the power pulse
        pins_trans.READOUT_pad = 1'b0;	// Pad to activate readout with an external pulse
        pins_trans.RSTN_pad = 1'b1;
								
								
								
        start_item(pins_trans);
        pins_trans.SHUTTER_pad = 1'b0;
        pins_trans.disc = '0; 
        pins_trans.TPULSE_pad = 1'b0;
        finish_item(pins_trans);
        #(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);

        
								
        // One of the discriminator bits in the superpixels to pulse and the digital test pulse rise before the shutter -> this first pulse won't be taken into account
								start_item(pins_trans);
								for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_0] = 1'b1;
								end
        pins_trans.TPULSE_pad = 1'b1;
								finish_item(pins_trans); 
        #(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);

        // TIME POINT 1 = t0
        // START OF SHUTTER
								start_item(pins_trans);
        pins_trans.SHUTTER_pad = 1'b1;
								finish_item(pins_trans); 
								
								// TIME POINT 2 = TIME POINT 1 + MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION
        #(`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
        // The discriminator bits that rose before and the digital test pulse fall
								start_item(pins_trans);
        for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_0] = 1'b0;
								end
        pins_trans.TPULSE_pad = 1'b0;
								finish_item(pins_trans); 

        // TIME POINT 3 = TIME POINT 2 + delay_to_startpulses_after_shutter_ns - MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION
        #(time_measurements_obj.delay_to_startpulses_after_shutter_ns-`MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION);
								start_item(pins_trans);
        // MAIN PULSE -> USED TO COMPUTE TOT, TOA, PHOTON COUNT
        for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_0] = 1'b1;
								end
        pins_trans.TPULSE_pad = 1'b1; 
								finish_item(pins_trans);
			
			     // ******************************************************
			     if (time_measurements_obj.number_of_pulses_to_apply_within_shutter == 1) begin: one_pulse_in_shutter
			         // APPLY 1 PULSE WITHIN SHUTTER
												
												// TIME POINT 4: the other discriminator bit in each of the superpixels to pulse, which was still low, rises
												#(time_measurements_obj.time_separation_between_pulses_in_two_disc_bits);
												start_item(pins_trans);
												for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     				pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_1] = 1'b1;
												end
												finish_item(pins_trans);
												
												// TIME POINT 5: the first of the discriminator bits to rise (in all the superpixels to pulse) falls, the second to rise is still high
												#(time_measurements_obj.duration_first_pulse_in_disc_bits - time_measurements_obj.time_separation_between_pulses_in_two_disc_bits); 
												start_item(pins_trans);
												for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     				pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_0] = 1'b0;
												end
												finish_item(pins_trans);
												
												
												// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
												if (time_measurements_obj.tot_ns >= time_measurements_obj.toa_ns) begin: one_pulse_in_shutter_tot_larger_than_toa
												    // TOT >= TOA
																
																// TIME POINT 7: END OF SHUTTER
																#(time_measurements_obj.toa_ns - time_measurements_obj.duration_first_pulse_in_disc_bits);
																start_item(pins_trans);
																pins_trans.SHUTTER_pad = 1'b0;
																finish_item(pins_trans);
																
																// TIME POINT 8: the second discriminator bit to rise in all superpixels to pulse and the digital test pulse fall
																#(time_measurements_obj.tot_ns - time_measurements_obj.toa_ns);
																start_item(pins_trans);
																for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     								pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_1] = 1'b0;
																end
        								pins_trans.TPULSE_pad = 1'b0;
																finish_item(pins_trans);
																
												end: one_pulse_in_shutter_tot_larger_than_toa
												// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
												else begin: one_pulse_in_shutter_tot_smaller_than_toa
												    // TOT < TOA
																
																// TIME POINT 6: the second discriminator bit to rise in all superpixels to pulse and the digital test pulse fall
																#(time_measurements_obj.duration_second_pulse_in_disc_bits - time_measurements_obj.duration_first_pulse_in_disc_bits + time_measurements_obj.time_separation_between_pulses_in_two_disc_bits);
																start_item(pins_trans);
																for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     								pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_1] = 1'b0;
																end
        								pins_trans.TPULSE_pad = 1'b0;
																finish_item(pins_trans);
																
																// TIME POINT 7: END OF SHUTTER
																#(time_measurements_obj.toa_ns - time_measurements_obj.duration_second_pulse_in_disc_bits - time_measurements_obj.time_separation_between_pulses_in_two_disc_bits);
																start_item(pins_trans);
																pins_trans.SHUTTER_pad = 1'b0;
																finish_item(pins_trans);
																																
												end: one_pulse_in_shutter_tot_smaller_than_toa
												// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
								
								end: one_pulse_in_shutter
								// ******************************************************
								else begin: two_pulses_in_shutter
								    // APPLY 2 PULSES WITHIN SHUTTER
												
												// TIME POINT 4: the discriminator bit which was high in each of the superpixels to pulse and the digital test pulse fall 
												#(time_measurements_obj.duration_first_pulse_in_disc_bits);
												start_item(pins_trans);
												for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     				pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_0] = 1'b0;
												end
												pins_trans.TPULSE_pad = 1'b0;
												finish_item(pins_trans);
												
												// TIME POINT 5: the discriminator bit that has not been high yet (in all the superpixels to pulse) and the digital test pulse rise
												#(time_measurements_obj.time_separation_between_pulses_in_two_disc_bits); 
												start_item(pins_trans);
												for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     				pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_1] = 1'b1;
												end
												pins_trans.TPULSE_pad = 1'b1;
												finish_item(pins_trans);
												
												// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
												if (time_measurements_obj.tot_ns >= time_measurements_obj.toa_ns) begin: two_pulses_in_shutter_tot_larger_than_toa
												    // TOT >= TOA
																
																// TIME POINT 7: END OF SHUTTER
																#(time_measurements_obj.toa_ns - time_measurements_obj.duration_first_pulse_in_disc_bits - time_measurements_obj.time_separation_between_pulses_in_two_disc_bits);
																start_item(pins_trans);
																pins_trans.SHUTTER_pad = 1'b0;
																finish_item(pins_trans);
																
																// TIME POINT 8: the digital test pulse and the second discriminator bit to rise (in all superpixels to pulse) fall
																#(time_measurements_obj.duration_second_pulse_in_disc_bits - time_measurements_obj.toa_ns + time_measurements_obj.duration_first_pulse_in_disc_bits + time_measurements_obj.time_separation_between_pulses_in_two_disc_bits);
																start_item(pins_trans);
																for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     								pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_1] = 1'b0;
																end
        								pins_trans.TPULSE_pad = 1'b0;
																finish_item(pins_trans);
																
																
																
												end: two_pulses_in_shutter_tot_larger_than_toa
												// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
												else begin: two_pulses_in_shutter_tot_smaller_than_toa
												    // TOT < TOA
																
																// TIME POINT 6: the second discriminator bit to rise in all superpixels to pulse and the digital test pulse fall
																#(time_measurements_obj.duration_second_pulse_in_disc_bits);
																start_item(pins_trans);
																for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     								pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_1] = 1'b0;
																end
																pins_trans.TPULSE_pad = 1'b0;
																finish_item(pins_trans);
																
																// TIME POINT 7: END OF SHUTTER
																#(time_measurements_obj.toa_ns - time_measurements_obj.tot_ns - time_measurements_obj.time_separation_between_pulses_in_two_disc_bits);
																start_item(pins_trans);
																pins_trans.SHUTTER_pad = 1'b0;
																finish_item(pins_trans);
																															
												end: two_pulses_in_shutter_tot_smaller_than_toa
												// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
												
												
								end: two_pulses_in_shutter
								// ******************************************************
								
								// TIME POINT 9: a last pulse starts in TPULSE_pad and one of the discriminator bits of every superpixel to pulse;
								// since this pulse comes outside the shutter, it shouldn't be handled
								#100;
								start_item(pins_trans);
								for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_1] = 1'b1;
								end
        pins_trans.TPULSE_pad = 1'b1;
								finish_item(pins_trans);
																															

								// TIME POINT 10: the last pulse ends in TPULSE_pad and one of the discriminator bits of every superpixel to pulse
								#100;
								start_item(pins_trans);
								for (int i = 0; i < config_matrix_obj.occupancy; i++) begin
								     pins_trans.disc[config_matrix_obj.superpixels_to_pulse[i].column][config_matrix_obj.superpixels_to_pulse[i].superpixel_segment][config_matrix_obj.superpixels_to_pulse[i].superpixel_in_segment][config_matrix_obj.superpixels_to_pulse[i].bit_applydiscpulse_1] = 1'b0;
								end
        pins_trans.TPULSE_pad = 1'b0;
								finish_item(pins_trans);
																															

     endtask: body




endclass: CLICTD_apply_hits_external_sequence_for_coverage

`endif // __CLICTD_apply_hits_external_sequence_for_coverage_sv__
