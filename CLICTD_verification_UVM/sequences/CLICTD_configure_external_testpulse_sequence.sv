// Filename           : CLICTD_configure_external_testpulse_sequence.sv
// Author             : Núria Egidos 
// Created on         : 31/10/18
// Last modification  : 31/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to configure the activation of the test puls to be applied from TPULSE_pad
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_configure_external_testpulse_sequence_sv__
`define __CLICTD_configure_external_testpulse_sequence_sv__



class CLICTD_configure_external_testpulse_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_configure_external_testpulse_sequence) // Register the component to the factory


     // - - - - - - - - - - - - Constructor
     function new(string name = "CLICTD_perform_serial_readout_activatefrom_slowcontrol_sequence");
          super.new(name);
     endfunction: new
    
     
		  task body ();
          sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
										
										// Enable tets pulse activated externally 
          bits_slowcontrol_reg_obj.tpExtEn = 1'b1; 
										bits_slowcontrol_reg_obj.tpEn = 1'b1; // this bit is the global test pulse (set to 1 to enable the injection of any test pulse, 0 to disable any injection of test pulses)
          send_slowcontrol_transaction_write( .register_address(`ADDRESS_globalConfig), .register_content({6'b0, bits_slowcontrol_reg_obj.tpExtEn, bits_slowcontrol_reg_obj.tpEn}) );

		      
		  endtask: body




endclass: CLICTD_configure_external_testpulse_sequence

`endif // __CLICTD_configure_external_testpulse_sequence_sv__





  



