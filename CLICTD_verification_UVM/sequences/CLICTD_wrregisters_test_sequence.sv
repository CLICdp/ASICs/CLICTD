// Filename           : CLICTD_wrregisters_test_sequence.sv
// Author             : Núria Egidos 
// Created on         : 27/9/18
// Last modification  : 27/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence for the WRREGISTERS test: write a random value to all slow control registers and then read all slow control registers in a row
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/



`ifndef __CLICTD_wrregisters_test_sequence_sv__
`define __CLICTD_wrregisters_test_sequence_sv__



class CLICTD_wrregisters_test_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_wrregisters_test_sequence) // Register the component to the factory


     ChipStatusInfo chip_status_obj;
     addresses_registers_slowcontrol_type addresses_registers_slowcontrol;
     rand bit [`NUMBER_SLOWCONTROL_REGISTERS-1:0][7:0] random_register_values;

     // Constructor
     function new(string name = "CLICTD_wrregisters_test_sequence");
          super.new(name);
     endfunction: new


     task body ();
          logic [7:0] register_address = 0;

          chip_status_obj.chip_status = STATUS_WRITE_ALL_SLOWCONTROL_REGISTERS; 
          chip_status_obj.chip_status_update_event.trigger(); //trigger from within the sequence, so that the start readout condition has already occurred
		      `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);

          // Write some random value to all slow control registers
          foreach(addresses_registers_slowcontrol[i]) begin
										       /*
										       if (addresses_registers_slowcontrol[i]== `ADDRESS_matrixConfig) begin
																	
																	       // With the netlist netlisPostlayout_20190120, we observed there were some timing violations in the maximum corner when register `ADDRESS_matrixConfig is written
																								
																								//	# UVM_INFO ../environment//CLICTD_regslowcontrol_model.sv(131) @ 2495040000: uvm_test_top.env.regslowcontrol_scoreboard.regslowcontrol_model [Transaction from monitor] register_address = 04, register_content = c2, 
																								//	chip_status = STATUS_WRITE_ALL_SLOWCONTROL_REGISTERS
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304398 ps, posedge CK:2534304453 ps, 141 ps );
																								//	#    Time: 2534304749 ps  Iteration: 0  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[2] 
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304329 ps, posedge CK:2534304474 ps, 147 ps );
																								//	#    Time: 2534304751 ps  Iteration: 1  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/EOC/\countReadoutn_D_reg[6] 
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304352 ps, posedge CK:2534304474 ps, 153 ps );
																								//	#    Time: 2534304751 ps  Iteration: 1  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/EOC/\countReadoutn_D_reg[4] 
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304362 ps, posedge CK:2534304475 ps, 154 ps );
																								//	#    Time: 2534304752 ps  Iteration: 1  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/EOC/\countReadoutn_D_reg[5] 
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304374 ps, posedge CK:2534304475 ps, 158 ps );
																								//	#    Time: 2534304752 ps  Iteration: 1  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/EOC/\countReadoutn_D_reg[1] 
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304331 ps, posedge CK:2534304475 ps, 147 ps );
																								//	#    Time: 2534304752 ps  Iteration: 1  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/EOC/\countReadoutn_D_reg[7] 
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304405 ps, posedge CK:2534304453 ps, 143 ps );
																								//	#    Time: 2534304758 ps  Iteration: 0  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[3] 
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304416 ps, posedge CK:2534304454 ps, 146 ps );
																								//	#    Time: 2534304773 ps  Iteration: 0  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/countReadoutn_Del_reg
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304419 ps, posedge CK:2534304453 ps, 147 ps );
																								//	#    Time: 2534304776 ps  Iteration: 0  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[6] 
																								//	# ** Error: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v(10190): $setup( negedge D:2534304421 ps, posedge CK:2534304454 ps, 147 ps );
																								//	#    Time: 2534304779 ps  Iteration: 0  Instance: /CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[7] 
																								
																								// These violations were caused by writing into registers ADDRESS_configCtrl and ADDRESS_readoutCtrl, which would trigger an unintentional readout started from the slow control;
																								// so for this test, these registers are skipped - they will be checked with other tests - , which avoids the timing violations
																									
         														 
																								// To try to identify the origin of the error, we first force a 0 at this register and then write some other value										

																	      sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 						send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(8'h00));
																							
																							
																							sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 						send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(8'hc0));
																							
																							sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                       send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(random_register_values[i]));
																							
																							sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 						send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(8'h00));
																							
																							sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 						send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(8'h02));
																							
																							
																							sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                       send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(random_register_values[i]));
																							
																							sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 						send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(8'h00));
																							
																							sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 						send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(8'h02));
																							
																							sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 						send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(8'hc0));
																	
																	end
																	*/
                 if (addresses_registers_slowcontrol[i]!= `ADDRESS_configCtrl && addresses_registers_slowcontrol[i]!= `ADDRESS_readoutCtrl) begin
																	sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 //$display("%t write operation transaction sent: address %h, content %h",$realtime,addresses_registers_slowcontrol[i],random_register_values[i]);
                 send_slowcontrol_transaction_write(.register_address(addresses_registers_slowcontrol[i]), .register_content(random_register_values[i]));
                 //$display("%t write operation transaction sent: address %h, content %h",$realtime,addresses_registers_slowcontrol[i],random_register_values[i]);
																	end
          end


          chip_status_obj.chip_status = STATUS_READ_ALL_SLOWCONTROL_REGISTERS; 
          chip_status_obj.chip_status_update_event.trigger(); //trigger from within the sequence, so that the start readout condition has already occurred
		      `uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);

					// Read all slow control registers to check if their content 
          // matches the value sent before
          foreach(addresses_registers_slowcontrol[i]) begin
										       if (addresses_registers_slowcontrol[i]!= `ADDRESS_configCtrl && addresses_registers_slowcontrol[i]!= `ADDRESS_readoutCtrl) begin
                 sc_trans = CLICTD_slowcontrol_transaction::type_id::create(.name("sc_trans"), .contxt(get_full_name()));
                 //$display("%t read operation transaction sent: address %h",$realtime,addresses_registers_slowcontrol[i]);
                 send_slowcontrol_transaction_read(.register_address(addresses_registers_slowcontrol[i]));
                 //$display("%t read operation transaction sent: address %h",$realtime,addresses_registers_slowcontrol[i]);
																	end
          end
										
										
																	
     endtask: body




endclass: CLICTD_wrregisters_test_sequence

`endif // __CLICTD_wrregisters_test_sequence_sv__
