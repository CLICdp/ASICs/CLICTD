// Filename           : CLICTD_testpulsecontrol_test_virtual_sequence.sv
// Author             : N�ria Egidos 
// Created on         : 6/11/18
// Last modification  : 6/11/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to perform the test pulse control test (and auxiliary sequences)
// Derived from       : https://colorlesscube.com/uvm-guide-for-beginners/chapter-4-transactions-sequences-and-sequencers/


// Common constraints: https://www.chipverify.com/systemverilog/constraint-examples-sv


`ifndef __CLICTD_testpulsecontrol_test_virtual_sequence_sv__
`define __CLICTD_testpulsecontrol_test_virtual_sequence_sv__



class CLICTD_start_shutter_sequence extends uvm_sequence#(CLICTD_pins_transaction);
      `uvm_object_utils(CLICTD_start_shutter_sequence) // Register the component to the factory
						
						CLICTD_pins_transaction pins_trans;
						
						
						
						
						task body();
						     pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
												
											//$display("DISPLAY - start shutter seq - %t",$realtime);
																		
											// Start shutter
											start_item(pins_trans);
											// Default values
											pins_trans.PWREN_pad = 1'b1;     
        			pins_trans.READOUT_pad = 1'b0;	
        			pins_trans.RSTN_pad = 1'b1;
        			pins_trans.SHUTTER_pad = 1'b0;
        			pins_trans.disc = '0; 
        			pins_trans.TPULSE_pad = 1'b0;
        			#100;
											pins_trans.SHUTTER_pad = 1'b1;
											finish_item(pins_trans);
																						
      endtask: body

endclass: CLICTD_start_shutter_sequence	

// -----------------------------------------------------------------------------------------------------------------

class CLICTD_stop_shutter_sequence extends uvm_sequence#(CLICTD_pins_transaction);
      `uvm_object_utils(CLICTD_stop_shutter_sequence) // Register the component to the factory
						
						CLICTD_pins_transaction pins_trans;
						
						
						
						
						task body();
						     pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
								
								
								   //$display("DISPLAY - stop shutter seq - %t",$realtime);
											
        			start_item(pins_trans);
											// Default values
											pins_trans.PWREN_pad = 1'b1;     
        			pins_trans.READOUT_pad = 1'b0;	
        			pins_trans.RSTN_pad = 1'b1;
        			pins_trans.disc = '0; 
        			pins_trans.TPULSE_pad = 1'b0;
											// Stop shutter
											pins_trans.SHUTTER_pad = 1'b0;
        			finish_item(pins_trans);
        			#100;
										
      endtask: body

endclass: CLICTD_stop_shutter_sequence						

// -----------------------------------------------------------------------------------------------------------------

class CLICTD_startstop_internal_testpulse_sequence extends CLICTD_slowcontroltrans_base_sequence;
     `uvm_object_utils(CLICTD_startstop_internal_testpulse_sequence) // Register the component to the factory

						
     // Constructor
     function new(string name = "CLICTD_startstop_internal_testpulse_sequence");
          super.new(name);
     endfunction: new

     task body ();
					            
           // Start test pulse											
           // Write register internalStrobes: set tpInt to 1'b1 
           // The moment this bit is set high, the test pulse starts.
           bits_slowcontrol_reg_obj.tpInt = 1'b1;
           send_slowcontrol_transaction_write(.register_address(`ADDRESS_internalStrobes), .register_content({6'b0, bits_slowcontrol_reg_obj.pwrInt, bits_slowcontrol_reg_obj.tpInt}) );
											//$display("DISPLAY - start internal test pulse - %t",$realtime);

           // Stop test pulse
           // The moment tpInt is cleared to 1'b0, the test pulse stops
           bits_slowcontrol_reg_obj.tpInt = 1'b0;
           send_slowcontrol_transaction_write(.register_address(`ADDRESS_internalStrobes), .register_content({6'b0, bits_slowcontrol_reg_obj.pwrInt, bits_slowcontrol_reg_obj.tpInt}) );
											//$display("DISPLAY - stop internal test pulse - %t",$realtime);
											
											// Wait some time for the I2C transfer to finish, so that the physical line associated to tpInt changes its value
											// 25 ns/bit * (start condition + 7 bits address + 1 bit R/W + 8 bits data  + 2 bits ACK + stop condition + safety margin)
											#500;

     endtask: body

endclass: CLICTD_startstop_internal_testpulse_sequence


// -----------------------------------------------------------------------------------------------------------------

// Note: the pulse applied from the slow control has a minimum duration set by the time required to transfer the slow control command,
// which exceeds the counting capability of ToT and ToA counters, which will saturate to the maximum value
class CLICTD_apply_onepulse_fromslowcontrol_and_shutter_virtual_sequence extends uvm_sequence#(uvm_sequence_item);
      `uvm_object_utils(CLICTD_apply_onepulse_fromslowcontrol_and_shutter_virtual_sequence) // Register the component to the factory

      CLICTD_slowcontrol_sequencer slowcontrol_sqr;
      CLICTD_pins_sequencer pins_sqr;
						
						
						BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
     	ChipStatusInfo chip_status_obj;
     	ReportingEnable reporting_enable_obj;
     	FieldsConfigureMatrix config_matrix_obj;
						
						
						// Sequence to start the shutter 
						CLICTD_start_shutter_sequence start_shutter_seq;
						// Sequence to apply a test pulse from the slow control
						CLICTD_startstop_internal_testpulse_sequence startstop_internal_testpulse_seq;
						// Sequence to stop the shutter
						CLICTD_stop_shutter_sequence stop_shutter_seq;
						
						// Constructor
     	function new(string name = "CLICTD_testpulsecontrol_test_virtual_sequence");
          	super.new(name);
     	endfunction: new
						
						task body();
						
						      start_shutter_seq = CLICTD_start_shutter_sequence::type_id::create( .name("start_shutter_seq"), .contxt( get_full_name() ) );
												startstop_internal_testpulse_seq = CLICTD_startstop_internal_testpulse_sequence::type_id::create( .name("startstop_internal_testpulse_seq"), .contxt( get_full_name() ) );
												startstop_internal_testpulse_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
												startstop_internal_testpulse_seq.chip_status_obj = chip_status_obj;
												startstop_internal_testpulse_seq.reporting_enable_obj = reporting_enable_obj;
												startstop_internal_testpulse_seq.config_matrix_obj = config_matrix_obj;
												stop_shutter_seq = CLICTD_stop_shutter_sequence::type_id::create( .name("stop_shutter_seq"), .contxt( get_full_name() ) );
												
						     
											// Start the shutter
											start_shutter_seq.start( .sequencer(pins_sqr) );
											// Apply a test pulse from the slow control
											startstop_internal_testpulse_seq.start( .sequencer(slowcontrol_sqr) );
											// Stop the shutter
											stop_shutter_seq.start( .sequencer(pins_sqr) );
						endtask: body
						
endclass: CLICTD_apply_onepulse_fromslowcontrol_and_shutter_virtual_sequence


// -----------------------------------------------------------------------------------------------------------------


// In this case, the test pulse we apply from TPULSE_pad will have a ToT/ToA lower than the maximum
class CLICTD_apply_onepulse_fromtpulsepad_and_shutter_sequence extends uvm_sequence#(CLICTD_pins_transaction);
      `uvm_object_utils(CLICTD_apply_onepulse_fromtpulsepad_and_shutter_sequence) // Register the component to the factory
						
						CLICTD_pins_transaction pins_trans;
						FieldsTimeMeasurements time_measurements_obj;
						
						task body();
						     pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
								
											// Default values
        			start_item(pins_trans);
											pins_trans.PWREN_pad = 1'b1;     
        			pins_trans.READOUT_pad = 1'b0;	
        			pins_trans.RSTN_pad = 1'b1;
        			pins_trans.SHUTTER_pad = 1'b0;
        			pins_trans.disc = '0; 
        			pins_trans.TPULSE_pad = 1'b0;
        			finish_item(pins_trans);
        			#100;
											
											
											// Start shutter
											start_item(pins_trans);
											pins_trans.SHUTTER_pad = 1'b1;
											finish_item(pins_trans);
											
											#(time_measurements_obj.delay_to_startpulses_after_shutter_ns);
											
											// Start test pulse
											start_item(pins_trans);
        			pins_trans.TPULSE_pad = 1'b1;
        			finish_item(pins_trans);
											#(time_measurements_obj.tot_short_ns);
											
											// Finish test pulse
											start_item(pins_trans);
        			pins_trans.TPULSE_pad = 1'b0;
        			finish_item(pins_trans);
											
											#(time_measurements_obj.toa_ns - time_measurements_obj.tot_short_ns);
																						
											// Finish shutter
											start_item(pins_trans);
											pins_trans.SHUTTER_pad = 1'b0;
											finish_item(pins_trans);
											#100;
											
      endtask: body


endclass: CLICTD_apply_onepulse_fromtpulsepad_and_shutter_sequence	



// -----------------------------------------------------------------------------------------------------------------


	
class CLICTD_testpulsecontrol_test_virtual_sequence extends uvm_sequence#(uvm_sequence_item);
      `uvm_object_utils(CLICTD_testpulsecontrol_test_virtual_sequence) // Register the component to the factory

      CLICTD_slowcontrol_sequencer slowcontrol_sqr;
      CLICTD_pins_sequencer pins_sqr;
												
						BitsMonitorSlowcontrolReg bits_slowcontrol_reg_obj;
     	ChipStatusInfo chip_status_obj;
     	ReportingEnable reporting_enable_obj;
     	FieldsConfigureMatrix config_matrix_obj;
						FieldsTimeMeasurements time_measurements_obj;
						CLICTDevents events_obj;
						
						// Sequence to configure the aquistion mode (nominal/long ToA/photon counting, compression...)
						CLICTD_configure_acquisition_mode_sequence configure_acquisition_mode_seq;

						// Sequence to enable the injection of test pulses and configure this injection to be performed from TPULSE_pad
     	CLICTD_configure_external_testpulse_sequence configure_external_testpulse_seq;
						// Sequence to enable the injection of test pulses and configure this injection to be performed from the slow control
     	CLICTD_configure_internal_testpulse_sequence configure_internal_testpulse_seq;

						// Sequence to apply a test pulse via TPULSE_pad within a shutter
     	CLICTD_apply_onepulse_fromtpulsepad_and_shutter_sequence apply_onepulse_fromtpulsepad_and_shutter_seq;
						// Sequence to apply a test pulse from the slow control within a shutter
						CLICTD_apply_onepulse_fromslowcontrol_and_shutter_virtual_sequence apply_onepulse_fromslowcontrol_and_shutter_virtual_seq;

     	// Sequence to activate serial readout with a slow control command
     	CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence perform_serial_readout_activatefrom_slowcontrol_virtual_seq;
						
						// Constructor
     	function new(string name = "CLICTD_testpulsecontrol_test_virtual_sequence");
          	super.new(name);
     	endfunction: new
						
						task body();
											
											
											time_measurements_obj.digital_pulse_lasts_longer_than_shutter = `NO; // the ToT value that will be forced is shorter than the shutter duration
										
											
											
											// Test pulse activated from the slow control is enabled and test pulsing is enabled
											if (bits_slowcontrol_reg_obj.tpExtEn == 1'b0 && bits_slowcontrol_reg_obj.tpEn == 1'b1) chip_status_obj.chip_status = STATUS_TESTPULSE_INTERNAL_WORKING; 
		      			// Test pulse activated from the slow control is disabled and/or test pulsing is disabled
											else chip_status_obj.chip_status = STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS;
											// We just need to make sure that the timeout set to check whether something happens is larger than the transmission delay of the slow control command
											
											
											
											 configure_acquisition_mode_seq = CLICTD_configure_acquisition_mode_sequence::type_id::create( .name( "configure_acquisition_mode_seq" ), .contxt( get_full_name() ) );
            configure_acquisition_mode_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
					       configure_acquisition_mode_seq.chip_status_obj = chip_status_obj;
					       configure_acquisition_mode_seq.reporting_enable_obj = reporting_enable_obj;
												configure_acquisition_mode_seq.config_matrix_obj = config_matrix_obj;
												
												configure_external_testpulse_seq = CLICTD_configure_external_testpulse_sequence::type_id::create( .name( "configure_external_testpulse_seq" ), .contxt( get_full_name() ) );
												configure_external_testpulse_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
     							configure_external_testpulse_seq.chip_status_obj = chip_status_obj;
     							configure_external_testpulse_seq.reporting_enable_obj = reporting_enable_obj;
     							configure_external_testpulse_seq.config_matrix_obj = config_matrix_obj;
												
												configure_internal_testpulse_seq = CLICTD_configure_internal_testpulse_sequence::type_id::create( .name( "configure_internal_testpulse_seq" ), .contxt( get_full_name() ) );
												configure_internal_testpulse_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
					       configure_internal_testpulse_seq.chip_status_obj = chip_status_obj;
					       configure_internal_testpulse_seq.reporting_enable_obj = reporting_enable_obj;
												configure_internal_testpulse_seq.config_matrix_obj = config_matrix_obj;
						
            apply_onepulse_fromtpulsepad_and_shutter_seq = CLICTD_apply_onepulse_fromtpulsepad_and_shutter_sequence::type_id::create( .name( "apply_onepulse_fromtpulsepad_and_shutter_seq" ), .contxt( get_full_name() ) );
												apply_onepulse_fromtpulsepad_and_shutter_seq.time_measurements_obj = time_measurements_obj;
												
												
												apply_onepulse_fromslowcontrol_and_shutter_virtual_seq = CLICTD_apply_onepulse_fromslowcontrol_and_shutter_virtual_sequence::type_id::create( .name("apply_onepulse_fromslowcontrol_and_shutter_virtual_seq"), .contxt( get_full_name() ) );
											 apply_onepulse_fromslowcontrol_and_shutter_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
     							apply_onepulse_fromslowcontrol_and_shutter_virtual_seq.chip_status_obj = chip_status_obj;
     							apply_onepulse_fromslowcontrol_and_shutter_virtual_seq.reporting_enable_obj = reporting_enable_obj;
     							apply_onepulse_fromslowcontrol_and_shutter_virtual_seq.config_matrix_obj = config_matrix_obj;
												apply_onepulse_fromslowcontrol_and_shutter_virtual_seq.pins_sqr = pins_sqr;
												apply_onepulse_fromslowcontrol_and_shutter_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
												

            perform_serial_readout_activatefrom_slowcontrol_virtual_seq = CLICTD_perform_serial_readout_activatefrom_slowcontrol_virtual_sequence::type_id::create( .name( "perform_serial_readout_activatefrom_slowcontrol_virtual_seq" ), .contxt( get_full_name() ) );
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.chip_status_obj = chip_status_obj;
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.events_obj = events_obj;
					       perform_serial_readout_activatefrom_slowcontrol_virtual_seq.bits_slowcontrol_reg_obj = bits_slowcontrol_reg_obj;
												perform_serial_readout_activatefrom_slowcontrol_virtual_seq.config_matrix_obj = config_matrix_obj;
            perform_serial_readout_activatefrom_slowcontrol_virtual_seq.slowcontrol_sqr = slowcontrol_sqr;
												
           
											
											
											 // Update the expected hit flag according to the compression configuration:
											 // if compression is disabled, all hit flags should be 1
											 for (int c = 0; c < `COLUMNS; c++) begin
															  for (int ss = 0; ss < `SUPERPIXEL_SEGMENTS; ss++) begin
																				  for (int sis =0; sis < `SUPERPIXELS_IN_ONE_SEGMENT; sis++) begin
						                     config_matrix_obj.expected_hit_flag_STATUS_MATRIX_READOUT_ACQUIRED[c][ss][sis] = 1'b1;
																						end
																	end
												end
            // Nominal readout, compression disabled
            bits_slowcontrol_reg_obj.noCompr = 1'b1;
            bits_slowcontrol_reg_obj.photonCnt = 1'b0; bits_slowcontrol_reg_obj.longCnt = 1'b0;
            // totDiv = 0
            bits_slowcontrol_reg_obj.totDiv = 2'b00;
												// Send the acquisition configuration to the slow control
           	chip_status_obj.chip_status = STATUS_CONFIG_ACQUISITION_MODE; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	configure_acquisition_mode_seq.start(  .sequencer(slowcontrol_sqr) );
												
												
												
										
												// Enable test pulse injection and configure test pulse to be injected from TPULSE_pad
											 configure_external_testpulse_seq.start ( .sequencer(slowcontrol_sqr) );

            #100;

           	// Enable 100 MHz clock
           	events_obj.enable_clock_100MHz_event.trigger();

												// Wait some time for the clock to propagate across the chip
												#100;
            

            
            // EXTERNAL CONFIGURED, INTERNAL APPLIED -> NO RESPONSE SHOULD BE OBSERVED
            chip_status_obj.chip_status = STATUS_TESTPULSE_EXTERNAL_NOTHING_HAPPENS; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	apply_onepulse_fromslowcontrol_and_shutter_virtual_seq.start(  .sequencer(null) );
												#(`TIMEOUT_UPDATE_DISCTP_WHEN_TESTPULSE_IS_APPLIED);
             
           
 											// EXTERNAL CONFIGURED, EXTERNAL APPLIED -> SOME RESPONSE SHOULD BE OBSERVED
												chip_status_obj.chip_status = STATUS_TESTPULSE_EXTERNAL_WORKING; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	apply_onepulse_fromtpulsepad_and_shutter_seq.start(  .sequencer(pins_sqr) );


												// Wait some time before deactivating the clock 
												#100;

           	// Disable 100 MHz clock
           	events_obj.disable_clock_100MHz_event.trigger();


           	// Readout acquisition (activated from the slow control)
           	chip_status_obj.chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( slowcontrol_sqr ));
												
												
												#100;
											
										
												// Enable test pulse injection and configure test pulse to be injected from the slow control
											 configure_internal_testpulse_seq.start ( .sequencer(slowcontrol_sqr) );


           	
            // INTERNAL CONFIGURED, EXTERNAL APPLIED -> NO RESPONSE SHOULD BE OBSERVED
            chip_status_obj.chip_status = STATUS_TESTPULSE_INTERNAL_NOTHING_HAPPENS; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
												
												#100;
												
												// Enable 100 MHz clock
           	events_obj.enable_clock_100MHz_event.trigger();

												// Wait some time for the clock to propagate across the chip
												#100;
												
           	apply_onepulse_fromtpulsepad_and_shutter_seq.start(  .sequencer(pins_sqr) );
												#(`TIMEOUT_UPDATE_DISCTP_WHEN_TESTPULSE_IS_APPLIED);
												
												
												// INTERNAL CONFIGURED, INTERNAL APPLIED -> SOME RESPONSE SHOULD BE OBSERVED
												chip_status_obj.chip_status = STATUS_TESTPULSE_INTERNAL_WORKING; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
												
												#100;
												// Enable 100 MHz clock
           	//events_obj.enable_clock_100MHz_event.trigger();

												// Wait some time for the clock to propagate across the chip
												//#100;
												
												
           	apply_onepulse_fromslowcontrol_and_shutter_virtual_seq.start(  .sequencer(null) );


												// Wait some time before deactivating the clock 
												#100;

           	// Disable 100 MHz clock
           	events_obj.disable_clock_100MHz_event.trigger();


           	// Readout acquisition (activated from the slow control)
           	chip_status_obj.chip_status = STATUS_MATRIX_READOUT_ACQUIRED_shorttot_nominal_nocompr_totDiv0; 
		         	chip_status_obj.chip_status_update_event.trigger();
           	`uvm_info("chip_status", $sformatf("Chip status: %s", chip_status_obj.chip_status), UVM_LOW);
           	perform_serial_readout_activatefrom_slowcontrol_virtual_seq.start( .sequencer( slowcontrol_sqr ));
												
											
											
											
						
						endtask: body
						
endclass: CLICTD_testpulsecontrol_test_virtual_sequence



`endif // __CLICTD_testpulsecontrol_test_virtual_sequence_sv__





  



