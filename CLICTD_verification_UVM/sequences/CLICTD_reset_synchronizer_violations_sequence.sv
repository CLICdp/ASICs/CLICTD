// Filename           : CLICTD_reset_synchronizer_violations_sequence.sv
// Author             : N�ria Egidos 
// Created on         : 7/12/18
// Last modification  : 25/1/19
// Project            : Doctoral student, verification of CLICTD
// Description        : Sequence to apply multiple reset pulses at RSTN_pad, with the release edge at a certain distance
//                      with the rising edge of the 40 MHz clock (a separation short enough to trigger possible timing 
//                      violations at the reset synchronizer)



`ifndef __CLICTD_reset_synchronizer_violations_sequence_sv__
`define __CLICTD_reset_synchronizer_violations_sequence_sv__




//                 ____      ____      ____      ____      ____      ____      ____      ____      ____      ____      ____                
// CLK 40 MHz ____|    |____|    |____|    |____|    |____|    |____|    |____|    |____|    |____|    |____|    |____|    |____
//
//                 <----- fixed part of duration: count 10 falling CLK edges to allow logic to react -----------><-a-><-b->
//            ____                                                                                                        _________
// RSTN_pad       |__________________________________________________________________________________________________|||||          
// RSTN_pad rises at some point within a,b region                                                                                     

// Zoom in the region a,b:
//
//             ____<------------------ a: 12.5 ns -------------------> ___________________________________________________             
// CLK 40 MHz     |___________________________________________________|<------------------- b: 12.5 ns ------------------>|____
//
//                <- (12.5 ns - setup_duration) -><- setup_duration -> <- hold_duration -> <- (12.5 ns - hold_duration) -> 
//                                                                                          ___________________________________
// RSTN_pad    ____________________________________/////////////////////////////////////////       
// Setup_duration region has a maximum duration corresponding to the maximum setup time observed in the .sdf delay files of CLICTD extraction: 0.621 ns, though 1 ns is taken for safety margin
// Hold_duration region has a maximum duration corresponding to the maximum hold time observed in the .sdf delay files of CLICTD extraction: 0.120 ns, though 0.5 ns is taken for safety margin
// RSTN_pad is forced to change (it is released) within the setup_duration-hold_duration window, so as to trigger possible timing violations.
// We will send several reset pulses in a row; in each of them, the setup_duration-hold_duration window will have a different duration. We will start with the maximum duration (maximum values
// for setup_duration and hold_duration), and decrease both of them by 1 ps (the timescale resolution) with every pulse we send, until both are 0 (RSTN_pad is released at the same time the clock
// edge arrives).





class CLICTD_reset_synchronizer_violations_sequence extends uvm_sequence#(CLICTD_pins_transaction);
     `uvm_object_utils(CLICTD_reset_synchronizer_violations_sequence) // Register the component to the factory


					ChipStatusInfo chip_status_obj;
					CLICTDevents events_obj;

     CLICTD_pins_transaction pins_trans;
					
					shortreal delay_to_wait_after_fall_edge_clk40;
					shortreal distance_resetrelease_and_clockrise,aux_time_falledge_clk40; // shorteal is used to be able to handle decimal numbers
			  int counter_falling_edges_clk40,counter_reset_pulses;
			

     
     // Constructor
     function new(string name = "CLICTD_reset_synchronizer_violations_sequence");
          super.new(name);
     endfunction: new
					
					
					// Detect when the reset (input to the reset synchronizer) is released and compute the time difference to the 
					// closest rising edge of the clock (input to the reset synchronizer)
					task detect_distance_resetrelease_and_clockrise();
					
					         shortreal aux_time_riseclk40, aux_time_fallclk40, aux_time_riserstnin;
														        
														forever begin								
																						aux_time_riseclk40 = 0; aux_time_fallclk40 = 0; aux_time_riserstnin = 0;
																						
																						
																						events_obj.rise_digitalperi_rstnctrl_rstnin_event.wait_trigger();
																						aux_time_riserstnin = $realtime;
																						fork
																						    // Detect rising edge of the clock (input to the reset synchronizer)
																										begin
																										      events_obj.rise_digitalperi_rstnctrl_clk40_event.wait_trigger();
																																aux_time_riseclk40 = $realtime;
																										end
																										// Detect falling edge of the clock (input to the reset synchronizer)
																										begin
																										      events_obj.fall_digitalperi_rstnctrl_clk40_event.wait_trigger();
																																// Capture when the next rising edge of the clock arrives
																																events_obj.rise_digitalperi_rstnctrl_clk40_event.wait_trigger();
																																aux_time_fallclk40 = $realtime;																															
																										end
																						join_any
																						disable fork;
																						
																						// If the falling edge of the clock is the first edge to arrive after the release of the reset,
																						// we're evaluating possible violations in the hold time
																						if (aux_time_fallclk40 > 0) begin
																						    distance_resetrelease_and_clockrise = 2*`CLK_HALF_PERIOD_40MHz - aux_time_fallclk40;
																										`uvm_info("Reset release", $sformatf("HOLD - Distance between reset release and clk rising edge: %f, reset pulse %0d/%0d, chip_status = %s", distance_resetrelease_and_clockrise,counter_reset_pulses,`EXPECTED_NUMBER_OF_RESET_PULSES,chip_status_obj.chip_status),  UVM_LOW);
																						end
																						
																						// If the rising edge of the clock is the first edge to arrive after the release of the reset,
																						// we're evaluating possible violations in the setup time
																						else if (aux_time_riseclk40 > 0) begin
																						     distance_resetrelease_and_clockrise = aux_time_riseclk40 - aux_time_riserstnin;
																											`uvm_info("Reset release", $sformatf("SETUP - Distance between reset release and clk rising edge: %f, reset pulse %0d/%0d, chip_status = %s", distance_resetrelease_and_clockrise,counter_reset_pulses,`EXPECTED_NUMBER_OF_RESET_PULSES,chip_status_obj.chip_status),  UVM_LOW);
																						end
													end	
					
					endtask: detect_distance_resetrelease_and_clockrise
					
					
					task inject_reset_pulses();
					     counter_reset_pulses = 0;
										delay_to_wait_after_fall_edge_clk40 = 0;
										pins_trans = CLICTD_pins_transaction::type_id::create(.name("pins_trans"), .contxt(get_full_name()));
					     while (counter_reset_pulses < `EXPECTED_NUMBER_OF_RESET_PULSES) begin
																	start_item(pins_trans);
          							pins_trans.RSTN_pad = 1'b1;
																	pins_trans.PWREN_pad = 1'b1;     
          							pins_trans.READOUT_pad = 1'b0;	
		        							pins_trans.SHUTTER_pad = 1'b0;
          							pins_trans.disc = '0; 
          							pins_trans.TPULSE_pad = 1'b0;
          							finish_item(pins_trans);
																	counter_falling_edges_clk40 = 0;
																																																				
																	events_obj.rise_digitalperi_rstnctrl_clk40_event.wait_trigger();
																	start_item(pins_trans);
																	pins_trans.RSTN_pad = 1'b0;
																	finish_item(pins_trans); 
																	counter_reset_pulses++; 

																	// Wait 10 falling edges of CLK
																	while (counter_falling_edges_clk40 < 10) begin
																								events_obj.fall_digitalperi_rstnctrl_clk40_event.wait_trigger();
																	       counter_falling_edges_clk40++;
																	end
																	
																	// From the last falling edge, add some delay and then release the reset
																	
																	#12;
																	#(0.001*counter_reset_pulses);
																	start_item(pins_trans);
																	pins_trans.RSTN_pad = 1'b1;
																	finish_item(pins_trans);
																	
																	#(2*`CLK_HALF_PERIOD_40MHz);
																	
										end
										
					endtask: inject_reset_pulses
     
					
			

     task body ();
          
										uvm_event finish_simulation_event = new("finish_simulation_event");
																														
										fork
										    /*
										    begin
														     events_obj.fall_digitalperi_rstnctrl_clk40_event.wait_trigger();
																			events_obj.fall_digitalperi_rstnctrl_clk40_event.wait_trigger();
																			actual_duration_low_level = $realtime;
																			events_obj.rise_digitalperi_rstnctrl_clk40_event.wait_trigger();
																			actual_duration_low_level = $realtime - actual_duration_low_level;
																			$display("DISPLAY - reset sync seq - %t actual duration low level %f",$realtime,actual_duration_low_level);
																			finish_simulation_event.wait_trigger();
														end
														*/
										    begin
														     detect_distance_resetrelease_and_clockrise();
														end
														begin
														      
																				inject_reset_pulses();
																				/*
																				//We need to apply a first reset pulse to enable the propagation of the clock to the reset synchronizer,
																				// otherwise the clock doesn't reach it and the rest of this test was not working!
																				// Default values for signals
																				start_item(pins_trans);
          										pins_trans.RSTN_pad = 1'b1;
																				pins_trans.PWREN_pad = 1'b1;     
          										pins_trans.READOUT_pad = 1'b0;	
		        										pins_trans.SHUTTER_pad = 1'b0;
          										pins_trans.disc = '0; 
          										pins_trans.TPULSE_pad = 1'b0;
          										finish_item(pins_trans); 

          										#500;
          										start_item(pins_trans);
          										pins_trans.RSTN_pad = 1'b0;
																				pins_trans.PWREN_pad = 1'b1;     
          										pins_trans.READOUT_pad = 1'b0;	
		        										pins_trans.SHUTTER_pad = 1'b0;
          										pins_trans.disc = '0; 
          										pins_trans.TPULSE_pad = 1'b0;
          										finish_item(pins_trans);

				  										    #500;
          										start_item(pins_trans);
          										pins_trans.RSTN_pad = 1'b1;
																				pins_trans.PWREN_pad = 1'b1;     
          										pins_trans.READOUT_pad = 1'b0;	
		        										pins_trans.SHUTTER_pad = 1'b0;
          										pins_trans.disc = '0; 
          										pins_trans.TPULSE_pad = 1'b0;
          										finish_item(pins_trans);
          										#500;
										          */
										
										
																				
																			
																			
																			finish_simulation_event.trigger();
														end
										join_any
										disable fork;
										
										
     endtask: body




endclass: CLICTD_reset_synchronizer_violations_sequence

`endif // __CLICTD_reset_synchronizer_violations_sequence_sv__
