// Filename           : CLICTD_testbench_common.sv
// Author             : N�ria Egidos 
// Created on         : 30/5/18
// Last modification  : 23/10/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Testbench top - this code is to be `included in CLICTD_testbench_top and CLICTD_testbench_top_backannotated,
//                      i.e. it's used for simulations with and without back annotation
// Derived from       : pixel_chip_tb.sv developed by Sara Marconi and Elia Conti for VEPIX53 verification

// Randomization seed: https://stackoverflow.com/questions/31084419/verilog-changing-random-seed



//import CLICTD_test_pkg::*;



	//------------------------- Import UVM package and macros
	import uvm_pkg::*;
	`include <uvm_macros.svh>

 import CLICTD_test_pkg::*;
     

	//------------------------- List of signals

	// I2C interface
  logic SDA_pad;        // SDA pin
  logic SCL_pad;        // SCL pin
  logic [1:0] ADDR_pad; // I2C address least significant bits
  
  logic  CLK_100_n_pad;	// 100 MHz clock (negative polarity) 
  logic  CLK_100_p_pad = 1'b0; // 100 MHz clock (positive polarity) 
  logic  CLK_40_n_pad;	// 40 MHz clock (negative polarity) 
  logic  CLK_40_p_pad = 1'b0;  // 40 MHz clock (positive polarity) 
  //logic  CLK_OUT;       // clock signal to reconstruct DATA_OUT
  
  
	//------------------------- Interfaces
	CLICTD_slowcontrol_interface slowcontrol_if();
  CLICTD_readout_interface     readout_if();
  CLICTD_pins_interface        pins_if();


	//------------------------- Wrapper to connect the interfaces to the DUT
  CLICTD_dut_wrapper dut_wrapper(.slowcontrol_if(slowcontrol_if), .readout_if(readout_if), .pins_if(pins_if));

	//------------------------- Generation of 40 MHz clock (readout)

  always #(`CLK_HALF_PERIOD_40MHz) CLK_40_p_pad = ~CLK_40_p_pad;  
  assign CLK_40_n_pad = ~CLK_40_p_pad;

	//------------------------- Generation of 100 MHz clock (measurements)
	always #(`CLK_HALF_PERIOD_100MHz) CLK_100_p_pad = ~CLK_100_p_pad;
  assign CLK_100_n_pad = ~CLK_100_p_pad;

  assign pins_if.CLK_100_n_pad = CLK_100_n_pad;
  assign pins_if.CLK_100_p_pad = CLK_100_p_pad;
  assign pins_if.CLK_40_n_pad = CLK_40_n_pad;
  assign pins_if.CLK_40_p_pad = CLK_40_p_pad;


  assign slowcontrol_if.ADDR_pad = 2'b0;
 
	


	//------------------------- Run the test
  
	initial begin


    // Initially, the 100 MHz clock is disabled. It will be enabled when acquisition is to be performed
    pins_if.enable_clock_100MHz = 1'b0;

    // Virtual interface registration into the database
    // https://verificationacademy.com/verification-methodology-reference/uvm/docs_1.1b/html/files/base/uvm_config_db-svh.html
    // http://www.verificationguide.com/p/uvm-config-db-configuration-database.html 
    // https://www.synopsys.com/content/dam/synopsys/services/whitepapers/hierarchical-testbench-configuration-using-uvm.pdf
    // static function void set(	uvm_component 	cntxt, string 	inst_name, string 	field_name, T 	value	)
    // cntxt      : hierarchical starting point where the operation is performed. Use null for global scope, use this when we set/get inside a class
    // inst_name  : hierarchical path that limits accessibility of the database entry, name of the instance where the database entry is accessible. 
    // 							Since we are going to store interfaces, we could use the agent's names so that the drivers and monitors contained in the agents 
		//							could access the interfaces. The problem is that what agents are used and thus what interfaces are necessary depends on the test. 
		//							To simplify things, all interfaces are made available from the environment.
    // field_name : name with which the database entry can be retrieved (this is the key or name to search the entry in the database)
    // value      : value to store in the database associated to the specified field_name. In this case, these are the virtual interfaces.
    uvm_config_db#(virtual CLICTD_slowcontrol_interface)::set(null,"*", "slowcontrol_if", slowcontrol_if);
    uvm_config_db#(virtual CLICTD_readout_interface)::set(null,"*", "readout_if", readout_if);
    uvm_config_db#(virtual CLICTD_pins_interface)::set(null,"*", "pins_if", pins_if);
		//uvm_resource_db#(virtual CLICTD_slowcontrol_interface)::set(.scope("ifs"), .name("CLICTD_slowcontrol_interface"), .val(slowcontrol_if));
    //uvm_resource_db#(virtual CLICTD_readout_interface)::set(.scope("ifs"), .name("CLICTD_readout_interface"), .val(readout_if));
    //uvm_resource_db#(virtual CLICTD_pins_interface)::set(.scope("ifs"), .name("CLICTD_pins_interface"), .val(pins_if));
 
    //fork
         //pins_if.apply_reset_pulse(); // but this line and the one below cannot be executed like this: it leads to UVM_FATAL because run_test
                                      // must be executed at time 0, with no delay before
         run_test(); 
    //join_any
    //$stop; // no instruction will be executed after the test has ended (http://www.sunburst-design.com/papers/CummingsDVCon2011_UVM_TerminationTechniques.pdf page 57)
	end
 
 // final $stop;
 // http://forums.accellera.org/topic/169-end-of-test-methodology-in-uvm/

  
