// Filename           : CLICTD_readout_interface.sv
// Author             : Núria Egidos 
// Created on         : 30/5/18
// Last modification  : 30/5/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Interface that communicates the serial readout and the UVM framework


`ifndef __CLICTD_readout_interface_sv__
`define __CLICTD_readout_interface_sv__


interface CLICTD_readout_interface ();

// Clocking block is not needed! (recommendation of Tuomas Poikela)
// Modports: they're used on the DUT side (interfaceinstance.modportx.signaly), 
// because otherwise the synthesizer would interpret all the ports as inouts, 
// while with the modports we can specify which of them are in or out. But the 
// modports are not necessary on the testbench side, so we will just use interfaceinstance.signaly
 
logic DATA_OUT; // Serial readout 
logic CLK_OUT;  // Output clock synchronized to the serial readout

  // Asynchronous modport used by the DUT
  //modport dut_modport(output DATA_OUT);
		// Modports are only required if the interface is to be synthesized, so as to indicate the direction of the ports;
		// for verification, they're not necessary!

endinterface: CLICTD_readout_interface

`endif // __CLICTD_readout_interface_sv__
