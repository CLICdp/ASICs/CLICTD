// Filename           : CLICTD_slowcontrol_interface.sv
// Author             : Núria Egidos 
// Created on         : 30/5/18
// Last modification  : 11/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Interface that communicates the slow control and the UVM framework

`ifndef __CLICTD_slowcontrol_interface_sv__
`define __CLICTD_slowcontrol_interface_sv__


// Include the parameter definition
`ifndef CLK_HALF_PERIOD_40MHz
        `include "../CLICTD_parameters.sv"
`endif

//`include "../CLICTD_typedefs_pkg.sv"



interface CLICTD_slowcontrol_interface;
 `include "./i2cMaster.v"

  //import CLICTD_typedefs_pkg::*;

  tri SDA_pad;        // SDA pin
  tri SCL_pad;        // SCL pin
  logic [1:0] ADDR_pad; // I2C address least significant bits


  // Signals to store the content and address 
  // of the register read and write
  logic [5:0] former_i2cstate = I2CIDLE;
  logic [7:0] register_address_from_operation;
  logic [7:0] register_content_from_operation;
  // Signal that will be set high when a register readout has finished
  logic high_when_regreadout_finished = 1'b0;
  // Signal that will be set high when a register write has finished
  logic high_when_regwrite_finished = 1'b0;

  // From the i2cSlave.v:
  parameter deviceAddress = 5'h14;
  logic [6:0] slave_addr;
  logic reportError = 1'b1; // set to 1 to enable reporting errors

   

 // WORKAROUND
 // the code in i2cMaster.v corresponds to an unidirectional I2C master,
 // while the master that clictdTop expects is bidirectional. To be able
 // to test it without Questa Verification IPs (QVIPs implemented in C3PD),
 // as a first approach SDAen/in/out are retrieved from the dut_wrapper/clictdTop  
 // in the testbench top
 logic SDAen;
 logic SDAin;
 logic SDAout;
 //logic SCLin;

  // Signals to store the status of the matrix configuration 
  int matrix_config_superpixelindex = 0;
  int matrix_config_FFindex = `NUMBER_OF_FF_PER_SUPERPIXEL;
  bit [1:0] matrix_config_half = 2'b00;


  string operation = `WRITE; // operation to be performed with the I2C

  

  // Asynchronous modport used by the DUT
  //modport dut_modport(inout SDA_pad, input SCL_pad, input ADDR_pad);
		// Modports are only required if the interface is to be synthesized, so as to indicate the direction of the ports;
		// for verification, they're not necessary!

 
  //assign SCLin = SCL;
  assign SCL_pad = SCL; 
  assign slave_addr = {deviceAddress,ADDR_pad};

  //assign SDAin = SDA;
		assign SDA_pad = SDA;
		assign SDAin = MS_SDAout;
  assign SL_SDAout = SDAout;
  assign SL_SDAen = SDAen;
  
  // -----------------------------------------------------------------------------
  // Write the content of one register via the slow control
  // -----------------------------------------------------------------------------
  task write_register_slowcontrol(input [7:0] register_address, input [7:0] register_content);
       operation = `WRITE;
       register_content_from_operation = register_content;
       register_address_from_operation = register_address;
       i2c_write_single(slave_addr,register_address,register_content,reportError);
       
   
  endtask: write_register_slowcontrol

  // -----------------------------------------------------------------------------
  // Read the content of one register via the slow control
  // -----------------------------------------------------------------------------
  task read_register_slowcontrol(input [7:0] register_address, output logic [7:0] register_content);
       //high_when_regreadout_finished = 1'b0;

       operation = `READ;
       i2c_read_single(slave_addr,register_address,register_content,reportError);
       
       // Detect when a register readout has finished -> this is used by the 
       // regslowcontrol_agent to know when to generate a transaction with the value
       // of the register read
       register_content_from_operation = register_content;
       register_address_from_operation = register_address;
       
  endtask: read_register_slowcontrol


/*
  // -----------------------------------------------------------------------------
  // Read the content of all slow control registers (this is used in the reset test)
  // -----------------------------------------------------------------------------
  task read_allregisters_slowcontrol_resettest();
      logic [7:0] dummy;
      // When this task is called, reset has already been applied. 
      // Once the reset synchronized to the 40 MHz clock
      // has reached the slow control, turn off the clock
      // to speed up the simulation
      //`ifdef ENABLE_DEACTIVATING_CLOCKS_TO_SPEED_UP_SIMULATION
      //       enable_clock_40MHz = 1'b0;
      //       //enable_clock_100MHz = 1'b0;
      //`endif
      // The 100 MHz clock could in fact be disabled for the whole reset test (it's
      // not required by the reset synchronizer), but it's disabled here for convenience

      for (int i = 0; i < `NUMBER_SLOWCONTROL_REGISTERS; i = i+1 ) begin
          #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_READ_SLOWCONTROL);
	        read_register_slowcontrol(.register_address(i), .register_content(dummy));
          #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_READ_SLOWCONTROL);
	  // the value and address of the register will be stored from this task into 
	  // register_content_from_operation and register_address_from_operation respectively in each iteration
          
          // the call register_address(i) works as long as the first register address is 8'd0 and 
          // increment progressively in one unit. i is cropped to 8 bits internally 
      end
       
  endtask: read_allregisters_slowcontrol_resettest

  // -----------------------------------------------------------------------------
  // Matrix configuration: configure one bit, one pixel, all columns 
  // (the position of the pixel and which bit to configure are not specified, 
  // but these are determined inherently by the time when this task is called,
  // i.e.: it will be called first for the first configuration bit of the first
  // configuration half of pixel 0; next, for the second configuration bit of 
  // the first configuration half of pixel 1, etc.)
  // -----------------------------------------------------------------------------
  task matrixconfig_onebit_onesuperpixel_allcols_onehalf(input [`COLUMNS-1:0] config_word, input [1:0] which_config_half);
          bit configSend; bit [1:0] configStage;

          configStage = which_config_half;

           // Assumption: configSend is 0 at this point (it's so at chip startup and it will
           // be reset to 0 after each configuration bit is provided)

           // Set configSend to 0 to ensure that it's ready to start the configuration of the matrix
           //write_register_slowcontrol(.register_address( `ADDRESS_configCtrl), .register_content({3'b0, 1'b0, 2'b0, which_config_half}) );
           //#(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);

           
           // Write register configData[7:0]: set the value of the corresponding bit for columns 7-0
           write_register_slowcontrol(.register_address( `ADDRESS_configData7downto0), .register_content(config_word[7:0]) );
           #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);
           // Write register configData[15:8]: set the value of the corresponding bit for columns 15-8
           write_register_slowcontrol(.register_address( `ADDRESS_configData15downto8), .register_content(config_word[15:8]) );
           #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);

           // Write configCtrl to set configSend to 1
           configSend = 1'b1;
           write_register_slowcontrol(.register_address( `ADDRESS_configCtrl), .register_content({3'b0, configSend, 2'b0, configStage}) );
           #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);

           // configSend is high during the time required by the I2C to send the command  

           // Write configCtrl to clear configSend to 0
           configSend = 1'b0;
           write_register_slowcontrol(.register_address( `ADDRESS_configCtrl), .register_content({3'b0, configSend, 2'b0, configStage}) );
           #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);


           // Update configuration status
           matrix_config_half = which_config_half;
           if (matrix_config_FFindex > 0) matrix_config_FFindex = matrix_config_FFindex-1;
           else matrix_config_FFindex = `NUMBER_OF_FF_PER_SUPERPIXEL;
					 if (matrix_config_FFindex == `NUMBER_OF_FF_PER_SUPERPIXEL-2 && matrix_config_half == `FIRST_CONFIGURATION_HALF) matrix_config_superpixelindex = matrix_config_superpixelindex + 1;
           
           

  endtask: matrixconfig_onebit_onesuperpixel_allcols_onehalf

  // -----------------------------------------------------------------------------
  // Matrix configuration: configure all bits in one half of the configuration, 
  // one pixel, all columns
  // (the position of the pixel is not specified, but it is determined inherently 
  // by the time when this task is called, i.e.: it will be called first for the 
  // first configuration half of pixel 0; next, for first configuration half of pixel 
  // 1, etc.)
  // -----------------------------------------------------------------------------
  task matrixconfig_allbits_onesuperpixel_allcols_onehalf( input [`NUMBER_OF_FF_PER_SUPERPIXEL-1:0][`COLUMNS-1:0] config_word, input [1:0] which_config_half, input value_for_dummy_bit );
            // Note that bits 21 for the first configuration half; 0 and 21 for the second configuration half 
            // are dummy bits (these are translated to hit_flag, but it shouldn't matter their value)

            // Note that the MSB is sent first!!!!

            // Structure of config_word:
            //                                                _____________________________________________________________
            // bit_index = 0                                 |               config_word[0][`COLUMNS-1:0]                  | <- tpEnableDigital (1st configuration half) or                   
            //     this is last bit sent!                    |                                                             |    value_for_dummy_bit (2nd configuration half)
            //                                               |                                                             |
            //                                               |         ...       ...         ...          ...              |
            //                                               |                                                             |
            //                                               |                                                             |
            // bit_index = `NUMBER_OF_FF_PER_SUPERPIXEL - 1  | config_word[`NUMBER_OF_FF_PER_SUPERPIXEL - 1][`COLUMNS-1:0] | <- value_for_dummy_bit (1st and 2nd configuration halves)
            //     this is first bit sent!                    -------------------------------------------------------------


            int bit_index;

            // Set configSend to 0 to ensure that it's ready to start the configuration of the matrix
            //write_register_slowcontrol(.register_address( `ADDRESS_configCtrl), .register_content({3'b0, 1'b0, 2'b0, which_config_half}) );
            //#(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);

            
            for (int aux_index = 0 ; aux_index < `NUMBER_OF_FF_PER_SUPERPIXEL ; aux_index ++ ) begin
                bit_index = `NUMBER_OF_FF_PER_SUPERPIXEL - 1 - aux_index;
                //$display("Loop matrixconfig_allbits_onesuperpixel_allcols_onehalf, bit_index = %d",bit_index );
								if      (bit_index == `NUMBER_OF_FF_PER_SUPERPIXEL - 1 || (bit_index == 0 && which_config_half == `SECOND_CONFIGURATION_HALF) )
                        matrixconfig_onebit_onesuperpixel_allcols_onehalf(.config_word(value_for_dummy_bit), .which_config_half(which_config_half));
				        else    matrixconfig_onebit_onesuperpixel_allcols_onehalf(.config_word(config_word[bit_index]), .which_config_half(which_config_half));
            end
  endtask: matrixconfig_allbits_onesuperpixel_allcols_onehalf

  // -----------------------------------------------------------------------------
  // Matrix configuration: configure all bits in one half of the configuration, 
  // all pixels in one column (128), all columns
  // -----------------------------------------------------------------------------
  task matrixconfig_allbits_allpixels_allcols_onehalf( input [`ROWS-1:0][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0][`COLUMNS-1:0] config_word, input [1:0] which_config_half, input value_for_dummy_bit);
            // Structure of config_word:
            //
            //                          __________________________________________________________________________________________________________________________________
            // pixel_index = `ROWS - 1 |column 0, pixel `ROWS-1   config_word[`ROWS-1][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0][`COLUMNS-1:0]   column `COLUMNS-1, pixel `ROWS-1 |
            //                         |                                                                                                                                  |
            //                         |                 ...               ...              ...               ...                    ...              ...                 |
            //                         |                                                                                                                                  |
            // pixel_index = n         |                          config_word[n][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0][`COLUMNS-1:0]                                          |
            //                         |                                                                                                                                  |
            //                         |                 ...               ...              ...               ...                    ...              ...                 |
            //                         |                                                                                                                                  |          
            // pixel_index = 0         | column 0, pixel 0        config_word[0][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0][`COLUMNS-1:0]               column `COLUMNS-1, pixel 0 |
            //                          ----------------------------------------------------------------------------------------------------------------------------------
            // 
            // See below a zoom into pixel n:
            //                                                ________________________________________________________________
            // bit_index = 0                                 |               config_word[n][0][`COLUMNS-1:0]                  | <- tpEnableDigital (1st configuration half) or                   
            //     this is last bit sent!                    |                                                                |    value_for_dummy_bit (2nd configuration half)
            //                                               |                                                                |
            //                                               |         ...       ...         ...          ...                 |
            //                                               |                                                                |
            //                                               |                                                                |
            // bit_index = `NUMBER_OF_FF_PER_SUPERPIXEL - 1  | config_word[n][`NUMBER_OF_FF_PER_SUPERPIXEL - 1][`COLUMNS-1:0] | <- value_for_dummy_bit (1st and 2nd configuration halves)
            //     this is first bit sent!                    ----------------------------------------------------------------



          // READOUT FIRST
          // When we readout at this point, zeros are shifted into the matrix configuration, acting as a reset that sets a default
          // value to all the configuration signals. Afterwards we configure them to the desired value.

          // Set configSend to 0 to ensure that it's ready to start the configuration of the matrix
          //write_register_slowcontrol(.register_address( `ADDRESS_configCtrl), .register_content({3'b0, 1'b0, 2'b0, `FIRST_CONFIGURATION_HALF}) );
          //#(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);

          // Write register configCtrl to indicate which of the two configuration
          // stages we are going to start. At this point, configSend is 0, so we
          // can overwrite it with 0
          write_register_slowcontrol(.register_address(`ADDRESS_configCtrl), .register_content({6'b0,which_config_half}));
           #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);

          // Write all bits for all pixels, all columns
          for (int pixel_index = 0; pixel_index < `ROWS; pixel_index = pixel_index + 1 ) begin
              //f$display("Loop matrixconfig_allbits_allpixels_allcols_onehalf, pixel_index = %d",pixel_index );
              matrixconfig_allbits_onesuperpixel_allcols_onehalf( .config_word(config_word[pixel_index]), .which_config_half(which_config_half), .value_for_dummy_bit(value_for_dummy_bit) );

          end

          // Write register configCtrl to indicate which of the two configuration
          // stages we are going to start. At this point, configSend is 0, so we
          // can overwrite it with 0
          write_register_slowcontrol(.register_address(`ADDRESS_configCtrl), .register_content(8'b0));
           #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);
       
          // READOUT AFTER THE CONFIGURATION HAS FINISHED
          // Here it is expected to perform readout to check the configuration we have sent 


  endtask: matrixconfig_allbits_allpixels_allcols_onehalf


  // -----------------------------------------------------------------------------
  // Matrix configuration: configure all bits, all pixels in the matrix (128x16),
  // both configuration stages
  // -----------------------------------------------------------------------------
  task matrixconfig_allbits_allpixels_allcols_twohalves(input [`ROWS-1:0][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0][`COLUMNS-1:0] config_word_first_half, 
                                                        input [`ROWS-1:0][`NUMBER_OF_FF_PER_SUPERPIXEL-1:0][`COLUMNS-1:0] config_word_second_half,
                                                        input value_for_dummy_bit );
           //`ifdef ENABLE_DEACTIVATING_CLOCKS_TO_SPEED_UP_SIMULATION
           //  enable_clock_100MHz = 1'b0; // this clock is no required to configure the matrix, it goes to the pixels for acquisition
           //`endif

           // Set configSend to 0 to ensure that it's ready to start the configuration of the matrix
           write_register_slowcontrol(.register_address( `ADDRESS_configCtrl), .register_content({3'b0, 1'b0, 2'b0, `FIRST_CONFIGURATION_HALF}) );
           #(`NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL);

           // First configuration half
           matrixconfig_allbits_allpixels_allcols_onehalf(.config_word(config_word_first_half), .which_config_half(`FIRST_CONFIGURATION_HALF), .value_for_dummy_bit(value_for_dummy_bit));
           // Second configuration half
           matrixconfig_allbits_allpixels_allcols_onehalf(.config_word(config_word_first_half), .which_config_half(`SECOND_CONFIGURATION_HALF), .value_for_dummy_bit(value_for_dummy_bit));
  endtask: matrixconfig_allbits_allpixels_allcols_twohalves

*/
  


  // -----------------------------------------------------------------------------
  // Store the content and address of the register that has been read when read
  // operation is complete (debug I2C)
  // -----------------------------------------------------------------------------
  always @ (i2cstate) begin
           if (former_i2cstate == I2CSTOP && operation == `READ) begin
               #100; // allow some time for signals to stabilize
               high_when_regreadout_finished = 1'b1;
               #100;
               high_when_regreadout_finished = 1'b0;
               //operation = `WRITE;
           end
           else if (former_i2cstate == I2CSTOP && operation == `WRITE) begin
           #100; // allow some time for signals to stabilize
               high_when_regwrite_finished = 1'b1;
               #100;
               high_when_regwrite_finished = 1'b0;
               //operation = `READ;
           end
           former_i2cstate = i2cstate;
  end
  

endinterface: CLICTD_slowcontrol_interface
  
`endif // __CLICTD_slowcontrol_interface_sv__
