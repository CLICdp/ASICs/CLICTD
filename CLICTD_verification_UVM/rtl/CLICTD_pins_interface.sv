// Filename           : CLICTD_pins_interface.sv
// Author             : Núria Egidos 
// Created on         : 30/5/18
// Last modification  : 30/5/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Interface that communicates several pads and internal signals with the UVM framework


`ifndef __CLICTD_pins_interface_sv__
`define __CLICTD_pins_interface_sv__

// Include the parameter definition
`ifndef CLK_HALF_PERIOD_40MHz
        `include "../CLICTD_parameters.sv"
`endif


interface CLICTD_pins_interface ( );


  bit enable_clock_100MHz; // set to 1 to enable applying the 100 MHz clock to the chip, 0 to disable it

  logic  CLK_100_n_pad;	// 100 MHz clock (negative polarity) (measurements)
  logic  CLK_100_p_pad; // 100 MHz clock (positive polarity) 
  logic  CLK_40_n_pad;  // 40 MHz clock (negative polarity)(readout)
  logic  CLK_40_p_pad;   // 40 MHz clock (positive polarity) 

  //`CLICTD_parameters 

  //-------------------- DUT input pads 
  logic  PWREN_pad;      // Pad to provide the power pulse
  logic  TPULSE_pad;    // Pad to provide the external test pulse 
  logic  READOUT_pad;		// Pad to activate readout with an external pulse
  logic  SHUTTER_pad;		// Pad to provide the shutter signal 
  logic  RSTN_pad = 1'b1;      // Reset signal (active low)
  

	//-------------------- DUT output pads
  logic ENABLE_OUT_pad; // Signal that is high while the serial readout is in process


  //-------------------- Internal signals of the DUT to probe

  // Reset test (check that they're low)
  logic [`COLUMNS-1:0] columnDone;
  logic [`COLUMNS-1:0] dataIn;
  logic [`COLUMNS-1:0] commonToken;


  // Readout control test 
  // Signal used to check if the external readout control is enabled or disabled
  logic readoutStartExtEn;
  // Output of the multiplexer that selects internal/external readout -> check that this value matches the configuration provided with the slow control
  logic readoutStart;

  // Test pulse test
  // Signal used to check if the external test pulse is enabled or disabled
  logic testPulseExtEnable;
  // Output of the multiplexer that selects internal/external test pulse -> check that this value matches the configuration provided with the slow control
  logic testPulse;

  // Power pulse test 
  // Signal used to enable the external power pulse -> check if it's enabled or disabled
  logic powerExtEnable;
	// Output of the multiplexer that selects internal/external power pulse -> check that this value matches the configuration provided with the slow control
  logic powerEnable;
  // Signal to which the pwrInt bit (from the internalStrobes register of the slow control) is translated
  logic powerInternal;
  // Signal that propagates the 100 MHz clock into the matrix only when powerEnable is low (active, clock gating)
  logic CLK_100_GATED; 
		
		// Signal to monitor tpInt bit from internalStrobes register
		logic testPulseIntStrobe; 

  // Configuration test (check that these match the values configured with the slow control)

  // pins_if.tpEnableDigital is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains 1 bit
  logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] tpEnableDigital;
	// pins_if.mask is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] mask;
	// pins_if.tpEnableAnalog is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] tpEnableAnalog;
	// pins_if.tuningDACx are COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed arrays and each of their positions contains 3 bits
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC0;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC1;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC2;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC3;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC4;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC5;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC6;
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][2:0] tuningDAC7;


	// Readout, masking, checking the effect of the moment when the discriminator rises (this signal is used to force a signal at the
  // output of the discriminator and thus emulate the arrival of a hit)
	// pins_if.disc is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
	logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0][`PIXELS_IN_ONE_SUPERPIXEL-1:0] disc;


  logic [`COLUMNS-1:0][`SUPERPIXEL_SEGMENTS-1:0][`SUPERPIXELS_IN_ONE_SEGMENT-1:0] discTp; // output of the multiplexer that selects between digital test pulse
  // and OR of the disc outputs of each front-end in the superpixel (which are already and-end with the corresponding mask signal, and which come as a result
  // of incoming hits or if analog test pulse is applied)
		
		
		
		
		// Auxiliary signal used to probe the CLK_40 signal that reaches the synchronization FF that synchronize readoutStart (the output of the multiplexer
		// that selects between READOUT_pad or the slow control signal to drive the readout), in order to check if there are timing violations in those FFs
		logic digitalperi_roctrl_clk40;
		logic digitalperi_roctrl_readoutStart;
		
		// Auxiliary signal used to probe the CLK_40 and RSTN_IN signals that reache the reset synchronizer, in order to check if there are timing violations in the FF
		// that compose it
		logic digitalperi_rstnctrl_clk40;
		logic digitalperi_rstnctrl_rstnin;


/*	
  // Asynchronous modport used by the DUT
  modport dut_modport(
  	input  CLK_100_n_pad,	// 100 MHz clock (negative polarity) (measurements)
  	input  CLK_100_p_pad, // 100 MHz clock (positive polarity) 
  	input  CLK_40_n_pad,  // 40 MHz clock (negative polarity)(readout)
  	input  CLK_40_p_pad,  // 40 MHz clock (positive polarity) 

		input  PWREN_pad,      // Pad to provide the power pulse
		input  TPULSE_pad,    // Pad to provide the external test pulse 
		input  READOUT_pad,		// Pad to activate readout with an external pulse
		input  SHUTTER_pad,		// Pad to provide the shutter signal 
		input  RSTN_pad,      // Reset signal (active low)
  
    output ENABLE_OUT_pad // Signal that is high while the serial readout is in process
  ); 
  // Modports are only required if the interface is to be synthesized, so as to indicate the direction of the ports;
		// for verification, they're not necessary!
*/

endinterface: CLICTD_pins_interface





`endif // __CLICTD_pins_interface_sv__

