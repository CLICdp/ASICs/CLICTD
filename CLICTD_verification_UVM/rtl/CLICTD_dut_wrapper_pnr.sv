// Filename           : CLICTD_dut_wrapper_pnr.sv
// Author             : Núria Egidos 
// Created on         : 30/5/18
// Last modification  : 11/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Wrapper to connect the pads and the probed internal signals to the UVM framework - for P&R netlist


// packed arrays: https://electronics.stackexchange.com/questions/101821/instantiating-multidimensional-array-in-system-verilog

`ifndef __CLICTD_dut_wrapper_pnr_sv__
`define __CLICTD_dut_wrapper_pnr_sv__

// Include the parameter definition
`ifndef CLK_HALF_PERIOD_40MHz
        `include "../CLICTD_parameters.sv"
`endif



module CLICTD_dut_wrapper (
	CLICTD_slowcontrol_interface slowcontrol_if, 
  CLICTD_readout_interface readout_if, 
  CLICTD_pins_interface pins_if
);




  
  
  
   

  //-------------------- Device under test (DUT)
		
		`include "../CLICTD_instantiation_dut_in_dutwrapperpnr.sv"

  



  //-------------------- Internal signals of the DUT to probe

  

  // Reset test (check that they're low)
  assign pins_if.columnDone  = dut.DigitalPeri.columnDone;  // 16 bits
  assign pins_if.dataIn      = dut.DigitalPeri.dataIn;      // 16 bits
  assign pins_if.commonToken = dut.DigitalPeri.columnToken; // 16 bits


  // Readout control test 
  // Signal used to check if the external readout control is enabled or disabled
  assign pins_if.readoutStartExtEn = dut.DigitalPeri.SCTRL.RO.readoutStartExtEn; 
  // Output of the multiplexer that selects internal/external readout -> check that this value matches the configuration provided with the slow control
  assign pins_if.readoutStart = dut.DigitalPeri.SCTRL.readoutStart;

  // Test pulse test
  // Signal used to check if the external test pulse is enabled or disabled
  assign pins_if.testPulseExtEnable = dut.DigitalPeri.SCTRL.TP.testPulseExtEnable; 
  // Output of the multiplexer that selects internal/external test pulse -> check that this value matches the configuration provided with the slow control
  assign pins_if.testPulse = dut.DigitalPeri.SCTRL.testPulse;

  // Power pulse test 
  // Signal used to enable the external power pulse -> check if it's enabled or disabled
  assign pins_if.powerExtEnable = dut.DigitalPeri.SCTRL.PE.powerExtEnable; 
	// Output of the multiplexer that selects internal/external power pulse -> check that this value matches the configuration provided with the slow control
  assign pins_if.powerEnable = dut.DigitalPeri.SCTRL.powerEnable;
  // Signal to which the pwrInt bit (from the internalStrobes register of the slow control) is translated
  assign pins_if.powerInternal = dut.DigitalPeri.SCTRL.PE.powerInternal; 
  // Signal that propagates the 100 MHz clock into the matrix only when powerEnable is low (active, clock gating)
  assign pins_if.CLK_100_GATED = dut.CLK_100_GATED;


  
  // Include the file where the assignments are performed only if such file exists (this is used to avoid 
  // compilation errors before the file is generated)
  //if ($fopen(`FILE_TO_WRITE_ASSIGNED_INTERNALCONFIGSIGNALS_TO_PINSIF)) begin 
     //$fclose(`FILE_TO_WRITE_ASSIGNED_INTERNALCONFIGSIGNALS_TO_PINSIF);
     
					`include `FILE_TO_WRITE_ASSIGNED_INTERNALCONFIGSIGNALS_TO_PINSIF;
					
  //end

  assign pins_if.digitalperi_roctrl_clk40 = dut.DigitalPeri.ROCTRL.clk40;
		assign pins_if.digitalperi_roctrl_readoutStart = dut.DigitalPeri.ROCTRL.readoutStart;
		assign pins_if.digitalperi_rstnctrl_clk40 = dut.DigitalPeri.RSTNCTRL.clk_in;
		assign pins_if.digitalperi_rstnctrl_rstnin = dut.DigitalPeri.RSTNCTRL.rstn_in;



endmodule: CLICTD_dut_wrapper




`endif // __CLICTD_dut_wrapper_pnr_sv__
