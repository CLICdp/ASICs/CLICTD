// Filename           : CLICTD_dut_wrapper_rtl.sv
// Author             : Núria Egidos 
// Created on         : 30/5/18
// Last modification  : 11/9/18
// Project            : Doctoral student, verification of CLICTD
// Description        : Wrapper to connect the pads and the probed internal signals to the UVM framework - for RTL netlist


// packed arrays: https://electronics.stackexchange.com/questions/101821/instantiating-multidimensional-array-in-system-verilog

`ifndef __CLICTD_dut_wrapper_rtl_sv__
`define __CLICTD_dut_wrapper_rtl_sv__

// Include the parameter definition
`ifndef CLK_HALF_PERIOD_40MHz
        `include "../CLICTD_parameters.sv"
`endif

module CLICTD_dut_wrapper (
	CLICTD_slowcontrol_interface slowcontrol_if, 
  CLICTD_readout_interface readout_if, 
  CLICTD_pins_interface pins_if
);

  //-------------------- Device under test (DUT)

  clictdTop #(.COLUMNS(`COLUMNS),.ROWS(`ROWS)) dut
				(
					// *******************
					// These pins have changed with respect to clictdTop in order to use a unidirectional
					// I2C master
					//
					.SDAin(slowcontrol_if.SDAin),      		// SDA pin
					.SDAout(slowcontrol_if.SDAout),
					.SDAen(slowcontrol_if.SDAen),
					//.SDA_pad(slowcontrol_if.SDA_pad),
					// *******************
					.SCL_pad(slowcontrol_if.SCL_pad),     		// SCL pin
					//.ADDR_pad(slowcontrol_if.ADDR_pad), 	    // i2c address least significant bits
					.ANALOG_IN_pad(),
					.REF_pad(),
					.ANALOG_OUT_pad(),
					.MONITOR_pad(),		// to be revised
					.PWREN_pad(pins_if.PWREN_pad),      	// power pulsing input
					.TPULSE_pad(pins_if.TPULSE_pad),    	// external test pulse input
					.READOUT_pad(pins_if.READOUT_pad),		// external pin for issuing readout
					.SHUTTER_pad(pins_if.SHUTTER_pad),		// shutter signal input
					.RSTN_pad(pins_if.RSTN_pad),    		// reset signal (active low)
					.CLK_100_n_pad(pins_if.CLK_100_n_pad & pins_if.enable_clock_100MHz),		// 100 MHz clock
					.CLK_100_p_pad(pins_if.CLK_100_p_pad & pins_if.enable_clock_100MHz),
					.CLK_40_n_pad(pins_if.CLK_40_n_pad),		// 40 MHz clock
					.CLK_40_p_pad(pins_if.CLK_40_p_pad),
					.DATA_OUT_n_pad(), 	// data output signal
					.DATA_OUT_p_pad(readout_if.DATA_OUT),
					.CLK_OUT_n_pad(), 	// clock output signal
					.CLK_OUT_p_pad(readout_if.CLK_OUT),
					.ENABLE_OUT_n_pad(), 	
					.ENABLE_OUT_p_pad(pins_if.ENABLE_OUT_pad), 	// enable output signal (to indicate when data starts coming out of the chip)
					.VDDA(), 			// analog power supply
					.VDD(),			// digital power supply
					.VSSA(),			// analog ground
					.VSS(),			// digital ground
					.PWELL(),			// p-well bias
					.SUB()			// substrate bias
	);
  

 /*
	clictdTop #(.COLUMNS(`COLUMNS),.ROWS(`ROWS)) dut
	(
		.SDA_pad(slowcontrol_if.SDA_pad),      		// SDA pin
		.SCL_pad(slowcontrol_if.SCL_pad),     		// SCL pin
		.ADDR_pad(slowcontrol_if.ADDR_pad), 	    // i2c address least significant bits
		.ANALOG_IN_pad(),
		.REF_pad(),
		.ANALOG_OUT_pad(),
		.MONITOR_pad(),		// to be revised
		.PWREN_pad(pins_if.PWREN_pad),      	// power pulsing input
		.TPULSE_pad(pins_if.TPULSE_pad),    	// external test pulse input
		.READOUT_pad(pins_if.READOUT_pad),		// external pin for issuing readout
		.SHUTTER_pad(pins_if.SHUTTER_pad),		// shutter signal input
		.RSTN_pad(pins_if.RSTN_pad),    		// reset signal (active low)
		.CLK_100_n_pad(pins_if.CLK_100_n_pad),		// 100 MHz clock
		.CLK_100_p_pad(pins_if.CLK_100_p_pad),
		.CLK_40_n_pad(pins_if.CLK_40_n_pad),		// 40 MHz clock
		.CLK_40_p_pad(pins_if.CLK_40_p_pad),
		.DATA_OUT_n_pad(), 	// data output signal
		.DATA_OUT_p_pad(readout_if.DATA_OUT),
		.CLK_OUT_n_pad(), 	// clock output signal
		.CLK_OUT_p_pad(readout_if.CLK_OUT),
		.ENABLE_OUT_pad(pins_if.ENABLE_OUT_pad), 	// enable output signal (to indicate when data starts coming out of the chip)
		.VDDA(), 			// analog power supply
		.VDD(),			// digital power supply
		.VSSA(),			// analog ground
		.VSS(),			// digital ground
		.PWELL(),			// p-well bias
		.SUB()			// substrate bias
	);
  */

  //-------------------- Internal signals of the DUT to probe

  // Reset test (check that they're low)
  assign pins_if.columnDone  = dut.DigitalPeri.columnDone;  // 16 bits
  assign pins_if.dataIn      = dut.DigitalPeri.dataIn;      // 16 bits
  assign pins_if.commonToken = dut.DigitalPeri.columnToken; // 16 bits


  // Readout control test 
  // Signal used to check if the external readout control is enabled or disabled
  assign pins_if.readoutStartExtEn = dut.DigitalPeri.SCTRL.readoutStartExtEn;
  // Output of the multiplexer that selects internal/external readout -> check that this value matches the configuration provided with the slow control
  assign pins_if.readoutStart = dut.DigitalPeri.SCTRL.readoutStart;

  // Test pulse test
  // Signal used to check if the external test pulse is enabled or disabled
  assign pins_if.testPulseExtEnable = dut.DigitalPeri.SCTRL.testPulseExtEnable;
  // Output of the multiplexer that selects internal/external test pulse -> check that this value matches the configuration provided with the slow control
  assign pins_if.testPulse = dut.DigitalPeri.SCTRL.testPulse;

  // Power pulse test 
  // Signal used to enable the external power pulse -> check if it's enabled or disabled
  assign pins_if.powerExtEnable = dut.DigitalPeri.SCTRL.powerExtEnable;
	// Output of the multiplexer that selects internal/external power pulse -> check that this value matches the configuration provided with the slow control
  assign pins_if.powerEnable = dut.DigitalPeri.SCTRL.powerEnable;
  // Signal to which the pwrInt bit (from the internalStrobes register of the slow control) is translated
  assign pins_if.powerInternal = dut.DigitalPeri.SCTRL.powerInternal;
  // Signal that propagates the 100 MHz clock into the matrix only when powerEnable is low (active, clock gating)
  assign pins_if.CLK_100_GATED = dut.CLK_100_GATED;
		
		
		// Signal to monitor tpInt bit from internalStrobes register
		assign pins_if.testPulseIntStrobe = dut.DigitalPeri.SCTRL.testPulseIntStrobe;
		

  genvar column, segment, superpixel_in_segment, front_end_bits;
  generate
  for (column = 0; column < `COLUMNS; column = column + 1) begin
							for(segment = 0; segment < `SUPERPIXEL_SEGMENTS; segment=segment + 1) begin
											for(superpixel_in_segment = 0; superpixel_in_segment < `SUPERPIXELS_IN_ONE_SEGMENT; superpixel_in_segment = superpixel_in_segment + 1) begin

													// Configuration test (check that these match the values configured with the slow control)

              						// pins_if.tpEnableDigital is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains 1 bit
  												assign pins_if.tpEnableDigital[column][segment][superpixel_in_segment] = 
 									   						dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tpEnableDigital; // 1 bit
													// pins_if.mask is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
													assign pins_if.mask[column][segment][superpixel_in_segment] = 
										 						dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.mask; // `PIXELS_IN_ONE_SUPERPIXEL bits
													// pins_if.tpEnableAnalog is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
													assign pins_if.tpEnableAnalog[column][segment][superpixel_in_segment] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tpEnableAnalog; // 8 bits
													// pins_if.tuningDACx are COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed arrays and each of their positions contains 3 bits
													assign pins_if.tuningDAC0[column][segment][superpixel_in_segment] =  dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tuningDAC_0; // 3 bits
													assign pins_if.tuningDAC1[column][segment][superpixel_in_segment] =  dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tuningDAC_1; // 3 bits
													assign pins_if.tuningDAC2[column][segment][superpixel_in_segment] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tuningDAC_2; // 3 bits
													assign pins_if.tuningDAC3[column][segment][superpixel_in_segment] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tuningDAC_3; // 3 bits
													assign pins_if.tuningDAC4[column][segment][superpixel_in_segment] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tuningDAC_4; // 3 bits
													assign pins_if.tuningDAC5[column][segment][superpixel_in_segment] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tuningDAC_5; // 3 bits
													assign pins_if.tuningDAC6[column][segment][superpixel_in_segment] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tuningDAC_6; // 3 bits
													assign pins_if.tuningDAC7[column][segment][superpixel_in_segment] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.tuningDAC_7; // 3 bits


													// Readout, masking, checking the effect of the moment when the discriminator rises (this signal is used to force a signal at the
       						// output of the discriminator and thus emulate the arrival of a hit)
													// pins_if.disc is a COLUMNS-by-SUPERPIXEL_SEGMENTS-by-SUPERPIXELS_IN_ONE_SEGMENT packed array and each of its positions contains `PIXELS_IN_ONE_SUPERPIXEL bits
													assign pins_if.disc[column][segment][superpixel_in_segment] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.disc; // `PIXELS_IN_ONE_SUPERPIXEL bits

													for (front_end_bits = 0; front_end_bits < 8; front_end_bits++) begin 
													     //assign pins_if.disc[column][segment][superpixel_in_segment][front_end_bits] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.fe_row[superpixel_in_segment].fe_cluster[front_end_bits].ANALOG_FE.IN;
																		//assign pins_if.disc[column][segment][superpixel_in_segment][front_end_bits] = dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.fe_row[superpixel_in_segment].fe_cluster[front_end_bits].ANALOG_FE.OUTDisc; 
																		assign dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.fe_row[superpixel_in_segment].fe_cluster[front_end_bits].ANALOG_FE.IN = pins_if.disc[column][segment][superpixel_in_segment][front_end_bits];
													end

       						assign pins_if.discTp[column][segment][superpixel_in_segment] =  dut.MATRIX.column_gen[column].column_gen.column.Spix_gen[segment].Spix_gen.Spix.pixel_gen[superpixel_in_segment].pixel_gen.pixel.discTp; // 1 bit




											end // for pixel in the superpixel
							end // for superpixels
		end // for columns
  endgenerate
		

  assign pins_if.digitalperi_roctrl_clk40 = dut.DigitalPeri.ROCTRL.clk40;
		assign pins_if.digitalperi_roctrl_readoutStart = dut.DigitalPeri.ROCTRL.readoutStart;
		assign pins_if.digitalperi_rstnctrl_clk40 = dut.DigitalPeri.RSTNCTRL.clk_in;
		
		
		

endmodule: CLICTD_dut_wrapper




`endif // __CLICTD_dut_wrapper_rtl_sv__
