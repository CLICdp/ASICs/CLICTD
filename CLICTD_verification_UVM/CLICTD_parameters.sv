// Filename           : CLICTD_parameters.sv
// Author             : N�ria Egidos 
// Created on         : 8/6/18 - on 31/8/18 updated slow control registers definition 
// Last modification  : 10/1/19 
// Project            : Doctoral student, verification of CLICTD
// Description        : List of all the `defines used in the test framework
// Derived from       : pixel_defines.sv by Elia Conti and Sara Marconi, developed for the VEPIX53 verification


`ifndef __CLICTD_parameters_sv__
`define __CLICTD_parameters_sv__

 // set to the value in ns we want to delay the application of the reset or of the readout command to avoid timing violations
	// at the reset synchronizer or the readoutStart sychronizer, respectively. Set to 0 to not apply any additional delay.
	// (CLICTD_apply_reset_pulse_sequence.sv, CLICTD_matrixconfig_virtual_sequence.sv)
  `define DELAY_NS_USED_TO_AVOID_VIOLATIONS_DUE_TO_ASYNC_SIGNALS 10


  `define YES  1'b1
  `define NO   1'b0
  `define TOP_LEVEL_NETLIST "P&R"
   // other alternatives for TOP_LEVEL_NETLIST: "RTL"

  // File on which the assigments from internal configuration signals into pins_if are stored, to be used in CLICTD_dur_wrapper_pnr
  `define FILE_TO_WRITE_ASSIGNED_INTERNALCONFIGSIGNALS_TO_PINSIF "../rtl/generated_assign_internalconfigsignals_to_pinsif.v"
  // File with the istantiation of columns (RTL or post layout netlist, depending on the positions of column_is_rtl, which are randomised) to be included in clictdTop_pnr_workaroundi2c
  `define FILE_TO_WRITE_COLUMN_GEN_CLICTDTOP_WORKAROUNDI2C_PNR "../rtl/generated_columngen_clictdtop_i2cworkaround_somecolpnr.v"
  // File where the different modules in the postlayout netlist are "connected" to their corresponding .sdf file to perform back-annotated simulation
  `define PATH_SDFASSIGNMENTS_FILE "../sim_annotated/sdfAssignments"
  // File where the sdf_annotate instructions are stored to then be included in the top level netlist to perform back annotated simulation (either sdfAssignments or this file are used, not both at the same time)
  `define PATH_SDFANNOTATE_FILE "../sim_annotated/generated_sdf_annotate"
		// File in which a modified version of the clictd_output.sdf is stored, which does not contain special characters
		// that were not supported by the back annotation in Questasim
		`define PATH_REPLACED_CLICTDOUTPUT_SDF "../sim_annotated/clictd_output_replaced.sdf"
		// File to which the column_output.sdf is copied. Since it's placed in a local folder, vsim can have access to it to read it and generate the _min.csd, _typ.csd and _max.csd (binary files, compiled sdf) required to perform the back annotated simulation
		`define PATH_LOCAL_COLUMNOUTPUT_SDF "../sim_annotated/column_output_localcopy.sdf"
		
		// File in which the instantiation of some columns from the original top level postlayout netlist is modified to replace them with their corresponding RTL netlist,
		// as well as adding the I2C workaround
		`define FILE_TOPLEVEL_POSTLAYOUT_NETLIST_WITH_SOMECOL_PNR_I2CWORKAROUND "../rtl/clictdTop_pnr_workaroundi2c_somecolpnr.v"
		
		// File in which the original gate level netlist is modified to include the I2C workaround
		`define FILE_TOPLEVEL_GATELEVEL_NETLIST_I2CWORKAROUND "../rtl/clictdTop_workaroundi2c.v"
		
		// Original gate level netlist
		`define PATH_ORIGINAL_CLICTDNETLIST_GATELEVEL "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/clictdTop/syn/verilog/clictdTop.v"
		
`include "../CLICTD_paths_including_dut_folder.sv"

  //---------------- SIMULATION KNOBS ------------------------//
  `define ENABLE_DEACTIVATING_CLOCKS_TO_SPEED_UP_SIMULATION 1
	//`define ENABLE_WORKAROUND_CHIPSTATUS_UPDATED_FROM_DATAOUTMONITOR 1
  //`define ENABLE_REPORTING_SECOND_READOUT_ALL_ZEROS 1
  //`define ENABLE_REPORTING_MATRIX_CONFIG_HALF 1
  //----------------------------------------------------------//
		
		
		
		
		
		// Parameters used in CLICTD_reset_synchronizer_violations_sequence and CLICTD_readoutstart_synchronizer_violations_sequence
		// (the max values of hold and setup windows have been obtained from clictd_output.sdf and column_output.sdf, with some
		// safety margin). The increment step is the timescale resolution
		//`define MAX_HOLD_DURATION_NS 0.6
		`define MAX_HOLD_DURATION_NS 1
		//`define MAX_SETUP_DURATION_NS 0.1
		//`define MAX_SETUP_DURATION_NS 0.6
		`define MAX_SETUP_DURATION_NS 1
		`define INCREMENT_STEP_HOLD_DURATION 0.001
		`define INCREMENT_STEP_SETUP_DURATION 0.001
		`define EXPECTED_NUMBER_OF_RESET_PULSES `MAX_SETUP_DURATION_NS/`INCREMENT_STEP_SETUP_DURATION + `MAX_HOLD_DURATION_NS/`INCREMENT_STEP_HOLD_DURATION
  `define EXPECTED_NUMBER_OF_READOUT_PULSES `MAX_SETUP_DURATION_NS/`INCREMENT_STEP_SETUP_DURATION + `MAX_HOLD_DURATION_NS/`INCREMENT_STEP_HOLD_DURATION

  // Parameters to define the ToT and ToA for the READOUT_TEST
   // tot is expressed for totDiv = 0 as the number of "20 ns period" (50 MHz clock) counts, i.e. if tot = 16,  we will count a delay of 16*20 ns
   // if a different value for totDiv is used, the recorded value of ToT will not correspond to the provided tot 
   `define DELAY_FACTOR_TOT  20 // ns
   // toa is expressed as the number of "10 ns period" (100 MHz clock) counts, i.e. if toa = 48, we will count a delay of 48*10 ns
   `define DELAY_FACTOR_TOA 10 // ns


  // Number of flip-flops per pixel (for the matrix configuration)
  `define NUMBER_OF_FF_PER_SUPERPIXEL 22

  // Identifiers of configuration halves
  `define FIRST_CONFIGURATION_HALF 2'b01
  `define SECOND_CONFIGURATION_HALF 2'b10
 
  // Available configuration bits for the first configuration half
  `define MATRIXCONFIG_tpEnableDigital        0
  `define MATRIXCONFIG_mask0                  1 
		`define MATRIXCONFIG_mask1                  2 
		`define MATRIXCONFIG_mask2                  3
		`define MATRIXCONFIG_mask3                  4 
		`define MATRIXCONFIG_mask4                  5 
		`define MATRIXCONFIG_mask5                  6
		`define MATRIXCONFIG_mask6                  7
		`define MATRIXCONFIG_mask7                  8
		`define MATRIXCONFIG_tpEnableAnalog0        9
		`define MATRIXCONFIG_tpEnableAnalog1        10
		`define MATRIXCONFIG_tpEnableAnalog2        11
		`define MATRIXCONFIG_tpEnableAnalog3        12
		`define MATRIXCONFIG_tpEnableAnalog4        13
		`define MATRIXCONFIG_tpEnableAnalog5        14
		`define MATRIXCONFIG_tpEnableAnalog6        15
		`define MATRIXCONFIG_tpEnableAnalog7        16
		`define MATRIXCONFIG_tuningDAC00            17
		`define MATRIXCONFIG_tuningDAC01            18
		`define MATRIXCONFIG_tuningDAC02            19
		`define MATRIXCONFIG_tuningDAC10            20 
		`define MATRIXCONFIG_dummy_bit_21_firsthalf 21


  // Available configuration bits for the second configuration half
  `define MATRIXCONFIG_dummy_bit_0_secondhalf  0
  `define MATRIXCONFIG_tuningDAC11             1
		`define MATRIXCONFIG_tuningDAC12             2
		`define MATRIXCONFIG_tuningDAC20             3
		`define MATRIXCONFIG_tuningDAC21             4 
		`define MATRIXCONFIG_tuningDAC22             5
		`define MATRIXCONFIG_tuningDAC30             6
		`define MATRIXCONFIG_tuningDAC31             7
		`define MATRIXCONFIG_tuningDAC32             8
		`define MATRIXCONFIG_tuningDAC40             9
		`define MATRIXCONFIG_tuningDAC41             10
		`define MATRIXCONFIG_tuningDAC42             11
		`define MATRIXCONFIG_tuningDAC50             12
		`define MATRIXCONFIG_tuningDAC51             13
		`define MATRIXCONFIG_tuningDAC52             14
		`define MATRIXCONFIG_tuningDAC60             15
		`define MATRIXCONFIG_tuningDAC61             16 
		`define MATRIXCONFIG_tuningDAC62             17
		`define MATRIXCONFIG_tuningDAC70             18
		`define MATRIXCONFIG_tuningDAC71             19
		`define MATRIXCONFIG_tuningDAC72             20
		`define MATRIXCONFIG_dummy_bit_21_secondhalf 21

  // Readout control
  `define CTRLREADOUT_SLOWCONTROL 0
  `define CTRLREADOUT_EXTERNAL 1

  // Maximum time (number of timeunits) to wait until the 
  // "start readout" condition (rising edge of ENABLE_OUT_pad
  // and CLK_OUT) is matched
  `define TIMEOUT_RISETIME_CLKOUTENABLEOUT 0
  // Maximum time (number of timeunits) to wait until the 
  // "start readout" condition (rising edge of ENABLE_OUT_pad occurs
  `define TIMEOUT_START_READOUT_CONDITION 10000000
  // Maximum time (number of timeunits) to wait until the 
  // powerEnable signal is updated when PWREN_pad or pwrInt change
  // (it has to be long enough to allow the required time for the 
  // slow control command to arrive when pwrInt is changed)
  `define TIMEOUT_UPDATE_POWERENABLE 100000
		// Maximum time (number of timeunits) to wait to determine if bit discTp changes
		// when a test pulse is applied at TPULSE_pad and external pulsing was enabled,
		// or when a test pulse is applied from the slow control and internal pulsing
		// was enabled
		`define TIMEOUT_UPDATE_DISCTP_WHEN_TESTPULSE_IS_APPLIED 100000
  

  // Maximum time (number of timeunits) allowed to wait in 
  // STATUS_DETECT_STOP_READOUT until the falling edge of
  // ENABLE_OUT_pad is detected
  `define TIMEOUT_FALLTIME_ENABLEOUT 30

  // Number of bits to readout
  `define NUMBER_BITS_START_HEADER 22
  `define NUMBER_BITS_COLUMN_HEADER 22
  `define NUMBER_BITS_STOP_HEADER 22
  //`define NUMBER_BITS_READOUT_PER_COLUMN_COMPRESSION_DISABLED 22
  `define NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED 22
  `define NUMBER_BITS_READOUT_COMPRESSION_DISABLED `NUMBER_BITS_START_HEADER + `COLUMNS*(`NUMBER_BITS_COLUMN_HEADER + `NUMBER_BITS_READOUT_PER_SUPERPIXEL_COMPRESSION_DISABLED*`ROWS) + `NUMBER_BITS_STOP_HEADER

  // Compression enabled/disabled
  `define COMPRESSION_ENABLED  1'b1
  `define COMPRESSION_DISABLED 1'b0
  
  // Value of the readout headers
		
		// Values before 31/10/18
		/*
  `define HEADER_START     22'b1100000101000000000000
  `define HEADER_STOP      22'b1110000101000000000000
  `define HEADER_COLUMN_0  22'b1011000101000000000000
  `define HEADER_COLUMN_1  22'b1011000101000100000000
  `define HEADER_COLUMN_2  22'b1011000101001000000000
  `define HEADER_COLUMN_3  22'b1011000101001100000000
  `define HEADER_COLUMN_4  22'b1011000101010000000000
  `define HEADER_COLUMN_5  22'b1011000101010100000000
  `define HEADER_COLUMN_6  22'b1011000101011000000000
  `define HEADER_COLUMN_7  22'b1011000101011100000000
  `define HEADER_COLUMN_8  22'b1011000101100000000000
  `define HEADER_COLUMN_9  22'b1011000101100100000000
  `define HEADER_COLUMN_10 22'b1011000101101000000000
  `define HEADER_COLUMN_11 22'b1011000101101100000000
  `define HEADER_COLUMN_12 22'b1011000101110000000000
  `define HEADER_COLUMN_13 22'b1011000101110100000000
  `define HEADER_COLUMN_14 22'b1011000101111000000000
  `define HEADER_COLUMN_15 22'b1011000101111100000000
		*/
		
		// Values from 31/10/18 (changed so that the header values couldn't be confused with actual measurement values)
		`define HEADER_START     22'b1111111111111110101000
  `define HEADER_STOP      22'b1111111111111110010100
  `define HEADER_COLUMN_0  22'b1111111111111101000000
  `define HEADER_COLUMN_1  22'b1111111111111101000100
  `define HEADER_COLUMN_2  22'b1111111111111101001000
  `define HEADER_COLUMN_3  22'b1111111111111101001100
  `define HEADER_COLUMN_4  22'b1111111111111101010000
  `define HEADER_COLUMN_5  22'b1111111111111101010100
  `define HEADER_COLUMN_6  22'b1111111111111101011000
  `define HEADER_COLUMN_7  22'b1111111111111101011100
  `define HEADER_COLUMN_8  22'b1111111111111101100000
  `define HEADER_COLUMN_9  22'b1111111111111101100100
  `define HEADER_COLUMN_10 22'b1111111111111101101000
  `define HEADER_COLUMN_11 22'b1111111111111101101100
  `define HEADER_COLUMN_12 22'b1111111111111101110000
  `define HEADER_COLUMN_13 22'b1111111111111101110100
  `define HEADER_COLUMN_14 22'b1111111111111101111000
  `define HEADER_COLUMN_15 22'b1111111111111101111100
   
  

  // Power pulse control
  `define CTRLPOWERPULSE_SLOWCONTROL 0
  `define CTRLPOWERPULSE_EXTERNAL 1
  `define DURATION_POWER_PULSE_TIMEUNITS 1000
		
		
		
		`include "./CLICTD_registers_info.sv"


  
	

 


  // Number of bits to read out via the slow control
  // interface (size of a slow control register)
  `define SIZEWORD_SLOWCONTROL  8 

  // Read or write operation of the I2C, which is translated into reading or
  // writing a slow control register respectively
  `define READ "READ"
  `define WRITE "WRITE"

  // Number of time units to wait between two consecutive write operations
  // of the slow control
  //`define NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL 0
  `define NUMBER_TIMEUNITS_WAIT_BETWEEN_WRITE_SLOWCONTROL 10000
 

  // Number of time units to wait between two consecutive read operations
  // of the slow control
  `define NUMBER_TIMEUNITS_WAIT_BETWEEN_READ_SLOWCONTROL 10000

  // Time resolution for the simulation
  `define TIMEUNIT  1ns 
  `define TIMEPRECISION  100ps 

  


  // Number of time units to wait after some stimuli has been
  // applied (falling edge in RSTN_pad...) before probing some signals
  // with the monitors. It is expressed as a number of TIMEUNIT.
  `define WAIT_A_LITTLE_BIT_NUMTIMEUNITS  100 

  // List of `defines related to applying a reset to the chip
  // Randomisation seed to randomize the time when the falling edge of RSTN_pad arrives
  // it is expressed in number of TIMEUNIT. If randomization is enabled, it is constrained so
  // that the maximum possible time of arrival is RESET_TIMEARRIVAL_NUMTIMEUNITS. If randomisation
  // is disabled, the time of arrival is fixed to RESET_TIMEARRIVAL_NUMTIMEUNITS. 
  `define RANDOMIZE_TIMEARRIVAL_RESETPULSE  `YES 
  `define RANDOMIZATIONSEED_TIMEARRIVAL_RESETPULSE_NUMTIMEUNITS  10 
  `define RESET_TIMEARRIVAL_NUMTIMEUNITS  100 
  // Randomisation seed to randomize the time when the falling edge of RSTN_pad arrives
  // it is expressed in number of TIMEUNIT. If randomization is enabled, it is constrained so
  // that the maximum possible duration is RESET_DURATION_NUMTIMEUNITS. If randomisation
  // is disabled, the duration is fixed to RESET_DURATION_NUMTIMEUNITS.
  `define RANDOMIZE_DURATION_RESETPULSE  `YES
  `define RANDOMIZATIONSEED_DURATION_RESETPULSE_NUMTIMEUNITS  100 
  `define RESET_DURATION_NUMTIMEUNITS  500 
  // Value to force to the state of some internal state machines, so that they start up at 
  // a known value instead of undefined.
  `define VALUE_FOR_STATE_MACHINE  0 

	// Definitions (distribution of pixels, columns, rows)
  `define COLUMNS  16   // Number of colums in the chip
  `define ROWS     128  // Number of rows in the chip
  `define SEG      8    // Number of front-end segments in the pixel
  `define SPIX     8    // Number of pixels in the superpixel
  `define SUPERPIXEL_SEGMENTS  16 
  `define SUPERPIXELS_IN_ONE_SEGMENT 8
  `define PIXELS_IN_ONE_SUPERPIXEL  8 





  // Definitions for the environment
  // Possible agents to instantiate
  `define SLOWCONTROL_AGENT     "slowcontrol_agent"
  `define REGSLOWCONTROL_AGENT  "regslowcontrol_agent"
  `define DATAOUT_AGENT         "dataout_agent" 
  `define PINS_AGENT            "pins_agent" 

  // Possible scoreboards to instantiate
  `define REGSLOWCONTROL_SCOREBOARD  "regslowcontrol_scoreboard" 
  `define DATAOUT_SCOREBOARD         "dataout_scoreboad" 
  `define PINS_SCOREBOARD            "pins_scoreboard" 


  

  // List of tests to perform
  `define RESET_TEST            "CLICTD_reset_test" 
  `define CONFIGURATION_TEST    "CLICTD_configuration_test" 
  `define READOUT_TEST          "CLICTD_readout_test" 
  `define READOUTCONTROL_TEST   "CLICTD_readoutcontrol_test" 
  `define WRREGISTERS_TEST      "CLICTD_wrregisters_test" 
  `define TESTPULSECONTROL_TEST "CLICTD_testpulsecontrol_test" 
  `define POWERCONTROL_TEST     "CLICTD_powercontrol_test" 
  `define DISCRIMINATOR_TEST    "CLICTD_discriminator_test"  
		`define RESET_SYNCHRONIZER_VIOLATIONS_TEST "CLICTD_reset_synchronizer_violations_test"
  `define READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST "CLICTD_readoutstart_synchronizer_violations_test"
		`define READOUT_TEST_FOR_COVERAGE "CLICTD_readout_test_for_coverage"
    

    // List of errors to check:

    // Virtual interface (pins interface) not included in the database
    `define NOVIF_PINS "NOVIF_PINS"
    // Virtual interface (pins interface) not included in the database
    `define NOVIF_READOUT "NOVIF_READOUT"
    // Virtual interface (slowcontrol interface) not included in the database
    `define NOVIF_SLOWCONTROL "NOVIF_SLOWCONTROL"
    // Testname not stored in the database
    `define NOTESTNAME "NOTESTNAME"
    // Agent configuration not stored in the database
    `define NOCONFIG_AGENT "NOCONFIG_AGENT"
    // Environment configuration not stored in the database
    `define NOCONFIG_ENV "NOCONFIG_ENV"
    // In the configuration of the environment, no agents are specified to be created
    `define NOAGENTS "NOAGENTS"
    // In the configuration of the environment, it is missing the specification
    // to create a certain agent
    `define NOAGENT "NOAGENT"
    // In the configuration of the environment, no scoreboards are specified to be created
    `define NOSCOREBOARDS "NOSCOREBOARDS"
    // In the configuration of the environment, it is missing the specification
    // to create a certain scoreboard
    `define NOSCOREBOARD "NOSCOREBOARD"
    // In the regslowcontrol_model, the default values of the slow control registers 
    // cannot be retrieved from the database
    `define NODEFAULTVALUEREG "NODEFAULTVALUEREG"
    // No chip_status in the database (it should have been stored by the test in the build_phase)
    `define NOCHIPSTATUS "NOCHIPSTATUS"
				// No FieldsConfigureMatrix in the database (it should have been stored by the test in the build_phase)
				`define NOMATRIXCONFIG "NOMATRIXCONFIG"
				// No FieldsTimeMeasurements in the database (it should have been stored by the test in the build_phase)
				`define NOTIMEMEASUREMENTS "NOTIMEMEASUREMENTS"
				// No BitsMonitorSlowcontrolReg in the database (it should have been stored by the test in the build_phase)
				`define NOBITSSLOWCONTROLREG "NOBITSSLOWCONTROLREG"
    // No CLICTDevents object in the database (it should have been stored by the test in the build_phase)
    `define NOCLICTDEVENTS "NOCLICTDEVENTS" 
    // No ReportingEnable object in the database (it should have been stored by the test in the build_phase)
    `define NOREPORTINGENABLE "NOREPORTINGENABLE"
    // The array containing the expected value for DATA_OUT when the second readout is performed, 
    // before performing any configuration or acquisition, is not found in the database. This array
    // should have been stored in the database by CLICTD_test
    `define NODATAOUT_SECONDREADOUT "NODATAOUT_SECONDREADOUT"
    // Protocol error: some signal related to data readout protocol (DATA_OUT_pad, CLK_OUT_pad, ENABLE_OUT_pad)
    // does not have the expected value
    `define ERROR_READOUT_PROTOCOL "ERROR_READOUT_PROTOCOL"
    // Error found in some model (regslowcontrol_model, pins_model, dataout_model
    `define ERROR_MODEL "ERROR_MODEL"
				// Error found when trying to convert the associative array column_content into the different fields of 
				// Acquired_word_struct: the hit_flag of a certain position doesn't match the value it should have, i.e.
				// either the hit_flag has the wrong value or there are some missing/excess bits in column_content and 
				// the breakdown can't be performed correctly (the hit_flag is shifted to a wrong position)
     `define ERROR_BREAKDOWN_COLUMNCONTENT "ERROR_BREAKDOWN_COLUMNCONTENT"
					// Error pops up because some field in some transaction is Z or X
					`define ERROR_UNKNOWN_VALUE "ERROR_UNKNOWN_VALUE"



    // In CLICTD_pins_monitor: NOVIF_PINS, NOTESTNAME
    // In CLICTD_dataout_monitor: NOVIF_PINS, NOVIF_READOUT, NOTESTNAME
    // In CLICTD_regslowcontrol_monitor: NOVIF_SLOWCONTROL, NOTESTNAME
    // In CLICTD_slowcontrol_driver: NOVIF_PINS, NOVIF_SLOWCONTROL
    // In CLICTD_agent: NOCONFIG_AGENT
    // In CLICTD_pins_model: NOTESTNAME
    // In CLICTD_regslowcontrol_model: NOTESTNAME, NODEFAULTVALUEREG
    // In CLICTD_pins_comparator: NOTESTNAME
    // In CLICTD_environment: NOCONFIG_ENV, NOAGENTS, NOSCOREBOARDS, NOAGENT, NOSCOREBOARD
    // In CLICTD_reset_tet: NOVIF_PINS (interface task used to apply reset pulse)


    // Create a typedef for the associative array that will store the
    // default values of the registers of the slow control, in order to
    // be able to store it into the configuration database
    // https://verificationacademy.com/forums/uvm/setting-associate-array-factory-using-uvmconfigdb
    //typedef int default_values_registers_slowcontrol_type [int];
				
				
				// This superpixel will have digital test pulsing enabled and expect a digital test pulse
				`define SUPERPIXEL_COORDINATE_EXPECTS_DIGITAL_TEST_PULSE 0
				// This superpixel will have digital test pulsing disabled and expect some hit to be forced at two of its discriminator bits
				`define SUPERPIXEL_COORDINATE_EXPECTS_FORCE_PUSE_AT_SOME_DISC_BIT 1
				
				
				
				// Minimum distance between test pulses or minimum duration of test pulses in ns depending on whether 
				// very short test pulses (shouldn't be longer that 1 tot count) or regular duration test pulses are used
				`ifdef USE_VERY_SHORT_TEST_PULSES
										`define MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION 5 
				`else
										`define MINIMUM_TIME_INCREMENT_FOR_TESTPULSE_GENERATION 100
			`endif
																								
																								



`endif // __CLICTD_parameters_sv__
