# Filename:          launcher_one_test.tcl
# Author:            N�ria Egidos 
# Created on:        10/1/2019
# Last modified on:  10/1/2019
# Project:           Doctoral student, verification of CLICTD
# Description:       This script is called from launcher_all_tests.tcl to perform the compilation and simulation of one test

# Note: this script assumes that the path to the current version of Questasim has already been added to the PATH environment 
# variable. You can check if this is the case by typing echo $PATH in the terminal.
# If this is not the case, you won't be able to run vsim (vsim & will not work). To add it, you can editing ~/.cshrc, just 
# add the following line after #<DECM_END>:
# setenv PATH "/eda/mentor/2017-18/RHELx86/QUESTA-CORE-PRIME_10.6c-1/questasim/bin:$PATH"
# Then close the terminal and restart gp



#
############################################################################
############# Import the possible actions to perform #######################
############################################################################
#

source {../sim_annotated/actions.do}



#
############################################################################
######## Read simulation parameters from the plusargs ######################
############################################################################
#
# http://www.project-veripage.com/plusarg.php
# https://www.valpont.com/use-valueplusargs-and-namevalue-definenamevalue-and-gnamevalue-gnamevalue-to-change-verilog-rtl-and-testbench-behavior-in-simulation-compile-or-run-time/pst/
# plusargs is runtime for the testbench, we can't use it as an argument for vlog/vsim!

#$value$plusarg("TIMESCALE=%s", TIMESCALE);
#$value$plusarg("INCLUDE_DIRS=%s", INCLUDE_DIRS);
#$value$plusarg("FILES_TO_COMPILE=%s", FILES_TO_COMPILE);
#$value$plusarg("DESIGN=%s", DESIGN);
#$value$plusarg("TOP_MODULE=%s", TOP_MODULE);
#$value$plusarg("UVM_TESTNAME=%s", UVM_TESTNAME);
#$value$plusarg("QUESTA_RUN_OPTIONS=%s", QUESTA_RUN_OPTIONS);
#$value$plusarg("WAVE_FILE=%s", WAVE_FILE);

# https://www.tcl.tk/man/tcl8.6/TclCmd/source.htm
set PATH_TO_SOURCE [pwd]
set PATH_TO_SOURCE [string trim $PATH_TO_SOURCE][string trim "/parameters_for_launcher_one_test.tcl"]
source $PATH_TO_SOURCE

#echo "WAVE FILE $WAVE_FILE"

#
############################################################################
# Set the working directory, clear it if it exists                         #
############################################################################
#

if {[file exists $QUESTA_DESIGN_LIB]} { 
   #vdel -lib work -all 
   actions::clean $QUESTA_DESIGN_LIB
} 
actions::setup $QUESTA_DESIGN_LIB $QUESTA_DESIGN_LIB_DEFAULT


#
############################################################################
# Compile the files and run the simulation for the specified test          #
############################################################################
#

# Compile the test
actions::compile $TIMESCALE $INCLUDE_DIRS $FILES_TO_COMPILE 
# Simulate the test
actions::sim $DESIGN $TOP_MODULE $UVM_TESTNAME $QUESTA_RUN_OPTIONS $WAVE_FILE "" 
# Print simulation statistics (CPU and memory usage - Kbytes- ) -> this information will be necessary when vmanager is used, so as to dimension the required number of jobs and machine memory
simstats


#vsim $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME -voptargs="+acc" -novopt {*}$QUESTA_RUN_OPTIONS
## allow some time for the UVM testbench to be built, 
## so as to be able to create the necessary handles to plot them
#run 1 ns
#vsim $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME -voptargs="+acc" -novopt {*}$QUESTA_RUN_OPTIONS < $WAVE_FILE
#run -all ; # runs indefinitely
		
		
		
		


#
############################################################################
# Close the simulator                                                      #
############################################################################
#
quit -f
