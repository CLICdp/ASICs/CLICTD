# Filename:          actions.do
# Author:            Núria Egidos 
# Created on:        7/03/2018
# Last modified on:  9/03/2018
# Project:           Doctoral student, training for the verification of CLICTD
# Description:       This do macro contains some TCL processes used to run a simulation with Questasim (vsim);
#                    these processes are called from launcher.do 
# Derived from:      Adrian Fiergolski scripts in https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim
#										 http://www.cems.uwe.ac.uk/~a2-lenz/n-gunton/worksheets/modelsim_tcl.pdf 		
#										 https://users.ece.cmu.edu/~kbiswas/se_cmds.pdf
#										 http://www-g.eng.cam.ac.uk/mentor/mti/docs/ee_cmds.pdf
# Redirect stdout: https://askubuntu.com/questions/420981/how-do-i-save-terminal-output-to-a-file


# Note: more reusable code with namespace package: https://www.tcl.tk/man/tcl8.5/tutorial/Tcl31.html

# Note: all commands executed are automatically saved in the transcript file, as well as the errors that may occur

############################################################################
# Explanation of the parameters used by the processes in this file and     #
# examples of their value                                                  #
############################################################################
#
# ACTION_TO_PERFORM:         possible actions to perform when launcher.do is called
#                            possible actions: "setup" / "compile" / "sim" / "multisim" / "all" / "restart" / "clean"
#
# BACK_ANNOTATED:            "YES" to enable back annotation, "NO" to disable it ("NO" is used by default)
#
# COLUMN_IS_RTL:             vector of 16 positions (MSB, left, is column 15; LSB, right, is column 0); positions of value 1 mean that the corresponding column is replaced by column_RTL in the netlist,
#                            positions with 0 mean that this column remains with the postlayout netlist\
#                            example: "16'b1111111111111110"
#
# DESIGN:                    "rtl" or "netlist"
#
# FILES_TO_COMPILE:          list of ALL files to be compiled to be able to run a 
#                            simulation; files are listed in one single line 
#                            example: "../../../rtl/simpleadder_if_v3.sv ../../../rtl/simpleadder.sv ../../../rtl/simpleadder_dutwrapper_v3.sv ../uvm_files/tests/simpleadder_test_pkg.sv ../uvm_files/simpleadder_tb_top_v5.sv" 
#
# FILES_TO_RECOMPILE:        the same format as FILES_TO_COMPILE, but it can contain
#                            a lower number of files to be compiled when the "restart"
#                            action is used, not need to specify all files invoked but
#                            only those that have changed 
#
# INCLUDE_DIRS:              list of folders where to find the files to be compiled
#                            example: +incdir+../../../rtl/+../uvm_files/environment/+../uvm_files/sequences/+../uvm_files/tests/+../uvm_files/transactions/ 
#
# NUMBER_OF_SIMULATIONS:     when "multisim" option is selected with launcher.do, this is the number of simulations to perform in a row with the same test. 1 is taken by default
#
# PATH_TO_DUT_FOLDER:        path to the folder where the DUT files (top level netlist, modules, sdf files...) are located
#                            example: /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout
#
# PATH_OLD, PATH_20181220, PATH_20190120, PATH_20190130: paths to different versions of the postlayout netlist (they're defined in sim_annotated/launcher_all_tests.tcl and sim_annotated/launcher.do)
#
# QUESTA_DESIGN_LIB:         name of the design library where ModelSim will store the 
#                            compiled design units (we will create it with vlib)
#                            example: work
#
# QUESTA_DESIGN_LIB_DEFAULT: name of the default design library 
#                            (we will map it to QUESTA_DESIGN_LIB with vmap)
#                            value: work
#
# QUESTA_RUN_OPTIONS:        list of options to use when vsim is called
#                            example: "-t 10ps" (run simulation with 10 ps time precision)
#
# TIMESCALE:                 timeunit/timeprecision to determine the timing resolution for simulation
#                            example: "1 ns / 10 ps"
#
# TOP_MODULE:                name of the simulation top module (e.g. "simpleadder_tb_top_v5")
#
# SDF_CORNER:                "min", "typ" or "max" ("typ" is used by default)
#
# SIMULATION_TIME:           duration of the simulation, example: "900 ns"
#
# UVM_TESTNAME:              name of the test to be simulation, example: "simpleadder_test_v4"
#
# WAVE_FILE:                 path to the wave.do file to load waveforms, example: "wave_do/wave_simpleadder_test_v5.do"
#
# WRITE_TRANSCRIPT_TO_TXT:   "YES" to enable storing the transcript into a file (in the sim_annotated_min/typ/max folder, within logs folder). "NO" is taken by default
#
# YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE: date (in the format year-month-day, for instance 19-01-30) used to select the set of test coverage collection (ucdb files) to merge, the name of the resulting merged coverage
#                                           and the name of the folder where the resulting html coverage report will be stored


############################################################################
# Default values of the arguments                                          #
############################################################################

set DEFAULT_QUESTA_DESIGN_LIB work
set DEFAULT_QUESTA_DESIGN_LIB_DEFAULT work


set DEFAULT_TIMESCALE "1 ns / 10 ps"
set DEFAULT_SIMULATION_TIME "900 ns"

set DEFAULT_BACK_ANNOTATED "NO"
set DEFAULT_SDF_CORNER "typ"
set DEFAULT_WRITE_TRANSCRIPT_TO_TXT "NO"
set DEFAULT_NUMBER_OF_SIMULATIONS 1

set CURRENT_PATH [pwd]

############################################################################
# Export commands so that they can be accessible from other namespaces     #
############################################################################
namespace eval ::actions {
    namespace export setup compile sim all restart_sim clean assign_params perform_action \
				compile_for_coverage  sim_for_coverage merge_coverage_from_alltests merge_coverage_with_testplan \
				write_file_with_dut_path_to_include_in_parametersfile write_file_with_registers_info_to_include_in_parametersfile write_file_with_assocarray_regs_to_include_in_clictdtest write_file_with_dut_ports_to_include_in_dutwrapperpnr \
				generate_verilog_to_include update_column_is_rtl update_netlist_for_simulation \
				build_include_dirs formatTimeInterval generate_filename_including_testnameCornerSystemtime
}
#
#
#
############################################################################
# Add Mentor version to PATH, specify the design library and map the       #
# default design library to it                                             #
############################################################################
proc ::actions::setup { QUESTA_DESIGN_LIB_ARG QUESTA_DESIGN_LIB_DEFAULT_ARG } {

if { $QUESTA_DESIGN_LIB_ARG == "" } { set QUESTA_DESIGN_LIB $::DEFAULT_QUESTA_DESIGN_LIB
} else { set QUESTA_DESIGN_LIB $QUESTA_DESIGN_LIB_ARG
} 

if { $QUESTA_DESIGN_LIB_DEFAULT_ARG == "" } { set QUESTA_DESIGN_LIB_DEFAULT $::DEFAULT_QUESTA_DESIGN_LIB_DEFAULT
} else { set QUESTA_DESIGN_LIB_DEFAULT $QUESTA_DESIGN_LIB_DEFAULT_ARG
} 
  
# Add the current version of ModelSim to the front of the PATH environment 
# variable string (https://www.cc.gatech.edu/~hadi/teaching/cs3220/doc/modelsim/modelsimTutorial.pdf)
# The line below is not necessary here because PATH has been modified in to ~/.cshrc
#  setenv PATH "$MENTOR_ROOTDIR/bin:$PATH"  

# Create design library (where ModelSim stores the compiled design units)
# Note: at this point, we don't create a folder named work!! This will 
# be done automatically later!
	vlib $QUESTA_DESIGN_LIB 

# work is the default design library and it's mapped to ./work by default;
# next we map it to the folder we specified with vlib.
# By mapping, ModelSim creates a directory QUESTA_DESIGN_LIB under 
# sim_questa and creates a file _info inside QUESTA_DESIGN_LIB. 
# You should never edit _info by yourself, it is used by ModelSim to keep 
# track about all design units and their state in your design, like a table 
# of contents of the design library for the simulator.
# In addition, ModelSim copies a file called modelsim.ini in sim_questa if it is 
# not there already and modifies its library section. When ModelSim is invoked, 
# it will read this file and use its mappings to locate design libraries.
# You can check the contents of modelsim.ini with a text editor or typing 
# vmap in the terminal (without arguments)
	vmap $QUESTA_DESIGN_LIB_DEFAULT $QUESTA_DESIGN_LIB
	vmap ; # print the current configuration of vmap to verify that the actions
# above have been correctly executed
}
#
#
#
############################################################################
# Compile the source files (the simulator can't read source code itself,   #
# need to compile the designs into the design library)                     #
############################################################################
proc ::actions::compile { TIMESCALE_ARG INCLUDE_DIRS FILES_TO_COMPILE } {

  if { $TIMESCALE_ARG == "" } { set TIMESCALE $::DEFAULT_TIMESCALE
		} else { set TIMESCALE $TIMESCALE_ARG
		}
		
		
    
		#vlog -override_timescale $TIMESCALE $INCLUDE_DIRS +acc {*}$FILES_TO_COMPILE > temp_compile.log ; # this works!
		vlog -override_timescale $TIMESCALE $INCLUDE_DIRS +acc {*}$FILES_TO_COMPILE 
		#vlog -override_timescale $TIMESCALE $INCLUDE_DIRS +acc $FILES_TO_COMPILE
    #vlog -override_timescale $TIMESCALE $INCLUDE_DIRS -suppress 13276 +acc {*}$FILES_TO_COMPILE
    #suppress -clear 13276
    # be careful: override_timescale is used here, which means that even 
    # if some timescale is specified in the files to be compiled, such
    # timescale will be ignored and the one determined by TIMESCALE variable
    # will be used

    # +acc is used to enable the access to more signals in the hierarchy (otherwise,
    # we can't probe all signals we need); and we limit which blocks to access so as 
    # to avoid that the simulation gets too slow
    # http://www.pldworld.com/_hdl/2/_ref/se_html/manual_html/c_vlog19.html
}
#
#
#
#
############################################################################
# Compile the source files (the simulator can't read source code itself,   #
# need to compile the designs into the design library) - this syntax       #
# enables performing coverage collection afterwards                        #                     
############################################################################
# http://vlsidesignverification.blogspot.com/2015/06/generate-code-coverage-report-with.html
proc ::actions::compile_for_coverage { TIMESCALE_ARG INCLUDE_DIRS FILES_TO_COMPILE } {

  if { $TIMESCALE_ARG == "" } { set TIMESCALE $::DEFAULT_TIMESCALE
		} else { set TIMESCALE $TIMESCALE_ARG
		}
		
		
 		vlog -override_timescale $TIMESCALE $INCLUDE_DIRS -coveropt 3 +cover +acc {*}$FILES_TO_COMPILE 
		
}

#
#
#
############################################################################
# Start the simulation                                                     #
############################################################################
proc ::actions::sim { DESIGN TOP_MODULE UVM_TESTNAME QUESTA_RUN_OPTIONS WAVE_FILE SIMULATION_TIME_ARG } {
	# You can control any return values after the run operation completes with the following preference variables:
  #     noRunTimeMsg : Set this variable to 0 to display simulation time and delta information or set it to 1 to disable the display of this information.
  #     noRunStatusMsg : Set this variable to 0 to display run status informatin or set it to 1 to disable the display of this information.
  #set PrefMain(noRunTimeMsg) 0
  #set PrefMain(noRunStatusMsg) 0
		
		
		if { $SIMULATION_TIME_ARG == "" } { set SIMULATION_TIME $::DEFAULT_SIMULATION_TIME
		} else { set SIMULATION_TIME $SIMULATION_TIME_ARG
		} 


  # Ignore any sdf assignments if RTL
  #if { $DESIGN == "rtl"} {
	#set sdftyp ""
  #}
  
  #restart -f

  #vsim $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME {*}$QUESTA_RUN_OPTIONS
		if { $UVM_TESTNAME != "" } { 
		    #echo "DEBUG ACTIONS.DO SIM"
		    #echo $QUESTA_RUN_OPTIONS 
		    #vsim $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME -voptargs="+acc" -novopt {*}$QUESTA_RUN_OPTIONS > temp_sim.log ; # this doesn't work, it gives error!
						#vsim $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME -voptargs="+acc" -novopt {*}$QUESTA_RUN_OPTIONS
						vsim $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME -voptargs="+acc" {*}$QUESTA_RUN_OPTIONS
						
						#####################################################################################################
						# NOTE THAT +notimingchecks IS USED TO DISABLE CHECKING TIMING VIOLATIONS, THIS IS ONLY TO DEBUG!!! #
						#####################################################################################################
						#vsim $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME -voptargs="+acc" +notimingchecks {*}$QUESTA_RUN_OPTIONS
						
						
		} else { 
		    vsim $TOP_MODULE 
		} 
		# we only use this case to run the code to generate the verilog files to be `included afterwards in CLICTD_dut_wrapper_pnr and top level post layout netlist
  
  #vsim $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME -voptargs="+acc" {*}$QUESTA_RUN_OPTIONS
  # -novopt is slower, but gives you more error messages!

  #quietly set NumericStdNoWarnings 1;
  #quietly set StdArithNoWarnings 1;
  #quietly set IgnoreNote 1;
  #1 step
  #run 
  # run $SIMULATION_TIME_NS

  # allow some time for the UVM testbench to be built, 
  # so as to be able to create the necessary handles to plot them
  run 1 ns

  # Enable wave
  if {$WAVE_FILE != ""} {
		do $WAVE_FILE
  }
  
		  
  #quietly set NumericStdNoWarnings 0;
  #quietly set StdArithNoWarnings 0;

	#if { $DESIGN == "netlist" } {
  #  suppress 3916
  #  run 100 ns
  #  suppress -clear 3916
	#}
	
	 # https://stackoverflow.com/questions/25533774/how-can-i-make-modelsim-exit-with-a-specified-exit-code-from-systemverilog
		# the command below is used so that the simulation ends with a $stop instead of $finish, which means the simulator will remain paused and accept further simulations
		# instead of exiting. Such characteristic is indispensable to be able to run several simulations in a row, for instance when one test is run multiple times
  onfinish stop
		
  run -all ; # runs indefinitely
  #run 900 ns 
  #run {*}$SIMULATION_TIME
  #run 900 ; # takes the latest defined timeprecision (not timeunit)

}

#
#
#
############################################################################
# Start the simulation  - with coverage collection                         #
############################################################################
# http://vlsidesignverification.blogspot.com/2015/06/generate-code-coverage-report-with.html
proc ::actions::sim_for_coverage { DESIGN TOP_MODULE UVM_TESTNAME QUESTA_RUN_OPTIONS WAVE_FILE SIMULATION_TIME_ARG FILENAME_STORE_COVERAGE_UCDB } {



  #while { [file exist ./coverage_db/temp.ucdb] } {
		#}
		
		if { $SIMULATION_TIME_ARG == "" } { set SIMULATION_TIME $::DEFAULT_SIMULATION_TIME
		} else { set SIMULATION_TIME $SIMULATION_TIME_ARG
		} 

  
  set AUX_FILENAME_STORE_COVERAGE_UCDB [string trim "./coverage_db/"][string trim $FILENAME_STORE_COVERAGE_UCDB][string trim ".ucdb"]
		set AUX_FILENAME_STORE_COVERAGE_TXT [string trim "./coverage_db/"][string trim $FILENAME_STORE_COVERAGE_UCDB][string trim ".txt"]
		
		
		#echo "DISPLAY - sim_for_coverage - $UVM_TESTNAME"
		#echo "DISPLAY - $FILENAME_STORE_COVERAGE_UCDB, $AUX_FILENAME_STORE_COVERAGE_UCDB"


  #vsim $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME {*}$QUESTA_RUN_OPTIONS
		if { $UVM_TESTNAME != "" } { 
		    
						#https://wiki.sj.ifsc.edu.br/wiki/images/0/03/Modelsim_pe_ref.pdf						
						vsim -coverage $TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME  -voptargs="+acc" {*}$QUESTA_RUN_OPTIONS
												
		} else { 
		    vsim $TOP_MODULE 
		} 
		# we only use this case to run the code to generate the verilog files to be `included afterwards in CLICTD_dut_wrapper_pnr and top level post layout netlist


  # allow some time for the UVM testbench to be built, 
  # so as to be able to create the necessary handles to plot them
  run 1 ns

  # Enable wave
  if {$WAVE_FILE != ""} {
		do $WAVE_FILE
  }
  
	
	 # https://stackoverflow.com/questions/25533774/how-can-i-make-modelsim-exit-with-a-specified-exit-code-from-systemverilog
		# the command below is used so that the simulation ends with a $stop instead of $finish, which means the simulator will remain paused and accept further simulations
		# instead of exiting. Such characteristic is indispensable to be able to run several simulations in a row, for instance when one test is run multiple times
  onfinish stop
		
  run -all ; # runs indefinitely
		
		# If we set the logical name at the ucdb as the uvm testname and we store coverage for the same test more than once, all the ucdb stored will
		# have the same logical name and this will cause a merge coverage error. To avoid this, we save each test, even those of the same kind, with a 
		# different logical name
		# The '-testname <name>' option is mutually exclusive with any of the following options: -uvmtestname, -ovmtestname or -seed.
		#coverage save -uvmtestname -seed $AUX_FILENAME_STORE_COVERAGE_UCDB
		coverage save -testname $FILENAME_STORE_COVERAGE_UCDB $AUX_FILENAME_STORE_COVERAGE_UCDB
		
		
		echo $AUX_FILENAME_STORE_COVERAGE_UCDB
		echo [vcover attribute -name TESTNAME $AUX_FILENAME_STORE_COVERAGE_UCDB]
				
		# https://verificationacademy.com/forums/coverage/teststatus-5-error-when-merging-ucdbs-same-test-record-name
				
}

#
#
#
############################################################################
# Merge coverage from all the tests whose ucdb is located under a certain  #
# folder, which is selected with the argument passed to this process       #
############################################################################
proc ::actions::merge_coverage_from_alltests { YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE } {


  # Merge coverage from different files 
		# https://verificationacademy.com/forums/coverage/how-merge-coverage-files-0
		#vcover merge *.ucdb -out merged.ucdb
		set FILENAME_MERGED_COVERAGE_UCDB [string trim "./coverage_db/merged_coverage_"][string trim $YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE][string trim ".ucdb"]
		set FILENAME_MERGED_COVERAGE_TXT [string trim "./coverage_db/merged_coverage_"][string trim $YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE][string trim ".txt"]
		
		
		# https://users.ece.cmu.edu/~kbiswas/se_cmds.pdf
		# https://wiki.sj.ifsc.edu.br/wiki/images/0/03/Modelsim_pe_ref.pdf
		# https://verificationacademy.com/forums/coverage/questa-sim-ucdb-merge-issue
		# https://verificationacademy.com/forums/coverage/how-merge-coverage-files-0
		set FILES_TO_MERGE [string trim "./coverage_db/*"][string trim $YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE][string trim "*.ucdb"]
		vcover merge -verbose -64 $FILES_TO_MERGE -out $FILENAME_MERGED_COVERAGE_UCDB


  
  # print details of the coverage group structures
		vcover dump $FILENAME_MERGED_COVERAGE_UCDB > $FILENAME_MERGED_COVERAGE_TXT
		
  # print details of measured coverage
		vcover report -details $FILENAME_MERGED_COVERAGE_UCDB	>> $FILENAME_MERGED_COVERAGE_TXT

		# print the summary of measured coverage
		vcover report $FILENAME_MERGED_COVERAGE_UCDB >> $FILENAME_MERGED_COVERAGE_TXT

  # dump the coverage report into an html report
		set FOLDER_MERGED_COVERAGE_HTML [string trim "./coverage_html/merged_coverage_"][string trim $YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE]
		vcover report -html $FILENAME_MERGED_COVERAGE_UCDB -htmldir $FOLDER_MERGED_COVERAGE_HTML
		# How to open this report?
		# firefox &
		# At the search bar, type: file:///projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_typ/coverage_html/pages/__frametop.htm
		# or select File Open -> select coverage_html/index.html
				
		# Visualize the results
		# https://verificationacademy.com/forums/coverage/how-merge-coverage-files-0
  #vsim -i -viewcov $FILENAME_MERGED_COVERAGE_UCDB
		# After this command, we can go to the GUI and select view/coverage and the types of coverage we want to see (assertions, covergroups...)
}
#
#
#
############################################################################
# Merge the ucdb where the coverage from all tests in a certain folder has #
# been previously merged with a testplan (initially written as an Excel    #
# chart and then exported to an xml file), resulting into a new ucdb and   #
# the associated html report                                               #
############################################################################
# http://munjalm.blogspot.com/2015/09/how-coveragetracking-helps-in.html
proc ::actions::merge_coverage_with_testplan { YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE } {

  set TESTPLAN_XML ./coverage_db/CLICTD_Testplan.xml
		set TESTPLAN_UCDB ./coverage_db/CLICTD_Testplan.ucdb
		
		set ORIGINAL_COVERAGE_MERGE_FROM_TESTS [string trim "./coverage_db/merged_coverage_"][string trim $YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE][string trim ".ucdb"]
		set RESULTING_COVERAGE_MERGE_TESTS_AND_TESTPLAN [string trim "./coverage_db/merged_coverage_and_testplan_"][string trim $YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE][string trim ".ucdb"]

  # Create ucdb from the testplan 
		xml2ucdb -format Excel $TESTPLAN_XML $TESTPLAN_UCDB
		
		# Merge the ucdb created from the testplan with the merged coverage from all tests defined by the date YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE
		vcover merge -verbose -64 $RESULTING_COVERAGE_MERGE_TESTS_AND_TESTPLAN $ORIGINAL_COVERAGE_MERGE_FROM_TESTS $TESTPLAN_UCDB
		
		# dump the coverage report into an html report
		set FOLDER_MERGED_COVERAGE_HTML [string trim "./coverage_html/merged_coverage_with_testplan_"][string trim $YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE]
		vcover report -html $RESULTING_COVERAGE_MERGE_TESTS_AND_TESTPLAN -htmldir $FOLDER_MERGED_COVERAGE_HTML
		# How to open this report?
		# firefox &
		# At the search bar, type: file:///projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_typ/coverage_html/pages/__frametop.htm
		# or select File Open -> select coverage_html/index.html
		
		# Visualize the results
		# https://verificationacademy.com/forums/coverage/how-merge-coverage-files-0
  #vsim -i -viewcov $RESULTING_COVERAGE_MERGE_TESTS_AND_TESTPLAN
		# After this command, we can go to the GUI and select view/coverage and the types of coverage we want to see (assertions, covergroups...)
}

#
#
#
############################################################################
# Compile and start the simulation in a row
############################################################################
proc ::actions::all { TIMESCALE_ARG INCLUDE_DIRS FILES_TO_COMPILE DESIGN TOP_MODULE UVM_TESTNAME QUESTA_RUN_OPTIONS WAVE_FILE SIMULATION_TIME_ARG } {
  
		if { $TIMESCALE_ARG == "" } { set TIMESCALE $::DEFAULT_TIMESCALE
		} else { set TIMESCALE $TIMESCALE_ARG
		}
		
		if { $SIMULATION_TIME_ARG == "" } { set SIMULATION_TIME $::DEFAULT_SIMULATION_TIME
		} else { set SIMULATION_TIME $SIMULATION_TIME_ARG
		}
		
		
  # Note: the steps commented below have been moved to launcher.do
  # so that they're always performed
	## Stop the former simulation
  #quit -sim 
  ## Clear and reopen the transcript file
  #.main clear
  #transcript file ""
  #transcript file transcript

  # Compile the design files
  compile $TIMESCALE $INCLUDE_DIRS $FILES_TO_COMPILE  
  # Simulate
  sim $DESIGN $TOP_MODULE $UVM_TESTNAME $QUESTA_RUN_OPTIONS $WAVE_FILE $SIMULATION_TIME
  # http://wiki.tcl.tk/11156
}
#
#
#
############################################################################
# Recompile specific files and restart the simulation (this is useful when #
# the only changes are made in a few files, for instance the testbench)    #
# https://stackoverflow.com/questions/5265807/how-to-restart-a-verilog-simulation-in-modelsim
############################################################################
proc ::actions::restart_sim { TIMESCALE_ARG INCLUDE_DIRS FILES_TO_RECOMPILE SIMULATION_TIME_ARG } {
   if { $TIMESCALE_ARG == "" } { set TIMESCALE $::DEFAULT_TIMESCALE
			} else { set TIMESCALE $TIMESCALE_ARG
			}
			
			if { $SIMULATION_TIME_ARG == "" } { set SIMULATION_TIME $::DEFAULT_SIMULATION_TIME
			} else { set SIMULATION_TIME $SIMULATION_TIME_ARG
			}
		
   compile $TIMESCALE $INCLUDE_DIRS $FILES_TO_RECOMPILE
   restart -f
   #run {*}$SIMULATION_TIME
   run -all
}
#
#
#
############################################################################
# Delete files from the design library (avoid errors due to old files when #
# we recompile)                                                            #
############################################################################
proc ::actions::clean { QUESTA_DESIGN_LIB_DEFAULT_ARG } {
 
if { $QUESTA_DESIGN_LIB_DEFAULT_ARG == "" } { set QUESTA_DESIGN_LIB_DEFAULT $::DEFAULT_QUESTA_DESIGN_LIB_DEFAULT
} else { set QUESTA_DESIGN_LIB_DEFAULT $QUESTA_DESIGN_LIB_DEFAULT_ARG
} 

	vdel -lib $QUESTA_DESIGN_LIB_DEFAULT -all
}



############################################################################
# Auxiliary function to replace parts of strings                           #
############################################################################
# From https://gitlab.cern.ch/afiergol/HDLVerificationLibrary/blob/master/QuestaScripts/vsimPackage.do by Adrian Fiergolski
proc generate_arguments_from_file { {file_name ""} {prefix_command ""} {replaced_string ""} {replacing_string ""} } {
    # https://www.tcl.tk/man/tcl8.4/TclCmd/regsub.htm

    set fp [open $file_name r]
    set file_lines [split [read $fp] "\n"]
    set args {}
    foreach line $file_lines {
	       lappend args $prefix_command [regsub -- [join [list "(\[^\.]*)" RTL_PATH "(\[^\.]*)" ] "" ] $line [join [list \\1 $replacing_string \\2 ] "" ] ]
    }
				# regsub ?switches? exp string subSpec 
				# --     : Marks the end of switches. The argument following this one will be treated as exp 
    # exp    : [join [list "(\[^\.]*)" RTL_PATH "(\[^\.]*)" ] "" ]
				# string : $line
				# subSpec: [join [list \\1 $replacing_string \\2 ] "" ]
				# The portion of string that matches exp is replaced with subSpec. If subSpec contains a ``\n'', where n is a digit between 1 and 9, then it is replaced in the substitution with the portion of string that matches the n-th parenthesized subexpression of exp.
    return $args
}



############################################################################
# Build INCLUDE_DIRS for the corresponding PATH_TO_DUT_FOLDER              #
############################################################################

proc ::actions::build_include_dirs { PATH_TO_DUT_FOLDER } {

   set INCLUDE_DIRS_AUX "+incdir+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/clictdTop/syn/verilog/+../+../rtl/+../sim/wave_dos/+../environment/+../environment/tasks_and_functions+../environment/auxiliary_classes+../sequences/+../tests/+../transactions/+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/column/syn/verilog/+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/endOfColumn/syn/verilog+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/periphery/syn/verilog+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/pixel190/syn/verilog+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/pixelNoBuffers/syn/verilog+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/SPixDigital/syn/verilog+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/analog/AnalogCLICTD_rb/AnalogTerminationBottomEdge/functional/+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/analog/AnalogCLICTD_rb/AnalogTerminationLeftEdge/functional/+/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/analog/CLICTD_Periphery/digitalTermLeft/functional/+"
		
  set INCLUDE_DIRS [string trim $INCLUDE_DIRS_AUX][string trim $PATH_TO_DUT_FOLDER][string trim "+"][string trim $PATH_TO_DUT_FOLDER][string trim "/column_RTL"]
		
  return $INCLUDE_DIRS
}


############################################################################
# Assign arguments into simulation parameters                              #
############################################################################

proc ::actions::assign_params { DESIGN UVM_TESTNAME PATH_TO_DUT_FOLDER USE_WAVE_LOG } {

  # Simulation top module
	 set TOP_MODULE CLICTD_testbench_top

  # Technology files
  set TECH_FILE1 "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt.v"
  set TECH_FILE2 "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/techfiles/tsl18fs190svt_udp.v"
  set TECH_FILE_AUX [concat $TECH_FILE1 $TECH_FILE2]
  set TECH_FILE [split $TECH_FILE_AUX " "]

  # Note: all the UVM files are `include(d) in CLICTD_test_pkg, therefore, we don't need to specify 
  # the path to each of them individually in UVM_FILES
	 #set UVM_FILES [concat "../tests/CLICTD_test_pkg.sv" "../CLICTD_typedefs_pkg.sv"  "../CLICTD_testbench_top.sv"]
  set UVM_FILES "../CLICTD_testbench_top.sv"
  #set UVM_FILES [concat "../CLICTD_testbench_top.sv" "../CLICTD_testbench_top_backannotated_max.sv" "../CLICTD_testbench_top_backannotated_typ.sv" "../CLICTD_testbench_top_backannotated_min.sv"] 

  # Path to DUT, interface and wrapper modules
  #set DUT_FILES [concat "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/clictdTop/syn/verilog/clictdTop.v" "../rtl/CLICTD_readout_interface.sv"  "../rtl/CLICTD_pins_interface.sv" "../rtl/CLICTD_slowcontrol_interface.sv" "../rtl/CLICTD_dut_wrapper.sv"]

  # Below the clictdTop used is a workaround to be able to use Adrian Fiergolski's unidirectional I2C master
  # (which is instantiated in CLICTD_slowcontrol_interface.sv
  if { $DESIGN == "rtl" } {
       set DUT_FILES [concat "../rtl/clictdTop_workaroundi2c.v" "../rtl/CLICTD_readout_interface.sv"  "../rtl/CLICTD_pins_interface.sv" "../rtl/CLICTD_slowcontrol_interface.sv" "../rtl/CLICTD_dut_wrapper_rtl.sv"]
  } else  {
		     set DUT_FILES [concat [string trim $PATH_TO_DUT_FOLDER][string trim "/column_RTL/column_RTL.v"] "../rtl/clictdTop_pnr_workaroundi2c_somecolpnr.v" "../rtl/CLICTD_readout_interface.sv"  "../rtl/CLICTD_pins_interface.sv" "../rtl/CLICTD_slowcontrol_interface.sv" "../rtl/CLICTD_dut_wrapper_pnr.sv"]
  }


 # file where the waveforms to be loaded are specified
 #set WAVE_FILE "" ; # to avoid excessive memory occupation -> this may lead to program collapse at a certain point of the test due to lack of memory
	if { $USE_WAVE_LOG == "YES" } {
	     if { $UVM_TESTNAME == "CLICTD_reset_test" } {
    						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_reset_test_log.do" ; 
 						}
	} else {
 						if { $UVM_TESTNAME == "CLICTD_reset_test" } {
    						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_reset_test.do" ; 
 						} elseif { $UVM_TESTNAME == "CLICTD_configuration_test" } {
    						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_configuration_test.do" ; 
 						} elseif { $UVM_TESTNAME == "CLICTD_readout_test" } {
    						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_readout_test.do" ; 
										#set WAVE_FILE ""
 						} elseif { $UVM_TESTNAME == "CLICTD_readout_test_for_coverage" } {
    						#set WAVE_FILE "../sim/wave_dos/wave_CLICTD_readout_test_for_coverage.do" ; 
										#set WAVE_FILE ""
										#set WAVE_FILE "../sim/wave_dos/wave_CLICTD_readout_test.do" ; 
										set WAVE_FILE "../sim/wave_dos/wave_CLICTD_readout_test_for_coverage.do" 
 						} elseif { $UVM_TESTNAME == "CLICTD_readoutcontrol_test" } {
    						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_readoutcontrol_test.do"
 						} elseif { $UVM_TESTNAME == "CLICTD_powercontrol_test" } {
    						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_powercontrol_test.do"
 						} elseif { $UVM_TESTNAME == "CLICTD_testpulsecontrol_test" } {
    						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_readout_test.do"
							} elseif { $UVM_TESTNAME == "CLICTD_wrregisters_test" } {
	   						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_wrregisters_test.do"
							} elseif  { $UVM_TESTNAME == "CLICTD_reset_synchronizer_violations_test" } {
	   						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_resetsync_test.do"
 						} elseif { $UVM_TESTNAME == "CLICTD_readoutstart_synchronizer_violations_test" } {
	   						set WAVE_FILE "../sim/wave_dos/wave_CLICTD_readoutstartsync_test.do"
							} else {
    						set WAVE_FILE ""
 						}
	}


  # NOTE: WHAT DO I DO TO DEBUG?
  # First, I set FILES_TO_COMPILE only to $TECH_FILE $DUT_FILES
  # Then, I add UVM_FILES, but in CLICTD_test_pkg I only uncomment the lowest block in the hierarchy (transaction).
  # In this case, I started only with CLICTD_pins_transaction.
  # I compile and fix errors.
  # One by one, I uncomment the next level in the hierarchy (CLICTD_pins_monitor) and repeat, and so on, until I reach
  # CLICTD_test.  I repeat for the other interfaces (readout, slow control...)


  set FILES_TO_COMPILE_AUX [concat $TECH_FILE $DUT_FILES $UVM_FILES]
  set FILES_TO_COMPILE [split $FILES_TO_COMPILE_AUX " "]
  set FILES_TO_RECOMPILE [split $UVM_FILES " "]
		
		
		set INCLUDE_DIRS [actions::build_include_dirs $PATH_TO_DUT_FOLDER]
		

				
		return [list $TOP_MODULE $WAVE_FILE $FILES_TO_COMPILE $FILES_TO_RECOMPILE $INCLUDE_DIRS ]
}



############################################################################
# Provide the path where the DUT is located to a file that is included in  #
# CLICTD_parameters to know which files to source in                       #
# CLICTD_generate_verilog_to_include                                       #
############################################################################
proc ::actions::write_file_with_dut_path_to_include_in_parametersfile { PATH_TO_DUT_FOLDER } {

    set FILE_TO_WRITE ../CLICTD_paths_including_dut_folder.sv
				
				# Open the file for writing
    set FILE_OPENED [open $FILE_TO_WRITE w]
				
				# Write the paths into the file
				set AUX_PATH_COLUMNOUTPUT [string trim $PATH_TO_DUT_FOLDER][string trim "/column_output.sdf"]
				set AUX_PATH_CLICTDOUTPUT [string trim $PATH_TO_DUT_FOLDER][string trim "/clictd_output.sdf"]
    set AUX_PATH_CLICTDNETLIST [string trim $PATH_TO_DUT_FOLDER][string trim "/clictd_export.v"]
				
				set line [string trim "`define PATH_ORIGINAL_COLUMNOUTPUT_SDF \""][string trim $AUX_PATH_COLUMNOUTPUT][string trim "\""]
				#echo $line
				puts $FILE_OPENED $line

    set line [string trim "`define PATH_ORIGINAL_CLICTDOUTPUT_SDF \""][string trim $AUX_PATH_CLICTDOUTPUT][string trim "\""]
				#echo $line
				puts $FILE_OPENED $line
				
				set line [string trim "`define PATH_ORIGINAL_CLICTDNETLIST_POSTLAYOUT \""][string trim $AUX_PATH_CLICTDNETLIST][string trim "\""]
				#echo $line
				puts $FILE_OPENED $line

				#`define PATH_ORIGINAL_COLUMNOUTPUT_SDF "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/netlistPostLayout/column_output.sdf"
				#`define PATH_ORIGINAL_CLICTDOUTPUT_SDF "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/netlistPostLayout/clictd_output.sdf"
				#`define PATH_ORIGINAL_CLICTDNETLIST_POSTLAYOUT "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/netlistPostLayout/clictd_export.v"

    # Close the file
    close $FILE_OPENED

}


############################################################################
# Provide the registers address and default value to a file that is        #
# included in CLICTD_parameters                                            #
############################################################################
proc ::actions::write_file_with_registers_info_to_include_in_parametersfile { PATH_TO_DUT_FOLDER PATH_OLD PATH_20181220 PATH_20190120  PATH_20190130 USE_HIGHER_CLOCK_FREQ USE_VERY_SHORT_TEST_PULSES USE_FAST_I2C_CLOCK READOUTTESTFORCOVERAGE_DEBUGMODE } {

    set FILE_TO_WRITE ../CLICTD_registers_info.sv
				
				# Open the file for writing
    set FILE_OPENED [open $FILE_TO_WRITE w]
				
				
				
				if { $USE_HIGHER_CLOCK_FREQ != "" } {
				    puts $FILE_OPENED "// Clock periods"
  						puts $FILE_OPENED "// Configuration"
  						puts $FILE_OPENED "`define CLK_HALF_PERIOD_40MHz  10  //ns"
  						puts $FILE_OPENED "// Acquisition"
  						puts $FILE_OPENED "`define CLK_HALF_PERIOD_100MHz  4  //ns"
				} else {
				    puts $FILE_OPENED "// Clock periods"
  						puts $FILE_OPENED "// Configuration"
  						puts $FILE_OPENED "`define CLK_HALF_PERIOD_40MHz  12.5  //ns"
  						puts $FILE_OPENED "// Acquisition"
  						puts $FILE_OPENED "`define CLK_HALF_PERIOD_100MHz  5  //ns"
				}
				
				
				if { $USE_VERY_SHORT_TEST_PULSES != "" } {
				    puts $FILE_OPENED "`define USE_VERY_SHORT_TEST_PULSES 1"
				}
				
				if { $USE_FAST_I2C_CLOCK != "" } {
				    puts $FILE_OPENED "`define USE_FAST_I2C_CLOCK 1"
				}
				
				if { $READOUTTESTFORCOVERAGE_DEBUGMODE != "" } {
				    puts $FILE_OPENED "`define READOUTTESTFORCOVERAGE_DEBUGMODE 1"
				}
				
				if { [string equal $PATH_TO_DUT_FOLDER $PATH_OLD] } { 
        puts $FILE_OPENED "// Number of slow control registers"
								puts $FILE_OPENED "`define NUMBER_SLOWCONTROL_REGISTERS 35"
								puts $FILE_OPENED "// List of addresses of the slow control registers"
								puts $FILE_OPENED "`define ADDRESS_globalConfig         8'h00"
								puts $FILE_OPENED "`define ADDRESS_internalStrobes      8'h01"
								puts $FILE_OPENED "`define ADDRESS_analogInMux          8'h02"
								puts $FILE_OPENED "`define ADDRESS_analogOutMux         8'h03"
								puts $FILE_OPENED "`define ADDRESS_matrixConfig         8'h04"
								puts $FILE_OPENED "`define ADDRESS_configCtrl           8'h05"
								puts $FILE_OPENED "`define ADDRESS_configData7downto0   8'h06"
								puts $FILE_OPENED "`define ADDRESS_configData15downto8  8'h07"
								puts $FILE_OPENED "`define ADDRESS_readoutCtrl          8'h08"
								puts $FILE_OPENED "`define ADDRESS_lvdsCtrl             8'h09"
								puts $FILE_OPENED "`define ADDRESS_tpulseCtrl7downto0   8'h0A"
								puts $FILE_OPENED "`define ADDRESS_tpulseCtrl15downto8  8'h0B"
								puts $FILE_OPENED "`define ADDRESS_VBIASResetTransistor 8'h10 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VRESET               8'h11 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASLevelShift      8'h12 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VANALOG1             8'h13 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VANALOG2             8'h14 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASPREAMPN         8'h15 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VNCASC               8'h16 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VPCASC               8'h17 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VFBK                 8'h18 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASIKRUM           8'h19 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCN           8'h1A // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCP           8'h1B // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASDAC             8'h1C // TBD"
								puts $FILE_OPENED "`define ADDRESS_VTHRESHOLD7downto0   8'h1D // TBD"
								puts $FILE_OPENED "`define ADDRESS_VTHRESHOLD9downto8   8'h1E // TBD"
 							puts $FILE_OPENED "`define ADDRESS_VNCASCCOMP           8'h1F // TBD"
 							puts $FILE_OPENED "`define ADDRESS_VBIASLevelShift_OFF  8'h20 // TBD"
 							puts $FILE_OPENED "`define ADDRESS_VBIASPREAMPN_OFF     8'h21 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCN_OFF       8'h22 // TBD"
 							puts $FILE_OPENED "`define ADDRESS_VBIASDISCP_OFF       8'h23 // TBD"
 							puts $FILE_OPENED "`define ADDRESS_VBIASDAC_OFF         8'h24 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VLVDSD               8'h25 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VLVDSR               8'h26 // TBD"
								puts $FILE_OPENED "// List of default values of the slow control registers"
 							puts $FILE_OPENED "`define DEFAULT_globalConfig         8'h01"  
 							puts $FILE_OPENED "`define DEFAULT_internalStrobes      8'h00"
								puts $FILE_OPENED "`define DEFAULT_analogInMux          8'h00"
								puts $FILE_OPENED "`define DEFAULT_analogOutMux         8'h00"
								puts $FILE_OPENED "`define DEFAULT_matrixConfig         8'h08"
								puts $FILE_OPENED "`define DEFAULT_configCtrl           8'h00"
								puts $FILE_OPENED "`define DEFAULT_configData7downto0   8'h00"
								puts $FILE_OPENED "`define DEFAULT_configData15downto8  8'h00"
								puts $FILE_OPENED "`define DEFAULT_readoutCtrl          8'h00"
								puts $FILE_OPENED "`define DEFAULT_lvdsCtrl             8'h07"
								puts $FILE_OPENED "`define DEFAULT_tpulseCtrl7downto0   8'h00"
								puts $FILE_OPENED "`define DEFAULT_tpulseCtrl15downto8  8'h00"
								puts $FILE_OPENED "`define DEFAULT_VBIASResetTransistor 8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VRESET               8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASLevelShift      8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VANALOG1             8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VANALOG2             8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASPREAMPN         8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VNCASC               8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VPCASC               8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VFBK                 8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASIKRUM           8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCN           8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCP           8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASDAC             8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VTHRESHOLD7downto0   8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VTHRESHOLD9downto8   8'h00 // TBD"
 							puts $FILE_OPENED "`define DEFAULT_VNCASCCOMP           8'h00 // TBD"
 							puts $FILE_OPENED "`define DEFAULT_VBIASLevelShift_OFF  8'h00 // TBD"
 							puts $FILE_OPENED "`define DEFAULT_VBIASPREAMPN_OFF     8'h00 // TBD"
 							puts $FILE_OPENED "`define DEFAULT_VBIASDISCN_OFF       8'h00 // TBD"
 							puts $FILE_OPENED "`define DEFAULT_VBIASDISCP_OFF       8'h00 // TBD"
 							puts $FILE_OPENED "`define DEFAULT_VBIASDAC_OFF         8'h00 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VLVDSD               8'h0F"
								puts $FILE_OPENED "`define DEFAULT_VLVDSR               8'h0F"
								
    } elseif { [string equal $PATH_TO_DUT_FOLDER $PATH_20181220] } {
				    puts $FILE_OPENED "// Number of slow control registers"
								puts $FILE_OPENED "`define NUMBER_SLOWCONTROL_REGISTERS 36"
								puts $FILE_OPENED "// List of addresses of the slow control registers"
								puts $FILE_OPENED "`define ADDRESS_globalConfig         8'h00"
								puts $FILE_OPENED "`define ADDRESS_internalStrobes      8'h01" 
								puts $FILE_OPENED "`define ADDRESS_externalDACSel       8'h02" 
								puts $FILE_OPENED "`define ADDRESS_monitorDACSel        8'h03"
								puts $FILE_OPENED "`define ADDRESS_matrixConfig         8'h04"
								puts $FILE_OPENED "`define ADDRESS_configCtrl           8'h05"
								puts $FILE_OPENED "`define ADDRESS_configData7downto0   8'h06"
								puts $FILE_OPENED "`define ADDRESS_configData15downto8  8'h07"
								puts $FILE_OPENED "`define ADDRESS_readoutCtrl          8'h08"
								puts $FILE_OPENED "`define ADDRESS_tpulseCtrl7downto0   8'h0A"
								puts $FILE_OPENED "`define ADDRESS_tpulseCtrl15downto8  8'h0B"
								puts $FILE_OPENED "`define ADDRESS_VBIASResetTransistor 8'h10 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VRESET               8'h11 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASLevelShift      8'h12 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VANALOG17downto0     8'h13 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VANALOG115downto8    8'h14 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VANALOG2             8'h15 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASPREAMPN         8'h16 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VNCASC               8'h17 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VPCASC               8'h18 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VFBK                 8'h19 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASIKRUM           8'h1A // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCN           8'h1B // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCP           8'h1C // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASDAC             8'h1D // TBD"
								puts $FILE_OPENED "`define ADDRESS_VTHRESHOLD7downto0   8'h1E // TBD"
								puts $FILE_OPENED "`define ADDRESS_VTHRESHOLD9downto8   8'h1F // TBD"
 							puts $FILE_OPENED "`define ADDRESS_VNCASCCOMP           8'h20 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASLevelShiftstby  8'h21 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASPREAMPNstby     8'h22 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCNstby       8'h23 // TBD"
 							puts $FILE_OPENED "`define ADDRESS_VBIASDISCPstby       8'h24 // TBD"
 							puts $FILE_OPENED "`define ADDRESS_VBIASDACstby         8'h25 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VBIASSlowBuffer      8'h26 // TBD"
								puts $FILE_OPENED "`define ADDRESS_AdjustDACRange       8'h27 // TBD"
								puts $FILE_OPENED "`define ADDRESS_VLVDSD               8'h28"
								puts $FILE_OPENED "// List of default values of the slow control registers"
								puts $FILE_OPENED "`define DEFAULT_globalConfig         8'h01"
								puts $FILE_OPENED "`define DEFAULT_internalStrobes      8'h00" 
								puts $FILE_OPENED "`define DEFAULT_externalDACSel       8'h00" 
								puts $FILE_OPENED "`define DEFAULT_monitorDACSel        8'h00"
								puts $FILE_OPENED "`define DEFAULT_matrixConfig         8'h08"
								puts $FILE_OPENED "`define DEFAULT_configCtrl           8'h00"
								puts $FILE_OPENED "`define DEFAULT_configData7downto0   8'h00"
								puts $FILE_OPENED "`define DEFAULT_configData15downto8  8'h00"
								puts $FILE_OPENED "`define DEFAULT_readoutCtrl          8'h00"
								puts $FILE_OPENED "`define DEFAULT_tpulseCtrl7downto0   8'hFF"
								puts $FILE_OPENED "`define DEFAULT_tpulseCtrl15downto8  8'hFF"
								puts $FILE_OPENED "`define DEFAULT_VBIASResetTransistor 8'h10 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VRESET               8'h11 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASLevelShift      8'h12 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VANALOG17downto0     8'h13 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VANALOG115downto8    8'h14 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VANALOG2             8'h15 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASPREAMPN         8'h16 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VNCASC               8'h17 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VPCASC               8'h18 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VFBK                 8'h19 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASIKRUM           8'h1A // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCN           8'h1B // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCP           8'h1C // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASDAC             8'h1D // TBD"
								puts $FILE_OPENED "`define DEFAULT_VTHRESHOLD7downto0   8'h1E // TBD"
								puts $FILE_OPENED "`define DEFAULT_VTHRESHOLD9downto8   8'h1F // TBD"
 							puts $FILE_OPENED "`define DEFAULT_VNCASCCOMP           8'h20 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASLevelShiftstby  8'h21 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASPREAMPNstby     8'h22 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCNstby       8'h23 // TBD"
 							puts $FILE_OPENED "`define DEFAULT_VBIASDISCPstby       8'h24 // TBD"
 							puts $FILE_OPENED "`define DEFAULT_VBIASDACstby         8'h25 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VBIASSlowBuffer      8'h26 // TBD"
								puts $FILE_OPENED "`define DEFAULT_AdjustDACRange       8'h27 // TBD"
								puts $FILE_OPENED "`define DEFAULT_VLVDSD               8'h0F"
								
				} elseif { [string equal $PATH_TO_DUT_FOLDER $PATH_20190120] } {
				    puts $FILE_OPENED "// Number of slow control registers"
								puts $FILE_OPENED "`define NUMBER_SLOWCONTROL_REGISTERS 36"
								puts $FILE_OPENED "// List of addresses of the slow control registers"
								puts $FILE_OPENED "`define ADDRESS_globalConfig         8'h00"
								puts $FILE_OPENED "`define ADDRESS_internalStrobes      8'h01" 
								puts $FILE_OPENED "`define ADDRESS_externalDACSel       8'h02" 
								puts $FILE_OPENED "`define ADDRESS_monitorDACSel        8'h03"
								puts $FILE_OPENED "`define ADDRESS_matrixConfig         8'h04"
								puts $FILE_OPENED "`define ADDRESS_configCtrl           8'h05"
								puts $FILE_OPENED "`define ADDRESS_configData7downto0   8'h06"
								puts $FILE_OPENED "`define ADDRESS_configData15downto8  8'h07"
								puts $FILE_OPENED "`define ADDRESS_readoutCtrl          8'h08"
								puts $FILE_OPENED "`define ADDRESS_tpulseCtrl7downto0   8'h0A"
								puts $FILE_OPENED "`define ADDRESS_tpulseCtrl15downto8  8'h0B"
								puts $FILE_OPENED "`define ADDRESS_VBIASResetTransistor 8'h10"
								puts $FILE_OPENED "`define ADDRESS_VRESET               8'h11"
								puts $FILE_OPENED "`define ADDRESS_VBIASLevelShift      8'h12"
								puts $FILE_OPENED "`define ADDRESS_VANALOG17downto0     8'h13"
								puts $FILE_OPENED "`define ADDRESS_VANALOG115downto8    8'h14"
								puts $FILE_OPENED "`define ADDRESS_VANALOG2             8'h15"
								puts $FILE_OPENED "`define ADDRESS_VBIASPREAMPN         8'h16"
								puts $FILE_OPENED "`define ADDRESS_VNCASC               8'h17"
								puts $FILE_OPENED "`define ADDRESS_VPCASC               8'h18"
								puts $FILE_OPENED "`define ADDRESS_VFBK                 8'h19"
								puts $FILE_OPENED "`define ADDRESS_VBIASIKRUM           8'h1A"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCN           8'h1B"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCP           8'h1C"
								puts $FILE_OPENED "`define ADDRESS_VBIASDAC             8'h1D"
								puts $FILE_OPENED "`define ADDRESS_VTHRESHOLD7downto0   8'h1E"
								puts $FILE_OPENED "`define ADDRESS_VTHRESHOLD9downto8   8'h1F"
 							puts $FILE_OPENED "`define ADDRESS_VNCASCCOMP           8'h20"
								puts $FILE_OPENED "`define ADDRESS_VBIASLevelShiftstby  8'h21"
								puts $FILE_OPENED "`define ADDRESS_VBIASPREAMPNstby     8'h22"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCNstby       8'h23"
 							puts $FILE_OPENED "`define ADDRESS_VBIASDISCPstby       8'h24"
 							puts $FILE_OPENED "`define ADDRESS_VBIASDACstby         8'h25"
								puts $FILE_OPENED "`define ADDRESS_VBIASSlowBuffer      8'h26"
								puts $FILE_OPENED "`define ADDRESS_AdjustDACRange       8'h27"
								puts $FILE_OPENED "`define ADDRESS_VLVDSD               8'h28"
								puts $FILE_OPENED "// List of default values of the slow control registers"
								puts $FILE_OPENED "`define DEFAULT_globalConfig         8'h01"
								puts $FILE_OPENED "`define DEFAULT_internalStrobes      8'h00" 
								puts $FILE_OPENED "`define DEFAULT_externalDACSel       8'h00" 
								puts $FILE_OPENED "`define DEFAULT_monitorDACSel        8'h00"
								puts $FILE_OPENED "`define DEFAULT_matrixConfig         8'h08"
								puts $FILE_OPENED "`define DEFAULT_configCtrl           8'h00"
								puts $FILE_OPENED "`define DEFAULT_configData7downto0   8'h00"
								puts $FILE_OPENED "`define DEFAULT_configData15downto8  8'h00"
								puts $FILE_OPENED "`define DEFAULT_readoutCtrl          8'h00"
								puts $FILE_OPENED "`define DEFAULT_tpulseCtrl7downto0   8'hFF"
								puts $FILE_OPENED "`define DEFAULT_tpulseCtrl15downto8  8'hFF"
								puts $FILE_OPENED "`define DEFAULT_VBIASResetTransistor 8'h13"
								puts $FILE_OPENED "`define DEFAULT_VRESET               8'h68"
								puts $FILE_OPENED "`define DEFAULT_VBIASLevelShift      8'h09"
								puts $FILE_OPENED "`define DEFAULT_VANALOG17downto0     8'h00"
								puts $FILE_OPENED "`define DEFAULT_VANALOG115downto8    8'h00"
								puts $FILE_OPENED "`define DEFAULT_VANALOG2             8'h00"
								puts $FILE_OPENED "`define DEFAULT_VBIASPREAMPN         8'h05"
								puts $FILE_OPENED "`define DEFAULT_VNCASC               8'h5B"
								puts $FILE_OPENED "`define DEFAULT_VPCASC               8'h82"
								puts $FILE_OPENED "`define DEFAULT_VFBK                 8'h47"
								puts $FILE_OPENED "`define DEFAULT_VBIASIKRUM           8'h04"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCN           8'h08"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCP           8'h09"
								puts $FILE_OPENED "`define DEFAULT_VBIASDAC             8'h07"
								puts $FILE_OPENED "`define DEFAULT_VTHRESHOLD7downto0   8'h9A"
								puts $FILE_OPENED "`define DEFAULT_VTHRESHOLD9downto8   8'h00"
 							puts $FILE_OPENED "`define DEFAULT_VNCASCCOMP           8'h76"
								puts $FILE_OPENED "`define DEFAULT_VBIASLevelShiftstby  8'h01"
								puts $FILE_OPENED "`define DEFAULT_VBIASPREAMPNstby     8'h01"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCNstby       8'h01"
 							puts $FILE_OPENED "`define DEFAULT_VBIASDISCPstby       8'h01"
 							puts $FILE_OPENED "`define DEFAULT_VBIASDACstby         8'h01"
								puts $FILE_OPENED "`define DEFAULT_VBIASSlowBuffer      8'h01"
								puts $FILE_OPENED "`define DEFAULT_AdjustDACRange       8'h80"
								puts $FILE_OPENED "`define DEFAULT_VLVDSD               8'h0F"
								
				}  elseif { [string equal $PATH_TO_DUT_FOLDER $PATH_20190130] } {
				    puts $FILE_OPENED "// Number of slow control registers"
								puts $FILE_OPENED "`define NUMBER_SLOWCONTROL_REGISTERS 36"
								puts $FILE_OPENED "// List of addresses of the slow control registers"
								puts $FILE_OPENED "`define ADDRESS_globalConfig         8'h00"
								puts $FILE_OPENED "`define ADDRESS_internalStrobes      8'h01" 
								puts $FILE_OPENED "`define ADDRESS_externalDACSel       8'h02" 
								puts $FILE_OPENED "`define ADDRESS_monitorDACSel        8'h03"
								puts $FILE_OPENED "`define ADDRESS_matrixConfig         8'h04"
								puts $FILE_OPENED "`define ADDRESS_configCtrl           8'h05"
								puts $FILE_OPENED "`define ADDRESS_configData7downto0   8'h06"
								puts $FILE_OPENED "`define ADDRESS_configData15downto8  8'h07"
								puts $FILE_OPENED "`define ADDRESS_readoutCtrl          8'h08"
								puts $FILE_OPENED "`define ADDRESS_tpulseCtrl7downto0   8'h0A"
								puts $FILE_OPENED "`define ADDRESS_tpulseCtrl15downto8  8'h0B"
								puts $FILE_OPENED "`define ADDRESS_VBIASResetTransistor 8'h10"
								puts $FILE_OPENED "`define ADDRESS_VRESET               8'h11"
								puts $FILE_OPENED "`define ADDRESS_VBIASLevelShift      8'h12"
								puts $FILE_OPENED "`define ADDRESS_VANALOG17downto0     8'h13"
								puts $FILE_OPENED "`define ADDRESS_VANALOG115downto8    8'h14"
								puts $FILE_OPENED "`define ADDRESS_VANALOG2             8'h15"
								puts $FILE_OPENED "`define ADDRESS_VBIASPREAMPN         8'h16"
								puts $FILE_OPENED "`define ADDRESS_VNCASC               8'h17"
								puts $FILE_OPENED "`define ADDRESS_VPCASC               8'h18"
								puts $FILE_OPENED "`define ADDRESS_VFBK                 8'h19"
								puts $FILE_OPENED "`define ADDRESS_VBIASIKRUM           8'h1A"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCN           8'h1B"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCP           8'h1C"
								puts $FILE_OPENED "`define ADDRESS_VBIASDAC             8'h1D"
								puts $FILE_OPENED "`define ADDRESS_VTHRESHOLD7downto0   8'h1E"
								puts $FILE_OPENED "`define ADDRESS_VTHRESHOLD9downto8   8'h1F"
 							puts $FILE_OPENED "`define ADDRESS_VNCASCCOMP           8'h20"
								puts $FILE_OPENED "`define ADDRESS_VBIASLevelShiftstby  8'h21"
								puts $FILE_OPENED "`define ADDRESS_VBIASPREAMPNstby     8'h22"
								puts $FILE_OPENED "`define ADDRESS_VBIASDISCNstby       8'h23"
 							puts $FILE_OPENED "`define ADDRESS_VBIASDISCPstby       8'h24"
 							puts $FILE_OPENED "`define ADDRESS_VBIASDACstby         8'h25"
								puts $FILE_OPENED "`define ADDRESS_VBIASSlowBuffer      8'h26"
								puts $FILE_OPENED "`define ADDRESS_AdjustDACRange       8'h27"
								puts $FILE_OPENED "`define ADDRESS_VLVDSD               8'h28"
								puts $FILE_OPENED "// List of default values of the slow control registers"
								puts $FILE_OPENED "`define DEFAULT_globalConfig         8'h01"
								puts $FILE_OPENED "`define DEFAULT_internalStrobes      8'h00" 
								puts $FILE_OPENED "`define DEFAULT_externalDACSel       8'h00" 
								puts $FILE_OPENED "`define DEFAULT_monitorDACSel        8'h00"
								puts $FILE_OPENED "`define DEFAULT_matrixConfig         8'h08"
								puts $FILE_OPENED "`define DEFAULT_configCtrl           8'h00"
								puts $FILE_OPENED "`define DEFAULT_configData7downto0   8'h00"
								puts $FILE_OPENED "`define DEFAULT_configData15downto8  8'h00"
								puts $FILE_OPENED "`define DEFAULT_readoutCtrl          8'h00"
								puts $FILE_OPENED "`define DEFAULT_tpulseCtrl7downto0   8'hFF"
								puts $FILE_OPENED "`define DEFAULT_tpulseCtrl15downto8  8'hFF"
								puts $FILE_OPENED "`define DEFAULT_VBIASResetTransistor 8'h03"
								puts $FILE_OPENED "`define DEFAULT_VRESET               8'h68"
								puts $FILE_OPENED "`define DEFAULT_VBIASLevelShift      8'h09"
								puts $FILE_OPENED "`define DEFAULT_VANALOG17downto0     8'h00"
								puts $FILE_OPENED "`define DEFAULT_VANALOG115downto8    8'h00"
								puts $FILE_OPENED "`define DEFAULT_VANALOG2             8'h00"
								puts $FILE_OPENED "`define DEFAULT_VBIASPREAMPN         8'h05"
								puts $FILE_OPENED "`define DEFAULT_VNCASC               8'h5B"
								puts $FILE_OPENED "`define DEFAULT_VPCASC               8'h82"
								puts $FILE_OPENED "`define DEFAULT_VFBK                 8'h47"
								puts $FILE_OPENED "`define DEFAULT_VBIASIKRUM           8'h04"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCN           8'h08"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCP           8'h09"
								puts $FILE_OPENED "`define DEFAULT_VBIASDAC             8'h07"
								puts $FILE_OPENED "`define DEFAULT_VTHRESHOLD7downto0   8'h9A"
								puts $FILE_OPENED "`define DEFAULT_VTHRESHOLD9downto8   8'h00"
 							puts $FILE_OPENED "`define DEFAULT_VNCASCCOMP           8'h76"
								puts $FILE_OPENED "`define DEFAULT_VBIASLevelShiftstby  8'h01"
								puts $FILE_OPENED "`define DEFAULT_VBIASPREAMPNstby     8'h01"
								puts $FILE_OPENED "`define DEFAULT_VBIASDISCNstby       8'h01"
 							puts $FILE_OPENED "`define DEFAULT_VBIASDISCPstby       8'h01"
 							puts $FILE_OPENED "`define DEFAULT_VBIASDACstby         8'h01"
								puts $FILE_OPENED "`define DEFAULT_VBIASSlowBuffer      8'h0F"
								puts $FILE_OPENED "`define DEFAULT_AdjustDACRange       8'h03"
								puts $FILE_OPENED "`define DEFAULT_VLVDSD               8'h0F"
				}
				
				
	

    # Close the file
    close $FILE_OPENED

}




############################################################################
# Instantiate the associative arrays in CLICTD_test that contain the       #
# default values and the addresses of the slow control registers           # 
############################################################################
proc ::actions::write_file_with_assocarray_regs_to_include_in_clictdtest { PATH_TO_DUT_FOLDER PATH_OLD PATH_20181220 PATH_20190120 PATH_20190130 } {

    set FILE_TO_WRITE ../CLICTD_instantiation_assocarray_defaultNaddr_regs.sv
				
				# Open the file for writing
    set FILE_OPENED [open $FILE_TO_WRITE w]
				
				
				if { [string equal $PATH_TO_DUT_FOLDER $PATH_OLD] } { 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_globalConfig        \] = `DEFAULT_globalConfig;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_internalStrobes     \] = `DEFAULT_internalStrobes;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_analogInMux         \] = `DEFAULT_analogInMux;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_analogOutMux        \] = `DEFAULT_analogOutMux;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_matrixConfig        \] = `DEFAULT_matrixConfig;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_configCtrl          \] = `DEFAULT_configCtrl;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_configData7downto0  \] = `DEFAULT_configData7downto0;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_configData15downto8 \] = `DEFAULT_configData15downto8;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_readoutCtrl         \] = `DEFAULT_readoutCtrl;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_lvdsCtrl            \] = `DEFAULT_lvdsCtrl;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_tpulseCtrl7downto0  \] = `DEFAULT_tpulseCtrl7downto0;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_tpulseCtrl15downto8 \] = `DEFAULT_tpulseCtrl15downto8;"
        puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASResetTransistor\] = `DEFAULT_VBIASResetTransistor;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VRESET              \] = `DEFAULT_VRESET;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASLevelShift     \] = `DEFAULT_VBIASLevelShift;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VANALOG1            \] = `DEFAULT_VANALOG1;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VANALOG2            \] = `DEFAULT_VANALOG2;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASPREAMPN        \] = `DEFAULT_VANALOG2;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VNCASC              \] = `DEFAULT_VNCASC;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VPCASC              \] = `DEFAULT_VPCASC;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VFBK                \] = `DEFAULT_VFBK;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASIKRUM          \] = `DEFAULT_VBIASIKRUM;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDISCN          \] = `DEFAULT_VBIASDISCN;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDISCP          \] = `DEFAULT_VBIASDISCP;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDAC            \] = `DEFAULT_VBIASDAC;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VTHRESHOLD7downto0  \] = `DEFAULT_VTHRESHOLD7downto0;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VTHRESHOLD9downto8  \] = `DEFAULT_VTHRESHOLD9downto8;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VNCASCCOMP          \] = `DEFAULT_VNCASCCOMP;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASLevelShift_OFF \] = `DEFAULT_VBIASLevelShift_OFF;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASPREAMPN_OFF    \] = `DEFAULT_VBIASPREAMPN_OFF;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDISCN_OFF      \] = `DEFAULT_VBIASDISCN_OFF;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDISCP_OFF      \] = `DEFAULT_VBIASDISCP_OFF;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDAC_OFF        \] = `DEFAULT_VBIASDAC_OFF;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VLVDSD              \] = `DEFAULT_VLVDSD;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VLVDSR              \] = `DEFAULT_VLVDSR;"
								
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_globalConfig        \] = `ADDRESS_globalConfig;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_internalStrobes     \] = `ADDRESS_internalStrobes;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_analogInMux         \] = `ADDRESS_analogInMux;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_analogOutMux        \] = `ADDRESS_analogOutMux;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_matrixConfig        \] = `ADDRESS_matrixConfig;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_configCtrl          \] = `ADDRESS_configCtrl;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_configData7downto0  \] = `ADDRESS_configData7downto0;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_configData15downto8 \] = `ADDRESS_configData15downto8;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_readoutCtrl         \] = `ADDRESS_readoutCtrl;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_lvdsCtrl            \] = `ADDRESS_lvdsCtrl;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_tpulseCtrl7downto0  \] = `ADDRESS_tpulseCtrl7downto0;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_tpulseCtrl15downto8 \] = `ADDRESS_tpulseCtrl15downto8;"
        puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASResetTransistor\] = `ADDRESS_VBIASResetTransistor;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VRESET              \] = `ADDRESS_VRESET;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASLevelShift     \] = `ADDRESS_VBIASLevelShift;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VANALOG1            \] = `ADDRESS_VANALOG1;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VANALOG2            \] = `ADDRESS_VANALOG2;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASPREAMPN        \] = `ADDRESS_VBIASPREAMPN;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VNCASC              \] = `ADDRESS_VNCASC;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VPCASC              \] = `ADDRESS_VPCASC;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VFBK                \] = `ADDRESS_VFBK;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASIKRUM          \] = `ADDRESS_VBIASIKRUM;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDISCN          \] = `ADDRESS_VBIASDISCN;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDISCP          \] = `ADDRESS_VBIASDISCP;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDAC            \] = `ADDRESS_VBIASDAC;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VTHRESHOLD7downto0  \] = `ADDRESS_VTHRESHOLD7downto0;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VTHRESHOLD9downto8  \] = `ADDRESS_VTHRESHOLD9downto8;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VNCASCCOMP          \] = `ADDRESS_VNCASCCOMP;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASLevelShift_OFF \] = `ADDRESS_VBIASLevelShift_OFF;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASPREAMPN_OFF    \] = `ADDRESS_VBIASPREAMPN_OFF;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDISCN_OFF      \] = `ADDRESS_VBIASDISCN_OFF;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDISCP_OFF      \] = `ADDRESS_VBIASDISCP_OFF;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDAC_OFF        \] = `ADDRESS_VBIASDAC_OFF;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VLVDSD              \] = `ADDRESS_VLVDSD;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VLVDSR              \] = `ADDRESS_VLVDSR;"
								
    } elseif { [string equal $PATH_TO_DUT_FOLDER $PATH_20181220] || [string equal $PATH_TO_DUT_FOLDER $PATH_20190120] || [string equal $PATH_TO_DUT_FOLDER $PATH_20190130] } {
				    
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_globalConfig         \] = `DEFAULT_globalConfig;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_internalStrobes      \] = `DEFAULT_internalStrobes;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_externalDACSel       \] = `DEFAULT_externalDACSel;" 
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_monitorDACSel        \] = `DEFAULT_monitorDACSel;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_matrixConfig         \] = `DEFAULT_matrixConfig;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_configCtrl           \] = `DEFAULT_configCtrl;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_configData7downto0   \] = `DEFAULT_configData7downto0;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_configData15downto8  \] = `DEFAULT_configData15downto8;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_readoutCtrl          \] = `DEFAULT_readoutCtrl;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_tpulseCtrl7downto0   \] = `DEFAULT_tpulseCtrl7downto0;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_tpulseCtrl15downto8  \] = `DEFAULT_tpulseCtrl15downto8;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASResetTransistor \] = `DEFAULT_VBIASResetTransistor;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VRESET               \] = `DEFAULT_VRESET;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASLevelShift      \] = `DEFAULT_VBIASLevelShift;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VANALOG17downto0     \] = `DEFAULT_VANALOG17downto0;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VANALOG115downto8    \] = `DEFAULT_VANALOG115downto8;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VANALOG2             \] = `DEFAULT_VANALOG2;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASPREAMPN         \] = `DEFAULT_VBIASPREAMPN;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VNCASC               \] = `DEFAULT_VNCASC;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VPCASC               \] = `DEFAULT_VPCASC;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VFBK                 \] = `DEFAULT_VFBK;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASIKRUM           \] = `DEFAULT_VBIASIKRUM;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDISCN           \] = `DEFAULT_VBIASDISCN;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDISCP           \] = `DEFAULT_VBIASDISCP;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDAC             \] = `DEFAULT_VBIASDAC;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VTHRESHOLD7downto0   \] = `DEFAULT_VTHRESHOLD7downto0;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VTHRESHOLD9downto8   \] = `DEFAULT_VTHRESHOLD9downto8;"
 							puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VNCASCCOMP           \] = `DEFAULT_VNCASCCOMP;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASLevelShiftstby  \] = `DEFAULT_VBIASLevelShiftstby;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASPREAMPNstby     \] = `DEFAULT_VBIASPREAMPNstby;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDISCNstby       \] = `DEFAULT_VBIASDISCNstby;"
 							puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDISCPstby       \] = `DEFAULT_VBIASDISCPstby;"
 							puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASDACstby         \] = `DEFAULT_VBIASDACstby;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VBIASSlowBuffer      \] = `DEFAULT_VBIASSlowBuffer;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_AdjustDACRange       \] = `DEFAULT_AdjustDACRange;"
								puts $FILE_OPENED "default_values_registers_slowcontrol \[`ADDRESS_VLVDSD               \] = `DEFAULT_VLVDSD;"
								
								
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_globalConfig         \] = `ADDRESS_globalConfig;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_internalStrobes      \] = `ADDRESS_internalStrobes;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_externalDACSel       \] = `ADDRESS_externalDACSel;" 
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_monitorDACSel        \] = `ADDRESS_monitorDACSel;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_matrixConfig         \] = `ADDRESS_matrixConfig;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_configCtrl           \] = `ADDRESS_configCtrl;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_configData7downto0   \] = `ADDRESS_configData7downto0;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_configData15downto8  \] = `ADDRESS_configData15downto8;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_readoutCtrl          \] = `ADDRESS_readoutCtrl;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_tpulseCtrl7downto0   \] = `ADDRESS_tpulseCtrl7downto0;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_tpulseCtrl15downto8  \] = `ADDRESS_tpulseCtrl15downto8;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASResetTransistor \] = `ADDRESS_VBIASResetTransistor;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VRESET               \] = `ADDRESS_VRESET;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASLevelShift      \] = `ADDRESS_VBIASLevelShift;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VANALOG17downto0     \] = `ADDRESS_VANALOG17downto0;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VANALOG115downto8    \] = `ADDRESS_VANALOG115downto8;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VANALOG2             \] = `ADDRESS_VANALOG2;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASPREAMPN         \] = `ADDRESS_VBIASPREAMPN;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VNCASC               \] = `ADDRESS_VNCASC;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VPCASC               \] = `ADDRESS_VPCASC;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VFBK                 \] = `ADDRESS_VFBK;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASIKRUM           \] = `ADDRESS_VBIASIKRUM;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDISCN           \] = `ADDRESS_VBIASDISCN;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDISCP           \] = `ADDRESS_VBIASDISCP;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDAC             \] = `ADDRESS_VBIASDAC;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VTHRESHOLD7downto0   \] = `ADDRESS_VTHRESHOLD7downto0;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VTHRESHOLD9downto8   \] = `ADDRESS_VTHRESHOLD9downto8;"
 							puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VNCASCCOMP           \] = `ADDRESS_VNCASCCOMP;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASLevelShiftstby  \] = `ADDRESS_VBIASLevelShiftstby;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASPREAMPNstby     \] = `ADDRESS_VBIASPREAMPNstby;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDISCNstby       \] = `ADDRESS_VBIASDISCNstby;"
 							puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDISCPstby       \] = `ADDRESS_VBIASDISCPstby;"
 							puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASDACstby         \] = `ADDRESS_VBIASDACstby;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VBIASSlowBuffer      \] = `ADDRESS_VBIASSlowBuffer;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_AdjustDACRange       \] = `ADDRESS_AdjustDACRange;"
								puts $FILE_OPENED "addresses_registers_slowcontrol \[`ADDRESS_VLVDSD               \] = `ADDRESS_VLVDSD;"
								
				}
				
				
	

    # Close the file
    close $FILE_OPENED

}



############################################################################
# Instantiate the associative arrays in CLICTD_test that contain the       #
# default values and the addresses of the slow control registers           # 
############################################################################
proc ::actions::write_file_with_dut_ports_to_include_in_dutwrapperpnr { PATH_TO_DUT_FOLDER PATH_OLD PATH_20181220 PATH_20190120 PATH_20190130 } {

    set FILE_TO_WRITE ../CLICTD_instantiation_dut_in_dutwrapperpnr.sv
				
				# Open the file for writing
    set FILE_OPENED [open $FILE_TO_WRITE w]
				
				
				if { [string equal $PATH_TO_DUT_FOLDER $PATH_OLD] } { 
								puts $FILE_OPENED "clictdTop dut"
								puts $FILE_OPENED "("
								puts $FILE_OPENED "	///////////////////////////"
								puts $FILE_OPENED "	// These pins have changed with respect to clictdTop in order to use a unidirectional"
								puts $FILE_OPENED "	// I2C master"
								puts $FILE_OPENED "	//"
								puts $FILE_OPENED "	.SDAin(slowcontrol_if.SDAin),      		// SDA pin"
								puts $FILE_OPENED "	//.SDAin(slowcontrol_if.SDA),      		// SDA pin"
								puts $FILE_OPENED "	.SDAout(slowcontrol_if.SDAout),"
								puts $FILE_OPENED "	.SDAen(slowcontrol_if.SDAen),"
								puts $FILE_OPENED "	///////////////////////////"
								puts $FILE_OPENED "	.SCL_pad(slowcontrol_if.SCL_pad),     		// SCL pin"
								puts $FILE_OPENED "	//.ADDR_pad(slowcontrol_if.ADDR_pad), 	    // i2c address least significant bits"
								puts $FILE_OPENED "	.ANALOG_IN_pad(),"
								puts $FILE_OPENED "	.REF_pad(),"
								puts $FILE_OPENED "	.ANALOG_OUT_pad(),"
								puts $FILE_OPENED "	.MONITOR_pad(),		// to be revised"
								puts $FILE_OPENED "	.PWREN_pad(pins_if.PWREN_pad),      	// power pulsing input"
								puts $FILE_OPENED "	.TPULSE_pad(pins_if.TPULSE_pad),    	// external test pulse input"
								puts $FILE_OPENED "	.READOUT_pad(pins_if.READOUT_pad),		// external pin for issuing readout"
								puts $FILE_OPENED "	.SHUTTER_pad(pins_if.SHUTTER_pad),		// shutter signal input"
								puts $FILE_OPENED "	.RSTN_pad(pins_if.RSTN_pad),    		// reset signal (active low)"
								puts $FILE_OPENED "	.CLK_100_n_pad(pins_if.CLK_100_n_pad & pins_if.enable_clock_100MHz),		// 100 MHz clock"
								puts $FILE_OPENED "	.CLK_100_p_pad(pins_if.CLK_100_p_pad & pins_if.enable_clock_100MHz),"
								puts $FILE_OPENED "	.CLK_40_n_pad(pins_if.CLK_40_n_pad),		// 40 MHz clock"
								puts $FILE_OPENED "	.CLK_40_p_pad(pins_if.CLK_40_p_pad),"
								puts $FILE_OPENED "	.DATA_OUT_n_pad(), 	// data output signal"
								puts $FILE_OPENED "	.DATA_OUT_p_pad(readout_if.DATA_OUT),"
								puts $FILE_OPENED "	.CLK_OUT_n_pad(), 	// clock output signal"
								puts $FILE_OPENED "	.CLK_OUT_p_pad(readout_if.CLK_OUT),"
								puts $FILE_OPENED "	.ENABLE_OUT_p_pad(pins_if.ENABLE_OUT_pad), 	// enable output signal (to indicate when data starts coming out of the chip)"
								puts $FILE_OPENED "	.ENABLE_OUT_n_pad(),"
								puts $FILE_OPENED "	.VDDA(), 			// analog power supply"
								puts $FILE_OPENED "	.VDD(),			// digital power supply"
								puts $FILE_OPENED "	.VSSA(),			// analog ground"
								puts $FILE_OPENED "	.VSS(),			// digital ground"
								puts $FILE_OPENED "	.PWELL(),			// p-well bias"
								puts $FILE_OPENED "	.SUB()			// substrate bias"
					   puts $FILE_OPENED ");"
        
								
    } elseif { [string equal $PATH_TO_DUT_FOLDER $PATH_20181220] || [string equal $PATH_TO_DUT_FOLDER $PATH_20190120] || [string equal $PATH_TO_DUT_FOLDER $PATH_20190130] } {
				    
								puts $FILE_OPENED "clictdTop dut"
								puts $FILE_OPENED "("
								puts $FILE_OPENED "	///////////////////////////"
								puts $FILE_OPENED "	// These pins have changed with respect to clictdTop in order to use a unidirectional"
								puts $FILE_OPENED "	// I2C master"
								puts $FILE_OPENED "	//"
								puts $FILE_OPENED "	.SDAin(slowcontrol_if.SDAin),      		// SDA pin"
								puts $FILE_OPENED "	//.SDAin(slowcontrol_if.SDA),      		// SDA pin"
								puts $FILE_OPENED "	.SDAout(slowcontrol_if.SDAout),"
								puts $FILE_OPENED "	.SDAen(slowcontrol_if.SDAen),"
								puts $FILE_OPENED "	///////////////////////////"
								puts $FILE_OPENED "	.SCL_pad(slowcontrol_if.SCL_pad),     		// SCL pin"
								puts $FILE_OPENED "	//.ADDR_pad(slowcontrol_if.ADDR_pad), 	    // i2c address least significant bits"
								puts $FILE_OPENED "	.ANALOG_IN_pad(),"
								puts $FILE_OPENED "	.REF_pad(),"
								puts $FILE_OPENED "	.ANALOG_OUT_pad(),"
								puts $FILE_OPENED "	.PWREN_pad(pins_if.PWREN_pad),      	// power pulsing input"
								puts $FILE_OPENED "	.TPULSE_pad(pins_if.TPULSE_pad),    	// external test pulse input"
								puts $FILE_OPENED "	.READOUT_pad(pins_if.READOUT_pad),		// external pin for issuing readout"
								puts $FILE_OPENED "	.SHUTTER_pad(pins_if.SHUTTER_pad),		// shutter signal input"
								puts $FILE_OPENED "	.RSTN_pad(pins_if.RSTN_pad),    		// reset signal (active low)"
								puts $FILE_OPENED "	.CLK_100_n_pad(pins_if.CLK_100_n_pad & pins_if.enable_clock_100MHz),		// 100 MHz clock"
								puts $FILE_OPENED "	.CLK_100_p_pad(pins_if.CLK_100_p_pad & pins_if.enable_clock_100MHz),"
								puts $FILE_OPENED "	.CLK_40_n_pad(pins_if.CLK_40_n_pad),		// 40 MHz clock"
								puts $FILE_OPENED "	.CLK_40_p_pad(pins_if.CLK_40_p_pad),"
								puts $FILE_OPENED "	.DATA_OUT_n_pad(), 	// data output signal"
								puts $FILE_OPENED "	.DATA_OUT_p_pad(readout_if.DATA_OUT),"
								puts $FILE_OPENED "	.CLK_OUT_n_pad(), 	// clock output signal"
								puts $FILE_OPENED "	.CLK_OUT_p_pad(readout_if.CLK_OUT),"
								puts $FILE_OPENED "	.ENABLE_OUT_p_pad(pins_if.ENABLE_OUT_pad), 	// enable output signal (to indicate when data starts coming out of the chip)"
								puts $FILE_OPENED "	.ENABLE_OUT_n_pad(),"
								puts $FILE_OPENED "	.VDDA(), 			// analog power supply"
								puts $FILE_OPENED "	.VDD(),			// digital power supply"
								puts $FILE_OPENED "	.VSSA(),			// analog ground"
								puts $FILE_OPENED "	.VSS(),			// digital ground"
								puts $FILE_OPENED "	.PWELL(),			// p-well bias"
								puts $FILE_OPENED "	.SUB()			// substrate bias"
					   puts $FILE_OPENED ");"
								
								
								
								
								
				}
				
				
	

    # Close the file
    close $FILE_OPENED

}




############################################################################
# Generate the verilog code to interconnect the interface signals to the   #
# postlayout signals and the code to replace some columns of the postlayout#
# netlist by their RTL version                                             #
############################################################################

proc ::actions::generate_verilog_to_include {TIMESCALE_ARG INCLUDE_DIRS} {

  if { $TIMESCALE_ARG == "" } { 
		    set TIMESCALE $::DEFAULT_TIMESCALE
		} else { 
		    set TIMESCALE $TIMESCALE_ARG
		}
		
		
		
  # Compile and run the codes to generate the verilog files in which: 
		# a) The configuration signals (tpEnableDigital, mask, tpEnableAnalog, tuningDAC_X) are assigned to the pins interface signals  
		# b) The columns to instantiate in the top level post layout netlist are randomized to be either RTL or post layout column netlist. 
		#    Then the columns are instantiated according to the results of the randomization. 
		# The generated verilog files will then be included in the CLICTD_dut_wrapper_pnr and post layout top level netlist respectively.
	 set FILES_TO_COMPILE_BEFORE ../environment/tasks_and_functions/CLICTD_generate_verilog_to_include.sv
		set TOP_MODULE_BEFORE CLICTD_generate_verilog_to_include
		actions::compile $TIMESCALE $INCLUDE_DIRS $FILES_TO_COMPILE_BEFORE
		actions::sim "" $TOP_MODULE_BEFORE "" "" "" ""
}



############################################################################
# Generate a file which is included in CLICTD_generate_verilog_to_include  #
# that contains the value of column_is_rtl (which columns are RTL and which#
# are postlayout) to be used for the simulation
############################################################################
proc ::actions::update_column_is_rtl { COLUMN_IS_RTL } {

				
				set FILE_TO_WRITE ../which_column_is_rtl.sv
				
				# Open the file for writing
    set FILE_OPENED [open $FILE_TO_WRITE w]
				
				# Write the value into the file		
				#set line [string trim "column_is_rtl = "][string trim $COLUMN_IS_RTL][string trim ";"]
				set line [concat "column_is_rtl = " $COLUMN_IS_RTL ";"]
				#echo $line
				puts $FILE_OPENED $line

    # Close the file
    close $FILE_OPENED
				
  
} 



############################################################################
# Update all the files that depend on the netlist version and on the value #
# of column_is_rtl                                                         #
############################################################################
proc ::actions::update_netlist_for_simulation { PATH_TO_DUT_FOLDER CURRENT_PATH QUESTA_DESIGN_LIB QUESTA_DESIGN_LIB_DEFAULT } {

				set INCLUDE_DIRS [actions::build_include_dirs $PATH_TO_DUT_FOLDER]	

				set FILE_WITH_SIMULATION_COMMANDS [string trim $CURRENT_PATH][string trim "/generate_verilog_to_include.tcl"]

				# Write parameters to call generate_verilog_to_include
				# Open the file for writing
				set FILE_TO_OPEN [string trim $CURRENT_PATH][string trim "/parameters_for_generate_verilog_to_include.tcl"]
				set FILE_OPENED [open $FILE_TO_OPEN w]
				# Write the content into the file
				puts $FILE_OPENED "# Parameters for simulation, sourced in generate_verilog_to_include.tcl"
				puts $FILE_OPENED "set INCLUDE_DIRS [string trim "\""][string trim $INCLUDE_DIRS][string trim "\""]"
				puts $FILE_OPENED "set QUESTA_DESIGN_LIB [string trim "\""][string trim $QUESTA_DESIGN_LIB][string trim "\""]" 
				puts $FILE_OPENED "set QUESTA_DESIGN_LIB_DEFAULT [string trim "\""][string trim $QUESTA_DESIGN_LIB_DEFAULT][string trim "\""]"
				# Close the file
				close $FILE_OPENED
				# Update netlists for simulation
				exec vsim -c -do $FILE_WITH_SIMULATION_COMMANDS 									
				
				
  
} 



############################################################################
# Select which action to perform according to the selected option          #
############################################################################

proc ::actions::perform_action { ACTION_TO_PERFORM QUESTA_DESIGN_LIB_ARG QUESTA_DESIGN_LIB_DEFAULT_ARG TIMESCALE_ARG INCLUDE_DIRS FILES_TO_COMPILE BACK_ANNOTATED_ARG DESIGN TOP_MODULE UVM_TESTNAME QUESTA_RUN_OPTIONS WAVE_FILE SIMULATION_TIME_ARG SDF_CORNER_ARG WRITE_TRANSCRIPT_TO_TXT_ARG NUMBER_OF_SIMULATIONS_ARG PATH_TO_DUT_FOLDER PATH_OLD PATH_20181220 PATH_20190120 PATH_20190130 YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE } {

 if { $QUESTA_DESIGN_LIB_ARG == "" } { set QUESTA_DESIGN_LIB $::DEFAULT_QUESTA_DESIGN_LIB
	} else { set QUESTA_DESIGN_LIB $QUESTA_DESIGN_LIB_ARG
	} 

	if { $QUESTA_DESIGN_LIB_DEFAULT_ARG == "" } { set QUESTA_DESIGN_LIB_DEFAULT $::DEFAULT_QUESTA_DESIGN_LIB_DEFAULT
	} else { set QUESTA_DESIGN_LIB_DEFAULT $QUESTA_DESIGN_LIB_DEFAULT_ARG
	} 

 if { $TIMESCALE_ARG == "" } { set TIMESCALE $::DEFAULT_TIMESCALE
	} else { set TIMESCALE $TIMESCALE_ARG
	}
		
 if { $SIMULATION_TIME_ARG == "" } { set SIMULATION_TIME $::DEFAULT_SIMULATION_TIME
	} else { set SIMULATION_TIME $SIMULATION_TIME_ARG
	}
	
	if { $BACK_ANNOTATED_ARG == "" } { set BACK_ANNOTATED $::DEFAULT_BACK_ANNOTATED
	} else { set BACK_ANNOTATED $BACK_ANNOTATED_ARG
	}
	
	if { $SDF_CORNER_ARG == "" } { set SDF_CORNER $::DEFAULT_SDF_CORNER
	} else { set SDF_CORNER $SDF_CORNER_ARG
	}

 if { $WRITE_TRANSCRIPT_TO_TXT_ARG == "" } { set WRITE_TRANSCRIPT_TO_TXT $::DEFAULT_WRITE_TRANSCRIPT_TO_TXT
	} else { set WRITE_TRANSCRIPT_TO_TXT $WRITE_TRANSCRIPT_TO_TXT_ARG
	}
	
	if { $NUMBER_OF_SIMULATIONS_ARG == "" } { set NUMBER_OF_SIMULATIONS $::DEFAULT_NUMBER_OF_SIMULATIONS
	} else { set NUMBER_OF_SIMULATIONS $NUMBER_OF_SIMULATIONS_ARG
	}
	
	
		
	if { $ACTION_TO_PERFORM == "setup"   } { 
		actions::setup $QUESTA_DESIGN_LIB $QUESTA_DESIGN_LIB_DEFAULT  
		
	} elseif { $ACTION_TO_PERFORM == "compile_first_time" } { 
	 # First of all, compile and run the codes to generate the verilog files in which: 
		# a) The configuration signals (tpEnableDigital, mask, tpEnableAnalog, tuningDAC_X) are assigned to the pins interface signals  
		# b) The columns to instantiate in the top level post layout netlist are randomized to be either RTL or post layout column netlist. 
		#    Then the columns are instantiated according to the results of the randomization. 
		# The generated verilog files will then be included in the CLICTD_dut_wrapper_pnr and post layout top level netlist respectively.
	 #set FILES_TO_COMPILE_BEFORE "../environment/tasks_and_functions/CLICTD_generate_verilog_to_include.sv" 
		#set TOP_MODULE_BEFORE CLICTD_generate_verilog_to_include
		#actions::compile $TIMESCALE $INCLUDE_DIRS $FILES_TO_COMPILE_BEFORE
		#actions::sim "" $TOP_MODULE_BEFORE "" "" "" ""
		
		
		actions::write_file_with_dut_path_to_include_in_parametersfile $PATH_TO_DUT_FOLDER
		actions::write_file_with_registers_info_to_include_in_parametersfile $PATH_TO_DUT_FOLDER $PATH_OLD $PATH_20181220 $PATH_20190120 $PATH_20190130
		actions::write_file_with_assocarray_regs_to_include_in_clictdtest $PATH_TO_DUT_FOLDER $PATH_OLD $PATH_20181220 $PATH_20190120 $PATH_20190130
		actions::write_file_with_dut_ports_to_include_in_dutwrapperpnr $PATH_TO_DUT_FOLDER $PATH_OLD $PATH_20181220 $PATH_20190120 $PATH_20190130
		actions::generate_verilog_to_include $TIMESCALE $INCLUDE_DIRS
		
		
		# Compile the testbench files, among which some `include the verilog files generated formerly
		actions::compile $TIMESCALE $INCLUDE_DIRS $FILES_TO_COMPILE 
	
	} elseif { $ACTION_TO_PERFORM == "compile" } { 
	
		# Compile the testbench files, among which some `include the verilog files generated formerly
		actions::compile $TIMESCALE $INCLUDE_DIRS $FILES_TO_COMPILE 
		
		} elseif { $ACTION_TO_PERFORM == "compile_for_coverage" } { 
	
		# Compile the testbench files, among which some `include the verilog files generated formerly
		actions::compile_for_coverage $TIMESCALE $INCLUDE_DIRS $FILES_TO_COMPILE 
		
	} elseif { $ACTION_TO_PERFORM == "sim"     } { 
	   
	 		if { $BACK_ANNOTATED == "NO" } { 
				    actions::sim $DESIGN $TOP_MODULE $UVM_TESTNAME $QUESTA_RUN_OPTIONS $WAVE_FILE $SIMULATION_TIME 
				} else {
	
  								set SDF_ASSIGNMENTS_FILE ../sim_annotated/sdfAssignments
										#set RTL_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/netlistPostLayout
										set RTL_PATH ../sim_annotated
										
										
										set SDF_CORNER_AUX [string trim "-sdf"][string trim $SDF_CORNER]
			       set SDFPARAMS [generate_arguments_from_file $SDF_ASSIGNMENTS_FILE $SDF_CORNER_AUX RTL_PATH $RTL_PATH] 
		 	      lappend SDFPARAMS "-sdfnoerror" 
		       	set SDFPARAMS [join $SDFPARAMS]
		       	#lappend $QUESTA_RUN_OPTIONS {*}$SDFPARAMS
										
										set QUESTA_RUN_OPTIONS [concat $QUESTA_RUN_OPTIONS $SDFPARAMS]
										#echo "DEBUG ACTIONS.DO PERFORM_ACTION"
										#echo $QUESTA_RUN_OPTIONS
				      actions::sim $DESIGN $TOP_MODULE $UVM_TESTNAME $QUESTA_RUN_OPTIONS $WAVE_FILE $SIMULATION_TIME
				
				
				      if { $WRITE_TRANSCRIPT_TO_TXT == "YES" } {
				      				set systemTime [clock seconds]
				      				set TXT_LOG_FILENAME [string trim "$UVM_TESTNAME"][string trim "_"][string trim $SDF_CORNER][string trim "_"][clock format $systemTime -format %y-%m-%d_%H-%M-%S][string trim ".log"]
				      				#echo $TXT_LOG_FILENAME
														
														#if { $SDF_CORNER == "min" } {
														#    set TXT_LOG_PATH ../sim_annotated_min/logs/
														#} elseif { $SDF_CORNER == "typ" } {
														#    set TXT_LOG_PATH ../sim_annotated_typ/logs/
														#} else {
														#    set TXT_LOG_PATH ../sim_annotated_max/logs/
														#}
														
														set TXT_LOG_PATH ./logs/
														
														set TXT_LOG_FILENAME [string trim $TXT_LOG_PATH][string trim $TXT_LOG_FILENAME]
														
														#write transcript $TXT_LOG_FILENAME ; # this did not copy the whole transcript, it was cropped! http://people.cs.pitt.edu/~don/coe1502/Reference/vsim_quickref.pdf  (MTI_TF_LIMIT)
														file copy -force transcript $TXT_LOG_FILENAME
				      }
		
					}
		
		   
	} elseif { $ACTION_TO_PERFORM == "sim_for_coverage"     } { 
	   set FILENAME_STORE_COVERAGE_UCDB [actions::generate_filename_including_testnameCornerSystemtime $UVM_TESTNAME $SDF_CORNER]
	   
	 		if { $BACK_ANNOTATED == "NO" } { 
				    
								actions::sim_for_coverage $DESIGN $TOP_MODULE $UVM_TESTNAME $QUESTA_RUN_OPTIONS $WAVE_FILE $SIMULATION_TIME $FILENAME_STORE_COVERAGE_UCDB
								
				} else {
	
  								set SDF_ASSIGNMENTS_FILE ../sim_annotated/sdfAssignments
										#set RTL_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/netlistPostLayout
										set RTL_PATH ../sim_annotated
										
										
										set SDF_CORNER_AUX [string trim "-sdf"][string trim $SDF_CORNER]
			       set SDFPARAMS [generate_arguments_from_file $SDF_ASSIGNMENTS_FILE $SDF_CORNER_AUX RTL_PATH $RTL_PATH] 
		 	      lappend SDFPARAMS "-sdfnoerror" 
		       	set SDFPARAMS [join $SDFPARAMS]
		       	#lappend $QUESTA_RUN_OPTIONS {*}$SDFPARAMS
										
										set QUESTA_RUN_OPTIONS [concat $QUESTA_RUN_OPTIONS $SDFPARAMS]
										#echo "DEBUG ACTIONS.DO PERFORM_ACTION"
										#echo $QUESTA_RUN_OPTIONS
										actions::sim_for_coverage $DESIGN $TOP_MODULE $UVM_TESTNAME $QUESTA_RUN_OPTIONS $WAVE_FILE $SIMULATION_TIME $FILENAME_STORE_COVERAGE_UCDB
				
				
				      if { $WRITE_TRANSCRIPT_TO_TXT == "YES" } {
				      				set systemTime [clock seconds]
				      				set TXT_LOG_FILENAME [string trim "$UVM_TESTNAME"][string trim "_"][string trim $SDF_CORNER][string trim "_"][clock format $systemTime -format %y-%m-%d_%H-%M-%S][string trim ".log"]
				      				#echo $TXT_LOG_FILENAME
														
														#if { $SDF_CORNER == "min" } {
														#    set TXT_LOG_PATH ../sim_annotated_min/logs/
														#} elseif { $SDF_CORNER == "typ" } {
														#    set TXT_LOG_PATH ../sim_annotated_typ/logs/
														#} else {
														#    set TXT_LOG_PATH ../sim_annotated_max/logs/
														#}
														
														set TXT_LOG_PATH ./logs/
														
														set TXT_LOG_FILENAME [string trim $TXT_LOG_PATH][string trim $TXT_LOG_FILENAME]
														
														#write transcript $TXT_LOG_FILENAME ; # this did not copy the whole transcript, it was cropped! http://people.cs.pitt.edu/~don/coe1502/Reference/vsim_quickref.pdf  (MTI_TF_LIMIT)
														file copy -force transcript $TXT_LOG_FILENAME
				      }
		
					}
		
		   
	} elseif { $ACTION_TO_PERFORM == "merge_coverage"     } { 
			actions::merge_coverage_from_alltests $YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE
	
	} elseif { $ACTION_TO_PERFORM == "merge_testplan"     } { 
			actions::merge_coverage_with_testplan $YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE
	
	} elseif { $ACTION_TO_PERFORM == "multisim"     } { 
	  set SDF_ASSIGNMENTS_FILE ../sim_annotated/sdfAssignments
			#set RTL_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/netlistPostLayout
			set RTL_PATH ../sim_annotated
				
				
			set SDF_CORNER_AUX [string trim "-sdf"][string trim $SDF_CORNER]
			set SDFPARAMS [generate_arguments_from_file $SDF_ASSIGNMENTS_FILE $SDF_CORNER_AUX RTL_PATH $RTL_PATH] 
		 lappend SDFPARAMS "-sdfnoerror" 
		 set SDFPARAMS [join $SDFPARAMS]
		 lappend $QUESTA_RUN_OPTIONS {*}$SDFPARAMS
	  for {set i 0} {$i < $NUMBER_OF_SIMULATIONS} {incr i} {
			     if { $BACK_ANNOTATED == "NO" } { 
				        actions::sim $DESIGN $TOP_MODULE $UVM_TESTNAME $QUESTA_RUN_OPTIONS $WAVE_FILE $SIMULATION_TIME 
				    } else {
								    set aux [concat "MULTISIM - " $UVM_TESTNAME " CORNER " $SDF_CORNER " " ITERATION " $i]
								    echo "########################################################################################################################"
							     echo $aux
							     echo "########################################################################################################################"
							
								    
									  	actions::sim $DESIGN $TOP_MODULE $UVM_TESTNAME $QUESTA_RUN_OPTIONS $WAVE_FILE $SIMULATION_TIME
								}
								 
			}
	
	} elseif { $ACTION_TO_PERFORM == "all"     } { 
		actions::all $TIMESCALE $INCLUDE_DIRS $FILES_TO_COMPILE $DESIGN $TOP_MODULE $UVM_TESTNAME $QUESTA_RUN_OPTIONS $WAVE_FILE $SIMULATION_TIME
  } elseif { $ACTION_TO_PERFORM == "restart" } { 
    actions::restart_sim $TIMESCALE $INCLUDE_DIRS $FILES_TO_RECOMPILE $SIMULATION_TIME
    # before running this option, actions in "all" need to be performed!
	} elseif { $ACTION_TO_PERFORM == "clean"   } { 
		actions::clean $QUESTA_DESIGN_LIB_DEFAULT    
	}
}



############################################################################
# Format time measurement expressed in seconds into different fields so as #
# to be able to perform a difference of two time intervals broken down into#
# these fields                                                             #
############################################################################
# https://stackoverflow.com/questions/44366856/how-to-calculate-the-time-span-in-tcl-using-clock-commands
proc ::actions::formatTimeInterval {intervalSeconds} {
    # *Assume* that the interval is positive
    set s [expr {$intervalSeconds % 60}]
    set i [expr {$intervalSeconds / 60}]
    set m [expr {$i % 60}]
    set i [expr {$i / 60}]
    set h [expr {$i % 24}]
    set d [expr {$i / 24}]
    return [format "%+d:%02d:%02d:%02d" $d $h $m $s]
}


############################################################################
# Auxiliary function to generate a string containing UVM_TESTNAME, the     #
# corner to simulate and the current date so as to compose file names      #
############################################################################
proc ::actions::generate_filename_including_testnameCornerSystemtime { UVM_TESTNAME SDF_CORNER } {

     set systemTime [clock seconds]
     set RETURN_FILENAME [string trim "$UVM_TESTNAME"][string trim "_"][string trim $SDF_CORNER][string trim "_"][clock format $systemTime -format %y-%m-%d_%H-%M-%S]
     return $RETURN_FILENAME 
}
