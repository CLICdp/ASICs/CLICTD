# Filename:          launcher.do
# Author:            Núria Egidos 
# Created on:        7/06/2018
# Last modified on:  7/06/2018
# Project:           Doctoral student, verification of CLICTD
# Description:       This do macro is called from the Questasim (vsim) command line 
#                    to perform some actions related to UVM testbench simulation.
#                    A description of the available actions is provided below. 
# Derived from:      Adrian Fiergolski scripts in https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim
#										 http://www.cems.uwe.ac.uk/~a2-lenz/n-gunton/worksheets/modelsim_tcl.pdf 		
#										 https://users.ece.cmu.edu/~kbiswas/se_cmds.pdf
#										 http://www-g.eng.cam.ac.uk/mentor/mti/docs/ee_cmds.pdf
# Clock: https://www.tcl.tk/man/tcl8.5/tutorial/Tcl41.html
# Writing transcript to a file: https://forums.intel.com/s/question/0D50P00003yyO0JSAU/modelsim-redirect-output-stream-of-a-macro?language=en_US
# Proc: https://www.tek-tips.com/viewthread.cfm?qid=1514987, http://zetcode.com/lang/tcl/procedures/

# Note: this script assumes that the path to the current version of Questasim has already been added to the PATH environment 
# variable. You can check if this is the case by typing echo $PATH in the terminal.
# If this is not the case, you won't be able to run vsim (vsim & will not work). To add it, you can editing ~/.cshrc, just 
# add the following line after #<DECM_END>:
# setenv PATH "/eda/mentor/2017-18/RHELx86/QUESTA-CORE-PRIME_10.6c-1/questasim/bin:$PATH"
# Then close the terminal, restart gp, change to sim_questa and type: vsim &

# To use this script:
#    cd sim
#    vsim &
# In the vsim command line:
#    do launcher.do <action> [<testname>]
# <action> is the action (compile, simulate...) to perform and is translated to argument $1
# Some actions require to specify <testname>, which is the name of the test to simulate and is translated to argument $2 
# (see http://www.pldworld.com/_hdl/2/_ref/se_html/manual_html/c_vcmds74.html#11018789)

# Available actions and how to call them:
# setup    - don't specify <testname>     
#            Setup the design directory (only required when we start or restart the simulator)
#            Usage example: do launcher.do setup
# compile  - specify <testname> <design type>
#            Compile all files required for the specified test, to run a simulation next or 
#            to check for compilation errors for instance
#            Usage example: do launcher.do compile RESET_TEST rtl
#                           do launcher.do compile RESET_TEST pnr
#            The design type (rtl or pnr) corresponds to the netlist to be used for sim or multisim. 
#            It's an optional parameter (if no design type is specified, rtl is considered by default).
# sim      - specify <testname> <design type> <BACK_ANNOTATED> <corner> <TXT>
#            Run a simulation with previously compiled files (compile/all should be called before)
#            Usage example: do launcher.do sim RESET_TEST pnr BACK_ANNOTATED min TXT
#            Design type, BACK_ANNOTATED, corner and TXT are optional. When included, BACK_ANNOTATED means that simulation with .sdf back annotation
#            is performed. When included, TXT means that the transcript of the simulation is stored into a .txt file
#            BACK_ANNOTATED is only allowed if pnr has been chosen as design type. If no design type is specified, rtl is considered by default.
#            If pnr and BACK_ANNOTATED are used and no corner is specified, "typ" is considered by default.
# multisim - specify <testname> <number of simulations> <design type> <BACK_ANNOTATED> <corner> <TXT> 
#            Run a simulation with previously compiled files (compile/all should be called before)
#            Usage example: do launcher.do multisim RESET_TEST 3 pnr BACK_ANNOTATED max TXT
#            The number of simulations is required to be provided. The simulator will be run the specified number of times in a row
#            and the simulation results (messages printed on the command line) can be stored into a .txt file if TXT is used as an argument.
#            Design type, BACK_ANNOTATED, corner and TXT are optional. When included, BACK_ANNOTATED means that simulation with .sdf back annotation is performed. 
#            Corner specifies in which corner the simulation is annotated (min, max, typ). BACK_ANNOTATED is only allowed if pnr has been chosen as design type. 
#            The corner is only accepted if BACK_ANNOTATED is enabled. If no design type is specified, rtl is considered by default. If no corner is specified, typ is used.
# all      - specify <testname>
#            Compile all files corresponding to the specified test and start the simulation
#            Usage example: do launcher.do all RESET_TEST
# restart  - specify <testname>
#            Compile only the files specified with the variable FILES_TO_RECOMPILE and restart the simulation
#            (sim/all should be called before)
#            Usage example: do launcher.do restart RESET_TEST
# clean    - don't specify <testname>
#            Clean the design library (to avoid errors due to old-versioned files when we recompile)
#            Usage example: do launcher.do clean
# Note: to run two actions in a row, the first one must have finished (or have been
# interrupted) first

# Available testnames:
# RESET_TEST
# CONFIGURATION_TEST
# 


# Note: all commands executed are automatically saved in the transcript file, as well as the errors that may occur

# Debugging errors: For example, ModelSim may display the following error message:
# ** Error: (vsim-19) Failed to access library 'work' at "work".
#To find the cause of and resolution to this error, use the following verror command at the ModelSim command prompt:
#verror 19;

############################################################################
############# Clear and reopen the transcript file #########################
############################################################################
#
.main clear
transcript file ""
transcript file transcript
#file delete "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/rtl/generated_assign_internalconfigsignals_to_pinsif.v"
#
############################################################################
############# Import the possible actions to perform #######################
############################################################################
#
source {../sim_annotated/actions.do}
#
############################################################################
#################### Definitions (simulator) ###############################
############################################################################
#
# Questasim run options
set DESIGN    "rtl" ; # rtl or netlist
set SDF_CORNER "typ"
set VOPT      "" ; # optimization options
#set VOPT      "novopt" ; # optimization options
#set SDFTYP    ""
set VOPTARGS  ""
#set VSIM_PARAM "-classdebug -uvmcontrol=all -msgmode both -t ps -quiet"
set VSIM_PARAM "-t ps"

set WAVE_FILE ""
set TIMESCALE "1 ns / 1 ps" ; # timeunit / timeprecision

set ::QUESTA_RUN_OPTIONS {}
set ::QUESTA_RUN_OPTIONS [split $::VSIM_PARAM " "]
if { $::VOPT       != "" } { lappend ::QUESTA_RUN_OPTIONS "-$::VOPT"   }
#if { $::SDFTYP     != "" } { lappend ::QUESTA_RUN_OPTIONS {*}$::SDFTYP }
if { $::VOPTARGS   != "" } { lappend ::QUESTA_RUN_OPTIONS [concat "-VOPTARGS=" $::VOPTARGS ""] }
#set ::QUESTA_RUN_OPTIONS ""

set BACK_ANNOTATED "NO"
set WRITE_TRANSCRIPT_TO_TXT "NO"


#
# Name of the design library where ModelSim will store the compiled design 
# units (we will create it with vlib)
set QUESTA_DESIGN_LIB work
#
# Name of the default design library 
# (we will map it to QUESTA_DESIGN_LIB with vmap)
set QUESTA_DESIGN_LIB_DEFAULT work
#30

#
############################################################################
########## Indicate the path where the DUT is located ######################
############################################################################
#
set PATH_OLD /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout
set PATH_20181220 /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/ikremast/ALICEITS_OA/work_libs/ikremast/cds/CLICTD_DM/digital/netlistPostLayout_20181220
set PATH_20190120 /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/ikremast/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout_20190120
set PATH_20190130 /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/ikremast/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout_20190131
set PATH_TO_DUT_FOLDER $PATH_20190130

set CURRENT_PATH [pwd]
set PATH_TO_STORE_COVERAGE_DB [string trim $CURRENT_PATH][string trim "/coverage_db/temp"]



#
############################################################################
####### Translate the call arguments to UVM simulation parameters ##########
############################################################################

# list of available actions
set optionlist            { setup compile compile_first_time sim multisim all restart clean compile_for_coverage sim_for_coverage merge_coverage merge_testplan } ; 
# list of actions that don't require a testname
set optionlist_notestname { setup clean merge_coverage merge_testplan } ; 
# list of available testnames
set testlist              { RESET_TEST CONFIGURATION_TEST READOUT_TEST READOUT_TEST_FOR_COVERAGE READOUTCONTROL_TEST WRREGISTERS_TEST TESTPULSECONTROL_TEST POWERCONTROL_TEST RESET_SYNCHRONIZER_VIOLATIONS_TEST READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST} ; 




############################################################################
########### Translate the call arguments to simulation actions #############
############################################################################



proc assign_params {} {
     set temp [actions::assign_params $::DESIGN $::UVM_TESTNAME $::PATH_TO_DUT_FOLDER ""]
					set ::TOP_MODULE [lindex $temp 0]
					set ::WAVE_FILE [lindex $temp 1]
					set ::FILES_TO_COMPILE [lindex $temp 2]
					set ::FILES_TO_RECOMPILE [lindex $temp 3]
					set ::INCLUDE_DIRS [lindex $temp 4]
									
}				




proc perform_action {} {
     if {$::ACTION_TO_PERFORM == "merge_coverage" || $::ACTION_TO_PERFORM == "merge_testplan"} { 
					        ::actions::perform_action $::ACTION_TO_PERFORM $::QUESTA_DESIGN_LIB $::QUESTA_DESIGN_LIB_DEFAULT $::TIMESCALE $::INCLUDE_DIRS $::FILES_TO_COMPILE $::BACK_ANNOTATED $::DESIGN $::TOP_MODULE $::UVM_TESTNAME $::QUESTA_RUN_OPTIONS $::WAVE_FILE "" $::SDF_CORNER $::WRITE_TRANSCRIPT_TO_TXT "" $::PATH_TO_DUT_FOLDER $::PATH_OLD $::PATH_20181220 $::PATH_20190120 $::PATH_20190130 $::YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE
					} else {
             ::actions::perform_action $::ACTION_TO_PERFORM $::QUESTA_DESIGN_LIB $::QUESTA_DESIGN_LIB_DEFAULT $::TIMESCALE $::INCLUDE_DIRS $::FILES_TO_COMPILE $::BACK_ANNOTATED $::DESIGN $::TOP_MODULE $::UVM_TESTNAME $::QUESTA_RUN_OPTIONS $::WAVE_FILE "" $::SDF_CORNER $::WRITE_TRANSCRIPT_TO_TXT "" $::PATH_TO_DUT_FOLDER $::PATH_OLD $::PATH_20181220 $::PATH_20190120 $::PATH_20190130 ""
					}
}



# argc is a simulator state variable that contains the number of parameters passed to the macro
if       { $argc == 0 } { # No arguments provided

  echo "#########################################################"
	echo "Please specify an action (and testname if applicable) #"
	echo "#########################################################"

}  elseif { $argc == 1 } { # 1 argument provided (action)

	if { [lsearch $optionlist $1] < 0 } { # this action is not supported
				echo "######################"
				echo "Unsupported action #" 
				echo "######################"
	} elseif { [lsearch $optionlist_notestname $1] < 0 } { # the requested action requires a testname!
        echo "########################################################"
	      echo "Please call this option again, specifying a testname #" 
        echo "########################################################"
  } elseif { $1 == {merge_coverage} || $1 == {merge_testplan} } {
		      echo "######################################################################################"
	       echo "Please call this option again, specifying YYMMDD in the name of the files to merge #" 
        echo "######################################################################################"
		} else { 
        set ::ACTION_TO_PERFORM $1
			  perform_action  
  }

} elseif { $argc == 2 } { # 2 arguments provided (action and testname) 

	if { [lsearch $optionlist $1] < 0 } { # this action is not supported
				echo "######################"
				echo "Unsupported action #" 
				echo "######################"
	} elseif { $1 == {merge_coverage} || $1 == {merge_testplan} } {
		   set ::ACTION_TO_PERFORM $1
					set YYMMDD_TO_SELECT_FILES_TO_MERGE_COVERAGE $2 ; # for instance, 19-01-30
					perform_action
		
	} elseif { [lsearch $testlist $2] < 0 } { # this testname is not supported
	  		echo "########################"
				echo "Unsupported testname #" 
				echo "########################"
	} elseif { $1 == {multisim} } { # we should have at least 3 arguments (number of iterations, "BACK_ANNOTATED" to activate back annotation, "TXT" to save transcript into a text file) 
	      echo "################################################################################################################"
				echo "Please call this option again, specifying the number of iterations and - optionally - BACK_ANNOTATED and TXT #" 
				echo "################################################################################################################"
  
		} else { # supported action and testname
				set ::ACTION_TO_PERFORM $1
        switch $2 {
        		  RESET_TEST                                {set UVM_TESTNAME "CLICTD_reset_test"                               }
            CONFIGURATION_TEST                        {set UVM_TESTNAME "CLICTD_configuration_test"                       }
            READOUT_TEST                              {set UVM_TESTNAME "CLICTD_readout_test"                             }
												READOUT_TEST_FOR_COVERAGE                 {set UVM_TESTNAME "CLICTD_readout_test_for_coverage"                }
            READOUTCONTROL_TEST                       {set UVM_TESTNAME "CLICTD_readoutcontrol_test"                      }
            WRREGISTERS_TEST                          {set UVM_TESTNAME "CLICTD_wrregisters_test"                         }
            TESTPULSECONTROL_TEST                     {set UVM_TESTNAME "CLICTD_testpulsecontrol_test"                    }
            POWERCONTROL_TEST                         {set UVM_TESTNAME "CLICTD_powercontrol_test"                        }
												RESET_SYNCHRONIZER_VIOLATIONS_TEST        {set UVM_TESTNAME "CLICTD_reset_synchronizer_violations_test"       }
												READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST {set UVM_TESTNAME "CLICTD_readoutstart_synchronizer_violations_test"}
        }
      
			  if { [lsearch $optionlist_notestname $1] < 0 } { # this option requires parameters to be assigned
									assign_params
				}
        perform_action ; # execute the requested action
  
	}

} elseif { $argc == 3 } {
       if { [lsearch $optionlist $1] < 0 } { # this action is not supported
  									echo "######################"
											echo "Unsupported action #" 
											echo "######################"
								} elseif { [lsearch $testlist $2] < 0 } { # this testname is not supported
											echo "########################"
											echo "Unsupported testname #" 
											echo "########################"
								} elseif { $1 != {sim} && $1 != {multisim} && $1 != {sim_for_coverage} && $1 != {compile} && $1 != {compile_first_time} && $1 != {compile_for_coverage} } {
            echo "######################################"
												echo "Too many arguments for this action #" 
												echo "######################################"
       	} elseif { $1 == {compile} && $3 != "rtl" && $1 == {compile} && $3 != "pnr" || $1 == {compile_first_time} && $3 != "rtl" && $1 == {compile_first_time} && $3 != "pnr" || $1 == {compile_for_coverage} && $3 != "rtl" && $1 == {compile_for_coverage} && $3 != "pnr" || $1 == {sim} && $3 != "TXT" && $1 == {sim} && $3 != "pnr" || $1 == {multisim} && $3 == 0 || $1 == {multisim} && $3 > 10 || $1 == {sim_for_coverage} && $3 != "TXT" && $1 == {sim_for_coverage} && $3 != "pnr"} {
												echo "#########################"
												echo "Unsupported arguments #" 
												echo "#########################"
       	} elseif { $1 == {compile} || $1 == {compile_first_time} || $1 == {compile_for_coverage} || $1 == {sim} || $1 == {multisim} || $1 == {sim_for_coverage} } {
            set ::ACTION_TO_PERFORM $1
            switch $2 {
              		RESET_TEST                                {set UVM_TESTNAME "CLICTD_reset_test"                               }
            				CONFIGURATION_TEST                        {set UVM_TESTNAME "CLICTD_configuration_test"                       }
            				READOUT_TEST                              {set UVM_TESTNAME "CLICTD_readout_test"                             }
																READOUT_TEST_FOR_COVERAGE                 {set UVM_TESTNAME "CLICTD_readout_test_for_coverage"                }
            				READOUTCONTROL_TEST                       {set UVM_TESTNAME "CLICTD_readoutcontrol_test"                      }
            				WRREGISTERS_TEST                          {set UVM_TESTNAME "CLICTD_wrregisters_test"                         }
            				TESTPULSECONTROL_TEST                     {set UVM_TESTNAME "CLICTD_testpulsecontrol_test"                    }
            				POWERCONTROL_TEST                         {set UVM_TESTNAME "CLICTD_powercontrol_test"                        }
																RESET_SYNCHRONIZER_VIOLATIONS_TEST        {set UVM_TESTNAME "CLICTD_reset_synchronizer_violations_test"       }
																READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST {set UVM_TESTNAME "CLICTD_readoutstart_synchronizer_violations_test"}
						  				}
             if { $1 == {compile} || $1 == {compile_first_time} || $1 == {compile_for_coverage} } { 
                set ::DESIGN $3 
             } elseif { $1 == {sim} || $1 == {sim_for_coverage} } {
                if { $3 == "pnr" } { 
																    set ::DESIGN $3 
																} elseif { $3 == "TXT" } { 
																   set ::WRITE_TRANSCRIPT_TO_TXT "YES" 
																}
             } elseif { $1 == {multisim} } {
                set ::NUMBER_OF_SIMULATIONS $3
             }
             assign_params 
             perform_action ; # execute the requested action
       }
							
							
							
} elseif { $argc == 4 } {
    if { $1 != {sim} && $1 != {multisim} && $1 != {sim_for_coverage} } { # this action is not supported
       echo "######################################"
							echo "Too many arguments for this action #" 
							echo "######################################"
				} elseif { [lsearch $testlist $2] < 0 } { # this testname is not supported
							echo "########################"
							echo "Unsupported testname #" 
							echo "########################"
				} elseif { $1 == {sim} && $3 != "pnr" && $4 == "BACK_ANNOTATED" || $1 == {multisim} && $4 == "BACK_ANNOTATED" || $1 == {sim_for_coverage} && $3 != "pnr" && $4 == "BACK_ANNOTATED"  } {
							echo "##############################################################################################"
							echo "PNR netlist should be loaded to perform back annotated simulation, but rtl has been loaded #" 
							echo "##############################################################################################"
				} elseif { $1 == {multisim} && $3 == 0 || $1 == {multisim} && $3 > 10 } {
							echo "#####################################"
							echo "Unsupported number of simulations #" 
							echo "#####################################"
				} else {
				    set ::ACTION_TO_PERFORM $1
        switch $2 {
            RESET_TEST                                {set UVM_TESTNAME "CLICTD_reset_test"                               }
            CONFIGURATION_TEST                        {set UVM_TESTNAME "CLICTD_configuration_test"                       }
            READOUT_TEST                              {set UVM_TESTNAME "CLICTD_readout_test"                             }
												READOUT_TEST_FOR_COVERAGE                 {set UVM_TESTNAME "CLICTD_readout_test_for_coverage"                }
            READOUTCONTROL_TEST                       {set UVM_TESTNAME "CLICTD_readoutcontrol_test"                      }
            WRREGISTERS_TEST                          {set UVM_TESTNAME "CLICTD_wrregisters_test"                         }
            TESTPULSECONTROL_TEST                     {set UVM_TESTNAME "CLICTD_testpulsecontrol_test"                    }
            POWERCONTROL_TEST                         {set UVM_TESTNAME "CLICTD_powercontrol_test"                        }
												RESET_SYNCHRONIZER_VIOLATIONS_TEST        {set UVM_TESTNAME "CLICTD_reset_synchronizer_violations_test"       }
												READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST {set UVM_TESTNAME "CLICTD_readoutstart_synchronizer_violations_test"}
				     }
									if { $1 == {sim} || $1 == {sim_for_coverage} } { 
									   set ::DESIGN $3 
												if { $4 == "BACK_ANNOTATED" } {
												  set ::BACK_ANNOTATED "YES" 
												} elseif { $4 == "TXT" } {
												  set ::WRITE_TRANSCRIPT_TO_TXT "YES"
												}
									} elseif { $1 == {multisim} } {
									   set ::NUMBER_OF_SIMULATIONS $3
												if { $4 == "rtl" || $4 == "pnr" } {
												} elseif { $4 == "BACK_ANNOTATED" } {
												  set ::BACK_ANNOTATED "YES"
												} elseif { $4 == "TXT" } {
												  set ::WRITE_TRANSCRIPT_TO_TXT "YES"
												}
									}  
									assign_params 
         perform_action ; # execute the requested action
				}
				
				 
} elseif { $argc == 5 } {
    if { $1 != {sim} && $1 != {multisim} && $1 != {sim_for_coverage} } { # this action is not supported
       echo "######################################"
							echo "Too many arguments for this action #" 
							echo "######################################"
				} elseif { [lsearch $testlist $2] < 0 } { # this testname is not supported
							echo "########################"
							echo "Unsupported testname #" 
							echo "########################"
				} elseif { $1 == {sim} && $3 != "pnr" || $1 == {sim_for_coverage} && $3 != "pnr" || $1 == {multisim} && $4 == "rtl" && $5 == "BACK_ANNOTATED" } {
							echo "##############################################################################################"
							echo "PNR netlist should be loaded to perform back annotated simulation, but rtl has been loaded #" 
							echo "##############################################################################################"
				} elseif { $1 == {multisim} && $3 == 0 || $1 == {multisim} && $3 > 10 } {
							echo "#####################################"
							echo "Unsupported number of simulations #" 
							echo "#####################################"
    } elseif { $1 == {sim} && $4 != "TXT" && $4 != "BACK_ANNOTATED" && $4 != "min"  && $4 != "typ"  && $4 != "max" ||  $1 == {sim} && $5 != "TXT" && $5 != "BACK_ANNOTATED" && $5 != "min"  && $5 != "typ"  && $5 != "max" || $1 == {multisim} && $5 != "BACK_ANNOTATED" && $5 != "TXT" || $1 == {sim_for_coverage} && $4 != "TXT" && $4 != "BACK_ANNOTATED" && $4 != "min"  && $4 != "typ"  && $4 != "max" || $1 == {sim_for_coverage} && $5 != "TXT" && $5 != "BACK_ANNOTATED" && $5 != "min"  && $5 != "typ"  && $5 != "max" } {
							echo "#########################"
							echo "Unsupported arguments #" 
							echo "#########################"                      
    } else {
       set ::ACTION_TO_PERFORM $1
       switch $2 {
            RESET_TEST                                {set UVM_TESTNAME "CLICTD_reset_test"                               }
            CONFIGURATION_TEST                        {set UVM_TESTNAME "CLICTD_configuration_test"                       }
            READOUT_TEST                              {set UVM_TESTNAME "CLICTD_readout_test"                             }
												READOUT_TEST_FOR_COVERAGE                 {set UVM_TESTNAME "CLICTD_readout_test_for_coverage"                }
            READOUTCONTROL_TEST                       {set UVM_TESTNAME "CLICTD_readoutcontrol_test"                      }
            WRREGISTERS_TEST                          {set UVM_TESTNAME "CLICTD_wrregisters_test"                         }
            TESTPULSECONTROL_TEST                     {set UVM_TESTNAME "CLICTD_testpulsecontrol_test"                    }
            POWERCONTROL_TEST                         {set UVM_TESTNAME "CLICTD_powercontrol_test"                        }
												RESET_SYNCHRONIZER_VIOLATIONS_TEST        {set UVM_TESTNAME "CLICTD_reset_synchronizer_violations_test"       }
												READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST {set UVM_TESTNAME "CLICTD_readoutstart_synchronizer_violations_test"}
				    }
								
        if {$1 == {multisim} } { 
								    set ::NUMBER_OF_SIMULATIONS $3
												set ::DESIGN $4
												if { $5 == "BACK_ANNOTATED" } {
												   set ::BACK_ANNOTATED "YES"
												} elseif { $5 == "TXT" } { 
												   set ::WRITE_TRANSCRIPT_TO_TXT "YES"
												}
												
								} else { 
								    set ::DESIGN $3 
												set ::BACK_ANNOTATED "YES"
												if { $5 == "TXT" } {
												    set ::WRITE_TRANSCRIPT_TO_TXT "YES"
												} else {
												    set ::SDF_CORNER $5
												}
								}
								assign_params 
								perform_action ; # execute the requested action
             
    } 
				
} elseif { $argc == 6 || $argc == 7 } {
    if { $1 == {sim} && $argc == 7 } { # this action is not supported
       echo "######################################"
							echo "Too many arguments for this action #" 
							echo "######################################"
				} elseif { $1 != {sim} && $1 != {multisim} && $1 != {sim_for_coverage} } {
				   echo "######################################"
							echo "Too many arguments for this action #" 
							echo "######################################"
				} elseif { [lsearch $testlist $2] < 0 } { # this testname is not supported
							echo "########################"
							echo "Unsupported testname #" 
							echo "########################"
				} elseif { $1 == {sim} && $3 != "pnr" || $1 == {sim_for_coverage} && $3 != "pnr" || $1 == {multisim} && $4 == "rtl" && $5 == "BACK_ANNOTATED" } {
							echo "##############################################################################################"
							echo "PNR netlist should be loaded to perform back annotated simulation, but rtl has been loaded #" 
							echo "##############################################################################################"
				} elseif { $1 == {multisim} && $3 == 0 || $1 == {multisim} && $3 > 10 } {
							echo "#####################################"
							echo "Unsupported number of simulations #" 
							echo "#####################################
    } elseif { $1 == {sim} && $4 != "TXT" && $4 != "BACK_ANNOTATED" && $4 != "min"  && $4 != "typ"  && $4 != "max" || $1 == {sim} && $5 != "TXT" && $5 != "BACK_ANNOTATED" && $5 != "min"  && $5 != "typ"  && $5 != "max" || $1 == {sim} && $6 != "TXT" && $6 != "BACK_ANNOTATED" && $6 != "min"  && $6 != "typ"  && $6 != "max" || $1 == {multisim} && $5 != "TXT" && $5 != "BACK_ANNOTATED" && $5 != "min"  && $5 != "typ"  && $5 != "max" || $1 == {multisim} && $6 != "TXT" && $6 != "BACK_ANNOTATED" && $6 != "min"  && $6 != "typ"  && $6 != "max" || $argc == 7 && $1 == {multisim} && $7 != "TXT" && $7 != "BACK_ANNOTATED" && $7 != "min"  && $7 != "typ"  && $7 != "max" || $1 == {sim_for_coverage} && $4 != "TXT" && $4 != "BACK_ANNOTATED" && $4 != "min"  && $4 != "typ"  && $4 != "max" || $1 == {sim_for_coverage} && $5 != "TXT" && $5 != "BACK_ANNOTATED" && $5 != "min"  && $5 != "typ"  && $5 != "max" || $1 == {sim_for_coverage} && $6 != "TXT" && $6 != "BACK_ANNOTATED" && $6 != "min"  && $6 != "typ"  && $6 != "max" } {
							echo "#########################"
							echo "Unsupported arguments #" 
							echo "#########################"   
    } else {
       set ::ACTION_TO_PERFORM $1
       switch $2 {
            RESET_TEST                                {set UVM_TESTNAME "CLICTD_reset_test"                               }
            CONFIGURATION_TEST                        {set UVM_TESTNAME "CLICTD_configuration_test"                       }
            READOUT_TEST                              {set UVM_TESTNAME "CLICTD_readout_test"                             }
												READOUT_TEST_FOR_COVERAGE                 {set UVM_TESTNAME "CLICTD_readout_test_for_coverage"                }
            READOUTCONTROL_TEST                       {set UVM_TESTNAME "CLICTD_readoutcontrol_test"                      }
            WRREGISTERS_TEST                          {set UVM_TESTNAME "CLICTD_wrregisters_test"                         }
            TESTPULSECONTROL_TEST                     {set UVM_TESTNAME "CLICTD_testpulsecontrol_test"                    }
            POWERCONTROL_TEST                         {set UVM_TESTNAME "CLICTD_powercontrol_test"                        }
												RESET_SYNCHRONIZER_VIOLATIONS_TEST        {set UVM_TESTNAME "CLICTD_reset_synchronizer_violations_test"       }
												READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST {set UVM_TESTNAME "CLICTD_readoutstart_synchronizer_violations_test"}
				    }
								
								
								if { $1 == {sim} } {
							  	set ::DESIGN $3
							  	set ::BACK_ANNOTATED "YES"
							  	set ::WRITE_TRANSCRIPT_TO_TXT "YES" 
							  	set ::SDF_CORNER $5 
								} else {
								  set ::NUMBER_OF_SIMULATIONS $3
							  	set ::DESIGN $4
							  	set ::BACK_ANNOTATED "YES"
							  	if { $argc == 6 && $6 == "TXT"} {
								         set ::WRITE_TRANSCRIPT_TO_TXT "YES" 
							  	} else { 
								         set ::SDF_CORNER $6 
								  } elseif { $argc == 7 } {
								    if { $6 == "TXT" } { set ::SDF_CORNER $7 
									  	} else { set ::SDF_CORNER $6  }
							  	  set ::WRITE_TRANSCRIPT_TO_TXT "YES"
							  	}
								}
        
								assign_params 
								perform_action ; # execute the requested action
             
    }
}
