# Filename:          generate_verilog_to_include.tcl
# Author:            N�ria Egidos 
# Created on:        11/1/2019
# Last modified on:  11/1/2019
# Project:           Doctoral student, verification of CLICTD
# Description:       This script is called from launcher_all_tests.tcl to update the source files used for the simulation depending on which version of the netlist we're simulating (PATH_TO_DUT_FOLDER)

# Note: this script assumes that the path to the current version of Questasim has already been added to the PATH environment 
# variable. You can check if this is the case by typing echo $PATH in the terminal.
# If this is not the case, you won't be able to run vsim (vsim & will not work). To add it, you can editing ~/.cshrc, just 
# add the following line after #<DECM_END>:
# setenv PATH "/eda/mentor/2017-18/RHELx86/QUESTA-CORE-PRIME_10.6c-1/questasim/bin:$PATH"
# Then close the terminal and restart gp



#
############################################################################
############# Import the possible actions to perform #######################
############################################################################
#

source {../sim_annotated/actions.do}



#
############################################################################
######## Read simulation parameters ########################################
############################################################################
#

# https://www.tcl.tk/man/tcl8.6/TclCmd/source.htm
set PATH_TO_SOURCE [pwd]
set PATH_TO_SOURCE [string trim $PATH_TO_SOURCE][string trim "/parameters_for_generate_verilog_to_include.tcl"]
source $PATH_TO_SOURCE


#
############################################################################
# Set the working directory, clear it if it exists                         #
############################################################################
#

if {[file exists $QUESTA_DESIGN_LIB]} { 
   #vdel -lib work -all 
   actions::clean $QUESTA_DESIGN_LIB
} 
actions::setup $QUESTA_DESIGN_LIB $QUESTA_DESIGN_LIB_DEFAULT

actions::generate_verilog_to_include "" $INCLUDE_DIRS


#
############################################################################
# Close the simulator                                                      #
############################################################################
#
quit -f
