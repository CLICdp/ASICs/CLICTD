# Filename:          launcher_all_tests.tcl
# Author:            N�ria Egidos 
# Created on:        10/1/2019
# Last modified on:  16/1/2019
# Project:           Doctoral student, verification of CLICTD
# Description:       This script is called from the terminal (outside Questasim) to compile and simulate all tests in a row with the 
#                    corresponding back annotation; messages (in folder logs) and waveforms (in folder waveforms) are saved in separated 
#                    files for each test

# Note: this script assumes that the path to the current version of Questasim has already been added to the PATH environment 
# variable. You can check if this is the case by typing echo $PATH in the terminal.
# If this is not the case, you won't be able to run vsim (vsim & will not work). To add it, you can editing ~/.cshrc, just 
# add the following line after #<DECM_END>:
# setenv PATH "/eda/mentor/2017-18/RHELx86/QUESTA-CORE-PRIME_10.6c-1/questasim/bin:$PATH"
# Then close the terminal and restart gp

# https://forums.intel.com/s/question/0D50P00003yyPJXSA2/running-modelsim-in-commandline-mode?language=en_US
# https://www.tcl.tk/man/tcl8.5/tutorial/Tcl0a.html

# To use this script:
#    cd sim_annotated_min OR cd sim_annotated_typ OR cd sim_annotated_max (depending on the corner that we want to run)
#    tclsh launcher_all_tests.tcl 

# Note: to exit the simulator if some error occurs and the simulation doesn't finish, type quit in the terminal or (from another terminal) kill -9 <PID>

# To save waveforms and visualize them after the simulation:
# http://www.pldworld.com/_hdl/2/_ref/se_html/manual_html/a_tnt9.html
# http://www.pldworld.com/_hdl/2/_ref/se_html/tutorial_html/t_batch.html
# https://stackoverflow.com/questions/36444928/modelsim-export-wave-bitmap-batch-mode
#vsim -view saved.wlf 
#Open these windows with the View menu in the Main window, or the equivalent command at the ModelSim prompt:
#view signals list wave
#Note: If you open the Process or Variables windows they will be empty. You are looking at a saved simulation, not examining one interactively; the logfile saved in saved.wlf was used to reconstruct the current windows.
#Now that you have the windows open, put the signals in them:
#add wave *
#add list * 
# vsim -view waveform.wlf -do wave.do


#
############################################################################
############# Import the possible actions to perform #######################
############################################################################
#
source {../sim_annotated/actions.do}

#
############################################################################
#################### Definitions (simulator) ###############################
############################################################################
#
# Questasim run options
set VOPT      "" ; # optimization options
#set VOPT      "novopt" ; # optimization options
set VOPTARGS  ""
#set VSIM_PARAM "-classdebug -uvmcontrol=all -msgmode both -t ps -quiet"
set VSIM_PARAM "-t ps"

set TIMESCALE "1 ns / 1 ps" ; # timeunit / timeprecision

set QUESTA_RUN_OPTIONS {}
set QUESTA_RUN_OPTIONS [split $VSIM_PARAM " "]
#if { $SDFTYP     != "" } { lappend QUESTA_RUN_OPTIONS {*}$SDFTYP }
if { $VOPTARGS   != "" } { lappend QUESTA_RUN_OPTIONS [concat "-VOPTARGS=" $VOPTARGS ""] }
#set QUESTA_RUN_OPTIONS ""


#
# Name of the design library where ModelSim will store the compiled design 
# units (we will create it with vlib)
set QUESTA_DESIGN_LIB work
#
# Name of the default design library 
# (we will map it to QUESTA_DESIGN_LIB with vmap)
set QUESTA_DESIGN_LIB_DEFAULT work
#
#
#
set DESIGN "pnr"


#
############################################################################
########################### Simulation knobs ###############################
############################################################################
#


# Set this variable to "" to run the simulations using the nominal clock frequency (40 MHz for configuration and readout,
# 100 MHz for acquisition). Set to "YES" to force a higher clock frequency to test the limits of correct operation
# (50 MHz for configuration and readout, 125 MHz for acquisition)
set USE_HIGHER_CLOCK_FREQ ""

# Set this variable to "" to use test pulses of regular duration (1-31 in binary value);
# set this variable to "YES" to force using very short test pulses (shorter than 1 TOT count)
# so as to emulate a situation in which a low charge is collected, but large enough to 
# reach the threshold for a very short time, which generates a very short test pulse; 
# this test pulse should in principle be filtered, so the idea is to verify that no anomalous
# counter values are obtained
set USE_VERY_SHORT_TEST_PULSES ""
# Set this variable to "" to use 400 kHz I2C clock frequency; set to "YES" to use 1 MHz clock (the second
# doesn't have a physical meaning, but it's used to speed up the simulation)
set USE_FAST_I2C_CLOCK "YES"

# When READOUT_TEST_FOR_COVERAGE is run, set to "YES" to run it in debug mode, set to "" to run it in regular mode
set READOUTTESTFORCOVERAGE_DEBUGMODE ""

set CURRENT_PATH [pwd]

# Path where the DUT is located
set PATH_OLD /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout
set PATH_20181220 /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/ikremast/ALICEITS_OA/work_libs/ikremast/cds/CLICTD_DM/digital/netlistPostLayout_20181220
set PATH_20190120 /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/ikremast/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout_20190120
#set PATH_20190130 /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/ikremast/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout_20190130v2
set PATH_20190130 /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/ikremast/ALICEITS_OA/work_libs/user/cds/CLICTD_DM/digital/netlistPostLayout_20190131

set PATH_TO_DUT_FOLDER $PATH_20190130
actions::write_file_with_dut_path_to_include_in_parametersfile $PATH_TO_DUT_FOLDER
actions::write_file_with_registers_info_to_include_in_parametersfile $PATH_TO_DUT_FOLDER $PATH_OLD $PATH_20181220 $PATH_20190120 $PATH_20190130 $USE_HIGHER_CLOCK_FREQ $USE_VERY_SHORT_TEST_PULSES $USE_FAST_I2C_CLOCK $READOUTTESTFORCOVERAGE_DEBUGMODE
actions::write_file_with_assocarray_regs_to_include_in_clictdtest $PATH_TO_DUT_FOLDER $PATH_OLD $PATH_20181220 $PATH_20190120 $PATH_20190130
actions::write_file_with_dut_ports_to_include_in_dutwrapperpnr $PATH_TO_DUT_FOLDER $PATH_OLD $PATH_20181220 $PATH_20190120 $PATH_20190130
			

# Enable or disable loading and storing waveforms: if "YES", the waveforms corresponding to each test are
# stored in a .wlf file in the waveforms folder, so they are available to be plotted afterwards; if "NO", 
# no waveform is generated or stored
set USE_WAVEFORM "NO"


# Select if the script is run in command line ("COMMAND_LINE") or batch ("BATCH") mode
set RUN_SCRIPT_MODE "COMMAND_LINE"

# Check which tests will be simulated at the bottom of the script



#
############################################################################
################ Decide which corner to simulate ###########################
############################################################################
# 
set PATH_MIN_CORNER "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/PROJECT/work_libs/user/cds/CLICTD_workspace/CLICTD/CLICTD_verification_UVM/sim_annotated_min"
set PATH_TYP_CORNER "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/PROJECT/work_libs/user/cds/CLICTD_workspace/CLICTD/CLICTD_verification_UVM/sim_annotated_typ"
set PATH_MAX_CORNER "/projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/PROJECT/work_libs/user/cds/CLICTD_workspace/CLICTD/CLICTD_verification_UVM/sim_annotated_max"



if { [string equal $CURRENT_PATH $PATH_MIN_CORNER] } { 
    set SDF_CORNER "min"
} elseif  { [string equal $CURRENT_PATH $PATH_TYP_CORNER] } { 
   set SDF_CORNER "typ"
} elseif { [string equal $CURRENT_PATH $PATH_MAX_CORNER] } { 
    set SDF_CORNER "max"
} else {
    set SDF_CORNER "typ"
}
#} else {
#    echo "#################################################"
#				echo "Please cd to one of the supported directories #" 
#				echo "#################################################"
#}



#
############################################################################
# Change the value of colulmn_is_rtl (which columns to be used in RTL and  #
# which ones as postlayout) and update all the related files accordingly   #
############################################################################
#

proc update_value_columnisrtl_and_associated_files {} {

				# If you have changed the value of column_is_rtl, update the corresponding files!    
				# (which columns are RTL and which are postlayout in the top level netlist)                      
				actions::update_column_is_rtl $::COLUMN_IS_RTL	

				# If you have changed the value of column_is_rtl and/or the netlist version, update the corresponding files!
				# (which columns are RTL and which are postlayout in the top level netlist; interconnection of signals in pins interface to the signals in the postlayout netlist) 
				actions::update_netlist_for_simulation $::PATH_TO_DUT_FOLDER $::CURRENT_PATH $::QUESTA_DESIGN_LIB $::QUESTA_DESIGN_LIB_DEFAULT

}



#
############################################################################
# Print the simulation conditions                                          #
############################################################################
#
proc print_simulation_conditions {FILE_TO_WRITE} {

    # Open the file for writing
    set FILE_OPENED [open $FILE_TO_WRITE w]
				
				# Write the content into the file
				puts $FILE_OPENED "###################################################"
    puts $FILE_OPENED "############# SIMULATION CONDITIONS ###############"
				puts $FILE_OPENED "SIMULATION TEST LOG: $FILE_TO_WRITE"

				# Running in batch mode or in command line mode?
				# https://www.microsemi.com/document-portal/doc_view/136660-modelsim-me-10-5c-reference-manual-for-libero-soc-v11-8
				# This command returns 1 if ModelSim is operating in batch mode, otherwise it returns ?0.? It is typically used as a condition in an if statement.  
				# Some GUI commands do not exist in batch mode. If you want to write a script that will work in or out of batch mode, you can use the batch_mode command to determine which command to use. 
				# Note: batch_mode is only supported within Questasim, so we can't use it from the tclsh execution!
				#if [batch_mode] { 
				#   if { $RUN_SCRIPT_MODE == "BATCH" } {
				#			    puts $FILE_OPENED "BATCH MODE"
				#			} else {
				#			    puts $FILE_OPENED "COMMAND LINE MODE"
				#			}
				#} else {  puts $FILE_OPENED "GUI MODE"
				#} 
				if { $::RUN_SCRIPT_MODE == "BATCH" } {
							  puts $FILE_OPENED "BATCH MODE"
				} else {
						   puts $FILE_OPENED "COMMAND LINE MODE"
				}
				 
				puts $FILE_OPENED "MODULES AND SDF FILES FROM $::PATH_TO_DUT_FOLDER"
				puts $FILE_OPENED "BACK ANNOTATED, CORNER $::SDF_CORNER"
				puts $FILE_OPENED "FROM COLUMN 15 (LEFT) DOWN TO 0 (RIGHT), POSITION IS 1 IF RTL, 0 IF POSTLAYOUT: $::COLUMN_IS_RTL"
				puts $FILE_OPENED "WAVEFORM AVAILABLE IN waveforms FOLDER: $::USE_WAVEFORM"
				if { $::USE_HIGHER_CLOCK_FREQ == "YES" } {
				    puts $FILE_OPENED "Clock frequency higher than nominal used: 50 MHz configuration and readout, 125 MHz acquisition"
				}
				if { $::USE_VERY_SHORT_TEST_PULSES == "YES" } {
				    puts $FILE_OPENED "Test pulses shorter than 1 TOT count are used to emulate low charge collection and detect possible counter malfunction"
				}
				if { $::USE_FAST_I2C_CLOCK == "YES" } {
				    puts $FILE_OPENED "I2C clock frequency: 1 MHz (no physical meaning, only to speed up simulation)"
				} else {
				    puts $FILE_OPENED "I2C clock frequency: 400 kHz"
				}
			

				puts $FILE_OPENED "###################################################"
				
				# Close the file
    close $FILE_OPENED
}

#
############################################################################
############## Actions to be performed for every test ######################
############################################################################
# 




proc common_actions { UVM_TESTNAME_ARG } {

     
     set systemTime [clock seconds]

					#if { [string equal $::CURRENT_PATH $::PATH_MIN_CORNER] } { 
    	#				set TXT_LOG_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_min/logs/
					#				set WLF_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_min/waveforms/
					#} elseif  { [string equal $::CURRENT_PATH $::PATH_TYP_CORNER] } { 
   		#			set TXT_LOG_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_typ/logs/
					#			set WLF_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_typ/waveforms/
					#} elseif { [string equal $::CURRENT_PATH $::PATH_MAX_CORNER] } { 
    	#				set TXT_LOG_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_max/logs/
					#				set WLF_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_max/waveforms/
					#} else {
    	#				set TXT_LOG_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_tests/logs/
					#				set WLF_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/user/cds/CLICTD_workspace/CLICTD/dirty_tests_nuegidos/CLICTD_FullDesign/sim_annotated_tests/waveforms/
					#}

     set TXT_LOG_PATH ./logs/
					set WLF_PATH ./waveforms/
									
					
     switch $UVM_TESTNAME_ARG {
            RESET_TEST                                {set UVM_TESTNAME "CLICTD_reset_test"                               }
            CONFIGURATION_TEST                        {set UVM_TESTNAME "CLICTD_configuration_test"                       }
            READOUT_TEST                              {set UVM_TESTNAME "CLICTD_readout_test"                             }
												READOUT_TEST_FOR_COVERAGE                 {set UVM_TESTNAME "CLICTD_readout_test_for_coverage"                }
            READOUTCONTROL_TEST                       {set UVM_TESTNAME "CLICTD_readoutcontrol_test"                      }
            WRREGISTERS_TEST                          {set UVM_TESTNAME "CLICTD_wrregisters_test"                         }
            TESTPULSECONTROL_TEST                     {set UVM_TESTNAME "CLICTD_testpulsecontrol_test"                    }
            POWERCONTROL_TEST                         {set UVM_TESTNAME "CLICTD_powercontrol_test"                        }
												RESET_SYNCHRONIZER_VIOLATIONS_TEST        {set UVM_TESTNAME "CLICTD_reset_synchronizer_violations_test"       }
												READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST {set UVM_TESTNAME "CLICTD_readoutstart_synchronizer_violations_test"}
				    }
								
						puts "DEBUG LAUNCHER_ALL_TESTS.TCL $UVM_TESTNAME_ARG $UVM_TESTNAME"
					
					#set TXT_LOG_FILENAME [string trim "$UVM_TESTNAME"][string trim "_"][string trim $::SDF_CORNER][string trim "_"][clock format $systemTime -format %y-%m-%d_%H-%M-%S][string trim ".log"]
					set TXT_LOG_FILENAME [actions::generate_filename_including_testnameCornerSystemtime $UVM_TESTNAME $::SDF_CORNER][string trim ".log"]
					set TXT_LOG_FILENAME [string trim $TXT_LOG_PATH][string trim $TXT_LOG_FILENAME]
				 #echo "DEBUG LAUNCHER_ALL_TEST.DO TXT_LOG_FILENAME: $TXT_LOG_FILENAME "
					
					#set WLF_FILENAME [string trim "$UVM_TESTNAME"][string trim "_"][string trim $::SDF_CORNER][string trim "_"][clock format $systemTime -format %y-%m-%d_%H-%M-%S][string trim ".wlf"]
					set WLF_FILENAME [actions::generate_filename_including_testnameCornerSystemtime $UVM_TESTNAME $::SDF_CORNER][string trim ".wlf"]
					set WLF_FILENAME [string trim $WLF_PATH][string trim $WLF_FILENAME]
					
					
					
					# Write to file only in command line mode, because otherwise there will be
					# no content to store from the transcript
					print_simulation_conditions $TXT_LOG_FILENAME 
					
					
     if { $::USE_WAVEFORM == "YES" } { set temp [actions::assign_params $::DESIGN $UVM_TESTNAME $::PATH_TO_DUT_FOLDER "YES"]
					} else { set temp [actions::assign_params $::DESIGN $UVM_TESTNAME $::PATH_TO_DUT_FOLDER ""]
					}
					
					set TOP_MODULE [lindex $temp 0]
					if { $::USE_WAVEFORM == "YES" } { set WAVE_FILE [lindex $temp 1]
					} else { set WAVE_FILE ""
					}
					set FILES_TO_COMPILE [lindex $temp 2]
					set FILES_TO_RECOMPILE [lindex $temp 3]
					set INCLUDE_DIRS [lindex $temp 4]
					
					
					set SDF_ASSIGNMENTS_FILE ../sim_annotated/sdfAssignments
					#set RTL_PATH /projects/TOWER180/ALICEITS/CLICTD_53/workAreas/nuegidos/ALICEITS_OA/work_libs/nuegidos/cds/CLICTD_DM/digital/netlistPostLayout
					set RTL_PATH ../sim_annotated


					set SDF_CORNER_AUX [string trim "-sdf"][string trim $::SDF_CORNER]
			  set SDFPARAMS [generate_arguments_from_file $SDF_ASSIGNMENTS_FILE $SDF_CORNER_AUX RTL_PATH $RTL_PATH] 
		 	 lappend SDFPARAMS "-sdfnoerror" 
		   set SDFPARAMS [join $SDFPARAMS]
		   #lappend $::QUESTA_RUN_OPTIONS {*}$SDFPARAMS
					set ::QUESTA_RUN_OPTIONS [concat $::QUESTA_RUN_OPTIONS $SDFPARAMS]
					if { $::USE_WAVEFORM == "YES" } {
					    lappend $::QUESTA_RUN_OPTIONS [concat "-wlf" $WLF_FILENAME"]
					}
					
					
					# Write parameters for the simulation 
					# Open the file for writing
					set FILE_TO_OPEN [string trim $::CURRENT_PATH][string trim "/parameters_for_launcher_one_test.tcl"]
    	set FILE_OPENED [open $FILE_TO_OPEN w]
					# Write the content into the file
					puts $FILE_OPENED "# Parameters for simulation, sourced in launcher_one_test.tcl: $TXT_LOG_FILENAME"
					puts $FILE_OPENED "set TIMESCALE [string trim "\""][string trim $::TIMESCALE][string trim "\""]"
					puts $FILE_OPENED "set INCLUDE_DIRS [string trim "\""][string trim $INCLUDE_DIRS][string trim "\""]"
					puts $FILE_OPENED "set FILES_TO_COMPILE [string trim "\""][string trim $FILES_TO_COMPILE][string trim "\""]"
					puts $FILE_OPENED "set DESIGN [string trim "\""][string trim $::DESIGN][string trim "\""]"
					puts $FILE_OPENED "set TOP_MODULE [string trim "\""][string trim $TOP_MODULE][string trim "\""]"
					puts $FILE_OPENED "set UVM_TESTNAME [string trim "\""][string trim $UVM_TESTNAME][string trim "\""]"
					puts $FILE_OPENED "set QUESTA_RUN_OPTIONS [string trim "\""][string trim $::QUESTA_RUN_OPTIONS][string trim "\""]"
					puts $FILE_OPENED "set WAVE_FILE [string trim "\""][string trim $WAVE_FILE][string trim "\""]"
					puts $FILE_OPENED "set QUESTA_DESIGN_LIB [string trim "\""][string trim $::QUESTA_DESIGN_LIB][string trim "\""]" 
					puts $FILE_OPENED "set QUESTA_DESIGN_LIB_DEFAULT [string trim "\""][string trim $::QUESTA_DESIGN_LIB_DEFAULT][string trim "\""]"
					# Close the file
    	close $FILE_OPENED
									
		   set FILE_WITH_SIMULATION_COMMANDS [string trim $::CURRENT_PATH][string trim "/launcher_one_test.tcl"]
					set TEMPORARY_FILE_TO_REDIRECT_STDOUT [string trim $::CURRENT_PATH][string trim "/temp.log"]
		   # Perform the simulation, save the message log in a temporary file
					# Note: plusargs are for run time (for the testbench, can't be used with vlog/vsim!)
					#if { $::RUN_SCRIPT_MODE == "BATCH" } {
					#    # Batch mode; this is the preferred option, it should run faster
					#				if { $::USE_WAVEFORM == "YES" } {
					#				        exec vsim -batch -do $FILE_WITH_SIMULATION_COMMANDS +TIMESCALE=$::TIMESCALE +INCLUDE_DIRS=$INCLUDE_DIRS +FILES_TO_COMPILE=$FILES_TO_COMPILE +DESIGN=$::DESIGN +TOP_MODULE=$TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME +QUESTA_RUN_OPTIONS=$::QUESTA_RUN_OPTIONS +WAVE_FILE=$WAVE_FILE -wlf $WLF_FILENAME > $TEMPORARY_FILE_TO_REDIRECT_STDOUT
					#				} else {
					#				        exec vsim -batch -do $FILE_WITH_SIMULATION_COMMANDS +TIMESCALE=$::TIMESCALE +INCLUDE_DIRS=$INCLUDE_DIRS +FILES_TO_COMPILE=$FILES_TO_COMPILE +DESIGN=$::DESIGN +TOP_MODULE=$TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME +QUESTA_RUN_OPTIONS=$::QUESTA_RUN_OPTIONS +WAVE_FILE=$WAVE_FILE > $TEMPORARY_FILE_TO_REDIRECT_STDOUT
					#				}
					#} else {
					#    # Command line mode
					#    if { $::USE_WAVEFORM == "YES" } {
					#				        exec vsim -c -do launcher_one_test.tcl +TIMESCALE=$::TIMESCALE +INCLUDE_DIRS=$INCLUDE_DIRS +FILES_TO_COMPILE=$FILES_TO_COMPILE +DESIGN=$::DESIGN +TOP_MODULE=$TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME +QUESTA_RUN_OPTIONS=$::QUESTA_RUN_OPTIONS +WAVE_FILE=$WAVE_FILE -wlf $WLF_FILENAME > $TEMPORARY_FILE_TO_REDIRECT_STDOUT
					#				} else {
					#				        exec vsim -c -do launcher_one_test.tcl +TIMESCALE=$::TIMESCALE +INCLUDE_DIRS=$INCLUDE_DIRS +FILES_TO_COMPILE=$FILES_TO_COMPILE +DESIGN=$::DESIGN +TOP_MODULE=$TOP_MODULE +UVM_TESTNAME=$UVM_TESTNAME +QUESTA_RUN_OPTIONS=$::QUESTA_RUN_OPTIONS +WAVE_FILE=$WAVE_FILE > $TEMPORARY_FILE_TO_REDIRECT_STDOUT
					#				}
					#}		
					if { $::RUN_SCRIPT_MODE == "BATCH" } {
					    # Batch mode; this is the preferred option, it should run faster
									if { $::USE_WAVEFORM == "YES" } {
									        exec vsim -batch -do $FILE_WITH_SIMULATION_COMMANDS -wlf $WLF_FILENAME > $TEMPORARY_FILE_TO_REDIRECT_STDOUT
									} else {
									        exec vsim -batch -do $FILE_WITH_SIMULATION_COMMANDS > $TEMPORARY_FILE_TO_REDIRECT_STDOUT
									}
					} else {
				     # Command line mode
					    if { $::USE_WAVEFORM == "YES" } {
									        exec vsim -c -do launcher_one_test.tcl -wlf $WLF_FILENAME > $TEMPORARY_FILE_TO_REDIRECT_STDOUT
									} else {
									        exec vsim -c -do launcher_one_test.tcl > $TEMPORARY_FILE_TO_REDIRECT_STDOUT
									}
					}					
				 

				 # Append the message log to the existing log
									
					# How to append the content of a file to another: https://groups.google.com/forum/#!topic/comp.lang.tcl/s2nWb5lxRck
    	# Open the "final" file in append mode
					set FILE_OPENED_APPEND [open $TXT_LOG_FILENAME a] 
					set FILE_OPENED_READ [open $TEMPORARY_FILE_TO_REDIRECT_STDOUT r] 
					
					
					# Write simulation time to the "final" file
					puts $FILE_OPENED "SIMULATION START TIME: [clock format $systemTime -format %y/%m/%d,%H:%M:%S]"
					set systemTime2 [clock seconds]
					puts $FILE_OPENED "SIMULATION END TIME: [clock format $systemTime2 -format %y/%m/%d,%H:%M:%S]"
					puts $FILE_OPENED "SIMULATION ELAPSED: [actions::formatTimeInterval [expr {$systemTime2 - $systemTime}]]"
    	puts $FILE_OPENED "###################################################"
					
					
					# Append the contents of the auxiliary file to the "final" file
					fcopy $FILE_OPENED_READ $FILE_OPENED_APPEND
					# Close all files
					close $FILE_OPENED_READ
					close $FILE_OPENED_APPEND
					
					
					# Delete the auxiliary log file
					file delete $TEMPORARY_FILE_TO_REDIRECT_STDOUT
				 
}



#
############################################################################
# Run all the tests for the specified corner, using the postlayout netlist #
############################################################################
#


# Indicate which columns are RTL and which columns are postlayout (some can be set to RTL to speed up simulation)
set COLUMN_IS_RTL "16'b0000000000000000" ; #	all columns are post layout	
#set COLUMN_IS_RTL "16'b1111111111111110" ; #	only column 0 is post layout netlist, the rest are RTL	
# Update the corresponding files: which columns are RTL and which are postlayout in the top level netlist; interconnection of signals in pins interface to the signals in the postlayout netlist
#update_value_columnisrtl_and_associated_files

common_actions RESET_TEST 
common_actions RESET_SYNCHRONIZER_VIOLATIONS_TEST 
common_actions READOUTSTART_SYNCHRONIZER_VIOLATIONS_TEST 
common_actions CONFIGURATION_TEST 
common_actions READOUT_TEST ; #some functionality in the dataout model has been updated with respect to READOUT_TEST 
common_actions WRREGISTERS_TEST 
common_actions READOUTCONTROL_TEST 
common_actions TESTPULSECONTROL_TEST 
common_actions POWERCONTROL_TEST 
# Force very short TOT pulses to emulate small charge collection
#set USE_VERY_SHORT_TEST_PULSES "YES"
#actions::write_file_with_registers_info_to_include_in_parametersfile $PATH_TO_DUT_FOLDER $PATH_OLD $PATH_20181220 $PATH_20190120 $PATH_20190130 $USE_HIGHER_CLOCK_FREQ $USE_VERY_SHORT_TEST_PULSES $USE_FAST_I2C_CLOCK $READOUTTESTFORCOVERAGE_DEBUGMODE
#common_actions READOUT_TEST ; #some functionality in the dataout model has been updated with respect to READOUT_TEST 


#set READOUTTESTFORCOVERAGE_DEBUGMODE "YES"


#set USE_VERY_SHORT_TEST_PULSES ""
#actions::write_file_with_registers_info_to_include_in_parametersfile $PATH_TO_DUT_FOLDER $PATH_OLD $PATH_20181220 $PATH_20190120 $PATH_20190130 $USE_HIGHER_CLOCK_FREQ $USE_VERY_SHORT_TEST_PULSES $USE_FAST_I2C_CLOCK $READOUTTESTFORCOVERAGE_DEBUGMODE
#common_actions READOUT_TEST_FOR_COVERAGE
## Force very short TOT pulses to emulate small charge collection
#set USE_VERY_SHORT_TEST_PULSES "YES"
#actions::write_file_with_registers_info_to_include_in_parametersfile $PATH_TO_DUT_FOLDER $PATH_OLD $PATH_20181220 $PATH_20190120 $PATH_20190130 $USE_HIGHER_CLOCK_FREQ $USE_VERY_SHORT_TEST_PULSES $USE_FAST_I2C_CLOCK $READOUTTESTFORCOVERAGE_DEBUGMODE
#common_actions READOUT_TEST_FOR_COVERAGE 



