clictdTop dut
(
	///////////////////////////
	// These pins have changed with respect to clictdTop in order to use a unidirectional
	// I2C master
	//
	.SDAin(slowcontrol_if.SDAin),      		// SDA pin
	//.SDAin(slowcontrol_if.SDA),      		// SDA pin
	.SDAout(slowcontrol_if.SDAout),
	.SDAen(slowcontrol_if.SDAen),
	///////////////////////////
	.SCL_pad(slowcontrol_if.SCL_pad),     		// SCL pin
	//.ADDR_pad(slowcontrol_if.ADDR_pad), 	    // i2c address least significant bits
	.ANALOG_IN_pad(),
	.REF_pad(),
	.ANALOG_OUT_pad(),
	.PWREN_pad(pins_if.PWREN_pad),      	// power pulsing input
	.TPULSE_pad(pins_if.TPULSE_pad),    	// external test pulse input
	.READOUT_pad(pins_if.READOUT_pad),		// external pin for issuing readout
	.SHUTTER_pad(pins_if.SHUTTER_pad),		// shutter signal input
	.RSTN_pad(pins_if.RSTN_pad),    		// reset signal (active low)
	.CLK_100_n_pad(pins_if.CLK_100_n_pad & pins_if.enable_clock_100MHz),		// 100 MHz clock
	.CLK_100_p_pad(pins_if.CLK_100_p_pad & pins_if.enable_clock_100MHz),
	.CLK_40_n_pad(pins_if.CLK_40_n_pad),		// 40 MHz clock
	.CLK_40_p_pad(pins_if.CLK_40_p_pad),
	.DATA_OUT_n_pad(), 	// data output signal
	.DATA_OUT_p_pad(readout_if.DATA_OUT),
	.CLK_OUT_n_pad(), 	// clock output signal
	.CLK_OUT_p_pad(readout_if.CLK_OUT),
	.ENABLE_OUT_p_pad(pins_if.ENABLE_OUT_pad), 	// enable output signal (to indicate when data starts coming out of the chip)
	.ENABLE_OUT_n_pad(),
	.VDDA(), 			// analog power supply
	.VDD(),			// digital power supply
	.VSSA(),			// analog ground
	.VSS(),			// digital ground
	.PWELL(),			// p-well bias
	.SUB()			// substrate bias
);
