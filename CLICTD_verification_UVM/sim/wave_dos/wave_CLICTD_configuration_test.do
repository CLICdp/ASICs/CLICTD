# Filename:          wave_CLICTD_configuration_test.do
# Author:            Núria Egidos 
# Created on:        9/08/2018
# Last modified on:  9/08/2018
# Project:           Doctoral student, verification of CLICTD
# Description:       Macro containing the waveforms to be loaded to Modelsim after the simulation
# Derived from:      Adrian Fiergolski scripts in https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim
#


#sim:/CLICTD_testbench_top/pins_if/PWRE_pad \
#sim:/CLICTD_testbench_top/pins_if/TPULSE_pad \
#sim:/CLICTD_testbench_top/pins_if/READOUT_pad \
#sim:/CLICTD_testbench_top/pins_if/SHUTTER_pad \
#sim:/CLICTD_testbench_top/pins_if/ENABLE_OUT_pad \
#sim:/CLICTD_testbench_top/pins_if/readoutStartExtEn \
#sim:/CLICTD_testbench_top/pins_if/readoutStart \
#sim:/CLICTD_testbench_top/pins_if/testPulseExtEnable \
#sim:/CLICTD_testbench_top/pins_if/testPulse \
#sim:/CLICTD_testbench_top/pins_if/powerExtEnable \
#sim:/CLICTD_testbench_top/pins_if/powerEnable \
#sim:/CLICTD_testbench_top/pins_if/tpEnableDigital \
#sim:/CLICTD_testbench_top/pins_if/mask \
#sim:/CLICTD_testbench_top/pins_if/tpEnableAnalog \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC0 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC1 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC2 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC3 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC4 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC5 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC6 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC7 \
#sim:/CLICTD_testbench_top/pins_if/disc

onerror {resume}
quietly WaveActivateNextPane {} 0


add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column12/EOC/\pixels_count_reg[2] /*}



#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/mux_40_25/p214748365A__9719 }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/mux_40_25/p214748365A__9719/*}



######### Signals associated to setup violations detected on 12/11/18 #########

#add wave -noupdate -divider {Column9/EOC/clk40_latch_preserve }
##add wave -noupdate -label {Column9/EOC/clk40_latch_preserve} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column9/EOC/clk40_latch_preserve/*}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column9/EOC/clk40_latch_preserve/*}

#add wave -noupdate -divider {Column12/EOC/clk40_latch_preserve }
##add wave -noupdate -label {Column12/EOC/clk40_latch_preserve} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column12/EOC/clk40_latch_preserve/*}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column12/EOC/clk40_latch_preserve/*}

#add wave -noupdate -divider {Column13/EOC/clk40_latch_preserve }
##add wave -noupdate -label {Column13/EOC/clk40_latch_preserve} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column13/EOC/clk40_latch_preserve/*}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column13/EOC/clk40_latch_preserve/*}

#add wave -noupdate -divider {Column14/EOC/clk40_latch_preserve }
##add wave -noupdate -label {Column14/EOC/clk40_latch_preserve} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column14/EOC/clk40_latch_preserve/*}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column14/EOC/clk40_latch_preserve/*}

#add wave -noupdate -divider {Column15/EOC/clk40_latch_preserve }
##add wave -noupdate -label {Column15/EOC/clk40_latch_preserve} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/clk40_latch_preserve/*}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/clk40_latch_preserve/*}

#add wave -noupdate -divider {DigitalPeri/ROCTRL/\header_reg[3] }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\header_reg[3] /*}
##add wave -noupdate -label {DigitalPeri/ROCTRL/\header_reg[3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\header_reg[3] /*}
#CK
#D
#RN
#SN
#Q
#QN
#notifier
#D_delay
#RN_delay
#SN_delay
#CK_delay

###############################################################################





######### Signals associated to setup/width violations detected on 27/11/18 #########


add wave -noupdate -label {slowcontrol_if/SCL_pad} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SCL_pad}


#add wave -noupdate -label {slowcontrol_if/MS_SDAout} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/MS_SDAout}
#add wave -noupdate -label {slowcontrol_if/MS_SDAen} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/MS_SDAen }
#add wave -noupdate -label {slowcontrol_if/SL_SDAout} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SL_SDAout }
#add wave -noupdate -label {slowcontrol_if/SL_SDAen} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SL_SDAen }
#add wave -noupdate -label {slowcontrol_if/SDA} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SDA }
#add wave -noupdate -label {slowcontrol_if/SDAen} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SDAen }
#add wave -noupdate -label {slowcontrol_if/SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SDAin }
#add wave -noupdate -label {slowcontrol_if/SDAout} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SDAout}
#add wave -noupdate -label {port SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/dut/SDAin}
#add wave -noupdate -label {port SDAout} {sim:/CLICTD_testbench_top/dut_wrapper/dut/SDAout}
#add wave -noupdate -label {port SDAen} {sim:/CLICTD_testbench_top/dut_wrapper/dut/SDAen}
#add wave -noupdate -label {DigitalPeri/SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SDAin} 
#add wave -noupdate -label {DigitalPeri/SCTRL/SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/SDAin} 
#add wave -noupdate -label {DigitalPeri/SCTRL/I2CS/SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAin} 
#add wave -noupdate -label {DigitalPeri/SCTRL/I2CS/SDAGF/in} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAGF/in} 
#add wave -noupdate -label {DigitalPeri/SCTRL/I2CS/SDAGF/out_reg/D} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAGF/out_reg/D} 



#add wave -noupdate -divider {DigitalPeri/SCTRL/I2CS/SDAGF/ }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAGF/*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[7].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /\configBits_reg }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[7].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /\configBits_reg[0]/*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[10].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /hit_bits/\genblk1[5].genblk1.hit /hit_reg }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[10].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /hit_bits/\genblk1[5].genblk1.hit /hit_reg/*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[2].pixel_gen.pixel /hit/hf_reg_reg }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[2].pixel_gen.pixel /hit/hf_reg_reg/*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[12].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /Counter/\tot_reg[1] }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[12].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /Counter/\tot_reg[1]/*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[13].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[1] }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[13].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[1]/*}

###############################################################################


add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_check_reg[0]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_check_reg[0] /*}

add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_check_reg[1]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_check_reg[1] /*}

add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_check_reg[2]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_check_reg[2] /*}

add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_check_reg[3]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_check_reg[3] /*}

add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_check_reg[4]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_check_reg[4] /*}

add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_check_reg[5]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_check_reg[5] /*}

add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_check_reg[6]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_check_reg[6] /*}

add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_check_reg[7]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_check_reg[7] /*}


add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[0]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[0] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[1]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[1] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[2]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[2] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[3]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[3] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[4]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[4] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[5]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[5] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[6]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[6] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[7]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[7] /*}


add wave -noupdate -divider {DigitalPeri/CONFIG/\config_word_reg[0]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/\config_word_reg[0] /*}
add wave -noupdate -divider {DigitalPeri/CONFIG/\config_word_reg[1]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/\config_word_reg[1] /*}
add wave -noupdate -divider {DigitalPeri/CONFIG/\config_word_reg[2]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/\config_word_reg[2] /*}
add wave -noupdate -divider {DigitalPeri/CONFIG/\config_word_reg[3]}
add wave -position insertpoint {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/\config_word_reg[3] /*}



######### Signals used to debug simulation with post layout netlist #########
# An error was detected in STATUS_SECOND_READOUT_ALL_ZEROS: ENABLE_OUT_pad and
# DATA_OUT were all the time undefined or had many glitches


add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/RSTNCTRL/*

add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/FE_OFN1_RSTN_SYNC 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/FE_OFN3_RSTN_SYNC
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/FE_OFN280_RSTN_SYNC
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/FE_OFN285_RSTN_SYNC
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/FE_OFN284_RSTN_SYNC


add wave -noupdate -label {mask[0][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[0][0][0]}
add wave -noupdate -label {mask[1][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[1][0][0]}
add wave -noupdate -label {mask[2][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[2][0][0]}
add wave -noupdate -label {mask[3][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[3][0][0]}
add wave -noupdate -label {mask[4][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[4][0][0]}
add wave -noupdate -label {mask[5][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[5][0][0]}
add wave -noupdate -label {mask[6][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[6][0][0]}
add wave -noupdate -label {mask[7][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[7][0][0]}
add wave -noupdate -label {mask[8][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[8][0][0]}

add wave -noupdate -label {tpEnableDigital[0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[0][0][0]}
add wave -noupdate -label {tpEnableDigital[1][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[1][0][0]}
add wave -noupdate -label {tpEnableDigital[2][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[2][0][0]}
add wave -noupdate -label {tpEnableDigital[3][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[3][0][0]}
add wave -noupdate -label {tpEnableDigital[4][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[4][0][0]}
add wave -noupdate -label {tpEnableDigital[5][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[5][0][0]}
add wave -noupdate -label {tpEnableDigital[6][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[6][0][0]}
add wave -noupdate -label {tpEnableDigital[7][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[7][0][0]}
add wave -noupdate -label {tpEnableDigital[8][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[8][0][0]}



#add wave -noupdate -label {tuningDAC1[0][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[0][0][0][0]}
#add wave -noupdate -label {tuningDAC1[1][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[1][0][0][0]}
#add wave -noupdate -label {tuningDAC1[2][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[2][0][0][0]}
#add wave -noupdate -label {tuningDAC1[3][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[3][0][0][0]}
#add wave -noupdate -label {tuningDAC1[4][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[4][0][0][0]}
#add wave -noupdate -label {tuningDAC1[5][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[5][0][0][0]}
#add wave -noupdate -label {tuningDAC1[6][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[6][0][0][0]}
#add wave -noupdate -label {tuningDAC1[7][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[7][0][0][0]}
#add wave -noupdate -label {tuningDAC1[8][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[8][0][0][0]}
#add wave -noupdate -label {tuningDAC1[9][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[9][0][0][0]}
#add wave -noupdate -label {tuningDAC1[10][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[10][0][0][0]}
#add wave -noupdate -label {tuningDAC1[11][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[11][0][0][0]}
#add wave -noupdate -label {tuningDAC1[12][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[12][0][0][0]}
#add wave -noupdate -label {tuningDAC1[13][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[13][0][0][0]}
#add wave -noupdate -label {tuningDAC1[14][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[14][0][0][0]}
#add wave -noupdate -label {tuningDAC1[15][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[15][0][0][0]}

#add wave -noupdate -label {tuningDAC7[0][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[0][0][0][2]}
#add wave -noupdate -label {tuningDAC7[1][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[1][0][0][2]}
##add wave -noupdate -label {tuningDAC7[2][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[2][0][0][2]}
#add wave -noupdate -label {tuningDAC7[3][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[3][0][0][2]}
#add wave -noupdate -label {tuningDAC7[4][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[4][0][0][2]}
#add wave -noupdate -label {tuningDAC7[5][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[5][0][0][2]}
#add wave -noupdate -label {tuningDAC7[6][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[6][0][0][2]}
#add wave -noupdate -label {tuningDAC7[7][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[7][0][0][2]}
#add wave -noupdate -label {tuningDAC7[8][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[8][0][0][2]}
#add wave -noupdate -label {tuningDAC7[9][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[9][0][0][2]}
#add wave -noupdate -label {tuningDAC7[10][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[10][0][0][2]}
#add wave -noupdate -label {tuningDAC7[11][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[11][0][0][2]}
#add wave -noupdate -label {tuningDAC7[12][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[12][0][0][2]}
#add wave -noupdate -label {tuningDAC7[13][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[13][0][0][2]}
#add wave -noupdate -label {tuningDAC7[14][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[14][0][0][2]}
#add wave -noupdate -label {tuningDAC7[15][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[15][0][0][2]}





add wave -noupdate -divider {Top pixel col 0 (pnr) }
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /dataIn}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /dataOut}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /hitBits} 
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /cnt_toa} 
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /cnt_tot}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /noCompression}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[7].pixel_gen.pixel /hit/hf_reg}

add wave -noupdate -divider {Bottom pixel col 0 (pnr) }
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /dataIn}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /dataOut}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hitBits} 
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /cnt_toa} 
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /cnt_tot}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /noCompression}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit/hf_reg}

add wave -noupdate -divider {EOC col 0 (pnr)}
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/resetn
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/countReadoutn_out
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/clk_out
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/resetn
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/column_done
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/dataOut_peri 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/dataIn_col 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/dataIn_peri 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/dataOut_col

add wave -noupdate -divider {Digital periphery ROCTRL}
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/dataIn 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/columnDone 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/columnToken
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/headerEnd 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/headerStart 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/header
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/headerDone 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/headerEndDone 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/headerStartDone
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/lpg_rc_gclk 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/lpg_rc_gclk_2727 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/lpg_rc_gclk_2729
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/enableReadoutInt
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutRstn
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/resetn


add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutStart 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/configStage 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/countReadoutn 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/clkOut 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/enableOut 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/dataOut 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/clk40 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/clkConf 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/clk40_clone1
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/columnDoneDelayed 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/columnDoneReadout 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/columnTokenAux 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/columnTokenReg 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/counterRstn_check 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/counterRstn_pos_bit
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/clkIn_BAR 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/countReadoutnD 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/counterRstn 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/counterRstn_neg 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/enableReadoutInt 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/endRstn 


add wave -noupdate -divider {Digital periphery CONFIG}

add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/conf 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/config_send 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/config_word_in 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/config_word_out 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/clk_out 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/clk_in_clone1 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/clk_in
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/config_word
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/configuration



#############################################################################

add wave -noupdate -divider {Stimuli (pins_if)}
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/RSTN_pad 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/CLK_100_n_pad 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/CLK_100_p_pad 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/CLK_40_n_pad 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/CLK_40_p_pad 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_100_n_pad 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_100_p_pad 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_40_n_pad 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_40_p_pad 

#add wave -noupdate -divider {Control (slowcontrol_if)}
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/enable_clock_40MHz
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/enable_clock_100MHz

add wave -noupdate -divider {Outputs (pins_if)}
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/columnDone 
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/dataIn 
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/commonToken 

#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tpEnableDigital 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/mask 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tpEnableAnalog 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC0 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC1
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC2 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC3 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC4 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC5 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC6 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC7 


#add wave -noupdate sim:/uvm_root/uvm_test_top/config_matrix_obj


add wave -noupdate -divider {Outputs (readout_if)}
add wave -noupdate sim:/CLICTD_testbench_top/readout_if/DATA_OUT 
add wave -noupdate sim:/CLICTD_testbench_top/readout_if/CLK_OUT
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/ENABLE_OUT_pad

#add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/readout_vif
#add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/readout_vif/DATA_OUT
#add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/readout_vif/CLK_OUT
#add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/pins_aux_vif/ENABLE_OUT_pad

add wave -noupdate -divider {Serial readout}
add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/chip_status_obj
add wave -noupdate -radix decimal sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/bit_readout_counter 
add wave -noupdate -radix decimal sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/aux_bit_counter 
add wave -noupdate -radix decimal sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/column_counter 
add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/window 
add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/readout_status 

#add wave -noupdate -divider {Half configuration sequence}
#bit_counter; // // count configuration bits as they are sent, from `NUMBER_OF_FF_PER_SUPERPIXEL-1 to 0
#aux_bit_counter = 0; // count configuration bits as they are sent, from 0 to `NUMBER_OF_FF_PER_SUPERPIXEL-1
#superpixel_segment_counter = 0;
#superpixel_in_segment_counter = 0;
#config_word;
#config_matrix_obj


TreeUpdate [SetDefaultTree]
#WaveRestoreCursors {{Cursor 1} {4286575000000 fs} 1} {{Cursor 2} {5602197320 fs} 0}
#quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
#WaveRestoreZoom {5584047 ps} {5923953 ps}

