# Filename:          wave_CLICTD_readoutstartsync_test.do
# Author:            N�ria Egidos 
# Created on:        11/12/2018
# Last modified on:  11/12/2018
# Project:           Doctoral student, verification of CLICTD
# Description:       Macro containing the waveforms to be loaded to Modelsim after the simulation
# Derived from:      Adrian Fiergolski scripts in https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim
#

onerror {resume}
quietly WaveActivateNextPane {} 0





add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/pins_if/RSTN_pad}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_40_p_pad}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/pins_if/digitalperi_roctrl_clk40}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/pins_if/READOUT_pad}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutStart}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutStart_sync}

add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/*}




TreeUpdate [SetDefaultTree]
#WaveRestoreCursors {{Cursor 1} {4286575000000 fs} 1} {{Cursor 2} {5602197320 fs} 0}
#quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
#WaveRestoreZoom {5584047 ps} {5923953 ps}

