# Filename:          wave_CLICTD_wrregisters_test.do
# Author:            N�ria Egidos 
# Created on:        29/11/2018
# Last modified on:  29/11/2018
# Project:           Doctoral student, verification of CLICTD
# Description:       Macro containing the waveforms to be loaded to Modelsim after the simulation
# Derived from:      Adrian Fiergolski scripts in https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim
#



add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/RSTN
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_40
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CLK
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CLK_OUT




#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/counterRstn_neg_reg/*}


add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/countReadoutn_in}


add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/clk_40_in}

add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/countReadoutn}

add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutStart_sync}



add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/READOUT
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/readoutStart
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/configStage}




add wave -noupdate -divider {Column15/EOC/countReadoutn_D_reg[1]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[1] /*}

add wave -noupdate -divider {Column15/EOC/countReadoutn_D_reg[2]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[2] /*}


add wave -noupdate -divider {Column15/EOC/countReadoutn_D_reg[3]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[3] /*}


add wave -noupdate -divider {Column15/EOC/countReadoutn_D_reg[4]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[4] /*}


add wave -noupdate -divider {Column15/EOC/countReadoutn_D_reg[5]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[5] /*}


add wave -noupdate -divider {Column15/EOC/countReadoutn_D_reg[6]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[6] /*}


add wave -noupdate -divider {Column15/EOC/countReadoutn_D_reg[7]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column15/EOC/\countReadoutn_D_reg[7] /*}







	

TreeUpdate [SetDefaultTree]
#WaveRestoreCursors {{Cursor 1} {4286575000000 fs} 1} {{Cursor 2} {5602197320 fs} 0}
#quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
#WaveRestoreZoom {5584047 ps} {5923953 ps}

