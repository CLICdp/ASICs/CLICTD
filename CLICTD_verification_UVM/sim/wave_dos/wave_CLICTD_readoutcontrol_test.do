# Filename:          wave_CLICTD_readoutcontrol_test.do
# Author:            Núria Egidos 
# Created on:        26/09/2018
# Last modified on:  26/09/2018
# Project:           Doctoral student, verification of CLICTD
# Description:       Macro containing the waveforms to be loaded to Modelsim after the simulation
# Derived from:      Adrian Fiergolski scripts in https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim
#

onerror {resume}
quietly WaveActivateNextPane {} 0


add wave -noupdate -divider {Stimuli}

#add wave -noupdate sim:/uvm_root/uvm_test_top/chip_status_obj
add wave -noupdate sim:/uvm_root/uvm_test_top/chip_status_obj.chip_status
#add wave -noupdate sim:@ChipStatusInfo@1.chip_status

add wave -noupdate sim:/CLICTD_testbench_top/pins_if/READOUT_pad 
#add wave -noupdate sim:/uvm_root/uvm_test_top/bits_slowcontrol_reg_obj
add wave -noupdate sim:/uvm_root/uvm_test_top/bits_slowcontrol_reg_obj.roExtEn
add wave -noupdate sim:/uvm_root/uvm_test_top/bits_slowcontrol_reg_obj.roInt
#add wave -noupdate sim:@BitsMonitorSlowcontrolReg@1.roExtEn
#add wave -noupdate sim:@BitsMonitorSlowcontrolReg@1.roInt

add wave -noupdate sim:/CLICTD_testbench_top/pins_if/ENABLE_OUT_pad 




add wave -noupdate sim:/CLICTD_testbench_top/readout_if/CLK_OUT 
add wave -noupdate sim:/CLICTD_testbench_top/readout_if/DATA_OUT

 
######### Signals associated to setup/width violations detected on 27/11/18 #########


#add wave -noupdate -divider {DigitalPeri/SCTRL/RO}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/RO/readoutStart }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/RO/readoutStartInt }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/RO/readoutStartExt }
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/RO/readoutStartExtEn}



#add wave -noupdate -divider {DigitalPeri/ROCTRL/clkReadout_latch_preserve}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/clkReadout_latch_preserve/*}

#add wave -noupdate -divider {DigitalPeri/ROCTRL/counterRstn_neg_reg}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/counterRstn_neg_reg/*}

#add wave -noupdate -divider {MATRIX/Column4/EOC/clk100_latch_preserve}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/EOC/clk100_latch_preserve/*}

#add wave -noupdate -divider {MATRIX/Column3/EOC/clk100_latch_preserve}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/EOC/clk100_latch_preserve/*}

#add wave -noupdate -divider {DigitalPeri/ROCTRL/\headerEnd_reg[15]}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\headerEnd_reg[15] /*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\tot_reg[0]}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\tot_reg[0] /*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[4].Spix_gen.Spix /\pixel_gen[2].pixel_gen.pixel /Counter/\toa_reg[1]}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[4].Spix_gen.Spix /\pixel_gen[2].pixel_gen.pixel /Counter/\toa_reg[1] /*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk12p5_reg}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk12p5_reg/*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk6p25_reg}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[15].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk6p25_reg/*}

#add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[9].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /clkToT/clk50_reg}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[9].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /clkToT/clk50_reg/*}

#add wave -noupdate -divider {MATRIX/Column0/EOC/\pixels_count_reg[1]}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/\pixels_count_reg[1] /*}

#add wave -noupdate -divider {MATRIX/Column0/EOC/\bits_count_reg[0]}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/\bits_count_reg[0] /*}

#add wave -noupdate -divider {DigitalPeri/ROCTRL/\configStage_0_Del_reg[4]}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\configStage_0_Del_reg[4] /*}

#add wave -noupdate -divider {DigitalPeri/CONFIG/\config_word_reg[4]}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/CONFIG/\config_word_reg[4] /*}

#add wave -noupdate -divider {DigitalPeri/ROCTRL/\configStage_0_Del_reg[0]}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\configStage_0_Del_reg[0] /*} 

#add wave -noupdate -divider {DigitalPeri/ROCTRL/\configStage_1_Del_reg[0]}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\configStage_1_Del_reg[0] /*}

#add wave -noupdate -divider {DigitalPeri/RSTNCTRL/rstn_out_reg}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/RSTNCTRL/rstn_out_reg/*}
	

###############################################################################



######### Signals associated to violations detected on 5/12/18 ################

add wave -noupdate sim:/CLICTD_testbench_top/pins_if/RSTN_pad

add wave -noupdate -divider {DigitalPeri/RSTNCTRL}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/RSTNCTRL/rstn_out}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/RSTNCTRL/rstn_in}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/RSTNCTRL/FE_OFN398_n_0}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/RSTNCTRL/clk_in}

add wave -noupdate -divider {DigitalPeri/SCTRL/I2CS}

add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SCL}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAen }
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAout}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAin}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/rst}

add wave -noupdate -divider {DigitalPeri/SCTRL/I2CS/SDAGF}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAGF/*}




add wave -noupdate -divider {DigitalPeri/SCTRL/RO}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/RO/readoutStart }
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/RO/readoutStartInt }
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/RO/readoutStartExt }
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/RO/readoutStartExtEn}



add wave -noupdate -divider {DigitalPeri/ROCTRL/readoutStart_meta_reg}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutStart_meta_reg/*}

add wave -noupdate -divider {DigitalPeri/ROCTRL/readoutStart_sync_reg}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutStart_sync_reg/*}

add wave -noupdate -divider {DigitalPeri/ROCTRL/}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutStart }
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutStart_sync }
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/readoutStart_meta }



add wave -noupdate -divider {DigitalPeri/ROCTRL/dataOut_reg}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/dataOut_reg/*}

add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[0]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[0] /*}

add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[1]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[1] /*}

add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[2]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[2] /*}

add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[3]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[3] /*}

add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[4]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[4] /*}

add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[5]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[5] /*}

add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[6]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[6] /*}

add wave -noupdate -divider {MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[7]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[1].Spix_gen.Spix /\pixel_gen[1].pixel_gen.pixel /Counter/\toa_reg[7] /*}


###############################################################################



TreeUpdate [SetDefaultTree]
#WaveRestoreCursors {{Cursor 1} {4286575000000 fs} 1} {{Cursor 2} {5602197320 fs} 0}
#quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
#WaveRestoreZoom {5584047 ps} {5923953 ps}

