# Filename:          wave_CLICTD_reset_test.do
# Author:            Núria Egidos 
# Created on:        15/06/2018
# Last modified on:  18/06/2018
# Project:           Doctoral student, verification of CLICTD
# Description:       Macro containing the waveforms to be loaded to Modelsim after the simulation
# Derived from:      Adrian Fiergolski scripts in https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim
#


#sim:/CLICTD_testbench_top/pins_if/PWRE_pad \
#sim:/CLICTD_testbench_top/pins_if/TPULSE_pad \
#sim:/CLICTD_testbench_top/pins_if/READOUT_pad \
#sim:/CLICTD_testbench_top/pins_if/SHUTTER_pad \
#sim:/CLICTD_testbench_top/pins_if/ENABLE_OUT_pad \
#sim:/CLICTD_testbench_top/pins_if/readoutStartExtEn \
#sim:/CLICTD_testbench_top/pins_if/readoutStart \
#sim:/CLICTD_testbench_top/pins_if/testPulseExtEnable \
#sim:/CLICTD_testbench_top/pins_if/testPulse \
#sim:/CLICTD_testbench_top/pins_if/powerExtEnable \
#sim:/CLICTD_testbench_top/pins_if/powerEnable \
#sim:/CLICTD_testbench_top/pins_if/tpEnableDigital \
#sim:/CLICTD_testbench_top/pins_if/mask \
#sim:/CLICTD_testbench_top/pins_if/tpEnableAnalog \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC0 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC1 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC2 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC3 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC4 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC5 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC6 \
#sim:/CLICTD_testbench_top/pins_if/tuningDAC7 \
#sim:/CLICTD_testbench_top/pins_if/disc

onerror {resume}
quietly WaveActivateNextPane {} 0


# Bug fixed on 20/6/18 by Iraklis Kremastiotis: the LVDS receiver enabled should be initialized to 1 before 
# the reset is applied at RSTN_pad, so that this reset can reach the logic
#add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/RF/recEn
#add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/recCLK40/ENABLER
#add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/RSTNCTRL/rstn_in 
#add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/RSTNCTRL/rstn_out



#add wave -noupdate -divider {DigitalPeri/ROCTRL/lpg_RC_CG_HIER_INST0/RC_CGIC_INST}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/lpg_RC_CG_HIER_INST0/RC_CGIC_INST/*}
#add wave -noupdate -divider {DigitalPeri/ROCTRL/lpg_RC_CG_HIER_INST1/RC_CGIC_INST}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/lpg_RC_CG_HIER_INST1/RC_CGIC_INST/*}
#add wave -noupdate -divider {DigitalPeri/ROCTRL/lpg_RC_CG_HIER_INST2/RC_CGIC_INST}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/lpg_RC_CG_HIER_INST2/RC_CGIC_INST/*}


#add wave -noupdate -divider {DigitalPeri/ROCTRL/CNT/lpg_RC_CG_HIER_INST3/RC_CGIC_INST}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/CNT/lpg_RC_CG_HIER_INST3/RC_CGIC_INST/*}


#add wave -noupdate -divider {DigitalPeri/ROCTRL/CNT_END/lpg_RC_CG_HIER_INST4/RC_CGIC_INST}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/CNT_END/lpg_RC_CG_HIER_INST4/RC_CGIC_INST/*}


#add wave -noupdate -divider {DigitalPeri/ROCTRL/CNT_START/lpg_RC_CG_HIER_INST5/RC_CGIC_INST}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/CNT_START/lpg_RC_CG_HIER_INST5/RC_CGIC_INST/*}



#add wave -noupdate -divider {DigitalPeri/ROCTRL/clkConfMux_preserve}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/clkConfMux_preserve/*}
#add wave -noupdate -divider {DigitalPeri/ROCTRL/clkReadout_latch_preserve}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/clkReadout_latch_preserve/*}
#add wave -noupdate -divider {DigitalPeri/ROCTRL/clkConf_latch_preserve}
#add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/clkConf_latch_preserve/*}


add wave -noupdate -divider {Column6/EOC/clk40_latch_preserve}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/EOC/clk40_latch_preserve /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[2]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\counterRstn_pos_bit_reg[2] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\columnDoneDelayed_reg[5]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\columnDoneDelayed_reg[5] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\columnDoneReadout_reg[3]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\columnDoneReadout_reg[3] /*}
add wave -noupdate -divider {DigitalPeri/ROCTRL/\header_reg[2]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/ROCTRL/\header_reg[2] /*}
add wave -noupdate -divider {MATRIX/Column7/EOC/\bits_count_reg[0]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/EOC/\bits_count_reg[0] /*}
add wave -noupdate -divider {MATRIX/Column6/EOC/\pixels_count_reg[5}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/EOC/\pixels_count_reg[5] /*}
add wave -noupdate -divider {MATRIX/Column0/EOC/dataOut_peri_reg}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/dataOut_peri_reg /*}
add wave -noupdate -divider {MATRIX/Column3/EOC/\countReadoutn_D_reg[2]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/EOC/\countReadoutn_D_reg[2] /*}
add wave -noupdate -divider {MATRIX/Column13/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /Counter/\toa_reg[5]}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column13/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /Counter/\toa_reg[5] /*}
add wave -noupdate -divider {MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/\genblk1[7].genblk1.hit /hit_reg}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/\genblk1[7].genblk1.hit /hit_reg /*}


add wave -noupdate -divider {Stimuli (pins_if)}
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/RSTN_pad 
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/CLK_100_n_pad 
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/CLK_100_p_pad 
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/CLK_40_n_pad 
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/CLK_40_p_pad 

#add wave -noupdate -divider {Control (slowcontrol_if)}
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/enable_clock_40MHz
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/enable_clock_100MHz

add wave -noupdate -divider {Outputs (pins_if)}
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/columnDone 
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/dataIn 
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/commonToken 

add wave -noupdate -divider {Outputs (readout_if)}
add wave -noupdate sim:/CLICTD_testbench_top/readout_if/DATA_OUT 
add wave -noupdate sim:/CLICTD_testbench_top/readout_if/CLK_OUT

#add wave -noupdate -divider {Pins_agent}
#add wave -noupdate sim:/uvm_root/uvm_test_top/env/pins_agent/pins_monitor/pins_vif/RSTN_pad




#add wave -noupdate -divider {I2C slave}
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/SDAin
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/SDAout
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/SDAen
#add wave -noupdate sim:/CLICTD_testbench_top/SCL_pad
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/register_address_read
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/register_content_read
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/high_when_regreadout_finished

#add wave -noupdate -divider {I2C master}
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/i2cstate
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/high_when_regreadout_finished
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/SDA
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/MS_SDAout
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/MS_SDAen
#add wave -noupdate sim:/CLICTD_testbench_top/slowcontrol_if/slave_addr




TreeUpdate [SetDefaultTree]
#WaveRestoreCursors {{Cursor 1} {4286575000000 fs} 1} {{Cursor 2} {5602197320 fs} 0}
#quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
#WaveRestoreZoom {5584047 ps} {5923953 ps}

