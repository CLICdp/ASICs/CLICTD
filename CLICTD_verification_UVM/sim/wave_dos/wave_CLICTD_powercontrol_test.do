# Filename:          wave_CLICTD_powercontrol_test.do
# Author:            Núria Egidos 
# Created on:        1/10/2018
# Last modified on:  1/10/2018
# Project:           Doctoral student, verification of CLICTD
# Description:       Macro containing the waveforms to be loaded to Modelsim after the simulation
# Derived from:      Adrian Fiergolski scripts in https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim
#

onerror {resume}
quietly WaveActivateNextPane {} 0


add wave -noupdate sim:/CLICTD_testbench_top/pins_if/RSTN_pad 


add wave -noupdate -divider {Outputs (readout_if)}
add wave -noupdate sim:/CLICTD_testbench_top/readout_if/DATA_OUT 
add wave -noupdate sim:/CLICTD_testbench_top/readout_if/CLK_OUT
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/ENABLE_OUT_pad


add wave -noupdate -divider {Stimuli}

#add wave -noupdate sim:/uvm_root/uvm_test_top/chip_status_obj
add wave -noupdate sim:/uvm_root/uvm_test_top/chip_status_obj.chip_status
#add wave -noupdate sim:@ChipStatusInfo@1.chip_status

add wave -noupdate sim:/uvm_root/uvm_test_top/bits_slowcontrol_reg_obj.pwrExtEn
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/powerExtEnable

add wave -noupdate sim:/CLICTD_testbench_top/pins_if/PWREN_pad 
add wave -noupdate sim:/uvm_root/uvm_test_top/env/pins_scoreboard/pins_model/rise_pwren_pad_event_arrived
add wave -noupdate sim:/uvm_root/uvm_test_top/env/pins_scoreboard/pins_model/fall_pwren_pad_event_arrived



add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/powerInternal
#add wave -noupdate sim:/uvm_root/uvm_test_top/env/pins_scoreboard/pins_model/rise_pwrInt_event_arrived
#add wave -noupdate sim:/uvm_root/uvm_test_top/env/pins_scoreboard/pins_model/fall_pwrInt_event_arrived
add wave -noupdate sim:/uvm_root/uvm_test_top/env/pins_scoreboard/pins_model/rise_powerInternal_event_arrived
add wave -noupdate sim:/uvm_root/uvm_test_top/env/pins_scoreboard/pins_model/fall_powerInternal_event_arrived
#add wave -noupdate sim:/uvm_root/uvm_test_top/bits_slowcontrol_reg_obj.pwrInt
#add wave -noupdate sim:@BitsMonitorSlowcontrolReg@1.pwrExtEn
#add wave -noupdate sim:@BitsMonitorSlowcontrolReg@1.pwrInt

add wave -noupdate sim:/CLICTD_testbench_top/pins_if/powerEnable 
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/CLK_100_GATED
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_100_GATED
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_100_p_pad
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_100_n_pad
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/enable_clock_100MHz
add wave -noupdate sim:/uvm_root/uvm_test_top/env/pins_agent/pins_monitor/counter_clk100gated_while_powerEnable_is_low





TreeUpdate [SetDefaultTree]
#WaveRestoreCursors {{Cursor 1} {4286575000000 fs} 1} {{Cursor 2} {5602197320 fs} 0}
#quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
#WaveRestoreZoom {5584047 ps} {5923953 ps}

