# Filename:          wave_CLICTD_readout_test.do
# Author:            Núria Egidos 
# Created on:        11/09/2018
# Last modified on:  6/11/2018
# Project:           Doctoral student, verification of CLICTD
# Description:       Macro containing the waveforms to be loaded to Modelsim after the simulation
# Derived from:      Adrian Fiergolski scripts in https://gitlab.cern.ch/CLICdp/ASICs/CLICpix2/blob/master/vrf/FullDesign/sim
#



onerror {resume}
quietly WaveActivateNextPane {} 0

add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/noCompression

add wave -noupdate -label {slowcontrol_if/SCL_pad} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SCL_pad}
add wave -noupdate -label {slowcontrol_if/MS_SDAout} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/MS_SDAout}
add wave -noupdate -label {slowcontrol_if/MS_SDAen} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/MS_SDAen }
add wave -noupdate -label {slowcontrol_if/SL_SDAout} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SL_SDAout }
add wave -noupdate -label {slowcontrol_if/SL_SDAen} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SL_SDAen }
add wave -noupdate -label {slowcontrol_if/SDA} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SDA }
add wave -noupdate -label {slowcontrol_if/SDAen} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SDAen }
add wave -noupdate -label {slowcontrol_if/SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SDAin }
add wave -noupdate -label {slowcontrol_if/SDAout} {sim:/CLICTD_testbench_top/dut_wrapper/slowcontrol_if/SDAout}
add wave -noupdate -label {port SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/dut/SDAin}
add wave -noupdate -label {port SDAout} {sim:/CLICTD_testbench_top/dut_wrapper/dut/SDAout}
add wave -noupdate -label {port SDAen} {sim:/CLICTD_testbench_top/dut_wrapper/dut/SDAen}
add wave -noupdate -label {DigitalPeri/SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SDAin} 
add wave -noupdate -label {DigitalPeri/SCTRL/SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/SDAin} 
add wave -noupdate -label {DigitalPeri/SCTRL/I2CS/SDAin} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAin} 
add wave -noupdate -label {DigitalPeri/SCTRL/I2CS/SDAGF/in} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAGF/in} 
add wave -noupdate -label {DigitalPeri/SCTRL/I2CS/SDAGF/out_reg/D} {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAGF/out_reg/D} 
add wave -noupdate -divider {DigitalPeri/SCTRL/I2CS/SDAGF/ }
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/I2CS/SDAGF/*}




add wave -noupdate -divider {Stimuli (pins_if)}
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/RSTN_pad 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_100_n_pad 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_100_p_pad 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_40_n_pad 
add wave -noupdate sim:/CLICTD_testbench_top/dut_wrapper/dut/CLK_40_p_pad 

add wave -noupdate -divider {Control (slowcontrol_if)}
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/enable_clock_100MHz

add wave -noupdate -divider {Outputs (pins_if)}
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/columnDone 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/dataIn 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/commonToken 

#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tpEnableDigital 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/mask 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tpEnableAnalog 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC0 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC1
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC2 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC3 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC4 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC5 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC6 
#add wave -noupdate sim:/CLICTD_testbench_top/pins_if/tuningDAC7 


add wave -noupdate -label {mask[0][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[0][0][0]}
add wave -noupdate -label {mask[1][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[1][0][0]}
add wave -noupdate -label {mask[2][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[2][0][0]}
add wave -noupdate -label {mask[3][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[3][0][0]}
add wave -noupdate -label {mask[4][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[4][0][0]}
add wave -noupdate -label {mask[5][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[5][0][0]}
add wave -noupdate -label {mask[6][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[6][0][0]}
add wave -noupdate -label {mask[7][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[7][0][0]}
add wave -noupdate -label {mask[8][0][0]} {sim:/CLICTD_testbench_top/pins_if/mask[8][0][0]}

add wave -noupdate -label {tpEnableDigital[0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[0][0][0]}
add wave -noupdate -label {tpEnableDigital[1][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[1][0][0]}
add wave -noupdate -label {tpEnableDigital[2][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[2][0][0]}
add wave -noupdate -label {tpEnableDigital[3][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[3][0][0]}
add wave -noupdate -label {tpEnableDigital[4][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[4][0][0]}
add wave -noupdate -label {tpEnableDigital[5][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[5][0][0]}
add wave -noupdate -label {tpEnableDigital[6][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[6][0][0]}
add wave -noupdate -label {tpEnableDigital[7][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[7][0][0]}
add wave -noupdate -label {tpEnableDigital[8][0][0]} {sim:/CLICTD_testbench_top/pins_if/tpEnableDigital[8][0][0]}



#add wave -noupdate -label {tuningDAC1[0][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[0][0][0][0]}
#add wave -noupdate -label {tuningDAC1[1][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[1][0][0][0]}
#add wave -noupdate -label {tuningDAC1[2][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[2][0][0][0]}
#add wave -noupdate -label {tuningDAC1[3][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[3][0][0][0]}
#add wave -noupdate -label {tuningDAC1[4][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[4][0][0][0]}
#add wave -noupdate -label {tuningDAC1[5][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[5][0][0][0]}
#add wave -noupdate -label {tuningDAC1[6][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[6][0][0][0]}
#add wave -noupdate -label {tuningDAC1[7][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[7][0][0][0]}
#add wave -noupdate -label {tuningDAC1[8][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[8][0][0][0]}
#add wave -noupdate -label {tuningDAC1[9][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[9][0][0][0]}
#add wave -noupdate -label {tuningDAC1[10][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[10][0][0][0]}
#add wave -noupdate -label {tuningDAC1[11][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[11][0][0][0]}
#add wave -noupdate -label {tuningDAC1[12][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[12][0][0][0]}
#add wave -noupdate -label {tuningDAC1[13][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[13][0][0][0]}
#add wave -noupdate -label {tuningDAC1[14][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[14][0][0][0]}
#add wave -noupdate -label {tuningDAC1[15][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC1[15][0][0][0]}

#add wave -noupdate -label {tuningDAC7[0][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[0][0][0][2]}
#add wave -noupdate -label {tuningDAC7[1][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[1][0][0][2]}
##add wave -noupdate -label {tuningDAC7[2][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[2][0][0][2]}
#add wave -noupdate -label {tuningDAC7[3][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[3][0][0][2]}
#add wave -noupdate -label {tuningDAC7[4][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[4][0][0][2]}
#add wave -noupdate -label {tuningDAC7[5][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[5][0][0][2]}
#add wave -noupdate -label {tuningDAC7[6][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[6][0][0][2]}
#add wave -noupdate -label {tuningDAC7[7][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[7][0][0][2]}
#add wave -noupdate -label {tuningDAC7[8][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[8][0][0][2]}
#add wave -noupdate -label {tuningDAC7[9][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[9][0][0][2]}
#add wave -noupdate -label {tuningDAC7[10][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[10][0][0][2]}
#add wave -noupdate -label {tuningDAC7[11][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[11][0][0][2]}
#add wave -noupdate -label {tuningDAC7[12][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[12][0][0][2]}
#add wave -noupdate -label {tuningDAC7[13][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[13][0][0][2]}
#add wave -noupdate -label {tuningDAC7[14][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[14][0][0][2]}
#add wave -noupdate -label {tuningDAC7[15][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/tuningDAC7[15][0][0][2]}



add wave -noupdate sim:/uvm_root/uvm_test_top/config_matrix_obj


add wave -noupdate -divider {Outputs (readout_if)}
add wave -noupdate sim:/CLICTD_testbench_top/readout_if/DATA_OUT 
add wave -noupdate sim:/CLICTD_testbench_top/readout_if/CLK_OUT
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/ENABLE_OUT_pad

#add wave -noupdate -divider {Serial readout}
#add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/chip_status_obj
#add wave -noupdate -radix decimal sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/bit_readout_counter 
#add wave -noupdate -radix decimal sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/aux_bit_counter 
#add wave -noupdate -radix decimal sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/column_counter 
#add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/window 
#add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/readout_status 

add wave -noupdate -divider {Force hits}

add wave -noupdate sim:/CLICTD_testbench_top/pins_if/testPulseExtEnable
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/testPulse
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/discTp
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/SHUTTER_pad
add wave -noupdate sim:/CLICTD_testbench_top/pins_if/TPULSE_pad

add wave -noupdate -divider {Force hits, DigitalPeri/SCTRL/TP}
add wave -noupdate {sim:/CLICTD_testbench_top/dut_wrapper/dut/DigitalPeri/SCTRL/TP/*}





########################################### POSTLAYOUT COLUMNS #################################################3


add wave -noupdate -divider {Column 0 - digital}
add wave -noupdate -label {discTp  [0][0][0]} {sim:/CLICTD_testbench_top/pins_if/discTp[0][0][0]}
add wave -noupdate -label {disc_int[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {disc_int[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToT [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {clock ToA [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {count ToA [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/cnt_toa}
add wave -noupdate -label {count ToA [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /cnt_toa}
add wave -noupdate -label {toa/disc superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/toa/disc}
add wave -noupdate -label {toa/disc superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /toa/disc}
add wave -noupdate -label {toa/clk_out superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/toa/clk_out}
add wave -noupdate -label {toa/clk_out superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /toa/clk_out}
add wave -noupdate -label {clk_in superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_in}
add wave -noupdate -label {clk_in superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_in}
add wave -noupdate -label {shutter_in superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/shutter_in}
add wave -noupdate -label {shutter_in superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /shutter_in}
add wave -noupdate -label {count_readoutn superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/count_readoutn}
add wave -noupdate -label {count_readoutn superpixel[0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /count_readoutn}
add wave -noupdate -label {col 0 EOC/clk_out} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/EOC/clk_out}
add wave -noupdate -label {col 0 EOC/clk_out} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/EOC/clk_out}
add wave -noupdate -label {recCLK100/OUT}  sim:/CLICTD_testbench_top/dut_wrapper/dut/recCLK100/OUT
add wave -noupdate -label {IO_PADS/PAD_CLK100p/PAD} sim:/CLICTD_testbench_top/dut_wrapper/dut/IO_PADS/PAD_CLK100p/PAD


add wave -noupdate -label {IN       [0][0][0][1]} {sim:/CLICTD_testbench_top/pins_if/disc[0][0][0][1]}
add wave -noupdate -label {OUTDisc  [0][0][0][1]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[1].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [0][0][0][1]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\fe_row[1].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [0][0][0][1]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[1]}
add wave -noupdate -label {disc     [0][0][0][1]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix/\pixel_gen[0].pixel_gen.pixel /disc[1]}
add wave -noupdate -label {AND mask [0][0][0][1], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_1_preserve/AN}
add wave -noupdate -label {AND mask [0][0][0][1], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_1_preserve/AN}
add wave -noupdate -label {AND mask [0][0][0][1], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_1_preserve/B}
add wave -noupdate -label {AND mask [0][0][0][1], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_1_preserve/B}
add wave -noupdate -label {AND mask [0][0][0][1], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_1_preserve/Q}
add wave -noupdate -label {AND mask [0][0][0][1], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_1_preserve/Q}
add wave -noupdate -label {disc_m   [0][0][0][1]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[1]}
add wave -noupdate -label {disc_m   [0][0][0][1]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[1]}
add wave -noupdate -label {IN       [0][0][0][2]} {sim:/CLICTD_testbench_top/pins_if/disc[0][0][0][2]}
add wave -noupdate -label {OUTDisc  [0][0][0][2]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[2].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [0][0][0][2]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[2].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [0][0][0][2]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[2]}
add wave -noupdate -label {disc     [0][0][0][2]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[2]}
add wave -noupdate -label {AND mask [0][0][0][2], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_2_preserve/AN}
add wave -noupdate -label {AND mask [0][0][0][2], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_2_preserve/AN}
add wave -noupdate -label {AND mask [0][0][0][2], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_2_preserve/B}
add wave -noupdate -label {AND mask [0][0][0][2], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_2_preserve/B}
add wave -noupdate -label {AND mask [0][0][0][2], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_2_preserve/Q}
add wave -noupdate -label {AND mask [0][0][0][2], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_2_preserve/Q}
add wave -noupdate -label {disc_m   [0][0][0][2]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[2]}
add wave -noupdate -label {disc_m   [0][0][0][2]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[2]}
add wave -noupdate -label {disc_out [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {disc_out [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {discTp   [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {discTp   [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {disc_int [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {disc_int [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToT [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {clock ToA [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {hit map [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[0]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}
add wave -noupdate -label {hit map clock [0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column0/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}



add wave -noupdate -divider {Column 1 - force disc}
add wave -noupdate -label {IN       [1][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[1][0][0][0]}
add wave -noupdate -label {OUTDisc  [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {disc     [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix/\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {AND mask [1][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [1][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [1][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [1][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [1][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {AND mask [1][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {disc_m   [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {IN       [1][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[1][0][0][3]}
add wave -noupdate -label {OUTDisc  [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {disc     [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {AND mask [1][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [1][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [1][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [1][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [1][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {AND mask [1][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_m   [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_out [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {disc_out [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {discTp   [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {discTp   [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {disc_int [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {disc_int [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToT [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {clock ToA [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {hit map [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}
add wave -noupdate -label {hit map clock [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}


add wave -noupdate -divider {Column 2 - force disc}
add wave -noupdate -label {IN       [2][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[2][0][0][0]}
add wave -noupdate -label {OUTDisc  [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {disc     [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix/\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {AND mask [2][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [2][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [2][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [2][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [2][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {AND mask [2][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {disc_m   [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {IN       [2][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[2][0][0][3]}
add wave -noupdate -label {OUTDisc  [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {disc     [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {AND mask [2][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [2][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [2][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [2][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [2][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {AND mask [2][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_m   [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_out [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {disc_out [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {discTp   [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {discTp   [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {disc_int [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {disc_int [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToT [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {clock ToA [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {hit map [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}
add wave -noupdate -label {hit map clock [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}




add wave -noupdate -divider {Column 3 - force disc}
add wave -noupdate -label {IN       [3][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[3][0][0][0]}
add wave -noupdate -label {OUTDisc  [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {disc     [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix/\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {AND mask [3][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [3][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [3][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [3][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [3][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {AND mask [3][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {disc_m   [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {IN       [3][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[3][0][0][3]}
add wave -noupdate -label {OUTDisc  [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {disc     [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {AND mask [3][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [3][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [3][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [3][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [3][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {AND mask [3][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_m   [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_out [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {disc_out [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {discTp   [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {discTp   [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {disc_int [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {disc_int [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToT [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {clock ToA [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {hit map [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}
add wave -noupdate -label {hit map clock [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}


add wave -noupdate -divider {Column 4 - all masked}
add wave -noupdate -label {IN       [4][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[4][0][0][0]}
add wave -noupdate -label {OUTDisc  [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {disc     [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix/\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {AND mask [4][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [4][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [4][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [4][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [4][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {AND mask [4][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {disc_m   [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {IN       [4][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[4][0][0][3]}
add wave -noupdate -label {OUTDisc  [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {disc     [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {AND mask [4][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [4][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [4][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [4][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [4][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {AND mask [4][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_m   [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_out [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {disc_out [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {discTp   [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {discTp   [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {disc_int [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {disc_int [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToT [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {clock ToA [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {hit map [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}
add wave -noupdate -label {hit map clock [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}


add wave -noupdate -divider {Column 5 - force disc}
add wave -noupdate -label {IN       [5][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[5][0][0][0]}
add wave -noupdate -label {OUTDisc  [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {disc     [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix/\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {AND mask [5][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [5][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [5][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [5][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [5][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {AND mask [5][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {disc_m   [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {IN       [5][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[5][0][0][3]}
add wave -noupdate -label {OUTDisc  [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {disc     [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {AND mask [5][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [5][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [5][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [5][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [5][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {AND mask [5][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_m   [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_out [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {disc_out [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {discTp   [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {discTp   [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {disc_int [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {disc_int [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToT [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {clock ToA [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {hit map [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}
add wave -noupdate -label {hit map clock [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}


add wave -noupdate -divider {Column 6 - force disc}
add wave -noupdate -label {IN       [6][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[6][0][0][0]}
add wave -noupdate -label {OUTDisc  [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {disc     [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix/\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {AND mask [6][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [6][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [6][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [6][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [6][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {AND mask [6][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {disc_m   [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {IN       [6][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[6][0][0][3]}
add wave -noupdate -label {OUTDisc  [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {disc     [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {AND mask [6][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [6][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [6][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [6][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [6][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {AND mask [6][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_m   [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_out [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {disc_out [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {discTp   [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {discTp   [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {disc_int [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {disc_int [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToT [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {clock ToA [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {hit map [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}
add wave -noupdate -label {hit map clock [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}

add wave -noupdate -divider {Column 7 - force disc}
add wave -noupdate -label {IN       [7][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[7][0][0][0]}
add wave -noupdate -label {OUTDisc  [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {disc     [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix/\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {AND mask [7][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [7][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [7][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [7][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [7][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {AND mask [7][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {disc_m   [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {IN       [7][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[7][0][0][3]}
add wave -noupdate -label {OUTDisc  [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {disc     [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {AND mask [7][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [7][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [7][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [7][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [7][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {AND mask [7][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_m   [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_out [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {disc_out [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {discTp   [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {discTp   [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {disc_int [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {disc_int [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToT [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {clock ToA [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {hit map [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}
add wave -noupdate -label {hit map clock [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}


add wave -noupdate -divider {Column 8 - all masked}
add wave -noupdate -label {IN       [8][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[8][0][0][0]}
add wave -noupdate -label {OUTDisc  [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[0].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {disc     [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix/\pixel_gen[0].pixel_gen.pixel /disc[0]}
add wave -noupdate -label {AND mask [8][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [8][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/AN}
add wave -noupdate -label {AND mask [8][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [8][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/B}
add wave -noupdate -label {AND mask [8][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {AND mask [8][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {disc_m   [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[0]}
add wave -noupdate -label {IN       [8][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[8][0][0][3]}
add wave -noupdate -label {OUTDisc  [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {OUTDisc  [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\fe_row[0].fe_cluster[3].ANALOG_FE /OUTDisc}
add wave -noupdate -label {disc     [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {disc     [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc[3]}
add wave -noupdate -label {AND mask [8][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [8][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/AN}
add wave -noupdate -label {AND mask [8][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [8][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/B}
add wave -noupdate -label {AND mask [8][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {AND mask [8][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_m   [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_m[3]}
add wave -noupdate -label {disc_out [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {disc_out [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_out}
add wave -noupdate -label {discTp   [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {discTp   [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /discTp}
add wave -noupdate -label {disc_int [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {disc_int [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /disc_int}
add wave -noupdate -label {clock ToT [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToT [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clkToT/clk_out}
add wave -noupdate -label {clock ToA [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {clock ToA [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /clk_toa}
add wave -noupdate -label {hit map [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}
add wave -noupdate -label {hit map clock [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/\Spix_gen[0].Spix_gen.Spix /\pixel_gen[0].pixel_gen.pixel /hit_bits/clk}




########################################### RTL COLUMNS #################################################3

add wave -noupdate -divider {Column 1 - force disc}
add wave -noupdate -label {IN       [1][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[1][0][0][0]}
add wave -noupdate -label {OUTDisc  [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[0]}
add wave -noupdate -label {disc     [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0].Spix_gen.Spix/pixel_gen[0].pixel_gen.pixel/disc[0]}
add wave -noupdate -label {AND mask [1][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [1][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [1][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [1][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [1][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {AND mask [1][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {disc_m   [1][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {IN       [1][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[1][0][0][3]}
add wave -noupdate -label {OUTDisc  [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {disc     [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {AND mask [1][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [1][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [1][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [1][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [1][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {AND mask [1][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_m   [1][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_out [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {disc_out [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {discTp   [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {discTp   [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {disc_int [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {disc_int [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {clock ToT [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToT [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToA [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {clock ToA [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {hit map [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[1]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}
add wave -noupdate -label {hit map clock [1][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column1/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}


add wave -noupdate -divider {Column 2 - force disc}
add wave -noupdate -label {IN       [2][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[2][0][0][0]}
add wave -noupdate -label {OUTDisc  [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[0]}
add wave -noupdate -label {disc     [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0].Spix_gen.Spix/pixel_gen[0].pixel_gen.pixel/disc[0]}
add wave -noupdate -label {AND mask [2][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [2][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [2][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [2][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [2][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {AND mask [2][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {disc_m   [2][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {IN       [2][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[2][0][0][3]}
add wave -noupdate -label {OUTDisc  [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {disc     [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {AND mask [2][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [2][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [2][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [2][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [2][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {AND mask [2][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_m   [2][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_out [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {disc_out [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {discTp   [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {discTp   [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {disc_int [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {disc_int [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {clock ToT [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToT [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToA [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {clock ToA [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {hit map [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[2]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}
add wave -noupdate -label {hit map clock [2][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column2/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}




add wave -noupdate -divider {Column 3 - force disc}
add wave -noupdate -label {IN       [3][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[3][0][0][0]}
add wave -noupdate -label {OUTDisc  [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[0]}
add wave -noupdate -label {disc     [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0].Spix_gen.Spix/pixel_gen[0].pixel_gen.pixel/disc[0]}
add wave -noupdate -label {AND mask [3][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [3][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [3][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [3][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [3][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {AND mask [3][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {disc_m   [3][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {IN       [3][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[3][0][0][3]}
add wave -noupdate -label {OUTDisc  [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {disc     [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {AND mask [3][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [3][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [3][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [3][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [3][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {AND mask [3][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_m   [3][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_out [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {disc_out [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {discTp   [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {discTp   [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {disc_int [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {disc_int [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {clock ToT [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToT [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToA [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {clock ToA [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {hit map [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[3]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}
add wave -noupdate -label {hit map clock [3][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column3/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}


add wave -noupdate -divider {Column 4 - all masked}
add wave -noupdate -label {IN       [4][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[4][0][0][0]}
add wave -noupdate -label {OUTDisc  [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[0]}
add wave -noupdate -label {disc     [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0].Spix_gen.Spix/pixel_gen[0].pixel_gen.pixel/disc[0]}
add wave -noupdate -label {AND mask [4][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [4][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [4][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [4][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [4][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {AND mask [4][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {disc_m   [4][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {IN       [4][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[4][0][0][3]}
add wave -noupdate -label {OUTDisc  [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {disc     [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {AND mask [4][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [4][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [4][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [4][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [4][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {AND mask [4][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_m   [4][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_out [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {disc_out [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {discTp   [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {discTp   [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {disc_int [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {disc_int [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {clock ToT [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToT [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToA [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {clock ToA [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {hit map [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[4]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}
add wave -noupdate -label {hit map clock [4][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column4/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}


add wave -noupdate -divider {Column 5 - force disc}
add wave -noupdate -label {IN       [5][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[5][0][0][0]}
add wave -noupdate -label {OUTDisc  [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[0]}
add wave -noupdate -label {disc     [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0].Spix_gen.Spix/pixel_gen[0].pixel_gen.pixel/disc[0]}
add wave -noupdate -label {AND mask [5][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [5][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [5][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [5][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [5][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {AND mask [5][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {disc_m   [5][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {IN       [5][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[5][0][0][3]}
add wave -noupdate -label {OUTDisc  [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {disc     [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {AND mask [5][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [5][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [5][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [5][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [5][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {AND mask [5][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_m   [5][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_out [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {disc_out [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {discTp   [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {discTp   [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {disc_int [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {disc_int [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {clock ToT [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToT [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToA [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {clock ToA [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {hit map [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[5]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}
add wave -noupdate -label {hit map clock [5][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column5/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}


add wave -noupdate -divider {Column 6 - force disc}
add wave -noupdate -label {IN       [6][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[6][0][0][0]}
add wave -noupdate -label {OUTDisc  [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[0]}
add wave -noupdate -label {disc     [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0].Spix_gen.Spix/pixel_gen[0].pixel_gen.pixel/disc[0]}
add wave -noupdate -label {AND mask [6][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [6][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [6][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [6][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [6][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {AND mask [6][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {disc_m   [6][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {IN       [6][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[6][0][0][3]}
add wave -noupdate -label {OUTDisc  [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {disc     [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {AND mask [6][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [6][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [6][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [6][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [6][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {AND mask [6][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_m   [6][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_out [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {disc_out [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {discTp   [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {discTp   [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {disc_int [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {disc_int [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {clock ToT [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToT [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToA [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {clock ToA [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {hit map [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[6]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}
add wave -noupdate -label {hit map clock [6][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column6/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}

add wave -noupdate -divider {Column 7 - force disc}
add wave -noupdate -label {IN       [7][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[7][0][0][0]}
add wave -noupdate -label {OUTDisc  [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[0]}
add wave -noupdate -label {disc     [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0].Spix_gen.Spix/pixel_gen[0].pixel_gen.pixel/disc[0]}
add wave -noupdate -label {AND mask [7][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [7][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [7][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [7][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [7][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {AND mask [7][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {disc_m   [7][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {IN       [7][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[7][0][0][3]}
add wave -noupdate -label {OUTDisc  [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {disc     [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {AND mask [7][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [7][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [7][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [7][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [7][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {AND mask [7][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_m   [7][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_out [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {disc_out [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {discTp   [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {discTp   [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {disc_int [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {disc_int [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {clock ToT [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToT [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToA [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {clock ToA [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {hit map [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[7]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}
add wave -noupdate -label {hit map clock [7][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column7/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}


add wave -noupdate -divider {Column 8 - all masked}
add wave -noupdate -label {IN       [8][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[8][0][0][0]}
add wave -noupdate -label {OUTDisc  [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[0]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[0]}
add wave -noupdate -label {disc     [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0].Spix_gen.Spix/pixel_gen[0].pixel_gen.pixel/disc[0]}
add wave -noupdate -label {AND mask [8][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [8][0][0][0], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/AN}
add wave -noupdate -label {AND mask [8][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [8][0][0][0], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/B}
add wave -noupdate -label {AND mask [8][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {AND mask [8][0][0][0], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_0_preserve/Q}
add wave -noupdate -label {disc_m   [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {disc_m   [8][0][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[0]}
add wave -noupdate -label {IN       [8][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[8][0][0][3]}
add wave -noupdate -label {OUTDisc  [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {OUTDisc  [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/fe_row[0]/fe_cluster[3]/ANALOG_FE/OUTDisc}
add wave -noupdate -label {disc     [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {disc     [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc[3]}
add wave -noupdate -label {AND mask [8][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [8][0][0][3], AN} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/AN}
add wave -noupdate -label {AND mask [8][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [8][0][0][3], B} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/B}
add wave -noupdate -label {AND mask [8][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {AND mask [8][0][0][3], Q} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/mask_3_preserve/Q}
add wave -noupdate -label {disc_m   [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_m   [8][0][0][3]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_m[3]}
add wave -noupdate -label {disc_out [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {disc_out [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_out}
add wave -noupdate -label {discTp   [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {discTp   [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/discTp}
add wave -noupdate -label {disc_int [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {disc_int [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/disc_int}
add wave -noupdate -label {clock ToT [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToT [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clkToT/clk_out}
add wave -noupdate -label {clock ToA [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {clock ToA [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/clk_toa}
add wave -noupdate -label {hit map [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/hit_bits}
add wave -noupdate -label {hit map clock [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/column_gen[8]/column_gen/column/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}
add wave -noupdate -label {hit map clock [8][0][0]} {sim:/CLICTD_testbench_top/dut_wrapper/dut/MATRIX/Column8/Spix_gen[0]/Spix_gen/Spix/pixel_gen[0]/pixel_gen/pixel/hit_bits/clk}


#add wave -noupdate -label {disc[1][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[1][0][0][0]}
#add wave -noupdate -label {disc[1][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[1][0][0][3]}
#add wave -noupdate -label {disc[2][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[2][0][0][0]}
#add wave -noupdate -label {disc[2][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[2][0][0][3]}
#add wave -noupdate -label {disc[3][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[3][0][0][0]}
#add wave -noupdate -label {disc[3][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[3][0][0][3]}
#add wave -noupdate -label {disc[5][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[5][0][0][0]}
#add wave -noupdate -label {disc[5][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[5][0][0][3]}
#add wave -noupdate -label {disc[6][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[6][0][0][0]}
#add wave -noupdate -label {disc[6][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[6][0][0][3]}
#add wave -noupdate -label {disc[7][0][0][0]} {sim:/CLICTD_testbench_top/pins_if/disc[7][0][0][0]}
#add wave -noupdate -label {disc[7][0][0][3]} {sim:/CLICTD_testbench_top/pins_if/disc[7][0][0][3]}

add wave -noupdate sim:/uvm_root/uvm_test_top/env/dataout_agent/dataout_monitor/chip_status_obj





TreeUpdate [SetDefaultTree]
#WaveRestoreCursors {{Cursor 1} {4286575000000 fs} 1} {{Cursor 2} {5602197320 fs} 0}
#quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
#WaveRestoreZoom {5584047 ps} {5923953 ps}

